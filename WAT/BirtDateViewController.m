//
//  BirtDateViewController.m
//  WAT
//
//  Created by neurosky on 5/26/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import "BirtDateViewController.h"
#import "Constant.h"

#import "TGBleEmulator.h"


@interface BirtDateViewController ()

@end

@implementation BirtDateViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    datePicker.datePickerMode = UIDatePickerModeDate;
    
    NSDateFormatter *nsdf = [[NSDateFormatter alloc]init];
    [nsdf setDateFormat:@"YYYY/MM/dd"];
    
    NSDate* minDate = [nsdf dateFromString:@"1900/01/01"  ];
    NSDate* maxDate = [NSDate date];
    
    datePicker.minimumDate = minDate;
    datePicker.maximumDate = maxDate;
    
    [datePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged ];
    // show the latest date;
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *birth;
        birth  =  [userDefault objectForKey:USER_BIRTH_DATE];
    NSLog(@"stored birth = %@",birth);
    if (birth != NULL && ![birth isEqualToString:@""]) {
        birth  =  [userDefault objectForKey:USER_BIRTH_DATE];
    }
    else{

        
        birth  =    [NSString stringWithFormat:@"%ld/%02d/%02ld",(long)[[TGBleManager sharedTGBleManager] birthYear],[[TGBleManager sharedTGBleManager] birthMonth], (long)[[TGBleManager sharedTGBleManager] birthDay]];
        
        
    }

    NSLog(@"birth = %@",birth);
    datePicker.date =  [nsdf dateFromString:birth];
    
}

- (NSUInteger)supportedInterfaceOrientations

{
    return UIInterfaceOrientationMaskPortrait;
    
}

-(BOOL)shouldAutorotate{
    
    return NO;
}
-(void)dateChanged:(id)sender{
    UIDatePicker* control = (UIDatePicker*)sender;
    selectedDate = control.date;
    
    NSLog(@"date  = %@",selectedDate);
    /*添加你自己响应代码*/
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)saveBtnClick:(id)sender {
    NSLog(@"BirthDate saveBtnClick---");
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [dateFormatter setDateFormat:@"YYYY/MM/dd"];
    NSString *dateString;
    if (selectedDate != NULL) {
        //        selectedDate = [NSDate date];
        //    }
        
        
        dateString = [dateFormatter stringFromDate:selectedDate];
        
        
        
        //        ageText.text = dateString;
        //        age = dateString;
    }
    else{
        NSString *storedBirthStr = [ud objectForKey:USER_BIRTH_DATE];
        if (storedBirthStr == NULL) {
            dateString = [dateFormatter stringFromDate:[NSDate  date]];
        }
        else{
            dateString =  storedBirthStr;
        }
    
    }
    
    NSLog(@"dateString = %@",dateString);
    
    
    [ud setObject:dateString forKey:USER_BIRTH_DATE];
    [ud synchronize];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancelBtnClick:(id)sender {
    NSLog(@"BirthDate cancelBtnClick---");
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end

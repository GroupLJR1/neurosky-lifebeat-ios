//
//  CardioZone2ViewController.m
//  WAT
//
//  Created by  NeuroSky on 5/21/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import "CardioZone2ViewController.h"

@interface CardioZone2ViewController ()

@end

@implementation CardioZone2ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSLog(@"CardioZone2ViewController =======viewDidLoad");
	
	//[[[UIApplication sharedApplication] delegate] setDotViewController:self];
	
	//EX_template_name = @"dot_templates";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)back:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}
@end

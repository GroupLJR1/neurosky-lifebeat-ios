//
//  UserProfile.m
//  WAT
//
//  Created by Julia on 7/29/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import "UserProfile.h"

@implementation UserProfile
@synthesize age,isFemale,height,isBandOnRight,walkingStepLength,runningStepLength;
-(id)init{
    if (self = [super init]) {
        self.age = 30;
        self.isFemale = NO;
        self.height = 175;
        self.isBandOnRight = NO;
        self.walkingStepLength = 0;
        self.runningStepLength = 0;
        
        self.steps = 10;
        self.distance = 10;
        self.calori = 10;
        self.noRepeat = NO;
    }
    return self;
}
@end

//
//  NewCell.h
//  WAT
//
//  Created by  NeuroSky on 9/23/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CircleProgress.h"
#import "CustomerView.h"

@protocol cellDelegate
-(void)showHelpView:(NSString *)title;
@end

@interface NewCell : UITableViewCell{

    UILabel *labelLeft;
    UILabel *labelRight;
    UILabel *label3;
    UIImageView *image1;
    UIImageView *image2;
    UIView *uiView;
    
    
    double stepProgress;
    double caloriesProgress;
    
    id target;
    SEL method;

}

@property (retain,nonatomic) UIImageView *image1;
@property (retain,nonatomic) UIImageView *separ;
@property (retain,nonatomic) UILabel *labelLeft;
@property (retain,nonatomic) UILabel *labelRight;
@property (retain,nonatomic) UILabel *version;
@property (retain,nonatomic)  UIView *uiView;
@property (retain,nonatomic)  CircleProgress *circle;
@property (retain,nonatomic)  CircleProgress *circle1;
@property (retain,nonatomic)  CustomerView *customerView1;
@property (retain,nonatomic)  CustomerView *customerView2;
@property (retain,nonatomic)  CustomerView *customerView3;
@property   int  section;
@property (nonatomic, assign) id <cellDelegate> delegate;
@property (nonatomic, assign) id ns_target;
@property (nonatomic, assign) SEL ns_method;
@property (assign,nonatomic) double stepProgress;
@property (assign,nonatomic) double caloriesProgress;
@end

//
//  TGBleEmulator.m
//  WAT
//
//  Created by Mark I. Walsh on 2/22/15.
//  Copyright 2015 Julia. All rights reserved.
//

#import "TGBleEmulator.h"


@implementation TGBleEmulator


//////////////////////////////////////////////////////////////////////////////////////////////
//
#pragma mark - TGBleEmulator

@synthesize status;
@synthesize connected;

@synthesize age;
@synthesize birthYear;
@synthesize birthDay;
@synthesize birthMonth;
@synthesize female;
@synthesize height;
@synthesize weight;
@synthesize walkingStepLength;
@synthesize runningStepLength;
@synthesize displayImperialUnits;
@synthesize displayTime24Hour;
@synthesize bandOnRight;
@synthesize autoSyncEnabled;// NO LONGER USED - to be deleted

@synthesize goalDurationHour;
@synthesize goalDurationMinute;
@synthesize goalDurationSecond;
@synthesize goalSteps; // 0 to 65535, 0 = set to FW default (10,000)
@synthesize alarmHour; // any value outside 0 to 23 disables the Alarm
@synthesize alarmMinute; // any value outside 0 to 59 disables the Alarm
@synthesize alarmRepeat;
// NSStrings...
@synthesize advName;
@synthesize hwModel;
@synthesize hwManufacturer;
@synthesize hwSerialNumber;
@synthesize hwVersion;
@synthesize fwVersion;
@synthesize swVersion;
@synthesize sdkVersion;

@synthesize bondToken;
@synthesize mfgID;
@synthesize connectedID;


@synthesize batteryLevel;

@synthesize delegate;



//  basic initialization routine (this object should never be created by default, but
//  should only be created through the "singleton" initialization method)...
- ( id ) init

{

    //  idiot check - this method should never be called...
    NSAssert( ( 0 == 1 )
             , @"\"init\" base class method for \"singleton\" object should never be called"
             );

    return nil;

}  //  end TGBleEmulator::init


//  initialization...
- ( id ) initSingletonItemOnly

{

    //  initialize/default member variables...
    birthYear = 1976;
    birthMonth = 7;
    birthDay = 4;
    age = 38;
    hwSerialNumber = @"98765432109876543210";
    status = TGBleNotConnected;
    bondToken = NULL;

    connected = NO;

    female = NO;
    height = 5;
    weight = 165;
    walkingStepLength = 2;
    runningStepLength = 3;
    displayImperialUnits = YES;
    displayTime24Hour = YES;
    bandOnRight = YES;
    autoSyncEnabled = NO;// NO LONGER USED - to be deleted

    goalDurationHour = 1;
    goalDurationMinute = 0;
    goalDurationSecond = 0;
    goalSteps = 0; // 0 to 65535, 0 = set to FW default (10,000)
    alarmHour = 0; // any value outside 0 to 23 disables the Alarm
    alarmMinute = 0; // any value outside 0 to 59 disables the Alarm
    alarmRepeat = 0;

    advName = nil;
    hwModel = nil;
    hwManufacturer = nil;
    hwVersion = nil;
    fwVersion = @"1.0.0.1";
    swVersion = @"2.0.0.2";
    sdkVersion = @"3.0.0.3";
    
    mfgID = @"SplashBits";
    connectedID = @"Disconnected";

    batteryLevel = 5;
    
    delegate = nil;

    //  always call the "super" initializer...
    if ( self = [ super init ] )

        {

        }  // end call "super" initializer...

    return self;

}  //  end TGBleEmulator::initSingletonItemOnly


//  additional initialization actions...
- ( void ) onEnter

{

    //  always remember to call the "super" onEnter method...
//    [ super onEnter ];

    return;

}  //  end TGBleEmulator::onEnter


//  clean-up method...
- ( void ) dealloc

{

    //  always remember to call the "super" dealloc method...
//    [ super dealloc ];

    return;

}  //  end TGBleEmulator::dealloc


//  create/access a "singleton" object for this class...
+ ( TGBleEmulator * ) sharedTGBleManager

{

    //  actual pointer to the "singleton" instance of this class...
    static TGBleEmulator * shared = nil;

    //  once-only identifier for instantiating this "singleton" class...
    static dispatch_once_t onceToken = 0l;

    //  allocate this object and call the "init" method, if it hasn't been done already...
    dispatch_once( &onceToken
                  , ^{

                      //  allocate the actual UserData item...
                      shared = [ [ TGBleEmulator alloc ] initSingletonItemOnly ];

                      //  idiot check...
                      NSAssert( ( shared )
                               , @"error allocating object"
                               );
                      
                      //  we need to manually call the "onEnter" method...
                      [ shared onEnter ];
                      
                  }  // end function block
                  );
    
    //  return the "singleton" object...
    return shared;
    
}  // end TGBleEmulator::sharedTGBleManager


- ( void ) setDelegate: ( id ) target
{
    return;
}


- ( int ) getVersion
{
    return 99;
}

- (void)tryBond
{
    return;
}
- (void)takeBond
{
    return;
}
- (void)adoptBond:(NSData*) bondToken serialNumber:(NSString*) sn
{
    return;
}
- (void)releaseBond
{
    return;
}

- (void)tryDisconnect
{
    return;
}

- (void)candidateScan:(NSArray*) nameList
{
    return;
}
- (void)candidateConnect:(NSString*) candidateID
{
    return;
}
- (void)candidateStopScan
{
    return;
}

- (void)sleepInitAnalysis:(NSDate*) startTime endTime:(NSDate *) endTime
{
    return;
}
- (void)sleepInitAnalysis
{
    return;
}
- (void)sleepAddData:(NSDate*) sampleTime sleepPhase:(int) sleepPhase
{
    return;
}
- (void)sleepRequestAnalysis
{
    return;
}
- (void)sleepRequestAnalysis: (NSDate*) startTime endTime:(NSDate *) endTime
{
    return;
}
- (void)sleepSetInterval:(int)minutes completeData:(bool) fullDataSet
{
    return;
}

- (void)trySetSecurity
{
    return;
}
- (void)tryStartRT
{
    return;
}
- (void)tryStopRT
{
    return;
}
- (void)trySyncData
{
    return;
}
- (void)trySyncData:(bool) all
{
    return;
}
- (void)tryEraseData
{
    return;
}

- (int)computeHRVNow
{
    return 72;
}

- (void)setupManager
{
    return;
}
- (void)setupManager:(bool) manualMode
{
    return;
}

- (void)enableLogging
{
    return;
}
- (void)stopLogging
{
    return;
}
- (void)redirectLoggingToFile
{
    return;
}
- (void)restoreLoggingToConsole
{
    return;
}
- (void)diagnosticFilesEnabled
{
    return;
}
- (void)diagnosticFilesDisabled:(bool) option
{
    return;
}

- (void)enableVBM { return; }
- (void)disableVBM { return; }

- (BOOL)mfg_support:(int) arg1 arg2:(int)arg2 { return YES; }

- (void)teardownManager { return; }

- (void)tryFwDown { return; }
- (void)askCurrentCount { return; }

@end

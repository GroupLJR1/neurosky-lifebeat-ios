//
//  TabBarController.m
//  WAT
//
//  Created by Julia on 4/16/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import "TabBarController.h"
#import "lib/TGAccessoryManager.h"
@interface TabBarController ()

@end

@implementation TabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    if([[TGAccessoryManager sharedTGAccessoryManager] accessory] != nil)
        [[TGAccessoryManager sharedTGAccessoryManager] startStream];
}
-(void)viewWillDisappear:(BOOL)animated{
    if([[TGAccessoryManager sharedTGAccessoryManager] connected])
        [[TGAccessoryManager sharedTGAccessoryManager] stopStream];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -
#pragma mark TGAccessoryDelegate protocol methods

- (void)dataReceived:(NSDictionary *)data {
    NSLog(@"dataReceived");
//    NSString * temp = [[NSString alloc] init];
//    NSDate * date = [NSDate date];
//    
//    if([data valueForKey:@"poorSignal"]) {
//        poorSignalValue = [[data valueForKey:@"poorSignal"] intValue];
//        if (poorSignalValue ==200) {
//            [progresstimer setFireDate:[NSDate date]];
//            temp = [temp stringByAppendingFormat:@"%f: Poor Signal: %d\n", [date timeIntervalSince1970], poorSignalValue];
//        }else{
//            [self clearData];
//        }
//    }
//    if (poorSignalValue == 200) {
//        rawValue = [[data valueForKey:@"raw"] shortValue];
//        temp = [temp stringByAppendingFormat:@"%f: Raw: %d\n", [date timeIntervalSince1970], rawValue];
//        if([data valueForKey:@"heartRate"]) {
//            ekgValues.heart_rate = [[data valueForKey:@"heartRate"] intValue];
//            temp = [temp stringByAppendingFormat:@"%f: HeartRate: %d\n", [date timeIntervalSince1970], ekgValues.heart_rate];
//        }
//        if([data valueForKey:@"rrInt"]) {
//            ekgValues.rr_Interval = [[data valueForKey:@"rrInt"] intValue];
//            temp = [temp stringByAppendingFormat:@"%f: RRInt: %d\n", [date timeIntervalSince1970], ekgValues.rr_Interval];
//            if ([rrIntArray count] ==RR_ARRAY_LENGTH) {
//                ekgValues.hrv= [self calcHRV];
//                fitnessValue= [self calculateWithData:rrIntArray];
//                [rrIntArray removeAllObjects];
//            }else if([rrIntArray count] <RR_ARRAY_LENGTH){
//                [rrIntArray addObject:[data valueForKey:@"rrInt"]];
//            }
//        }
//        if ([data valueForKey:@"heartAge"]) {
//            ekgValues.heart_age = [[data valueForKey:@"heartAge"] intValue];
//            temp = [temp stringByAppendingFormat:@"%f: HeartAge: %d\n", [date timeIntervalSince1970], ekgValues.heart_age];
//        }
//        
//        if([data valueForKey:@"respiration"]) {
//            ekgValues.respiration_rate= [[data valueForKey:@"respiration"] floatValue];
//            temp = [temp stringByAppendingFormat:@"%f: Respiration: %f\n", [date timeIntervalSince1970], ekgValues.respiration_rate];
//        }
//        if (!isLogFinished) {
//            if (timeLeft ==1) {
//                isLogFinished = YES;
//                [logFile closeFile];
//                testTimeLeft = MAX_TIME;
//            }else{
//                if (timeLeft == MAX_TIME){ //当计时器从头开始
//                    if (testTimeLeft >1 && testTimeLeft != MAX_TIME) {
//                        [self clearLog];//清除logfile上的所有数据
//                    }
//                }
//                if (testTimeLeft == (timeLeft +1)) {
//                    testTimeLeft = timeLeft;
//                    [output release];
//                    output = [[NSString stringWithString:temp] retain];
//                    [self performSelectorOnMainThread:@selector(writeLog) withObject:nil waitUntilDone:NO];
//                }else if(testTimeLeft == timeLeft){
//                    [output release];
//                    output = [[NSString stringWithString:temp] retain];
//                    [self performSelectorOnMainThread:@selector(writeLog) withObject:nil waitUntilDone:NO];
//                }
//            }
//        }
//    }
//    [ekgView addValue:(double)rawValue];
}

- (void)accessoryDidConnect:(EAAccessory *)accessory {
    // start the data stream to the accessory
    [[TGAccessoryManager sharedTGAccessoryManager] startStream];
    NSLog(@"Connected");
}

- (void)accessoryDidDisconnect {
    // set up the appropriate view
    NSLog(@"disconnected");
}
@end

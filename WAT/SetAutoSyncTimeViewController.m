//
//  SetAutoSyncTimeViewController.m
//  WAT
//
//  Created by neurosky on 10/14/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import "SetAutoSyncTimeViewController.h"
#import "Constant.h"
#import "CommonValues.h"

@interface SetAutoSyncTimeViewController ()

@end

@implementation SetAutoSyncTimeViewController


-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (void)viewDidLoad

{
    __LOG_METHOD_START;
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    timerPicker = [ [ UIDatePicker alloc] initWithFrame:CGRectMake(0.0,100.0,0.0,0.0)];
    timerPicker.minuteInterval = 1;
    [timerPicker addTarget:self action:@selector(timerTimeChanged_:) forControlEvents:UIControlEventValueChanged ];
    [timerPicker setDatePickerMode:UIDatePickerModeCountDownTimer] ;
    [timerPicker setCountDownDuration:1];
    
    NSDate *today =  [NSDate date];
    NSLog(@"today = %@",today);
    NSDateFormatter *df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSString *dateStr =  [df stringFromDate:today];
    NSString  *dateTimeStr =  [NSString stringWithFormat:@"%@ 00:30",dateStr];
    //    NSLog(@"dateTimeStr = %@",dateTimeStr);
    [df setDateFormat:@"yyyy-MM-dd HH:mm"];
    //    NSDate *newDate = [df dateFromString:dateTimeStr];
    //    [ timerPicker setDate:newDate animated:YES];
    minDate = [df dateFromString:dateTimeStr];
    //
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *storedHM = [defaults objectForKey:AUTOSYNC_TIME];
    
    if (storedHM == NULL) {
        ////20141020
        NSString  *tmpTimeStr =  [NSString stringWithFormat:@"%@ 00:30",dateStr];
        newDate = [df dateFromString:tmpTimeStr];
      }
    
    else{
        
        NSString *storedTimerDateStr = [NSString stringWithFormat:@"%@ %@", dateStr, [defaults objectForKey:AUTOSYNC_TIME]];
        NSLog(@"storedTimerDateStr = %@",storedTimerDateStr);
        
        newDate = [df dateFromString:storedTimerDateStr];
        
    }
    [ timerPicker setDate:newDate animated:YES];
    
    //20141020
    //    NSString  *tmpTimeStr =  [NSString stringWithFormat:@"%@ 00:30",dateStr];
    //    timerPicker.minimumDate = [df dateFromString:tmpTimeStr];
    [self.view addSubview:timerPicker];
    __LOG_METHOD_END;
}



-(void)timerTimeChanged_:(id)sender{
    __LOG_METHOD_START;
    UIDatePicker* control = (UIDatePicker*)sender;
    timerSelectedTime = control.date;
    
    //    NSLog(@"timerSelectedTime  = %@",timerSelectedTime);
    /*添加你自己响应代码*/
    NSDateFormatter *df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"HH:mm"];
    [df setTimeZone:[NSTimeZone defaultTimeZone]];
    NSString *fomattedStr =  [df stringFromDate:timerSelectedTime];
    //    NSLog(@"fomattedStr = %@",fomattedStr);
    NSArray *hmArr = [fomattedStr componentsSeparatedByString:@":"];
    if (hmArr.count == 2) {
        int hour = [[hmArr objectAtIndex:0]intValue];
        int min =  [[hmArr objectAtIndex:1]intValue];
        int totalMin = 60*hour + min;
        if (totalMin < 30 ) {
#pragma mark -limit30Mins Tommy
            [ timerPicker setDate:minDate animated:YES];
            timerSelectedTime = NULL;
            NSLog(@"auto scroll to 00:30");
        }
        
    }
    else{
        NSLog(@"hmArr error");
    }
    //selectDateBtn.
    __LOG_METHOD_END;
}
- (void)didReceiveMemoryWarning {
    __LOG_METHOD_START;
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    __LOG_METHOD_END;
}

- (IBAction)cancelBtnClick:(id)sender {
    __LOG_METHOD_START;
    [CommonValues setFlag:6];
    [self dismissViewControllerAnimated:YES completion:nil];
    __LOG_METHOD_END;
}

- (IBAction)saveBtnClick:(id)sender {
    
    __LOG_METHOD_START;
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSString *dateString;
    NSDate *nowDate = [NSDate date];
    NSDate *date1;
    NSString *dateString2;
    if (timerSelectedTime == NULL) {
        // timerSelectedTime = [NSDate date];
        NSString * storedAutosyncTime = [defaults objectForKey :AUTOSYNC_TIME];
        NSLog(@"storedAutosyncTime = %@",storedAutosyncTime);
        
        //        if (storedAutosyncTime == NULL) {
        NSTimeInterval interval = 60*30;
        date1 = [nowDate initWithTimeIntervalSinceNow:+interval];
        dateString2 = [dateFormatter stringFromDate:date1];
        NSLog(@"dateString2 = %@",dateString2);
        dateString = @"00:30";
        //        }
        //        else{
        //            dateString = storedAutosyncTime;
        //
        //        }
    }
    
    else{
        dateString = [dateFormatter stringFromDate:timerSelectedTime];
        NSLog(@"timerSelectedTime = %@",dateString);
        NSArray *arr = [dateString componentsSeparatedByString:@":"];
        // NSLog(@"now date = %@",[dateFormatter stringFromDate :[NSDate date]]);
        if (arr.count == 3) {
            NSTimeInterval interval = [arr[0] integerValue]*60*60+ [arr [1] integerValue]*60 + [arr[2] integerValue];
            date1 = [nowDate initWithTimeIntervalSinceNow:+interval];
            dateString2 = [dateFormatter stringFromDate:date1];
            NSLog(@"dateString2 = %@",dateString2);
        }
        
        NSDateFormatter *dateFormatterWithHm =[[NSDateFormatter alloc] init];
        
        [dateFormatterWithHm setDateFormat:@"HH:mm"];
        dateString = [dateFormatterWithHm stringFromDate:timerSelectedTime];
        
    }
    
    [defaults setBool:YES forKey:AUTOSYNC_SWITCH];
    [defaults setObject:dateString forKey:AUTOSYNC_TIME];
    [defaults synchronize];
    [CommonValues setFlag:5];
    [self dismissViewControllerAnimated:YES completion:nil];
    __LOG_METHOD_END;
}


@end

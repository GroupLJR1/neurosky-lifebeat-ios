//
//  UserProfile.h
//  WAT
//
//  Created by Julia on 7/29/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserProfile : NSObject


@property (assign) int age;
@property (assign) BOOL isFemale;
@property (assign) int height;//height in centimeters
@property (assign) int weight;//weight in kilograms
@property (assign) BOOL isBandOnRight;
@property (assign) int walkingStepLength;//length in centimeters
@property (assign) int runningStepLength;//length in centimeters
@property (retain,nonatomic) NSString *weightUnit;//length in centimeters
@property (assign) int steps;
@property (assign) int distance;
@property (assign) int calori;
@property (retain,nonatomic) NSString *timerString;
@property (assign) BOOL noRepeat;
@end

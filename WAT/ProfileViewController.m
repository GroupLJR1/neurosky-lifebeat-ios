//
//  ProfileViewController.m
//  WAT
//
//  Created by neurosky on 5/23/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import "ProfileViewController.h"
#import "BirtDateViewController.h"
#import "Constant.h"
#import "SVProgressHUD.h"
#import "CommonValues.h"
#import "CommonColors.h"
#import "StringOperation.h"

#import "TGBleEmulator.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController
@synthesize femaleBtn,maleBtn;
@synthesize selectedIndexPath;


- ( void ) dataReceived: ( NSDictionary * ) data { return; }

- ( void ) touchtouchForDismiss { return; }

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    myWholeView.frame = CGRectMake(0, [CommonValues offsetHeight4DiffiOS], 320, 568-20);
    NSLog(@"Profile viewdidload---");
    if ([CommonValues getScreenHeight] == 568) {
        profile_title_label.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
    }
    else
        profile_title_label.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:25.0];
    // Do any additional setup after loading the view from its nib.
    devicesArray  = [[NSMutableArray alloc]init];
    mfgDataArray = [[NSMutableArray alloc]init];
    rssiArray = [[NSMutableArray alloc]init];
    candIDArray = [[NSMutableArray alloc]init];
    devicesIDArray  = [[NSMutableArray alloc]init];
    profile = [[Profile alloc] init];
    
    
    if ([CommonValues getAutoSyncingFlag]) {
        NSLog(@"AutoSyncing, set delegate to Dashboard---");
        [[TGBleManager sharedTGBleManager]setDelegate:[CommonValues getDashboardID]];
    }
    else{
        [[TGBleManager sharedTGBleManager]setDelegate:self];
    }
    
    
    heightText.delegate = self;
    weightText.delegate = self;
    walkingLengthText.delegate = self;
    runningLengthText.delegate = self;
    nameText.delegate = self;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL display_24hr = [defaults boolForKey:DISPLAY_24HR_Time];
    BOOL display_units = [defaults boolForKey:DISPLAY_IMPERIAL_UNITS];
    BOOL storedIsAutoWalkingFlag = [defaults boolForKey:IS_AUTO_WALKING];
    BOOL storedIsAutoRunningFlag = [defaults boolForKey:IS_AUTO_RUNNING];
    NSString *storedBirthDate = [defaults objectForKey:USER_BIRTH_DATE];
    
    
    birthdayFromSDK =  [NSString stringWithFormat:@"%ld/%02d/%02ld",(long)[[TGBleManager sharedTGBleManager] birthYear],[[TGBleManager sharedTGBleManager] birthMonth],(long)[[TGBleManager sharedTGBleManager] birthDay]];
    NSLog(@"birthdayFromSDK = %@",birthdayFromSDK);
    
    // if 保存的不为空
    
    if (storedBirthDate != NULL &&  (![storedBirthDate isEqualToString:@""])) {
        birthDateText.text = storedBirthDate;
    }
    else{
        birthDateText.text = birthdayFromSDK;
    }
    
    NSString* height =  [defaults objectForKey:USER_HEIGHT];
    NSString* weight =  [defaults objectForKey:USER_WEIGHT];
    walkLength =  [defaults objectForKey:USER_WALKING_STEP_LENGH];
    runLength =  [defaults objectForKey:USER_RUNNING_STEP_LENGH];
    is_female = [defaults boolForKey:USER_IS_FEMALE];
    is_band_right = [defaults boolForKey:USER_IS_BAND_RIGHT];
    name = [defaults objectForKey:USER_NAME];
    heightText.text = height;
    weightText.text = weight;
    
    nameText.text = name;
    
    if (storedIsAutoWalkingFlag) {
        
        [self setWalkingRunningLengthAutoSelected:walkingLengthAutoBtn];
        walkingLengthText.text = walkLength;
        
    }
    else
    {
        [self setWalkingRunningLengthManualSelected:walkingLengthManualBtn];
        walkingLengthText.text = walkLength;
        
        
        
    }
    if (storedIsAutoRunningFlag ) {
        
        [self setWalkingRunningLengthAutoSelected:runningLengthAutoBtn];
        runningLengthText.text = runLength;
    }
    else{
        [self setWalkingRunningLengthManualSelected:runningLengthManualBtn];
        runningLengthText.text = runLength;
    }
    
    if (name == NULL || [name isEqualToString:@""]) {
        
        if ([CommonValues getUserName] != NULL && (![[CommonValues getUserName] isEqualToString:@""])) {
            nameText.text = [CommonValues getUserName];
        }
        
    }
    
    
    if (is_female) {
        [self femaleClick:nil];
    }
    else
    {
        [self maleClick:nil];
        
    }
    
    if(!is_band_right){
        NSLog(@" user default 在右手");
        
        
        [self rightClick:nil];
    }
    else{
        NSLog(@"user default  在左手");
        [self leftClick:nil];
    }
    
    if (display_24hr) {
        is24hour = YES;
        [hourBtn setImage:[UIImage imageNamed:@"on.png"] forState:UIControlStateNormal];
        //        [[TGBleManager sharedTGBleManager] setDisplayTime24Hour:is24hour];
        NSLog(@"stored display_24hr");
        
        
    }
    else{
        is24hour = NO;
        [hourBtn setImage:[UIImage imageNamed:@"off.png"] forState:UIControlStateNormal];
        //        [[TGBleManager sharedTGBleManager] setDisplayTime24Hour:is24hour];
        NSLog(@"stored display_12hr");
    }
    
    if (display_units) {
        isImperialUnits = YES;
        [unitsBtn setImage:[UIImage imageNamed:@"on.png"] forState:UIControlStateNormal];
        //        [[TGBleManager sharedTGBleManager] setDisplayImperialUnits:isImperialUnits];
        NSLog(@"stored display_units");
    }
    else{
        isImperialUnits = NO;
        [unitsBtn setImage:[UIImage imageNamed:@"off.png"] forState:UIControlStateNormal];
        //        [[TGBleManager sharedTGBleManager] setDisplayImperialUnits:isImperialUnits];
        NSLog(@"stored standard_units");
    }
    
    NSLog(@"view didload finish");
}

-(void)setWalkingRunningLengthAutoSelected : (UIButton *)btnName {
    UIButton *theOtherBtn;
    if (btnName == walkingLengthAutoBtn) {
        theOtherBtn = walkingLengthManualBtn;
        walkingLengthText.hidden = YES;
        walkingAutoFlag = YES;
        
    }
    else if(btnName == runningLengthAutoBtn){
        theOtherBtn = runningLengthManualBtn;
        runningLengthText.hidden = YES;
        runningAutoFlag = YES;
        
    }
    
    [theOtherBtn setBackgroundImage:[UIImage imageNamed:@"right_2"] forState:UIControlStateNormal];
    [theOtherBtn setTitleColor:[CommonColors getProfileBlueColor] forState:UIControlStateNormal];
    
    [btnName setBackgroundImage:[UIImage imageNamed:@"lift_1"] forState:UIControlStateNormal];
    
    [btnName setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
}

-(void)setWalkingRunningLengthManualSelected: (UIButton *)btnName{
    UIButton *theOtherBtn;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (btnName == walkingLengthManualBtn) {
        theOtherBtn = walkingLengthAutoBtn;
        walkingLengthText.hidden = NO;
        walkingAutoFlag = NO;
        NSString *stroedWalkingStr = [defaults objectForKey:USER_WALKING_STEP_LENGH];
        walkingLengthText.text = stroedWalkingStr;
    }
    else if(btnName == runningLengthManualBtn){
        theOtherBtn = runningLengthAutoBtn;
        runningLengthText.hidden = NO;
        runningAutoFlag = NO;
        NSString *stroedRunningStr = [defaults objectForKey:USER_RUNNING_STEP_LENGH];
        runningLengthText.text = stroedRunningStr;
        
    }
    
    
    [theOtherBtn setBackgroundImage:[UIImage imageNamed:@"lift_2"] forState:UIControlStateNormal];
    
    [theOtherBtn setTitleColor:[CommonColors getProfileBlueColor] forState:UIControlStateNormal];
    
    [btnName setBackgroundImage:[UIImage imageNamed:@"right_1"] forState:UIControlStateNormal];
    
    [btnName setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}


-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"ProfileViewController  viewWillAppear--");
    [ super viewWillAppear: animated ];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *storedBirthDate = [defaults objectForKey:USER_BIRTH_DATE];
    birthdayFromSDK =  [NSString stringWithFormat:@"%ld/%02d/%02ld",(long)[[TGBleManager sharedTGBleManager] birthYear],[[TGBleManager sharedTGBleManager] birthMonth],(long)[[TGBleManager sharedTGBleManager] birthDay]];
    if (storedBirthDate != NULL &&  (![storedBirthDate isEqualToString:@""])) {
        birthDateText.text = storedBirthDate;
    }
    else{
        
        birthDateText.text = birthdayFromSDK;
        
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backClick:(id)sender {
    NSLog(@"profile back button click");
    [CommonValues setUserName:@""];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"" forKey:USER_BIRTH_DATE];
    [defaults synchronize];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    
    
    CGRect frame = textField.frame;
    int offset = frame.origin.y + 32 - (self.view.frame.size.height - 216.0)+[CommonValues offsetHeight4DiffiOS];//键盘高度216
    NSTimeInterval animationDuration = 0.30f;
    [UIView beginAnimations:@"ResizeForKeyBoard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    float frameWidth = self.view.frame.size.width;
    float frameHeight = self.view.frame.size.height;
    if(offset > 0)
    {
        CGRect rect = CGRectMake(0.0f, -offset, frameWidth, frameHeight);
        self.view.frame = rect;
    }
    [UIView commitAnimations];
    
}
- (IBAction)bgTouch:(id)sender {
    NSLog(@"bgTouch------");
    [heightText resignFirstResponder];
    [weightText resignFirstResponder];
    [walkingLengthText resignFirstResponder];
    [runningLengthText resignFirstResponder];
    [nameText resignFirstResponder];
    
    
    // When the user presses return, take focus away from the text field so that the keyboard is dismissed.
    NSTimeInterval animationDuration = 0.30f;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    CGRect rect = CGRectMake(0.0f, 0.0f+20-[CommonValues offsetHeight4DiffiOS], self.view.frame.size.width, self.view.frame.size.height);
    self.view.frame = rect;
    [UIView commitAnimations];
    
}

- (IBAction)femaleClick:(id)sender {
    
    [maleBtn setBackgroundImage:[UIImage imageNamed:@"lift_2"] forState:UIControlStateNormal];
    
    [maleBtn setTitleColor:[CommonColors getProfileBlueColor] forState:UIControlStateNormal];
    
    [femaleBtn setBackgroundImage:[UIImage imageNamed:@"right_1"] forState:UIControlStateNormal];
    
    [femaleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //    [defaults setBool:YES forKey:USER_IS_FEMALE];
    //    [defaults synchronize];
    is_female = YES;
}

- (IBAction)maleClick:(id)sender {
    
    [femaleBtn setBackgroundImage:[UIImage imageNamed:@"right_2"] forState:UIControlStateNormal];
    
    [femaleBtn setTitleColor:[CommonColors getProfileBlueColor] forState:UIControlStateNormal];
    
    [maleBtn setBackgroundImage:[UIImage imageNamed:@"lift_1"] forState:UIControlStateNormal];
    
    [maleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    is_female = NO;
    //     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //    [defaults setBool:NO forKey:USER_IS_FEMALE];
    //    [defaults synchronize];
}

- (IBAction)rightClick:(id)sender {
    [rightBtn setBackgroundImage:[UIImage imageNamed:@"right_1"] forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"lift_2"] forState:UIControlStateNormal];
    [leftBtn setTitleColor:[CommonColors getProfileBlueColor] forState:UIControlStateNormal];
    
    //    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //    [defaults setBool:YES forKey:USER_IS_BAND_RIGHT];
    //    [defaults synchronize];
    is_band_right = NO;
}

- (IBAction)leftClick:(id)sender {
    
    [leftBtn setBackgroundImage:[UIImage imageNamed:@"lift_1"] forState:UIControlStateNormal];
    [leftBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [rightBtn setBackgroundImage:[UIImage imageNamed:@"right_2"] forState:UIControlStateNormal];
    [rightBtn setTitleColor:[CommonColors getProfileBlueColor] forState:UIControlStateNormal];
    is_band_right = YES;
    
    //    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //    [defaults setBool:NO forKey:USER_IS_BAND_RIGHT];
    //    [defaults synchronize];
    
}

- (IBAction)hour24Click:(id)sender {
    NSLog(@"hour24Click");
    //      NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (is24hour) {
        is24hour = NO;
        [hourBtn setImage:[UIImage imageNamed:@"off.png"] forState:UIControlStateNormal];
        //        [defaults setBool:NO forKey:DISPLAY_24HR_Time];
        NSLog(@"click 24hour to set 12");
    }
    else{
        is24hour = YES;
        [hourBtn setImage:[UIImage imageNamed:@"on@2x.png"] forState:UIControlStateNormal];
        //        [defaults setBool:YES forKey:DISPLAY_24HR_Time];
        NSLog(@"click 24hour to set 24");
    }
    //     [defaults synchronize];
}

- (IBAction)unitsClick:(id)sender {
    NSLog(@"unitsClick");
    //      NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (isImperialUnits) {
        isImperialUnits = NO;
        [unitsBtn setImage:[UIImage imageNamed:@"off.png"] forState:UIControlStateNormal];
        //        [defaults setBool:NO forKey:DISPLAY_IMPERIAL_UNITS];
    }
    else{
        isImperialUnits = YES;
        [unitsBtn setImage:[UIImage imageNamed:@"on@2x.png"] forState:UIControlStateNormal];
        //        [defaults setBool:YES forKey:DISPLAY_IMPERIAL_UNITS];
    }
    //     [defaults synchronize];
}

- (IBAction)birthdayBtnClick:(id)sender {
    NSLog(@"birthdayBtnClick---");
    [CommonValues setUserName:[nameText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    
    
    
    BirtDateViewController *bdvc = [[BirtDateViewController alloc]init];
    [self presentViewController:bdvc animated:YES completion:nil];
}

- (IBAction)walkingLengthAutoClick:(id)sender {
    NSLog(@"walkingLengthAutoClick----");
    [self setWalkingRunningLengthAutoSelected:walkingLengthAutoBtn];
    [self setWalkingRunningLengthAutoSelected:runningLengthAutoBtn];
    
}

- (IBAction)walkingLengthManualClick:(id)sender {
    NSLog(@"walkingLengthManualClick----");
    [self setWalkingRunningLengthManualSelected:walkingLengthManualBtn];
    [self setWalkingRunningLengthManualSelected:runningLengthManualBtn];
    
}

- (IBAction)runningLengthAutoClick:(id)sender {
    NSLog(@"runningLengthAutoClick----");
    [self setWalkingRunningLengthAutoSelected:runningLengthAutoBtn];
    [self setWalkingRunningLengthAutoSelected:walkingLengthAutoBtn];
    
    
}

- (IBAction)runningLengthManualClick:(id)sender {
    NSLog(@"runningLengthManualClick----");
    [self setWalkingRunningLengthManualSelected:runningLengthManualBtn];
    [self setWalkingRunningLengthManualSelected:walkingLengthManualBtn];
    
}

- (IBAction)saveBtnClick:(id)sender {
    NSLog(@"profile saveBtnClick");
    if ([CommonValues getAutoSyncingFlag]) {
        NSLog(@"auto syncing, main operation need wait  ");
        [[CommonValues autoSyncingAlert] show];
        return;
    }
    
    
    if (![CommonValues getBluetoothOnFlag]) {
        NSLog(@"BT OFF, can't go on candidateConnect:candId");
        [self showBTNotOnAlert];
        return;
    }
    
    //test input Valid check;
    if (![self inputValidCheck]) {
        return;
    };
    
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    
    
    if ([self profileValueChangedCheck]) {
        [CommonValues setSaveBtnClcikFlag:YES];
        //profile Value Changed  (不含userName)
        //ready to connect
        NSLog(@"profile Value Changed  (不含userName)");
        
        lostBeforeMainOperation = YES;
        
        [CommonValues setHasTaskFlag:YES];
        //设回delegate，  For auto Sync
        [[TGBleManager sharedTGBleManager]setDelegate:self];
        
        if ([TGBleManager sharedTGBleManager].status == TGBleConnectedToBond) {
            //已有连接,直接做主要操作
            NSLog(@"已有连接,直接做主要操作");
            [self.view setUserInteractionEnabled:NO];
            [self setUserPro2band];
            [SVProgressHUD showWithStatus:@" Setting Profile"];
            
            
            
        }
        else{
            
            NSLog(@"没有连接");
            NSData *token = [[TGBleManager sharedTGBleManager]bondToken];
            if (token != NULL) {
                NSLog(@"bond_state");
                
                
                
                [self.view setUserInteractionEnabled:NO];
                
                // connect
                
                NSString *candId = [defaults objectForKey:@"DeviceID"];
                NSLog(@"call candidateConnect:candId-----");
                [[TGBleManager sharedTGBleManager] candidateConnect:candId];
                
                [SVProgressHUD showWithStatus:@" Connecting"];
            }
            
            else {
                NSLog(@"not_bond_state");
                // show devices list
                [self showDevicesList];
            }
            
            timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
            
        }
    }
    
    else{
        
        NSLog(@"profile (不含userName) Value not Changed  ");
        NSString *displayedName =[StringOperation trim: nameText.text];
        if ([displayedName isEqualToString:[defaults objectForKey:USER_NAME]]) {
            
            NSLog(@"user name not changed,其他的也没变");
            if ([CommonValues iOSVersion] >= IOS8) {
                alertController = [UIAlertController alertControllerWithTitle:nil message:PROFILENOTHINGCHANGED preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:okAction];
                [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
            }
            else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:PROFILENOTHINGCHANGED delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            }
        }
        else{
            NSLog(@"user email changed，其他的没变");
            [defaults setObject:displayedName forKey:USER_NAME];
            [defaults synchronize];
            [SVProgressHUD showSuccessWithStatus:@"Set user name completed."];
            
        }
        
    }
    
    
    
    
}


-(BOOL)inputValidCheck{
    NSLog(@"inputValidCheck start---");
    NSString *heightStr = [heightText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    double height =  [heightStr doubleValue];
    
    NSString *weightStr = [weightText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    double weight =  [weightStr doubleValue];
    
    NSString *wlStr = [walkingLengthText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    int wl =  [wlStr intValue];
    
    NSString *rlStr = [runningLengthText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    int rl =  [rlStr intValue];
    
    NSString *emailStr = [nameText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString *message = @"";
    
    if (![emailStr isEqualToString:  @""]) {
        NSLog(@"email 不为空");
        
        //暂时取消email 格式验证
        //        if ([CommonValues emailValidation:emailStr]) {
        //
        //        }
        //        else{
        //            message = @"Name should be an email address  ";
        //            [CommonValues showAlert:message];
        //            return NO;
        //        }
        
    }
    
    
    
    if ([StringOperation inputValidation3:heightStr]) {
        NSLog(@"height是 非负整数");
        if (height < 25 || height > 250) {
            NSLog(@"height not between 25  and 250");
            message = @"height should be between 25  and 250 ";
            [CommonValues showAlert:message];
            return NO;
        }
        
    }
    else
    {
        message = @"height should be between 25  and 250 ";
        [CommonValues showAlert:message];
        return NO;
    }
    
    if ([StringOperation inputValidation3:weightStr] ||[StringOperation inputValidation2:weightStr] ) {
        NSLog(@"weightStr 是非负整数 或 一位小数");
        if (weight < 10.0 || weight > 1300.0) {
            NSLog(@"weight not between 10.0  and 1300.0");
            
            message = @"weight  should be between 10.0  and 1300.0 ";
            [CommonValues showAlert:message];
            return NO;
            
        }
    }
    else{
        message = @"weight  should be between 10.0  and 1300.0 ";
        [CommonValues showAlert:message];
        return NO;
        
    }
    
    if ([StringOperation inputValidation3:wlStr]) {
        NSLog(@"wlStr是 非负整数 = %@",wlStr);
        if (wl < 0 || wl > 255) {
            NSLog(@"walking length not Valid");
            message = @"walking length  should be between 0  and 255 ";
            
            [CommonValues showAlert:message];
            return NO;
            
        }
        
        
    }
    else{
        message = @"walking length  should be between 0  and 255 ";
        [CommonValues showAlert:message];
        return NO;
    }
    
    NSLog(@"rlStr = %@",rlStr);
    if ([StringOperation inputValidation3:rlStr]) {
        NSLog(@"rlStr是 非负整数 ");
        if (rl < 0 || rl > 255) {
            NSLog(@"running length not Valid");
            message = @"running length  should be between 0  and 255 ";
            [CommonValues showAlert:message];
            return NO;
        }
        
        
    }
    else{
        message = @"running length  should be between 0  and 255 ";
        [CommonValues showAlert:message];
        return NO;
    }
    
    NSLog(@"heightStr = %@ ,weightStr = %@ ,wlStr = %@ ,rlStr = %@ ,height = %f ,weight = %f ,wl = %d ,rl = %d ",heightStr,weightStr,wlStr,rlStr,height,weight,wl,rl);
    
    
    NSLog(@"inputValidCheck end---");
    return YES;
    
}

-(BOOL) profileValueChangedCheck{
    NSLog(@"valueChangedCheck start ---");
    birthdayFromSDK =  [NSString stringWithFormat:@"%ld/%02d/%02ld",(long)[[TGBleManager sharedTGBleManager] birthYear],[[TGBleManager sharedTGBleManager] birthMonth],(long)[[TGBleManager sharedTGBleManager] birthDay]];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *walkingLengthStr;
    NSString *runningLengthStr;
    //    if (walkingAutoFlag) {
    //        walkingLengthStr = @"0";
    //    }
    //    else{
    walkingLengthStr = walkingLengthText.text;
    //    }
    
    //    if (runningAutoFlag) {
    //        runningLengthStr = @"0";
    //    }
    //    else{
    runningLengthStr = runningLengthText.text;
    //    }
    
    
    if ([defaults boolForKey:IS_AUTO_WALKING]) {
        NSLog(@"stored IS_AUTO_WALKING YES");
    }
    else{
        NSLog(@"stored IS_AUTO_WALKING NO");
    }
    
    if ([defaults boolForKey:IS_AUTO_RUNNING]) {
        NSLog(@"stored IS_AUTO_RUNNING YES");
    }
    else{
        NSLog(@"stored IS_AUTO_RUNNING NO");
    }
    
    if (walkingAutoFlag) {
        NSLog(@"current  value  in Ram  walkingAutoFlag YES");
    }
    else{
        NSLog(@"current  value  in Ram  walkingAutoFlag NO");
    }
    if (runningAutoFlag) {
        NSLog(@"current  value  in Ram  runningAutoFlag YES");
    }
    else{
        NSLog(@"current  value  in Ram  runningAutoFlag NO");
    }
    
    if ([defaults boolForKey:DISPLAY_IMPERIAL_UNITS]) {
        NSLog(@"stored IMPERIAL_UNITS");
    }
    else{
        NSLog(@"stored STANDARD_UNITS");
    }
    
    if ([defaults boolForKey:DISPLAY_24HR_Time]) {
        NSLog(@"stored 24HR_Time");
    }
    else{
        NSLog(@"stored 12HR_Time");
    }
    if (isImperialUnits) {
        NSLog(@"current is ImperialUnits");
    }
    else{
        NSLog(@"current is Standard Units");
    }
    if(is24hour){
        NSLog(@"current is 24 hour");
    }
    else{
        NSLog(@"current is 12 hour");
    }
    
    
    
    
    //profile values
    if ([birthDateText.text isEqualToString: birthdayFromSDK]
        &&(is_female&[defaults boolForKey:USER_IS_FEMALE]||(!is_female)&(![defaults boolForKey:USER_IS_FEMALE]))
        &&[heightText.text isEqualToString:  [defaults objectForKey:USER_HEIGHT]]
        && [weightText.text isEqualToString:  [defaults objectForKey:USER_WEIGHT]]
        &&[walkingLengthStr isEqualToString:  [defaults objectForKey:USER_WALKING_STEP_LENGH]]
        &&[runningLengthStr isEqualToString:  [defaults objectForKey:USER_RUNNING_STEP_LENGH]]
        &&(walkingAutoFlag & [defaults boolForKey:IS_AUTO_WALKING] || (!walkingAutoFlag) & (![defaults boolForKey:IS_AUTO_WALKING]))
        &&(runningAutoFlag & [defaults boolForKey:IS_AUTO_RUNNING]||(!runningAutoFlag) & (![defaults boolForKey:IS_AUTO_RUNNING]))
        &&(is_band_right&[defaults boolForKey:USER_IS_BAND_RIGHT]||(!is_band_right)&(![defaults boolForKey:USER_IS_BAND_RIGHT]))
        &&(is24hour&[defaults boolForKey:DISPLAY_24HR_Time]||(!is24hour)&(![defaults boolForKey:DISPLAY_24HR_Time]))
        &&(isImperialUnits&[defaults boolForKey:DISPLAY_IMPERIAL_UNITS]||(!isImperialUnits)&(![defaults boolForKey:DISPLAY_IMPERIAL_UNITS]))) {
        
        NSLog(@"birthDateText.text = %@,heightText.text = %@,weightText.text = %@,walkingLengthText.text = %@,runningLengthText = %@",birthDateText.text,heightText.text,weightText.text,walkingLengthStr,runningLengthStr);
        NSLog(@"birthDateText.defaults = %@,heightText.defaults = %@,weightText.defaults = %@,walkingLengthText.defaults = %@,walkingLengthText.defaults = %@",birthdayFromSDK,[defaults objectForKey:USER_HEIGHT],[defaults objectForKey:USER_WEIGHT],[defaults objectForKey:USER_WALKING_STEP_LENGH],[defaults objectForKey:USER_RUNNING_STEP_LENGH]);
        
        NSLog(@"Profile Nothing Changed,There's No Need To Set To Band");
        
        return NO;
    }
    
    else{
        NSLog(@"birthDateText.text = %@,heightText.text = %@,weightText.text = %@,walkingLengthText.text = %@,walkingLengthText.text = %@",birthDateText.text,heightText.text,weightText.text,walkingLengthStr,runningLengthStr);
        NSLog(@"birthDateText.defaults = %@,heightText.defaults = %@,weightText.defaults = %@,walkingLengthText.defaults = %@,runningLengthText.defaults = %@",birthdayFromSDK,[defaults objectForKey:USER_HEIGHT],[defaults objectForKey:USER_WEIGHT],[defaults objectForKey:USER_WALKING_STEP_LENGH],[defaults objectForKey:USER_RUNNING_STEP_LENGH]);
        
        NSLog(@"Profile  Changed, need  set to band");
        return YES;
        
        
    }
    
    NSLog(@"valueChangedCheck end ---");
    
}


- (void)checkBTStatusDone{
    NSLog(@"set bt done click ");
    if ([CommonValues getBluetoothOnFlag]) {
        //turn on the BT
        NSLog(@"already have  turn on the BT");
    }
    else{
        NSLog(@" BT  still not on state");
        [self showBTNotOnAlert];
    }
}

- (void)hasTokenTimeOutAlertRetry{
    //Retry
    NSLog(@"hasToken time out retry");
    [self saveBtnClick:nil];
}
- (void)hasTokenTimeOutAlertCancel{
    NSLog(@"hasTokenTime OutAlert Cancel");
}

- (void)noTokenTimeOutAlertCancel{
    NSLog(@"time out  cancel...");
}
- (void)noTokenTimeOutAlertRetry{
    //Retry
    [self saveBtnClick:nil];
}

- (void)selectDeviceAlertCancel{
    NSLog(@"selectDeviceAlertTag cancelButton");
    //                [[TGBleManager sharedTGBleManager] candidateStopScan];
    [self stopScan];
}
- (void)selectDeviceAlertOK{
    NSLog(@"selectDeviceAlertTag OK Button");
    [self.view setUserInteractionEnabled:NO];
    //                [SVProgressHUD showWithStatus:@"Setting  profile..."];
    [self stopScan];
    //                NSLog(@"call  candidateStopScan------");
    //                [[TGBleManager sharedTGBleManager] candidateStopScan];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSLog(@"need to save deviceName = %@",readyToSaveDeviceName);
    //if ([defaults stringForKey:@"DeviceName"] == nil) {
    [defaults setObject: readyToSaveDeviceName forKey:@"DeviceName"];
    [defaults setObject: readyToSaveDeviceID forKey:@"DeviceID"];
    [defaults synchronize];
    
    //candidate connect to the band;
    NSLog(@"call candidateConnect -----");
    [[TGBleManager sharedTGBleManager] candidateConnect:readyToSaveDeviceID];
    timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
    [SVProgressHUD showWithStatus:@" Connecting"];
}
- (void)resetDeviceTempList{
    [devicesArray removeAllObjects];
    [mfgDataArray removeAllObjects];
    [rssiArray removeAllObjects];
    [candIDArray removeAllObjects];
    [devicesIDArray removeAllObjects];
    [listView dismiss];
    [self.view setUserInteractionEnabled:YES];
}

- (void)bondAlertCancel{
    NSLog(@"bondAlert cancelButton");
    NSLog(@"call tryDisconnect------");
    [[TGBleManager sharedTGBleManager] tryDisconnect];
    [SVProgressHUD dismiss];
    [self.view setUserInteractionEnabled:YES];
}
- (void)bondAlertOK{
    NSLog(@"bondAlert OK Button");
    NSLog(@"call takeBond------");
    [[TGBleManager sharedTGBleManager] takeBond];
    [SVProgressHUD showWithStatus:@" Bonding"];
}

- (void)noTokenBondFailedCancel{
    [SVProgressHUD dismiss];
    NSLog(@"noToken  bondFailedTag cancelButton");
    [self.view setUserInteractionEnabled:YES];
}
- (void)noTokenBondFailedRetry{
    [SVProgressHUD dismiss];
    NSLog(@"noToken bondFailedTag Retry");
    //connect previous band
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"DeviceName"];
    NSString *candId = [defaults objectForKey:@"DeviceID"];
    NSLog(@"stored candId = %@",candId);
    NSLog(@"call candidateConnect---");
    [[TGBleManager sharedTGBleManager]candidateConnect:candId];
    timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
    
    [SVProgressHUD showWithStatus:@" Connecting"];
}
- (void)noTokenBondFailedTryAnother{
    [SVProgressHUD dismiss];
    NSLog(@"noToken bondFailedTag Try another band");
    [self saveBtnClick:nil];
}

- (void)hasTokenBondFailedCancel{
    [SVProgressHUD dismiss];
    NSLog(@"hasToken BondFailedTag cancel");
    [self.view setUserInteractionEnabled:YES];
}
- (void)hasTokenBondFailedRetry{
    [SVProgressHUD dismiss];
    NSLog(@"hasToken BondFailedTag Retry");
    //connect previous band
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"DeviceName"];
    NSString *candId = [defaults objectForKey:@"DeviceID"];
    NSLog(@"stored candId = %@",candId);
    NSLog(@"call candidateConnect---");
    [[TGBleManager sharedTGBleManager]candidateConnect:candId];
    timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
    [SVProgressHUD showWithStatus:@" Connecting"];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    NSLog(@"buttonIndex:%ld", (long)buttonIndex);
    switch (alertView.tag) {
        case checkBTStatusTag:
            [self checkBTStatusDone];
            break;
            
            
        case   hasTokenTimeOutAlertTag:
            if (buttonIndex == 0) {
                [self hasTokenTimeOutAlertRetry];
            }
            
            else {
                [self hasTokenTimeOutAlertCancel];
            }
            break;
            
            
        case   noTokenTimeOutAlertTag:
            
            if (buttonIndex == 0) {
                [self noTokenTimeOutAlertCancel];
            }
            
            else if (buttonIndex == 1)
            {
                [self noTokenTimeOutAlertRetry];
            }
            break;
        case selectDeviceAlertTag:
        {
            if (buttonIndex == 0) {
                [self selectDeviceAlertCancel];
            }
            
            else if(buttonIndex == 1){
                [self selectDeviceAlertOK];
            }
            [self resetDeviceTempList];
            
        }
            break;
            
        case bondAlertTag:
        {
            if (buttonIndex == 0) {
                [self bondAlertCancel];
            }
            
            else if(buttonIndex == 1){
                [self bondAlertOK];
            }
        }
            break;
        case noTokenBondFailedTag:
        {
            if (buttonIndex == 0) {
                [self noTokenBondFailedCancel];
            }
            
            else if(buttonIndex == 1){
                [self noTokenBondFailedRetry];
            }
            else if(buttonIndex == 2){
                [self noTokenBondFailedTryAnother];
            }
            
        }
            break;
            
        case hasTokenBondFailedTag:
        {
            if (buttonIndex == 1) {
                [self hasTokenBondFailedRetry];
                
            }
            
            else{
                [self hasTokenBondFailedCancel];
                
            }
            
            
        }
            
            
            break;
            
            
        default:
            break;
    }
    
}



- (NSUInteger)supportedInterfaceOrientations

{
    return UIInterfaceOrientationMaskPortrait;
    
}

-(BOOL)shouldAutorotate{
    
    return NO;
}
-(void)showDevicesList{
    NSLog(@" select device click");
    
    if (![CommonValues getBluetoothOnFlag]) {
        NSLog(@"BT OFF, can't go on candidateScaning");
        [self showBTNotOnAlert];
        return;
    }
    [SVProgressHUD showWithStatus:@" Search bands "];
    [self.view setUserInteractionEnabled:NO];
    
    NSLog(@" call  candidateScan:nameList");
    [[TGBleManager sharedTGBleManager] candidateScan:[CommonValues bandDeviceArray]];
    
    
    NSLog(@"Menu item  devicesArray count = %lu",(unsigned long)devicesArray.count);
    
}
- (void) candidateFound:(NSString *)devName rssi:(NSNumber *)rssi mfgID:(NSString *)mfgID candidateID:(NSString *)candidateID
{
    NSString* message = [NSString stringWithFormat:
                         @"Device Name: %@\nMfg Data: %@\nID: %@\nRSSI value: %@",
                         devName,
                         mfgID,
                         candidateID,
                         rssi
                         ];
    NSLog(@"SELECT DEVICE candidateFound delegate: %@", message);
    
    
    if ([candIDArray containsObject:candidateID]) {
        return;
    }
    if (!firstEnterCandiDel) {
        NSLog(@"first enter candidate Found");
        firstEnterCandiDel = YES;
        [timeOutTimer setFireDate:[NSDate distantFuture]];
        [timeOutTimer invalidate];
        [SVProgressHUD dismiss];
        
        listView = [[ZSYPopoverListView alloc] initWithFrame:CGRectMake(0, 0, 300, 400)];
        
        listView.tagert =self;
        listView.stopEBLScan =@selector(stopScan);
        
        listView.datasource = self;
        listView.delegate = self;
        showListTimer =  [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(delay2ShowList) userInfo:nil repeats:NO];
        
        //        [listView show];
        
    }
    
    
    [devicesArray addObject:devName];
    [mfgDataArray addObject:mfgID];
    [rssiArray addObject:rssi];
    [candIDArray addObject:candidateID];
    [devicesIDArray addObject:candidateID];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [listView reload];
        
        
    });}

#pragma marks
#pragma tableview datasource methods
#pragma mark -
- (NSInteger)popoverListView:(ZSYPopoverListView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return devicesArray.count;
}

- (UITableViewCell *)popoverListView:(ZSYPopoverListView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString * savedDeviceName = [defaults stringForKey:@"DeviceName"];
    NSLog(@"NSUserDefaults deviceName = %@",savedDeviceName);
    
    static NSString *identifier = @"identifier";
    UITableViewCell *cell = [tableView dequeueReusablePopoverCellWithIdentifier:identifier];
    if (nil == cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] ;
    }
    
    
    NSString *tmpStr = [NSString stringWithFormat:@"%@:%@",[ mfgDataArray objectAtIndex:indexPath.row],[ devicesArray objectAtIndex:indexPath.row]];
    //cell.textLabel.text =[ devicesArray objectAtIndex:indexPath.row];
    cell.textLabel.text = tmpStr;
    cell.textLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:25.0];
    
    if ([[ devicesArray objectAtIndex:indexPath.row] isEqualToString:savedDeviceName]) {
        // many devices have the same name
        //
        //cell.imageView.image = [UIImage imageNamed:@"fs_main_login_selected.png"];
    }
    
    return cell;
}

- (void)popoverListView:(ZSYPopoverListView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*UITableViewCell *cell =*/ [tableView popoverCellForRowAtIndexPath:indexPath];
    //cell.imageView.image = [UIImage imageNamed:@"fs_main_login_normal.png"];
    NSLog(@"deselect:%ld", (long)indexPath.row);
    
    
    
}

- (void)popoverListView:(ZSYPopoverListView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedIndexPath = indexPath;
    /*UITableViewCell *cell =*/ [tableView popoverCellForRowAtIndexPath:indexPath];
    //cell.imageView.image = [UIImage imageNamed:@"fs_main_login_selected.png"];
    NSLog(@"select:%ld", (long)indexPath.row);
    //  NSString *deviceName = [NSString stringWithFormat:@"Wat%d", indexPath.row];
    
    NSString *deviceName = [devicesArray objectAtIndex:indexPath.row];
    NSString *mfgData = [mfgDataArray objectAtIndex:indexPath.row];
    
    //NSString *rssi = [rssiArray objectAtIndex:indexPath.row];
    
    readyToSaveDeviceName = [devicesArray objectAtIndex:indexPath.row];
    readyToSaveDeviceID = [devicesIDArray objectAtIndex:indexPath.row];
    // [self.tagert performSelector:stopEBLScan withObject:nil];
    
    //     [devicesArray removeAllObjects];
    //     [listView dismiss];
    [listView dismiss];
    NSString *msg = [NSString stringWithFormat:@"Are you sure you would like to connect to %@ %@  ?",deviceName, mfgData];
    if ([CommonValues iOSVersion] >= IOS8) {
        alertController = [UIAlertController alertControllerWithTitle:msg message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:CANCEL style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
            [self selectDeviceAlertCancel];
            
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:OK style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self selectDeviceAlertOK];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
    }
    
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:msg message:nil delegate:self cancelButtonTitle:CANCEL otherButtonTitles:OK, nil];
        alert.tag   = selectDeviceAlertTag;
        [alert show];
    }

}



-(void)stopScan{
    NSLog(@"touch listview other place to stopScan ");
    [self.view setUserInteractionEnabled:YES];
    NSLog(@"call candidateStopScan----");
    
    [[TGBleManager sharedTGBleManager]candidateStopScan];
    [devicesArray removeAllObjects];
    [mfgDataArray removeAllObjects];
    [rssiArray removeAllObjects];
    [candIDArray removeAllObjects];
    [devicesIDArray removeAllObjects];
    
    //    selectDeviceBtnClickFlag = NO;
    
    // cancel delayed time out alert.
    if ([timeOutTimer isValid]) {
        [timeOutTimer invalidate];
        NSLog(@"timeOutTimer invalidate----");
        
    }
    
    firstEnterCandiDel = NO;
    
}

- (void)potentialBond:(NSString*) code sn: (NSString*) sn devName:(NSString*) devName
{
    
    
    NSString* displayMessage = [NSString stringWithFormat:
                                @"%@%@?",DigitCodeConfirmMsg,
     
                                code];
    dispatch_async( dispatch_get_main_queue(), ^ {
    [SVProgressHUD dismiss];
    });
     dispatch_async( dispatch_get_main_queue(), ^ {
    if ([CommonValues iOSVersion] >= IOS8) {
        alertController = [UIAlertController alertControllerWithTitle:@"" message:displayMessage preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
            [self bondAlertCancel];
            
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self bondAlertOK];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
    }
    else{
       
            UIAlertView *bondDialog = [[UIAlertView alloc]initWithTitle:@"" message:displayMessage delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
            bondDialog.tag = bondAlertTag;
            [bondDialog show];
            
       
    }
 });
    
}
//BLE delegate methods
- (void) bleDidBond:(TGbondResult)result {
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    // set bond flag,check it when do every action
    
    NSString * message;
    NSString * title;
    
    if (result == TGbondResultTokenAccepted) {
        message = @"Bond Succesful";
    }
    else if (result == TGbondResultTokenReleased) {
        title = TokenReleasedTitle;
        message = TokenReleasedMsg;
    }
    else if (result == TGbondResultErrorBondedNoMatch) {
        title = CommunicationErrorTitle;
        message = BondedNoMatchMsg;
    }
    else if (result == TGbondResultErrorBadTokenFormat) {
        title = BondRejectedTitle;
        message = BadTokenFormatMsg;
    }
    else if (result == TGbondResultErrorTimeOut) {
        title = TimeOutTitle;
        message = TimeOutMsg;
    }
    
    else if (result == TGbondResultErrorNoConnection) {
        title = NoConnectionTitle;
        message = NoConnectionMsg;
    }
    
    else if (result == TGbondResultErrorReadTimeOut) {
        title = ReadBackTimeOutTitle;
        message = ReadTimeOutMsg;
    }
    
    
    else if (result == TGbondResultErrorReadBackTimeOut) {
        title = ReadBackTimeOutTitle;
        message = ReadBackTimeOutMsg;
    }
    
    else if (result == TGbondResultErrorWriteFail) {
        title = WriteFailTitle;
        message = WriteFailMsg;
    }
    
    else if (result == TGbondResultErrorTargetIsAlreadyBonded) {
        title = BondRejectedTitle;
        message = TargetIsAlreadyBondedMsg;
    }
    
    
    else if (result == TGbondAppErrorNoPotentialBondDelegate) {
        title  = BondAppErrorTitle;
        message = NoPotentialBondDelegateMsg;
    }
    
    
    else if (result == TGbondResultErrorTargetHasWrongSN) {
        title  = CommunicationErrorTitle;
        message = TargetHasWrongSNMSg;
    }
    else if (result == TGbondResultErrorPairingRejected) {
        title  = PairingErrorTitle;
        message = PairingRejectedMsg;
    }
    
    
    else if (result == TGbondResultErrorSecurityMismatch) {
        title  = PairingErrorTitle;
        message = SecurityMismatchMsg;
    }
    
    
    else {
        title  = UnkonwnBondErrorTitle;
        message = [NSString stringWithFormat:@"Bonding Error, code: %d", (int) result];
    }
    NSLog(@"BONDIING RESULT Code  %lu",(unsigned long)result);
    NSLog(@"BONDIING RESULT msg  %@",message);
    
    if (result == TGbondResultTokenAccepted) {
        successBondedFlag  = YES;
        
        [ud setBool:YES forKey:@"bond_state"];
        [ud synchronize];
        
        
        NSLog(@"Bond Succesful,start to set userProfile");
        [self setUserPro2band];
        
        dispatch_async( dispatch_get_main_queue(), ^ {
            [SVProgressHUD showWithStatus:@" Setting Profile"];
        });
    }
    
    else if (result == TGbondResultTokenReleased){
        NSLog(@"此处不应该被执行！！！");
    }
    else{
        lostBeforeMainOperation = NO;
        successBondedFlag  = NO;
        [ud setBool:NO forKey:@"bond_state"];
        [ud synchronize];
        NSLog(@"call tryDisconnect----");
        [[TGBleManager sharedTGBleManager]tryDisconnect];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            
        });
        
        NSData *token = [[TGBleManager sharedTGBleManager] bondToken];
        if (token == NULL) {
            if ([CommonValues iOSVersion] >= IOS8) {
                alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:CANCEL style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                    [self noTokenBondFailedCancel];
                    
                }];
                UIAlertAction *retryAction = [UIAlertAction actionWithTitle:RETRY style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self noTokenBondFailedRetry];
                }];
                UIAlertAction *tryAnotherAction = [UIAlertAction actionWithTitle:TRYANOTHERBAND style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self noTokenBondFailedTryAnother];
                }];
                [alertController addAction:cancelAction];
                [alertController addAction:retryAction];
                [alertController addAction:tryAnotherAction];
                [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
            }
            else{
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:CANCEL otherButtonTitles:RETRY,TRYANOTHERBAND, nil];
                alert.tag = noTokenBondFailedTag;
                dispatch_async( dispatch_get_main_queue(), ^ {
                    [alert show];
                });
            }
        }
        
        else{
            if ([CommonValues iOSVersion] >= IOS8) {
                alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:CANCEL style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                    [self hasTokenBondFailedCancel];
                    
                }];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:RETRY style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self hasTokenBondFailedRetry];
                }];
                [alertController addAction:cancelAction];
                [alertController addAction:okAction];
                [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
            }
            else{
                UIAlertView *alert_hasToken = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:CANCEL otherButtonTitles:RETRY, nil];
                
                alert_hasToken.tag = hasTokenBondFailedTag;
                dispatch_async( dispatch_get_main_queue(), ^ {
                    [alert_hasToken show];
                });
                
            }
        }
    }
    
}

- (void) bleDidConnect {
    
    NSLog(@"delegate bleDidConnect----");
    if (timeOutTimer .isValid) {
        [timeOutTimer invalidate];
    }
    NSData *sdkToken =  [[TGBleManager sharedTGBleManager] bondToken];
    
    if (sdkToken == NULL) {
        NSLog(@"call tryBond-----");
        [[TGBleManager sharedTGBleManager] tryBond];
        
    }
    
    else{
        
        NSString *sn =  [[TGBleManager sharedTGBleManager] hwSerialNumber];
        NSLog(@"sdkToken = %@, sn = %@",sdkToken,sn);
        
        NSLog(@"call adoptBond: serialNumber ");
        [[TGBleManager sharedTGBleManager] adoptBond:sdkToken serialNumber:sn];
        
    }
    
    
    lostBeforeMainOperation = NO;
    
    dispatch_async( dispatch_get_main_queue(), ^ {
        [SVProgressHUD showWithStatus:@" Bonding"];
    });
    
    
}


- (void) bleDidDisconnect {
    NSLog(@"delegate bleDidDisconnect----");
    [self.view setUserInteractionEnabled:YES];
    //  [SVProgressHUD showErrorWithStatus:@"save failed"];
}
- (void) bleLostConnect {
    
    NSLog(@"delegate bleLostConnect---------");
    //    [self.view setUserInteractionEnabled:YES];
    if ([timeOutTimer isValid]) {
        [timeOutTimer invalidate];
        NSLog(@"timeOutTimer invalidate----");
        
    }
    
    
    if (lostBeforeMainOperation ) {
        
        NSLog(@"want to Set Profile , lost connection before Main operation");
        NSData *token = [[TGBleManager sharedTGBleManager] bondToken];
        if (token == NULL) {
            if ([CommonValues iOSVersion] >= IOS8) {
                alertController = [UIAlertController alertControllerWithTitle:LostConnectTitle message:LostConnectMsg preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                    [self noTokenBondFailedCancel];
                    
                }];
                UIAlertAction *retryAction = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self noTokenBondFailedRetry];
                }];
                UIAlertAction *tryAnotherAction = [UIAlertAction actionWithTitle:@"Try another band" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self noTokenBondFailedTryAnother];
                }];
                [alertController addAction:cancelAction];
                [alertController addAction:retryAction];
                [alertController addAction:tryAnotherAction];
                [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
            }
            else{
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:LostConnectTitle message:LostConnectMsg
                                                              delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry",@"Try another band", nil];
                alert.tag = noTokenBondFailedTag;
                dispatch_async( dispatch_get_main_queue(), ^ {
                    [alert show];
                });
            }
        }
        
        else{
            if ([CommonValues iOSVersion] >= IOS8) {
                alertController = [UIAlertController alertControllerWithTitle:LostConnectTitle message:LostConnectMsg preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                    [self hasTokenBondFailedCancel];
                    
                }];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self hasTokenBondFailedRetry];
                }];
                [alertController addAction:cancelAction];
                [alertController addAction:okAction];
                [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
            }
            else{
                UIAlertView *alert_hasToken = [[UIAlertView alloc]initWithTitle:LostConnectTitle message:LostConnectMsg delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry",nil];
                
                alert_hasToken.tag = hasTokenBondFailedTag;
                dispatch_async( dispatch_get_main_queue(), ^ {
                    //                [self.view setUserInteractionEnabled:YES];
                    //                [SVProgressHUD dismiss];
                    [alert_hasToken show];
                });
            }
            
        }
        
        
        
    }
    else{
        dispatch_async( dispatch_get_main_queue(), ^ {
            
            [SVProgressHUD dismiss];
            [self.view setUserInteractionEnabled: YES];
        });
    }
    
}

- (void) bleDidAbortConnect {
    NSLog(@"delegate bleDidAbortConnect----");
    [self.view setUserInteractionEnabled:YES];
    
}
- (void) bleDidSendUserConfig:(TGsendResult)result {
    NSString * message;
    
    [CommonValues setHasTaskFlag:NO];
    // delegate 设回 Dashboard ;
    [[TGBleManager sharedTGBleManager]setDelegate:[CommonValues getDashboardID]];
    
    if (result == TGSendSuccess) {
        message = @"USER CFG Succesful";
        //        [self saveUserSettingsTo];
        
        if ([CommonValues isBackgroundFlag]) {
            [self saveUserSettingsTo];
            NSLog(@"Set Profile 完成，且在后台，应断开连接");
            
            NSLog(@"call tryDisconnect ------");
            [[TGBleManager sharedTGBleManager] tryDisconnect];
        }
        
        if ([CommonValues getSaveBtnClcikFlag]) {
            [CommonValues setSaveBtnClcikFlag:NO];
            
            [self saveUserSettingsTo];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.view setUserInteractionEnabled:YES];
                [SVProgressHUD showSuccessWithStatus:@"Set Profile completed."];
                
            });
        }
        
        
        
        
        
    }
    else if (result == TGSendFailedNoAcknowledgement) {
        message = @"USER CFG time out, no ack";
    }
    else if (result == TGSendFailedDisconnected) {
        message = @"USER CFG disconnected";
    }
    else {
        message = [NSString stringWithFormat:@"USER CFG Unexpected Error, code: %d", (int) result];
    }
    NSLog(@"USER CFG Result Code  %lu",(unsigned long)result);
    NSLog(@"USER CFG Result msg  %@",message);
    
    
    if (result != TGSendSuccess) {
        lostBeforeMainOperation = NO;
        [profile setBackPreviousInfo2SDK];
        if ([CommonValues iOSVersion] >= IOS8) {
            alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Set Profile failed." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                [self hasTokenBondFailedCancel];
                
            }];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self hasTokenBondFailedRetry];
            }];
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
        }
        else{
        
        UIAlertView *alert_hasToken = [[UIAlertView alloc]initWithTitle:@"Error " message:@"Set Profile failed." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
        
        alert_hasToken.tag = hasTokenBondFailedTag;
        
        dispatch_async(dispatch_get_main_queue(), ^{ [alert_hasToken show]; });
        }// do all alerts on the main thread
        
    }
   
}
-(void)dismissView{
    sleep(3);
    [self.view setUserInteractionEnabled:YES];
    [[TGBleManager sharedTGBleManager] tryDisconnect];
    [SVProgressHUD showSuccessWithStatus:@"Set Profile completed."];
    [SVProgressHUD dismiss];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}
- (void) bleDidSendAlarmGoals:(TGsendResult)result {
    NSString * message;
    
    if (result == TGSendSuccess) {
        message = @"ALARM CFG Succesful";
    }
    else if (result == TGSendFailedNoAcknowledgement) {
        message = @"ALARM CFG time out, no ack";
    }
    else if (result == TGSendFailedDisconnected) {
        message = @"ALARM CFG disconnected";
    }
    else {
        message = [NSString stringWithFormat:@"ALARM CFG Unexpected Error, code: %d", (int) result];
    }
    NSLog(@"ALARM CFG Result Code  %lu",(unsigned long)result);
    NSLog(@"ALARM CFG Result msg  %@",message);
    
 
}

-(void) batteryLevel:(int)level {
    NSLog(@">>>>>>>-----New Battery Level: percent complete: %d", level);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:level  forKey:LATEST_BATTERY];
    [defaults synchronize];
}
- (void) exceptionMessage:(TGBleExceptionEvent)eventType {
    NSString * message;
    
    if (eventType == TGBleHistoryCorruptErased) {
        message = @"Flash Memory Corrupted, Will Be erased, suspect battery has been exhausted";
    }
    else if (eventType == TGBleConfigurationModeCanNotBeChanged) {
        message = @"Exception Message - Ble Connection Mode CAN NOT be changed";
    }
    else if (eventType == TGBleUserAgeHasBecomeNegative_BirthDateIsNOTcorrect) {
        message = @"User Age has become negative, correct the birth date";
    }
    else if (eventType == TGBleUserAgeIsTooLarge_BirthDateIsNOTcorrect) {
        message = @"User Age is TOO OLD, check the birth date";
    }
    else if (eventType == TGBleUserBirthDateRejected_AgeOutOfRange) {
        message = @"User Age is out of RANGE, corret the birth date";
    }
    else if (eventType == TGBleFailedOtherOperationInProgress) {
        message = @"Exception Message - Another Operation is Already in Progress";
    }
    else if (eventType == TGBleFailedSecurityNotInplace) {
        message = @"Exception Message - Security is NOT In Place";
    }
    else if (eventType == TGBleTestingEvent) {
        message = @"Exception Message - This is only a Test";
    }
    else if (eventType == TGBleReInitializedNeedAlarmAndGoalCheck) {
        message = @"Exception Message - Band lost power or was MFG reset, check your alarm and goal settings";
    }
    else if (eventType == TGBleStepGoalRejected_OutOfRange) {
        message = @"Exception Message - Step Goal Settings rejected -- OUT OF RANGE";
    }
    else if (eventType == TGBleCurrentCountRequestTimedOut) {
        message = @"Exception Message - Can Not get Current Count values\nBand not responding";
    }
    else if (eventType == TGBleConnectFailedSuspectKeyMismatch) {
        message = @"Exception Message - Band appears to be paired, possible encryption key mismatch, hold side button for 10 seconds to reboot";
    }
    else if (eventType == TGBleNonRepeatingAlarmMayBeStaleCheckAlarmConfiguration) {
        message = @"Exception Message - A Non-repeating Alarm may be stale, check your alarm settings";
    }
    else {
        message = [NSString stringWithFormat:@"Exception Detected, code: %lu", (unsigned long)eventType];
    }
    
    //    NSLog(@"message = %@",message);
    
    NSLog(@"exceptionMessage =  %@",message);

}


-(void)setUserPro2band{
    
    NSLog(@"setUserPro2band----");
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *displayedBirth = [StringOperation trim: birthDateText.text] ;
    NSString * displayedHeight =  [StringOperation trim:heightText.text] ;
    NSString * displayedWeight=  [StringOperation trim: weightText.text] ;
    NSString * displayedWalking;
    NSString * displayedRunning;
    if (walkingAutoFlag) {
        displayedWalking = @"0";
    }
    else{
        displayedWalking = [StringOperation trim: walkingLengthText.text] ;
    }
    if (runningAutoFlag) {
        displayedRunning = @"0";
    }
    else{
        displayedRunning  =  [StringOperation trim:runningLengthText.text] ;
    }
    
    int year = 0;
    int month = 0;
    int day = 0;
    
    NSArray *array = [displayedBirth componentsSeparatedByString:@"/"];
    if (array.count == 3) {
        year  =  [ array[0] intValue];
        month =  [ array[1] intValue];
        day =  [ array[2] intValue];
        //  NSLog(@"year = %d,month = %d, day =%d",year,month,day);
    }
    
    
    //set user profile
    [[TGBleManager sharedTGBleManager] setFemale:is_female];
    
    [[TGBleManager sharedTGBleManager] setBirthYear:year];
    [[TGBleManager sharedTGBleManager] setBirthMonth:month];
    [[TGBleManager sharedTGBleManager] setBirthDay:day];
    
    
    [[TGBleManager sharedTGBleManager] setHeight:[displayedHeight intValue]]; // in cm
    [[TGBleManager sharedTGBleManager] setWeight:[displayedWeight floatValue]*10]; // in 1/10 of kg
    
    [[TGBleManager sharedTGBleManager] setWalkingStepLength:[displayedWalking intValue]]; // in cm
    [[TGBleManager sharedTGBleManager] setRunningStepLength:[displayedRunning intValue]]; // in cm
    [[TGBleManager sharedTGBleManager] setBandOnRight:is_band_right]; // yes =  left wrist
    
    [[TGBleManager sharedTGBleManager] setDisplayTime24Hour:is24hour];
    [[TGBleManager sharedTGBleManager] setDisplayImperialUnits:isImperialUnits];
    
    NSInteger const step =  [defaults integerForKey:USER_GOAL_STEP];
    [[TGBleManager sharedTGBleManager] setGoalSteps:(int)step];
    
    NSLog(@"year = %d ,month = %d ,day = %d ,height = %@ ,weight = %@ ,walkLength = %@ ,runLength = %@",year,month,day,heightText,weightText,walkLength,runLength);
    if (is24hour) {
        NSLog(@"is24hr");
    }
    else{
        NSLog(@"12 hr");
    }
    
    if (isImperialUnits) {
        NSLog(@"isImpial");
    }
    else{
        NSLog(@"not isImpial");
    }
    if (is_female) {
        NSLog(@"female");
        
    }
    else{
        
        
        NSLog(@"male");
    }
    
    
    if (is_band_right) {
        NSLog(@"left");
        
        
        if ([[TGBleManager sharedTGBleManager] isBandOnRight]) {
            NSLog(@"SDK  band在左手");
        }
        else{
            NSLog(@"SDK  band在右手");
        }
        
    }
    else{
        NSLog(@"right" );
        
        if ([[TGBleManager sharedTGBleManager] isBandOnRight]) {
            NSLog(@"SDK  band在左手");
        }
        else{
            NSLog(@"SDK  band在右手");
        }
        
    }
    
    
    
    
}
-(void)saveUserSettingsTo{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *displayedBirth = [StringOperation trim: birthDateText.text] ;
    NSString * displayedHeight =  [StringOperation trim:heightText.text] ;
    NSString * displayedWeight=  [StringOperation trim: weightText.text] ;
    NSString * displayedWalking;
    NSString * displayedRunning;
    if (walkingAutoFlag) {
        [defaults setBool:YES forKey:IS_AUTO_WALKING];
    }
    else{
        displayedWalking = [StringOperation trim: walkingLengthText.text] ;
        [defaults setObject:displayedWalking forKey:USER_WALKING_STEP_LENGH];
        [defaults setBool:NO forKey:IS_AUTO_WALKING];
    }
    if (runningAutoFlag) {
        [defaults setBool:YES forKey:IS_AUTO_RUNNING];
    }
    else{
        displayedRunning  =  [StringOperation trim:runningLengthText.text] ;
        [defaults setObject:displayedRunning forKey:USER_RUNNING_STEP_LENGH];
        [defaults setBool:NO forKey:IS_AUTO_RUNNING];
    }
    NSString *displayedName = [StringOperation trim:nameText.text];
    
    NSLog(@"displayedBirth === %@ ,",displayedBirth);
    
    [defaults setObject:displayedName forKey:USER_NAME];
    [defaults setObject:displayedBirth forKey:USER_BIRTH_DATE];
    [defaults setObject:displayedHeight forKey:USER_HEIGHT];
    [defaults setObject:displayedWeight forKey:USER_WEIGHT];
    [defaults setBool: is_band_right forKey:USER_IS_BAND_RIGHT];
    [defaults setBool: is24hour forKey:DISPLAY_24HR_Time];
    [defaults setBool: isImperialUnits forKey:DISPLAY_IMPERIAL_UNITS];
    [defaults setBool: is_female forKey:USER_IS_FEMALE];
    [defaults synchronize];
    [profile writeToTemp];
}


-(void)showTimeOutAlert{
    [SVProgressHUD dismiss];
    
    [self stopScan];
    // delay to show alert ,prevent the confligs from SVProgressHUD dismiss
    [self performSelector:@selector(delay2ShowTimeOutAlert) withObject:nil afterDelay:0.5];
    
}
-(void)delay2ShowTimeOutAlert{
    if ([CommonValues iOSVersion] >= IOS8) {
        UIAlertAction *cancelAction;
        UIAlertAction *okAction;
        if ([[TGBleManager sharedTGBleManager]bondToken] != NULL ) {
            alertController = [UIAlertController alertControllerWithTitle:ConnectTimeOutTitle message:ConnectTimeOutMsg preferredStyle:UIAlertControllerStyleAlert];
            cancelAction = [UIAlertAction actionWithTitle:CANCEL style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                [self hasTokenTimeOutAlertCancel];
                
            }];
            okAction = [UIAlertAction actionWithTitle:RETRY style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self hasTokenTimeOutAlertRetry];
            }];
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
        }
        else{
            alertController = [UIAlertController alertControllerWithTitle:ScanTimeOutTitle message:ScanTimeOutMsg preferredStyle:UIAlertControllerStyleAlert];
            cancelAction = [UIAlertAction actionWithTitle:CANCEL style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                [self noTokenTimeOutAlertCancel];
                
            }];
            okAction = [UIAlertAction actionWithTitle:RETRY style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self noTokenTimeOutAlertRetry];
            }];
            
        }
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
        
        
    }
    else{
        UIAlertView *timeOutAlert;
        if ([[TGBleManager sharedTGBleManager]bondToken] != NULL ) {
            timeOutAlert =  [[UIAlertView alloc]initWithTitle:ConnectTimeOutTitle message:ConnectTimeOutMsg delegate:self cancelButtonTitle:nil otherButtonTitles:RETRY, CANCEL, nil];
            timeOutAlert.tag = hasTokenTimeOutAlertTag;
            
            
        }
        else{
            timeOutAlert = [[UIAlertView alloc]initWithTitle:ScanTimeOutTitle message:ScanTimeOutMsg delegate:self cancelButtonTitle:CANCEL otherButtonTitles:RETRY, nil];
            timeOutAlert.tag = noTokenTimeOutAlertTag;
        }
        
        [timeOutAlert show];
    }
    NSLog(@"call tryDisconnect----");
    [[TGBleManager sharedTGBleManager] tryDisconnect];
}
-(void)delay2ShowList{
    NSLog(@"showTimeOutAlerttest---");
    [listView show];
    
}


-(void)showBTNotOnAlert {
    if ([CommonValues iOSVersion] >= IOS8) {
        alertController = [UIAlertController alertControllerWithTitle:BluetoothIsOffTitle message:BluetoothIsOffMsg preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self checkBTStatusDone];
        }];
        [alertController addAction:okAction];
        [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
    }
    else{
        UIAlertView *alert =[ [UIAlertView alloc]initWithTitle:BluetoothIsOffTitle message:BluetoothIsOffMsg delegate:self cancelButtonTitle:nil otherButtonTitles:@"Done", nil];
        alert.tag = checkBTStatusTag;
        [alert show];
    }
    
}

- (void)showAlertController{
    [self presentViewController:alertController animated:YES completion:nil];
    
}
@end

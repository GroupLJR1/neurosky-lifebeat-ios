//
//  UIViewController_Extensions.m
//  WAT
//
//  Created by Mark I. Walsh on 3/11/15.
//  Copyright (c) 2015 Mark I. Walsh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
#import "CommonValues.h"


@implementation UIViewController ( Extensions )


- ( void ) checkBlueToothStatusDone: ( BOOL ) promptIfNotEnabled

{

    __LOG_METHOD_START;

    if ( [ CommonValues getBluetoothOnFlag ] )
        {
        //turn on the BT
        NSLog( @"already have turn on the BT" );
        }
    else
        {
        NSLog( @"BT still not on state" );
        if ( promptIfNotEnabled )
            {
            [ self showBlueToothNotOnAlert ];
            }
        }

    __LOG_METHOD_END;

    return;

}  //  end UIViewController::checkBlueToothStatusDone


- ( void ) showBlueToothNotOnAlert

{

    __LOG_METHOD_START;

    if ( [ CommonValues iOSVersion ] >= IOS8 )
        {
        UIAlertController * const alertController = [ UIAlertController alertControllerWithTitle: BluetoothIsOffTitle
                                                                                         message: BluetoothIsOffMsg
                                                                                  preferredStyle: UIAlertControllerStyleAlert
                                                    ];
        UIAlertAction * const okAction = [ UIAlertAction actionWithTitle: @"Done"
                                                                   style: UIAlertActionStyleDefault
                                                                 handler: ^( UIAlertAction * action )
                                          {
                                          [ self checkBlueToothStatusDone: NO ];
                                          }
                                         ];
        [ alertController addAction: okAction ];
        [ self presentViewController: alertController
                            animated: YES
                          completion: nil
        ];
        }
    else
        {
        UIAlertView * const alert = [ [ UIAlertView alloc ] initWithTitle: BluetoothIsOffTitle
                                                                  message: BluetoothIsOffMsg
                                                                 delegate: self
                                                        cancelButtonTitle: nil
                                                        otherButtonTitles: @"Done", nil
                                    ];
        alert.tag = checkBTStatusTag;
        [ alert show ];
        }

    __LOG_METHOD_END;

    return;

}  //  end UIViewController::showBlueToothNotOnAlert


@end  //  end @implementation UIViewController ( Extensions )

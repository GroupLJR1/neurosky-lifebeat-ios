//
//  SettingsViewController.m
//  WAT
//
//  Created by neurosky on 1/9/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import "SettingsViewController.h"
#import "Constant.h"
#import "TGBleManager.h"
#import "SVProgressHUD.h"
#import "ProfileViewController.h"
#import "AlarmViewController.h"
#import "TimerViewController.h"
#import "CommonValues.h"
#import "SetGoalViewController.h"
#import "SetAutoSyncTimeViewController.h"
#import "setSleepTimeView.h"

#import "TGBleEmulator.h"

@interface SettingsViewController ()

@property (strong,nonatomic) setSleepTimeView *setSleepTimeView;

@end

@implementation SettingsViewController


@synthesize selectedIndexPath;
//@synthesize cell;

- ( void ) dataReceived: ( NSDictionary * ) data { return; }

- ( void ) touchtouchForDismiss { return; }

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    __LOG_METHOD_START;
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    __LOG_METHOD_END;
    return self;
}

-(void)dateChanged:(id)sender{
    UIDatePicker* control = (UIDatePicker*)sender;
    selectedDate = control.date;
    
    NSLog(@"date  = %@",selectedDate);
    /*添加你自己响应代码*/
}

- (void)viewDidLoad
{
    __LOG_METHOD_START;
    CGRect rect_screen = [[UIScreen mainScreen]bounds];
    CGSize size_screen = rect_screen.size;
    screenHeight = size_screen.height;
    NSLog(@"screenHeight = %f",screenHeight);
    NSLog(@"setting view did load");
    [super viewDidLoad];
    myWholeView.frame = CGRectMake(0, [CommonValues offsetHeight4DiffiOS], 320, 568-20);
    devicesArray  = [[NSMutableArray alloc]init];
    mfgDataArray = [[NSMutableArray alloc]init];
    rssiArray = [[NSMutableArray alloc]init];
    candIDArray = [[NSMutableArray alloc]init];
    
    devicesIDArray  = [[NSMutableArray alloc]init];
    lostBeforeMainOperation = YES;
    if (screenHeight == 568) {
        settingTitle.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:25.0];
    }
    
    else{
        settingTitle.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:20.0];
    }
    repeatDayArray = [[NSMutableArray alloc]init];
    
    repeatArray = [[NSArray alloc]initWithObjects:@"Once",@"Everyday",@"Monday",@"Tuesday", @"Wednesday",@"Thursday", @"Fridy",@"Saturday", @"Sunday", nil];
    
    
    //====
    // Do any additional setup after loading the view from its nib.
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    age =  [defaults objectForKey:USER_AGE];
    height =  [defaults objectForKey:USER_HEIGHT];
    weight =  [defaults objectForKey:USER_WEIGHT];
    walkLength =  [defaults objectForKey:USER_WALKING_STEP_LENGH];
    runLength =  [defaults objectForKey:USER_RUNNING_STEP_LENGH];
    is_female = [defaults boolForKey:USER_IS_FEMALE];
    is_band_right = [defaults boolForKey:USER_IS_BAND_RIGHT];
    
    alarmStr = [defaults objectForKey:USER_ALARM];
    // NSLog(@"didload alarmStr = %@",alarmStr);
    timerStr = [defaults objectForKey:USER_TIMER_ALARM];
    alarmMode = [defaults objectForKey:USER_ALARM_MODE];
    
//    BOOL display_24hr = [defaults boolForKey:DISPLAY_24HR_Time];
//    BOOL display_units = [defaults boolForKey:DISPLAY_IMPERIAL_UNITS];
    BOOL storedAlarmSwitch = [defaults boolForKey:ALARM_SWITCH];
    BOOL storedTimerSwitch = [defaults boolForKey:TIMER_SWITCH];
    NSDate *timer_deri_time = [defaults objectForKey :TIMER_DERICTORY_TIME];
    NSLog(@"stored timer_deri_time = %@",timer_deri_time);
    NSDate *alarm_deri_time = [defaults objectForKey :ALARM_DERICTORY_TIME];
    NSLog(@"stored alarm_deri_time = %@",alarm_deri_time);
    
    int timerInterval =   [timer_deri_time timeIntervalSince1970];
    int nowDateInterval =  [[NSDate date] timeIntervalSince1970];
    int alarmInterval = [alarm_deri_time timeIntervalSince1970];
    NSLog(@"timerInterval = %d nowDateInterval = %d alarmInterval = %d",timerInterval ,nowDateInterval,alarmInterval);
    
    NSLog(@"alarmMode = %@",alarmMode);
    
    
    NSLog(@"1111");
    [alarmMode containsObject:@"Once"];
    NSLog(@"222");
    
    if ((storedAlarmSwitch&&(![alarmMode containsObject:@"Once"])) || (alarmInterval> nowDateInterval)) {
        //        alarmSwitch.on = YES;
    }
    
    else{
        //        alarmSwitch.on = NO;
    }
    
    
    
    if (storedTimerSwitch && (timerInterval>nowDateInterval)) {
        //        timerSwitch.on = YES;
    }
    else{
        //        timerSwitch.on = NO;
    }
    if (alarmMode == NULL) {
        [repeatDayArray addObject:@"Test"];
        
        [repeatDayArray addObject:@"Once"];
    }
    else if ([alarmMode isKindOfClass:[NSMutableArray class ]]){
        for (int i = 0; i<alarmMode.count; i++) {
            [repeatDayArray addObject:[alarmMode objectAtIndex:i]];
            
        }
        
    }
    
    NSLog(@"did load repeatDayArray = %@",repeatDayArray);
    
    if (alarmStr != NULL) {
        alarmText.text = alarmStr;
    }
    if (timerStr != NULL) {
        timerText.text = timerStr;
    }
    BOOL storedBeepFlag =  [defaults boolForKey:BEEP_SOUND];
    if (storedBeepFlag) {
        [soundSwtichBtn  setImage:[UIImage imageNamed:@"on.png"] forState:UIControlStateNormal];
        
    }
    else{
        [soundSwtichBtn  setImage:[UIImage imageNamed:@"off.png"] forState:UIControlStateNormal];
    }
    
    viewTag = 0;
    NSLog(@"setting view did load finish");
    
#pragma mark - peter sleep time set
    NSString *startSleepTime = [defaults objectForKey:AUTO_SLEEP_START_TIME];
    NSString *endSleepTime = [defaults objectForKey:AUTO_SLEEP_END_TIME];
    _sleepStartTimeLabel.text=startSleepTime;
    _sleepStopTimeLabel.text=endSleepTime;
    __LOG_METHOD_END;
}

-(void)viewWillAppear:(BOOL)animated{
    __LOG_METHOD_START;
    NSLog(@"setting viewWillAppear---getFlag =%d",[CommonValues getFlag]);

    [ super viewWillAppear: animated ];
    
    if ([CommonValues getAutoSyncingFlag]) {
        [ [TGBleManager sharedTGBleManager] setDelegate:[CommonValues getDashboardID]];
    }
    else{
        
        [ [TGBleManager sharedTGBleManager] setDelegate:self];
        
    }
    NSLog(@"self = %@",self);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    

    
    BOOL storedAlarmSwitch = [defaults boolForKey:ALARM_SWITCH];
    BOOL storedTimerSwitch = [defaults boolForKey:TIMER_SWITCH];
    NSDate *timer_deri_time = [defaults objectForKey :TIMER_DERICTORY_TIME];
    NSLog(@"stored timer_deri_time = %@",timer_deri_time);
    NSDate *alarm_deri_time = [defaults objectForKey :ALARM_DERICTORY_TIME];
    NSLog(@"stored alarm_deri_time = %@",alarm_deri_time);
    int timerInterval =   [timer_deri_time timeIntervalSince1970];
    int nowDateInterval =  [[NSDate date] timeIntervalSince1970];
    int alarmInterval = [alarm_deri_time timeIntervalSince1970];
    NSLog(@"timerInterval = %d nowDateInterval = %d alarmInterval = %d",timerInterval ,nowDateInterval,alarmInterval);
    alarmMode = [defaults objectForKey:USER_ALARM_MODE];
    
    NSLog(@"alarmMode = %@",alarmMode);
    alarmStr = [defaults objectForKey:USER_ALARM];
    // NSLog(@"didload alarmStr = %@",alarmStr);
    timerStr = [defaults objectForKey:USER_TIMER_ALARM];
    
    autoSyncStr = [defaults objectForKey:AUTOSYNC_TIME];
    
    alarmText.text = alarmStr;
    timerText.text = timerStr;
    autoSyncText.text = autoSyncStr;
    BOOL storedAutoSyncSwitch = [defaults boolForKey:AUTOSYNC_SWITCH];
    if (storedAutoSyncSwitch) {
        [autoSyncSwitchBtn setImage:[UIImage imageNamed:@"on.png"] forState:UIControlStateNormal];
        
        
        if ([CommonValues getFlag] == 5) {
            
            
            //            if ([CommonValues getAutoSyncTimer].valid) {
            //                [[CommonValues getAutoSyncTimer] invalidate];
            //                NSLog(@"timer 失效");
            //            }
            
            NSInteger seconds = [CommonValues secondsFromString:autoSyncStr];
            NSLog(@"AUTOSYNC_TIME stored seconds == %ld", (long)seconds);
            
            //            [CommonValues setAutoSyncTimer:[NSTimer scheduledTimerWithTimeInterval:seconds target:[CommonValues getDashboardID] selector:@selector(doSyncStuff) userInfo:nil repeats:YES]]  ;
            
            if (FALSE) {
                NSLog(@"background auto sync task ,not use now 暂时不启用");
            }
            else{
                //注释掉 为了测试T-T后台
                [[UIApplication sharedApplication] clearKeepAliveTimeout];
                NSLog(@"clearKeepAliveTimeout -------remove last background task ");
                [[UIApplication sharedApplication] setKeepAliveTimeout:seconds handler:^{
                    [[CommonValues getDashboardID] performSelector:@selector(doSyncStuff)];
                    
                }];
                NSLog(@" background auto sync task  start 开始");
            }
            
        }
    }
    else{
        [autoSyncSwitchBtn setImage:[UIImage imageNamed:@"off.png"] forState:UIControlStateNormal];
        
    }
    
    
    
    if ([CommonValues getFlag] == 0) {
        if ((storedAlarmSwitch && (![alarmMode containsObject:@"Once"]))) {
            
            [alarmBtn setImage:[UIImage imageNamed:@"on.png"] forState:UIControlStateNormal];
            
        }
        else if(storedAlarmSwitch && (alarmInterval>nowDateInterval)){
            
            [alarmBtn setImage:[UIImage imageNamed:@"on.png"] forState:UIControlStateNormal];
        }
        
        else{
            
            [alarmBtn setImage:[UIImage imageNamed:@"off.png"] forState:UIControlStateNormal];
            [defaults setBool:NO forKey:ALARM_SWITCH];
            [[TGBleManager sharedTGBleManager] setAlarmHour:25];
        }
        
        
        
        if (storedTimerSwitch && (timerInterval>nowDateInterval)) {
            //        timerSwitch.on = YES;
            [timerBtn setImage:[UIImage imageNamed:@"on.png"] forState:UIControlStateNormal];
            //            [defaults setBool:YES forKey:TIMER_SWITCH];
        }
        else{
            //        timerSwitch.on = NO;
            [timerBtn setImage:[UIImage imageNamed:@"off.png"] forState:UIControlStateNormal];
            [defaults setBool:NO forKey:TIMER_SWITCH];
        }
        
    }
    
    //  repeatDayArray = [defaults objectForKey:USER_ALARM_MODE];
    
    if (alarmMode == NULL) {
        [repeatDayArray addObject:@"Test"];
        
        [repeatDayArray addObject:@"Once"];
    }
    else if ([alarmMode isKindOfClass:[NSMutableArray class ]]){
        for (int i = 0; i<alarmMode.count; i++) {
            [repeatDayArray addObject:[alarmMode objectAtIndex:i]];
            
        }
        
    }
    
    //goal
    step =  [defaults integerForKey:USER_GOAL_STEP];
    distance =  [defaults integerForKey:USER_GOAL_DISTANCE];
    
    calorie =  [defaults integerForKey:USER_GOAL_CALORI];
    
    sleep_ = [defaults floatForKey:USER_GOAL_SLEEP];
    
    // Set Alarm/timer when back to setting..
    NSString *candId = [defaults objectForKey:@"DeviceID"];
    
    NSData *token = [[TGBleManager sharedTGBleManager] bondToken];
    
    
    
    if ((token == NULL) && ([CommonValues getFlag] == 1)) {
        [CommonValues setHasTaskFlag:YES];
        
        [[TGBleManager sharedTGBleManager] setDelegate:self];
        
        wantSetAlarm = YES;
        isSettingAlarm = YES;
        isSettingGoal = NO;
        isSettingGoal = NO;
        wantSetGoal = NO;
        wantSetTimer = NO;
        [self showDevicesList];
    }
    else{
        if ([CommonValues getFlag] == 1) {
            if (![CommonValues getBluetoothOnFlag]) {
                NSLog(@"BT OFF, can't go on candidateConnect");
                [self showBTNotOnAlert];
                return;
            }
            
            
            [self.view setUserInteractionEnabled:NO];
            isSettingAlarm = YES;
            wantSetAlarm = YES;
            isSettingGoal = NO;
            wantSetGoal = NO;
            isSettingTimer = NO;
            wantSetTimer = NO;
            [CommonValues setHasTaskFlag:YES];
            [[TGBleManager sharedTGBleManager] setDelegate:self];
            
            wantDisableAlarm = NO;
            
            if ([TGBleManager sharedTGBleManager].status == TGBleConnectedToBond) {
                //已有连接,直接做主要操作
                NSLog(@"已有连接,直接做主要操作");
                if (isSettingAlarm){
                    if (wantDisableAlarm) {
                        [[TGBleManager sharedTGBleManager] setAlarmHour:25];
                    }
                    else{
                        [self setAlarmAccording2Stored];
                    }
                    
                    [SVProgressHUD showWithStatus:@" Setting Alarm"];
                    
                }
                
                else if (isSettingTimer){
                    if (wantDisableTimer) {
                        [[TGBleManager sharedTGBleManager] setGoalDurationHour:25];
                        
                    }
                    else{
                        [self setTimerAccording2Stored];
                    }
                    
                    [SVProgressHUD showWithStatus:@" Setting Timer"];
                }
                
            }
            else{
                NSLog(@"没有连接");
                
                [[TGBleManager sharedTGBleManager] candidateConnect:candId];
                timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
                
                [SVProgressHUD showWithStatus:@" Connecting"];
                
            }
        }
        
        else if ([CommonValues getFlag] == 2){
            
        }
        
        else if ([CommonValues getFlag] == 3){
            wantDisableTimer = YES;
            [timerBtn setImage:[UIImage imageNamed:@"off.png"] forState:UIControlStateNormal];
            [defaults setBool:NO forKey:TIMER_SWITCH];
            
        }
        else if ([CommonValues getFlag] == 4){
            wantDisableTimer = YES;
            [timerBtn setImage:[UIImage imageNamed:@"off.png"] forState:UIControlStateNormal];
            [defaults setBool:NO forKey:TIMER_SWITCH];
        }
        
        
    }
    [defaults synchronize];
    [CommonValues setFlag:0];
    __LOG_METHOD_END;
}

-(void)insertImage:(UISwitch *)sender{
    __LOG_METHOD_START;
    __LOG_METHOD_END;
}

//tableview delegate method

//- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
//    return 1;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    __LOG_METHOD_START;
    //    if (self.datasource && [self.datasource respondsToSelector:@selector(popoverListView:numberOfRowsInSection:)])
    //    {
    //        return [self.datasource popoverListView:self numberOfRowsInSection:section];
    //    }
    NSLog(@"numberOfRowsInSection repeatArray.count = %lu",(unsigned long)repeatArray.count);
    __LOG_METHOD_END;
    return repeatArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    __LOG_METHOD_START;
    static NSString *CellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:CellIdentifier] ;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    NSUInteger row = [indexPath row];
    //  NSLog(@"cellForRowAtIndexPath = %d",row);
    cell.textLabel.text = [repeatArray objectAtIndex:row];
    if([repeatDayArray containsObject: [repeatArray objectAtIndex:row] ]){
        //   cell.imageView.image =  [UIImage imageNamed:@"Connection2"];
        cell.imageView.image = [UIImage imageNamed:@"duihao"];
        //     NSLog(@"3");
    }
    else {
        
        cell.imageView.image = [UIImage imageNamed:@"nothing"];
    }
    __LOG_METHOD_END;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    __LOG_METHOD_START;
    NSUInteger row =[indexPath row];
    NSString *rowString =  [repeatArray objectAtIndex:row];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    alarmMode  = [repeatArray objectAtIndex:row];
    [userDefault setObject:[repeatArray objectAtIndex:row] forKey:USER_ALARM_MODE];
    [userDefault synchronize];
    [self add2RepeatArray:rowString];
    [repeatTableView reloadData];
    __LOG_METHOD_END;
}

- (void)checkBTStatusDone{
    __LOG_METHOD_START;
    if ([CommonValues getBluetoothOnFlag]) {
        //turn on the BT
        NSLog(@"already have  turn on the BT");
    }
    else{
        NSLog(@" BT  still not on state");
        [self showBTNotOnAlert];
    }
    __LOG_METHOD_END;
}

- (void)hasTokenTimeOutCancel{
    __LOG_METHOD_START;
    __LOG_METHOD_END;
}
- (void)hasTokenTimeOutRetry{
    //Retry
    __LOG_METHOD_START;
    if (wantSetTimer) {
        [self timerBtnClick:nil];
    }
    else if (wantSetAlarm){
        [self alarmBtnClick:nil];
    }
    __LOG_METHOD_END;
}

- (void)noTokenTimeOutCancel{
    __LOG_METHOD_START;
    __LOG_METHOD_END;
}
- (void)noTokenTimeOutRetry{
    __LOG_METHOD_START;
    //Retry
    if (wantSetTimer) {
        [self timerBtnClick:nil];
    }
    else if (wantSetAlarm){
        [self alarmBtnClick:nil];
    }
    __LOG_METHOD_END;
}

- (void)selectDeviceCancel{
    __LOG_METHOD_START;
    [self stopScan];
    __LOG_METHOD_END;
}

- (void)selectDeviceOK{
    __LOG_METHOD_START;
    if (wantSetGoal) {
        
        [self.view setUserInteractionEnabled:NO];
        
    }
    else if (wantSetAlarm) {
        
        [self.view setUserInteractionEnabled:NO];
        
    }
    else if (wantSetTimer) {
        
        [self.view setUserInteractionEnabled:NO];
        
    }
    
    else{
        NSLog(@"SVProgressHUD unknown status");
    }
    
    //                NSLog(@"call  candidateStopScan------");
    //                [[TGBleManager sharedTGBleManager] candidateStopScan];
    [self stopScan];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSLog(@"need to save deviceName = %@",readyToSaveDeviceName);
    //if ([defaults stringForKey:@"DeviceName"] == nil) {
    [defaults setObject: readyToSaveDeviceName forKey:@"DeviceName"];
    [defaults setObject: readyToSaveDeviceID forKey:@"DeviceID"];
    [defaults synchronize];
    
    //candidate connect to the band;
    NSLog(@"call candidateConnect -----");
    [[TGBleManager sharedTGBleManager] candidateConnect:readyToSaveDeviceID];
    timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
    [SVProgressHUD showWithStatus:@" Connecting"];
    __LOG_METHOD_END;
}

- (void)resetDeviceListTemp{
    __LOG_METHOD_START;
    [devicesArray removeAllObjects];
    [mfgDataArray removeAllObjects];
    [rssiArray removeAllObjects];
    [candIDArray removeAllObjects];
    [devicesIDArray removeAllObjects];
    [listView dismiss];
    //            selectDeviceBtnClickFlag = NO;
    [self.view setUserInteractionEnabled:YES];
    __LOG_METHOD_END;
}

- (void)bondAlertCancel{
    __LOG_METHOD_START;
    NSLog(@"call tryDisconnect------");
    [[TGBleManager sharedTGBleManager] tryDisconnect];
    __LOG_METHOD_END;
}

- (void)bondAlertOK{
    __LOG_METHOD_START;
    NSLog(@"call takeBond------");
    [[TGBleManager sharedTGBleManager] takeBond];
    [SVProgressHUD showWithStatus:@" Bonding"];
    __LOG_METHOD_END;
}

- (void)noTokenBondFailedCancel{
    __LOG_METHOD_START;
    [self.view setUserInteractionEnabled:YES];
    [SVProgressHUD dismiss];
    __LOG_METHOD_END;
}
- (void)noTokenBondFailedRetry{
    __LOG_METHOD_START;
    //connect previous band
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"DeviceName"];
    NSString *candId = [defaults objectForKey:@"DeviceID"];
    NSLog(@"stored candId = %@",candId);
    NSLog(@"call candidateConnect---");
    [[TGBleManager sharedTGBleManager]candidateConnect:candId];
    timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
    [SVProgressHUD showWithStatus:@" Connecting"];
    
    if (wantSetGoal) {
        
        [self.view setUserInteractionEnabled:NO];
        
    }
    else if (wantSetAlarm) {
        [self.view setUserInteractionEnabled:NO];
        
    }
    else if (wantSetTimer) {
        [self.view setUserInteractionEnabled:NO];
        
    }
    else{
        NSLog(@"SVProgressHUD unknown status");
    }
    __LOG_METHOD_END;
}

- (void)noTokenBondFailedTryAnother{
    __LOG_METHOD_START;
    [self saveBtnClick:nil];
    __LOG_METHOD_END;
}

- (void)hasTokenBondFailedCancel{
    __LOG_METHOD_START;
    [SVProgressHUD dismiss];
    NSLog(@"hasToken  BondFailedTag cancel");
    [self.view setUserInteractionEnabled:YES];
    __LOG_METHOD_END;
}

- (void)hasTokenBondFailedRetry{
    __LOG_METHOD_START;
    [SVProgressHUD dismiss];
    if (![CommonValues getBluetoothOnFlag]) {
        NSLog(@"BT OFF, can't go on candidateConnect");
        [self showBTNotOnAlert];
        __LOG_METHOD_END;
        return;
    }
    //connect previous band
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"DeviceName"];
    NSString *candId = [defaults objectForKey:@"DeviceID"];
    NSLog(@"stored candId = %@",candId);
    NSLog(@"call candidateConnect---");
    [[TGBleManager sharedTGBleManager]candidateConnect:candId];
    timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
    [SVProgressHUD showWithStatus:@" Connecting"];
    
    if (wantSetGoal) {
        //                    [SVProgressHUD showWithStatus:@"Setting goal..."];
        [self.view setUserInteractionEnabled:NO];
        
    }
    else if (wantSetAlarm) {
        //                    [SVProgressHUD showWithStatus:@"Setting alarm..."];
        [self.view setUserInteractionEnabled:NO];
        
    }
    else if (wantSetTimer) {
        //                    [SVProgressHUD showWithStatus:@"Setting timer..."];
        [self.view setUserInteractionEnabled:NO];
        
    }
    else{
        NSLog(@"SVProgressHUD unknown status");
    }
    __LOG_METHOD_END;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    __LOG_METHOD_START;
    NSLog(@"buttonIndex:%ld", (long)buttonIndex);
    switch (alertView.tag) {
        case checkBTStatusTag:
            [self checkBTStatusDone];
            break;
            
            
        case   hasTokenTimeOutAlertTag:
            if (buttonIndex == 0) {
                [self hasTokenTimeOutRetry];
            }
            else {
                [self hasTokenTimeOutCancel];
            }
            break;
            
            
        case   noTokenTimeOutAlertTag:
            
            if (buttonIndex == 0) {
                [self noTokenTimeOutCancel];
            }
            
            else if (buttonIndex == 1)
            {
                [self noTokenTimeOutRetry];
                
            }
            break;
            
        case selectDeviceAlertTag:
        {
            
            if (buttonIndex == 0) {
                [self selectDeviceCancel];
            }
            
            else if(buttonIndex == 1){
                [self selectDeviceOK];
            }
            [self resetDeviceListTemp];
        }
            break;
        case bondAlertTag:{
            if (buttonIndex == 0) {
                [self bondAlertCancel];
            }
            
            else if(buttonIndex == 1){
                [self bondAlertOK];
            }
        }
            
            break;
        case noTokenBondFailedTag:
        {
            if (buttonIndex == 0) {
                [self noTokenBondFailedCancel];
            }
            
            else if(buttonIndex == 1){
                [self noTokenBondFailedRetry];
                
            }
            else if(buttonIndex == 2){
                [self noTokenBondFailedTryAnother];
            }
            
        }
            break;
            
        case hasTokenBondFailedTag:
        {
            if (buttonIndex == 1) {
                [self hasTokenBondFailedRetry];
            }
            else if(buttonIndex == 0){
                [self hasTokenBondFailedCancel];
            }
            
        }
            break;
            
        default:
            break;
    }
    __LOG_METHOD_END;
}


-(void)setAlarmAccording2Stored{
    __LOG_METHOD_START;
    // Set Alarm
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *alarm_arr_str = [defaults objectForKey:USER_ALARM];
    
    NSMutableArray *alarmMode_arr = [defaults objectForKey:USER_ALARM_MODE];
    NSLog(@"alarm_arr_str = %@ ",alarm_arr_str);
    NSLog(@"alarmMode_arr = %@ ",alarmMode_arr);
    
    NSArray *alarm_array = [alarm_arr_str componentsSeparatedByString:@":"];
    int alarm_h = 0;
    int alarm_m = 0;
    if (alarm_array.count == 2) {
        alarm_h = [alarm_array[0]intValue];
        alarm_m = [alarm_array [1]intValue];
    }
    
    if (alarmMode_arr != NULL && [alarmMode_arr isKindOfClass:[NSArray class]]) {
        NSLog(@"call setAlarm --------");
        //          NSLog(@"alarm_h = %d --alarm_m = %d ",alarm_h,alarm_m);
        
        [[TGBleManager sharedTGBleManager] setAlarmHour:alarm_h];// 10:09 AM
        [[TGBleManager sharedTGBleManager] setAlarmMinute:alarm_m];
        NSLog(@"alarm_h = %d,alarm_m = %d",alarm_h,alarm_m);
        //        if (repeatDayArray.count == 0) {
        //            [repeatDayArray addObject:@"Test"];
        //            [repeatDayArray addObject:@"Once"];
        //            [defaults setObject:repeatDayArray forKey:USER_ALARM_MODE];
        //        }
        
        /*
         for (int i = 0 ; i<alarmMode_arr.count; i++) {
         NSString *am =  alarmMode_arr[i];
         NSLog(@"am = %@ ",am);
         if (![am isEqualToString:@"Test"]) {
         NSLog(@"repeat mode = %@ ",am);
         
         }
         }
         
         */
        
        
        //         NSLog(@"alarm_h = %d --alarm_m = %d ",alarmMode_arr);
        [[TGBleManager sharedTGBleManager] setAlarmRepeat:[self repeatCode:alarmMode_arr]];
        
        
    }
    __LOG_METHOD_END;
}

-(void)setTimerAccording2Stored{
    __LOG_METHOD_START;
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    
    NSDate *nowDate = [NSDate date];
    
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString * timerAlarmStr =  [defaults objectForKey:USER_TIMER_ALARM];
    NSLog(@"stored timer string = %@",timerAlarmStr);
    NSArray *timer_alarm_array = [timerAlarmStr componentsSeparatedByString:@":"];
    int alarm_h = 0;
    int alarm_m = 0;
    
    if (timer_alarm_array.count == 2) {
        alarm_h = [ timer_alarm_array[0] intValue];
        alarm_m =[ timer_alarm_array[1] intValue];
        
        
    }
    
    NSTimeInterval interval = alarm_h*60*60+ alarm_m*60;
    NSDate  *date_last =  [nowDate initWithTimeIntervalSinceNow:+interval];
    NSString *dateString = [dateFormatter stringFromDate:date_last];
    NSLog(@"date_last str = %@",dateString);
    NSLog(@"TIMER_DERICTORY_TIME date_last = %@",date_last);
    [defaults setObject:date_last forKey:TIMER_DERICTORY_TIME];
    
    int alarm_h_last = 0;
    int alarm_m_last = 0;
    int alarm_s_last = 0;
    NSArray *timer_alarm_array_last = [dateString componentsSeparatedByString:@":"];
    if (timer_alarm_array_last.count == 3) {
        alarm_h_last = [ timer_alarm_array_last[0] intValue];
        alarm_m_last =[ timer_alarm_array_last[1] intValue];
        alarm_s_last = [ timer_alarm_array_last[2] intValue];
        
    }
    NSLog(@"alarm_h_last = %d ,alarm_m_last = %d ,alarm_s_last = %d",alarm_h_last,alarm_m_last,alarm_s_last);
    NSLog(@"call setGoalDuration-----");
    [[TGBleManager sharedTGBleManager] setGoalDurationHour:alarm_h_last];
    [[TGBleManager sharedTGBleManager] setGoalDurationMinute:alarm_m_last];
    [[TGBleManager sharedTGBleManager] setGoalDurationSecond:alarm_s_last];
    __LOG_METHOD_END;
}


- (void)didReceiveMemoryWarning
{
    __LOG_METHOD_START;
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    __LOG_METHOD_END;
}

- (IBAction)setSleepTime:(id)sender
{
    __LOG_METHOD_START;
    __LOG_METHOD_END;
}

- (IBAction)eraseClick:(id)sender
{
    __LOG_METHOD_START;
    __LOG_METHOD_END;
}

- (IBAction)backClick:(id)sender {
    __LOG_METHOD_START;
    [self writeToTemp];
    [self dismissViewControllerAnimated:YES completion:nil];
    //    [self performSelector:@selector(delayToDismissView) withObject:nil afterDelay:1];
    __LOG_METHOD_END;
}

- (IBAction)soundSwitchClick:(id)sender {
    __LOG_METHOD_START;
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    BOOL storedBeepFlag =  [ud boolForKey:BEEP_SOUND];
    if (storedBeepFlag) {
        [ud setBool:NO forKey:BEEP_SOUND];
        [soundSwtichBtn  setImage:[UIImage imageNamed:@"off.png"] forState:UIControlStateNormal];
        
    }
    else{
        [ud setBool:YES forKey:BEEP_SOUND];
        [soundSwtichBtn  setImage:[UIImage imageNamed:@"on.png"] forState:UIControlStateNormal];
    }
    
    [ud synchronize];
    __LOG_METHOD_END;
}

- (IBAction)autoSyncSwitchClick:(id)sender {
    __LOG_METHOD_START;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL storedAutoSyncSwitch = [defaults boolForKey:AUTOSYNC_SWITCH];
    NSString *storedAutoSyncTime = [defaults objectForKey:AUTOSYNC_TIME];
    
    if (storedAutoSyncSwitch) {
        [defaults setBool:NO forKey:AUTOSYNC_SWITCH];
        [autoSyncSwitchBtn  setImage:[UIImage imageNamed:@"off.png"] forState:UIControlStateNormal];
        //        if ([CommonValues getAutoSyncTimer].valid) {
        //            [[CommonValues getAutoSyncTimer] invalidate];
        //            NSLog(@"timer 失效");
        //        }
        
        /*先注掉  为了测试T-T background task*/
        [[UIApplication sharedApplication] clearKeepAliveTimeout];
        NSLog(@"clearKeepAliveTimeout -------remove last background task ");
        
        
    }
    else{
        if (autoSyncText.text.length == 0) {
            return;
        }
        
        [defaults setBool:YES forKey:AUTOSYNC_SWITCH];
        [autoSyncSwitchBtn  setImage:[UIImage imageNamed:@"on.png"] forState:UIControlStateNormal];
        //        if ([CommonValues getAutoSyncTimer].valid) {
        //            [[CommonValues getAutoSyncTimer] invalidate];
        //            NSLog(@"timer 失效");
        //        }
        //comment out for save
        NSInteger seconds = [CommonValues secondsFromString:storedAutoSyncTime];
        NSLog(@"AUTOSYNC_TIME stored seconds == %ld", (long)seconds);
        
        //        [CommonValues setAutoSyncTimer:[NSTimer scheduledTimerWithTimeInterval:seconds target:[CommonValues getDashboardID] selector:@selector(doSyncStuff) userInfo:nil repeats:YES]]  ;
        
        if (FALSE) {
            NSLog(@"background auto sync task 暂时不启用");
        }
        else{
            /* 先注掉  为了测试T-T background task */
            [[UIApplication sharedApplication] clearKeepAliveTimeout];
            NSLog(@"clearKeepAliveTimeout -------remove last background task ");
            [[UIApplication sharedApplication] setKeepAliveTimeout:seconds handler:^{
                [[CommonValues getDashboardID] performSelector:@selector(doSyncStuff)];
                
            }];
            
            
            NSLog(@" background auto sync task  开始");
        }
        
    }
    [defaults synchronize];
    __LOG_METHOD_END;
}

-(void)delayToDismissView{
    __LOG_METHOD_START;
    [self dismissViewControllerAnimated:YES completion:nil];
    __LOG_METHOD_END;
}

- (IBAction)bgTouch:(id)sender {
    __LOG_METHOD_START;
    // When the user presses return, take focus away from the text field so that the keyboard is dismissed.
    /*
     NSTimeInterval animationDuration = 0.30f;
     [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
     [UIView setAnimationDuration:animationDuration];
     CGRect rect = CGRectMake(0.0f, 0.0f, myWholeView.frame.size.width, myWholeView.frame.size.height);
     myWholeView.frame = rect;
     [UIView commitAnimations];
     
     */
    __LOG_METHOD_END;
}

- (IBAction)autoSyncSelectClick:(id)sender {
    __LOG_METHOD_START;
    SetAutoSyncTimeViewController *sast = [[SetAutoSyncTimeViewController alloc]init];
    [self presentViewController:sast animated:YES completion:nil];
    __LOG_METHOD_END;
}



- (IBAction)alarmBtnClick:(id)sender {
    __LOG_METHOD_START;
    if ([CommonValues getAutoSyncingFlag]) {
        NSLog(@"auto syncing, main operation need wait  ");
        [[CommonValues autoSyncingAlert] show];
        __LOG_METHOD_END;
        return;
    }
    
    
    if (![CommonValues getBluetoothOnFlag]) {
        NSLog(@"BT OFF, can't go on candidateConnect");
        [self showBTNotOnAlert];
        __LOG_METHOD_END;
        return;
    }
    
    
    if ([alarmText.text isEqualToString:@""]) {
        NSLog(@"invalid alarmBtnClick ");
        __LOG_METHOD_END;
        return;
    }
    
    isSettingAlarm = YES;
    [CommonValues setHasTaskFlag:YES];
    [[TGBleManager sharedTGBleManager] setDelegate:self];
    [self.view setUserInteractionEnabled:NO];
    lostBeforeMainOperation = YES;
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    alarmSwitch = [defaults boolForKey:ALARM_SWITCH];
    
    if (!alarmSwitch) {
        NSLog(@"alarmSwitch on ");
        alarmSwitch = YES;
        //        [alarmBtn setImage:[UIImage imageNamed:@"on.png"] forState:UIControlStateNormal];
        //        [self setAlarmAccording2Stored];
        //        [defaults setBool:YES forKey:ALARM_SWITCH];
        
        wantSetTimer = NO;
        wantSetAlarm = YES;
        wantSetGoal = NO;
        wantDisableAlarm = NO;
    }
    else{
        NSLog(@"alarmSwitch  NOT on ");
        alarmSwitch = NO;
        //        [alarmBtn setImage:[UIImage imageNamed:@"off.png"] forState:UIControlStateNormal];
        // disable timer
        [[TGBleManager sharedTGBleManager] setAlarmHour:25];
        
        wantDisableAlarm = YES;
        //        [defaults setBool:NO forKey:ALARM_SWITCH];
    }
    NSString *candId = [defaults objectForKey:@"DeviceID"];
    //    isBonded =  [defaults boolForKey:@"bond_state"];
    NSData *token = [[TGBleManager sharedTGBleManager]bondToken];
    wantSetAlarm = YES;
    
    if ([TGBleManager sharedTGBleManager].status == TGBleConnectedToBond) {
        //已有连接,直接做主要操作
        NSLog(@"已有连接,直接做主要操作");
        
        if (isSettingAlarm){
            if (wantDisableAlarm) {
                [[TGBleManager sharedTGBleManager] setAlarmHour:25];
            }
            else{
                [self setAlarmAccording2Stored];
            }
            
            [SVProgressHUD showWithStatus:@" Setting Alarm"];
            
        }
        
        else if (isSettingTimer){
            if (wantDisableTimer) {
                [[TGBleManager sharedTGBleManager] setGoalDurationHour:25];
                
            }
            else{
                [self setTimerAccording2Stored];
            }
            
            [SVProgressHUD showWithStatus:@" Setting Timer"];
        }
        
        
        
    }
    else{
        
        
        if (token == NULL) {
            [self showDevicesList];
        }
        else{
            //        [SVProgressHUD showWithStatus:@"Setting alarm ..."];
            [self.view setUserInteractionEnabled:NO];
            isSettingAlarm = YES;
            wantSetAlarm = YES;
            //connect
            [[TGBleManager sharedTGBleManager] candidateConnect:candId];
            
            [SVProgressHUD showWithStatus:@" Connecting"];
            timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
        }
        
    }
    [defaults synchronize];
    __LOG_METHOD_END;
}

- (IBAction)timerBtnClick:(id)sender {
    __LOG_METHOD_START;
    if ([CommonValues getAutoSyncingFlag]) {
        NSLog(@"auto syncing, main operation (set timer) need wait  ");
        [[CommonValues autoSyncingAlert] show];
        __LOG_METHOD_END;
        return;
    }
    

    if (![CommonValues getBluetoothOnFlag]) {
        NSLog(@"BT OFF, can't go on candidateConnect");
        [self showBTNotOnAlert];
        __LOG_METHOD_END;
        return;
    }
    
    if ([timerText.text isEqualToString:@""]) {
        NSLog(@"invalid timerBtnClick ");
        __LOG_METHOD_END;
        return;
    }
    
    lostBeforeMainOperation = YES;
    [CommonValues setHasTaskFlag:YES];
    [[TGBleManager sharedTGBleManager] setDelegate:self];
    [self.view setUserInteractionEnabled:NO];
    isSettingTimer = YES;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    timerSwitch = [defaults boolForKey:TIMER_SWITCH];
    
    
    if (timerSwitch) {
        NSLog(@"time Switch is on ");
        // disable timer
        wantDisableTimer = YES;
        [[TGBleManager sharedTGBleManager] setGoalDurationHour:25];
        
        //        [defaults setBool:NO forKey:TIMER_SWITCH];
        timerSwitch = NO ;
        //        [timerBtn setImage:[UIImage imageNamed:@"off.png"] forState:UIControlStateNormal];
    }
    
    else{
        //        // enable timer
        NSLog(@"time Switch is off ");
        
        wantDisableTimer = NO;
        
        //        [defaults setBool:YES forKey:TIMER_SWITCH];
        timerSwitch = YES;
        
        //        [timerBtn setImage:[UIImage imageNamed:@"on.png"] forState:UIControlStateNormal];
        
        
        wantSetTimer = YES;
        wantSetAlarm = NO;
        wantSetGoal = NO;
    }
    
    NSString *candId = [defaults objectForKey:@"DeviceID"];
    NSData *token = [[TGBleManager sharedTGBleManager]bondToken];
    wantSetTimer = YES;
    
    if ([TGBleManager sharedTGBleManager].status == TGBleConnectedToBond) {
        //已有连接,直接做主要操作
        NSLog(@"已有连接,直接做主要操作");
        
        if (isSettingAlarm){
            if (wantDisableAlarm) {
                [[TGBleManager sharedTGBleManager] setAlarmHour:25];
            }
            else{
                [self setAlarmAccording2Stored];
            }
            
            [SVProgressHUD showWithStatus:@" Setting Alarm"];
            
        }
        
        else if (isSettingTimer){
            if (wantDisableTimer) {
                [[TGBleManager sharedTGBleManager] setGoalDurationHour:25];
                
            }
            else{
                [self setTimerAccording2Stored];
            }
            
            [SVProgressHUD showWithStatus:@" Setting Timer"];
        }
        
        
        
    }
    else{
        
        
        if (token == NULL) {
            [self showDevicesList];
        }
        else{
            //        [SVProgressHUD showWithStatus:@"Setting timer ..."];
            [self.view setUserInteractionEnabled:NO];
            isSettingTimer = YES;
            wantSetTimer = YES;
            //connect
            [[TGBleManager sharedTGBleManager] candidateConnect:candId];
            [SVProgressHUD showWithStatus:@" Connecting"];
            
            timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
        }
        
        
    }
    [defaults synchronize];
    __LOG_METHOD_END;
}



- (IBAction)alarmSelectClick:(id)sender {
    __LOG_METHOD_START;
    if ([CommonValues getAutoSyncingFlag]) {
        NSLog(@"auto syncing, main operation need wait  ");
        [[CommonValues autoSyncingAlert] show];
    }
    else{
        viewTag = 1;
        AlarmViewController *avc = [[AlarmViewController alloc]init];
        [self presentViewController:avc animated:YES completion:nil];
    }
    __LOG_METHOD_END;
}

- (IBAction)timerSelectClick:(id)sender {
    __LOG_METHOD_START;
    viewTag = 2;
    TimerViewController *tvc = [[TimerViewController alloc]init];
    [self presentViewController:tvc animated:YES completion:nil];
    __LOG_METHOD_END;
}



- (IBAction)profileClick:(id)sender {
    __LOG_METHOD_START;
    viewTag = 0;
    
    //    if (screenHeight == 568) {
    ProfileViewController *myProfile = [[ProfileViewController alloc]init];
    [self presentViewController:myProfile animated:YES completion:nil];
    //    }
    //    else{
    //    Profile_4S_ViewController *myProfile = [[Profile_4S_ViewController alloc]init];
    //    [self presentViewController:myProfile animated:YES completion:nil];
    //
    //    }
    __LOG_METHOD_END;
}

- (IBAction)saveBtnClick:(id)sender {
    __LOG_METHOD_START;

    
    
    isSettingGoal = YES;
    wantSetGoal = YES;
    
    if ([TGBleManager sharedTGBleManager].status == TGBleConnectedToBond) {
        //已有连接,直接做主要操作
        NSLog(@"已有连接,直接做主要操作");
        
        if (isSettingGoal){
            
            
        }
        else if (isSettingAlarm){
            if (wantDisableAlarm) {
                [[TGBleManager sharedTGBleManager] setAlarmHour:25];
            }
            else{
                [self setAlarmAccording2Stored];
            }
            
            [SVProgressHUD showWithStatus:@" Setting Alarm"];
            
        }
        
        else if (isSettingTimer){
            if (wantDisableTimer) {
                [[TGBleManager sharedTGBleManager] setGoalDurationHour:25];
                
            }
            else{
                [self setTimerAccording2Stored];
            }
            
            [SVProgressHUD showWithStatus:@" Setting Timer"];
        }
        
    }
    else{
        NSLog(@"没有连接");
        
        NSData *token = [[TGBleManager sharedTGBleManager]bondToken];
        if (token != NULL) {
            NSLog(@"bond_state");
            // connect
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *candId = [defaults objectForKey:@"DeviceID"];
            NSLog(@"call candidateConnect:candId-----");
            [[TGBleManager sharedTGBleManager] candidateConnect:candId];
            timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
            //        [SVProgressHUD showWithStatus:@"Setting goal..."];
            [SVProgressHUD showWithStatus:@" Connecting"];
            [self.view setUserInteractionEnabled:NO];
        }
        
        else {
            NSLog(@"not_bond_state");
            // show devices list
            [self showDevicesList];
        }
        
        timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
        
    }
    __LOG_METHOD_END;
}

- (IBAction)goalClick:(id)sender {
    __LOG_METHOD_START;
    SetGoalViewController  *sgv = [[SetGoalViewController alloc]init];
    [self presentViewController:sgv animated:YES completion:nil];
    __LOG_METHOD_END;
}

- (IBAction)setDefaultSleepTime:(id)sender
{
    __LOG_METHOD_START;
    if (!_setSleepTimeView)
    {
        _setSleepTimeView=[[[NSBundle mainBundle] loadNibNamed:@"setSleepTimeView" owner:self options:nil] lastObject];
        
        [_setSleepTimeView adaptIOSVersion];
        
    }
    [self.view addSubview:_setSleepTimeView];
    
    __weak SettingsViewController *weakSelf=self;
    _setSleepTimeView.cancelBlock=^{
        [weakSelf.setSleepTimeView removeFromSuperview];
    };
    
    _setSleepTimeView.saveBlock=^{
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];

        [userDefault setObject:weakSelf.setSleepTimeView.startSleepTimeString forKey:AUTO_SLEEP_START_TIME];
        [userDefault setObject:weakSelf.setSleepTimeView.endSleepTimeString forKey:AUTO_SLEEP_END_TIME];
        NSLog(@"setting view controller startSleepTimeString %@",weakSelf.setSleepTimeView.startSleepTimeString );
        NSLog(@"setting view controller endSleepTimeString %@",weakSelf.setSleepTimeView.endSleepTimeString );
        [userDefault synchronize];
        
        weakSelf.sleepStartTimeLabel.text=weakSelf.setSleepTimeView.startSleepTimeString;
        weakSelf.sleepStopTimeLabel.text=weakSelf.setSleepTimeView.endSleepTimeString;
        
        [weakSelf.setSleepTimeView removeFromSuperview];
        
    };
    __LOG_METHOD_END;
}

-(void)showDevicesList{
    __LOG_METHOD_START;
    if (![CommonValues getBluetoothOnFlag]) {
        NSLog(@"BT OFF, can't go on candidateScaning");
        [self showBTNotOnAlert];
    }
    else{
    
        [SVProgressHUD showWithStatus:@" Search bands "];
        [self.view setUserInteractionEnabled:NO];
        
        NSLog(@" call  candidateScan:nameList");
        [[TGBleManager sharedTGBleManager] candidateScan:[CommonValues bandDeviceArray]];
        
        
        NSLog(@"Menu item  devicesArray count = %lu",(unsigned long)devicesArray.count);
    }
    __LOG_METHOD_END;
}

-(void)stopScan{
    __LOG_METHOD_START;
    [self.view setUserInteractionEnabled:YES];
    NSLog(@"call candidateStopScan----");
    
    [[TGBleManager sharedTGBleManager]candidateStopScan];
    [devicesArray removeAllObjects];
    [mfgDataArray removeAllObjects];
    [rssiArray removeAllObjects];
    [candIDArray removeAllObjects];
    [devicesIDArray removeAllObjects];
    
    //    selectDeviceBtnClickFlag = NO;
    
    // cancel delayed time out alert.
    [timeOutTimer setFireDate:[NSDate distantFuture]];
    [timeOutTimer invalidate];
    
    firstEnterCandiDel = NO;
    __LOG_METHOD_END;
}

- (NSUInteger)supportedInterfaceOrientations

{ // NSLog(@"Dash supportedInterfaceOrientations --- ");
    //   [[UIApplication sharedApplication]setStatusBarOrientation:UIInterfaceOrientationPortrait];
    
    return UIInterfaceOrientationMaskPortrait;
    
}
//
//
-(BOOL)shouldAutorotate{
    // NSLog(@"Dash shouldAutorotate --- ");
    return NO;
}
-(void)writeToTemp{
    __LOG_METHOD_START;
    //    NSFileManager *defaultManager;
    //   defaultManager = [NSFileManager defaultManager];
    //   [defaultManager removeFileAtPath: tildeFilename
    //                            handler: nil];
    
    
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    
    
    NSString*   birth  = [defaults objectForKey:USER_AGE];
    int year = 0;
    int  month = 0 ;
    int day = 0;
    NSArray *array_birth = [birth componentsSeparatedByString:@"/"];
    if (array_birth.count == 3) {
        year  =  [ array_birth[0] intValue];
        month =  [ array_birth[1] intValue];
        day =  [ array_birth[2] intValue];
        //  NSLog(@"year = %d,month = %d, day =%d",year,month,day);
    }
    
    [[TGBleManager sharedTGBleManager] setBirthYear:year];
    [[TGBleManager sharedTGBleManager] setBirthMonth:month];
    [[TGBleManager sharedTGBleManager] setBirthDay:day];
    
    
    NSLog(@"age from SDK = %ld",(long)[[TGBleManager sharedTGBleManager]age]);
    
    
    
    NSLog(@"USER_IS_FEMALE = %@,USER_AGE = %@,USER_WEIGHT = %@, USER_HEIGHT = %@,USER_WALKING_STEP_LENGH = %@, USER_RUNNING_STEP_LENGH = %@", [defaults objectForKey:USER_IS_FEMALE],[defaults objectForKey:USER_AGE], [defaults objectForKey:USER_WEIGHT],[defaults objectForKey:USER_HEIGHT],[defaults objectForKey:USER_WALKING_STEP_LENGH],[defaults objectForKey:USER_RUNNING_STEP_LENGH]);
    
    NSString *temp = [ NSString stringWithFormat:@"gender  %@\nage  %ld\nheight  %@\nweight  %@\nwalk_step_size  %@\nrun_step_size  %@"
                        , [defaults objectForKey:USER_IS_FEMALE]
                        , (long)[[TGBleManager sharedTGBleManager]age]
                        , [defaults objectForKey:USER_HEIGHT]
                        , [NSString stringWithFormat:@"%ld" ,(long)[[defaults objectForKey:USER_WEIGHT]integerValue]*10]
                        , [defaults objectForKey:USER_WALKING_STEP_LENGH]
                        , [defaults objectForKey:USER_RUNNING_STEP_LENGH]
                     ];
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];//去处需要的路径
    
    
    
    
    
    //创建文件管理器
    NSFileManager *fileManager = [NSFileManager defaultManager];
    //获取路径
    //参数NSDocumentDirectory要获取那种路径
    //    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //    NSString *documentsDirectory = [paths objectAtIndex:0];//去处需要的路径
    //
    //    //更改到待操作的目录下
    //    [fileManager changeCurrentDirectoryPath:[documentsDirectory stringByExpandingTildeInPath]];
    //
    //    //创建文件fileName文件名称，contents文件的内容，如果开始没有内容可以设置为nil，attributes文件的属性，初始为nil
    //    [fileManager createFileAtPath:@"fileName" contents:nil attributes:nil];
    //
    //    //删除待删除的文件
    //    [fileManager removeItemAtPath:@"createdNewFile" error:nil];
    
    // 写入数据：
    //获取文件路径
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"userPro.txt"];
    
    
    [fileManager removeItemAtPath:path error:nil];
    
    //待写入的数据
    //    NSString *temp = @"Hello friend3232kflskdlfkslkfls";
    //    int data0 = 1323232;
    //    float data1 = 23.432325f;
    
    //创建数据缓冲
    NSMutableData *writer = [[NSMutableData alloc] init];
    
    //将字符串添加到缓冲中
    [writer appendData:[temp dataUsingEncoding:NSUTF8StringEncoding]];
    
    //将其他数据添加到缓冲中
    //    [writer appendBytes:&data0 length:sizeof(data0)];
    //    [writer appendBytes:&data1 length:sizeof(data1)];
    
    
    //将缓冲的数据写入到文件中
    [writer writeToFile:path atomically:YES];
    __LOG_METHOD_END;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    __LOG_METHOD_START;
    CGRect frame = textField.frame;
    int offset = frame.origin.y + 32 - (self.view.frame.size.height - 216.0);//键盘高度216
    NSTimeInterval animationDuration = 0.30f;
    [UIView beginAnimations:@"ResizeForKeyBoard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    float width_ = self.view.frame.size.width;
    float height_ = self.view.frame.size.height;
    if(offset > 0)
    {
        CGRect rect = CGRectMake(0.0f, -offset,width_,height_);
        self.view.frame = rect;
    }
    [UIView commitAnimations];
    __LOG_METHOD_END;
}


-(NSMutableArray *) add2RepeatArray :(NSString *) day{
    __LOG_METHOD_START;
    NSArray *sevenDays = [[NSArray alloc]initWithObjects:@"Monday",@"Tuesday", @"Wednesday",@"Thursday", @"Fridy",@"Saturday", @"Sunday",nil];
    NSArray *twoDays =   [[NSArray alloc]initWithObjects:@"Once",@"Everyday" ,nil];
    //数组中包含这一天
    if ([repeatDayArray containsObject:day]) {
        [repeatDayArray removeObject:day];
    }
    
    //数组中不包含这一天
    else {
        //  day是7天中的某一天，则删除每天&&仅一次
        
        if ([sevenDays containsObject:day]) {
            if ([repeatDayArray containsObject:twoDays[0]]) {
                [repeatDayArray removeObject:twoDays[0]];
            }
            if ([repeatDayArray containsObject:twoDays[1]]) {
                [repeatDayArray removeObject:twoDays[1]];
            }
            
            
        }
        
        // day是每天||仅一次，则属于7天中的每一天
        else{
            
            [repeatDayArray removeAllObjects];
            //  [repeatDayArray addObject:@"Test"];
        }
        
        //加上day
        [repeatDayArray addObject:day];
    }
    
    if (![repeatDayArray containsObject:@"Test"]) {
        [repeatDayArray addObject:@"Test"];
    }
    
    if (repeatDayArray.count == 1 ) {
        [repeatDayArray addObject:@"Once"];
    }
    
    
    NSLog(@"add2RepeatArray  ----repeatDayArray = %@",repeatDayArray);
    
    
    if (repeatDayArray.count < 2) {
        NSLog(@"Error!!!!!!");
    }
    __LOG_METHOD_END;
    return repeatDayArray;
}
-(void)setUserPro2band{
    __LOG_METHOD_START;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    /*int*/ step =  [defaults integerForKey:USER_GOAL_STEP];
    
    
    
    /*NSString * */ age =  [defaults objectForKey:USER_AGE];
    /*NSString * */ height =  [defaults objectForKey:USER_HEIGHT];
    /*NSString * */ weight =  [defaults objectForKey:USER_WEIGHT];
    /*NSString * */ walkLength =  [defaults objectForKey:USER_WALKING_STEP_LENGH];
    /*NSString * */ runLength =  [defaults objectForKey:USER_RUNNING_STEP_LENGH];
    /*BOOL*/ is_female = [defaults boolForKey:USER_IS_FEMALE];
    /*BOOL*/ is_band_right = [defaults boolForKey:USER_IS_BAND_RIGHT];
    /*NSString * */ alarmStr =  [defaults objectForKey:USER_ALARM];
    
//    BOOL is24hr =  [defaults boolForKey:DISPLAY_24HR_Time];
//    BOOL isImpial =    [defaults boolForKey:DISPLAY_IMPERIAL_UNITS];

    
    int year = 0;
    int month = 0;
    int day = 0;
    
    NSArray *array = [age componentsSeparatedByString:@"/"];
    if (array.count == 3) {
        year  =  [ array[0] intValue];
        month =  [ array[1] intValue];
        day =  [ array[2] intValue];
        //  NSLog(@"year = %d,month = %d, day =%d",year,month,day);
    }
    
//    NSArray *alarm_array = [alarmStr componentsSeparatedByString:@":"];
//    int alarm_h = 0;
//    int alarm_m = 0;
//    if (alarm_array.count == 2) {
//        alarm_h = [ alarm_array[0] intValue];
//        alarm_m =[ alarm_array[1] intValue];
//        
//    }
    //  NSLog(@"alarm_h = %d,alarm_m = %d",alarm_h,alarm_m);
    
    //set user profile
    [[TGBleManager sharedTGBleManager] setFemale:is_female];
    
    [[TGBleManager sharedTGBleManager] setBirthYear:year];
    [[TGBleManager sharedTGBleManager] setBirthMonth:month];
    [[TGBleManager sharedTGBleManager] setBirthDay:day];
    
    
    [[TGBleManager sharedTGBleManager] setHeight:[height intValue]]; // in cm
    [[TGBleManager sharedTGBleManager] setWeight:[weight intValue]*10]; // in 1/10 of kg
    
    [[TGBleManager sharedTGBleManager] setWalkingStepLength:[walkLength intValue]]; // in cm
    [[TGBleManager sharedTGBleManager] setRunningStepLength:[runLength intValue]]; // in cm
    [[TGBleManager sharedTGBleManager] setBandOnRight:is_band_right]; // false =  left wrist
    [[TGBleManager sharedTGBleManager] setGoalSteps:(int)step]; // 0 = disabled
    
    
    //    [[TGBleManager sharedTGBleManager] setGoalDurationHour:0];
    //    [[TGBleManager sharedTGBleManager] setGoalDurationMinute:1];
    //    [[TGBleManager sharedTGBleManager] setGoalDurationSecond:10];
    
    //
    //
    //    [[TGBleManager sharedTGBleManager] setBirthMonth:1];
    //    [[TGBleManager sharedTGBleManager] setBirthDay:1];
    //    [[TGBleManager sharedTGBleManager] setDisplayImperialUnits:isImpial]; //     false = kilometer, true = miles
    //    [[TGBleManager sharedTGBleManager] setDisplayTime24Hour:is24hr];
    
    //    [[TGBleManager sharedTGBleManager] setAlarmRepeat:0x01]; // alarm repeats every day of week, except
    
    __LOG_METHOD_END;
}

-(int) repeatCode : (NSMutableArray*) repeatArry{
    __LOG_METHOD_START;
    int repeatCode = 0x00;
    int mon = 0x02;int tue = 0x04;int wed = 0x08;
    int thu = 0x10;int fri = 0x20;int sat = 0x40;int sun = 0x80;
    
    
    //    if ([repeatArry containsObject:@"Once"]) {
    //        repeatCode = 0x00;
    //    }
    //    else if ([repeatArry containsObject:@"Everyday"]){
    //        repeatCode = 0x01;
    //    }
    //   else{
    
    
    
    
    if (repeatArry.count <= 2 && repeatArry.count > 0) {
        if ([repeatArry[0] isEqualToString:@"Monday"]|| [repeatArry[1] isEqualToString:@"Monday"]) {
            repeatCode = mon;
            repeatCode = ~repeatCode;
        }
        else if ([repeatArry[0] isEqualToString:@"Tuesday"]||[repeatArry[1] isEqualToString:@"Tuesday"]) {
            repeatCode = tue;
            repeatCode = ~repeatCode;
        }
        else if ([repeatArry[0] isEqualToString:@"Wednesday"]||[repeatArry[1] isEqualToString:@"Wednesday"]) {
            repeatCode = wed;
            repeatCode = ~repeatCode;
        }
        else if ([repeatArry[0] isEqualToString:@"Thursday"]||[repeatArry[1] isEqualToString:@"Thursday"]) {
            repeatCode = thu;
            repeatCode = ~repeatCode;
        }
        else if ([repeatArry[0] isEqualToString:@"Friday"]||[repeatArry[1] isEqualToString:@"Friday"]) {
            repeatCode = fri;
            repeatCode = ~repeatCode;
        }
        else if ([repeatArry[0] isEqualToString:@"Saturday"]||[repeatArry[1] isEqualToString:@"Saturday"]) {
            repeatCode = sat;
            repeatCode = ~repeatCode;
        }
        else if ([repeatArry[0] isEqualToString:@"Sunday"]||[repeatArry[1] isEqualToString:@"Sunday"]) {
            repeatCode = sun;
            repeatCode = ~repeatCode;
        }
        
        else if ([repeatArry[0] isEqualToString:@"Once"]||[repeatArry[1] isEqualToString:@"Once"]) {
            repeatCode = 0x00;
        }
        else if ([repeatArry[0] isEqualToString:@"Everyday"]||[repeatArry[1] isEqualToString:@"Everyday"]) {
            repeatCode = 0x01;
        }
        
        
        
        
    }
    
    else{
        for (int i = 0; i< repeatArry.count; i++) {
            if ([[repeatArry objectAtIndex:i] isEqualToString:@"Monday"]) {
                repeatCode = repeatCode|mon;
            }
            if ([[repeatArry objectAtIndex:i] isEqualToString:@"Tuesday"]) {
                repeatCode = repeatCode|tue;
            }
            if ([[repeatArry objectAtIndex:i] isEqualToString:@"Wednesday"]) {
                repeatCode = repeatCode|wed;
            }
            if ([[repeatArry objectAtIndex:i] isEqualToString:@"Thursday"]) {
                repeatCode = repeatCode|thu;
            }
            if ([[repeatArry objectAtIndex:i] isEqualToString:@"Friday"]) {
                repeatCode = repeatCode|fri;
            }
            if ([[repeatArry objectAtIndex:i] isEqualToString:@"Saturday"]) {
                repeatCode = repeatCode|sat;
            }
            if ([[repeatArry objectAtIndex:i] isEqualToString:@"Sunday"]) {
                repeatCode = repeatCode|sun;
            }
            
        }
        
        repeatCode = ~repeatCode;
        
    }
    
    // }
    
    NSLog(@"repeatCode setting = %X",repeatCode);
    
    __LOG_METHOD_END;
    return  repeatCode;
}




- (void)show {
    __LOG_METHOD_START;
    [SVProgressHUD show];
    __LOG_METHOD_END;
}

- (void)showWithStatus {
    __LOG_METHOD_START;
    [SVProgressHUD showWithStatus:@"Doing Stuff"];
    __LOG_METHOD_END;
}


#pragma mark -
#pragma mark Dismiss Methods Sample

- (void)dismissSVProgressHUD {
    __LOG_METHOD_START;
    [SVProgressHUD dismiss];
    __LOG_METHOD_END;
}



- (void) bleDidConnect {
    __LOG_METHOD_START;
    [timeOutTimer setFireDate:[NSDate distantFuture]];
    [timeOutTimer invalidate];
    NSData *sdkToken =  [[TGBleManager sharedTGBleManager] bondToken];
    
    if (sdkToken == NULL) {
        NSLog(@"call tryBond-----");
        [[TGBleManager sharedTGBleManager] tryBond];
        
    }
    
    else{
        
        NSString *sn =  [[TGBleManager sharedTGBleManager] hwSerialNumber];
        NSLog(@"sdkToken = %@, sn = %@",sdkToken,sn);
        
        NSLog(@"call adoptBond: serialNumber ");
        [[TGBleManager sharedTGBleManager] adoptBond:sdkToken serialNumber:sn];
        
    }
    
    
    lostBeforeMainOperation = NO;
    
    dispatch_async( dispatch_get_main_queue(), ^ {
        [SVProgressHUD showWithStatus:@" Bonding"];
    });
    __LOG_METHOD_END;
}

- (void) bleDidDisconnect {
    __LOG_METHOD_START;
    firstEnterCandiDel = NO;
    isErasing = NO;
    isSettingTimer = NO;
    isSettingGoal = NO;
    isSettingAlarm = NO;
    dispatch_async( dispatch_get_main_queue(), ^ {
        [SVProgressHUD dismiss];
        [self.view setUserInteractionEnabled:YES];
    });
    __LOG_METHOD_END;
}

- (void) bleLostConnect {
    __LOG_METHOD_START;
    /*  comment out  2014 8/5 14:33
     firstEnterCandiDel = NO;
     isErasing = NO;
     isSettingTimer = NO;
     isSettingGoal = NO;
     isSettingAlarm = NO;
     dispatch_async( dispatch_get_main_queue(), ^ {
     [SVProgressHUD dismiss];
     [self.view setUserInteractionEnabled:YES];
     });
     
     */
    
    
    if ([timeOutTimer isValid]) {
        [timeOutTimer invalidate];
        NSLog(@"timeOutTimer invalidate----");
        
    }
    
    
    if (lostBeforeMainOperation ) {
        if (wantSetAlarm) {
            NSLog(@"want to SetAlarm , lost connection before Main operation");
            
        }
        
        else if (wantSetTimer) {
            NSLog(@"want to SetTimer , lost connection before Main operation");
            
        }
        else if (wantSetGoal) {
            NSLog(@"want to SetGoal , lost connection before Main operation");
            
        }
        else{
            NSLog(@"wantSetTimer = no && wantSetAlarm = no && wantSetGoal = no ,lost connection before Main operation");
        }
        
        
        //        dispatch_async( dispatch_get_main_queue(), ^ {
        //            [self.view setUserInteractionEnabled:YES];
        //            [SVProgressHUD dismiss];
        //
        //        });
        
        
        NSData *token = [[TGBleManager sharedTGBleManager] bondToken];
        if (token == NULL) {
            if ([CommonValues iOSVersion] >= IOS8) {
                alertController = [UIAlertController alertControllerWithTitle:LostConnectTitle message:LostConnectMsg preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:CANCEL style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                    [self noTokenBondFailedCancel];
                    
                }];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:RETRY style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self noTokenBondFailedRetry];
                }];
                UIAlertAction *tryAnotherAction = [UIAlertAction actionWithTitle:RETRY style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self noTokenBondFailedTryAnother];
                }];
                [alertController addAction:cancelAction];
                [alertController addAction:okAction];
                [alertController addAction:tryAnotherAction];
                [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
            }
            
            else{
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:LostConnectTitle message:LostConnectMsg
                                                              delegate:self cancelButtonTitle:CANCEL otherButtonTitles:RETRY,TRYANOTHERBAND, nil];
                alert.tag = noTokenBondFailedTag;
                dispatch_async( dispatch_get_main_queue(), ^ {
                    [alert show];
                });
                
            }
        }
        
        else{
            if ([CommonValues iOSVersion] >= IOS8) {
                alertController = [UIAlertController alertControllerWithTitle:LostConnectTitle message:LostConnectMsg preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:CANCEL style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                    [self hasTokenBondFailedCancel];
                    
                }];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:RETRY style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self hasTokenBondFailedRetry];
                }];
                [alertController addAction:cancelAction];
                [alertController addAction:okAction];
                [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
            }
            else{
                UIAlertView *alert_hasToken = [[UIAlertView alloc]initWithTitle:LostConnectTitle message:LostConnectMsg delegate:self cancelButtonTitle:CANCEL otherButtonTitles:RETRY, nil];
                
                alert_hasToken.tag = hasTokenBondFailedTag;
                dispatch_async( dispatch_get_main_queue(), ^ {
                    [alert_hasToken show];
                });
                
            }
            
        }
        
        
        
    }
    else{
        dispatch_async( dispatch_get_main_queue(), ^ {
            
            [SVProgressHUD dismiss];
            [self.view setUserInteractionEnabled: YES];
        });
    }
    __LOG_METHOD_END;
}

- (void) bleDidAbortConnect {
    __LOG_METHOD_START;
    __LOG_METHOD_END;
}

- (void) candidateFound:(NSString *)devName rssi:(NSNumber *)rssi mfgID:(NSString *)mfgID candidateID:(NSString *)candidateID
{
    __LOG_METHOD_START;
    NSString* message = [NSString stringWithFormat:
                         @"Device Name: %@\nMfg Data: %@\nID: %@\nRSSI value: %@",
                         devName,
                         mfgID,
                         candidateID,
                         rssi
                         ];
    NSLog(@"SELECT DEVICE candidateFound delegate: %@", message);
    
    
    if ([candIDArray containsObject:candidateID]) {
        __LOG_METHOD_END;
        return;
    }
    if (!firstEnterCandiDel) {
        NSLog(@"first enter candidate Found");
        firstEnterCandiDel = YES;
        [timeOutTimer setFireDate:[NSDate distantFuture]];
        [timeOutTimer invalidate];
        [SVProgressHUD dismiss];
        
        listView = [[ZSYPopoverListView alloc] initWithFrame:CGRectMake(0, 0, 300, 400)];
        
        listView.tagert =self;
        listView.stopEBLScan =@selector(stopScan);
        
        
        listView.datasource = self;
        listView.delegate = self;
        showListTimer =  [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(delay2ShowList) userInfo:nil repeats:NO];
        
        //        [listView show];
        
    }
    
    
    [devicesArray addObject:devName];
    [mfgDataArray addObject:mfgID];
    [rssiArray addObject:rssi];
    [candIDArray addObject:candidateID];
    [devicesIDArray addObject:candidateID];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [listView reload];
        
        
    });
    __LOG_METHOD_END;
}


- (void) bleDidSendAlarmGoals:(TGsendResult)result {
    __LOG_METHOD_START;
    lostBeforeMainOperation = NO;
    
    NSString * message;
    
    if (result == TGSendSuccess) {
        message = @"ALARM CFG Succesful";
        
        
    }
    else if (result == TGSendFailedNoAcknowledgement) {
        message = @"ALARM CFG time out, no ack";
        
    }
    else if (result == TGSendFailedDisconnected) {
        message = @"ALARM CFG disconnected";
    }
    else {
        message = [NSString stringWithFormat:@"ALARM CFG Unexpected Error, code: %d", (int) result];
    }
    
    NSLog(@"ALARM CFG Result Code  %lu",(unsigned long)result);
    NSLog(@"ALARM CFG Result msg  %@",message);
    
    [CommonValues setHasTaskFlag:NO];
    [[TGBleManager sharedTGBleManager] setDelegate:[CommonValues getDashboardID]];
    
    
    if ([CommonValues isBackgroundFlag]) {
        NSLog(@"Set Alarm 完成，且在后台，应断开连接");
        NSLog(@"call tryDisconnect ------");
        [[TGBleManager sharedTGBleManager] tryDisconnect];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.view setUserInteractionEnabled:YES];
    });
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (result == TGSendSuccess) {
        //message = @"ALARM CFG Succesful";
        if (isSettingGoal) {
            NSLog(@"set goal step success!");
            isSettingGoal = NO;
            wantSetGoal = NO;
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showSuccessWithStatus:@"Set Goal completed."];
                
                
            });
            
            //save goal
            
            [defaults setInteger:step forKey:USER_GOAL_STEP];
            [defaults setInteger:calorie forKey:USER_GOAL_CALORI];
            [defaults setInteger:distance forKey:USER_GOAL_DISTANCE];
            [defaults setFloat:sleep_ forKey:USER_GOAL_SLEEP];
            [defaults synchronize];
        }
        else if(isSettingAlarm){
            NSLog(@"Set Alarm success!");
            isSettingAlarm  = NO;
            wantSetAlarm = NO;
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //            [SVProgressHUD showSuccessWithStatus:@"Set Alarm success"];
                [SVProgressHUD showSuccessWithStatus:@"Set Alarm completed."];
                //update switch state;
                
                if (wantDisableAlarm) {
                    NSLog(@"wantDisableAlarm--- set off");
                    [alarmBtn setImage:[UIImage imageNamed:@"off.png"] forState:UIControlStateNormal];
                    [defaults setBool:NO forKey:ALARM_SWITCH];
                    [defaults synchronize];
                }
                else{
                    NSLog(@"wantOnAlarm--- set on");
                    [self setAlarmAccording2Stored];
                    [alarmBtn setImage:[UIImage imageNamed:@"on.png"] forState:UIControlStateNormal];
                    [defaults setBool:YES forKey:ALARM_SWITCH];
                    [defaults synchronize];
                }
                
            });
            
        }
        else if(isSettingTimer){
            NSLog(@"Set Timer success!");
            isSettingTimer  = NO;
            wantSetTimer = NO;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showSuccessWithStatus:@"Set Timer completed."];
                
                if (wantDisableTimer) {
                    [defaults setBool:NO forKey:TIMER_SWITCH];
                    timerSwitch = NO ;
                    [timerBtn setImage:[UIImage imageNamed:@"off.png"] forState:UIControlStateNormal];
                }
                
                else{
                    [defaults setBool:YES forKey:TIMER_SWITCH];
                    timerSwitch = YES;
                    
                    [timerBtn setImage:[UIImage imageNamed:@"on.png"] forState:UIControlStateNormal];
                    
                }
                
                [defaults synchronize];
            });
            
        }
        
        
    }
    else{
        
        NSLog(@"SendAlarmGoals  failed!!!!");
        //failed
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if (isSettingGoal) {
            
        }
        else if (isSettingAlarm){
            
            [alarmBtn setImage:[UIImage imageNamed:@"off.png"] forState:UIControlStateNormal];
            [defaults setBool:NO forKey:ALARM_SWITCH];
            dispatch_async(dispatch_get_main_queue(), ^{
                //            [SVProgressHUD showSuccessWithStatus:@"Set Alarm success"];
                [SVProgressHUD showSuccessWithStatus:@"Set Alarm failed."];
                
            });
        }
        else if (isSettingTimer){
            [timerBtn setImage:[UIImage imageNamed:@"off.png"] forState:UIControlStateNormal];
            [defaults setBool:NO forKey:TIMER_SWITCH];
            dispatch_async(dispatch_get_main_queue(), ^{
                //            [SVProgressHUD showSuccessWithStatus:@"Set Alarm success"];
                [SVProgressHUD showSuccessWithStatus:@"Set Timer failed."];
                
            });
        }
        else{
            NSLog(@"unkown operation --- sendAlarmGoals Failed");
        }
        
        [defaults synchronize];
    }
    __LOG_METHOD_END;
}

- (void) bleDidSendUserConfig:(TGsendResult)result {
    __LOG_METHOD_START;
    NSString * message;
    
    if (result == TGSendSuccess) {
        message = @"USER CFG Succesful";
    }
    else if (result == TGSendFailedNoAcknowledgement) {
        message = @"USER CFG time out, no ack";
    }
    else if (result == TGSendFailedDisconnected) {
        message = @"USER CFG disconnected";
    }
    else {
        message = [NSString stringWithFormat:@"USER CFG Unexpected Error, code: %d", (int) result];
    }
    NSLog(@"USER CFG Result Code  %lu",(unsigned long)result);
    NSLog(@"USER CFG Result msg  %@",message);
    __LOG_METHOD_END;
}

- (void)potentialBond:(NSString*) code sn: (NSString*) sn devName:(NSString*) devName
{
    __LOG_METHOD_START;
    NSString* displayMessage = [NSString stringWithFormat:
                                @"%@%@?",DigitCodeConfirmMsg,
                                code];
    dispatch_async( dispatch_get_main_queue(), ^ {
        [SVProgressHUD dismiss];
        if ([CommonValues iOSVersion] >= IOS8) {
            alertController = [UIAlertController alertControllerWithTitle:@"" message:displayMessage preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                [self bondAlertCancel];
                
            }];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self bondAlertOK];
            }];
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
        }
        else{
            UIAlertView *bondDialog = [[UIAlertView alloc]initWithTitle:@"" message:displayMessage delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];        bondDialog.tag = bondAlertTag;
            [bondDialog show];
            
        }
        
    });
    __LOG_METHOD_END;
}


- (void) bleDidBond:(TGbondResult)result {
    __LOG_METHOD_START;
    NSString * message;
    NSString * title;
    
    if (result == TGbondResultTokenAccepted) {
        message = @"Bond Succesful";
        
        
        
        
    }
    else if (result == TGbondResultTokenReleased) {
        title = TokenReleasedTitle;
        message = TokenReleasedMsg;
    }
    else if (result == TGbondResultErrorBondedNoMatch) {
        title = CommunicationErrorTitle;
        message = BondedNoMatchMsg;
    }
    else if (result == TGbondResultErrorBadTokenFormat) {
        title = BondRejectedTitle;
        message = BadTokenFormatMsg;
    }
    else if (result == TGbondResultErrorTimeOut) {
        title = TimeOutTitle;
        message = TimeOutMsg;
    }
    
    else if (result == TGbondResultErrorNoConnection) {
        title = NoConnectionTitle;
        message = NoConnectionMsg;
    }
    
    else if (result == TGbondResultErrorReadTimeOut) {
        title = ReadBackTimeOutTitle;
        message = ReadTimeOutMsg;
    }
    
    
    else if (result == TGbondResultErrorReadBackTimeOut) {
        title = ReadBackTimeOutTitle;
        message = ReadBackTimeOutMsg;
    }
    
    else if (result == TGbondResultErrorWriteFail) {
        title = WriteFailTitle;
        message = WriteFailMsg;
    }
    else if (result == TGbondResultErrorTargetIsAlreadyBonded) {
        title = BondRejectedTitle;
        message = TargetIsAlreadyBondedMsg;
    }
    
    
    else if (result == TGbondAppErrorNoPotentialBondDelegate) {
        title  = BondAppErrorTitle;
        message = NoPotentialBondDelegateMsg;
    }
    
    
    else if (result == TGbondResultErrorTargetHasWrongSN) {
        title  = CommunicationErrorTitle;
        message = TargetHasWrongSNMSg;
    }
    else if (result == TGbondResultErrorPairingRejected) {
        title  = PairingErrorTitle;
        message = PairingRejectedMsg;
    }
    
    
    else if (result == TGbondResultErrorSecurityMismatch) {
        title  = PairingErrorTitle;
        message = SecurityMismatchMsg;
    }
    
    
    else {
        title  = UnkonwnBondErrorTitle;
        message = [NSString stringWithFormat:@"Bonding Error, code: %d", (int) result];
    }
    NSLog(@"BONDIING RESULT Code  %lu",(unsigned long)result);
    NSLog(@"BONDIING RESULT msg  %@",message);
    
    // set bond flag,check it when do every action
    if (result == TGbondResultTokenAccepted) {
        successBondedFlag  = YES;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [eraseBtn setImage:[UIImage imageNamed:@"erase_data_1"] forState:UIControlStateNormal];
            [releaseBtn setImage:[UIImage imageNamed:@"release_bond_1"] forState:UIControlStateNormal];
            [eraseBtn setUserInteractionEnabled:YES];
            [releaseBtn setUserInteractionEnabled:YES];
        });
        
        NSLog(@"Bond Succesful,start to erase/setTimer/setAlarm");
        if (isErasing) {
            NSLog(@"call tryEraseData -----");
            [[TGBleManager  sharedTGBleManager] tryEraseData];
            
        }
        else if (isSettingGoal){
            
            [[TGBleManager  sharedTGBleManager] setGoalSteps:(int)step];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showWithStatus:@" Setting Goals"];
            });
            
            
            
            
        }
        else if (isSettingAlarm){
            if (wantDisableAlarm) {
                [[TGBleManager sharedTGBleManager] setAlarmHour:25];
            }
            else{
                [self setAlarmAccording2Stored];
            }
            
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showWithStatus:@" Setting Alarm"];
            });
            
        }
        
        else if (isSettingTimer){
            if (wantDisableTimer) {
                [[TGBleManager sharedTGBleManager] setGoalDurationHour:25];
                
            }
            else{
                [self setTimerAccording2Stored];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showWithStatus:@" Setting Timer"];
            });
        }
    }
    else if (result == TGbondResultTokenReleased){
        NSLog(@"此处不应该被执行到！！！");
        
    }
    else{
        
        lostBeforeMainOperation = NO;
        successBondedFlag  = NO;
        
        NSLog(@"call tryDisconnect----");
        [[TGBleManager sharedTGBleManager]tryDisconnect];
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            [eraseBtn setImage:[UIImage imageNamed:@"erase_data_2"] forState:UIControlStateNormal];
            [releaseBtn setImage:[UIImage imageNamed:@"release_bond_2"] forState:UIControlStateNormal];
            [eraseBtn setUserInteractionEnabled:NO];
            [releaseBtn setUserInteractionEnabled:NO];
        });
        
        NSData *token = [[TGBleManager sharedTGBleManager] bondToken];
        if (token == NULL) {
            if ([CommonValues iOSVersion] >= IOS8) {
                alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:CANCEL style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                    [self noTokenBondFailedCancel];
                    
                }];
                UIAlertAction *retryAction = [UIAlertAction actionWithTitle:RETRY style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self noTokenBondFailedRetry];
                }];
                UIAlertAction *tryAnotherAction = [UIAlertAction actionWithTitle:TRYANOTHERBAND style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self noTokenBondFailedTryAnother];
                }];
                [alertController addAction:cancelAction];
                [alertController addAction:retryAction];
                [alertController addAction:tryAnotherAction];
                [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
            }
            else{
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:CANCEL otherButtonTitles:RETRY,TRYANOTHERBAND, nil];
                alert.tag = noTokenBondFailedTag;
                dispatch_async( dispatch_get_main_queue(), ^ {
                    [alert show];
                });
            }
        }
        
        else{
            if ([CommonValues iOSVersion] >= IOS8) {
                alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:CANCEL style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                    [self hasTokenBondFailedCancel];
                    
                }];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:RETRY style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self hasTokenBondFailedRetry];
                }];
                [alertController addAction:cancelAction];
                [alertController addAction:okAction];
                [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
            }
            else{
                UIAlertView *alert_hasToken = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:CANCEL otherButtonTitles:RETRY, nil];
                
                alert_hasToken.tag = hasTokenBondFailedTag;
                dispatch_async( dispatch_get_main_queue(), ^ {
                    [alert_hasToken show];
                });
                
            }
        }    }
    __LOG_METHOD_END;
}

-(void) batteryLevel:(int)level {
    __LOG_METHOD_START;
    NSLog(@">>>>>>>-----New Battery Level: percent complete: %d", level);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:level  forKey:LATEST_BATTERY];
    [defaults synchronize];
    __LOG_METHOD_END;
}

- (void) exceptionMessage:(TGBleExceptionEvent)eventType {
    __LOG_METHOD_START;
    NSString * message;
    
    if (eventType == TGBleHistoryCorruptErased) {
        message = @"Flash Memory Corrupted, Will Be erased, suspect battery has been exhausted";
    }
    else if (eventType == TGBleConfigurationModeCanNotBeChanged) {
        message = @"Exception Message - Ble Connection Mode CAN NOT be changed";
    }
    else if (eventType == TGBleUserAgeHasBecomeNegative_BirthDateIsNOTcorrect) {
        message = @"User Age has become negative, correct the birth date";
    }
    else if (eventType == TGBleUserAgeIsTooLarge_BirthDateIsNOTcorrect) {
        message = @"User Age is TOO OLD, check the birth date";
    }
    else if (eventType == TGBleUserBirthDateRejected_AgeOutOfRange) {
        message = @"User Age is out of RANGE, corret the birth date";
    }
    else if (eventType == TGBleFailedOtherOperationInProgress) {
        message = @"Exception Message - Another Operation is Already in Progress";
    }
    else if (eventType == TGBleFailedSecurityNotInplace) {
        message = @"Exception Message - Security is NOT In Place";
    }
    else if (eventType == TGBleTestingEvent) {
        message = @"Exception Message - This is only a Test";
    }
    else if (eventType == TGBleReInitializedNeedAlarmAndGoalCheck) {
        message = @"Exception Message - Band lost power or was MFG reset, check your alarm and goal settings";
    }
    else if (eventType == TGBleStepGoalRejected_OutOfRange) {
        message = @"Exception Message - Step Goal Settings rejected -- OUT OF RANGE";
    }
    else if (eventType == TGBleCurrentCountRequestTimedOut) {
        message = @"Exception Message - Can Not get Current Count values\nBand not responding";
    }
    else if (eventType == TGBleConnectFailedSuspectKeyMismatch) {
        message = @"Exception Message - Band appears to be paired, possible encryption key mismatch, hold side button for 10 seconds to reboot";
    }
    else if (eventType == TGBleNonRepeatingAlarmMayBeStaleCheckAlarmConfiguration) {
        message = @"Exception Message - A Non-repeating Alarm may be stale, check your alarm settings";
    }
    else {
        message = [NSString stringWithFormat:@"Exception Detected, code: %lu", (unsigned long)eventType];
    }
    
    //    NSLog(@"message = %@",message);
    
    NSLog(@"exceptionMessage =  %@",message);
    __LOG_METHOD_END;
}



#pragma marks
#pragma tableview datasource methods
#pragma mark -
- (NSInteger)popoverListView:(ZSYPopoverListView *)tableView numberOfRowsInSection:(NSInteger)section
{
    __LOG_METHOD_START;
    __LOG_METHOD_END;
    return devicesArray.count;
}

- (UITableViewCell *)popoverListView:(ZSYPopoverListView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    __LOG_METHOD_START;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString * savedDeviceName = [defaults stringForKey:@"DeviceName"];
    NSLog(@"NSUserDefaults deviceName = %@",savedDeviceName);
    
    static NSString *identifier = @"identifier";
    UITableViewCell *cell = [tableView dequeueReusablePopoverCellWithIdentifier:identifier];
    if (nil == cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] ;
    }
    
    
    NSString *tmpStr = [NSString stringWithFormat:@"%@:%@",[ mfgDataArray objectAtIndex:indexPath.row],[ devicesArray objectAtIndex:indexPath.row]];
    //cell.textLabel.text =[ devicesArray objectAtIndex:indexPath.row];
    cell.textLabel.text = tmpStr;
    cell.textLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:25.0];
    
    if ([[ devicesArray objectAtIndex:indexPath.row] isEqualToString:savedDeviceName]) {
        // many devices have the same name
        //
        //cell.imageView.image = [UIImage imageNamed:@"fs_main_login_selected.png"];
    }
    
    __LOG_METHOD_END;
    return cell;
}

- (void)popoverListView:(ZSYPopoverListView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    __LOG_METHOD_START;
    /*UITableViewCell *cell = */ [tableView popoverCellForRowAtIndexPath:indexPath];
    //cell.imageView.image = [UIImage imageNamed:@"fs_main_login_normal.png"];
    NSLog(@"deselect:%ld", (long)indexPath.row);
    __LOG_METHOD_END;
}

- (void)popoverListView:(ZSYPopoverListView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    __LOG_METHOD_START;
    self.selectedIndexPath = indexPath;
    /*UITableViewCell *cell = */ [tableView popoverCellForRowAtIndexPath:indexPath];
    //cell.imageView.image = [UIImage imageNamed:@"fs_main_login_selected.png"];
    NSLog(@"select:%ld", (long)indexPath.row);
    //  NSString *deviceName = [NSString stringWithFormat:@"Wat%d", indexPath.row];
    
    NSString *deviceName = [devicesArray objectAtIndex:indexPath.row];
    NSString *mfgData = [mfgDataArray objectAtIndex:indexPath.row];
    
    //NSString *rssi = [rssiArray objectAtIndex:indexPath.row];
    
    readyToSaveDeviceName = [devicesArray objectAtIndex:indexPath.row];
    readyToSaveDeviceID = [devicesIDArray objectAtIndex:indexPath.row];
    NSString *msg = [NSString stringWithFormat:@"Are you sure you would like to connect to %@ %@  ?",deviceName, mfgData];
    [listView dismiss];
    if ([CommonValues iOSVersion] >= IOS8) {
        alertController = [UIAlertController alertControllerWithTitle:msg message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:CANCEL style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
            [self selectDeviceCancel];
            
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:OK style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self selectDeviceOK];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
    }
    
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:msg message:nil delegate:self cancelButtonTitle:CANCEL otherButtonTitles:OK, nil];
        alert.tag   = selectDeviceAlertTag;
        [alert show];
    }
    __LOG_METHOD_END;
}

-(void)showTimeOutAlert{
    __LOG_METHOD_START;
    [SVProgressHUD dismiss];
    
    [self stopScan];
    // delay to show alert ,prevent the confligs from SVProgressHUD dismiss
    [self performSelector:@selector(delay2ShowTimeOutAlert) withObject:nil afterDelay:0.5];
    __LOG_METHOD_END;
}

-(void)delay2ShowTimeOutAlert{
    __LOG_METHOD_START;
    if ([CommonValues iOSVersion] >= IOS8) {
        UIAlertAction *cancelAction;
        UIAlertAction *okAction;
        if ([[TGBleManager sharedTGBleManager]bondToken] != NULL ) {
            alertController = [UIAlertController alertControllerWithTitle:ConnectTimeOutTitle message:ConnectTimeOutMsg preferredStyle:UIAlertControllerStyleAlert];
            cancelAction = [UIAlertAction actionWithTitle:CANCEL style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                [self hasTokenTimeOutCancel];
                
            }];
            okAction = [UIAlertAction actionWithTitle:RETRY style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self hasTokenTimeOutRetry];
            }];
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
        }
        else{
            alertController = [UIAlertController alertControllerWithTitle:ScanTimeOutTitle message:ScanTimeOutMsg preferredStyle:UIAlertControllerStyleAlert];
            cancelAction = [UIAlertAction actionWithTitle:CANCEL style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                [self noTokenTimeOutCancel];
                
            }];
            okAction = [UIAlertAction actionWithTitle:RETRY style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self noTokenTimeOutRetry];
            }];
            
        }
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
        
        
    }
    else{
        UIAlertView *timeOutAlert;
        if ([[TGBleManager sharedTGBleManager]bondToken] != NULL ) {
            timeOutAlert =  [[UIAlertView alloc]initWithTitle:ConnectTimeOutTitle message:ConnectTimeOutMsg delegate:self cancelButtonTitle:nil otherButtonTitles:RETRY, CANCEL, nil];
            timeOutAlert.tag = hasTokenTimeOutAlertTag;
            
            
        }
        else{
            timeOutAlert = [[UIAlertView alloc]initWithTitle:ScanTimeOutTitle message:ScanTimeOutMsg delegate:self cancelButtonTitle:CANCEL otherButtonTitles:RETRY, nil];
            timeOutAlert.tag = noTokenTimeOutAlertTag;
        }
        
        [timeOutAlert show];
    }
    NSLog(@"call tryDisconnect----");
    [[TGBleManager sharedTGBleManager] tryDisconnect];
    __LOG_METHOD_END;
}

-(void)delay2ShowList{
    __LOG_METHOD_START;
    [listView show];
    __LOG_METHOD_END;
}

-(void)showBTNotOnAlert {
    __LOG_METHOD_START;
    if ([CommonValues iOSVersion] >= IOS8) {
        alertController = [UIAlertController alertControllerWithTitle:BluetoothIsOffTitle message:BluetoothIsOffMsg preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self checkBTStatusDone];
        }];
        [alertController addAction:okAction];
        [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
    }
    else{
        UIAlertView *alert =[ [UIAlertView alloc]initWithTitle:BluetoothIsOffTitle message:BluetoothIsOffMsg delegate:self cancelButtonTitle:nil otherButtonTitles:@"Done", nil];
        alert.tag = checkBTStatusTag;
        [alert show];
    }
    __LOG_METHOD_END;
}

- (void)showAlertController{
    __LOG_METHOD_START;
    [self presentViewController:alertController animated:YES completion:nil];
    __LOG_METHOD_END;
}


@end

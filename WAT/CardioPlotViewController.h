//
//  TestViewController.h
//  WAT
//
//  Created by neurosky on 10/20/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CHYPorgressImageView.h"
#import "SendEKGDataOperation.h"
#import "LineGraphView.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "HeartData.h"
#import "TGBleManager.h"
#import "TGLibEKG.h"
#import "CardioPlot.h"
#import "TGLibEKGdelegate.h"
#import "TGBleManagerDelegate.h"
#import "LineGraphView.h"
#import <sqlite3.h>
#import <AVFoundation/AVFoundation.h>
#import "AppDelegate.h"
#import "DoDataReceiveStuff.h"
#import "Constant.h"
#import "CommonValues.h"
#import "TGLibEKGdictionary.h"
#import "SVProgressHUD.h"
#import "FileOperation.h"

#import "TGLibEmulator.h"



@interface CardioPlotViewController : UIViewController<TGLibEKGdelegate,TGBleManagerDelegate>{
    
    NSOperationQueue *operationQueue;
    SendEKGDataOperation *sendOperation;
    int timeCount;
    NSString *fileURL;
    int dataCount;
    NSThread *hrThread;
    NSThread  *proThread;
    NSThread *yoloThread;
    NSThread *nerverlandThread;
    NSThread *rrIntervalThread;
    NSThread *peakThread;
    NSThread *hrvThread;
    NSThread* colourThread;
    
    BOOL hrThreadFlag;
    BOOL yoloThreadFlag;
    BOOL nerverlandThreadFlag;
    BOOL rrIntervalThreadFlag;
    BOOL  peakThreadFlag;
    BOOL hrvThreadFlag;
    BOOL colourThreadFlag;
    //record RT ecg data
    NSString* ecgRT_FileName;
    NSFileManager* ecgRT_fileManager;
    NSArray* ecgRT_paths;
    NSString* ecgRT_documentDirectory;
    NSString* ecgRT_path;
    NSFileHandle* ecgRT_write;
    
    NSString *deviceName;
    NSString *deviceID;
    int wat2ecgReceived;
    int wat2ecgSessionFlag;
    int wat2recordSesssionFlag;
    int wat2currentECGRecordTS;
    int wat2previousECGRecordTS;
    int wat2ecgCurrentTS;
    int wat2ecgRecordedTS;
    NSDate *wat2ecgiOSTS;
    int wat2watCurrentTS;
    
    
    BOOL ecgRT_fileIsOpen;
    BOOL isaa;
    int realtimeRawData;
    int  countSum;
    CFAbsoluteTime end;
    BOOL connectFlag;
    NSURL * audioUrl;
    AVAudioPlayer * audio;
    
    
    //record pedometer data
    NSString* pedFileName;
    NSString* pedPath;
    NSArray* pedPaths;
    NSString* pedDocumentDirectory;
    NSFileManager* pedFM;
    NSFileHandle* pedWrite;
    int pedBytes[18];
    NSString* pedData;
    
    NSString* ped2FileName;
    NSString* ped2Path;
    NSArray* ped2Paths;
    NSString* ped2DocumentDirectory;
    NSFileManager* ped2FM;
    NSFileHandle* ped2Write;
    int ped2Bytes[20];
    NSString* ped2Data;
    
    NSDate* iOSCurrentTime;
    NSString *iOSCurrentTimeString;
    UIImageView *img;
    UIView *uiview;
    UIButton *but;
    NSString * demoModeStr;
    BOOL reConnectFlag;
    float screenHeight;
    int caluRawCount;
    BOOL demoReplayFlag;
    
    float  * rawTempBuffer;
    int ECGBufferLength;
    BOOL tempTestFlag;
    NSMutableArray *rawBufferArr;
    const char * buffer[1024];
    CFAbsoluteTime lastValueTime;
    
    BOOL onSensorFlag;
    
    BOOL  keepFlag;
    CFAbsoluteTime recordedLastTime;
    
    NSThread *keepThread;
    
    int hrCounter;
    int hrSum;
    
    BOOL startFlag;
    NSString* ekg2FileName;
    NSString* ekg2Path;
    NSArray* ekg2Paths;
    NSString* ekg2DocumentDirectory;
    NSFileManager* ekg2FM;
    NSFileHandle* ekg2Write;
    int ekg2Bytes[20];
    NSString* ekg2Data;
    BOOL startHRVFlag;
    BOOL enterRawDataFlag;
    int sampleCount;
    
    TGLibEKG * tglibEKG;
    BOOL bandOnRightWrist;
    
    NSArray *rawArray;
    int rawIndex;
    NSTimer* sendDataTimer;
    
    BOOL isConnectFlag;
    NSMutableString *ecgLogStr;
    BOOL aNewTimeFlag;
    NSDateFormatter *dateFormatter;
    BOOL drawECGFlag;
    NSMutableArray *tenHRArr;
    UIAlertView *connectedAlert;
    UIAlertView *disconnectedAlert;
    BOOL changeColorLoopFlag;
    BOOL keepMonitorLoppFlag;
    int seconds;
    
    NSTimer *hrvTimer;
    BOOL firstOnSensorFlag;
    int latestHrv;
    BOOL firstSensorOffFlag;
    NSDate *startECGTime;
    NSMutableArray *hrvArr;
    NSMutableArray *hrArr;
    sqlite3  *db;
    NSTimer *stopECGTimeOutTimer;
    UIImageView *ProcessView;
    BOOL receiveRawDataFlag;
    BOOL receiveSmoothedRawFlag;
    BOOL firstReceiveRawFlag;
    BOOL firstReceiveFilteredDataFlag;
    int sampleRate;
    UIAlertController *alertController;
    int rawCountInTimer;
    NSArray *dataRaw ;
    NSInteger totalRawCount;
    NSTimer *addRaw2SDKTimer;
    int stress;
    CardioPlot *cardioPlot ;

    __weak IBOutlet UIView *myWholeView;
    __weak IBOutlet UILabel *hrvLabel;
    __weak IBOutlet UILabel *rrIntervalLabel;
    __weak IBOutlet UILabel *hrLabel;
    __weak IBOutlet UILabel *heartLevelLabel;
    IBOutlet UIImageView *countDownIv;
    IBOutlet UILabel *countDownTv;
    __weak IBOutlet UIImageView *trainZoneImg;
    __weak IBOutlet UILabel *testMoodLabel;
    __weak IBOutlet UIImageView *connectStateImage;
    __weak IBOutlet UILabel *moodLabel;
    __weak IBOutlet UILabel *wiredLabel;
    __weak IBOutlet UILabel *chilledLabel;
    
}

@property (retain, nonatomic) IBOutlet LineGraphView *ekgLineView;



@property (nonatomic,retain) TGLibEKG * tglibEKG;

//@property (strong, nonatomic) IBOutlet UIImageView *ProcessView;

- (IBAction)back:(id)sender;
//- (IBAction)upBtnClick:(id)sender;
//- (IBAction)downBtnClick:(id)sender;
@end

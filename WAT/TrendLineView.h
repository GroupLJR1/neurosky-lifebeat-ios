//
//  TrendLineView.h
//  WAT
//
//  Created by NeuroSky on 8/6/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"

@interface TrendLineView : UIView<CPTPlotDataSource,CPTAxisDelegate>{

    float _X1;
    float _X2;
    float _Y1;
    float _Y2;
}


-(id)initWithFrame:(CGRect)frame   :(NSMutableArray *)arr;

@end

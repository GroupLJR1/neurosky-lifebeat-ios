//
//  ECGHistoryViewController.h
//  WAT
//
//  Created by neurosky on 6/27/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@interface ECGHistoryViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{

    __weak IBOutlet UILabel *titleLabel;

    __weak IBOutlet UITableView *myTableView;
    NSDateFormatter *df ;
    sqlite3 *db;
    __weak IBOutlet UIView *myWholeView;
}
- (IBAction)backClick:(id)sender;

@end

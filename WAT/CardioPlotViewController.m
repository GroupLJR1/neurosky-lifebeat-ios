//
//  TestViewController.m
//  WAT
//
//  Created by neurosky on 10/20/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import "CardioPlotViewController.h"

#import "TGBleEmulator.h"
#import "TGLibEmulator.h"

@interface CardioPlotViewController ()

@end

@implementation CardioPlotViewController

@synthesize tglibEKG;
@synthesize ekgLineView;


- (void)sleepAnalysisResults:(TGEKGsleepResult)result
                   startTime:(NSDate *)startTime
                     endTime:(NSDate *)endTime
                    duration:(int)duration          // in minutes
                    preSleep:(int)preSleep          // in minutes
                    notSleep:(int)notSleep          // in minutes
                   deepSleep:(int)deepSleep         // in minutes
                  lightSleep:(int)lightSleep        // in minutes
                 wakeUpCount:(int)wakeUpCount       // number of wake ups after 1st sleep
                  totalSleep:(int)totalSleep
                     preWake:(int)preWake           // number of minutes active before the endTime
                  efficiency:(int)efficiency        // integer sleep efficiency
{
    __LOG_METHOD_START;
    return;
}

-(void)sleepAnalysisSmoothData:(int)samepleRate
                     sleepTime:(NSMutableArray*)sleepTimeArray
                    sleepPhase:(NSMutableArray*)sleepPhaseArray
    {
    __LOG_METHOD_START;
    return;
    }


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    __LOG_METHOD_START;
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    __LOG_METHOD_END;
    return self;
}

- (BOOL)prefersStatusBarHidden {
    __LOG_METHOD_START;
    return NO;
}
- (void)viewDidLoad
{
    __LOG_METHOD_START;
    [super viewDidLoad];
    NSLog(@"cardio plot viewDidLoad START");
    [self initCardioPlotVariables];
    __LOG_METHOD_END;
}

-(void)viewWillAppear:(BOOL)animated{
    __LOG_METHOD_START;
    [ super viewWillAppear: animated ];
    [self initCardioPlotUI];
    [self doRealtimeOrPlaybackTask];
    
    __LOG_METHOD_END;
}


-(void)initCardioPlotVariables{
    __LOG_METHOD_START;

    db = [CommonValues getMyDB];
    cardioPlot = [[CardioPlot alloc] init];
    hrvArr = [[NSMutableArray alloc] init];
    hrArr = [[NSMutableArray alloc] init];
    audioUrl = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/EKG.wav", [[NSBundle mainBundle] resourcePath]]];
    NSError *error;
    audio = [[AVAudioPlayer alloc] initWithContentsOfURL:audioUrl
                                                   error:&error];
    latestHrv = 0;
    seconds = 0;
    hrvLabel.hidden = YES;
    countDownTv.hidden = NO;
    countDownIv.hidden = NO;
    keepMonitorLoppFlag = YES;
    changeColorLoopFlag = YES;
    if ([CommonValues isPlayBackMode]) {
        connectStateImage.hidden = YES;
    }
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy_MM_dd_HH_mm_ss_SSS"];
    connectedAlert = [[UIAlertView alloc]initWithTitle:@"Successfully connected" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@" OK ", nil];
    disconnectedAlert = [[UIAlertView alloc]initWithTitle:@"Disconnected" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@" OK ", nil];
    screenHeight = [CommonValues getScreenHeight];
    NSLog(@"screenHeight = %f",screenHeight);
    tenHRArr = [[NSMutableArray alloc]init];
    sampleCount = 0;
    hrCounter = 0;
    hrSum = 0;
    startHRVFlag = NO;
    aNewTimeFlag = NO;
    keepThread = [[NSThread alloc]initWithTarget:self selector:@selector(keepMonitoringBackground) object:nil];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    deviceName = [defaults stringForKey:@"DeviceName"];
    deviceID = [defaults stringForKey:@"DeviceID"];
    NSLog(@"NSUserDefaults deviceName = %@",deviceName);
    NSString *age =  [defaults objectForKey:USER_AGE];
    int year = 0;
    int month = 0;
    int day = 0;
    
    NSArray *array = [age componentsSeparatedByString:@"/"];
    if (array.count == 3) {
        year  =  [ array[0] intValue];
        month =  [ array[1] intValue];
        day =  [ array[2] intValue];
        NSLog(@"year = %d,month = %d, day =%d",year,month,day);
    }
    
    if (![[TGBleManager sharedTGBleManager]isBandOnRight]) {
        NSLog(@"right hand---");
    }
    else{
        NSLog(@"left hand----");
        
    }
    
    //BLE SDK  set
    
    // real time mode, set BLE delegate
    if (![CommonValues isPlayBackMode]) {
        [[TGBleManager sharedTGBleManager] setDelegate:self];
    }
    bandOnRightWrist = NO;// should match the data file
    //    NSLog(@"age is = %d",year);
    NSLog(@"SDK age is = %ld",(long)[[TGBleManager sharedTGBleManager] age]);
    reConnectFlag = YES;
    hrThreadFlag = YES;
    hrvThreadFlag = YES;
    
    [self setStressConfig];
    __LOG_METHOD_END;
}

-(void)setStressConfig{
    __LOG_METHOD_START;
    //    [[TGBleManager sharedTGBleManager] setHeartAgeOutputPoint:30];
    //    [[TGBleManager sharedTGBleManager] setHeartAgeRecordNumber:5];
    //    [[TGBleManager sharedTGBleManager] setHeartAgeParameters:@"Mary" withAge:30 withPath:@""];
    //    [[TGBleManager sharedTGBleManager] setStressParameters:@"Mary" withGender:[[TGBleManager sharedTGBleManager] isFemale] withAge:[[TGBleManager sharedTGBleManager] age] withHeight:[[TGBleManager sharedTGBleManager] height] withWeight:[[TGBleManager sharedTGBleManager] weight]*10 withPath:@""];
    //    [[TGBleManager sharedTGBleManager] setStressOutputInterval:30];
    /*
     [[TGBleManager sharedTGBleManager] setHeartAgeOutputPoint:30];
     [[TGBleManager sharedTGBleManager] setHeartAgeRecordNumber:5];
     [[TGBleManager sharedTGBleManager] setHeartAgeParameters:@"Mary5" withAge:30 withPath:@""];
     [[TGBleManager sharedTGBleManager] setStressParameters:@"Mary5" withGender:true withAge:30 withHeight:170 withWeight:65 withPath:@""];
     [[TGBleManager sharedTGBleManager] setStressOutputInterval:30];
     */
    
    __LOG_METHOD_END;
}

-(void)doRealtimeOrPlaybackTask{
    __LOG_METHOD_START;

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    deviceName = [defaults stringForKey:@"DeviceName"];
    deviceID = [defaults stringForKey:@"DeviceID"];
    NSLog(@"NSUserDefaults deviceName = %@",deviceName);
    BOOL is_female = [defaults boolForKey:USER_IS_FEMALE];
    if ([CommonValues isPlayBackMode]) {
        
        tglibEKG = [[TGLibEKG alloc] init:50];
        [tglibEKG setDelegate:self];
        
        //    (void)[tglibEKG initEkgAnalysis:[NSDate date]];// just use the current time for demonstration
        
        if ([CommonValues isHistoryECGPlayBackMode]) {
            (void) [tglibEKG initEkgAnalysis:[NSDate date] sampleRate:256 settlingTime:0];
        }
        else{
            (void) [tglibEKG initEkgAnalysis:[NSDate date] sampleRate:256 settlingTime:2];
        }
        
        [tglibEKG setUserProfile:(int)[[TGBleManager sharedTGBleManager] age] withGender:(bool)is_female withWrist:(bool)[[TGBleManager sharedTGBleManager] isBandOnRight]]; // gender true = female,
        [tglibEKG setHRVOutputInterval:30];
        NSLog(@"age = %ld ,",(long)[[TGBleManager sharedTGBleManager] age]);
        if (is_female) {
            NSLog(@"is_female");
        }
        else{
            NSLog(@"male");
            
        }
        
        if (![[TGBleManager sharedTGBleManager] isBandOnRight]) {
            NSLog(@" is_band_right ");
        }
        else{
            
            NSLog(@" not band_right ");
        }
        NSLog(@" cardioPlot DemoMode ---- ");
        //画心电图
        timeCount = 60;
        dataCount = 0;
        //        fileURL =  [[NSBundle mainBundle] pathForResource:@"2013-01-30" ofType:@"plist"];
        fileURL = [NSString stringWithFormat:@"%@/TG_log/EKGData/%@",[CommonValues getDocumentPath],[CommonValues getSelectedEKG]];
        NSLog(@"fileURL = %@",fileURL);
        demoReplayFlag = YES;
        NSFileManager *fm = [[NSFileManager alloc]init];
        BOOL existed = [fm fileExistsAtPath:fileURL isDirectory:NO];
        NSString *contentStr = @"";
        if (existed) {
            NSLog(@"existed ==== ");
            contentStr = [NSString stringWithContentsOfFile:fileURL encoding:NSUTF8StringEncoding error:nil];
            dataRaw =   [contentStr componentsSeparatedByString:@"\n" ];
            NSLog(@"dataRaw length = %lu",(unsigned long)dataRaw.count);
        }
        else{
            NSLog(@" not existed ==== ");
        }
        rawCountInTimer = EKGFileRawStartLine;
        totalRawCount =  dataRaw.count;
        connectStateImage.image = [UIImage imageNamed:@"Connection2"];
        connectFlag = YES;
        if ([CommonValues isPlayBackMode]) {
            NSLog(@"scheduledTimerWithTimeInterval-----");
            rawCountInTimer = 0;
            addRaw2SDKTimer =  [NSTimer scheduledTimerWithTimeInterval:1.0/256*8 target:self selector:@selector(timerAddRaw2SDK) userInfo:nil repeats:YES];
        }
        
        
    }
    
    else{
        // REALTIME ECG Mode;
        
        NSInteger batterWarningNum = [defaults integerForKey:@"ShowBatteryWarning"];
        if (batterWarningNum != 1) {
            //suggest user it will spend battery
            if ([CommonValues iOSVersion] >= IOS8) {
                alertController = [UIAlertController alertControllerWithTitle:CARDIOPLOT_BATTERY_WARNING_TITLE message:CARDIOPLOT_BATTERY_WARNING_MSG preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                    [self batteryAlertNo];
                    
                }];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@" YES " style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self batteryAlertYes];
                }];
                [alertController addAction:cancelAction];
                [alertController addAction:okAction];
                [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
            }
            else{
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:CARDIOPLOT_BATTERY_WARNING_TITLE   message:CARDIOPLOT_BATTERY_WARNING_MSG  delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@" YES ", nil];
                alert.tag = batteryAlertTag;
                [alert show];
                
            }
            [defaults setInteger:1 forKey:@"ShowBatteryWarning"];
            [defaults synchronize];
        }
        else
        {
            
            [self doStartRTStuff];
            
        }
        
    }
    __LOG_METHOD_END;
}

-(void)initCardioPlotUI{
    __LOG_METHOD_START;
    myWholeView.frame = CGRectMake( 0, [CommonValues offsetHeight4DiffiOS],568, 320-20);
    
    //mood labels
    if (screenHeight == 568) {
        NSLog(@"cardio Plot  screenHeight = 568");
        
        UIFont *chilled_font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:28.0];
        chilledLabel.font = chilled_font;
        chilledLabel.textColor=[UIColor colorWithRed:189/255.0 green:66/255.0 blue:193/255.0 alpha:1];
        chilledLabel.text = @"‘Chilled’";
        
        wiredLabel.font =  chilled_font;
        wiredLabel.textColor =[UIColor colorWithRed:195/255.0 green:49/255.0 blue:49/255.0 alpha:1];
        wiredLabel.text = @"‘Wired’";
        
        UIFont *mood_font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:40.0];
        moodLabel.font = mood_font;
        moodLabel.textColor = [UIColor colorWithRed:123.0/255 green:122.0/255 blue:123.0/255 alpha:1];
    }else{
        
        NSLog(@"cardio Plot  screenHeight !!= 568");
        
        UIFont *chilled_font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:18.0];
        chilledLabel.font = chilled_font;
        chilledLabel.textColor=[UIColor colorWithRed:189/255.0 green:66/255.0 blue:193/255.0 alpha:1];
        chilledLabel.text = @"‘Chilled’";
        
        wiredLabel.font =  chilled_font;
        wiredLabel.textColor =[UIColor colorWithRed:195/255.0 green:49/255.0 blue:49/255.0 alpha:1];
        wiredLabel.text = @"‘Wired’";
        
        UIFont *mood_font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:36.0];
        moodLabel.font = mood_font;
        moodLabel.textColor = [UIColor colorWithRed:123.0/255 green:122.0/255 blue:123.0/255 alpha:1];
    }
    
    //
    
    
    UIFont *data_font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:45.0];
    
    UIFont *down_font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:25.0];
    countDownTv.font = down_font;
    
    hrLabel.font = data_font;
    rrIntervalLabel.font = data_font;
    rrIntervalLabel.textColor =  [UIColor colorWithRed:70.0/255 green:156.0/255 blue:169.0/255 alpha:1];
    hrvLabel.font = data_font;
    
    heartLevelLabel.font = data_font;
    
    //update UI
    
    hrvLabel.textColor=[UIColor colorWithRed:166.0/255.0 green:72.0/255.0 blue:72.0/255.0 alpha:1];
    rrIntervalLabel.textColor =  [UIColor colorWithRed:70.0/255 green:156.0/255 blue:169.0/255 alpha:1];
    
    hrLabel.textColor=[UIColor colorWithRed:166.0/255.0 green:72.0/255.0 blue:72.0/255.0 alpha:1];
    
    heartLevelLabel.textColor=[UIColor colorWithRed:240.0/255.0 green:143.0/255.0 blue:0.0/255.0 alpha:1];
    
    
    [self moodButtonControl];
    
    CGRect rect = myWholeView.frame;   ////  MIW 2015-02-27  -- previously uninitialized
    if ((int)screenHeight == 568) {
        ProcessView = [[UIImageView alloc]initWithFrame:CGRectMake(531, 152+35-20+3, 31, 0)];
    }
    else
    {
        ProcessView = [[UIImageView alloc]initWithFrame:CGRectMake(444, 152+35-20, 31, 0)];
        
    }
    ProcessView.image = [UIImage imageNamed:@"pulse_iphone_fg"];
    [self.view addSubview:ProcessView];
    
    ProcessView.frame = rect;
    
    __LOG_METHOD_END;
}

- (void)moodButtonControl{
    __LOG_METHOD_START;
    UIPanGestureRecognizer *pan=[[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handPan1:)];
    pan.minimumNumberOfTouches=1;
    pan.maximumNumberOfTouches=1;
    if (screenHeight == 568) {
        uiview=[[UIView alloc]initWithFrame:CGRectMake(90, 273-20, 37, 46)];
    }else{
        uiview=[[UIView alloc]initWithFrame:CGRectMake(45, 273-20, 37, 46)];
    }
    [myWholeView addSubview:uiview];
    
    img=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 37, 46)];
    img.image=[UIImage imageNamed:@"go_2"];
    img.userInteractionEnabled=YES;
    [uiview addSubview:img];
    
    but=[[UIButton alloc]initWithFrame:CGRectMake(6, 16.7, 24, 24)];
    [but.layer setCornerRadius:12.0];
    [uiview addSubview:but];
    
    UIImageView *light_img=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 37, 46)];
    light_img.image=[UIImage imageNamed:@"light"];
    [uiview addSubview:light_img];
    but.backgroundColor=[UIColor colorWithRed:255/255.0 green:0/255.0 blue:255/255.0 alpha:1];
    //Mood origin position
    if (screenHeight == 568) {
        uiview.center=CGPointMake(105+4.32*50, 297-20);
        float red=255*((50-45.0)/20);
        but.backgroundColor=[UIColor colorWithRed:red/255.0 green:255/255.0 blue:0/255.0 alpha:1];
    }
    
    else{
        uiview.center=CGPointMake(60+3.96*50, 295-20);
        but.backgroundColor=[UIColor colorWithRed:0/255.0 green:255/255.0 blue:0/255.0 alpha:1];
    }
    __LOG_METHOD_END;
}

- (void)releasUIRun{
    __LOG_METHOD_START;

    sleep(2);
    dispatch_async( dispatch_get_main_queue(), ^ {
        [self.view setUserInteractionEnabled:YES];
    });
    __LOG_METHOD_END;
}

- (void)changeColor{
    __LOG_METHOD_START;
    bool bb=true;
    while (changeColorLoopFlag) {
        if(colourThreadFlag){
            if (bb==true) {
                
                [self performSelectorOnMainThread:@selector(drawColor1) withObject:nil waitUntilDone:NO];
                bb=false;
            }else{
                
                [self performSelectorOnMainThread:@selector(drawColor2) withObject:nil waitUntilDone:NO];
                bb=true;
            }
        }
        usleep(500000);
        
    }
    //[NSThread exit];
    
    __LOG_METHOD_END;
}

- (void)drawColor1{
    __LOG_METHOD_START;
    [UIView beginAnimations:@"MoveIn" context:nil];
    [UIView setAnimationDuration:0.5];
    [but setAlpha:0.2];
    [UIView commitAnimations];
    __LOG_METHOD_END;
}
- (void)drawColor2{
    __LOG_METHOD_START;
    [UIView beginAnimations:@"MoveIn" context:nil];
    [UIView setAnimationDuration:0.5];
    [but setAlpha:1.0];
    [UIView commitAnimations];
    __LOG_METHOD_END;
}

- (void)handPan1:(UIPanGestureRecognizer *)paramSender{
    __LOG_METHOD_START;
    CGPoint point = [paramSender locationInView:self.view];
    [cardioPlot gestureAccording2Point:point uiView:uiview btn:but];
    __LOG_METHOD_END;
}
- (void)dataReceived:(NSDictionary *)data{
    __LOG_METHOD_START;
    //  NSLog(@"dataReceived =======");
    
    
    if([data valueForKey:@"CardioZoneHeartRate"])
    {
        int cardioZoneHeartRate = [[data valueForKey:@"CardioZoneHeartRate"] intValue];
        NSLog(@"cardioZoneHeartRate  ======  %d",cardioZoneHeartRate);
        
        
        if (ecgLogStr != NULL) {
            //            [ecgLogStr appendFormat:@"HR : %d\n",cardioZoneHeartRate];
            [hrArr addObject:[NSNumber numberWithInt:cardioZoneHeartRate]];
        }
        dispatch_async( dispatch_get_main_queue(), ^ {
            hrLabel.text = [NSString stringWithFormat:@"%d",cardioZoneHeartRate];
        }
                       );
        
        
    }
    
    
    if ([demoModeStr isEqualToString:@"1"]&&false) {
        
        if([data valueForKey:@"CardioZoneHRV"]) {
            int  cardioZoneHRV = [[data valueForKey:@"CardioZoneHRV"] intValue];
            NSLog(@"cardioZoneHRV  ======  %d",cardioZoneHRV);
            dispatch_async( dispatch_get_main_queue(), ^ {
                
                hrvLabel.text = [NSString stringWithFormat:@"%d",cardioZoneHRV];
                
            });
            
        }
    }
    
    if([data valueForKey:@"rrInt"]) {
        int  rrInt = [[data valueForKey:@"rrInt"] intValue];
        [self pulseWhenR2RComes];
        NSLog(@"rrint ====  %d",rrInt);
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        BOOL soundFlag = [userDefault boolForKey:BEEP_SOUND];
        if (soundFlag) {
            [audio play];
        }
        
        
        seconds++;
        
        if (seconds == 30) {
            //        NSLog(@"if seconds = %d",seconds);
            dispatch_async( dispatch_get_main_queue(), ^ {
                hrvLabel.hidden = NO;
                countDownTv.hidden = YES;
                countDownIv.hidden = YES;
                hrvLabel.text = [NSString stringWithFormat:@"%d",latestHrv];
            });
            
        }
        else{
            //         NSLog(@"else seconds = %d",seconds);
            
            if (seconds < 30) {
                
                dispatch_async( dispatch_get_main_queue(), ^ {
                    
                    
                    [UIView beginAnimations:@"ChangeNum" context:nil];
                    [UIView setAnimationDuration:0.5];
                    
                    countDownTv.text = [NSString stringWithFormat:@"%d",30-seconds];
                    [countDownTv setAlpha:0.0];
                    [UIView commitAnimations];
                    
                    [UIView beginAnimations:@"ChangeNum2" context:nil];
                    [UIView setAnimationDuration:0.5];
                    
                    countDownTv.text = [NSString stringWithFormat:@"%d",30-seconds];
                    [countDownTv setAlpha:1.0];
                    [UIView commitAnimations];
                });
            }
            
            else if((seconds-30)%15 == 0){
                dispatch_async( dispatch_get_main_queue(), ^ {
                    hrvLabel.hidden = NO;
                    countDownTv.hidden = YES;
                    countDownIv.hidden = YES;
                    hrvLabel.text = [NSString stringWithFormat:@"%d",latestHrv];
                    NSLog(@"displayed latestHrv hrv = %d",latestHrv);
                    //                [NSWriter Write:[NSString stringWithFormat:@"displayed latestHrv hrv = %d",latestHrv]];
                    
                });
                
            }
            else{
                
                //            hrvLabel.hidden = YES;
                //            countDownTv.hidden = NO;
                //            countDownIv.hidden = NO;
            }
            
        }
        
        
        
        dispatch_async( dispatch_get_main_queue(), ^ {
            
            rrIntervalLabel.text = [NSString stringWithFormat:@"%d",rrInt];
            
        });
        
    }
    
    if([data valueForKey:dicEKGhrv]) {
        int  cardioZoneHRV = [[data valueForKey:@"CardioZoneHRV"] intValue];
        NSLog(@" stuff cardioZoneHRV  ======  %d",cardioZoneHRV);
        
        if (ecgLogStr != NULL) {
            
            //            [ecgLogStr appendFormat:@"HRV : %d\n",cardioZoneHRV];
            [hrvArr addObject:[NSNumber numberWithInt:cardioZoneHRV]];
        }
        latestHrv = cardioZoneHRV;
        dispatch_async( dispatch_get_main_queue(), ^ {
            
            hrvLabel.text = [NSString stringWithFormat:@"%d",cardioZoneHRV];
            
        });
    }
    
    if([data valueForKey:@"Mood"]) {
        
        if (!colourThread.isExecuting) {
            [colourThread start];
            NSLog(@"colourThread start");
        }
        
        colourThreadFlag = YES;
        int mood = [[data valueForKey:@"Mood"] intValue];
        //        NSLog(@"Mood   ======  %d",mood);
        [self moodBtnMovingControl:mood];
    }
    
    
    if([data valueForKey:dicEKGtrainingZone]) {
        
        int trainingZone =  [[data valueForKey:dicEKGtrainingZone] intValue];
        NSLog(@"dicEKGtrainingZone = %d",trainingZone);
        if (trainingZone == 0) {
            dispatch_async( dispatch_get_main_queue(), ^ {
                
                // hrvLabel.text = [NSString stringWithFormat:@"%d",trainingZone];
                trainZoneImg.image = nil;
                
            });
            
        }
        
        else if (trainingZone == 1) {
            dispatch_async( dispatch_get_main_queue(), ^ {
                
                // hrvLabel.text = [NSString stringWithFormat:@"%d",trainingZone];
                trainZoneImg.image = [UIImage imageNamed:@"fat_burn_ip5"];
                
            });
            
        }
        
        else if (trainingZone == 2){
            
            
            dispatch_async( dispatch_get_main_queue(), ^ {
                
                trainZoneImg.image = [UIImage imageNamed:@"burn_warning_ip5"];
                
            });
            
        }
        
    }
    
    
    if([data valueForKey:dicEKGsmoothedWave]) {
        int smoothedWave =  [[data valueForKey:dicEKGsmoothedWave] intValue];
        
        
        if (!firstReceiveFilteredDataFlag) {
            firstReceiveFilteredDataFlag = YES;
            dispatch_async( dispatch_get_main_queue(), ^ {
                [SVProgressHUD dismiss];
            });
        }
        
        
        if (!firstOnSensorFlag) {
            firstOnSensorFlag = YES;
            //        [self startTimer];
        }
        
        
        keepFlag = YES;
        recordedLastTime = CFAbsoluteTimeGetCurrent();
        if (countSum % 256 == 0) {
            CFAbsoluteTime tempTime = CFAbsoluteTimeGetCurrent();
            //   CFAbsoluteTime XXX =      tempTime - end;
            //    NSLog(@"interval time  = %f ",XXX);
            end =  tempTime;
            
        }
        
        lastValueTime = CFAbsoluteTimeGetCurrent();
        
        onSensorFlag = true;
        firstSensorOffFlag = NO;
        //        NSLog(@"smoothedWave = %d",smoothedWave);
        [ekgLineView addValue:smoothedWave];
    }
    
    if([data valueForKey:dicEKGheartLevel]) {
        int heartLevel =  [[data valueForKey:dicEKGheartLevel] intValue];
        NSLog(@"dicEKGheartLevel = %d",heartLevel);
        dispatch_async( dispatch_get_main_queue(), ^ {
            heartLevelLabel.text = [NSString stringWithFormat:@"%d%%",heartLevel];
            
        });
    }
    
    if([data valueForKey:dicEKGstress]) {
        stress = [[data valueForKey:dicEKGstress] intValue];
        NSLog(@"cardioPlot stress ====== %d", stress);
    }
    if([data valueForKey:dicEKGheartAge]) {
        int heartAge = [[data valueForKey:dicEKGheartAge] intValue];
        NSLog(@"cardioPlot heartAge ====== %d", heartAge);
    }
    
    
    __LOG_METHOD_END;
}


- (void)moodBtnMovingControl:(int) mood{
    __LOG_METHOD_START;
    if (screenHeight == 568) {
        dispatch_async( dispatch_get_main_queue(), ^ {
            
            [UIView beginAnimations:@"MoveIn" context:nil];
            [UIView setAnimationDuration:1];
            uiview.center=CGPointMake(105+4.32*mood, 297-20);
            [UIView commitAnimations];
            [cardioPlot controlMoodChangeForIphone5S :mood btn:but];
            
        });
    }else{
        
        dispatch_async( dispatch_get_main_queue(), ^ {
            
            //  uiview.center=CGPointMake(60+3.96*mood, 289);
            [UIView beginAnimations:@"MoveIn" context:nil];
            [UIView setAnimationDuration:1];
            uiview.center=CGPointMake(60+3.96*mood, 295-20);
            [UIView commitAnimations];
            [cardioPlot controlMoodChangeForIphone4S:mood btn:but];
        });
    }
    
    __LOG_METHOD_END;
}

- (void)viewWillDisappear:(BOOL)animated{
    __LOG_METHOD_START;
    colourThreadFlag = NO;
    demoReplayFlag = NO;
    startFlag = NO;
    isConnectFlag = NO;
    changeColorLoopFlag = NO;
    keepMonitorLoppFlag = NO;
    
    if (colourThread) {
        [colourThread cancel] ;
    }
    
    if (proThread) {
        NSLog(@"proThread cancel-----");
        [proThread cancel];
    }
    
    if (keepThread) {
        NSLog(@"keepThread cancel-----");
        [keepThread cancel];
    }
    
    
    [ekgLineView cleardata] ;
    ekgLineView.freshEKGFlag = NO;
    [ekgLineView removeFromSuperview];
    [CommonValues setPlayBackMode:NO];

    [ super viewWillDisappear: animated ];

    __LOG_METHOD_END_DESTROYED;

}


- (void)didReceiveMemoryWarning
{
    __LOG_METHOD_START;
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    __LOG_METHOD_END;
}

-(NSUInteger)supportedInterfaceOrientations{
    __LOG_METHOD_START;
    return UIInterfaceOrientationMaskLandscapeRight;
}

- (BOOL)shouldAutorotate
{
    __LOG_METHOD_START;
    return NO;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    __LOG_METHOD_START;
    return UIInterfaceOrientationLandscapeRight;
}


-(void)drawStringNum:(NSString *)str{
    __LOG_METHOD_START;
    int tempShort=[str intValue];
    [ekgLineView addValue:tempShort];
    __LOG_METHOD_END;
}


-(void)PulseUpandDownAnimation{
    __LOG_METHOD_START;

    [UIView animateWithDuration:1000 animations:^{
        CGRect rect;
        if ((int)screenHeight == 568) {
            rect=CGRectMake(531, 53-20+3+[CommonValues offsetHeight4DiffiOS], 31, 152);
        }
        else
        {
            rect=CGRectMake(444, 53-20+[CommonValues offsetHeight4DiffiOS], 31, 152);
        }
        [UIView beginAnimations:@"MoveUp" context:nil];
        [UIView setAnimationDuration:0.02];
        ProcessView.frame = rect;
        [UIView commitAnimations];
        
    } completion:^(BOOL finished) {
        CGRect rect;
        if ((int)screenHeight == 568) {
            rect=CGRectMake(531, 152+53-5-20+3+[CommonValues offsetHeight4DiffiOS], 31, 0);
            
        }
        else
        {
            rect=CGRectMake(444, 152+53-5-20+[CommonValues offsetHeight4DiffiOS], 31, 0);
        }
        
        [UIView beginAnimations:@"MoveDown" context:nil];
        [UIView setAnimationDuration:0.4];
        ProcessView.frame = rect;
        [UIView commitAnimations];
        
    }];
    
    __LOG_METHOD_END;
}

- (void)pulseWhenR2RComes{
    
    __LOG_METHOD_START;
    [self performSelectorOnMainThread:@selector(PulseUpandDownAnimation) withObject:nil waitUntilDone:YES];
    
    __LOG_METHOD_END;

}



- (IBAction)back:(id)sender {
    __LOG_METHOD_START;
    [self.view setUserInteractionEnabled:NO];
    [self releaseTimer];
    hrThreadFlag = NO;
    peakThreadFlag = NO;
    hrvThreadFlag = NO;
    rrIntervalThreadFlag = NO;
    startFlag = NO;
    
    if ([addRaw2SDKTimer isValid]) {
        [addRaw2SDKTimer invalidate];
        NSLog(@"addRaw2SDKTimer invalidate----");
    }
    
    if (![CommonValues isPlayBackMode]) {
        
        NSLog(@"call tryStopRT------");
        [[TGBleManager sharedTGBleManager] tryStopRT];
        [SVProgressHUD showWithStatus:@" Stop real time ECG "];
        //dismissView in didDisoconnet call back method
        stopECGTimeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(doStuffSDKNotResponceStopECG) userInfo:nil repeats:NO];
        
    }
    
    else{
        [CommonValues setPlayBackMode:NO];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    __LOG_METHOD_END;
}


- (void)doStuffSDKNotResponceStopECG{
    __LOG_METHOD_START;
    // SDK  ekgStop call back  didn't excute.
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
    NSLog(@"SDK  ekgStop call back  didn't excute. XXXXXXXX");
    [CommonValues setStopRTNormalFlag:YES];
    
    usleep(1000);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self dismissViewControllerAnimated:YES completion:nil];
    });
    [CommonValues setHasTaskFlag:NO];
    
    __LOG_METHOD_END;
}

- (void)timerAddRaw2SDK{
    __LOG_METHOD_START;
    if (rawCountInTimer+8 < totalRawCount) {
        
        for (int i = 0 ; i < 8; i++) {
            int tempRaw = [[dataRaw objectAtIndex:(rawCountInTimer+i)]intValue];
            [tglibEKG requestEkgAnalysis_s:tempRaw];
            
            
        }
        rawCountInTimer += 8;
    }
    else{
        
        if ([addRaw2SDKTimer isValid]) {
            [addRaw2SDKTimer invalidate];
            NSLog(@"addRaw2SDKTimer invalidate---");
            //            rawCountInTimer = 256*rawCountInTimer;
        }
        // finish play back this time
        ekgLineView.freshEKGFlag = NO;
        
        [self releaseTimer];
        hrThreadFlag = NO;
        peakThreadFlag = NO;
        hrvThreadFlag = NO;
        rrIntervalThreadFlag = NO;
        startFlag = NO;
        
        
    }
    
    //    if (rawCountInTimer % 256 == 0) {
    //        NSLog(@"play back 256 data-----");
    //    }
    
    __LOG_METHOD_END;
}

- (void)dataTimeoutControl{
    __LOG_METHOD_START;
    onSensorFlag = NO;
    NSThread *draw0RawThread = [[NSThread alloc]initWithTarget:self selector:@selector(draw0RowRun) object:nil];
    [draw0RawThread start];
    __LOG_METHOD_END_DESTROYED;
}

- (void)draw0RowRun{
    __LOG_METHOD_START;

    while (!onSensorFlag) {
        
        NSString *temNum = @"0";
        [self performSelectorOnMainThread:@selector(drawStringNum:) withObject:temNum waitUntilDone:NO];
        usleep(1953*2);
    }
    __LOG_METHOD_END;
}


- (void)keepMonitoringBackground{
    
    __LOG_METHOD_START;
    while (keepMonitorLoppFlag) {
        
        if (isConnectFlag) {
            CFAbsoluteTime  currentTime = CFAbsoluteTimeGetCurrent();
            //NSLog(@"currentTime = %f",currentTime);
            //
            if (currentTime - recordedLastTime >= 0.8) {
                
                if (!firstSensorOffFlag) {
                    firstSensorOffFlag = YES;
                }
                
                else{
                }
                // onSensorFlag = NO;
                colourThreadFlag = NO;
                enterRawDataFlag = NO;
                sampleCount = 0;
                aNewTimeFlag = NO;
                // NSLog(@" ------ >= 0.8");
                [self performSelectorOnMainThread:@selector(refreshEkgLineView) withObject:nil waitUntilDone:NO];
                hrCounter = 0;
                hrSum = 0;
                [tenHRArr removeAllObjects];
                firstOnSensorFlag = NO;
            }
            
            usleep(1953);
            
            
        }
    }
    
    
    __LOG_METHOD_END;

}


- (void)refreshEkgLineView{
    __LOG_METHOD_START;
    //  NSLog(@"refreshEkgLineView---");
    //    guide_image.hidden = NO;
    [ekgLineView addValue:0];
    
    __LOG_METHOD_END;
}


- (void) batteryLevel:(int)level {
    __LOG_METHOD_START;
    NSLog(@">>>>>>>-----New Battery Level: percent complete: %d", level);
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    appDelegate.batteryLevel = level;
    [FileOperation save2UserDefaultsWithInteger:level withKey:LATEST_BATTERY];
    [CommonValues updateConnectStateImg:connectStateImage withLevel:level];
    __LOG_METHOD_END;
}



- (void)ekgStarting:(NSDate *)time sampleRate:(int) sr realTime:(bool) rt comment:(NSString*) comment{
    __LOG_METHOD_START;
    NSLog(@">>>>>>>-----START EKG RECORDING: %d Hz", sr);
    NSLog(@"cardioPlot  ekgStarting-----");
    
    startECGTime = time;
    sampleRate = sr;
    ecgLogStr =   [[NSMutableString alloc]init];
    NSString *currentDateStr = [[CommonValues timeDateFormatter:DateFormatterTimeAllUnderLineGMT] stringFromDate:time];
    NSLog(@"%@",currentDateStr);
    NSString* ecgFileName1 = [currentDateStr stringByAppendingString:@".txt"];
    NSString* tempTS = [NSString stringWithFormat:@"%@\n",ecgFileName1];
    // [fwrite writeData:[tempTS dataUsingEncoding:NSUTF8StringEncoding]];
    [ecgLogStr appendString:tempTS] ;
    
    [ecgLogStr appendString:comment];
    
    //    [self createECGFile];
    
    ekg2Path = [cardioPlot ekg2Path];
    startFlag = YES;
    
    sampleCount = 0;
    firstOnSensorFlag = NO;
    firstSensorOffFlag = NO;
    __LOG_METHOD_END;
}


- (void)ekgStop:(TGekgResult) result
        finalHR:(int)finalHR{
    
    __LOG_METHOD_START;
    //final_hrv
    int final_hrv =  [[TGBleManager sharedTGBleManager] computeHRVNow];
    NSLog(@"final_hrv = %d",final_hrv);
    
    if (sampleCount != 0) NSLog(@">>>>>>>-----received EKG samples %d", sampleCount);
    NSLog(@">>>>>>>-----STOP EKG RECORDING");
    
    [self releaseTimer];
    
    
    if (finalHR == 0) {
        if (startECGTime == NULL) {
            //没有ekgstart 直接ekgStop
        }
        else{
            //if是realtime ECG,插入数据库，以后play back时候区分是realtimeECG 还是history ECG
            //syncing ECG
            int avgHrv = [HeartData avgHrvWithHrvArr:hrvArr];
            int avgHr = [HeartData avgHrWithHrArr:hrArr];
            
            NSLog(@"startECGTime = %@,final hr = %d , avg_hr = %d ,avg_hrv = %d",startECGTime,finalHR,avgHr,avgHrv);
            
            if (ecgLogStr != NULL) {
                [ecgLogStr appendFormat:@"HR : %d\n",avgHr];
            }
            [CommonValues insertHeartData2DB: startECGTime
                                     finalHR: finalHR
                                      avgHR : avgHr
                                     avgHRV : avgHrv
                                   finalHRV : final_hrv
                                     stress : stress
                                   database : db
            ];
            [hrArr removeAllObjects];
            [hrvArr removeAllObjects];
        }
        
    }
    
    
    
    if (ecgLogStr != NULL) {
        [ecgLogStr appendFormat:@"HRV : %d\n",final_hrv];
    }
    
    // to avoid SDK bug
    if ([stopECGTimeOutTimer isValid]) {
        [stopECGTimeOutTimer invalidate];
        NSLog(@"stopECGTimeOutTimer invalidate----");
        
    }
    
    
    //保存这次的realtime的ECG数据
    if (ecgLogStr.length > 0) {
        [FileOperation saveEcg2DocumentWithFilePath:ekg2Path withFileContent:ecgLogStr];
    }
    sampleCount = 0;
    firstOnSensorFlag = NO;
    
    NSString * message;
    NSString * title;
    
    if (result == TGekgTerminatedNormally) {
        message = @"EKG Recording Ended Normally";
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        
        [CommonValues setStopRTNormalFlag:YES];
        
        usleep(1000);
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self dismissViewControllerAnimated:YES completion:nil];
        });
    }
    else if (result == TGekgTerminatedNoData) {
        title = TerminatedNoDataTitle;
        message = TerminatedNoDataMsg;
    }
    else if (result == TGekgTerminatedDataStopped) {
        title = TerminatedDataStoppedTitle;
        message = TerminatedDataStoppedMsg;
    }
    else if (result == TGekgTerminatedLostConnection) {
        
        //deal with it at didDisconnect call backs; to avoid special case
        
        
        title = TerminatedLostConnectionTitle;
        message = TerminatedLostConnectionMsg;
        
    }
    else {
        message = [NSString stringWithFormat:@"Unexpected result, code: %d", (int) result];
    }
    
    //    if (result == TGekgTerminatedLostConnection || result == TGekgTerminatedNormally) {
    //        // LostConnection situation is already dealed with in bleLostConnect callbacks
    //    }
    //    else{
    NSLog(@"CardioPlot message =  %@",message);
    [CommonValues setHasTaskFlag:NO];
    
    if (result!= TGekgTerminatedNormally) {
        
        [CommonValues setStopRTNormalFlag:NO];
        
        // ekg already stop, can disconnect
        if ([CommonValues isBackgroundFlag]) {
            NSLog(@"realtime ecg finish，且在后台，应断开连接");
            NSLog(@"call tryDisconnect ------");
            [[TGBleManager sharedTGBleManager] tryDisconnect];
        }
        //非正常stop  show alert
        
        
        if (result != TGekgTerminatedLostConnection) {
            UIAlertView *alert;
            //排除TGekgTerminatedLostConnection的情况，这个已在disconnect call back 里处理
            if ([CommonValues iOSVersion] >= IOS8) {
                alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@" OK " style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                    [self stopRTAlertOK];
                    
                }];
                [alertController addAction:okAction];
                
            }
            else{
                alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:nil otherButtonTitles:@" OK ", nil];
                alert.tag = stopRTAlertTag;
            }
            
            sleep(1);
            [self performSelectorOnMainThread:@selector(showEKGStopView:) withObject:alert waitUntilDone:YES];
        }
        
        
    }
    
    __LOG_METHOD_END_DESTROYED;
}

-(void)runThe : (UIAlertView *)alert{
    __LOG_METHOD_START;
    [self performSelectorOnMainThread:@selector(showEKGStopView:) withObject:alert waitUntilDone:YES];
    __LOG_METHOD_END_DESTROYED;
}

-(void)showEKGStopView : (UIAlertView *)alert{
    __LOG_METHOD_START;
    if ([CommonValues iOSVersion] >= IOS8) {
        [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
    }
    else{
        [alert show];
    }
    __LOG_METHOD_END;
}


- (void)ekgSample:(int)sample{
    __LOG_METHOD_START;
    if (!firstReceiveRawFlag) {
        firstReceiveRawFlag = YES;
        dispatch_async( dispatch_get_main_queue(), ^ {
            [SVProgressHUD showWithStatus:FILTER_DATA_MSG];
        });
    }
    
    [ecgLogStr appendString:[NSString stringWithFormat:@"RAW : %d\n",sample]] ;
    if (sampleCount != 0 && (sampleCount % 512) == 0) NSLog(@">>>>>>>-----received EKG samples %d", sampleCount);
    sampleCount++;
    __LOG_METHOD_END;
};

- (void) bleDidConnect {
    
    __LOG_METHOD_START;
    connectFlag = YES;
    NSLog(@"call tryBond-------");
    [ [TGBleManager sharedTGBleManager] tryBond];
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    int level = appDelegate.batteryLevel;
    
    [CommonValues updateConnectStateImg:connectStateImage withLevel:level];
    
    __LOG_METHOD_END;
}



- (void)startRTRun{
    __LOG_METHOD_START;
    [ [TGBleManager sharedTGBleManager] tryStartRT];
    [NSThread exit];
    __LOG_METHOD_END_DESTROYED;
}

- (void)delayToDo{
    __LOG_METHOD_START;
    //change to UI when connected
    
    isConnectFlag = YES;
    [disconnectedAlert dismissWithClickedButtonIndex:0 animated:NO];
    [connectedAlert show];
    
    
    __LOG_METHOD_END;
}

- (void)noConnectionAlertOK{
    __LOG_METHOD_START;
    [self dismissViewControllerAnimated:YES completion:nil];
    __LOG_METHOD_END_DESTROYED;
}

- (void)batteryAlertNo{
    __LOG_METHOD_START;
    [self dismissViewControllerAnimated:YES completion:nil];
    
    __LOG_METHOD_END_DESTROYED;
}
- (void)batteryAlertYes{
    __LOG_METHOD_START;
    [self doStartRTStuff];
    __LOG_METHOD_END_DESTROYED;
}

- (void)lostConnectAlertCancel{
    __LOG_METHOD_START;
    [self dismissViewControllerAnimated:YES completion:nil];
    __LOG_METHOD_END_DESTROYED;
}
- (void)lostConnectAlertOK{
    __LOG_METHOD_START;
    [self dismissViewControllerAnimated:YES completion:nil];
    __LOG_METHOD_END_DESTROYED;
}
- (void)stopRTAlertOK{
    __LOG_METHOD_START;
    [SVProgressHUD dismiss];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    __LOG_METHOD_END_DESTROYED;
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    __LOG_METHOD_START;
    NSLog(@"buttonIndex:%ld", (long)buttonIndex);
    
    switch (alertView.tag) {
        case noConnectionAlertTag:{
            if (buttonIndex == 0) {
                [self noConnectionAlertOK];
            }
            else{
                
            }
            
        }
            
            break;
            
            
        case batteryAlertTag:
        {
            if (buttonIndex == 0) {
                [self batteryAlertNo];
            }
            else if(buttonIndex == 1){
                [self batteryAlertYes];
            }
            
        }
            break;
        case lostConnectAlertTag:
            if (buttonIndex == 0) {
                [self lostConnectAlertCancel];
            }
            else if (buttonIndex == 1){
                [self lostConnectAlertOK];
                
            }
            
            break;
        case stopRTAlertTag:
            [self stopRTAlertOK];
            break;
            
    }
    
    
    
    __LOG_METHOD_END;
}




- (void) bleDidDisconnect {
    __LOG_METHOD_START;
    isConnectFlag = NO;
    connectFlag = NO;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (![CommonValues isPlayBackMode]) {
            connectStateImage.hidden = NO ;
            connectStateImage.image = [UIImage imageNamed:@"disconnect"];
        }
        [self.view setUserInteractionEnabled:YES];
    });
    
    
    if ([CommonValues getStopRTNormalFlag]) {
        [CommonValues setStopRTNormalFlag:NO];
        
        NSLog(@"[TGBleManager sharedTGBleManager].status = %lu",(unsigned long)[TGBleManager sharedTGBleManager].status);
        
        //    dispatch_async(dispatch_get_main_queue(), ^{
        //        [SVProgressHUD dismiss];
        //        [self dismissViewControllerAnimated:YES completion:nil];
        //    });
        
        
        NSString *title;
        NSString *message;
        
        title = TerminatedLostConnectionTitle;
        message = TerminatedLostConnectionMsg;
        //            message = @"Please back to dashboard and enter Realtime ECG again";
        if ([CommonValues iOSVersion] >= IOS8) {
            alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@" OK " style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                [self stopRTAlertOK];
            }];
            [alertController addAction:okAction];
            [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:nil otherButtonTitles:@" OK ", nil];
            alert.tag = stopRTAlertTag;
            dispatch_async(dispatch_get_main_queue(), ^{
                [alert show];
            });
        }
        
        
    }
    
    __LOG_METHOD_END_DESTROYED;
}


- (void) bleLostConnect {
    
    __LOG_METHOD_START;
    isConnectFlag = NO;
    connectFlag = NO;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (![CommonValues isPlayBackMode]) {
            connectStateImage.hidden = NO ;
            connectStateImage.image = [UIImage imageNamed:@"disconnect"];
        }
    });
    
    
    if (![CommonValues isPlayBackMode]) {
        //play back mode ,no need to show alert view.
        if ([CommonValues iOSVersion] >= IOS8) {
            alertController = [UIAlertController alertControllerWithTitle:LostConnectTitle message:LostConnectMsg preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                [self lostConnectAlertOK];
                
            }];
            [alertController addAction:okAction];
            [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
            
        }
        else{
            UIAlertView *alert_lostConnect = [[UIAlertView alloc]initWithTitle:LostConnectTitle message:LostConnectMsg delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            alert_lostConnect.tag = lostConnectAlertTag;
            
            dispatch_async( dispatch_get_main_queue(), ^ {
                //                [self.view setUserInteractionEnabled:YES];
                //                [SVProgressHUD dismiss];
                [alert_lostConnect show];
            });
        }
    }
    
    __LOG_METHOD_END_DESTROYED;
}

- (void) bleDidAbortConnect {
    //
    // TODO: add code to turn off the Connection animation and
    // allow user to press the Connect button again
    //
    
    __LOG_METHOD_START;
    __LOG_METHOD_END;

}

- (void) bleDidBond:(TGbondResult)result {
    __LOG_METHOD_START;
    NSString * message;
    NSString * title ;
    if (result == TGbondResultTokenAccepted) {
        message = @"Bond Succesful";
    }
    else if (result == TGbondResultTokenReleased) {
        title = TokenReleasedTitle;
        message = TokenReleasedMsg;
    }
    else if (result == TGbondResultErrorBondedNoMatch) {
        title = CommunicationErrorTitle;
        message = BondedNoMatchMsg;
    }
    else if (result == TGbondResultErrorBadTokenFormat) {
        title = BondRejectedTitle;
        message = BadTokenFormatMsg;
    }
    else if (result == TGbondResultErrorTimeOut) {
        title = TimeOutTitle;
        message = TimeOutMsg;
    }
    
    else if (result == TGbondResultErrorNoConnection) {
        title = NoConnectionTitle;
        message = NoConnectionMsg;
    }
    
    else if (result == TGbondResultErrorReadTimeOut) {
        title = ReadTimeOutTitle;
        message = ReadTimeOutMsg;
    }
    
    
    else if (result == TGbondResultErrorReadBackTimeOut) {
        title = ReadBackTimeOutTitle;
        message = ReadBackTimeOutMsg;
    }
    
    else if (result == TGbondResultErrorWriteFail) {
        title = WriteFailTitle;
        message = WriteFailMsg;
    }
    
    
    else if (result == TGbondResultErrorTargetIsAlreadyBonded) {
        title = BondRejectedTitle;
        message = TargetIsAlreadyBondedMsg;
    }
    
    
    else if (result == TGbondAppErrorNoPotentialBondDelegate) {
        title  = BondAppErrorTitle;
        message = NoPotentialBondDelegateMsg;
    }
    
    
    else if (result == TGbondResultErrorTargetHasWrongSN) {
        title  = CommunicationErrorTitle;
        message = TargetHasWrongSNMSg;
    }
    
    else if (result == TGbondResultErrorPairingRejected) {
        title  = PairingErrorTitle;
        message = PairingRejectedMsg;
    }
    
    
    else if (result == TGbondResultErrorSecurityMismatch) {
        title  = PairingErrorTitle;
        message = SecurityMismatchMsg;
    }
    
    else {
        title  = UnkonwnBondErrorTitle;
        message = [NSString stringWithFormat:@"Bonding Error, code: %d", (int) result];
    }
    NSLog(@"BONDIING RESULT Code  %lu",(unsigned long)result);
    NSLog(@"BONDIING RESULT msg  %@",message);
    dispatch_async( dispatch_get_main_queue(), ^ {
        if (result != TGbondResultTokenAccepted) {
            
            if ([CommonValues iOSVersion] >= IOS8) {
                alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@" OK " style:UIAlertActionStyleCancel handler:nil];
                [alertController addAction:okAction];
                [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
            }
            else{
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:@" OK ", nil];
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [alert show];
                });
                
            }
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self delayToDo];
            });
            NSLog(@"call tryStartRT----");
            [[TGBleManager sharedTGBleManager] tryStartRT];
            
        }
    });
    
    __LOG_METHOD_END;
}

- (void) exceptionMessage:(TGBleExceptionEvent)eventType {
    __LOG_METHOD_START;
    NSString * message;
    
    if (eventType == TGBleHistoryCorruptErased) {
        message = @"Flash Memory Corrupted, Will Be erased, suspect battery has been exhausted";
    }
    else if (eventType == TGBleConfigurationModeCanNotBeChanged) {
        message = @"Exception Message - Ble Connection Mode CAN NOT be changed";
    }
    else if (eventType == TGBleUserAgeHasBecomeNegative_BirthDateIsNOTcorrect) {
        message = @"User Age has become negative, correct the birth date";
    }
    else if (eventType == TGBleUserAgeIsTooLarge_BirthDateIsNOTcorrect) {
        message = @"User Age is TOO OLD, check the birth date";
    }
    else if (eventType == TGBleUserBirthDateRejected_AgeOutOfRange) {
        message = @"User Age is out of RANGE, corret the birth date";
    }
    else if (eventType == TGBleFailedOtherOperationInProgress) {
        message = @"Exception Message - Another Operation is Already in Progress";
    }
    else if (eventType == TGBleFailedSecurityNotInplace) {
        message = @"Exception Message - Security is NOT In Place";
    }
    else if (eventType == TGBleTestingEvent) {
        message = @"Exception Message - This is only a Test";
    }
    else if (eventType == TGBleReInitializedNeedAlarmAndGoalCheck) {
        message = @"Exception Message - Band lost power or was MFG reset, check your alarm and goal settings";
    }
    else if (eventType == TGBleStepGoalRejected_OutOfRange) {
        message = @"Exception Message - Step Goal Settings rejected -- OUT OF RANGE";
    }
    else if (eventType == TGBleCurrentCountRequestTimedOut) {
        message = @"Exception Message - Can Not get Current Count values\nBand not responding";
    }
    else if (eventType == TGBleConnectFailedSuspectKeyMismatch) {
        message = @"Exception Message - Band appears to be paired, possible encryption key mismatch, hold side button for 10 seconds to reboot";
    }
    else if (eventType == TGBleNonRepeatingAlarmMayBeStaleCheckAlarmConfiguration) {
        message = @"Exception Message - A Non-repeating Alarm may be stale, check your alarm settings";
    }
    else {
        message = [NSString stringWithFormat:@"Exception Detected, code: %lu", (unsigned long)eventType];
    }
    
    //    NSLog(@"message = %@",message);
    
    NSLog(@"exceptionMessage =  %@",message);
    
    __LOG_METHOD_END;
}




-(void)showAlertFun{
    __LOG_METHOD_START;

    if ([CommonValues iOSVersion] >= IOS8) {
        alertController = [UIAlertController alertControllerWithTitle:@"Successfully connected" message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@" OK " style:UIAlertActionStyleCancel handler:nil];
        [alertController addAction:okAction];
        [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
        
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Successfully connected" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@" OK ", nil];
        
        [alert show];
    }
    
    
    __LOG_METHOD_END;
}



//start timer 开始倒计时
-(void)startTimer{
    __LOG_METHOD_START;
    if ([hrvTimer isValid]) {
        [hrvTimer invalidate];
    }
    
    hrvTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerFireMethod:) userInfo:nil repeats:YES];
    __LOG_METHOD_END;
}

//stop timer 停止倒计时
- (void)releaseTimer {
    __LOG_METHOD_START;
    if (hrvTimer) {
        if ([hrvTimer respondsToSelector:@selector(isValid)]) {
            if ([hrvTimer isValid]) {
                [hrvTimer invalidate];
                NSLog(@"hrvTimer invalidate----");
                
            }
        }
    }
    
    
    hrvLabel.hidden = YES;
    countDownTv.hidden = NO;
    countDownIv.hidden = NO;
    seconds = 0;
    countDownTv.text = [NSString stringWithFormat:@"%d",30-seconds];
    [countDownTv setAlpha:1.0];
    
    hrvLabel.hidden = NO;
    countDownTv.hidden = YES;
    countDownIv.hidden = YES;
    
    __LOG_METHOD_END;
}


- (void)doStartRTStuff{
    __LOG_METHOD_START;
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    //        [tglibEKG setDelegate:self];
    NSLog(@" cardioPlot realMode ---- ");
    //fake paulse now,when has algrithem,related to R-peak
    demoReplayFlag = YES;
    //        connectStateLabel.hidden = NO;
    colourThreadFlag = NO;
    if ([TGBleManager sharedTGBleManager].status == TGBleConnected ||
        [TGBleManager sharedTGBleManager].status == TGBleConnectedToBond)  {
        
        NSLog(@"connected!");
        connectFlag = YES;
        isConnectFlag = YES;
        int level = appDelegate.batteryLevel;
        [CommonValues updateConnectStateImg:connectStateImage withLevel:level];
        NSLog(@"call tryStartRT-----");
        [ [TGBleManager sharedTGBleManager] tryStartRT];
        
        if (!keepThread.isExecuting) {
            [keepThread start];
        }
        
    }
    else{
        NSLog(@"Unfortunately, no connection ====");
        NSLog(@"not connected!");
        connectStateImage.image = [UIImage imageNamed:@"disconnect"];
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"No Connection" message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:@" OK ", nil];
        alert.tag = noConnectionAlertTag;
        [alert show];
        
        
    }
    
    colourThread  = [[NSThread alloc]initWithTarget:self selector:@selector(changeColor) object:nil];
    
    //lock UI first,release after 2s
    [self.view setUserInteractionEnabled:NO];
    NSThread *releasUIThread = [[NSThread alloc]initWithTarget:self selector:@selector(releasUIRun) object:nil];
    [releasUIThread start];
    
    NSLog(@"show TOUCH_SENSOR_MSG---");
    NSThread *showTouchSensorThread  = [[NSThread alloc]initWithTarget:self selector:@selector(delay2showTouchSensor) object:nil];
    [showTouchSensorThread start];
    
    __LOG_METHOD_END_DESTROYED;
}

- (void)delay2showTouchSensor{
    __LOG_METHOD_START;
    //    sleep(0.5);
    usleep(300000);
    [SVProgressHUD showWithStatus:TOUCH_SENSOR_MSG];
    __LOG_METHOD_END;
};

- (void)showAlertController{
    __LOG_METHOD_START;
    [self presentViewController:alertController animated:YES completion:nil];
    
    __LOG_METHOD_END_DESTROYED;
}

@end

//
//  TrendView.m
//  WAT
//
//  Created by neurosky on 7/8/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import "TrendView.h"


@implementation TrendView
{
    CPTXYGraph *graph;
    
    NSMutableArray *dataForPlot;
    
    // NSMutableArray  *dataArray;
    CPTGraphHostingView *hv;
    
    UIImage *screenshot;
    NSString *screenshotPath;
    NSString *ccc;
    
    CPTScatterPlot *dataSourceLinePlot3;
    
    CPTScatterPlot *dataSourceLinePlot4;
    
    NSInteger dayCount;
    
    float y1;
      float y2;

}
/*
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
 */
- (id)initWithFrame:(CGRect)frame dataArr:(NSMutableArray *)arr {

    self = [super initWithFrame:frame];
    if (self) {
       
     
        NSMutableArray *arrA =  [[NSMutableArray alloc]init];
        
        for (int i = 0; i<arr.count; i++) {
            [arrA addObject:[NSNumber numberWithInt:i]];
        }
        
        [self GetTrendAB:arrA :arr];
        
        
        dataForPlot = [[NSMutableArray alloc]init];
//        NSLog(@"dataArr=====%@",arr);
     
//        dataForPlot = [NSMutableArray arrayWithCapacity:7];
        int maxHR = -1;
        int minHR = 999;
        dayCount = arr.count;
        
//        NSUInteger i;
//        for ( i = 0; i < arr.count; i++ ) {
//            
//          NSMutableArray *tmpArr = [[NSMutableArray alloc]init];
//            
//           tmpArr =  [arr objectAtIndex:i];
        
            for (int j = 0; j< arr.count; j++) {
                id x = [NSNumber numberWithFloat:0 + j * 1];
                
                float hr = [[arr objectAtIndex:j] floatValue];
                if (hr >  maxHR) {
                    maxHR = hr;
                }
                
                if (hr < minHR) {
                    minHR = hr;
                }
                
                
                id y = [NSNumber numberWithFloat: hr];
                 [dataForPlot addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:x, @"x", y, @"y", nil]];
            }
            
            
           
//        }
        
        NSLog(@"minHR = %d,maxHR = %d",minHR,maxHR);
        
        // Create graph from theme
        // 设置CPTXYGraph的主题    self> CPTGraphHostingView (from CPTXYGraph) >
        
        graph = [[CPTXYGraph alloc] initWithFrame:CGRectZero];
//        CPTTheme *theme = [CPTTheme themeNamed:kCPTPlainWhiteTheme];
//        [graph applyTheme:theme];
        
        
        CPTGraphHostingView *hostingView = [[CPTGraphHostingView alloc]initWithFrame:CGRectMake(0, 0, frame.size.width-0, frame.size.height-0)];
        [self addSubview:hostingView];
        graph.plotAreaFrame.borderLineStyle = nil;
        graph.plotAreaFrame.cornerRadius = 0.0f;
        //   hostingView.collapsesLayers = NO; // Setting to YES reduces GPU memory usage, but can slow drawing/scrolling
        hostingView.hostedGraph     = graph;
        
        graph.paddingLeft   = 0.0;
        graph.paddingTop    = 0.0;
        graph.paddingRight  = 0.0;
        graph.paddingBottom = 0.0;
        
        // Create a plot that uses the data source method
        
      
        // Setup plot space  设置CPTXYGraph的位置，坐标轴，X,Y可见范围
        CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
        
       
        plotSpace.allowsUserInteraction = NO;
         //横向留出下面20%+1的(最大最小值只差)距离 以免画出的点 只有一半；总的留出40%+2
        
        if (minHR == maxHR) {
             plotSpace.xRange                = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(-dayCount*0.2-1) length:CPTDecimalFromFloat(dayCount*1.4+2)];
            
            plotSpace.yRange                = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(minHR/2) length:CPTDecimalFromFloat(minHR)];
            
        }
        
        else{
        
        plotSpace.xRange                = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(-dayCount*0.2-1) length:CPTDecimalFromFloat(dayCount*1.4+2)];
//        plotSpace.globalXRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(-1)length:CPTDecimalFromFloat(dayCount*4)];
      
        
    //纵向留出下面20%的(最大最小值只差)距离 以免画出的点 只有一半；总的留出40%
        plotSpace.yRange                = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(minHR-(maxHR-minHR)*0.2) length:CPTDecimalFromFloat((maxHR-minHR)*1.4)];
//        plotSpace.globalYRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(minHR*0.5)length:CPTDecimalFromFloat((maxHR-minHR)*4)];
        
        }
        
        // Axes
        CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph.axisSet;
        CPTXYAxis *x          = axisSet.xAxis;
        
        x.minorTickLineStyle = nil;
//        x.axisConstraints = [CPTConstraints constraintWithRelativeOffset:0.5];
        x.labelingPolicy = CPTAxisLabelingPolicyNone;
        CPTMutableLineStyle *myLineStyle = [CPTLineStyle lineStyle];
        myLineStyle.lineColor = [CPTColor clearColor];
        
        x.axisLineStyle =  myLineStyle;
        
       
       
        
        CPTXYAxis *y = axisSet.yAxis;
        y.labelingPolicy = CPTAxisLabelingPolicyNone;
        y.axisLineStyle=CPTAxisLabelingPolicyNone;
        y.axisLineStyle =  myLineStyle;

//        y.axisConstraints = [CPTConstraints constraintWithRelativeOffset:0.5];
        
 
     //构建散点图
        dataSourceLinePlot3 = [[CPTScatterPlot alloc] init] ;
        
     //设置连接 多个（椭圆）点的 线的样式
        CPTMutableLineStyle*lineStyle3 = [CPTMutableLineStyle lineStyle];
        lineStyle3.lineColor         = [CPTColor clearColor];
//
//        
//        lineStyle3.lineWidth =  3.0f;
        dataSourceLinePlot3.dataLineStyle = lineStyle3;
        
        dataSourceLinePlot3.identifier    = @"Green Plot";
        dataSourceLinePlot3.dataSource    = self;
        
        [graph addPlot:dataSourceLinePlot3];
     //设置围成这个（椭圆）点 的包在外面的线
        CPTMutableLineStyle *symbolLineStyle3 = [CPTMutableLineStyle lineStyle];
        
        symbolLineStyle3.lineColor =[CPTColor blueColor];
        
    
        
    //设置（椭圆）点的样式
        CPTPlotSymbol *plotSymbol3 = [CPTPlotSymbol ellipsePlotSymbol];
        plotSymbol3.fill          = [CPTFill fillWithColor:[CPTColor blueColor]];
        plotSymbol3.lineStyle = symbolLineStyle3;
        plotSymbol3.size          = CGSizeMake(1.0, 1.0);
        dataSourceLinePlot3.plotSymbol = plotSymbol3;

      
   //趋势图直线
//        NSLog(@"trend line ---do in other place");
//        DoTrendLineStuff *DTLS = [[DoTrendLineStuff alloc]initWithCoordinates:0 :y1:(arrA.count-1 ) :y2];
//        
//     dataSourceLinePlot4 = [[CPTScatterPlot alloc] init] ;
//        CPTMutableLineStyle *lineStyle4 = [CPTMutableLineStyle lineStyle];
//        lineStyle4.lineColor         = [CPTColor yellowColor];
//        dataSourceLinePlot4.dataLineStyle = lineStyle4;
//        dataSourceLinePlot3.identifier    = @"xxx";
//        dataSourceLinePlot4.dataSource    = DTLS;
//        [graph addPlot:dataSourceLinePlot4];
//
        
        
    }
    return self;


}
-(void)changePlotRange
{
    
    NSLog(@"== changePlotRange ==");
    // Setup plot space
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.0) length:CPTDecimalFromFloat(3.0 + 2.0 * rand() / RAND_MAX)];
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.0) length:CPTDecimalFromFloat(3.0 + 2.0 * rand() / RAND_MAX)];
}
#pragma mark -
#pragma mark Plot Data Source Methods
-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot
{
    
    return [dataForPlot count];
}




-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index

{
//    NSLog(@"numberForPlot----");
    
    NSString *key = (fieldEnum == CPTScatterPlotFieldX ? @"x" : @"y");
    NSNumber *num = [[dataForPlot objectAtIndex:index] valueForKey:key];
    
    // Green plot gets shifted above the blue
    if ( [(NSString *)plot.identifier isEqualToString:@"Green Plot"] ) {
        if ( fieldEnum == CPTScatterPlotFieldY ) {
//                       num = [NSNumber numberWithDouble:[num doubleValue] + 1.0];
            // num = [NSNumber numberWithFloat:55+arc4random() % (10+1)];
            
            
        }
    }
    
    
    else if([(NSString *)plot.identifier isEqualToString:@"Red Plot"]){
        if ( fieldEnum == CPTScatterPlotFieldY ) {
//               num = [NSNumber numberWithFloat:55+arc4random() % (10+1)];
        }
        
        
    }
    
//    NSLog(@"num = %@",num);
    return num;
}

#pragma mark -
#pragma mark Axis Delegate Methods

-(BOOL)axis:(CPTAxis *)axis shouldUpdateAxisLabelsAtLocations:(NSSet *)locations
{
    
    
    static CPTTextStyle *positiveStyle = nil;
    static CPTTextStyle *negativeStyle = nil;
    
    //NSNumberFormatter *formatter = axis.labelFormatter;
    CGFloat labelOffset          = axis.labelOffset;
    NSDecimalNumber *zero        = [NSDecimalNumber zero];
    
    NSMutableSet *newLabels = [NSMutableSet set];
    
    for ( NSDecimalNumber *tickLocation in locations ) {
        CPTTextStyle *theLabelTextStyle;
        
        if ( [tickLocation isGreaterThanOrEqualTo:zero] ) {
            if ( !positiveStyle ) {
                CPTMutableTextStyle *newStyle = [axis.labelTextStyle mutableCopy];
                newStyle.color = [CPTColor clearColor];
                positiveStyle  = newStyle;
            }
            theLabelTextStyle = positiveStyle;
        }
        else {
            if ( !negativeStyle ) {
                CPTMutableTextStyle *newStyle = [axis.labelTextStyle mutableCopy];
                newStyle.color = [CPTColor clearColor];
                negativeStyle  = newStyle;
            }
            theLabelTextStyle = negativeStyle;
        }
        
        NSString *labelString       = [NSString stringWithFormat:@"%ld",(long)[tickLocation integerValue]];
        //  NSLog(@"labelString = %@",labelString);
        CPTTextLayer *newLabelLayer = [[CPTTextLayer alloc] initWithText:labelString style:theLabelTextStyle];
        
        CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithContentLayer:newLabelLayer];
        newLabel.tickLocation = tickLocation.decimalValue;
        newLabel.offset       = labelOffset;
        
        [newLabels addObject:newLabel];
    }
    axis.axisLabels = newLabels;
    return NO;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void) GetTrendAB :(NSMutableArray *)arrX :(NSMutableArray *)arrY {
    float sumX = 0;
    float sumY = 0;
    float avgX = 0;
    float avgY = 0;
    
    for(int i = 0; i < arrX.count; i++){
        sumX += [[arrX objectAtIndex:i]floatValue];
        sumY += [[arrY objectAtIndex:i]floatValue];

    }
    avgX = sumX / arrX.count;
    avgY = sumY / arrY.count;
    
    float sumLowBlock = 0;
    float sumHighBlock = 0;
    
    for(int j = 0; j < arrX.count; j++){
        sumLowBlock += ([[arrX objectAtIndex:j]floatValue] - avgX) * ([[arrX objectAtIndex:j]floatValue] - avgX);
        sumHighBlock += ([[arrX objectAtIndex:j]floatValue] - avgX)*([[arrY objectAtIndex:j]floatValue] - avgY);
    }
    
    float a = sumHighBlock / sumLowBlock;
    float b = avgY - (a * avgX);
    
    y1 = b;
    y2 = a* (arrX.count-1)+b;
    
    NSLog(@"a = %f ,b = %f ,y1 = %f ,y2 = %f",a,b,y1,y2);
    
    
}

@end

//
//  TGLibEKG.m
//  WAT
//
//  Created by Mark I. Walsh on 2/25/15.
//  Copyright (c) 2015 Julia. All rights reserved.
//

#import "TGLibEKG.h"

#if( 0 )// !defined( USE_THINKGEAR_LIBRARY )

@implementation TGLibEKG

@synthesize  delegate;

- (id)init:(int) rawDataPowerGridNoiseFrequency
{
    return self;
}

- (bool)initEkgAnalysis:(NSDate *) oneSecondTS { return YES; } // deprecated, use the following replacement (sampleRate: 512, settling: 2)
- (bool)initEkgAnalysis:(NSDate *) oneSecondTS sampleRate:(int) sampleRate{ return YES; } // // deprecated, use the following replacement
- (bool)initEkgAnalysis:(NSDate *) oneSecondTS sampleRate:(int) sampleRate settlingTime: (int) seconds{ return YES; }
// sampleRate must be either 256 or 512
// settlingTime must be 0 to 5 seconds


- (bool)requestEkgAnalysis_s:(short) rawSample{ return YES; }
- (int)getVersion{ return 0; }
- (NSString *)getProductId{ return @"54321"; }
- (NSString *)getAlgoVersion{ return @"65432"; }

- (void)setUserProfile:(int) age
            withGender:(bool) female // true = female, false = male
             withWrist:(bool) left{ return; } // true = band is on left wrist


- (void)setHRVOutputInterval:(int)outputInterval{ return; }

- (int)computeHRVNow{ return 72; }


- (void)sleepInitAnalysis:(NSDate*) startTime endTime:(NSDate *) endTime { return; }
- (void)sleepInitAnalysis{ return; }
- (void)sleepAddData:(NSDate*) sampleTime sleepPhase:(int) sleepPhase{ return; }
- (void)sleepRequestAnalysis { return; }
- (void)sleepRequestAnalysis:(NSDate *) startTime endTime:(NSDate *) endTime{ return; }
- (void)sleepSetInterval:(int)minutes completeData:(bool) fullDataSet{ return; }


@end
#endif  //  end #if !defined( USE_THINKGEAR_LIBRARY )


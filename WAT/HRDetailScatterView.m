//
//  HRDetailScatterView.m
//  WAT
//
//  Created by neurosky on 10/10/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import "HRDetailScatterView.h"
#import "CommonValues.h"
#import "UserProfile.h"
#import "Constant.h"

@implementation HRDetailScatterView
{
    CPTXYGraph *graph;
    
    NSMutableArray *dataForPlot;
    
    // NSMutableArray  *dataArray;
    CPTGraphHostingView *hv;
    
    UIImage *screenshot;
    NSString *screenshotPath;
    NSString *ccc;
    
    CPTScatterPlot *dataSourceLinePlot3;
    
    CPTScatterPlot *dataSourceLinePlot4;
    
    NSInteger dayCount;
    
    float y1;
    float y2;
    NSMutableArray *DateArray;
    NSInteger lengthInDifferentPeriod;


}
- (id)initWithFrame:(CGRect)frame :(NSMutableArray *)arr :(NSString *)dateStr : (NSInteger)dayCounts : (NSInteger)chartXaxisPeriod :(NSInteger)chartLableDisplayCount
{
    
    self = [super initWithFrame:frame];
    if (self) {
        
        
        if (arr == NULL) {
            return  self;
        }
        
        lengthInDifferentPeriod = chartXaxisPeriod;
        
        NSMutableArray * const arrA = /* [[NSMutableArray alloc]init];
        arrA = */ [arr objectAtIndex:0];
        
        NSMutableArray * const arrB = /* [[NSMutableArray alloc]init];
        arrB = */ [arr objectAtIndex:1];
        
        DateArray =  [[NSMutableArray alloc]init];
        dataForPlot = [[NSMutableArray alloc]init];
        // NSLog(@"dataArr=====%@",arr);
        
        //获得数组中的最大最小值
        float maxHRFloat = -1.0;
        int   maxHR=-1.0;
        int   minHR = 999.0;
        dayCount = arrA.count;
        
       for (int j = 0; j< arrA.count; j++)
       {
            id x = [arrA objectAtIndex:j];
            
            float hr = [[arrB objectAtIndex:j] floatValue];
            if (hr >  maxHRFloat) {
                maxHRFloat = hr;
            }
            
            if (hr < minHR) {
                minHR = hr;
            }
           
            id y = [NSNumber numberWithFloat: hr];
            [dataForPlot addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:x, @"x", y, @"y", nil]];
           
        }
        
        if (0.5<(maxHRFloat - (int)maxHRFloat))
        {
            maxHR=(int)(maxHRFloat +1);
        }
        else
        {
            maxHR=(int)maxHRFloat ;

        }
        
        NSLog(@"minHR = %d,maxHR = %d,maxHRFloat= %f",minHR,maxHR,maxHRFloat);
        
        
        
        // Create graph from theme
        // 设置CPTXYGraph的主题    self> CPTGraphHostingView (from CPTXYGraph) >
        //   hostingView.collapsesLayers = NO; // Setting to YES reduces GPU memory usage, but can slow drawing/scrolling

        graph = [[CPTXYGraph alloc] initWithFrame:CGRectZero];
        CPTGraphHostingView *hostingView = [[CPTGraphHostingView alloc]initWithFrame:CGRectMake(0, 0, frame.size.width-0, frame.size.height-0)];
        [self addSubview:hostingView];
        
        graph.plotAreaFrame.borderLineStyle = nil;
        graph.plotAreaFrame.cornerRadius    = 0.0f;
        hostingView.hostedGraph                 = graph;
        
        graph.paddingLeft   = 3.0;
        graph.paddingTop    = 0.0;
        graph.paddingRight  = 3.0;
        graph.paddingBottom = 5.0;
        
        // Create a plot that uses the data source method
        
        // Setup plot space  设置CPTXYGraph的位置，坐标轴，X,Y可见范围
        
          CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
          plotSpace.allowsUserInteraction = NO;
          plotSpace.xRange                = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(-0.1*lengthInDifferentPeriod) length:CPTDecimalFromFloat(lengthInDifferentPeriod+0.15*lengthInDifferentPeriod)];
        
        //横向留出下面20%+1的(最大最小值只差)距离 以免画出的点 只有一半；总的留出40%+2
        
        
        if (minHR == maxHR)
        {
            
            plotSpace.yRange                = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(minHR - 2 - 4/0.7*0.2) length:CPTDecimalFromFloat(4/0.7)];
            minHR = minHR - 2;
            maxHR = maxHR + 2;
        }
        else
        {
            
            plotSpace.yRange                = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(minHR - (maxHR-minHR)/0.7*0.2) length:CPTDecimalFromFloat((maxHR-minHR)/0.7)];
            
        }
        
        // Axes
        CPTXYAxisSet *axisSet               = (CPTXYAxisSet *)graph.axisSet;
        CPTXYAxis *x                            = axisSet.xAxis;
        x.majorIntervalLength               = CPTDecimalFromString(@"0.5");
        x.orthogonalCoordinateDecimal  = CPTDecimalFromFloat(minHR - (maxHR-minHR)/0.7*0.1);

        
        CPTMutableTextStyle *myss = [[CPTMutableTextStyle alloc]init];
        if ([CommonValues getScreenHeight ] == 568)
        {
             myss.fontSize = 18.0f;
        }
        else
        {
            myss.fontSize = 16.0f;
        }
        myss.textAlignment =  NSTextAlignmentCenter;
       
        myss.fontName = DEFAULT_FONT_NAME;
        myss.color = [CPTColor grayColor];
        
        //X轴坐标
        DateArray = [CommonValues getDateArr:dateStr :dayCounts];
        NSLog(@"date array count %ld",(unsigned long)DateArray.count);
        x.labelingPolicy = CPTAxisLabelingPolicyNone;
        
        NSMutableArray *customTickLocations = [[NSMutableArray alloc]init];
        for (int i = 0 ; i < dayCounts; i++)
        {

            [customTickLocations addObject:[NSNumber numberWithInt:i]];
        }
        
        
        NSInteger      labelLocation     = 0;
        NSMutableArray *customLabels = [NSMutableArray array];

        for ( NSNumber *tickLocation in customTickLocations )
        {

            
            CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithText:[DateArray objectAtIndex:labelLocation] textStyle:myss];
            newLabel.tickLocation = [tickLocation decimalValue];
            newLabel.offset = 8.0;
            
            NSInteger const divide=dayCounts/(chartLableDisplayCount -1);
            double tmpLocation;
            if ( labelLocation==  0)
            {
    
                newLabel.tickLocation = [[NSNumber  numberWithDouble:0.2  ] decimalValue];
                
                [customLabels addObject:newLabel];
            }
        
            else if ((labelLocation +1) % divide== 0)
            {
                tmpLocation =(labelLocation +1)*chartXaxisPeriod/dayCounts;
     //           NSLog(@"label location %d, tmplocation is %f",labelLocation,tmpLocation);
                newLabel.tickLocation = [[NSNumber numberWithDouble:tmpLocation ] decimalValue];

                [customLabels addObject:newLabel];

            }

            labelLocation++;
            
            
        }
        
        x.axisLabels = [NSSet setWithArray:customLabels];
        x.titleOffset = 10;
 
        
        CPTXYAxis *y                            = axisSet.yAxis;
        int axisInterval                          = (maxHR -minHR)/4.0;
        y.majorIntervalLength                = CPTDecimalFromInt(axisInterval);
        y.orthogonalCoordinateDecimal   = CPTDecimalFromString(@"-0.5");

        NSNumberFormatter *labelFormatter = [[NSNumberFormatter alloc] init];
        labelFormatter.numberStyle              = NSNumberFormatterNoStyle;
        y.labelFormatter                              = labelFormatter;
        y.labelOffset                                   = 5.0f;
        y.delegate                                      = self;

        
        //设置连接 多个（椭圆）点的 线的样式
        CPTColor *lineColor = [[CPTColor alloc]initWithComponentRed:55/255.0 green:122/255.0 blue:226/255.0 alpha:1];

        CPTMutableLineStyle     *lineStyle3 = [CPTMutableLineStyle lineStyle];
        lineStyle3.lineColor                        = lineColor;
        lineStyle3.lineWidth                        = 3.0;
        
        dataSourceLinePlot3                        = [[CPTScatterPlot alloc] init] ;
        dataSourceLinePlot3.dataLineStyle    = lineStyle3;
        dataSourceLinePlot3.identifier          = @"Green Plot";
        dataSourceLinePlot3.dataSource       = self;
        
        [graph addPlot:dataSourceLinePlot3];
        
        //设置围成这个（椭圆）点 的包在外面的线
        CPTMutableLineStyle *symbolLineStyle3 = [CPTMutableLineStyle lineStyle];
        
        CPTColor *HRPointColor = lineColor;
        
        symbolLineStyle3.lineColor = HRPointColor;
        
        //设置（椭圆）点的样式
        CPTPlotSymbol *plotSymbol3 = [CPTPlotSymbol ellipsePlotSymbol];
        plotSymbol3.fill          = [CPTFill fillWithColor:HRPointColor];
        plotSymbol3.lineStyle = symbolLineStyle3;
        plotSymbol3.size          = CGSizeMake(2.0, 2.0);
        dataSourceLinePlot3.plotSymbol = plotSymbol3;
        
        
    }
    return self;
    
    
}

#pragma mark -
#pragma mark Plot Data Source Methods
-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot
{
    
    return [dataForPlot count];
}




-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index

{
    //    NSLog(@"numberForPlot----");
    
    NSString *key = (fieldEnum == CPTScatterPlotFieldX ? @"x" : @"y");
    NSNumber *num = [[dataForPlot objectAtIndex:index] valueForKey:key];
    
    // Green plot gets shifted above the blue
    if ( [(NSString *)plot.identifier isEqualToString:@"Green Plot"] ) {
        if ( fieldEnum == CPTScatterPlotFieldY ) {
            //                       num = [NSNumber numberWithDouble:[num doubleValue] + 1.0];
            // num = [NSNumber numberWithFloat:55+arc4random() % (10+1)];
            
            
        }
    }
    
    
    else if([(NSString *)plot.identifier isEqualToString:@"Red Plot"]){
        if ( fieldEnum == CPTScatterPlotFieldY ) {
            //               num = [NSNumber numberWithFloat:55+arc4random() % (10+1)];
        }
        
        
    }
    
    //    NSLog(@"num = %@",num);
    return num;
}

#pragma mark -
#pragma mark Axis Delegate Methods
-(BOOL)axis:(CPTAxis *)axis shouldUpdateAxisLabelsAtLocations:(NSSet *)locations
{
    static CPTTextStyle *positiveStyle = nil;
    static CPTTextStyle *negativeStyle = nil;
    
    NSFormatter *formatter = axis.labelFormatter;
    CGFloat labelOffset    = axis.labelOffset;
    NSDecimalNumber *zero  = [NSDecimalNumber zero];
    
    NSMutableSet *newLabels = [NSMutableSet set];
    
    for ( NSDecimalNumber *tickLocation in locations ) {
        CPTTextStyle *theLabelTextStyle;
        
        if ( [tickLocation isGreaterThanOrEqualTo:zero] ) {
            if ( !positiveStyle ) {
                CPTMutableTextStyle *newStyle = [axis.labelTextStyle mutableCopy];
                newStyle.color = [CPTColor grayColor];
                newStyle.fontName = DEFAULT_FONT_NAME;
                positiveStyle  = newStyle;
            }
            theLabelTextStyle = positiveStyle;
        }
        else {
            if ( !negativeStyle ) {
                CPTMutableTextStyle *newStyle = [axis.labelTextStyle mutableCopy];
                newStyle.color = [CPTColor clearColor];
                negativeStyle  = newStyle;
            }
            theLabelTextStyle = negativeStyle;
        }
        
        NSString *labelString       = [formatter stringForObjectValue:tickLocation];
        CPTTextLayer *newLabelLayer = [[CPTTextLayer alloc] initWithText:labelString style:theLabelTextStyle];
        
        CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithContentLayer:newLabelLayer];
        newLabel.tickLocation = tickLocation.decimalValue;
        newLabel.offset       = labelOffset;
        
        [newLabels addObject:newLabel];
    }
    
    axis.axisLabels = newLabels;
    
    return NO;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

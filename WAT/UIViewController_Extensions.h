//
//  UIViewController_Extensions.h
//  WAT
//
//  Created by Mark I. Walsh on 3/11/15.
//  Copyright (c) 2015 Mark I. Walsh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController ( Extensions )


- ( void ) checkBlueToothStatusDone: ( BOOL ) promptIfNotEnabled;


- ( void ) showBlueToothNotOnAlert;


@end  //  end @interface UIViewController ( Extensions )

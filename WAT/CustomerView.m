//
//  CustomerView.m
//  WAT
//
//  Created by  NeuroSky on 9/24/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import "CustomerView.h"
#import "CHYPorgressImageView.h"
#import "Constant.h"


@implementation CustomerView
//UIImageView * _backgroundImageView;
//UILabel * _label;
//CGFloat minimumForegroundWidth;
//CGFloat availableWidth;
//UIImageView * _progressForImg;
//CHYPorgressImageView *_progressImageView;





- (id)initWithFrame:(CGRect)frame backgroundImage:(UIImage *)backgroundImage textValue:(NSString*)stringValue  withTextColor:(UIColor *)textColor  progress:(float) progressValue  progressForImgName:(NSString *) imgName{
    self = [super initWithFrame:frame];
    if (self) {
        _backgroundImageView = [[UIImageView alloc] initWithFrame:self.bounds];
        _backgroundImageView.image = backgroundImage;
        [self addSubview:_backgroundImageView];
        _label = [[UILabel alloc]initWithFrame:CGRectMake(10,35, 65, 42)];
        _label.backgroundColor = [UIColor clearColor];
        _label.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:25.0];
        _label.textColor = textColor;
        _label.textAlignment = NSTextAlignmentCenter;
        _label.text = stringValue;
        
        [self addSubview:_label];
        _progressForImg = [[UIImageView alloc]initWithFrame:CGRectMake(75,13, 12, 83)];
         [_progressForImg setImage:[UIImage imageNamed:@"progress_bottom@2x.png"]];
        
        [self addSubview:_progressForImg];
        _progressImageView = [[CHYPorgressImageView alloc]initWithFrame:CGRectMake(75,13, 12, 83)];
        [_progressImageView setImage:[UIImage imageNamed:imgName]];
        _progressImageView.verticalProgress = YES;
        _progressImageView.hasGrayscaleBackground = NO;
        
        _progressImageView.progress = progressValue;
        
        [self addSubview:_progressImageView];
        
        // self.progress = 0.0;
    }
    return self;

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

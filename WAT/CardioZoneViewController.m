//
//  CardioZoneViewController.m
//  WAT
//
//  Created by Julia on 4/22/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import "CardioZoneViewController.h"

@interface CardioZoneViewController ()

@end

@implementation CardioZoneViewController

@synthesize dataForPlot;
@synthesize  hv;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    //看下面的，自己可以调整，以达到你想要的效果，比如只能横屏显示，只能竖屏显示，或者直接返回YES,表示可以支持任何方向的旋转.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void)viewDidLoad
{
    NSString *backString = NSLocalizedString(@"back", @"");
    
    back.titleLabel.text  = backString;
    /*
     //自定义设置X,Y标签刻度
     int length  = 6;  // 6个刻度值, 5格
     float valX[length];
     float valY[length] ;
     int intervalX = ([DemoData maxHr]- [DemoData minHr])/(length-1) + 1;
     NSLog(@"interval  x = %d",intervalX);
     
     int intervalY = ([DemoData maxHrv]- [DemoData minHrv])/(length-1) + 1;
     NSLog(@"interval  y = %d",intervalY);
     
     NAFloat y = [DemoData minHrv] ;
     NAFloat x = [DemoData minHr] ;
     
     //    float valX[5] ={78,81,84,87,90};
     //    float valY[5] = {36,38,40,42,44};
     for (int i = 0; i< length; i++) {
     valX[i] = [DemoData minHr] + intervalX*(i) ;
     valY[i] = [DemoData minHrv] + intervalY*(i) ;
     //        NSLog(@"valX = %f",valX[i]);
     //        NSLog(@"valY = %f",valY[i]);
     
     
     */
    int x_length  = 6;  // 6个刻度值, 5格
    
    int y_length = 6 ;
    NSArray *xArray = [NSArray arrayWithObjects:@"67",@"77",@"88",@"65",@"70",@"80", nil];
    NSLog(@"arr[3]  = %@",xArray[3]);
    int x_min = 67;
    int x_max = 88;
    
    int intervalX = (x_max- x_min)/(x_length - 1) + 1;
    
    NSArray *yArray = [NSArray arrayWithObjects:@"88",@"77",@"97",@"33",@"26",@"114", nil];
    int y_min = 26;
    int y_max = 114;
    int intervalY = (y_max- y_min)/(y_length - 1) + 1;
    NSLog(@" intervalX = %d ,intervalY = %d",intervalX,intervalY);
    NSLog(@"============");
    
    
    // Create graph from theme
    graph = [[CPTXYGraph alloc] initWithFrame:CGRectZero];
    CPTTheme *theme = [CPTTheme themeNamed:kCPTPlainWhiteTheme];
    [graph applyTheme:theme];
    CPTGraphHostingView *hostingView = (CPTGraphHostingView *)hv;
    NSLog(@"yyyyy");
    
    hostingView.collapsesLayers = NO; // Setting to YES reduces GPU memory usage, but can slow drawing/scrolling
    hostingView.hostedGraph     = graph;
    
    graph.paddingLeft   = 10.0;
    graph.paddingTop    = 10.0;
    graph.paddingRight  = 10.0;
    graph.paddingBottom = 10.0;
    
    // Create a plot that uses the data source method
    //    CPTScatterPlot *plotSpace = [[[CPTScatterPlot alloc] init] autorelease];
    //    plotSpace.identifier = @"Data Source Plot";
    
    NSLog(@"xxxxx");
    // Setup plot space
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    plotSpace.allowsUserInteraction = YES;
    plotSpace.xRange                = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(x_min - 2*intervalX ) length:CPTDecimalFromInt(x_max - x_min + 3*intervalX)];
    plotSpace.yRange                = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(y_min - 2*intervalY) length:CPTDecimalFromInt(y_max - y_min + 3*intervalY)];
    
    // Axes
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph.axisSet;
    
    
    CPTXYAxis *x          = axisSet.xAxis;
    
    /*
     //X轴单位长度
     x.title = @"Heart Rate ";
     x.majorIntervalLength         = CPTDecimalFromString(@"0.5");
     x.orthogonalCoordinateDecimal = CPTDecimalFromString(@"50");//设置坐标系的交叉位置
     x.minorTicksPerInterval       = 0;
     
     NSLog(@"11111");
     
     // x.labelRotation  = M_PI / 4;
     x.labelingPolicy = CPTAxisLabelingPolicyNone;
     NSArray *customTickLocations = [NSArray arrayWithObjects:[NSDecimalNumber numberWithInt:1], [NSDecimalNumber numberWithInt:2], [NSDecimalNumber numberWithInt:3], [NSDecimalNumber numberWithInt:4], [NSDecimalNumber numberWithInt:5],[NSDecimalNumber numberWithInt:6],[NSDecimalNumber numberWithInt:7], [NSDecimalNumber numberWithInt:8],   nil];
     NSArray *xAxisLabels         = [NSArray arrayWithObjects:@"Aug 27", @"Aug 28", @"Aug 29", @"Aug 30", @"Aug 31", @"Sep 1",@"Sep 2",@"Sep 3",nil];
     NSUInteger labelLocation     = 0;
     NSMutableArray *customLabels = [NSMutableArray arrayWithCapacity:[xAxisLabels count]];
     for ( NSNumber *tickLocation in customTickLocations ) {
     CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithText:[xAxisLabels objectAtIndex:labelLocation++] textStyle:x.labelTextStyle];
     newLabel.tickLocation = [tickLocation decimalValue];
     newLabel.offset       = x.labelOffset + x.majorTickLength;
     // newLabel.rotation     = M_PI / 4;
     [customLabels addObject:newLabel];
     
     }
     NSLog(@"222222");
     x.axisLabels = [NSSet setWithArray:customLabels];
     
     */
    
    
    
    
    //x轴单位长度
    
    x.title = @"Heart Rate";
    
    x.titleOffset = 18.0f;
    // x.majorIntervalLength         = CPTDecimalFromString(@"3");
    x.majorIntervalLength =    CPTDecimalFromInt(intervalX);
    
    x.minorTicksPerInterval       = 0;
    // x.majorTickLength =  1.f;
    //    x.tickDirection =10.0f;
    //x.preferredNumberOfMajorTicks = 10;
    
    x.orthogonalCoordinateDecimal = CPTDecimalFromInt(y_min - intervalY);//设置坐标系的交叉位置
    x.delegate             = self;
    
    CPTXYAxis *y = axisSet.yAxis;
    
    //Y轴单位长度
    
    y.title = @"H R V ";
    y.titleOffset = 18.0f;
    y.majorIntervalLength         = CPTDecimalFromInt(intervalY);
    y.minorTicksPerInterval       = 1;
    y.orthogonalCoordinateDecimal = CPTDecimalFromInt(x_min - intervalX);
    
    y.delegate             = self;
    
    
    NSLog(@"33333");
    ////绿色五角星
    CPTScatterPlot *dataSourceLinePlot3 = [[CPTScatterPlot alloc] init] ;
    CPTMutableLineStyle *lineStyle3 = [CPTMutableLineStyle lineStyle];
    //    lineStyle.miterLimit        = 1.0f;
    lineStyle3.lineWidth         = .5f;
    lineStyle3.lineColor         = [CPTColor clearColor];
    dataSourceLinePlot3.dataLineStyle = lineStyle3;
    
    dataSourceLinePlot3.identifier    = @"Green Plot";
    dataSourceLinePlot3.dataSource    = self;
    
    [graph addPlot:dataSourceLinePlot3];
    
    // Put an area gradient under the plot above
    
    CPTMutableLineStyle *symbolLineStyle3 = [CPTMutableLineStyle lineStyle];
    
    symbolLineStyle3.lineColor =[CPTColor redColor];
    
    
    CPTPlotSymbol *plotSymbol3 = [CPTPlotSymbol snowPlotSymbol];
    plotSymbol3.fill          = [CPTFill fillWithColor:[CPTColor redColor]];
    plotSymbol3.lineStyle = symbolLineStyle3;
    // plotSymbol3.lineStyle     = symbolLineStyle3;
    plotSymbol3.size          = CGSizeMake(5.0, 5.0);
    dataSourceLinePlot3.plotSymbol = plotSymbol3;
    //
    /*
     //add CPTImage
     UIImage *image=[UIImage imageNamed:@"CPTImage.png"];
     CPTImage *fillimage=[CPTImage imageWithCGImage:image.CGImage];
     CPTBorderedLayer *imageLayer = [[CPTBorderedLayer alloc]init];
     imageLayer.frame=CGRectMake(0, 0, 100, 50);
     imageLayer.fill=[CPTFill fillWithImage:fillimage];
     NSArray * anchorPoint = [NSArray arrayWithObjects:[NSDecimalNumber numberWithInt:6], [NSDecimalNumber numberWithInt:52],    nil];
     
     
     CPTPlotSpaceAnnotation *imageannotation  = [[CPTPlotSpaceAnnotation alloc] initWithPlotSpace:graph.defaultPlotSpace anchorPlotPoint:anchorPoint];
     imageannotation.contentLayer = imageLayer;
     [graph.plotAreaFrame.plotArea addAnnotation:imageannotation];
     */
    
    
    // Add some initial data
    NSMutableArray *contentArray = [NSMutableArray arrayWithCapacity:100];
    NSUInteger i;
    for ( i = 0; i < xArray.count; i++ ) {
        id x = [NSNumber numberWithInt:[xArray[i] intValue]];
        id y = [NSNumber numberWithInt:[yArray[i] intValue]];
        [contentArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:x, @"x", y, @"y", nil]];
    }
    self.dataForPlot = contentArray;
    
    
    
#ifdef PERFORMANCE_TEST
    [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(changePlotRange) userInfo:nil repeats:YES];
#endif
}

-(void)changePlotRange
{
    // Setup plot space
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(0) length:CPTDecimalFromInt(3 + 2 * rand() / RAND_MAX)];
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0) length:CPTDecimalFromInt(3 + 2 * rand() / RAND_MAX)];
}

#pragma mark -
#pragma mark Plot Data Source Methods

-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot
{
    return [dataForPlot count];
}

-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{
    NSString *key = (fieldEnum == CPTScatterPlotFieldX ? @"x" : @"y");
    NSNumber *num = [[dataForPlot objectAtIndex:index] valueForKey:key];
    
    // Green plot gets shifted above the blue
    if ( [(NSString *)plot.identifier isEqualToString:@"Green Plot"] ) {
        NSLog(@"Green");
        if ( fieldEnum == CPTScatterPlotFieldY ) {
            //            num = [NSNumber numberWithDouble:[num doubleValue] + 1.0];
            //  num = [NSNumber numberWithInt:55+arc4random() % (10+1)];
            
            
        }
    }
    
    
    else if([(NSString *)plot.identifier isEqualToString:@"Red Plot"]){
        NSLog(@"Red");
        if ( fieldEnum == CPTScatterPlotFieldY ) {
            //  num = [NSNumber numberWithInt:55+arc4random() % (10+1)];
        }
        
        
    }
    return num;
}

#pragma mark -
#pragma mark Axis Delegate Methods

-(BOOL)axis:(CPTAxis *)axis shouldUpdateAxisLabelsAtLocations:(NSSet *)locations
{
    
    
    static CPTTextStyle *positiveStyle = nil;
    static CPTTextStyle *negativeStyle = nil;
    
    // NSNumberFormatter *formatter = axis.labelFormatter;
    CGFloat labelOffset          = axis.labelOffset;
    NSDecimalNumber *zero        = [NSDecimalNumber zero];
    
    NSMutableSet *newLabels = [NSMutableSet set];
    
    for ( NSDecimalNumber *tickLocation in locations ) {
        CPTTextStyle *theLabelTextStyle;
        
        if ( [tickLocation isGreaterThanOrEqualTo:zero] ) {
            if ( !positiveStyle ) {
                CPTMutableTextStyle *newStyle = [axis.labelTextStyle mutableCopy];
                newStyle.color = [CPTColor greenColor];
                positiveStyle  = newStyle;
            }
            theLabelTextStyle = positiveStyle;
        }
        else {
            if ( !negativeStyle ) {
                CPTMutableTextStyle *newStyle = [axis.labelTextStyle mutableCopy];
                newStyle.color = [CPTColor redColor];
                negativeStyle  = newStyle;
            }
            theLabelTextStyle = negativeStyle;
        }
        
        NSString *labelString       = [NSString stringWithFormat:@"%d",[tickLocation integerValue]];
        
        // NSLog(@"labelString = %@",labelString);
        
        
        CPTTextLayer *newLabelLayer = [[CPTTextLayer alloc] initWithText:labelString style:theLabelTextStyle];
        
        CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithContentLayer:newLabelLayer];
        newLabel.tickLocation = tickLocation.decimalValue;
        newLabel.offset       = labelOffset;
        //         NSLog(@"newLable  = %f",newLabel.tickLocation );
        [newLabels addObject:newLabel];
    }
    axis.axisLabels = newLabels;
    return NO;
}
@end

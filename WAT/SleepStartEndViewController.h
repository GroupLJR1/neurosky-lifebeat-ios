//
//  SleepStartEndViewController.h
//  WAT
//
//  Created by neurosky on 12/30/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SleepStartEndViewController : UIViewController
{

    __weak IBOutlet UIDatePicker *myPicker;
 
}


typedef void (^saveDelegateBlock) ();
@property (nonatomic,copy) saveDelegateBlock saveBlock;

- (IBAction)cancelClick:(id)sender;
- (IBAction)saveClick:(id)sender;

@property(strong,nonatomic) NSDate *pickDateDisplay;
@property(strong,nonatomic) NSString *pickDateStartString;
@property(strong,nonatomic) NSString *pickDateEndString;

@end

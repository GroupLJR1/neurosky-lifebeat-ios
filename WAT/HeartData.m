//
//  HeartData.m
//  WAT
//
//  Created by NeuroSky on 1/29/15.
//  Copyright (c) 2015 Julia. All rights reserved.
//

#import "HeartData.h"

@implementation HeartData

/**
 根据天数，结束时间 获得中位数的数组
 According to the dayCount (5/10/20),endDateStr  ,get the  Median Array (use for graph).
 */
+(NSMutableArray *)  MedianArrayDayCount :(NSInteger)dayCount endDateStr :(NSString *) endDateStr isHR :(BOOL)is_hr database : (sqlite3 *) database MovingInterval:(NSUInteger) MovingInterval MedianInterval:(NSUInteger)medianInterval
{
    
    NSMutableArray *timeArray =  [[NSMutableArray alloc]init];
    NSMutableArray *hrOrHrvArr =  [[NSMutableArray alloc]init];
    
    NSDate   *endDate = [[CommonValues timeDateFormatter:DateFormatterDate] dateFromString:endDateStr];
    NSDate   *startDate = [endDate dateByAddingTimeInterval:-dayCount*24*60*60];
    NSString *startDateStr = [[CommonValues timeDateFormatter:DateFormatterDate] stringFromDate:startDate];
 
    NSString * sqlStr = [NSString stringWithFormat:@"select datetime(time,'%ld hour','+0 minute')  as local_time, final_hr,avg_hrv from Heart where local_time > '%@ 00:00:00' and local_time < '%@ 24:00:00' and final_hr != 0  order by local_time asc",(long)[[[CommonValues timeDateFormatter:DateFormatterDate] timeZone] secondsFromGMT]/3600,startDateStr,endDateStr];
    
    NSLog(@"TimeHROrHRV sqlStr = %@",sqlStr);
    sqlite3_stmt *stmtSql;
    if(sqlite3_prepare_v2(database, [sqlStr UTF8String], -1, &stmtSql, NULL) == SQLITE_OK) {
 //       NSLog(@"peter prepare sql query1111111");
        while(sqlite3_step(stmtSql) == SQLITE_ROW) {
//            NSLog(@"peter execute sql query222222");

            NSString *myDateStr;
            const  unsigned  char *timeCharStr = sqlite3_column_text(stmtSql, 0);
            myDateStr = [NSString stringWithFormat:@"%s",timeCharStr];
 //           NSLog(@"myDateStr = %@",myDateStr);
            
            NSDate *TimeRecord= [[CommonValues timeDateFormatter:DateFormatterTimeColon] dateFromString:myDateStr];
            [timeArray addObject:TimeRecord];
            
            int hr = sqlite3_column_int(stmtSql, 1);
            int hrv = sqlite3_column_int(stmtSql, 2);
 //           NSLog(@"hrv value is %d",hrv);
            if (is_hr) {
                [hrOrHrvArr addObject:[NSNumber numberWithInt:hr]];
            }
            else{
                [hrOrHrvArr addObject:[NSNumber numberWithInt:hrv]];
            }
        }
        sqlite3_finalize(stmtSql);
        
    }
    
    //     NSLog(@"timeArr = %@",timeArr);
    //    NSLog(@"hrOrHrvArr = %@",hrOrHrvArr);
    //    NSLog(@"dayCount = %d",dayCount);
    

    
   //==============================================================================
    
    NSMutableArray *dataArray = [NSMutableArray array];
    NSTimeInterval secondPerDay=24*60*60;

    int MovingIntervalInt=(int)MovingInterval;
    
    NSDate *dateInMovingIntervalStart   = [ startDate dateByAddingTimeInterval:MovingIntervalInt*secondPerDay];
    NSDate *dateInMovingIntervalEnd    = [dateInMovingIntervalStart dateByAddingTimeInterval:MovingIntervalInt*secondPerDay];

  //  NSLog(@"Moving interval is %lu, moving count is %d",(unsigned long)MovingInterval,dayCount/MovingInterval);
    
    NSMutableArray *dateInMovingIntervalStartArray=[NSMutableArray array];
    NSMutableArray *dateInMovingIntervalEndArray=[NSMutableArray array];
    for (NSUInteger i =0; i < dayCount/MovingInterval; i++)
    {
        NSMutableArray *dataArrayObject=[NSMutableArray array];
        [dataArray addObject:dataArrayObject];
        
        [ dateInMovingIntervalStartArray addObject:dateInMovingIntervalStart];
        [ dateInMovingIntervalEndArray addObject:dateInMovingIntervalEnd];
        
        dateInMovingIntervalStart=[dateInMovingIntervalStart dateByAddingTimeInterval:MovingIntervalInt*secondPerDay];
        dateInMovingIntervalEnd= [dateInMovingIntervalEnd dateByAddingTimeInterval:MovingIntervalInt*secondPerDay];
        
//        NSLog(@"start date is  %@ end date is %@",dateInMovingIntervalStart,dateInMovingIntervalEnd);

    }

    NSUInteger dateRecordIndex =0;
    for (NSDate  *oneDateRecord in timeArray)
    {

        for (NSInteger i =0; i<dateInMovingIntervalStartArray.count ; i++)
        {
            if (([oneDateRecord compare:[dateInMovingIntervalStartArray objectAtIndex:i] ]==1 || [oneDateRecord compare:[dateInMovingIntervalStartArray objectAtIndex:i] ]==0) && ([oneDateRecord compare:[dateInMovingIntervalEndArray objectAtIndex:i]]==-1 || [oneDateRecord compare:[dateInMovingIntervalEndArray objectAtIndex:i]]==0))
 
            {
//                NSLog(@"peter compare result %d",[oneDateRecord compare:[dateInMovingIntervalStartArray objectAtIndex:i]]);
 //               NSLog(@"peter found the in the array index %d",i);
                [[dataArray objectAtIndex:i] addObject:[hrOrHrvArr objectAtIndex:dateRecordIndex]];
                
                 break;
            }
   //         NSLog(@"peter break cycle at %d",i);
        }
    
       dateRecordIndex++;
    }
    
    
    // get median data  1.day 2. one week 3. two weeks
    
    NSMutableArray *medianArr = [[NSMutableArray alloc]init];
    for (int i = 0; i< dateInMovingIntervalStartArray.count; i++)
    {
        NSInteger medianTmpNum = [self getMedian:[dataArray objectAtIndex:i]];
        
        [medianArr addObject:[NSNumber numberWithInteger:medianTmpNum]];
    }
//    NSLog(@"medianArr = %@",medianArr);
    
    
    NSMutableArray *filteredMedianArray = [NSMutableArray array];
    NSMutableArray *filteredMedianIndexArray = [NSMutableArray array];
    
    for (int i = 0 ; i<medianArr.count; i++)
    {
        int tmpHRV = [[medianArr objectAtIndex:i] floatValue];
        if (tmpHRV != -1)
        {
            [filteredMedianArray addObject:[medianArr objectAtIndex:i]];
            [filteredMedianIndexArray addObject:[NSNumber numberWithInt:i]];
        }
        
    }
    //   NSLog(@"filteredMedianIndexArray = %@",filteredMedianIndexArray);
    //    NSLog(@"filteredMedianArray = %@",filteredMedianArray);
    
//    NSMutableArray  *filteredMedianAvgArray = [NSMutableArray array];
//    NSMutableArray  *GrahpyArray = [NSMutableArray array];

    NSMutableArray  * const filteredMedianAvgArray = [self avgValueArr:medianInterval :filteredMedianArray];
    
    NSMutableArray  * const GrahpyArray = [[NSMutableArray alloc] init];
    [GrahpyArray addObject:filteredMedianIndexArray];
    [GrahpyArray addObject:filteredMedianAvgArray];

    //  NSLog(@"myMutArr = %@",GrahpyArr);
    
    return GrahpyArray;
    
    
}


+(NSMutableArray *)  MultiHeartArrayDayCount :(int)dayCount  endDateStr :(NSString *) endDateStr isHR :(BOOL)is_hr database : (sqlite3 *) database{
    NSMutableArray *timeAndHrOrHrvArr =  [[NSMutableArray alloc]init];
    NSMutableArray *timeArr =  [[NSMutableArray alloc]init];
    NSMutableArray *hrOrHrvArr =  [[NSMutableArray alloc]init];
    NSDate *endDate = [[CommonValues timeDateFormatter:DateFormatterDate] dateFromString:endDateStr];
    NSDate *startDate = [endDate dateByAddingTimeInterval:-dayCount*24*60*60];
    NSString *startDateStr = [[CommonValues timeDateFormatter:DateFormatterDate] stringFromDate:startDate];
    
    //    NSString * sqlStr = @"select date(datetime(time,'%d hour','+0 minute'))  as local_time, HR from Heart where local_time > startDate and local_time < endDateStr order by local_time asc";
    
    NSString * sqlStr = [NSString stringWithFormat:@"select date(datetime(time,'%ld hour','+0 minute'))  as local_time, final_hr,avg_hrv from Heart where local_time > '%@ 00:00:00' and local_time < '%@ 24:00:00' and final_hr != 0  order by local_time asc",(long)[[[CommonValues timeDateFormatter:DateFormatterDate] timeZone] secondsFromGMT]/3600,startDateStr,endDateStr];
    NSLog(@"TimeHROrHRV sqlStr = %@",sqlStr);
    sqlite3_stmt *stmtSql;
    if(sqlite3_prepare_v2(database, [sqlStr UTF8String], -1, &stmtSql, NULL) == SQLITE_OK) {
        while(sqlite3_step(stmtSql) == SQLITE_ROW) {
            NSString *myDateStr;
            const  unsigned  char *timeCharStr = sqlite3_column_text(stmtSql, 0);
            myDateStr = [NSString stringWithFormat:@"%s",timeCharStr];
            myDateStr = [myDateStr substringToIndex:10];
            // NSLog(@"myDateStr = %@",myDateStr);
            [timeArr addObject:myDateStr];
            int hr = sqlite3_column_int(stmtSql, 1);
            int hrv = sqlite3_column_int(stmtSql, 2);
            if (is_hr) {
                [hrOrHrvArr addObject:[NSNumber numberWithInt:hr]];
            }
            else{
                [hrOrHrvArr addObject:[NSNumber numberWithInt:hrv]];
            }
        }
        sqlite3_finalize(stmtSql);
        
    }
    
    //     NSLog(@"timeArr = %@",timeArr);
   //      NSLog(@"hrOrHrvArr = %@",hrOrHrvArr);
    [timeAndHrOrHrvArr addObject:timeArr];
    [timeAndHrOrHrvArr addObject:hrOrHrvArr];
    return timeAndHrOrHrvArr;
    
    
}


//get the two-dimensional array，  include many days, and in one day there are many Heart data.
+(NSMutableArray *)  MultiDaysOfHeartArray :(int)dayCount endDateStr :(NSString *) endDateStr  timeAndHrOrHrvArr :(NSMutableArray *)timeAndHrOrHrvArr calculateDaysInterval:(int)daysInterval{
    
    //NSMutableArray *timeArr =  [[NSMutableArray alloc]init];
    //NSMutableArray *hrOrHrvArr =  [[NSMutableArray alloc]init];
    NSMutableArray * const timeArr = [timeAndHrOrHrvArr objectAtIndex:0];
    NSMutableArray * const hrOrHrvArr = [timeAndHrOrHrvArr objectAtIndex:1];
    NSDate *endDate = [[CommonValues timeDateFormatter:DateFormatterDate] dateFromString:endDateStr];
    NSMutableArray *dataArr = [[NSMutableArray alloc]init];
    //    NSLog(@"dayCount = %d",dayCount);
   
    for (int i = 0 ; i < dayCount; i++) {
        NSDate *currentDate = [endDate dateByAddingTimeInterval:-(dayCount-1-i)*24*60*60];
        NSString *currentDateStr = [[CommonValues timeDateFormatter:DateFormatterDate] stringFromDate:currentDate];
        //        NSLog(@"currentDateStr = %@",currentDateStr);
        
//        if (i < daysInterval) {
            NSMutableArray * const hrTempArr = [[NSMutableArray alloc]init];
        
        if (i % daysInterval == 0 ) {
            NSLog(@"1111");
        }
            
            //find the time  from timeArr which time = oneday's date  ,add the HR/HRV data to
            for (int j = 0 ; j < timeArr.count; j++) {
                if ([[timeArr objectAtIndex:j] isEqualToString:currentDateStr]) {
                    [hrTempArr addObject:[hrOrHrvArr objectAtIndex:j]];
                }
            }

//        }
        if ((i % daysInterval == 0 )&& i != 0) {
            [dataArr addObject:hrTempArr];
             NSLog(@"22222");
        }
        
        
        
    }
       NSLog(@"dataArr = %@",dataArr);
    
    return dataArr;
    
}


+ (NSMutableArray *) movingAvgMedianArr :(int)dayCount MultiDaysOfHeartArray:(NSMutableArray *)dataArr
{

    NSMutableArray *medianArr = [[NSMutableArray alloc]init];
    for (int i = 0; i< dayCount; i++) {
        NSInteger medianTmpNum = [self getMedian:[dataArr objectAtIndex:i]];
        [medianArr addObject:[NSNumber numberWithInteger:medianTmpNum]];
    }
    NSLog(@"medianArr = %@",medianArr);
    return medianArr;
}


/**
 获得数组中的中位数
 HRArr is one day HR value array,
 This method is to get the Median of one day.
 */
+(NSInteger)getMedian :(NSMutableArray *)HRArr {
    NSInteger dayCount = HRArr.count;
    if (dayCount < 1) {
        return -1;
    }
    //    NSMutableArray * medianArr = [[NSMutableArray alloc]init];
    NSComparator cmptr = ^(id obj1, id obj2){
        if ([obj1 integerValue] > [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        
        if ([obj1 integerValue] < [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    };
    
    
    //    NSArray *sortArray = [[NSArray alloc] initWithObjects:@"1",@"3",@"8",@"7",@"8",@"2",@"6",nil];
    //第一种排序
    NSArray *sortedArray = [HRArr sortedArrayUsingComparator:cmptr];
    //    NSLog(@"sorted arr = %@",sortedArray);
    NSInteger midianNum = 0 ;
    if (dayCount % 2 == 0) {
        NSInteger midianNum1 = [[sortedArray objectAtIndex:dayCount/2] integerValue];
        NSInteger midianNum2 = [[sortedArray objectAtIndex:(dayCount/2-1)] integerValue];
        midianNum =(midianNum1 + midianNum2)/2;
        
    }
    else{
        midianNum = [[sortedArray objectAtIndex:dayCount/2] integerValue];
        
    }
    return midianNum;
    
    
}


/**
 intervalCount = 5
 算法 n<5时  取n个数的平均数 ， n >= 5时 去最近5个数的平均数
 */
+(NSMutableArray *) avgValueArr : (NSUInteger) intervalCount :(NSMutableArray *)filteredMedianArray {
    
    NSMutableArray  *filteredMedianAvgArray = [[NSMutableArray alloc]init];
    
    for (int i = 0 ; i < filteredMedianArray.count; i++) {
        float sumMedian = 0;
        float avgMedian = 0;
        
        if (i < intervalCount-1) {
            for (int j = 0; j <= i; j++) {
                sumMedian += [[filteredMedianArray objectAtIndex:j]integerValue];
            }
            avgMedian = sumMedian/(i+1);
        }
        else{
            for (int k = 0; k < intervalCount; k++) {
                sumMedian += [[filteredMedianArray objectAtIndex:(i-k)]integerValue];
            }
            avgMedian = sumMedian/intervalCount;
            
        }
        
        [filteredMedianAvgArray addObject:[NSNumber numberWithFloat:avgMedian]];
    }
    
    NSLog(@"filteredMedianAvgArray = %@",filteredMedianAvgArray);
    return filteredMedianAvgArray;
    
}

+ (int)avgHrvWithHrvArr:(NSMutableArray *)hrvArr{
    //get avg hrv from hrv array.
    int sumHrv = 0;
    int avgHrv = 0;
    for (int i = 0 ; i<hrvArr.count; i++) {
        sumHrv += [[hrvArr objectAtIndex:i]intValue];
    }
    
    //平均HRV
    avgHrv = sumHrv/hrvArr.count;
    return avgHrv;

}

+ (int)avgHrWithHrArr:(NSMutableArray *)hrArr
{
    int sumHr = 0;
    int avgHr = 0;
    for (int i = 0 ; i<hrArr.count; i++) {
        sumHr += [[hrArr objectAtIndex:i]intValue];
    }
    
    //平均HR
    avgHr = sumHr/hrArr.count;
    return avgHr;
}

@end

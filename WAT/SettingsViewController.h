//
//  SettingsViewController.h
//  WAT
//
//  Created by neurosky on 1/9/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TGBleManager.h"
#import "TGBleManagerDelegate.h"
#import "ZSYPopoverListView.h"

@interface SettingsViewController : UIViewController<UITextFieldDelegate,TGBleManagerDelegate,ZSYPopoverListDatasource,ZSYPopoverListDelegate>{
    NSString  *age;
    NSString  *height;
    NSString  *weight;
    NSString  *walkLength;
    NSString  *runLength;
    NSString  *alarmStr;
    NSString  *timerStr;
    NSMutableArray *alarmMode;
    
    NSString  *autoSyncStr;
    
    BOOL is_band_right;
    BOOL is_female;
    
    
    NSDate *selectedDate;
    NSDate *selectedTime;
    
    UITableView *repeatTableView;
    NSArray *repeatArray;
    NSMutableArray *repeatDayArray;
    
    
    NSDate *timerSelectedTime;
    
    
    __weak IBOutlet UIView *myWholeView;
    __weak IBOutlet UIButton *backBtn;
    
    __weak IBOutlet UITextField *alarmText;
    __weak IBOutlet UITextField *timerText;
    
    
    
    __weak IBOutlet UILabel *settingTitle;
    __weak IBOutlet UIButton *timerBtn;
    __weak IBOutlet UIButton *alarmBtn;
    
    __weak IBOutlet UITextField *autoSyncText;
    
    BOOL timerSwitch;
    BOOL alarmSwitch;
    BOOL  successBondedFlag;
    
    BOOL  isErasing;
    BOOL  isSettingGoal;
    BOOL isSettingAlarm;
    BOOL isSettingTimer;
    NSInteger distance;
    NSInteger step;
    NSInteger calorie;
    float sleep_;
    BOOL isBonded;
    ZSYPopoverListView *listView ;
    NSMutableArray *devicesArray;
    NSMutableArray *mfgDataArray;
    NSMutableArray *rssiArray;
    NSMutableArray *candIDArray;
    NSMutableArray *devicesIDArray;
    
    __weak IBOutlet UIButton *releaseBtn;
    __weak IBOutlet UIButton *eraseBtn;
    NSString *readyToSaveDeviceName;
    NSString *readyToSaveDeviceID;
    int viewTag;
    
    NSTimer *timeOutTimer;
    BOOL firstEnterCandiDel;
    
    NSTimer *showListTimer;
    BOOL wantSetGoal;
    BOOL wantSetAlarm;
    BOOL wantSetTimer;
    float screenHeight;
    
    BOOL wantDisableAlarm;
    BOOL wantDisableTimer;
    
    BOOL lostBeforeMainOperation ;
    
    __weak IBOutlet UIButton *autoSyncSwitchBtn;
    __weak IBOutlet UIButton *soundSwtichBtn;
    UIBackgroundTaskIdentifier backgroundTask;
    NSTimer *updateTimer;
    UIAlertController *alertController;
    
    
}

@property (nonatomic, retain) NSIndexPath *selectedIndexPath;

@property (weak, nonatomic) IBOutlet UILabel *sleepStartTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *sleepStopTimeLabel;
- (IBAction)setDefaultSleepTime:(id)sender;

// @property (strong, nonatomic) RepeatCellTableViewCell *cell;
- (IBAction)backClick:(id)sender;
- (IBAction)soundSwitchClick:(id)sender;
- (IBAction)autoSyncSwitchClick:(id)sender;
- (IBAction)bgTouch:(id)sender;
- (IBAction)autoSyncSelectClick:(id)sender;
- (IBAction)alarmBtnClick:(id)sender;
- (IBAction)timerBtnClick:(id)sender;
- (IBAction)eraseClick:(id)sender;
- (IBAction)alarmSelectClick:(id)sender;
- (IBAction)timerSelectClick:(id)sender;
- (IBAction)profileClick:(id)sender;
- (IBAction)saveBtnClick:(id)sender;
- (IBAction)goalClick:(id)sender;


@end

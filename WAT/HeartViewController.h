//
//  HeartViewController.h
//  WAT
//
//  Created by NeuroSky on 11/11/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CellHeart.h"
#import <sqlite3.h>



@interface HeartViewController : UIViewController
{
       NSMutableArray *HRArray;
       NSMutableArray *DateArray;
       sqlite3 *db;
 
   NSMutableArray *oneWeekDateArr;
    __weak IBOutlet UIView *myWholeView;
    __weak IBOutlet UIButton *monthBtn;
    __weak IBOutlet UIButton *sixMonthBtn;
    __weak IBOutlet UIButton *yearBtn;
}
@property (strong, nonatomic) IBOutlet UILabel *title_value1;
@property (nonatomic, strong) UISwipeGestureRecognizer *leftSwipeGestureRecognizer;
@property (nonatomic, strong) UISwipeGestureRecognizer *rightSwipeGestureRecognizer;

- (void)leftBtnClick:(id)sender;
- (void)rightBtnClick:(id)sender;
- (id) initwithHrLabel:(BOOL) HrEnable;

- (IBAction)back:(id)sender;
- (IBAction)monthBtnClick:(id)sender;
- (IBAction)sixMonthBtnClick:(id)sender;
- (IBAction)yearBtnClick:(id)sender;
@end

//
//  setSleepTimeView.h
//  WAT
//
//  Created by peterwang on 2/11/15.
//  Copyright (c) 2015 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface setSleepTimeView : UIView

typedef void (^cancelDelegateBlock) ();
@property (nonatomic,copy) cancelDelegateBlock cancelBlock;

typedef void (^saveDelegateBlock) ();
@property (nonatomic,copy) saveDelegateBlock saveBlock;

//@property(nonatomic,strong) NSDate   *startSleepTime;
//@property(nonatomic,strong) NSDate   *endSleepTime;

@property(nonatomic,strong) NSString *startSleepTimeString;
@property(nonatomic,strong) NSString *endSleepTimeString;


- (IBAction)save:(id)sender;
- (IBAction)cancel:(id)sender;

@property (weak, nonatomic) IBOutlet UIDatePicker *startTimePicker;
@property (weak, nonatomic) IBOutlet UIDatePicker *endTimePicker;

- (IBAction)setSleepStartTime:(id)sender;
- (IBAction)setSleepEndTime:(id)sender;

-(void) adaptIOSVersion;

@end

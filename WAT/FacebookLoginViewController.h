//
//  FacebookLoginViewController.h
//  WAT
//
//  Created by Julia on 4/23/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FacebookLoginViewController : UIViewController
@property (retain, nonatomic) IBOutlet FBLoginView *loginView;

@end

//
//  SetAutoSyncTimeViewController.h
//  WAT
//
//  Created by neurosky on 10/14/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SetAutoSyncTimeViewController : UIViewController{

    NSDate *timerSelectedTime;
    NSString  *timerStr;
    UIDatePicker *timerPicker;
    NSDate *newDate;
    NSDate *minDate;
 

}
- (IBAction)cancelBtnClick:(id)sender;
- (IBAction)saveBtnClick:(id)sender;

@end

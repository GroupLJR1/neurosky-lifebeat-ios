
//  AppDelegate.m
//  WAT
//
//  Created by Julia on 4/16/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import "AppDelegate.h"

#import "DashboardViewController.h"
#import "Constant.h"
#import "CommonValues.h"
#import "SVProgressHUD.h"
#import <AVFoundation/AVFoundation.h>
#import <sqlite3.h>

#import "TGBleEmulator.h"


@implementation AppDelegate{
    
    sqlite3 *db;
    int index;
}

@synthesize window;
@synthesize animationOverFlag;
@synthesize safeDataFlag;
@synthesize synFinishFlag;
@synthesize batteryLevel;
@synthesize batterWarningFlag;


- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    if ([[url scheme] isEqualToString:@"wat"]) {
        
        [application setApplicationIconBadgeNumber:10];
        
        return YES;
    }
    
    return NO;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
   
    NSLog(@"didFinishLauching....applicationState = %ld", (long)[UIApplication sharedApplication].applicationState) ;

    [[TGBleManager sharedTGBleManager] setupManager:true];
    [[TGBleManager sharedTGBleManager] enableLogging];
    //[[TGBleManager sharedTGBleManager] redirectLoggingToFile];

    self.window = [UIWindow new];
    [self.window makeKeyAndVisible];    
    CGRect frame = [UIScreen mainScreen].bounds;
//    self.window.frame = [[UIScreen mainScreen] bounds];
    self.window.frame = CGRectMake(0, 0, frame.size.width+0.000001, frame.size.height+0.000001);

    [self openDataBase];
    [self controlAppVersion];
    [self setUserDefaultValues];
    [UIApplication sharedApplication].idleTimerDisabled=YES;
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    self.window.backgroundColor = [UIColor whiteColor];
    self.window.rootViewController = [[DashboardViewController alloc]init];
    return YES;
}

- (void)controlAppVersion{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *App_version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    NSLog(@"App_version = %@",App_version);
    NSString *storedAppVersion = [userDefaults objectForKey:@"last_app_version"];
    //V3.4.2D 以后加入新的userDefault,默认为真，自动步长
    
    if ([storedAppVersion compare:@"3.4.2D"] < 0 )  {
        NSLog(@"3.4.2D=======");
        [userDefaults setBool:YES forKey:IS_AUTO_WALKING];
        [userDefaults setBool:YES forKey:IS_AUTO_RUNNING];
    }
    //copy fw file to /Document
    
    NSString *document  = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    NSString *newFile = [document stringByAppendingPathComponent:@"nskfw.bin"];
    NSString *oldFile = [[NSBundle mainBundle] pathForResource:@"nskfw" ofType:@"bin"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    //本次版本号与上次不同，则要拷贝bin文件
    if (![storedAppVersion isEqualToString:App_version]) {
        NSLog(@"本次版本号与上次不同，则要拷贝bin文件");
        [userDefaults setObject:App_version forKey:@"last_app_version"];
        [userDefaults synchronize];
        
        if ([fileManager fileExistsAtPath:newFile]) {
            NSLog(@" has nskfw.bin ,delete then copy ----");
            [fileManager removeItemAtPath:newFile error:nil];
            NSLog(@"  file deleted  ----");
            //            return  YES;
        }
        
        [fileManager copyItemAtPath:oldFile toPath:newFile error:nil];
        
        NSLog(@"nskfw.bin copy finished----");
    }
    //相同，则不拷贝
    else{
        NSLog(@"本次版本号与上次相同，无需拷贝bin文件");
        if ([fileManager fileExistsAtPath:newFile]) {
            NSLog(@"already has nskfw.bin ,no need to copy ----");
        }
    }

}

- (void)setUserDefaultValues{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSTimeZone *fromzone = [NSTimeZone systemTimeZone];
    NSInteger const timeZone =   [fromzone secondsFromGMT]/3600;
    NSLog(@"timeZone = %ld", (long)timeZone);
    [defaults setInteger:timeZone forKey:@"TimeZone"];
    NSInteger const step =  [defaults integerForKey:USER_GOAL_STEP];
    NSInteger const distance =  [defaults integerForKey:USER_GOAL_DISTANCE];
    NSInteger const calorie =  [defaults integerForKey:USER_GOAL_CALORI];
    float sleep = [defaults integerForKey:USER_GOAL_SLEEP];
    float active_time = [defaults integerForKey:USER_GOAL_ACTIVE_TIME];
    if (step == 0 ||distance == 0 ||calorie == 0 ||sleep == 0 || active_time == 0) {
        NSLog(@"no goals,set default");
        [defaults setInteger:3000 forKey:USER_GOAL_STEP];  // valid range is 10 to 65000
        [defaults setInteger:2000 forKey:USER_GOAL_CALORI];
        [defaults setInteger:2200 forKey:USER_GOAL_DISTANCE];
        [defaults setFloat:8 forKey:USER_GOAL_SLEEP];
        [defaults setFloat:1 forKey:USER_GOAL_ACTIVE_TIME];
        // [defaults synchronize];
        
    }
    
    else
        
        NSLog(@"has goals");
    NSString*   birthDate =  [defaults objectForKey:USER_BIRTH_DATE];
    NSString*   height =  [defaults objectForKey:USER_HEIGHT];
    NSString*   weight =  [defaults objectForKey:USER_WEIGHT];
    NSString*   walkLength =  [defaults objectForKey:USER_WALKING_STEP_LENGH];
    NSString*   runLength =  [defaults objectForKey:USER_RUNNING_STEP_LENGH];
    if (birthDate == NULL || height == NULL || weight == NULL || walkLength == NULL || runLength== NULL) {
        NSLog(@"no userProfile,set defult");
        [defaults setValue:@"1987/01/01" forKey:USER_BIRTH_DATE];
        [defaults setValue:@"168" forKey:USER_HEIGHT];
        [defaults setValue:@"60" forKey:USER_WEIGHT];
        [defaults setValue:@"72" forKey:USER_WALKING_STEP_LENGH];
        [defaults setValue:@"96" forKey:USER_RUNNING_STEP_LENGH];
        [defaults setBool:NO forKey:USER_IS_FEMALE];
        [defaults setBool:YES forKey:USER_IS_BAND_RIGHT];
        [defaults setBool:YES forKey:DISPLAY_24HR_Time];
        [defaults setBool:YES forKey:IS_AUTO_WALKING];
        [defaults setBool:YES forKey:IS_AUTO_RUNNING];
        [defaults setBool:NO forKey:DISPLAY_IMPERIAL_UNITS];
        
        [[TGBleManager sharedTGBleManager] setBandOnRight:YES];
        [[TGBleManager sharedTGBleManager] setBirthDay:1];
        [[TGBleManager sharedTGBleManager] setBirthYear:1987];
        [[TGBleManager sharedTGBleManager] setBirthMonth:1];
        [[TGBleManager sharedTGBleManager] setHeight:168];
        [[TGBleManager sharedTGBleManager] setWeight:600];
        [[TGBleManager sharedTGBleManager] setWalkingStepLength:72];
        [[TGBleManager sharedTGBleManager] setRunningStepLength:96];
        [[TGBleManager sharedTGBleManager] setFemale:NO];
        [[TGBleManager sharedTGBleManager] setDisplayTime24Hour:YES];
        [[TGBleManager sharedTGBleManager] setDisplayImperialUnits:NO];
        
    }
    else
        NSLog(@"has userProfile");
    //
    
    NSString *autoSleepStartTime = [defaults objectForKey:AUTO_SLEEP_START_TIME];
    if (autoSleepStartTime == NULL) {
        NSLog(@"set autoSleep default Time");
        [defaults setObject:@"22:00" forKey:AUTO_SLEEP_START_TIME];
        [defaults setObject:@"07:00" forKey:AUTO_SLEEP_END_TIME];
    }
    
    [defaults synchronize];
    // Override point for customization after application launch.
    NSMutableArray *repeatDayArray;
    repeatDayArray =[[NSMutableArray alloc]init];
    NSMutableArray *alarmMode;
    alarmMode =[ [NSMutableArray alloc]init];
    [alarmMode addObject:@"Everyday"];
    [defaults setObject:alarmMode forKey:@"XXX"];
    [defaults synchronize];
    NSMutableArray * const dafa = /* [[NSMutableArray alloc]init];
    dafa = */ [defaults objectForKey:@"XXX"];
    NSLog(@"alarmMode = %@",dafa);
    
    if ([dafa containsObject:@"Once"]) {
        NSLog(@"contains---");
    }
    else{
        
        NSLog(@" not contains---");
    }
    
    if ([dafa isKindOfClass:[NSMutableArray class ]]){
        for (int i = 0; i<dafa.count; i++) {
            [repeatDayArray addObject:[dafa objectAtIndex:i]];
            
        }
    }
    [SVProgressHUD setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.8]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    safeDataFlag = @"NO";
    synFinishFlag = @"NO";
    enterFlag = @"";
    batterWarningFlag = YES;
}

-(void)copyFileToDocument
{
    NSLog(@"copyFileToDocument----");
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    for (int i = 0; i<DemoUsers.count; i++) {
        
        NSString *newFile = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.csv",DemoUsers[i]]];
        if ([fileManager fileExistsAtPath:newFile]) {
            NSLog(@"file exists");
        }else {
            
            NSString *oldFile =[[NSBundle mainBundle] pathForResource:DemoUsers[i] ofType:@"csv"];
            
            [fileManager copyItemAtPath:oldFile toPath:newFile error:nil];
            
            
        }}
    
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    NSLog(@"applicationWillResignActive----");
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    //test  background timer
    
//    UIApplication* app = [UIApplication sharedApplication];
//    __block UIBackgroundTaskIdentifier bgTask;
//    bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
//        dispatch_async(dispatch_get_main_queue(), ^{
//            if (bgTask != UIBackgroundTaskInvalid)
//            {
//                bgTask = UIBackgroundTaskInvalid;
//            }
//        });
//    }];
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        dispatch_async(dispatch_get_main_queue(), ^{
//            if (bgTask != UIBackgroundTaskInvalid)
//            {
//                bgTask = UIBackgroundTaskInvalid;
//            }
//        });
//    });
    
    //
    
    
    NSLog(@"applicationDidEnterBackground----");
    [CommonValues setBackgroundFlag:YES];
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    if ([CommonValues getHasTaskFlag]) {
        NSLog(@"has task");
    }
    else{
        NSLog(@"no task");
        if ([CommonValues getAutoSyncingFlag]) {
            NSLog(@"is auto syncing,");
        }
        else{
            
            NSLog(@"call tryDisconnect ------");
            [[TGBleManager sharedTGBleManager] tryDisconnect];
        }
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [CommonValues setBackgroundFlag:NO];
    NSLog(@"applicationWillEnterForeground----");
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
//    [[UIApplication sharedApplication] clearKeepAliveTimeout];
//      NSLog(@"xxxxxx  clearKeepAliveTimeout --xxxxxxx-----");
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    NSLog(@"applicationDidBecomeActive----");
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    NSLog(@"applicationWillTerminate----");
    
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    //   [[TGAccessoryManager sharedTGAccessoryManager] teardownManager];
    
    [[TGBleManager sharedTGBleManager] teardownManager];
    
    [self closeDataBase];
    // all good things must come to an end
    // this is a good idea because things may be hanging off the session, that need
    // releasing (completion block, etc.) and other components in the app may be awaiting
    // close notification in order to do cleanup
}

-(void)writeToTemp{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString*   birth  = [defaults objectForKey:USER_AGE];
    int year = 0;
    int  month = 0 ;
    int day = 0;
    NSArray *array_birth = [birth componentsSeparatedByString:@"/"];
    if (array_birth.count == 3) {
        year  =  [ array_birth[0] intValue];
        month =  [ array_birth[1] intValue];
        day =  [ array_birth[2] intValue];
        //  NSLog(@"year = %d,month = %d, day =%d",year,month,day);
    }
    
    [[TGBleManager sharedTGBleManager] setBirthYear:year];
    [[TGBleManager sharedTGBleManager] setBirthMonth:month];
    [[TGBleManager sharedTGBleManager] setBirthDay:day];
    
    
    
    NSLog(@"USER_IS_FEMALE = %@,USER_AGE = %ld,USER_WEIGHT = %@, USER_HEIGHT = %@,USER_WALKING_STEP_LENGH = %@, USER_RUNNING_STEP_LENGH = %@, USER_TIME_ZONE = %ld", [defaults objectForKey:USER_IS_FEMALE], (long)[[TGBleManager sharedTGBleManager] age], [defaults objectForKey:USER_WEIGHT],[defaults objectForKey:USER_HEIGHT],[defaults objectForKey:USER_WALKING_STEP_LENGH],[defaults objectForKey:USER_RUNNING_STEP_LENGH],(long)[defaults integerForKey:@"TimeZone"]);
    
    NSString *temp = [ NSString stringWithFormat:@"gender %@\nage %ld\nheight %@\nweight %@\nwalk_step_size %@\nrun_step_size %@\ntime_zone  %ld"
                      , [defaults objectForKey:USER_IS_FEMALE]
                      , (long)[[TGBleManager sharedTGBleManager] age]
                      , [defaults objectForKey:USER_HEIGHT]
                      , [NSString stringWithFormat:@"%ld",(long)[[defaults objectForKey:USER_WEIGHT]integerValue]*10]
                      , [defaults objectForKey:USER_WALKING_STEP_LENGH]
                      , [defaults objectForKey:USER_RUNNING_STEP_LENGH]
                      , (long)[defaults integerForKey:@"TimeZone"]
                     ];
    
    
    [NSString stringWithFormat:@"%ld",(long)[[defaults objectForKey:USER_WEIGHT]integerValue]*10];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];//去处需要的路径
    //创建文件管理器
    NSFileManager *fileManager = [NSFileManager defaultManager];
    // 写入数据：
    //获取文件路径
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"userPro.txt"];
    
    [fileManager removeItemAtPath:path error:nil];
    
    NSMutableData *writer = [[NSMutableData alloc] init];
    [writer appendData:[temp dataUsingEncoding:NSUTF8StringEncoding]];
    [writer writeToFile:path atomically:YES];
    
    
    
}


-(void)openDataBase{
    if (sqlite3_open([[CommonValues getDbPath ] UTF8String], &db)==SQLITE_OK)
    {
        [CommonValues setMyDB:db];
        NSLog(@"open sqlite db ok. App dele 打开数据库成功");
    }
    else
    {
        NSLog( @"can not open sqlite db  App dele 打开数据库失败" );
        
        //close database
        if(sqlite3_close(db) == SQLITE_OK){
            NSLog(@"App dele 关闭数据库成功");
        }
        else{
            NSLog(@"App dele 关闭数据库失败!!!!????!!!!!!");
        }
        
    }
    
    
}

-(void)closeDataBase{
    
    int closeResult = sqlite3_close([CommonValues getMyDB]);
    
    if(closeResult == SQLITE_OK){
        NSLog(@"App dele 关闭数据库成功");
    }
    else{
        NSLog(@"closeResult = %d",closeResult);
        NSLog(@"App dele 关闭数据库失败!!!!????!!!!!!");
    }
}

@end

//
//  ECGHistoryViewController.m
//  WAT
//
//  Created by neurosky on 6/27/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import "ECGHistoryViewController.h"
#import "CommonValues.h"
#import "CardioPlotViewController.h"
#import "FileOperation.h"

@interface ECGHistoryViewController ()
{
    NSMutableArray *ekgHistoryArr;
    NSMutableArray *ekgHistoryFileLengthArr;

    NSMutableArray *ekgRTArr;
    NSMutableArray *ekgRTFileLengthArr;
    
}
@end

@implementation ECGHistoryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    myWholeView.frame = CGRectMake(0, [CommonValues offsetHeight4DiffiOS], 320, 568-20);
    db = [CommonValues getMyDB];
 
    NSMutableArray * const ecgHistoryTimeArr =  /*[[NSMutableArray alloc]init];
    ecgHistoryTimeArr = */ [CommonValues getECGTimeArr:db :YES];
    
    NSMutableArray * const ecgRTTimeArr = /* [[NSMutableArray alloc]init];
    ecgRTTimeArr = */ [CommonValues getECGTimeArr:db :NO];
    
    
    df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"yyyy_MM_dd_HH_mm_ss"];
    
    ekgHistoryArr = [[NSMutableArray alloc]init];
    ekgRTArr =  [[NSMutableArray alloc]init];
    ekgRTFileLengthArr =  [[NSMutableArray alloc]init];
    ekgHistoryFileLengthArr = [[NSMutableArray alloc]init];
    
    
    float screenHeight = [CommonValues getScreenHeight];
    
    if (screenHeight == 568) {
        titleLabel.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
    }
    
    else
        titleLabel.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:25.0];
    
    NSArray *ekgList =  [FileOperation getFileArr:@"/TG_log/EKGData/"];
    //    NSLog(@"ekgList = %@",ekgList);
    
    for (int i = 0; i<ekgList.count ; i++) {
        
        NSString *fileName = [NSString stringWithFormat:@"%@",ekgList[i]];
        
        NSRange range =  [fileName rangeOfString:@"-RAW.txt" ];
        
        //        NSLog(@"range.location = %d,length = %d",range.location,range.length);
        if ((range.length) > 0) {
            NSFileManager* manager = [NSFileManager defaultManager];
            
            for (int j = 0; j<ecgHistoryTimeArr.count; j++) {
                NSRange range2 =  [fileName rangeOfString:[ecgHistoryTimeArr objectAtIndex:j] ];
                if ((range2.length) > 0){
                    [ ekgHistoryArr addObject:fileName];
                    //找到匹配时间的 raw.txt  ，跳出本次循环
                    NSString *path = [NSString stringWithFormat:@"%@/TG_log/EKGData/%@",[CommonValues getDocumentPath],fileName];
                    if ([manager fileExistsAtPath:path]) {
                      long fileSize =   [[manager attributesOfItemAtPath:path error:nil] fileSize];
                        
//                        NSLog(@"fileSize = %ld",fileSize);
                       [ekgHistoryFileLengthArr addObject:[NSString stringWithFormat:@"%ld",fileSize/1000]];
                    }
                    
//                    NSLog(@"path = %@",path);

                    break;
                    
                }
            }
            
            
            for (int j = 0; j<ecgRTTimeArr.count; j++) {
                NSRange range2 =  [fileName rangeOfString:[ecgRTTimeArr objectAtIndex:j] ];
                if ((range2.length) > 0){
                    [ ekgRTArr addObject:fileName];
                    //找到匹配时间的 raw.txt  ，跳出本次循环
                    NSString *path = [NSString stringWithFormat:@"%@/TG_log/EKGData/%@",[CommonValues getDocumentPath],fileName];
                    if ([manager fileExistsAtPath:path]) {
                        long fileSize =   [[manager attributesOfItemAtPath:path error:nil] fileSize];
                        
                        //                        NSLog(@"fileSize = %ld",fileSize);
                        [ekgRTFileLengthArr addObject:[NSString stringWithFormat:@"%ld",fileSize/1000]];
                    }
                    
                    break;
                    
                }
            }
            
            
            
            
        }
        else{
            
        }
        
    }
    
    NSLog(@"ekgHistoryArr = %@ \n ekgRTArr = %@",ekgHistoryArr,ekgRTArr);
    
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSUInteger)supportedInterfaceOrientations

{ // NSLog(@"Dash supportedInterfaceOrientations --- ");
    //   [[UIApplication sharedApplication]setStatusBarOrientation:UIInterfaceOrientationPortrait];
    
    return UIInterfaceOrientationMaskPortrait;
    
}
//
//
-(BOOL)shouldAutorotate{
    // NSLog(@"Dash shouldAutorotate --- ");
    return NO;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return  2;
}

//每个section显示的标题

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    switch (section) {
            
        case 0:
            
            return @"History ECG";
            
        case 1:
            
            return @"Realtime ECG";
            
        default:
            
            return @"Unknown";
            
    }
    
    
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger count = 0;
    
    if (section == 0) {
        count = ekgHistoryArr.count;
    }
    else if (section == 1){
        count = ekgRTArr.count;
    }
    else
    {
        NSLog(@"shouldn't excu here!!!! something should be wrong");
    }
    
    
    return count;
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *TableSampleIdentifier = @"TableSampleIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             TableSampleIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:TableSampleIdentifier];
    }
    cell.backgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"list"]];
    
    switch (indexPath.section) {
            
        case 0:
        {
            
            //    cell.textLabel.text = [ekgArr objectAtIndex:indexPath.row];
            //FFFFFFFFFFFFFF-2014_07_04_06_21_46Z-RAW.txt  截取表示时间的部分
            //ECED082-2014_07_06_11_58_43Z-RAW.txt
            NSString *fileLongName = [ekgHistoryArr objectAtIndex:indexPath.row];
            NSInteger totalLength =   fileLongName.length;
            //    NSString *timeStr  = [[ekgArr objectAtIndex:indexPath.row] substringWithRange:NSMakeRange(15,19) ];
            
            NSString * timeStr =[fileLongName substringWithRange:NSMakeRange(totalLength-9-19,19)];
            
            NSDate *date1 = [df dateFromString:timeStr];
            
            NSTimeInterval  ti =   [date1 timeIntervalSince1970];
            
            NSDate *date2 =      [NSDate dateWithTimeIntervalSince1970:(ti+[[df timeZone] secondsFromGMT])];
            
            NSString  *  localDateStr =     [df stringFromDate:date2];
            NSString *dateAndSize = [NSString stringWithFormat:@"%@  (%@KB)",localDateStr,[ekgHistoryFileLengthArr objectAtIndex:indexPath.row]];
            
            
            cell.textLabel.text = dateAndSize;
        }
            break;
            
        case 1:{
            NSString *fileLongName = [ekgRTArr objectAtIndex:indexPath.row];
            NSInteger totalLength =   fileLongName.length;
            //    NSString *timeStr  = [[ekgArr objectAtIndex:indexPath.row] substringWithRange:NSMakeRange(15,19) ];
            
            NSString * timeStr =[fileLongName substringWithRange:NSMakeRange(totalLength-9-19,19)];
            
            NSDate *date1 = [df dateFromString:timeStr];
            
            NSTimeInterval  ti =   [date1 timeIntervalSince1970];
            
            NSDate *date2 =      [NSDate dateWithTimeIntervalSince1970:(ti+[[df timeZone] secondsFromGMT])];
            
            NSString  *  localDateStr =     [df stringFromDate:date2];
            NSString *dateAndSize = [NSString stringWithFormat:@"%@  (%@KB)",localDateStr,[ekgRTFileLengthArr objectAtIndex:indexPath.row]];
            
            cell.textLabel.text = dateAndSize;
            
            
            
        }
            break;
        default:
            [[cell textLabel]  setText:@"Unknown"];
            break;
            
    }
    
    
    
    
    return cell;
    
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"didDeselectRowAtIndexPath row = %ld",(long)indexPath.row);
    
    [CommonValues setPlayBackMode:YES];
    
    switch (indexPath.section) {
        case 0:
            [CommonValues setSelectedEKG:[ekgHistoryArr objectAtIndex:indexPath.row]];
            [CommonValues setHistoryECGPlayBackMode:YES];
            break;
        case 1:
            [CommonValues setSelectedEKG:[ekgRTArr objectAtIndex:indexPath.row]];
             [CommonValues setHistoryECGPlayBackMode:NO];
            break;
        default:
            
            break;
            
            
            
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];//选中后的反显颜色即刻消失
    
    
    if ([CommonValues getScreenHeight] == 568) {
        CardioPlotViewController *cpvc = [[CardioPlotViewController alloc]initWithNibName:@"CardioPlotViewController_iphone5" bundle:nil] ;
        [self presentViewController:cpvc animated:YES
                         completion:nil];
    }
    
    else{
        
        CardioPlotViewController *cpvc = [[CardioPlotViewController alloc]initWithNibName:@"CardioPlotViewController_iphone4s" bundle:nil] ;
        [self presentViewController:cpvc animated:YES
                         completion:nil];
    }
    
}



- (IBAction)backClick:(id)sender {
    NSLog(@"play back view  back button click");
    
    [self dismissViewControllerAnimated:YES completion:nil];
}



@end

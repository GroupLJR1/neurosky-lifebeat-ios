//
//  TGBleManager_mfg.h
//
//  Copyright 2014 NeuroSky, Inc.. All rights reserved.
//
//  enumerated values for manufacturing/labratory commands
//
// MOST require a special build of the BLE SDK
//

enum {
    MFG_CommonTokenAndNoSnCheck = 1,
    MFG_ShipModeConfig = 2,
    MFG_OLED_TestConfig = 3,
    MFG_DisableAlarmsAndGoals = 4,
    MFG_Force4digitCodeToOLED = 5,
    MFG_UpdateBatteryLevel = 6,
    MFG_FWdownloadAdditionalFlashWriteDelay = 7, // parameter will be the number of 10 ms periods to wait
};
typedef NSUInteger TG_mfg_support;

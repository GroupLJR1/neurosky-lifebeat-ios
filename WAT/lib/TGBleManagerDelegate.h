/*
 *  TGBleManagerDelegate.h
 *
 *  Copyright 2014 NeuroSky, Inc.. All rights reserved.
 *
 *  Classes that act as the delegate for the TGBleManager need to implement
 *  this protocol. The TGBleManager fires off the messages in this protocol
 *  when the appropriate events are seen.
 *
 *  All methods are required.
 */

#import <Foundation/Foundation.h>

@protocol TGBleManagerDelegate <NSObject>

- (void)dataReceived:(NSDictionary *)data;

@optional

- (void)candidateFound:(NSString*) devName
                  rssi:(NSNumber*) rssi
                 mfgID:(NSString*) mfgID
           candidateID:(NSString*) candidateID;

- (void)potentialBond:(NSString*) code
                   sn:(NSString*) sn
              devName:(NSString*) devName;

enum {
    TGsyncResultNormal = 0,  // also check that available count matches received count
    TGsyncResultError = 1,
    TGsyncResultDisconnect = 2,
    TGsyncResultErrorFirmwareNotCompatible = 3,
    TGsyncResultErrorTimeOut = 4,
    TGsyncResultErrorBlankData = 5,
    TGsyncReusltEroorDataLost = 6,
    TGsyncResultErrorFlashCorrupt = 7,
};
typedef NSUInteger TGsyncResult;

- (void)transferInProgress:(bool) transfer; // History Sync
- (void)transferPercent:(int) percent;
- (void)transferReport:(TGsyncResult) result
             ped_avail:(int) ped_available // byte count available
              ped_recv:(int) ped_received // byte count actually received
             ekg_avail:(int) ekg_available
              ekg_recv:(int) ekg_received
            diag_avail:(int) diag_available
             diag_recv:(int) diag_received
           sleep_avail:(int) sleep_available
            sleep_recv:(int) sleep_received;

enum {
    TGeraseResultNormal = 0,
    TGeraseResultError = 1,
};
typedef NSUInteger TGeraseResult;
- (void)eraseComplete:(TGeraseResult) result;

enum {
    TGdownResultNormal = 0,
    TGdownResultError = 1,
    TGdownResultDisconnect = 2,
    TGdownResultNoFileFound = 3,
    TGdownFailedBatteryTooLowForDownload = 4,
    TGdownResultDisconnectWriteFailed = 5,
    TGdownResultWriteFailed = 6,
    TGdownResultWriteTimeOut = 7,
    TGdownResultErrorImproperImageFile = 8,
};
typedef NSUInteger TGdownResult;

- (void)fwUpdateInProgress:(bool) transfer;
- (void)fwUpdatePercent:(int) percent;
- (void)fwUpdateReport:(TGdownResult) result
                  size:(int)fileSize
              checksum:(unsigned int)checksum
              transfer:(int)transferCount;

- (void)batteryLevel:(int) percent;

- (void)ekgStarting:(NSDate*) time
         sampleRate:(int) sampleRate
           realTime:(bool) rt
            comment:(NSString*) comment;
- (void)ekgStop; // this delegate is deprecated, use the following replacement

enum {
    TGekgTerminatedNormally = 0,
    TGekgTerminatedNoData = 1, // RT recording - Time Out, no EKG data received, suspect no finger on the sensor
    TGekgTerminatedDataStopped = 2, // RT recording - suspect that user removed their finger
    TGekgTerminatedLostConnection = 3, // BLE connection lost
};
typedef NSUInteger TGekgResult;
- (void)ekgStop:(TGekgResult) result; // this delegate is depricated, use the following replacement

- (void)ekgStop:(TGekgResult) result
        finalHR:(int)finalHR; // for RealTime always zero, for History EKG the final HeartRate displayed on the band

- (void)ekgSample:(int) sample;


enum {
    TG_ACT_MOT_MVT_TYPE_UNKNOWN = 0,     // DATA not valid
    TG_ACT_MOT_MVT_TYPE_WALKING = 1,
    TG_ACT_MOT_MVT_TYPE_RUNNING = 2,
    TG_ACT_MOT_MVT_TYPE_reservedA = 3,   // NOT USED
    TG_ACT_MOT_MVT_TYPE_reservedB = 4,   // NOT USED
    TG_ACT_MOT_MVT_TYPE_reservedC = 5,   // NOT USED
    TG_ACT_MOT_MVT_TYPE_reservedD = 6,   // NOT USED
    TG_ACT_MOT_MVT_TYPE_SEDENTARY = 7
};
typedef NSUInteger TGpedometryActivityType;

enum {
    TG_ACT_MOT_SLEEP_PHASE_UNKNOWN = 0,          // NOT expected
    TG_ACT_MOT_SLEEP_PHASE_RESTLESS = 1,
    TG_ACT_MOT_SLEEP_PHASE_LIGHT = 2,
    TG_ACT_MOT_SLEEP_PHASE_DEEP = 3,
    TG_ACT_MOT_SLEEP_PHASE_WALK_AFTER_ONSET = 4, // NOT expected
};
typedef NSUInteger TGsleepPhaseType;


- (void)pedometryRecord:(NSDate*) time  // this delegate is deprecated, use the following replacement
                  steps:(int) steps
               calories:(int) calories
               distance:(int) distance
                  speed:(int) speed  // will always be zero
               speedBPM:(int) speedBPM
                 energy:(int) energy
                   mode:(TGpedometryActivityType) mode
             sleepPhase:(TGsleepPhaseType) sleepPhase;

- (void)pedometryRecord:(NSDate*) time  // this delegate is deprecated, use the following replacement
                  steps:(int) steps
               calories:(int) calories
               distance:(int) distance
         activeCalories:(int) activeCalories
               speedBPM:(int) speedBPM
                 energy:(int) energy
                   mode:(TGpedometryActivityType) mode
             sleepPhase:(TGsleepPhaseType) sleepPhase;

- (void)pedometryRecord:(NSDate*) time
                  steps:(int) steps
               calories:(int) calories
               distance:(int) distance
         activeCalories:(int) activeCalories
               stepBPM:(int) stepBPM
                 energy:(int) energy
                   mode:(TGpedometryActivityType) mode
             sleepPhase:(TGsleepPhaseType) sleepPhase;


- (void)sleepRecord:(NSDate*) time // this delegate is deprecated, use the following replacement
              phase:(TGsleepPhaseType) phase;

enum {
    TGsleepWakeButton = 0,
    TGsleepSleepButton = 1,
    TGsleepDailyReset = 2,
    TGsleepErase = 3,
    TGsleepInitialized = 4,
};
typedef NSUInteger TGsleepInitReason;

- (void)sleepRecord:(NSDate*) time
              phase:(TGsleepPhaseType) phase
           initCode:(TGsleepInitReason) code;

- (void)fatBurnRecord:(NSDate*) time // this delegate is deprecated, use the following replacement
            heartRate:(int) heartRate
              fatBurn:(int) fatBurn;

- (void)fatBurnRecord:(NSDate*) time
            heartRate:(int) heartRate
              fatBurn:(int) fatBurn
         trainingZone:(int) trainingZone;

- (void)currentCount:(NSDate*) time
               steps:(int) steps
            calories:(int) calories
            distance:(int) distance
              energy:(int) energy
                mode:(int) mode
                  hr:(int) hr;

- (void)diagEventRecord:(NSDate*) time
              eventCode:(int) eventCode;
//
// sleep analisys results
//

// this delegate is deprecated (why?)
enum {
    TGsleepResultNotValid = 0,
    TGsleepResultValid = 1,
    TGsleepResultNoData = 2,
    TGsleepResultNegativeTime = 3,
    TGsleepResultMoreThan48hours = 4,
    TGsleepResultAnalysisInProgress = 5,
};
typedef NSUInteger TGsleepResult;

- (void)sleepResults:(TGsleepResult) result  // this delegate is deprecated, use the sleepAnalysisResults as replacement
           startTime:(NSDate*) startTime
             endTime:(NSDate*) endTime
            duration:(int) duration  // in seconds
            preSleep:(int) preSleep // in seconds
            notSleep:(int) notSleep // in seconds active time
           deepSleep:(int) deepSleep // in seconds
          lightSleep:(int) lightSleep // in seconds
         wakeUpCount:(int) wakeUpCount // number of wake ups after 1st sleep
          totalSleep:(int) totalSleep
          efficiency:(int) efficiency // integer sleep efficiency = total sleeping/duration
            DEPRECATED_ATTRIBUTE;

- (void)sleepResults:(TGsleepResult) result // this delegate is deprecated, use the sleepAnalysisResults as replacement
           startTime:(NSDate*) startTime
             endTime:(NSDate*) endTime
            duration:(int) duration  // in seconds
            preSleep:(int) preSleep // in seconds
            notSleep:(int) notSleep // in seconds active time
           deepSleep:(int) deepSleep // in seconds
          lightSleep:(int) lightSleep // in seconds
         wakeUpCount:(int) wakeUpCount // number of wake ups after 1st sleep
          totalSleep:(int) totalSleep
             preWake:(int) preWake // number of seconds active before the end time
          efficiency:(int) efficiency; // integer sleep efficiency = total sleeping/duration

//- (void)sleepAnalysisResults:(NSDate *)startTime
//                 withEndTime:(NSDate *)endTime
//                withDuration:(int)duration
//           withAwakePresleep:(int)awakePresleep
//                withNotsleep:(int)notsleep
//               withDeepsleep:(int)deepsleep
//              withLightsleep:(int)lightsleep
//               withWakecount:(int)wakecount
//              withTotalsleep:(int)totalsleep
//          withAwakePostsleep:(int)awakePostsleep
//              withEfficiency:(int)efficiency
//                  withLength:(int)len
//               withSleepTime:(uint32_t*)sleeptime
//              withSleepPhase:(int8_t*)sleepphase;

- (void)sleepSmoothData:(int)samepleRate
              sleepTime:(NSMutableArray*)sleepTimeArray
             sleepPhase:(NSMutableArray*)sleepPhaseArray;


enum {
    TGBleUnexpectedEvent = 0,
    TGBleHistoryCorruptErased = 1,
    TGBleTestingEvent = 2,
    TGBleConfigurationModeCanNotBeChanged = 3,
    TGBleUserAgeHasBecomeNegative_BirthDateIsNOTcorrect = 4,
    TGBleUserAgeIsTooLarge_BirthDateIsNOTcorrect = 5,
    TGBleUserBirthDateRejected_AgeOutOfRange = 6, // rejected, age would be negative or greater than 255
    
    TGBleFailedOtherOperationInProgress = 7, // a SYNC, RT EKG, or FW Download is already in progress
    TGBleFailedSecurityNotInplace = 8,
    TGBleReInitializedNeedAlarmAndGoalCheck = 9, // band re-initialized, possible stale Alarm or Duration Goal was suppressed
    TGBleStepGoalRejected_OutOfRange = 10,
    TGBleCurrentCountRequestTimedOut = 11,
    TGBleDeprecatedMethodIsUsed = 12,
    TGBleConnectFailedSuspectKeyMismatch = 13, // target band has encryption configured
    TGBleNonRepeatingAlarmMayBeStaleCheckAlarmConfiguration = 14,
    TGBleBluetoothModuleError = 15,
    TGBlePossibleResetDetect = 16,
    TGBleNewConnectionEstablshed = 17,
};
typedef NSUInteger TGBleExceptionEvent;

- (void)exceptionMessage:(TGBleExceptionEvent) eventType;

- (void)bleDidConnect;
- (void)bleDidDisconnect; // disconnect because of application command
- (void)bleLostConnect; // connection lost because of: radio limit or other OS controled reason
- (void)bleDidAbortConnect;

enum {
    TGbondResultError = 0,
    TGbondResultTokenAccepted = 1,
    TGbondResultErrorBondedNoMatch = 2,  // means no token was received by the band, a communication error
    TGbondResultErrorBadTokenFormat = 3, // means invalid token or doesnt agree with current token
    TGbondResultErrorTimeOut = 4,
    TGbondResultErrorNoConnection = 5,
    TGbondResultErrorReadTimeOut = 6,
    TGbondResultErrorReadBackTimeOut = 7,
    TGbondResultErrorWriteFail = 8,
    TGbondResultErrorTargetIsAlreadyBonded = 9,
    TGbondAppErrorNoPotentialBondDelegate = 10,
    TGbondResultTokenReleased = 11,
    TGbondResultErrorUnSupportedHardware = 12,
    TGbondResultErrorTargetHasWrongSN = 13,
    TGbondResultErrorPairingRejected = 14, // iOS pairing dialog was rejected
    TGbondResultErrorSecurityMismatch = 15, // iOS can not renegotiate keys, device needs to be forgotten from the iOS Bluetooth menu
};
typedef NSUInteger TGbondResult;

- (void)bleDidBond:(TGbondResult) result;

enum {
    TGSendUnexpectedEvent = 0,
    TGSendSuccess = 1,
    TGSendFailedNoAcknowledgement = 2,
    TGSendFailedDisconnected = 3,
    TGSendSuccessTEST = 4, // test-mode only
};
typedef NSUInteger TGsendResult;

- (void)bleDidSendUserConfig:(TGsendResult) result;
- (void)bleDidSendAlarmGoals:(TGsendResult) result;



@end

/* 
 * TGLibEKGdelegate.h
 *
 *  Copyright 2014 NeuroSky, Inc.. All rights reserved.
 *
 *  Classes that act as the delegate for the TGLibEKG Manager need to implement
 *  this protocol. The TGLibEKG fires off the messages in this protocol
 *  when the appropriate events are seen.
 *
 *  All methods are required.
 */

#import <Foundation/Foundation.h>

@protocol TGLibEKGdelegate <NSObject>

- (void)dataReceived:(NSDictionary *)data;

enum {
    TGEKGsleepResultNotValid = 0,
    TGEKGsleepResultValid = 1,
    TGEKGsleepResultNoData = 2,
    TGEKGsleepResultNegativeTime = 3,
    TGEKGsleepResultMoreThan48hours = 4,
    TGEKGsleepResultAnalysisInProgress = 5,
};
typedef NSUInteger TGEKGsleepResult;

- (void)sleepAnalysisResults:(TGEKGsleepResult)result
               startTime:(NSDate *)startTime
                 endTime:(NSDate *)endTime
                duration:(int)duration          // in minutes
                preSleep:(int)preSleep          // in minutes
                notSleep:(int)notSleep          // in minutes
               deepSleep:(int)deepSleep         // in minutes
              lightSleep:(int)lightSleep        // in minutes
             wakeUpCount:(int)wakeUpCount       // number of wake ups after 1st sleep
              totalSleep:(int)totalSleep
                 preWake:(int)preWake           // number of minutes active before the endTime
              efficiency:(int)efficiency;       // integer sleep efficiency

-(void)sleepAnalysisSmoothData:(int)samepleRate
             sleepTime:(NSMutableArray*)sleepTimeArray
            sleepPhase:(NSMutableArray*)sleepPhaseArray;

//- (void)sleepAnalysisResults:(int)status
//               withStartTime:(NSDate *)startTime
//                 withEndTime:(NSDate *)endTime
//                withDuration:(int)duration
//           withAwakePresleep:(int)awakePresleep
//                withNotsleep:(int)notsleep
//               withDeepsleep:(int)deepsleep
//              withLightsleep:(int)lightsleep
//               withWakecount:(int)wakecount
//              withTotalsleep:(int)totalsleep
//          withAwakePostsleep:(int)awakePostsleep
//              withEfficiency:(int)efficiency
//                  withLength:(int)len
//               withSleepTime:(uint32_t*)sleeptime
//              withSleepPhase:(int8_t*)sleepphase
//              DEPRECATED_ATTRIBUTE;

//- (void)sleepDownsampleAnalysisResults:(int)status
//                           withMinutes:(int)minutes
//                         withStartTime:(NSDate *)startTime
//                           withEndTime:(NSDate *)endTime
//                            withLength:(int)len
//                         withSleepTime:(uint32_t*)sleeptime
//                        withSleepPhase:(int8_t*)sleepphase DEPRECATED_ATTRIBUTE;

@end

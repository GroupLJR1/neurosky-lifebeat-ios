//
//  TGBleManager.h
//
//  Copyright 2014 NeuroSky, Inc.. All rights reserved.
//
//  The TGBleManager class handles ThinkGear-enabled accessories connected to a device,
//  sending connect/disconnect and data receipt notifications to a designated delegate.
//

#import "TGBleManagerDelegate.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

#import "TGLibEKGdelegate.h"

enum {
    TGBleNotConnected = 0,
    TGBleConnected = 1,
    TGBleConnectedToBond = 2,
};
typedef NSUInteger TGBleConnectStatus;


@interface TGBleManager : NSObject <TGLibEKGdelegate, TGBleManagerDelegate, CBCentralManagerDelegate, CBPeripheralDelegate> {
    id<TGLibEKGdelegate> delegate;
}

@property (nonatomic,readonly) TGBleConnectStatus status;
@property (nonatomic,readonly,getter=isConnected) bool connected;

@property (nonatomic,readonly) int age;
@property (nonatomic) int birthYear;
@property (nonatomic) int birthDay;
@property (nonatomic) int birthMonth;
@property (nonatomic,getter=isFemale) bool female;
@property (nonatomic) NSInteger height;
@property (nonatomic) NSInteger weight;
@property (nonatomic) NSInteger walkingStepLength;
@property (nonatomic) NSInteger runningStepLength;
@property (nonatomic,getter=isDisplayImperialUnits) bool displayImperialUnits;
@property (nonatomic,getter=isDisplayTime24Hour) bool displayTime24Hour;
@property (nonatomic,getter=isBandOnRight) bool bandOnRight;
@property (nonatomic,getter=isAutoSyncEnabled) bool autoSyncEnabled;// NO LONGER USED - to be deleted


@property (nonatomic) int goalDurationHour;
@property (nonatomic) int goalDurationMinute;
@property (nonatomic) int goalDurationSecond;
@property (nonatomic) NSInteger goalSteps; // 0 to 65535, 0 = set to FW default (10,000)
@property (nonatomic) int alarmHour; // any value outside 0 to 23 disables the Alarm
@property (nonatomic) int alarmMinute; // any value outside 0 to 59 disables the Alarm
@property (nonatomic) int alarmRepeat;
// bit mask:  (only low order 8 bits have meaning, all others are ignored and should be set as zero)
// bit 0 = 0 - alarm functions once
//
// bit 0 = 1 - alarm repeats each day
// bit 1 = 1 - alarm repeat is suppressed on Monday
// bit 2 = 1 - alarm repeat is suppressed on Tuesday
// bit 3 = 1 - alarm repeat is suppressed on Wednesday
// bit 4 = 1 - alarm repeat is suppressed on Thursday
// bit 5 = 1 - alarm repeat is suppressed on Friday
// bit 6 = 1 - alarm repeat is suppressed on Saturday
// bit 7 = 1 - alarm repeat is suppressed on Sunday
//
// examples:
// 0x00 = disables repeat, if Alarm Hour and Minute are in range the Alarm functions once
// 0x11 = enables repeat Alarm every day except, Thursday

@property (nonatomic,readonly) NSString *advName;
@property (nonatomic,readonly) NSString *hwModel;
@property (nonatomic,readonly) NSString *hwManufacturer;
@property (nonatomic,readonly) NSString *hwSerialNumber;
@property (nonatomic,readonly) NSString *hwVersion;
@property (nonatomic,readonly) NSString *fwVersion;
@property (nonatomic,readonly) NSString *swVersion;
@property (nonatomic,readonly) NSString *sdkVersion;
@property (nonatomic,readonly) NSData *bondToken;
@property (nonatomic,readonly) NSString *mfgID;
@property (nonatomic,readonly) NSString *connectedID;
@property (nonatomic,readonly) int batteryLevel;

@property (nonatomic, assign) id<TGBleManagerDelegate> delegate;

#pragma mark -
#pragma mark Messages

+ (TGBleManager *)sharedTGBleManager;
- (void)setupManager; // auto connect mode
- (void)setupManager:(bool) manualMode; // true = manual mode, false = auto connect
// can not be changed after setupManager is called
- (void)teardownManager;
- (int)getVersion;
- (NSString *)getProductId;
//
// to autoconnect to the closest device with one of the nameList names
//
- (void)tryConnect:(NSArray*) nameList;
- (void)tryReconnect;  // testing only
//
// alternate manual discovery of the available candidates which have one of the nameList names
// allowing the application to choose a candidate for connection
//
// candidates are delivered to the application through the candidateFound delegate
//
- (void)candidateScan:(NSArray*) nameList;
- (void)candidateConnect:(NSString*) candidateID;
- (void)candidateStopScan;
//
//
- (void)tryBond;
- (void)takeBond;
- (void)adoptBond:(NSData*) bondToken serialNumber:(NSString*) sn;
- (void)releaseBond;
- (void)tryDisconnect;
- (void)trySetSecurity; // to be deleted, no longer functional
- (void)tryStartRT;
- (void)tryStopRT;
- (void)trySyncData;
- (void)trySyncData:(bool) all;
                // if all == true then all of the SYNC data received is transfered to the application
                // if all == false then only new SYNC data is transfered to the application
- (void)tryEraseData;

- (void)tryResetCounters; // testing only
- (void)tryFwDown;
- (void)askCurrentCount;

- (void)sleepInitAnalysis:(NSDate*) startTime endTime:(NSDate *) endTime DEPRECATED_ATTRIBUTE;
- (void)sleepInitAnalysis;
- (void)sleepAddData:(NSDate*) sampleTime sleepPhase:(int) sleepPhase;
- (void)sleepRequestAnalysis DEPRECATED_ATTRIBUTE;
- (void)sleepRequestAnalysis: (NSDate*) startTime endTime:(NSDate *) endTime;
- (void)sleepSetInterval:(int)minutes completeData:(bool) fullDataSet;

- (void)enableLogging; // enable additional NSLog messages
- (void)stopLogging; // disable the additional messages
- (void)redirectLoggingToFile; // redirect ALL NSLog message to a file in TG_log/Console
- (void)restoreLoggingToConsole; // return NSLog message to the original file (console/stderr)
- (void)diagnosticFilesEnabled; // NSK only, DO NOT USE, enable writing to files in the TG_log directory
- (void)diagnosticFilesDisabled:(bool) option; // NSK only, DO NOT USE disable writing to files in the TG_log directory

- (BOOL)mfg_support:(int) arg1 arg2:(int)arg2; // NSK only

- (BOOL)resetBand;

- (int)computeHRVNow;

- (void)enableVBM;//test only
- (void)disableVBM;//test only


@end

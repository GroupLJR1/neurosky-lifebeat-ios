/* 
 * TGLibEKGdictionary.h
 *
 *  Copyright 2014 NeuroSky, Inc.. All rights reserved.
 *
 *  The TGLibEKG fires off the messages using these keys
 *  when the appropriate events are seen.
 *
 *  All methods are required.
 */

#import <Foundation/Foundation.h>
/*
 * key names for the dataDictionary
 */
static NSString* dicEKGsmoothedWave             = @"smoothedWave";
static NSString* dicEKGr2rInt                   = @"rrInt";
static NSString* dicEKGr2rInt_TS                = @"rrInt_TS";
static NSString* dicEKGrpeakDetected            = @"RpeakDetected";
static NSString* dicEKGrpeakDetected_TS         = @"RpeakDetected_TS";
static NSString* dicEKGhrv                      = @"CardioZoneHRV";
static NSString* dicEKGhrv_TS                   = @"CardioZoneHRV_TS";
static NSString* dicEKGheartRate                = @"CardioZoneHeartRate";
static NSString* dicEKGheartRate_TS             = @"CardioZoneHeartRate_TS";
static NSString* dicEKGmood                     = @"Mood";
static NSString* dicEKGmood_TS                  = @"Mood_TS";
static NSString* dicEKGtrainingZone             = @"TrainingZone";
static NSString* dicEKGtrainingZone_TS          = @"TrainingZone_TS";
static NSString* dicEKGheartLevel               = @"HeartLevel";
static NSString* dicEKGheartLevel_TS            = @"HeartLevel_TS";
static NSString* dicEKGheartAge                 = @"HeartAge";
static NSString* dicEKGheartAge_TS              = @"HeartAge_TS";
static NSString* dicEKGstress                   = @"Stress";
static NSString* dicEKGstress_TS                = @"Stress_TS";
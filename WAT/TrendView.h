//
//  TrendView.h
//  WAT
//
//  Created by neurosky on 7/8/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"

@interface TrendView : UIView<CPTPlotDataSource,CPTAxisDelegate>
- (id)initWithFrame:(CGRect)frame dataArr:(NSMutableArray *)arr ;

@end

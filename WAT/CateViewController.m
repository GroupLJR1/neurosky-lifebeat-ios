//
//  CateViewController.m
//  top100
//
//  Created by Dai Cloud on 12-7-11.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "CateViewController.h"
#import "SubCateViewController.h"
#import "CateTableCell.h"
#import "UIFolderTableView.h"
#define DATA_PATH @"EKGData"
@interface CateViewController () <UIFolderTableViewDelegate>

@property (strong, nonatomic) SubCateViewController *subVc;
@property (strong, nonatomic) NSDictionary *currentFile;

@end

@implementation CateViewController

@synthesize fileList=_fileList;
@synthesize subVc=_subVc;
@synthesize currentFile=_currentFile;
@synthesize tableView=_tableView;


-(NSArray *)fileLists
{
//    NSBundle *myBundle = [NSBundle mainBundle];
    
//     NSLog(@"myBundle  = %@",[myBundle bundlePath]);
//    
//    NSString *pathStr =  [myBundle pathForResource:@"2013-05-14_14_36"ofType:@"plist"];
//    NSLog(@"myBundle pathStr = %@",pathStr );
    
    
    if (_fileList == nil){
        _fileList = [[NSArray alloc] initWithArray:[self listFilesInDocumentsSubFolder:DATA_PATH]];
    }    
    return _fileList;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
   
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.fileLists.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    static NSString *CellIdentifier = @"cate_cell";
//    CateTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];    
//    if (cell == nil) {
//        cell = [[[CateTableCell alloc] initWithStyle:UITableViewCellStyleSubtitle 
//                                      reuseIdentifier:CellIdentifier] autorelease];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
// 
//    }
//    NSDictionary *cate = [self.fileLists objectAtIndex:indexPath.row];
//    cell.logo.image = [UIImage imageNamed:[[cate objectForKey:@"imageName"] stringByAppendingString:@".png"]];
//    cell.title.text = [cate objectForKey:@"name"];
//    
//    NSMutableArray *subTitles = [[NSMutableArray alloc] init];
//    NSArray *subClass = [cate objectForKey:@"subClass"];
//    for (int i=0; i < MIN(4,  subClass.count); i++) {
//        [subTitles addObject:[[subClass objectAtIndex:i] objectForKey:@"name"]];
//    }
//    cell.subTtile.text = [subTitles componentsJoinedByString:@"/"];
//    [subTitles release];
    static NSString *CellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                       reuseIdentifier:CellIdentifier] ;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    [cell.textLabel setText:[self.fileList objectAtIndex:[indexPath row]]];
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

//    SubCateViewController *subVc = [[SubCateViewController alloc] 
//                  initWithNibName:NSStringFromClass([SubCateViewController class]) 
//                  bundle:nil] ;
//    self.currentFile = [self.fileList objectAtIndex:[indexPath row]];
//    NSLog(@"filename------%@",self.currentFile);
//    NSString *folderName = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:DATA_PATH];
//    
//    NSBundle *myBundle = [NSBundle mainBundle];
//    
//   NSString *pathStr =  [myBundle pathForResource:@"DataPath"ofType:@"txt"];
//    NSLog(@"myBundle pathStr = %@",pathStr );
//    
//    
//    
//    NSLog(@"folderName = %@",folderName );
//    
//    
//    NSLog(@"myBundle  = %@",[myBundle bundlePath]);
//    NSString *dir = [[myBundle bundlePath]stringByAppendingPathComponent:@"Data"];
//
//    
//    
//    subVc.fileURL = [[NSString alloc]initWithFormat:@"%@/%@",dir,self.currentFile] ;
//    subVc.cateVC = self;
//    
//    self.tableView.scrollEnabled = NO;
//    UIFolderTableView *folderTableView = (UIFolderTableView *)tableView;
//    [folderTableView openFolderAtIndexPath:indexPath WithContentView:subVc.view 
//                                 openBlock:^(UIView *subClassView, CFTimeInterval duration, CAMediaTimingFunction *timingFunction){
//                                     // opening actions
//                                 } 
//                                closeBlock:^(UIView *subClassView, CFTimeInterval duration, CAMediaTimingFunction *timingFunction){
//                                    // closing actions
//                                } 
//                           completionBlock:^{
//                               // completed actions
//                               self.tableView.scrollEnabled = YES;
//                           }];
    SubCateViewController *subVc = [[SubCateViewController alloc]
                                     initWithNibName:NSStringFromClass([SubCateViewController class])
                                     bundle:nil];
    self.currentFile = [self.fileList objectAtIndex:[indexPath row]];
    NSLog(@"filename------%@",self.currentFile);
    NSString *folderName = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:DATA_PATH];
    subVc.fileURL = [[NSString alloc]initWithFormat:@"%@/%@",folderName,self.currentFile];
    subVc.cateVC = self;
    
    self.tableView.scrollEnabled = NO;
    UIFolderTableView *folderTableView = (UIFolderTableView *)tableView;
    [folderTableView openFolderAtIndexPath:indexPath WithContentView:subVc.view
                                 openBlock:^(UIView *subClassView, CFTimeInterval duration, CAMediaTimingFunction *timingFunction){
                                     // opening actions
                                 }
                                closeBlock:^(UIView *subClassView, CFTimeInterval duration, CAMediaTimingFunction *timingFunction){
                                    // closing actions
                                }
                           completionBlock:^{
                               // completed actions
                               self.tableView.scrollEnabled = YES;
                           }];
}

//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return 80;
//}

-(CGFloat)tableView:(UIFolderTableView *)tableView xForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

-(void)subCateBtnAction:(UIButton *)btn
{
//    NSDictionary *subCate = [[self.currentFile objectForKey:@"subClass"] objectAtIndex:btn.tag];
//    NSString *name = [subCate objectForKey:@"name"];
//    UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:@"子类信息"
//                                                         message:[NSString stringWithFormat:@"名称:%@, ID: %@", name, [subCate objectForKey:@"classID"]]
//                                                        delegate:nil
//                                               cancelButtonTitle:@"确认"
//                                               otherButtonTitles:nil];
//    [Notpermitted show];
//    [Notpermitted release];
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [self setTableView:nil];
    [super viewDidUnload];
}

- (IBAction)back:(id)sender {
   
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(NSArray *) listFilesInDocumentsSubFolder:(NSString *)subFolderName{

//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    //在这里获取应用程序Documents文件夹里的文件及文件夹列表
////    NSString *dir = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:subFolderName];
//    
//    NSBundle *myBundle = [NSBundle mainBundle];
//    
//    NSLog(@"myBundle  = %@",[myBundle bundlePath]);
//    NSString *dir = [[myBundle bundlePath]stringByAppendingPathComponent:@"Data"];
//    
////    NSLog(@"dir----->%@",dir);
//    NSError *error = nil;
//    //fileList便是包含有该文件夹下所有文件的文件名及文件夹名的数组
//    NSArray *list = [fileManager contentsOfDirectoryAtPath:dir error:&error];
//    NSLog(@"Every Thing in the dir:%@",list);
//    //    NSMutableArray *dirArray = [[NSMutableArray alloc] init];
//    //    BOOL isDir = NO;
//    //    //在上面那段程序中获得的fileList中列出文件夹名
//    //    for (NSString *file in list) {
//    //        NSString *path = [dir stringByAppendingPathComponent:file];
//    //        [fileManager fileExistsAtPath:path isDirectory:(&isDir)];
//    //        if (isDir) {
//    //            [dirArray addObject:file];
//    //        }
//    //        isDir = NO;
//    //    }
//    //    NSLog(@"All folders:%@",dirArray);
//    //    [dirArray release];
//    return list;
    
    
    
    
    
    
    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    //在这里获取应用程序Documents文件夹里的文件及文件夹列表
    NSString *dir = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:subFolderName];
    //    NSLog(@"dir----->%@",dir);
    NSError *error = nil;
    //fileList便是包含有该文件夹下所有文件的文件名及文件夹名的数组
    NSArray *list = [fileManager contentsOfDirectoryAtPath:dir error:&error];
    NSLog(@"Every Thing in the dir:%@",list);
    //    NSMutableArray *dirArray = [[NSMutableArray alloc] init];
    //    BOOL isDir = NO;
    //    //在上面那段程序中获得的fileList中列出文件夹名
    //    for (NSString *file in list) {
    //        NSString *path = [dir stringByAppendingPathComponent:file];
    //        [fileManager fileExistsAtPath:path isDirectory:(&isDir)];
    //        if (isDir) {
    //            [dirArray addObject:file];
    //        }
    //        isDir = NO;
    //    }
    //    NSLog(@"All folders:%@",dirArray);
    //    [dirArray release];
    return list;
}

@end

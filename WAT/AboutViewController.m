//
//  AboutViewController.m
//  WAT
//
//  Created by  NeuroSky on 8/5/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import "AboutViewController.h"
#import "CommonValues.h"
#import "TGLibEKG.h"
#import "Constant.h"

#import "TGBleEmulator.h"
#import "TGLibEmulator.h"

@interface AboutViewController ()

@end

@implementation AboutViewController

- ( void ) dataReceived: ( NSDictionary * ) data { return; }

- ( void ) touchtouchForDismiss { return; }

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void) candidateFound:(NSString *)devName rssi:(NSNumber *)rssi mfgID:(NSString *)mfgID candidateID:(NSString *)candidateID
{
    NSString* message = [NSString stringWithFormat:
                         @"Device Name: %@\nMfg Data: %@\nID: %@\nRSSI value: %@",
                         devName,
                         mfgID,
                         candidateID,
                         rssi
                         ];
    NSLog(@"SELECT DEVICE candidateFound delegate: %@", message);
    
    
    if ([candIDArray containsObject:candidateID]) {
        return;
    }
    
    if (!firstEnterCandiDel) {
        NSLog(@"first enter candidate Found");
        firstEnterCandiDel = YES;
        if ([timeOutTimer isValid]) {
            [timeOutTimer invalidate];
            NSLog(@"timeOutTimer invalidate----");
            
        }
        [SVProgressHUD dismiss];
        listView = [[ZSYPopoverListView alloc] initWithFrame:CGRectMake(0, 0, 300, 400)];
        listView.tagert =self;
        listView.stopEBLScan =@selector(stopScan);
        listView.datasource = self;
        listView.delegate = self;
        showListTimer =  [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(delay2ShowList) userInfo:nil repeats:NO];
        
    }
    
    
    [devicesArray addObject:devName];
    [mfgDataArray addObject:mfgID];
    [rssiArray addObject:rssi];
    [candIDArray addObject:candidateID];
    [devicesIDArray addObject:candidateID];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [listView reload];
        
        
    });
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initVariables];
    [self initUI];
    
    
}

-(void)initVariables{
    CGRect rect_screen = [[UIScreen mainScreen]bounds];
    CGSize size_screen = rect_screen.size;
    screenHeight = size_screen.height;
    NSLog(@"screenHeight = %f",screenHeight);
    lostBeforeMainOperation = YES;
    devicesArray  = [[NSMutableArray alloc]init];
    mfgDataArray = [[NSMutableArray alloc]init];
    rssiArray = [[NSMutableArray alloc]init];
    candIDArray = [[NSMutableArray alloc]init];
    devicesIDArray  = [[NSMutableArray alloc]init];
    [[TGBleManager sharedTGBleManager] setDelegate:self];
    cannotConfirmUpdateAlert = [[UIAlertView alloc] initWithTitle:CANNOT_CONFIRM_UPDATE_TITLE message:CANNOT_CONFIRM_UPDATE_MSG delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    
    
}

-(void)initUI{
    myWholeView.frame = CGRectMake(0, [CommonValues offsetHeight4DiffiOS], 320, 568-20);
    // Do any additional setup after loading the view from its nib.
    if (screenHeight == 568) {
        title_label.font =  [UIFont fontWithName: DEFAULT_FONT_NAME size:30.0];
        progressPercentLabel.font =  [UIFont fontWithName: DEFAULT_FONT_NAME size:15.0];
    }
    else
    {
        title_label.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:25.0];
        progressPercentLabel.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:15.0];
    }
    proView.hidden = YES;
    
    if ( [CommonValues getAboutUpdateProgress] == 0.0) {
        progress_bg_img.hidden = YES;
        progressbar.hidden = YES;
        progressPercentLabel.hidden = YES;
    }
    else{
        progress_bg_img.hidden = NO;
        progressbar.hidden = NO;
        progressPercentLabel.hidden = NO;
        [updateBtn setImage:[UIImage imageNamed:@"update_2"] forState:UIControlStateNormal];
        [updateBtn setUserInteractionEnabled:NO];
    }
    [progressbar setImage:[UIImage imageNamed:@"about_progress"]];
    progressbar.verticalProgress = NO;
    progressbar.hasGrayscaleBackground = NO;
    progressbar.progress = [CommonValues getAboutUpdateProgress];
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *App_version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString  *ModelNum = [defaults stringForKey:@"ModelNum"];
    NSString  *SerialNum = [defaults stringForKey:@"SerialNum"];
    NSString  *FW_Version = [defaults stringForKey:@"FW_Version"];
    NSString  *HW_Version = [defaults stringForKey:@"HW_Version"];
    NSString  *SW_Version = [defaults stringForKey:@"SW_Version"];
    NSString  *Manufacturer = [defaults stringForKey:@"Manufacturer"];
    NSString  *SDK_version = [defaults stringForKey:@"SDK_version"];
    UIFont *commonFont =  [UIFont fontWithName: DEFAULT_FONT_NAME size:20.0];
    
    NSString *fwVersionFromSDK =   [[TGBleManager sharedTGBleManager]fwVersion];
    NSLog(@"fwVersionFromSDK = %@",fwVersionFromSDK);
    NSInteger const storedBatteryLevel = [defaults integerForKey:LATEST_BATTERY];
    
    
    _ModelNumLabel.text  = ModelNum;
    _SerialNumLabel.text = SerialNum;
    _FW_VersionLabel.text = FW_Version;
    _FW_VersionLabel.font = commonFont;
    _HW_VersionLabel.text = HW_Version;
    _HW_VersionLabel.font = commonFont;
    _SW_VersionLabel.text = SW_Version;
    _SW_VersionLabel.font = commonFont;
    _ManufacturerLabel.text = Manufacturer;
    _AppVersion.text = App_version;
    _AppVersion.font = commonFont;
    _CZVerison.text = SDK_version;
    batteryLabel.text = [NSString stringWithFormat:@"%ld%%",(long)storedBatteryLevel];
    
    int bleVersion = [[TGBleManager sharedTGBleManager]getVersion];
    ble_value_label.font = commonFont;
    ble_value_label.text = [NSString stringWithFormat:@"%d",bleVersion];
    TGLibEKG *myTGlib = [[TGLibEKG alloc]init];
    int algoVersion = [myTGlib getVersion];
    algo_value_label.text = [NSString stringWithFormat:@"%d",algoVersion];
    algo_value_label.font = commonFont;
    
    appVersionLabel.font = commonFont;
    sdkVersionLabel.font = commonFont;
    hrVersionLabel.font = commonFont;
    fwVersionLabel.font = commonFont;
    algoVersionLabel.font = commonFont;
    batteryLabel.font = commonFont;
    [self updateUI];
    
    
}


-(void)viewWillAppear:(BOOL)animated{
    
    [ super viewWillAppear: animated ];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) fwUpdateInProgress:(bool)transfer {
    if (transfer) {
        NSLog(@"FW update started");
        
        dispatch_async( dispatch_get_main_queue(), ^ {
            [SVProgressHUD dismiss];
            progress_bg_img.hidden = NO;
            progressbar.hidden = NO;
            progressPercentLabel.hidden = NO;
            updatingFW = YES;
            //            [self.view setUserInteractionEnabled:YES];
            [updateBtn setUserInteractionEnabled:NO];
        });
    }
    else {
        NSLog(@"FW update STOPPED");
        [CommonValues setAboutUpdateProgress:0.0];
        dispatch_async( dispatch_get_main_queue(), ^ {
            progressbar.hidden = YES;
            progress_bg_img.hidden = YES;
            progressPercentLabel.hidden = YES;
            [updateBtn setUserInteractionEnabled:YES];
            
        });
        
    }
}
-(void) fwUpdatePercent:(int)percent {
    float fpercent = (float) percent / 100.0;
    [CommonValues setAboutUpdateProgress:fpercent];
    NSLog(@">>>>>>>-----FW update: percent complete: %d -- %f", percent, fpercent);
    dispatch_async( dispatch_get_main_queue(), ^ {
        //        proView.progress = fpercent;
        progressbar.progress = fpercent;
        progressPercentLabel.text = [NSString stringWithFormat:@"%d %%",percent];
    }
                   );
    
    
    
}

-(void) fwUpdateReport:(TGdownResult)result size:(int)fileSize checksum:(unsigned int)checksum transfer:(int)transferCount {
    
    NSLog(@">>>>>>>-----FW update ENDDED: result code: %ld", (unsigned long)result);
    NSLog(@">>>>>>>-----FW update -  file size: %d", fileSize);
    NSLog(@">>>>>>>-----FW update -   checksum: %u", checksum);
    NSLog(@">>>>>>>-----FW update - xfer count: %d", transferCount);
    
    lostBeforeMainOperation = NO;
    NSString *title;
    NSString *msg;
    title = FWUpdateErrorTitle;
    
    [CommonValues setHasTaskFlag:NO];
    if (result == TGdownResultNormal) {
        //        title = @"Finished!";
        //        msg = @"Firmware Download completed.";
        title = RECONNECT_AFTER_FW_UPDATE_TITLE;
        msg = RECONNECT_AFTER_FW_UPDATE_MSG;
        //        msg_msg = @"Please plug to the USB power source to active your band";
        
        if ([CommonValues isBackgroundFlag]) {
            NSLog(@"Firmware Download 完成，且在后台，应断开连接");
            NSLog(@"call tryDisconnect ------");
            [[TGBleManager sharedTGBleManager] tryDisconnect];
        }
        if ([CommonValues iOSVersion] >= IOS8) {
            alertController = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                [self reconnectAlertCancel];
                
            }];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Connect" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self reconnectAlertOK];
            }];
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:msg delegate:self cancelButtonTitle:@" Cancel" otherButtonTitles:@"Connect", nil];
            alert.tag = RECONNECTTAG;
            dispatch_async(dispatch_get_main_queue(), ^{
                [alert show];
            });
        }
        return;
        
    }
    else if(result == TGdownResultError){
        
        
        msg = FWUpdateErrorMsg;
    }
    else if(result == TGdownResultDisconnect){
        msg = FWDownDisconnectMsg;
    }
    
    
    else if(result == TGdownResultNoFileFound){
        title = BinFileMissingTitle;
        msg = BinFileMissingMsg;
    }
    else if(result == TGdownFailedBatteryTooLowForDownload){
        title =  BatteryLowTitle;
        msg = BatteryLowMsg;
    }
    
    else if(result == TGdownResultDisconnectWriteFailed){
        
        msg = FWDownDisconnectMsg;
    }
    
    
    else if(result == TGdownResultWriteFailed){
        msg = FWDownWriteFailedMsg;
    }
    
    else if(result == TGdownResultWriteTimeOut){
        msg = FWDownWriteTimeOutMsg;
    }
    
    
    else if(result == TGdownResultErrorImproperImageFile){
        msg = FWDownErrorImproperImageFileMsg;
    }
    
    
    
    else {
        title = UnkonwnBondErrorTitle;
    }
    
    
    firstEnterCandiDel = NO;
    UIAlertView *alert;
    if ([CommonValues iOSVersion] >= IOS8) {
        alertController = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:okAction];
        
    }
    else{
        
        alert = [[UIAlertView alloc]initWithTitle:title message:msg delegate:nil cancelButtonTitle:@" OK " otherButtonTitles:nil, nil];
    }
    
    dispatch_async( dispatch_get_main_queue(), ^ {
        [SVProgressHUD dismiss];
        
        [CommonValues setAboutUpdateProgress:0.0];
        [updateBtn setImage:[UIImage imageNamed:@"update_1"] forState:UIControlStateNormal];
        
        [self.view setUserInteractionEnabled:YES];
        if ([CommonValues iOSVersion] >= IOS8){
            [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
        }
        else{
            [alert show];
        }
        
    });
    
    updatingFW = NO;
    //    if (result == 0) {
    //        NSLog(@"call tryDisconnect------");
    //         [[TGBleManager sharedTGBleManager] tryDisconnect];
    //    }
}

- (NSUInteger)supportedInterfaceOrientations

{ // NSLog(@"Dash supportedInterfaceOrientations --- ");
    //   [[UIApplication sharedApplication]setStatusBarOrientation:UIInterfaceOrientationPortrait];
    
    return UIInterfaceOrientationMaskPortrait;
    
}
//
//

- (void) bleLostConnect {
    NSLog(@"bleLostConnect---------");
    if (isReleaseBondDone ) {
        isReleaseBondDone = NO;
        NSLog(@"连接上的情况下 release bond ,引起的lost connect ，不需要处理，直接return");
        return;
    }
    
    dispatch_async( dispatch_get_main_queue(), ^ {
        [updateBtn setImage:[UIImage imageNamed:@"update_1"] forState:UIControlStateNormal];
    });
    /*
     [CommonValues setAboutUpdateProgress:0.0];
     [updateBtn setImage:[UIImage imageNamed:@"update_1"] forState:UIControlStateNormal];
     
     [SVProgressHUD showErrorWithStatus:@"Lost connection"];
     [self.view setUserInteractionEnabled:YES];
     
     */
    
    /*
     firstEnterCandiDel = NO;
     [CommonValues setAboutUpdateProgress:0.0];
     [updateBtn setImage:[UIImage imageNamed:@"update_1"] forState:UIControlStateNormal];
     
     
     dispatch_async( dispatch_get_main_queue(), ^ {
     [SVProgressHUD dismiss];
     [self.view setUserInteractionEnabled:YES];
     });
     
     */
    
    
    if ([timeOutTimer isValid]) {
        [timeOutTimer invalidate];
        NSLog(@"timeOutTimer invalidate----");
        
    }
    
    
    if (lostBeforeMainOperation ) {
        NSLog(@"want Firmware Download,lost connection before Main operation");
        //NSString *title = @"Lost Connection";
        NSData *token = [[TGBleManager sharedTGBleManager] bondToken];
        if (token == NULL) {
            if ([CommonValues iOSVersion] >= IOS8) {
                alertController = [UIAlertController alertControllerWithTitle:LostConnectTitle message:LostConnectMsg preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                    [self noTokenBondFailedCancel];
                    
                }];
                UIAlertAction *retryAction = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self noTokenBondFailedRetry];
                }];
                UIAlertAction *tryAnotherAction = [UIAlertAction actionWithTitle:@"Try another band" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self noTokenBondFailedTryAnother];
                }];
                [alertController addAction:cancelAction];
                [alertController addAction:retryAction];
                [alertController addAction:tryAnotherAction];
                [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
            }
            else{
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:LostConnectTitle message:LostConnectMsg
                                                              delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry",@"Try another band", nil];
                alert.tag = noTokenBondFailedTag;
                dispatch_async( dispatch_get_main_queue(), ^ {
                    [alert show];
                });
            }
        }
        
        else{
            if ([CommonValues iOSVersion] >= IOS8) {
                alertController = [UIAlertController alertControllerWithTitle:LostConnectTitle message:LostConnectMsg preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                    [self hasTokenBondFailedCancel];
                    
                }];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self hasTokenBondFailedRetry];
                }];
                [alertController addAction:cancelAction];
                [alertController addAction:okAction];
                [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
            }
            else{
                UIAlertView *alert_hasToken = [[UIAlertView alloc]initWithTitle:LostConnectTitle message:LostConnectMsg delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry",nil];
                
                alert_hasToken.tag = hasTokenBondFailedTag;
                dispatch_async( dispatch_get_main_queue(), ^ {
                    //                [self.view setUserInteractionEnabled:YES];
                    //                [SVProgressHUD dismiss];
                    [alert_hasToken show];
                });
            }
            
        }
        
        
    }
    else{
        dispatch_async( dispatch_get_main_queue(), ^ {
            
            [SVProgressHUD dismiss];
            [self.view setUserInteractionEnabled: YES];
        });
    }
    
    
    
}
- (BOOL)shouldAutorotate{
    // NSLog(@"Dash shouldAutorotate --- ");
    return NO;
}


- (BOOL)shouldAutomaticallyForwardRotationMethods{
    NSLog(@"shouldAutomaticallyForwardRotationMethods---");
    return NO;
}

- (void)viewDidUnload {
    [self setModelNumLabel:nil];
    [self setSerialNumLabel:nil];
    [self setFW_VersionLabel:nil];
    [self setHW_VersionLabel:nil];
    [self setSW_VersionLabel:nil];
    [self setManufacturerLabel:nil];
    [self setAppVersion:nil];
    [self setCZVerison:nil];
    [super viewDidUnload];
}
- (IBAction)backClick:(id)sender {
    NSLog(@"about backClick--");
    isReleaseBondDone = NO;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)fwUpdateClick:(id)sender {
    NSLog(@"fwUpdateClick--");
    isReleaseBondDone = NO;
    if ([CommonValues getAutoSyncingFlag]) {
        NSLog(@"auto syncing, main operation need wait  ");
        [[CommonValues autoSyncingAlert] show];
        return;
    }
    
    
    if (![CommonValues getBluetoothOnFlag]) {
        NSLog(@"BT OFF, can't go on updating FW");
        [self showBTNotOnAlert];
        return;
    }
    
    
    //show fw update confirm dialog
    if ([CommonValues iOSVersion] >= IOS8) {
        alertController = [UIAlertController alertControllerWithTitle:UPDATE_FW_WARNING_TITLE message:UPDATE_FW_WARNING_MSG preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
            [self fwDownAlertCancel];
            
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self fwDownAlertOK];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:UPDATE_FW_WARNING_TITLE message:UPDATE_FW_WARNING_MSG delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
        alert.tag = fwDownAlertTag;
        [alert show];
        
    }
}

-(void)connectFromStoredDeviceID{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *candId = [defaults objectForKey:@"DeviceID"];
    NSLog(@"call candidateConnect ----");
    [[TGBleManager sharedTGBleManager] candidateConnect:candId];
    [SVProgressHUD showWithStatus:@" Connecting"];
    [self.view setUserInteractionEnabled:NO];
    
}

- (IBAction)identifyBtnClick:(id)sender {
    NSLog(@"identifyBtnClick----");
    isReleaseBondDone = NO;
    if ([CommonValues getAutoSyncingFlag]) {
        NSLog(@"auto syncing, main operation need wait  ");
        [[CommonValues autoSyncingAlert] show];
        return;
    }
    [CommonValues setHasTaskFlag:YES];
    
    NSData *sdkToken =  [[TGBleManager sharedTGBleManager] bondToken];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([[TGBleManager sharedTGBleManager] status] == TGBleConnectedToBond) {
        NSLog(@"已连接");
        identifyingBand = YES;
        NSLog(@"call tryBond -----");
        [[TGBleManager sharedTGBleManager] tryBond];
    }
    else{
        NSLog(@"没有连接");
        if (sdkToken == NULL) {
            
            NSLog(@"It does not have any bands associated with this app.");
            if ([CommonValues iOSVersion] >= IOS8) {
                alertController = [UIAlertController alertControllerWithTitle:ERROR_TITLE message:NoAssociatedBand_MSG preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:okAction];
                [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
                
            }
            else{
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ERROR_TITLE message:NoAssociatedBand_MSG delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alert show];
            }
            
        }
        
        else{
            identifyingBand = YES;
            NSString *candId = [defaults objectForKey:@"DeviceID"];
            NSLog(@"stored candId = %@",candId);
            NSLog(@"call candidateConnect---");
            [[TGBleManager sharedTGBleManager]candidateConnect:candId];
            timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
            
            [SVProgressHUD showWithStatus:@" Connecting"];
            [self.view setUserInteractionEnabled:NO];
            
        }
        
        
    }
    
}

- (IBAction)releaseBondClick:(id)sender {
    NSLog(@"Connect to New Band----");
    isReleaseBondDone = NO;
    if ([CommonValues iOSVersion] >= IOS8) {
        alertController = [UIAlertController alertControllerWithTitle:nil message:CONNECT_NEW_BAND_CONFIRM_MSG preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
            [self releaseBondAlertCancel];
            
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self releaseBondAlertOK];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:CONNECT_NEW_BAND_CONFIRM_MSG delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        alert.tag = RELEASEBONDTAG;
        [alert show];
    }
}

- (void)reconnectAlertCancel{
    NSLog(@"RECONNECT cancel Button");
    reconnectingBand  = NO;
    [cannotConfirmUpdateAlert show];
}
- (void)reconnectAlertOK{
    NSLog(@"RECONNECT connect Button");
    reconnectingBand  = YES;
    [self connectFromStoredDeviceID];
    //                [[TGBleManager sharedTGBleManager] releaseBond];
    timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
}

- (void)checkBTStatusDone{
    NSLog(@"set bt done click ");
    if ([CommonValues getBluetoothOnFlag]) {
        //turn on the BT
        NSLog(@"already have  turn on the BT");
    }
    else{
        NSLog(@" BT  still not on state");
        [self showBTNotOnAlert];
    }
}

- (void)releaseBondAlertCancel{
    NSLog(@"RELEASEBOND cancel Button");
}
- (void)releaseBondAlertOK{
    NSLog(@"RELEASEBOND ok Button");
    [[TGBleManager sharedTGBleManager] releaseBond];
}

- (void)hasTokenTimeOutAlertCancel{
    if (reconnectingBand) {
        reconnectingBand = NO;
        [cannotConfirmUpdateAlert show];
    }
}
- (void)hasTokenTimeOutAlertRetry{
    //Retry
    NSLog(@"hasToken time out retry");
    if (reconnectingBand) {
        [self connectFromStoredDeviceID];
        timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
    }
    else{
        [self fwUpdateClick:nil];
    }
}

- (void)noTokenTimeOutAlertCancel{
    NSLog(@"time out  cancel...");
    
}
- (void)noTokenTimeOutAlertRetry{
    //Retry
    [self fwUpdateClick:nil];
}

- (void)selectDeviceAlertCancel{
    NSLog(@"selectDeviceAlertTag cancelButton");
    [ self stopScan];
}
- (void)selectDeviceAlertOK{
    NSLog(@"selectDeviceAlertTag OK Button");
    NSLog(@"call  candidateStopScan------");
    [[TGBleManager sharedTGBleManager] candidateStopScan];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [self.view setUserInteractionEnabled:NO];
    NSLog(@"need to save deviceName = %@",readyToSaveDeviceName);
    //if ([defaults stringForKey:@"DeviceName"] == nil) {
    [defaults setObject: readyToSaveDeviceName forKey:@"DeviceName"];
    [defaults setObject: readyToSaveDeviceID forKey:@"DeviceID"];
    [defaults synchronize];
    
    //candidate connect to the band;
    //                 [SVProgressHUD showWithStatus:@"Starting to Firmware Download..."];
    NSLog(@"call candidateConnect-----");
    [[TGBleManager sharedTGBleManager] candidateConnect:readyToSaveDeviceID];
    timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
    [SVProgressHUD showWithStatus:@" Connecting"];
    [self.view setUserInteractionEnabled:NO];
}
- (void)resetDeviceTempList{
    [devicesArray removeAllObjects];
    [mfgDataArray removeAllObjects];
    [rssiArray removeAllObjects];
    [candIDArray removeAllObjects];
    [devicesIDArray removeAllObjects];
    [listView dismiss];
}

- (void)bondAlertCancel{
    NSLog(@"bondAlert cancelButton");
    if (identifyingBand) {
        identifyingBand = NO;
        NSLog(@"identify Band cancel Button");
        [CommonValues setHasTaskFlag:NO];
        [SVProgressHUD dismiss];
        //                    [self.view setUserInteractionEnabled:YES];
        if ([TGBleManager sharedTGBleManager].status == TGBleConnectedToBond) {
            
        }
        else{
            NSLog(@"call tryDisconnect------");
            [[TGBleManager sharedTGBleManager] tryDisconnect];
            [SVProgressHUD showWithStatus:@"Disconnect"];
            identifyDisconnectFlag = YES;
        }
        
    }
    else{
        NSLog(@"call tryDisconnect------");
        [[TGBleManager sharedTGBleManager] tryDisconnect];
    }
    
}
- (void)bondAlertOK{
    NSLog(@"bondAlert OK Button");
    
    if (identifyingBand) {
        identifyingBand = NO;
        
        NSLog(@"identify Band OK Button");
        [SVProgressHUD dismiss];
        [self.view setUserInteractionEnabled:YES];
        
        if ([TGBleManager sharedTGBleManager].status == TGBleConnectedToBond) {
            
        }
        else{
            NSLog(@"call tryDisconnect------");
            [[TGBleManager sharedTGBleManager] tryDisconnect];
            [SVProgressHUD showWithStatus:@"Disconnect"];
            [self.view setUserInteractionEnabled:NO];
            identifyDisconnectFlag = YES;
        }
    }
    else{
        NSLog(@"call takeBond------");
        [[TGBleManager sharedTGBleManager] takeBond];
        [SVProgressHUD showWithStatus:@" Bonding"];
    }
    
}

- (void)fwDownAlertCancel{
    //不刷fw
    [updateBtn setImage:[UIImage imageNamed:@"update_1"] forState:UIControlStateNormal];
}
- (void)fwDownAlertOK{
    //    [[TGBleManager sharedTGBleManager]tryFwDown];
    //要刷fw
    lostBeforeMainOperation = YES;
    [CommonValues setHasTaskFlag:YES];
    
    [updateBtn setImage:[UIImage imageNamed:@"update_2"] forState:UIControlStateNormal];
    
    [self.view setUserInteractionEnabled:NO];
    if ([TGBleManager sharedTGBleManager].status == TGBleConnectedToBond) {
        //已有连接,直接做主要操作
        NSLog(@"已有连接,直接做主要操作");
        
        NSThread *thread = [[NSThread alloc]initWithTarget:self selector:@selector(fwdownRun) object:nil];
        [thread start];
        
    }
    else{
        NSLog(@"没有连接");
        
        NSData *token = [[TGBleManager sharedTGBleManager]bondToken];
        if (token == NULL) {
            //
            NSLog(@"unbonded");
            [self selectDevice];
            
        }
        else{
            NSLog(@"bonded");
            //         [SVProgressHUD showWithStatus:@"Starting to Firmware Download..."];
            [self connectFromStoredDeviceID];
            
        }
        
        timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
    }
}

- (void)noTokenBondFailedCancel{
    [SVProgressHUD dismiss];
    NSLog(@"noToken  bondFailedTag cancelButton");
    [self.view setUserInteractionEnabled:YES];
    
}
- (void)noTokenBondFailedRetry{
    [SVProgressHUD dismiss];
    NSLog(@"noToken bondFailedTag Retry");
    //connect previous band
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"DeviceName"];
    NSString *candId = [defaults objectForKey:@"DeviceID"];
    NSLog(@"stored candId = %@",candId);
    NSLog(@"call candidateConnect---");
    [[TGBleManager sharedTGBleManager]candidateConnect:candId];
    timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
    
    [SVProgressHUD showWithStatus:@" Connecting"];
    [self.view setUserInteractionEnabled:NO];
}
- (void)noTokenBondFailedTryAnother{
    [SVProgressHUD dismiss];
    NSLog(@"noToken bondFailedTag Try another band");
    [self fwUpdateClick:nil];
}

- (void)hasTokenBondFailedRetry{
    NSLog(@"hasToken BondFailedTag Retry");
    //connect previous band
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"DeviceName"];
    NSString *candId = [defaults objectForKey:@"DeviceID"];
    NSLog(@"stored candId = %@",candId);
    NSLog(@"call candidateConnect---");
    [[TGBleManager sharedTGBleManager]candidateConnect:candId];
    timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
    
    [CommonValues delay2ShowSVProgressHUD:@" Connecting"];
    [self.view setUserInteractionEnabled:NO];
    
}
- (void)hasTokenBondFailedCancel{
    NSLog(@"hasToken BondFailedTag cancel");
    [self.view setUserInteractionEnabled:YES];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"buttonIndex:%ld", (long)buttonIndex);
    
    switch (alertView.tag) {
            
        case RECONNECTTAG:
        {
            if (buttonIndex == 0) {
                [self reconnectAlertCancel];
            }
            else if (buttonIndex == 1) {
                [self reconnectAlertOK];
            }
        }
            
            break;
            
            
        case checkBTStatusTag:
            [self checkBTStatusDone];
            break;
        case RELEASEBONDTAG:
        {
            if (buttonIndex == 0) {
                [self releaseBondAlertCancel];
            }
            else if (buttonIndex == 1) {
                [self releaseBondAlertOK];
            }
            
            
        }
            break;
            
            
        case   hasTokenTimeOutAlertTag:
            if (buttonIndex == 0) {
                [self hasTokenTimeOutAlertRetry];
            }
            
            else {
                [self hasTokenTimeOutAlertCancel];
            }
            break;
            
            
        case   noTokenTimeOutAlertTag:
            
            if (buttonIndex == 0) {
                [self noTokenTimeOutAlertCancel];
            }
            
            else if (buttonIndex == 1)
            {
                [self noTokenTimeOutAlertRetry];
            }
            
            break;
            
            
        case selectDeviceAlertTag:{
            
            if (buttonIndex == 0) {
                [self selectDeviceAlertCancel];
                
            }
            
            else if(buttonIndex == 1){
                [self selectDeviceAlertOK];
            }
            [self resetDeviceTempList];
            
            
        }
            
            break;
        case bondAlertTag:{
            
            if (buttonIndex == 0) {
                [self bondAlertCancel];
                
            }
            
            else if(buttonIndex == 1){
                [self bondAlertOK];
            }
            
            
        }
            break;
            
        case fwDownAlertTag:
        {
            if (buttonIndex == 0) {
                [self fwDownAlertCancel];
            }
            else if(buttonIndex == 1){
                [self fwDownAlertOK];
            }
            
        }
            break;
            
            
        case noTokenBondFailedTag:
        {
            if (buttonIndex == 0) {
                [self noTokenBondFailedCancel];
            }
            
            else if(buttonIndex == 1){
                
                [self noTokenBondFailedRetry];
            }
            else if(buttonIndex == 2){
                [self noTokenBondFailedTryAnother];
            }
            
        }
            break;
            
        case hasTokenBondFailedTag:
        {
            [SVProgressHUD dismiss];
            if (buttonIndex == 1) {
                [self hasTokenBondFailedRetry];
            }
            else{
                [self hasTokenBondFailedCancel];
            }
        }
            
            
            break;
        default:
            break;
    }
    
}

- (void) bleDidConnect {
    NSLog(@"bleDidConnect----");
    if ([timeOutTimer isValid]) {
        [timeOutTimer invalidate];
        NSLog(@"timeOutTimer invalidate----");
        
    }
    lostBeforeMainOperation = NO;
    
    if (identifyingBand) {
        NSLog(@"call tryBond -----");
        [[TGBleManager sharedTGBleManager] tryBond];
    }
    
    else
    {
        dispatch_async( dispatch_get_main_queue(), ^ {
            [SVProgressHUD showWithStatus:@" Bonding"];
        });
        [self bondClick:nil];
    }
    
}

-(void) bleDidDisconnect{
    NSLog(@"bleDidDisconnect----");
    firstEnterCandiDel = NO;
    [CommonValues setAboutUpdateProgress:0.0];
    [updateBtn setImage:[UIImage imageNamed:@"update_1"] forState:UIControlStateNormal];
    
    dispatch_async( dispatch_get_main_queue(), ^ {
        [SVProgressHUD dismiss];
        [self.view setUserInteractionEnabled:YES];
    });
    
}


- (void)potentialBond:(NSString*) code sn: (NSString*) sn devName:(NSString*) devName
{
    
    
    NSString* displayMessage = [NSString stringWithFormat:
                                @"%@%@?",
                                DigitCodeConfirmMsg,code];
    
    NSString* identifyMessage = [NSString stringWithFormat:
                                 @"%@%@.",Associated_MSG,code];
    dispatch_async( dispatch_get_main_queue(), ^ {
        [SVProgressHUD dismiss];
        
    });
    
    if (identifyingBand) {
        
        dispatch_async( dispatch_get_main_queue(), ^ {
            
            if ([CommonValues iOSVersion] >= IOS8) {
                alertController = [UIAlertController alertControllerWithTitle:@"" message:identifyMessage preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self bondAlertOK];
                }];
                [alertController addAction:okAction];
                [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
            }
            else
            {
                UIAlertView *bondDialog = [[UIAlertView alloc]initWithTitle:@"" message:identifyMessage delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                bondDialog.tag = bondAlertTag;
                [bondDialog show];
            }
            
        });
    }
    
    else
    {
        dispatch_async( dispatch_get_main_queue(), ^ {
            if ([CommonValues iOSVersion] >= IOS8) {
                alertController = [UIAlertController alertControllerWithTitle:@"" message:displayMessage preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                    [self bondAlertCancel];
                    
                }];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self bondAlertOK];
                }];
                [alertController addAction:cancelAction];
                [alertController addAction:okAction];
                [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
            }
            else{
                UIAlertView *bondDialog = [[UIAlertView alloc]initWithTitle:@"" message:displayMessage delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
                bondDialog.tag = bondAlertTag;
                [bondDialog show];
            }
            
        });
    }
    
}
- (void) bleDidBond:(TGbondResult)result {
    NSString * message;
    NSString *title;
    
    if (result == TGbondResultTokenAccepted) {
        message = @"Bond Succesful";
        if ( reconnectingBand  ) {
            reconnectingBand = NO;
            NSLog(@"success to connect after update FW ");
            title = @"Finished!";
            message = @"Firmware Download completed.";
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:@" OK ", nil];
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                if ([CommonValues iOSVersion] >= IOS8) {
                    alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                    [alertController addAction:okAction];
                    [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
                    
                }
                else{
                    [alert show];
                }
                //update UI FW version,HW version,battery
                [self updateUI];
                [self.view setUserInteractionEnabled:YES];
            });
        }
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD showWithStatus:@" Downloading Firmware "];
            });
            //start to erase;
            NSThread *thread = [[NSThread alloc]initWithTarget:self selector:@selector(fwdownRun) object:nil];
            [thread start];
            
        }
        
        
    }
    else if (result == TGbondResultTokenReleased) {
        title = TokenReleasedTitle;
        message = TokenReleasedMsg;
    }
    else if (result == TGbondResultErrorBondedNoMatch) {
        title = CommunicationErrorTitle;
        message = BondedNoMatchMsg;
    }
    else if (result == TGbondResultErrorBadTokenFormat) {
        title = BondRejectedTitle;
        message = BadTokenFormatMsg;
    }
    else if (result == TGbondResultErrorTimeOut) {
        title = TimeOutTitle;
        message = TimeOutMsg;
    }
    
    else if (result == TGbondResultErrorNoConnection) {
        title = NoConnectionTitle;
        message = NoConnectionMsg;
    }
    
    else if (result == TGbondResultErrorReadTimeOut) {
        title = ReadTimeOutTitle;
        message = ReadTimeOutMsg;
    }
    
    
    else if (result == TGbondResultErrorReadBackTimeOut) {
        title = ReadBackTimeOutTitle;
        message = ReadBackTimeOutMsg;
    }
    
    else if (result == TGbondResultErrorWriteFail) {
        title = WriteFailTitle;
        message = WriteFailMsg;
    }
    
    
    else if (result == TGbondResultErrorTargetIsAlreadyBonded) {
        title = BondRejectedTitle;
        message = TargetIsAlreadyBondedMsg;
    }
    
    
    else if (result == TGbondAppErrorNoPotentialBondDelegate) {
        title  = BondAppErrorTitle;
        message = NoPotentialBondDelegateMsg;
    }
    
    
    else if (result == TGbondResultErrorTargetHasWrongSN) {
        title  = CommunicationErrorTitle;
        message = TargetHasWrongSNMSg;
    }
    else if (result == TGbondResultErrorPairingRejected) {
        title  = PairingErrorTitle;
        message = PairingRejectedMsg;
    }
    
    
    else if (result == TGbondResultErrorSecurityMismatch) {
        title  = PairingErrorTitle;
        message = SecurityMismatchMsg;
    }
    
    
    else {
        title  = UnkonwnBondErrorTitle;
        message = [NSString stringWithFormat:@"Bonding Error, code: %d", (int) result];
    }    NSLog(@"BONDIING RESULT Code  %ld",(unsigned long)result);
    NSLog(@"BONDIING RESULT msg  %@",message);
    
    if (result == TGbondResultTokenAccepted){
        
        
    }
    else   if (result == TGbondResultTokenReleased) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:TokenReleasedTitle message:TokenReleasedMsg delegate:nil cancelButtonTitle:nil otherButtonTitles:@" OK ", nil];
        isReleaseBondDone = YES;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view setUserInteractionEnabled:YES];
            [SVProgressHUD dismiss];
            if ([CommonValues iOSVersion] >= IOS8) {
                alertController = [UIAlertController alertControllerWithTitle:TokenReleasedTitle message:TokenReleasedMsg preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@" OK " style:UIAlertActionStyleCancel handler:nil];
                [alertController addAction:okAction];
                [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
            }
            else{
                [alert show];
            }
            [self updateUI];});
        
    }
    
    else{
        
        lostBeforeMainOperation = NO;
        
        NSLog(@"call tryDisconnect----");
        [[TGBleManager sharedTGBleManager]tryDisconnect];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        NSData *token = [[TGBleManager sharedTGBleManager] bondToken];
        if (token == NULL) {
            if ([CommonValues iOSVersion] >= IOS8) {
                alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:CANCEL style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                    [self noTokenBondFailedCancel];
                    
                }];
                UIAlertAction *retryAction = [UIAlertAction actionWithTitle:RETRY style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self noTokenBondFailedRetry];
                }];
                UIAlertAction *tryAnotherAction = [UIAlertAction actionWithTitle:TRYANOTHERBAND style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self noTokenBondFailedTryAnother];
                }];
                [alertController addAction:cancelAction];
                [alertController addAction:retryAction];
                [alertController addAction:tryAnotherAction];
                [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
            }
            else{
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:CANCEL otherButtonTitles:RETRY,TRYANOTHERBAND, nil];
                alert.tag = noTokenBondFailedTag;
                dispatch_async( dispatch_get_main_queue(), ^ {
                    [alert show];
                });
            }
        }
        
        else{
            if ([CommonValues iOSVersion] >= IOS8) {
                alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:CANCEL style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                    [self hasTokenBondFailedCancel];
                    
                }];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:RETRY style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self hasTokenBondFailedRetry];
                }];
                [alertController addAction:cancelAction];
                [alertController addAction:okAction];
                [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
            }
            else{
                UIAlertView *alert_hasToken = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:CANCEL otherButtonTitles:RETRY, nil];
                
                alert_hasToken.tag = hasTokenBondFailedTag;
                dispatch_async( dispatch_get_main_queue(), ^ {
                    [alert_hasToken show];
                });
                
            }
        }
        
    }
    
    
    
}
-(void) batteryLevel:(int)level {
    NSLog(@">>>>>>>-----New Battery Level: percent complete: %d", level);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:level  forKey:LATEST_BATTERY];
    [defaults synchronize];
}

- (void) exceptionMessage:(TGBleExceptionEvent)eventType {
    NSString * message;
    
    if (eventType == TGBleHistoryCorruptErased) {
        message = @"Flash Memory Corrupted, Will Be erased, suspect battery has been exhausted";
    }
    else if (eventType == TGBleConfigurationModeCanNotBeChanged) {
        message = @"Exception Message - Ble Connection Mode CAN NOT be changed";
    }
    else if (eventType == TGBleUserAgeHasBecomeNegative_BirthDateIsNOTcorrect) {
        message = @"User Age has become negative, correct the birth date";
    }
    else if (eventType == TGBleUserAgeIsTooLarge_BirthDateIsNOTcorrect) {
        message = @"User Age is TOO OLD, check the birth date";
    }
    else if (eventType == TGBleUserBirthDateRejected_AgeOutOfRange) {
        message = @"User Age is out of RANGE, corret the birth date";
    }
    else if (eventType == TGBleFailedOtherOperationInProgress) {
        message = @"Exception Message - Another Operation is Already in Progress";
    }
    else if (eventType == TGBleFailedSecurityNotInplace) {
        message = @"Exception Message - Security is NOT In Place";
    }
    else if (eventType == TGBleTestingEvent) {
        message = @"Exception Message - This is only a Test";
    }
    else if (eventType == TGBleReInitializedNeedAlarmAndGoalCheck) {
        message = @"Exception Message - Band lost power or was MFG reset, check your alarm and goal settings";
    }
    else if (eventType == TGBleStepGoalRejected_OutOfRange) {
        message = @"Exception Message - Step Goal Settings rejected -- OUT OF RANGE";
    }
    else if (eventType == TGBleCurrentCountRequestTimedOut) {
        message = @"Exception Message - Can Not get Current Count values\nBand not responding";
    }
    else if (eventType == TGBleConnectFailedSuspectKeyMismatch) {
        message = @"Exception Message - Band appears to be paired, possible encryption key mismatch, hold side button for 10 seconds to reboot";
    }
    else if (eventType == TGBleNonRepeatingAlarmMayBeStaleCheckAlarmConfiguration) {
        message = @"Exception Message - A Non-repeating Alarm may be stale, check your alarm settings";
    }
    else {
        message = [NSString stringWithFormat:@"Exception Detected, code: %lu", (unsigned long)eventType];
    }
    NSLog(@"exception message = %@",message);
}


- (IBAction)bondClick:(id)sender {
    
    NSLog(@"Bond Button Clicked");
    isReleaseBondDone = NO;
    if ([TGBleManager sharedTGBleManager].status == TGBleConnected ||
        [TGBleManager sharedTGBleManager].status == TGBleConnectedToBond) {
        NSLog(@"SDK say it is Connected");
        NSData *sdkToken =  [[TGBleManager sharedTGBleManager] bondToken];
        
        if (sdkToken == NULL) {
            NSLog(@"call tryBond-----");
            [[TGBleManager sharedTGBleManager] tryBond];
            
        }
        
        else{
            
            NSString *sn =  [[TGBleManager sharedTGBleManager] hwSerialNumber];
            NSLog(@"sdkToken = %@, sn = %@",sdkToken,sn);
            
            NSLog(@"call adoptBond: serialNumber ");
            [[TGBleManager sharedTGBleManager] adoptBond:sdkToken serialNumber:sn];
            
        }
        lostBeforeMainOperation = NO;
    }
    
    else{
        
        NSLog(@"SDK say it is NOT Connected");
    }
    
    
}

- (void)fwdownRun{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [SVProgressHUD showWithStatus:@" Downloading Firmware "];
    });
    NSLog(@"call tryFwDown----");
    [[TGBleManager sharedTGBleManager]tryFwDown];
}

- (void)selectDevice{
    NSLog(@"selectDevice---");
    if (![CommonValues getBluetoothOnFlag]) {
        NSLog(@"BT OFF, can't go on selectDevice");
        [self showBTNotOnAlert];
        return;
    }
    
    
    
    [SVProgressHUD showWithStatus:@" Search bands "];
    [self.view setUserInteractionEnabled:NO];
    
    NSLog(@" call  candidateScan:nameList");
    [[TGBleManager sharedTGBleManager] candidateScan:[CommonValues bandDeviceArray]];
    
    
    NSLog(@"Menu item  devicesArray count = %ld",(unsigned long)devicesArray.count);
    
    
    
    
}

- (void)stopScan{
    NSLog(@"touch listview other place to stopScan ");
    NSLog(@"call candidateStopScan----");
    // reset button state and flags.
    [updateBtn setImage:[UIImage imageNamed:@"update_1"] forState:UIControlStateNormal];
    [self.view setUserInteractionEnabled:YES];
    
    [[TGBleManager sharedTGBleManager]candidateStopScan];
    [devicesArray removeAllObjects];
    [mfgDataArray removeAllObjects];
    [rssiArray removeAllObjects];
    [candIDArray removeAllObjects];
    [devicesIDArray removeAllObjects];
    // cancel delayed time out alert.
    if ([timeOutTimer isValid]) {
        [timeOutTimer invalidate];
        NSLog(@"timeOutTimer invalidate----");
        
    }
    firstEnterCandiDel = NO;
    
    
    
}

#pragma marks
#pragma tableview datasource methods
#pragma mark -
- (NSInteger)popoverListView:(ZSYPopoverListView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return devicesArray.count;
}

- (UITableViewCell *)popoverListView:(ZSYPopoverListView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString * savedDeviceName = [defaults stringForKey:@"DeviceName"];
    NSLog(@"NSUserDefaults deviceName = %@",savedDeviceName);
    
    static NSString *identifier = @"identifier";
    UITableViewCell *cell = [tableView dequeueReusablePopoverCellWithIdentifier:identifier];
    if (nil == cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] ;
    }
    
    
    NSString *tmpStr = [NSString stringWithFormat:@"%@:%@",[ mfgDataArray objectAtIndex:indexPath.row],[ devicesArray objectAtIndex:indexPath.row]];
    //cell.textLabel.text =[ devicesArray objectAtIndex:indexPath.row];
    cell.textLabel.text = tmpStr;
    cell.textLabel.font = [UIFont fontWithName: DEFAULT_FONT_NAME size:25.0];
    
    if ([[ devicesArray objectAtIndex:indexPath.row] isEqualToString:savedDeviceName]) {
        // many devices have the same name
        //
        //cell.imageView.image = [UIImage imageNamed:@"fs_main_login_selected.png"];
    }
    
    return cell;
}

- (void)popoverListView:(ZSYPopoverListView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*UITableViewCell *cell =*/ [tableView popoverCellForRowAtIndexPath:indexPath];
    //cell.imageView.image = [UIImage imageNamed:@"fs_main_login_normal.png"];
    NSLog(@"deselect:%ld", (long)indexPath.row);
    
    
    
}

- (void)popoverListView:(ZSYPopoverListView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedIndexPath = indexPath;
    NSLog(@"select:%ld", (long)indexPath.row);
    //  NSString *deviceName = [NSString stringWithFormat:@"Wat%d", indexPath.row];
    
    NSString *deviceName = [devicesArray objectAtIndex:indexPath.row];
    NSString *mfgData = [mfgDataArray objectAtIndex:indexPath.row];
    readyToSaveDeviceName = [devicesArray objectAtIndex:indexPath.row];
    readyToSaveDeviceID = [devicesIDArray objectAtIndex:indexPath.row];
    // [self.tagert performSelector:stopEBLScan withObject:nil];
    
    //     [devicesArray removeAllObjects];
    //     [listView dismiss];
    NSString *msg = [NSString stringWithFormat:@"Are you sure you would like to connect to %@ %@  ?",deviceName, mfgData];
    [listView dismiss];
    if ([CommonValues iOSVersion] >= IOS8) {
        alertController = [UIAlertController alertControllerWithTitle:msg message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
            [self selectDeviceAlertCancel];
            
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self selectDeviceAlertOK];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
    }
    else{
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:msg message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        alert.tag   = selectDeviceAlertTag;
        [alert show];
        
    }
    
    
    
    
}


- (void)showTimeOutAlert{
    [SVProgressHUD dismiss];
    
    [self stopScan];
    // delay to show alert ,prevent the confligs from SVProgressHUD dismiss
    [self performSelector:@selector(delay2ShowTimeOutAlert) withObject:nil afterDelay:0.5];
    
}
- (void)delay2ShowTimeOutAlert{
    if ([CommonValues iOSVersion] >= IOS8) {
        UIAlertAction *cancelAction;
        UIAlertAction *okAction;
        if ([[TGBleManager sharedTGBleManager]bondToken] != NULL ) {
            alertController = [UIAlertController alertControllerWithTitle:ConnectTimeOutTitle message:ConnectTimeOutMsg preferredStyle:UIAlertControllerStyleAlert];
            cancelAction = [UIAlertAction actionWithTitle:CANCEL style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                [self hasTokenTimeOutAlertCancel];
                
            }];
            okAction = [UIAlertAction actionWithTitle:RETRY style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self hasTokenTimeOutAlertRetry];
            }];
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
        }
        else{
            alertController = [UIAlertController alertControllerWithTitle:ScanTimeOutTitle message:ScanTimeOutMsg preferredStyle:UIAlertControllerStyleAlert];
            cancelAction = [UIAlertAction actionWithTitle:CANCEL style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                [self noTokenTimeOutAlertCancel];
                
            }];
            okAction = [UIAlertAction actionWithTitle:RETRY style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self noTokenTimeOutAlertCancel];
            }];
            
        }
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
        
        
    }
    else{
        UIAlertView *timeOutAlert;
        if ([[TGBleManager sharedTGBleManager]bondToken] != NULL ) {
            timeOutAlert =  [[UIAlertView alloc]initWithTitle:ConnectTimeOutTitle message:ConnectTimeOutMsg delegate:self cancelButtonTitle:nil otherButtonTitles:RETRY, CANCEL, nil];
            timeOutAlert.tag = hasTokenTimeOutAlertTag;
            
            
        }
        else{
            timeOutAlert = [[UIAlertView alloc]initWithTitle:ScanTimeOutTitle message:ScanTimeOutMsg delegate:self cancelButtonTitle:CANCEL otherButtonTitles:RETRY, nil];
            timeOutAlert.tag = noTokenTimeOutAlertTag;
        }
        
        [timeOutAlert show];
    }
    NSLog(@"call tryDisconnect----");
    [[TGBleManager sharedTGBleManager] tryDisconnect];
}


- (void)delay2ShowList{
    NSLog(@"showTimeOutAlerttest---");
    [listView show];
}

- (void)showBTNotOnAlert {
    if ([CommonValues iOSVersion] >= IOS8) {
        alertController = [UIAlertController alertControllerWithTitle:BluetoothIsOffTitle message:BluetoothIsOffMsg preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self checkBTStatusDone];
        }];
        [alertController addAction:okAction];
        [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
    }
    else{
        UIAlertView *alert =[ [UIAlertView alloc]initWithTitle:BluetoothIsOffTitle message:BluetoothIsOffMsg delegate:self cancelButtonTitle:nil otherButtonTitles:@"Done", nil];
        alert.tag = checkBTStatusTag;
        [alert show];
    }
    
}


/**
 update labels of FW version ;_HW version ;battery level
 */
- (void)updateUI{
    //    NSString *fwStr =  [[TGBleManager sharedTGBleManager] fwVersion];
    //    NSString *hwStr =  [[TGBleManager sharedTGBleManager] hwVersion];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSInteger storedBatteryLevel = [userDefault integerForKey:LATEST_BATTERY];
    NSString  *FW_Version = [userDefault stringForKey:@"FW_Version"];
    NSString  *HW_Version = [userDefault stringForKey:@"HW_Version"];
    NSString *batteryLevelStr = [NSString stringWithFormat:@"%ld%%",(long)storedBatteryLevel];
    if ([[TGBleManager sharedTGBleManager] bondToken] == NULL) {
        FW_Version = @"";
        HW_Version = @"";
        batteryLevelStr = @"";
    }
    hrVersionLabel.text = HW_Version;
    fwVersionLabel.text = FW_Version;
    batteryLabel.text = batteryLevelStr;
    
}

- (void)showAlertController{
    [self presentViewController:alertController animated:YES completion:nil];
    
}
@end

//
//  HelpViewController.m
//  WAT
//
//  Created by neurosky on 5/28/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import "HelpViewController.h"
#import "CommonValues.h"
#import "Constant.h"

@interface HelpViewController ()

@end

@implementation HelpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    myWholeView.frame = CGRectMake(0, [CommonValues offsetHeight4DiffiOS], 320, 568-20);
    CGRect rect_screen = [[UIScreen mainScreen]bounds];
    CGSize size_screen = rect_screen.size;
    screenHeight = size_screen.height;
    NSLog(@"screenHeight = %f",screenHeight);
    
//   UIFont *titleFont =  [UIFont fontWithName:DEFAULT_FONT_NAME size:20.0];
//   UIColor *textColor = [UIColor colorWithRed:96/255.0 green:100/255.0 blue:103/255.0 alpha:1];
//    UIFont *detailFont =  [UIFont fontWithName:DEFAULT_FONT_NAME size:20.0];

    
    if (screenHeight == 568) {
        help_title_label.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
       
       
        
        /*
         UILabel *hrLabel =    [CommonValues labelWithFrame:CGRectMake(5, 5, 100, 25) WithBoldFont:titleFont strokeWidth:0 withAttributeText:@"HR-BPM:"];
         [hrLabel setTextColor:textColor];
         [helpScrollView addSubview:hrLabel];
        
        
        UITextView *hrDetailLabel =     [[UITextView alloc]initWithFrame:CGRectMake(0, 5+25, 320, 35)];
        [hrDetailLabel setFont:titleFont];
        [hrDetailLabel setText:HR_BPM];
        hrDetailLabel.textAlignment = NSTextAlignmentLeft;
        [hrDetailLabel setTextColor:textColor];
        [helpScrollView addSubview:hrDetailLabel];
        
        UILabel *hrvLabel =    [CommonValues labelWithFrame:CGRectMake(5, 30+40+5, 100, 25) WithBoldFont:titleFont strokeWidth:0 withAttributeText:@"HRV:"];
        [hrvLabel setTextColor:textColor];
        [helpScrollView addSubview:hrvLabel];
        
        
        UITextView *hrvDetailLabel =     [[UITextView alloc]initWithFrame:CGRectMake(0, 30+40+5+25, 320, 90)];
        [hrvDetailLabel setFont:titleFont];
        [hrvDetailLabel setText:HRV];
        hrvDetailLabel.textAlignment = NSTextAlignmentLeft;
        [hrvDetailLabel setTextColor:textColor];
        [helpScrollView addSubview:hrvDetailLabel];

//        
        UILabel *stressLabel =    [CommonValues labelWithFrame:CGRectMake(5, 30+40+5+25+25, 100, 25) WithBoldFont:titleFont strokeWidth:0 withAttributeText:@"STRESS:"];
        [stressLabel setTextColor:textColor];
        [helpScrollView addSubview:stressLabel];
        
 
        */
        
        
        
        
        
        
        
    }
    
    else{
        help_title_label.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:25.0];
    }
    
    helpScrollView.contentSize = CGSizeMake(320,1044);
    helpScrollView.backgroundColor = [UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1];
    UIImageView *imageContent = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"help_content"]];
    [helpScrollView addSubview:imageContent];

}

- (NSUInteger)supportedInterfaceOrientations

{
    return UIInterfaceOrientationMaskPortrait;
    
}


-(BOOL)shouldAutorotate{
    
    return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backClick:(id)sender {
    NSLog(@"help backClick ---");
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end

//
//  ActivityCalorieViewController.h
//  WAT
//
//  Created by neurosky on 9/17/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"
#import "circleProgressSmall.h"
#import <sqlite3.h>
#import "TGBleManagerDelegate.h"

@interface ActivityCalorieViewController : UIViewController < CPTPlotDataSource, TGBleManagerDelegate > {
    NSArray *StepArray;
    sqlite3 *db;
    NSMutableArray *mulStepArray;
    NSMutableArray *tempMulStepArray;
    NSArray *CaloriesArray;
    NSArray *DistanceArray;
    NSInteger Step_count;
    NSInteger Calories_count;
    NSInteger Distance_count;
    CPTXYGraph *barChart;
    CPTXYGraph *barChart1;
    CPTGraphHostingView *hv;
    circleProgressSmall *circle;
    UILabel *label;
    NSString *timeString;
    CGFloat beginy;
    UILabel *noticeLabel;
    UIView *uiView;
    NSMutableArray *step4GraphArray;
    NSMutableArray  *dbTimeArray;
    NSMutableArray *tempCaloriesArrList;
    NSMutableArray *oneDayCalorieArr; //6 am ~ 10 pm
    NSInteger subStepInOneHour;
    NSInteger stepIndex;
    NSInteger caloriesIndex;
    NSInteger distanceIndex;
    NSInteger timeDateIndex;
    NSInteger  tempStepIndex;
    NSMutableArray *sss;
    NSInteger recordCount;
    NSInteger recordIndex;
    NSInteger goalCalorie;
    NSMutableArray * sleepPhaseArray;
    NSMutableArray *sleepTimeArray;
    NSInteger  sleepIndex;
    NSInteger dayTotalSleepTime;
    NSString *time1;
    NSString *time2;
    NSMutableArray *sleepIntervalTimeArr;
    NSInteger totalEndTime;
    NSInteger oneDayTotalStep;
    NSInteger oneDayTotalCalories;
    NSInteger oneDayTotalDistance;
    NSString *demoModeStr;
    NSString *database_path;
    NSDateFormatter *df;
    UIColor *calorieColor;
    __weak IBOutlet UIView *myWholeView;
    __weak IBOutlet UIImageView *stepImg;
    __weak IBOutlet UIImageView *sleepImg;
    __weak IBOutlet UIImageView *distanceImg;
    __weak IBOutlet UIImageView *activeImg;
    __weak IBOutlet UILabel *leftMidLabel;
    __weak IBOutlet UILabel *rightMidLabel;
}

@property (strong, nonatomic) IBOutlet UILabel *title_calorie;
@property (strong, nonatomic) IBOutlet UILabel *title_date;
@property (strong, nonatomic) IBOutlet UILabel *title_end_time;
@property (strong, nonatomic) IBOutlet UILabel *title_start_time;
@property (strong, nonatomic) IBOutlet UILabel *title_mid_time;
@property (strong, nonatomic) IBOutlet CPTGraphHostingView *hv;
@property (strong, nonatomic) IBOutlet UILabel *step_value;
@property (strong, nonatomic) IBOutlet UILabel *distance_value;
@property (strong, nonatomic) IBOutlet UILabel *activity_time_value;
@property (strong, nonatomic) IBOutlet UILabel *sleep_value;

@property (strong, nonatomic) IBOutlet UIImageView *img;
- (IBAction)leftBt:(id)sender;
- (IBAction)rightBt:(id)sender;
- (IBAction)backBtnClick:(id)sender;

@end

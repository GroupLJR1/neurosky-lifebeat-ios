//
//  HeartViewController.m
//  WAT
//
//  Created by NeuroSky on 11/11/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import "HeartViewController.h"
#import "CommonValues.h"
#import "HRDetailTrendLineView.h"
#import "HRDetailScatterView.h"
#import "Constant.h"
#import "HeartData.h"

@interface HeartViewController ()


@end

//此文件和HRVTrendDetailViewController.m 几乎一模一样
@implementation HeartViewController
{
  
    float y1;
    float y2;
    
    NSInteger periodCount;
    NSInteger chartLabel;
    NSInteger chartXaxisLength;
    NSInteger chartLabelDisplayCount;
    NSInteger medianInterval;
    NSInteger MovingInterval;
    
    BOOL HrvYes;
    
    NSString                            *currentDateStr;
    NSMutableArray                  *GrahpyArray;
    NSDateFormatter                *df;
    NSString                            *minDateStr;
    HRDetailScatterView           *hrDetailScatter;
    HRDetailTrendLineView       *hrDetailTrend ;
    
    UILabel *todayLabel;

}

@synthesize title_value1;

-(id) initwithHrLabel:(BOOL) HrEnable
{
    
    if ([super init])
    {
        HrvYes=HrEnable;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (HrvYes)
    {
        title_value1.text=@"HR-BPM";
        
    }
    else
    {
        title_value1.text=@"HRV";

    }
    //fit for ios 6
    myWholeView.frame = CGRectMake(0, [CommonValues offsetHeight4DiffiOS], 320, 568-20);
    
    [monthBtn setImage:[UIImage imageNamed:@"month_1"] forState:UIControlStateNormal];
    [sixMonthBtn setImage:[UIImage imageNamed:@"six_month_1"] forState:UIControlStateNormal];
    [yearBtn setImage:[UIImage imageNamed:@"year_1"] forState:UIControlStateNormal];
    
    [monthBtn setImage:[UIImage imageNamed:@"month_2"] forState:UIControlStateSelected];
    [sixMonthBtn setImage:[UIImage imageNamed:@"six_month_2"] forState:UIControlStateSelected];
    [yearBtn setImage:[UIImage imageNamed:@"year_2"] forState:UIControlStateSelected];
    
    // Do any additional setup after loading the view from its nib.
    db = [CommonValues getMyDB];
    NSLog(@"HR detail viewDidLoad----");
    
    title_value1.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:24.0];
    title_value1.backgroundColor = [UIColor clearColor];
    title_value1.textColor = [UIColor grayColor];

    HRArray = [[NSMutableArray alloc]init];
    DateArray = [[NSMutableArray alloc]init];
   
    NSDate *currentDate = [NSDate date ];
    df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"yyyy-MM-dd"];
    currentDateStr = [df stringFromDate:currentDate];
    NSLog(@"currentDateStr = %@",currentDateStr);
    
    
    minDateStr = [CommonValues getHeartMinDateStr:db];
    NSLog(@"min date string %@",minDateStr);
    oneWeekDateArr = [[NSMutableArray alloc]init];
    
    
    chartLabel=4;
    [self monthBtnClick:nil];
    
//  [self insertHeatRecordToDB];
    
}

-(void)insertHeatRecordToDB
{
    NSDate *insertDate=[NSDate date];
    NSUInteger insertCount=365;
    NSTimeInterval secondPerDay=24*60*60;
    int random=0;
    while (insertCount)
    {
        random=rand()%100;
        [CommonValues insertHeartData2DB : insertDate
                                 finalHR : random
                                   avgHR : 1
                                  avgHRV : random
                                finalHRV : 1
                                  stress : 1
                                database : db
        ];
        insertDate=[insertDate dateByAddingTimeInterval:-secondPerDay];
        insertCount--;
    }
    
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"HR detail viewWillAppear----");
    [ super viewWillAppear: animated ];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)leftBtnClick:(id)sender
{
    NSLog(@"leftBtnClick---");
    if (minDateStr == nil ) {
         return;
     }
    
    NSDate *currentDate =    [df dateFromString:currentDateStr];
    NSDate *lastDateB =  [currentDate dateByAddingTimeInterval:-periodCount*(24*60*60)];
    NSDate *lastDateA =  [currentDate dateByAddingTimeInterval:-periodCount*2*(24*60*60)];
    NSString *tempLastDateBStr = [df stringFromDate:lastDateB];
    NSLog(@"tempLastDateBStr = %@",tempLastDateBStr);
    NSString *tempLastDateAStr = [df stringFromDate:lastDateA];
    NSLog(@"tempLastDateAStr = %@",tempLastDateAStr);
    NSDate *minDate = [df dateFromString:minDateStr];
    NSLog(@"minDate = %@",minDateStr);
    
    if ([lastDateB timeIntervalSinceDate:minDate] >=0)
    {
         NSLog(@"lastDateB >= minDate)");
         currentDateStr = tempLastDateBStr;
         GrahpyArray = [HeartData MedianArrayDayCount:periodCount endDateStr:currentDateStr isHR:HrvYes database:db MovingInterval:MovingInterval MedianInterval:medianInterval];
         [self drawGraph];
         [todayLabel setHidden:YES];
        
    }
    else
    {
        NSLog(@"date beyond range");
    }
    
}

- (void)rightBtnClick:(id)sender
{
      NSLog(@"rightBtnClick---");
    if (minDateStr == nil ) {
        return;
    }

    NSDate *currentDate =    [df dateFromString:currentDateStr];
    NSDate *lastDateB =  [currentDate dateByAddingTimeInterval:periodCount*(24*60*60)];
    NSDate *lastDateA =  [currentDate dateByAddingTimeInterval:(24*60*60)];
    NSString *tempLastDateBStr = [df stringFromDate:lastDateB];
    NSLog(@"tempLastDateBStr = %@",tempLastDateBStr);
    NSString *tempLastDateAStr = [df stringFromDate:lastDateA];
    NSLog(@"tempLastDateAStr = %@",tempLastDateAStr);
//  NSDate *minDate = [df dateFromString:minDateStr];
    NSLog(@"minDate = %@",minDateStr);
    
    NSDate *today = [NSDate date];
    NSString *todayStr =  [df stringFromDate:today];
    
    if ([tempLastDateBStr compare:todayStr] <= 0)
    {
//      NSLog(@"lastDateB >= minDate)");
        currentDateStr = tempLastDateBStr;
        GrahpyArray = [HeartData MedianArrayDayCount:periodCount endDateStr:currentDateStr isHR:HrvYes database:db MovingInterval:MovingInterval MedianInterval:medianInterval];
        [self drawGraph];
        if ([tempLastDateBStr isEqualToString:todayStr])
        {
            [todayLabel setHidden:NO];
        }
        
    }
    else
    {
        NSLog(@"date beyond range");
    }

}

- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)monthBtnClick:(id)sender
{
    NSLog(@"monthBtnClick -----");
    NSLog(@"chart label is %ld",(long)chartLabel);
    
    if (chartLabel == ChartLabelMonth) {
        return;
    }

        chartLabel = ChartLabelMonth;
        yearBtn.selected=NO;
        sixMonthBtn.selected=NO;
        monthBtn.selected=YES;

    
        NSDate *currentDate = [NSDate date ];
        currentDateStr = [df stringFromDate:currentDate];
        NSLog(@"currentDateStr = %@",currentDateStr);
    
        if (minDateStr != nil )
        {

            [self GraphSettingMovingIterval:MovingIntervalDay periodCount:PeriodIntervalMonth chartXaxisLength:TotalPointMonth chartLabelDisplayCount:ChartLabelDisplayCountMonth medianInterval:MedianIntervalMonth];
            [self getDataFromDB:MovingInterval];
            [self drawGraph];
            
        }
    
 
}

- (IBAction)sixMonthBtnClick:(id)sender
{
    
    NSLog(@"sixMonthBtnClick -----");

    
    if (chartLabel == ChartLabelSixMonth) {
        return;
    }

        chartLabel = ChartLabelSixMonth;
        yearBtn.selected=NO;
        sixMonthBtn.selected=YES;
        monthBtn.selected=NO;

    
    NSDate *currentDate = [NSDate date ];
    currentDateStr = [df stringFromDate:currentDate];
    NSLog(@"currentDateStr = %@",currentDateStr);

    
        if (minDateStr != nil )
        {
  
            [self GraphSettingMovingIterval:MovingIntervalOneWeek periodCount:PeriodIntervalSixMonth chartXaxisLength:TotalPointSixMonth chartLabelDisplayCount:ChartLabelDisplayCountSixMonth medianInterval:MedianIntervalSixMonth];
            [self getDataFromDB:MovingInterval];
            [self drawGraph];
        
        }

    
}

- (IBAction)yearBtnClick:(id)sender {
    NSLog(@"yearBtnClick -----");

    if (chartLabel == ChartLabelOneYear) {
        return;
    }

        chartLabel = ChartLabelOneYear;
        yearBtn.selected=YES;
        sixMonthBtn.selected=NO;
        monthBtn.selected=NO;

        NSDate *currentDate = [NSDate date ];
        currentDateStr = [df stringFromDate:currentDate];
        NSLog(@"currentDateStr = %@",currentDateStr);
    
        if (minDateStr != nil )
       {

           [self GraphSettingMovingIterval:MovingIntervalTwoWeek periodCount:PeriodIntervalOneYear chartXaxisLength:TotalPointOneYear chartLabelDisplayCount:ChartLabelDisplayCountOneYear medianInterval:MedianIntervalOneYear];
           [self getDataFromDB:MovingInterval];
           [self drawGraph];
        
       }

    
}


-(void) GraphSettingMovingIterval:(NSUInteger) _MovingInterval periodCount:(NSUInteger) _periodCount chartXaxisLength:(NSUInteger) _chartXaxisLength chartLabelDisplayCount:(NSUInteger) _chartLabelDisplayCount medianInterval:(NSUInteger)_medianInterval
{
    
    MovingInterval                 = _MovingInterval;
    periodCount                     = _periodCount;
    chartXaxisLength              = _chartXaxisLength;
    chartLabelDisplayCount     = _chartLabelDisplayCount;
    medianInterval                 =_medianInterval;

}

-(void)drawGraph
{
    [hrDetailScatter removeFromSuperview];
    
    if ([CommonValues getScreenHeight] == 568)
    {
        hrDetailScatter = [[HRDetailScatterView alloc] initWithFrame:CGRectMake(5, 48+[CommonValues offsetHeight4DiffiOS]+35, 310  ,515-20-35) : GrahpyArray :currentDateStr :periodCount :chartXaxisLength :chartLabelDisplayCount];

    }
    else
    {
        hrDetailScatter = [[HRDetailScatterView alloc] initWithFrame:CGRectMake(5, 48+[CommonValues offsetHeight4DiffiOS]+35, 310  ,515-88-20-35) : GrahpyArray :currentDateStr :periodCount :chartXaxisLength :chartLabelDisplayCount];
    }
    
    [self.view addSubview:hrDetailScatter];

    self.leftSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipes:)];
    self.rightSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipes:)];
    
    self.leftSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    self.rightSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    
    [hrDetailScatter addGestureRecognizer:self.leftSwipeGestureRecognizer];
    [hrDetailScatter addGestureRecognizer:self.rightSwipeGestureRecognizer];
}

- (void)handleSwipes:(UISwipeGestureRecognizer *)sender
{
    if (sender.direction == UISwipeGestureRecognizerDirectionLeft) {
        [self rightBtnClick:nil];
    }
    
    if (sender.direction == UISwipeGestureRecognizerDirectionRight) {

        [self leftBtnClick:nil];
    }
}

-(void)getDataFromDB:(NSInteger) _MovingInterval
{
    
    //test new  method...
    GrahpyArray = [HeartData MedianArrayDayCount:periodCount endDateStr:currentDateStr isHR:HrvYes database:db MovingInterval:_MovingInterval MedianInterval:medianInterval];

}


@end

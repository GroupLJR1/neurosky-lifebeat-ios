//
//  ActivityDistanceViewController.m
//  WAT
//
//  Created by neurosky on 9/17/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import "ActivityDistanceViewController.h"

#import "TGBleEmulator.h"

@interface ActivityDistanceViewController ()

@end

@implementation ActivityDistanceViewController
@synthesize hv;
@synthesize step_value,calories_value,activity_time_value,sleep_value;
@synthesize title_calorie,title_date,title_end_time,title_start_time,title_mid_time;
@synthesize step,calories;
@synthesize img;

- ( void ) dataReceived: ( NSDictionary * ) data { return; }


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    __LOG_METHOD_START;
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    __LOG_METHOD_END;
    return self;
}
-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}
- (void)viewDidLoad
{
    __LOG_METHOD_START;
    [super viewDidLoad];
    myWholeView.frame = CGRectMake(0, [CommonValues offsetHeight4DiffiOS], 320, 568-20);
    db = [CommonValues getMyDB];
    // Do any additional setup after loading the view from its nib.
    NSLog(@"Activity distance didload------");
    
//    [self addClickEventsFor4Img];
    [[TGBleManager sharedTGBleManager] setDelegate:self];
    
    stepIndex =  [CommonValues getActivityIndex];
    NSLog(@"CommonValues getActivityIndex = %ld",(long)[CommonValues getActivityIndex]);
    df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    dbTimeArray = [[NSMutableArray alloc]init];
    sleepIntervalTimeArr = [[NSMutableArray alloc]init];
    sleepPhaseArray = [[NSMutableArray alloc]init];
    
    sleepTimeArray = [[NSMutableArray alloc]init];
    
    oneDayDistanceArr = [[NSMutableArray alloc]init];
    
    distanceColor =  [UIColor colorWithRed:40.0/255.0 green:190/255.0 blue:30/255.0 alpha:1.0];
   
    
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    goalDistance = [userDef integerForKey:USER_GOAL_DISTANCE];
    // NSLog(@"goalDistance = %d",goalDistance);
    storedUnits =  [userDef boolForKey:DISPLAY_IMPERIAL_UNITS];
    
    StepArray = [[NSArray alloc]init];
    step4GraphArray =  [[NSMutableArray alloc]init];
    mulStepArray =   [[NSMutableArray alloc]init];
    tempCaloriesArrList =[[NSMutableArray alloc]init];
    
    NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
    demoModeStr = [defaults1 stringForKey:@"DemoMode"];
    if ([demoModeStr isEqualToString:@"1"]) {
        NSLog(@" cardioPlot DemoMode ---- ");
        dbTimeArray = [[NSMutableArray alloc]initWithObjects:@"2014-02-13",@"2014-02-12",@"2014-02-11", nil];
        
        totalEndTime = 7.5*60;
        
        NSMutableArray *arr1 = [[NSMutableArray alloc]initWithObjects:@"50",@"216", @"195",@"163",@"85",@"208",@"300",@"63",@"75",@"106",@"70",@"217",@"293",@"86",@"114",@"76",@"69",nil];
        
        NSMutableArray *arr2 = [[NSMutableArray alloc]initWithObjects:@"0",@"166", @"145",@"13",@"35",@"158",@"250",@"13",@"25",@"36",@"20",@"167",@"243",@"36",@"34",@"26",@"19",nil];
        NSMutableArray *arr3 = [[NSMutableArray alloc]initWithObjects:@"30",@"196", @"175",@"43",@"65",@"188",@"280",@"43",@"55",@"86",@"50",@"197",@"273",@"66",@"64",@"56",@"49",nil];
        
        
        [tempCaloriesArrList addObject:arr1];
        [tempCaloriesArrList addObject:arr2];
        [tempCaloriesArrList addObject:arr3];
        
    }
    
    else{
        NSLog(@" cardioPlot RealMode ---- ");
        dbTimeArray = [CommonValues getPedTimeArr:db];
        
        [self getDataFromDB];
        if ([CommonValues startSleepAnalysis:stepIndex :db] == -1) {
            totalEndTime = 0;
            
        }
        
    }
    
    
    //test select like sleep  ,consider timeZone
    
    
    step_value.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
    step_value.backgroundColor = [UIColor clearColor];
    step_value.textColor = [CommonColors getStepColor];
    step_value.text = [NSString stringWithFormat:@"%@steps",[StringOperation intToString:Step_count]];
    if (dbTimeArray.count > 0) {
        step_value.text = [NSString stringWithFormat:@"%dsteps",Step_count];
    }
    
    // NSLog(@"3.5");
    calories_value.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
    calories_value.backgroundColor = [UIColor clearColor];
    calories_value.textColor = [CommonColors getCaloriesColor];
    calories_value.text = [NSString stringWithFormat:@"%dkcal",Calories_count];
    
    if (dbTimeArray.count > 0) {
        calories_value.text = [NSString stringWithFormat:@"%dkcal",Calories_count];
    }
    // NSLog(@"3.6");
    
    activity_time_value.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
    activity_time_value.backgroundColor = [UIColor clearColor];
    activity_time_value.textColor = [UIColor colorWithRed:255/255.0 green:156/255.0 blue:37/255.0 alpha:(1)];
    activity_time_value.text = @"1h24m";
    
    sleep_value.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
    sleep_value.backgroundColor = [UIColor clearColor];
    sleep_value.textColor = [UIColor colorWithRed:200/255.0 green:36/255.0 blue:167/255.0 alpha:(1)];
    sleep_value.text = @"6h42m";
    
    NSInteger hour = totalEndTime/60;
    NSInteger min = totalEndTime%60;
    NSLog(@"hour = %ld, min = %ld", (long)hour, (long)min);
    sleep_value.text = [NSString stringWithFormat:@"%ldh%ldm",(long)hour,(long)min];
    
    
    title_calorie.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:23.0];
    title_calorie.backgroundColor = [UIColor clearColor];
    title_calorie.textColor = [UIColor grayColor];
    title_calorie.text=@"Activity";
    
    title_date.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:20.0];
    title_date.backgroundColor = [UIColor clearColor];
    title_date.textColor = [UIColor grayColor];
    title_date.text=timeString;
    
    title_end_time.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:18.0];
    title_end_time.backgroundColor = [UIColor clearColor];
    title_end_time.textColor = [UIColor blackColor];
    title_end_time.text=@"0:00 AM";
    
    title_start_time.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:18.0];
    title_start_time.backgroundColor = [UIColor clearColor];
    title_start_time.textColor = [UIColor blackColor];
    title_start_time.text=@"0:00 AM";
    
    title_mid_time.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:18.0];
    title_mid_time.backgroundColor = [UIColor clearColor];
    title_mid_time.textColor = [UIColor blackColor];
    title_mid_time.text=@"12:00 PM";
    
    leftMidLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:18.0];;
    leftMidLabel.backgroundColor = [UIColor clearColor];
    leftMidLabel.textColor = [UIColor blackColor];
    leftMidLabel.text = @"6:00AM";
    
    
    rightMidLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:18.0];;
    rightMidLabel.backgroundColor = [UIColor clearColor];
    rightMidLabel.textColor = [UIColor blackColor];
    rightMidLabel.text = @"6:00PM";
    
    circle = [[circleProgressSmall alloc] initWithFrame:CGRectMake(20, 74+[CommonValues offsetHeight4DiffiOS], 55, 55)];
    circle.foreImage = [UIImage imageNamed:@"progress_distance"];
    circle.innerLabelTextColor = distanceColor;
    
    [self.view addSubview:circle];
    
    label=[[UILabel alloc]initWithFrame:CGRectMake(80, 80+[CommonValues offsetHeight4DiffiOS], 100, 50)];
    label.text=[NSString stringWithFormat:@"%@ m",[StringOperation intToString:Distance_count]];
    label.font =[UIFont fontWithName:DEFAULT_FONT_NAME size:35.0];
    //设置圆环右边的label颜色 [UIColor colorWithRed:47/255.0 green:105/255.0 blue:55/255.0 alpha:(1)];
    label.textColor = distanceColor;
    [self.view addSubview:label];
    // NSLog(@"3.7");
    step.text=@"STEPS:";
    step.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:18.0];
    step.textColor = [UIColor grayColor];
    
    calories.text=@"CALORIES:";
    calories.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:18.0];
    calories.textColor = [UIColor grayColor];
    // NSLog(@"3.8");
    
    //  NSLog(@"3.9");
    
    
    uiView=[[UIView alloc]initWithFrame:CGRectMake(0, 128+[CommonValues offsetHeight4DiffiOS], 50, 141)];
    [self.view addSubview:uiView];
    img=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 50, 141)];
    img.image=[UIImage imageNamed:@"symbol_distance"];
    img.userInteractionEnabled=YES;
    [uiView addSubview:img];
    //  NSLog(@"4.0");
    
    noticeLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 22, 50, 30)];
    
    if (StepArray.count>0) {
        noticeLabel.text=[NSString stringWithFormat:@"%@",[StepArray objectAtIndex:0]];
        
    }
    noticeLabel.textColor=[UIColor whiteColor];
    noticeLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:12.0];
    noticeLabel.textAlignment = NSTextAlignmentCenter;
    noticeLabel.backgroundColor = [UIColor clearColor];
    [uiView addSubview:noticeLabel];
    UIPanGestureRecognizer *pan=[[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handPan:)];
    pan.minimumNumberOfTouches=1;
    pan.maximumNumberOfTouches=1;
    
    [uiView addGestureRecognizer:pan];
    
    //    if (tempStepsArrList.count > stepIndex) {
    //         StepArray = [tempStepsArrList objectAtIndex:stepIndex];
    //    }
    
    StepArray = oneDayDistanceArr;
    
    
    circle.progress = 1.0*Distance_count/goalDistance;
    int aa=(int)([[NSString stringWithFormat:@"%.2f", 1.0*Distance_count/goalDistance]doubleValue]*100);
    circle.pString=[NSString stringWithFormat:@"%d%%",aa];
    
    [self drawGrafe];
    [self addValue];
    
    //    NSLog(@"StepArray = %@",StepArray);
    
    NSLog(@"set delegate to Dashboard, prepare to auto sync");
     [[TGBleManager sharedTGBleManager] setDelegate:[CommonValues getDashboardID]];
    __LOG_METHOD_END;
}

-(void)handPan:(UIPanGestureRecognizer *)paramSender{
    __LOG_METHOD_START;
    CGPoint point=[paramSender locationInView:self.view];
    // NSLog(@"x====%f",point.x);
    if (point.x<297 && 16<point.x) {
        uiView.frame=CGRectMake(point.x-25, 128+[CommonValues offsetHeight4DiffiOS], 50, 141);
    }
    if (point.x>297) {
        uiView.frame=CGRectMake(270, 128+[CommonValues offsetHeight4DiffiOS], 50, 141);
    }
    if (point.x<16) {
        uiView.frame=CGRectMake(0, 128+[CommonValues offsetHeight4DiffiOS], 50, 141);
    }
    float xielv = 11.347;
    //1m = 1.0936133yd
    
   
    for (int i = 0; i < 24; i++) {
        if ( point.x > (xielv*i+26)  &&  point.x < (xielv*i+26+7) ) {
            
            if (storedUnits) {
                  noticeLabel.text=[NSString stringWithFormat:@"%dyd",(int)(1.0936*[[StepArray objectAtIndex:i]intValue])];
            }
            else{
                noticeLabel.text=[NSString stringWithFormat:@"%@m",[StepArray objectAtIndex:i]];
            }
        }
        if ( point.x > (xielv*i+26+8) && point.x < (xielv*i+26+8+2.347)) {
            noticeLabel.text = @"";
        }
    }
    noticeLabel.backgroundColor = [UIColor clearColor];
    __LOG_METHOD_END;
}

-(void)addValue{
    __LOG_METHOD_START;
    circle.progress = 1.0*Distance_count/goalDistance;
    noticeLabel.text = @"";
    NSLog(@"addValue  oneDayTotalDistance sum every minute = %d",oneDayTotalDistance);
    if (dbTimeArray.count >= stepIndex+1) {
        float b =  1.0*oneDayTotalDistance/goalDistance;
        NSLog(@"goal distance percent = %f",b);
        
        circle.progress = b;
        
        // float c = 0.50;
        
        int aa=(int)([[NSString stringWithFormat:@"%.2f", 1.0*oneDayTotalDistance/goalDistance] doubleValue]*100);
        NSLog(@" goal distance percent format like 35%% ==%d",aa);
        
        
        if (aa > 100) {
            aa = 100;
        }
        else if(aa<  0){
            aa = 0;
        }
        circle.pString=[NSString stringWithFormat:@"%d%%",aa];
        
    }
    
    
    
    label.text=[NSString stringWithFormat:@"%@ m",[StringOperation intToString:Distance_count]];
    //    step_value.text = [NSString stringWithFormat:@"%@kcal",[self intToString:Calories_count]];
    //    calories_value.text = [NSString stringWithFormat:@"%dm",Distance_count];
    step_value.text = [NSString stringWithFormat:@"%@steps",[StringOperation intToString:Step_count]];
    calories_value.text = [NSString stringWithFormat:@"%dkcal",Calories_count];
    
    
    title_date.text=timeString;
    
    NSInteger hour = totalEndTime/60;
    NSInteger min = totalEndTime%60;
    NSLog(@"hour = %ld, min = %ld",(long)hour,(long)min);
    sleep_value.text = [NSString stringWithFormat:@"%ldh%ldm",(long)hour,(long)min];
    
    NSLog(@"1111");
    if (dbTimeArray.count > 0) {
        NSLog(@"[dbTimeArray objectAtIndex:timeDateIndex] = %@",[dbTimeArray objectAtIndex:stepIndex]);
        
        NSArray *time = [[dbTimeArray objectAtIndex:stepIndex] componentsSeparatedByString:@"-"];
        timeString = timeString = [StringOperation formatDateFromTimeArr:time];
        title_date.text=timeString;
    }
    NSLog(@"222");
    // 假如是最近的同步的那天，则使用currentCount 来代替 sum step/calories/distance
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *storedLatestStepStr =  [defaults objectForKey:STEP_LATESTDATE];
    NSString *storedLatestCalorieStr =  [defaults objectForKey:CALORIE_LATESTDATE];
    NSString *storedLatestDistanceStr =  [defaults objectForKey:DISTANCE_LATESTDATE];
    
    if (dbTimeArray.count > 0) {
        if (stepIndex == 0) {
            if (storedLatestDistanceStr == NULL) {
                storedLatestDistanceStr = @"0";
            }
            
            int storedDistance = [storedLatestDistanceStr intValue];
            label.text = [CommonValues distanceStrFromDistanceCount:storedDistance];
        }
        else{
            label.text = [CommonValues distanceStrFromDistanceCount:Distance_count];
        }
    }
    NSLog(@"333");
    
    if (dbTimeArray.count > 0) {
        if (stepIndex == 0) {
            if (storedLatestStepStr == NULL) {
                storedLatestStepStr = @"0";
            }
            step_value.text = [NSString stringWithFormat:@"%@steps",storedLatestStepStr];
        }
        else{
            step_value.text = [NSString stringWithFormat:@"%dsteps",Step_count];
        }
    }
    
    if (dbTimeArray.count > 0) {
        if (stepIndex == 0 ) {
            if (storedLatestCalorieStr == NULL) {
                storedLatestCalorieStr = @"0";
            }
            calories_value.text = [NSString stringWithFormat:@"%@kcal",storedLatestCalorieStr];
            
        }
        else{
            calories_value.text = [NSString stringWithFormat:@"%dkcal",Calories_count];
        }
    }
    
    
    //active time get
    NSString *activeTimeStr = @"0h0m";
    NSInteger active_time_minutes = 0;
    if (dbTimeArray.count > stepIndex ) {
        [CommonValues activeModeArr:[dbTimeArray objectAtIndex:stepIndex] :db];
        active_time_minutes = [CommonValues getActiveTimeTotalMinutes];
        activeTimeStr = [NSString stringWithFormat:@"%ldh%ldm",(long)active_time_minutes/60,(long)active_time_minutes%60];
    }
    activity_time_value.text = activeTimeStr;
    __LOG_METHOD_END;
}

-(void)drawGrafe{
    __LOG_METHOD_START;
    barChart = [[CPTXYGraph alloc] initWithFrame:CGRectZero];
    CPTTheme *theme = [CPTTheme themeNamed:kCPTPlainWhiteTheme];
    [barChart applyTheme:theme];
    // CPTGraphHostingView *hostingView = (CPTGraphHostingView *)hv;
    CPTGraphHostingView *hostingView = (CPTGraphHostingView *)hv;
    hostingView.hostedGraph = barChart;
    
    // Border
    barChart.plotAreaFrame.borderLineStyle = nil;
    barChart.plotAreaFrame.cornerRadius = 0.0f;
    
    // Paddings
    barChart.paddingLeft = 0.0f;
    barChart.paddingRight = 0.0f;
    barChart.paddingTop = 0.0f;
    barChart.paddingBottom = 0.0f;
    
    barChart.plotAreaFrame.paddingLeft = 0.0;
    barChart.plotAreaFrame.paddingTop = 20.0;
    barChart.plotAreaFrame.paddingRight = 0.0;
    barChart.plotAreaFrame.paddingBottom = 0.0;
    
    int maxDistance = [CommonValues findMaxNumInArr:oneDayDistanceArr];
    NSLog(@"maxDistance = %d",maxDistance);
    
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)barChart.defaultPlotSpace;
    plotSpace.allowsUserInteraction = NO;
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(0)length:CPTDecimalFromInt(maxDistance)];
    plotSpace.globalYRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(0)length:CPTDecimalFromInt(maxDistance)];
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(-1)length:CPTDecimalFromFloat(25)];
    
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)barChart.axisSet;
    CPTXYAxis *x = axisSet.xAxis;
    x.minorTickLineStyle = nil;
    
    x.labelingPolicy = CPTAxisLabelingPolicyNone;
    // x.axisConstraints = [CPTConstraints constraintWithRelativeOffset:0.2];
    
    CPTXYAxis *y = axisSet.yAxis;
    
    y.labelingPolicy = CPTAxisLabelingPolicyNone;
    y.axisLineStyle=CPTAxisLabelingPolicyNone;
    y.axisConstraints = [CPTConstraints constraintWithRelativeOffset:11/12.0];
    y.delegate = self;
    
    CPTColor *localDistanceColor = [CPTColor colorWithComponentRed:40/255.0 green:190/255.0 blue:30/255.0 alpha:1.0];
    //设置柱状图的颜色[UIColor colorWithRed:47/255.0 green:105/255.0 blue:55/255.0 alpha:(1)];
    CPTBarPlot *barPlot = [CPTBarPlot tubularBarPlotWithColor:localDistanceColor horizontalBars:NO];
    barPlot.dataSource = self;
    barPlot.identifier = @"Bar Plot 2";
    barPlot.fill = [CPTFill fillWithColor: localDistanceColor];
    barPlot.lineStyle = nil;
    [barChart addPlot:barPlot toPlotSpace:plotSpace];
    __LOG_METHOD_END;
}


-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot {
    __LOG_METHOD_START;
    return StepArray.count;
}
-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{
    __LOG_METHOD_START;
    NSDecimalNumber *num = nil;
    if ( [plot isKindOfClass:[CPTBarPlot class]] ) {
        switch ( fieldEnum ) {
            case CPTBarPlotFieldBarLocation:
                num = (NSDecimalNumber *)[NSDecimalNumber numberWithUnsignedInteger:index];
                break;
            case CPTBarPlotFieldBarTip:
                num = (NSDecimalNumber *)[NSDecimalNumber numberWithUnsignedInt:[[StepArray objectAtIndex:index ] intValue] ];
                break;
        }
    }
    __LOG_METHOD_END;
    return num;
}

- (IBAction)leftBt:(id)sender {
    __LOG_METHOD_START;

    if (stepIndex+1 >= dbTimeArray.count ) {
        __LOG_METHOD_END;
        return;
    }
    
     [[TGBleManager sharedTGBleManager] setDelegate:self];
    sleepIndex++;
    stepIndex++;
    totalEndTime = 0;
    
    caloriesIndex++;
    distanceIndex++;
    timeDateIndex++;
    [CommonValues setActivityIndex:stepIndex];
    NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
    demoModeStr = [defaults1 stringForKey:@"DemoMode"];
    if ([demoModeStr isEqualToString:@"1"]) {
        NSLog(@" cardioPlot DemoMode ---- ");
        totalEndTime = 7.5*60;
        
    }
    
    else{
        NSLog(@" cardioPlot RealMode ---- ");
        
        [self getDataFromDB];
        if ([CommonValues startSleepAnalysis:stepIndex :db] == -1) {
            totalEndTime = 0;
            
        }

        
    }
    
    mulStepArray = nil;
    mulStepArray = [[NSMutableArray alloc]init];

    NSLog(@"leftBt---");
    
    StepArray = oneDayDistanceArr;
    
    [self drawGrafe];
    [self addValue];
    NSLog(@"set delegate to Dashboard, prepare to auto sync");
    [[TGBleManager sharedTGBleManager] setDelegate:[CommonValues getDashboardID]];
    __LOG_METHOD_END;
}

- (IBAction)rightBt:(id)sender {
    __LOG_METHOD_START;

    if (stepIndex < 1) {
        __LOG_METHOD_END;
        return;
    }
    
     [[TGBleManager sharedTGBleManager] setDelegate:self];
    sleepIndex--;
    stepIndex--;
    caloriesIndex--;
    distanceIndex--;
    timeDateIndex--;
    totalEndTime = 0;
    [CommonValues setActivityIndex:stepIndex];
    NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
    demoModeStr = [defaults1 stringForKey:@"DemoMode"];
    if ([demoModeStr isEqualToString:@"1"]) {
        NSLog(@" cardioPlot DemoMode ---- ");
        totalEndTime = 7.5*60;
        
    }
    
    else{
        NSLog(@" cardioPlot RealMode ---- ");
        
        [self getDataFromDB];
        if ([CommonValues startSleepAnalysis:stepIndex :db] == -1) {
            totalEndTime = 0;
            
        }

    }
    mulStepArray = nil;
    mulStepArray = [[NSMutableArray alloc]init];
    StepArray = oneDayDistanceArr;
    
    //StepArray = mulStepArray;
    
    NSLog(@"StepArray = %@",StepArray);
    
    [self drawGrafe];
    [self addValue];
    NSLog(@"set delegate to Dashboard, prepare to auto sync");
    [[TGBleManager sharedTGBleManager] setDelegate:[CommonValues getDashboardID]];
    __LOG_METHOD_END;
}

- (void)sleepResults:(TGsleepResult) result
           startTime:(NSDate*) startTime
             endTime:(NSDate*) endTime
            duration:(int) duration  // in seconds
            preSleep:(int) preSleep // in seconds
            notSleep:(int) notSleep // in seconds active time
           deepSleep:(int) deepSleep // in seconds
          lightSleep:(int) lightSleep // in seconds
         wakeUpCount:(int) wakeUpCount // number of wake ups after 1st sleep
          totalSleep:(int) totalSleep
             preWake:(int) preWake // number of seconds active before the end time
          efficiency:(int) efficiency {
    __LOG_METHOD_START;
    NSString * message;
    int sleepSeconds = 0;
    switch (result) {
            
        case TGsleepResultValid:
        {
            NSLog(@"sleepResults: TGsleepResultValid");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\nTotal Seconds: %d\nSeconds before Sleep: %d\nSeconds AWAKE: %d\nSeconds Deep Sleep: %d\nSeconds Light Sleep: %d\nWake Up Count: %d\nTotal Sleep Seconds: %d\nEfficiency: %d %%",
                       startTime,
                       endTime,
                       duration,
                       preSleep,
                       notSleep,
                       deepSleep,
                       lightSleep,
                       wakeUpCount,
                       totalSleep,
                       efficiency
                       ];
            sleepSeconds = lightSleep + deepSleep;
            
            
        }
            break;
            
        case TGsleepResultNegativeTime:
            NSLog(@"sleepResults: TGsleepResultNegativeTime");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\n\nStart Time is After the End Time",
                       startTime,
                       endTime
                       ];
            break;
            
        case TGsleepResultMoreThan48hours:
            NSLog(@"sleepResults: TGsleepResultMoreThan24hours");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\n\nToo Much Time, must be less than 24 hours",
                       startTime,
                       endTime
                       ];
            break;
        case TGsleepResultNoData:
            NSLog(@"sleepResults: TGsleepResultNoData");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\n\nNo Sleep Data Supplied",
                       startTime,
                       endTime
                       ];
            break;
            
        case TGsleepResultNotValid:
            NSLog(@"sleepResults: TGsleepResultNotValid");
        default:
            NSLog(@"sleepResults: default");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\n\nUnexpected Sleep Results\nResult Code: %lu",
                       startTime,
                       endTime,
                       (unsigned long)result
                       ];
            
            break;
    }
    
    
    
    //    dispatch_async(dispatch_get_main_queue(), ^{ [alert show]; }); // do all alerts on the main thread
    NSLog(@"message  = %@",message);
    totalEndTime = sleepSeconds;
    
    sleep_value.text = [NSString stringWithFormat:@"%dh%dm",sleepSeconds/60,sleepSeconds%60];
    //    customerView3.progressImageView.progress = 1.0f*sleepSeconds/(goalSleep*60);
    
    __LOG_METHOD_END;
}



-(void)getDataFromDB{
    __LOG_METHOD_START;
    oneDayDistanceArr = nil;
    oneDayDistanceArr = [[NSMutableArray alloc]init];
    oneDayTotalStep = 0;
    oneDayTotalCalories = 0;
    oneDayTotalDistance = 0;
    
     sqlite3_stmt *stmt;
    //    [self getTimeArr];
    //     sqlite3_stmt *stmt;
    NSMutableArray *mutArray = [[NSMutableArray alloc]initWithCapacity:24];
    for (int i = 0; i < 24; i++) {
        [mutArray addObject:[NSNumber numberWithInt:0]];
    }
    if (dbTimeArray.count <= 0) {
        oneDayDistanceArr = mutArray;
        __LOG_METHOD_END;
        return;
    }
    
    NSString *thisDate =   [dbTimeArray objectAtIndex:stepIndex];
    NSString *quary3  = [NSString stringWithFormat:@"select datetime(datetime(time,'%ld hour','+0 minute'))  as time1,step,calories,distance from ped where time1 like '%@%%' order by time1 desc",(long)[[df timeZone] secondsFromGMT]/3600,thisDate];
  
//    NSLog(@"init mutArray = %@",mutArray);
    
    NSLog(@"查询这一天总的ped quary3 = %@",quary3);
    if(sqlite3_prepare_v2(db, [quary3 UTF8String], -1, &stmt, NULL) == SQLITE_OK) {
        while(sqlite3_step(stmt) == SQLITE_ROW) {
            const  unsigned  char *dateStr = sqlite3_column_text(stmt, 0);
            NSString *timeStr = [NSString stringWithFormat:@"%s",dateStr];
            
            NSString *subTimeStr =  [timeStr substringToIndex:13];
            //                    NSLog(@"subTimeStr = %@",subTimeStr);
            
            int temStep = sqlite3_column_int(stmt, 1);
            int temCalories = sqlite3_column_int(stmt, 2);
            
            int temDistance = sqlite3_column_int(stmt, 3);
            
            
            oneDayTotalStep +=  temStep;
            oneDayTotalCalories +=  temCalories;
            oneDayTotalDistance +=  temDistance;
            
            for (int  l = 0 ; l < 24; l++) {
                NSString *dateStr111 = [NSString stringWithFormat:@"%@ %02d",[dbTimeArray objectAtIndex:stepIndex],l];
                if ([subTimeStr isEqualToString:dateStr111]) {
                    
                    NSInteger   tmpStepValue = [[mutArray objectAtIndex:l] integerValue];
                    tmpStepValue += temDistance;
                    [mutArray replaceObjectAtIndex:l withObject:[NSNumber numberWithInteger:tmpStepValue ] ];
                }
            }
            
        }
        sqlite3_finalize(stmt);
    }
    
      NSLog(@"mutArray = %@",mutArray);
    NSLog(@"oneDayTotalStep = %d,oneDayTotalCalories = %d,oneDayTotalDistance = %d",oneDayTotalStep,oneDayTotalCalories,oneDayTotalDistance);
    
    
    Step_count = oneDayTotalStep;
    Calories_count = oneDayTotalCalories;
    Distance_count = oneDayTotalDistance;
    
    oneDayDistanceArr = mutArray;
    NSLog(@"oneDayDistanceArr = %@",oneDayDistanceArr);
    __LOG_METHOD_END;
}

-(void)getTimeArr{
    __LOG_METHOD_START;

    sqlite3_stmt *stmt = nil;
    
    // local  date time  from sleep
    NSString *dbTimeStr =[NSString stringWithFormat:@"select  distinct date(datetime(time,'%ld hour','+0 minute'))  as time1 from ped order by time1 desc;",(long)[[df timeZone] secondsFromGMT]/3600];
    
    
    NSLog(@"select_dbTimeStr = %@",dbTimeStr);
    
    if(sqlite3_prepare_v2(db, [dbTimeStr UTF8String], -1, &stmt, NULL) == SQLITE_OK) {
        while(sqlite3_step(stmt) == SQLITE_ROW) {
            const  unsigned  char *dateStr = sqlite3_column_text(stmt, 0);
            NSString *timeStr = [NSString stringWithFormat:@"%s",dateStr];
            //  NSLog(@"timeStr = %@",timeStr);
            [dbTimeArray addObject:timeStr];
            
        }
        sqlite3_finalize(stmt);
        
    }
    NSLog(@"dbTimeArray = %@",dbTimeArray);
    __LOG_METHOD_END;
}


- (void)didReceiveMemoryWarning
{
    __LOG_METHOD_START;
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    __LOG_METHOD_END;
}

- (IBAction)backBtnClick:(id)sender {
    __LOG_METHOD_START;
       [self dismissViewControllerAnimated:YES completion:nil];
    //    [self.navigationController popToRootViewControllerAnimated:YES];
    __LOG_METHOD_END;
}



-(void)addClickEventsFor4Img{
    __LOG_METHOD_START;
    caloriesImg.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap1 =           [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(caloriesImgClick:)];
    [caloriesImg addGestureRecognizer:singleTap1];
    
    stepImg.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap2 =           [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(stepImgClick:)];
    [stepImg addGestureRecognizer:singleTap2];
    
    sleepImg.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap3 =           [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sleepImgClick:)];
    [sleepImg addGestureRecognizer:singleTap3];
    
    activeImg.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap4 =           [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(activeImgClick:)];
    [activeImg addGestureRecognizer:singleTap4];
    __LOG_METHOD_END;
}
-(void)activeImgClick:(id)sender{
    __LOG_METHOD_START;
    ActiveTimeViewController *acvc = [[ActiveTimeViewController alloc]init];
    [self presentViewController:acvc animated:YES completion:nil];
    __LOG_METHOD_END;
}
-(void)caloriesImgClick:(id)sender{
    __LOG_METHOD_START;
    ActivityCalorieViewController *acvc = [[ActivityCalorieViewController alloc]init];
    [self presentViewController:acvc animated:YES completion:nil];
    __LOG_METHOD_END;
}
-(void)stepImgClick:(id)sender{
    __LOG_METHOD_START;
    ActivityStep *acvc = [[ActivityStep alloc]init];
    [self presentViewController:acvc animated:YES completion:nil];
    //     [self.navigationController popToViewController:acvc animated:YES];
    __LOG_METHOD_END;
}
-(void)sleepImgClick:(id)sender{
    __LOG_METHOD_START;
    int screenHeight = [CommonValues getScreenHeight];
    if (screenHeight == 568) {
        SleepViewController *cpvc = [[SleepViewController alloc]init] ;
        [self presentViewController:cpvc animated:YES
                         completion:nil];
        
    }
    
    else{
        Sleep_ViewController *cpvc = [[Sleep_ViewController alloc]init] ;
        [self presentViewController:cpvc animated:YES
                         completion:nil];
        
    }
    __LOG_METHOD_END;
}


@end

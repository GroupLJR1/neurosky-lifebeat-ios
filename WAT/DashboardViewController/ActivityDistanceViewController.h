//
//  ActivityDistanceViewController.h
//  WAT
//
//  Created by neurosky on 9/17/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"
#import "circleProgressSmall.h"
#import <sqlite3.h>
#import "CommonValues.h"
#import "circleProgressSmall.h"
#import "Constant.h"
#import "CommonColors.h"
#import "ActivityStep.h"
#import "SleepViewController.h"
#import "Sleep_ViewController.h"
#import "ActivityCalorieViewController.h"
#import "ActiveTimeViewController.h"
#import "TGBleManagerDelegate.h"


@interface ActivityDistanceViewController : UIViewController < CPTPlotDataSource, TGBleManagerDelegate > {
    BOOL storedUnits ;
    NSArray *StepArray;
    sqlite3 *db;
    NSMutableArray *mulStepArray;
    NSMutableArray *tempMulStepArray;
    NSArray *CaloriesArray;
    NSArray *DistanceArray;
    int Step_count;
    int Calories_count;
    int Distance_count;
    CPTXYGraph *barChart;
    CPTXYGraph *barChart1;
    CPTGraphHostingView *hv;
    circleProgressSmall *circle;
    UILabel *label;
    NSString *timeString;
    CGFloat beginy;
    UILabel *noticeLabel;
    UIView *uiView;
    NSMutableArray *step4GraphArray;
    NSMutableArray  *dbTimeArray;
    NSMutableArray *tempCaloriesArrList;
    NSMutableArray *oneDayDistanceArr; //6 am ~ 10 pm
    int subStepInOneHour;
    NSInteger stepIndex;
    int caloriesIndex;
    int distanceIndex;
    int timeDateIndex;
    int  tempStepIndex;
    NSMutableArray *sss;
    int recordCount;
    int recordIndex;
    NSInteger goalDistance;
    NSMutableArray * sleepPhaseArray;
    NSMutableArray *sleepTimeArray;
    int  sleepIndex;
    int dayTotalSleepTime;
    NSString *time1;
    NSString *time2;
    NSMutableArray *sleepIntervalTimeArr;
    NSInteger totalEndTime;
    int oneDayTotalStep;
    int oneDayTotalCalories;
    int oneDayTotalDistance;
    NSString *demoModeStr;
    NSString *database_path;
    NSDateFormatter *df;
    UIColor *distanceColor;
   
    __weak IBOutlet UILabel *rightMidLabel;
    __weak IBOutlet UILabel *leftMidLabel;
    __weak IBOutlet UIImageView *stepImg;
    
    __weak IBOutlet UIImageView *activeImg;
    __weak IBOutlet UIImageView *sleepImg;
    __weak IBOutlet UIImageView *caloriesImg;
    __weak IBOutlet UIView *myWholeView;
}

@property (strong, nonatomic) IBOutlet UILabel *title_calorie;
@property (strong, nonatomic) IBOutlet UILabel *title_date;
@property (strong, nonatomic) IBOutlet UILabel *title_end_time;
@property (strong, nonatomic) IBOutlet UILabel *title_start_time;
@property (strong, nonatomic) IBOutlet UILabel *title_mid_time;
@property (strong, nonatomic) IBOutlet CPTGraphHostingView *hv;
@property (strong, nonatomic) IBOutlet UILabel *step_value;
@property (strong, nonatomic) IBOutlet UILabel *calories_value;
@property (strong, nonatomic) IBOutlet UILabel *activity_time_value;
@property (strong, nonatomic) IBOutlet UILabel *sleep_value;
@property (strong, nonatomic) IBOutlet UILabel *step;
@property (strong, nonatomic) IBOutlet UILabel *calories;
@property (strong, nonatomic) IBOutlet UIImageView *img;
- (IBAction)leftBt:(id)sender;
- (IBAction)rightBt:(id)sender;
- (IBAction)backBtnClick:(id)sender;

@end


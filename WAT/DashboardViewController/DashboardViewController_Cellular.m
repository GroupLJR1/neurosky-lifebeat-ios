//
//  DashboardViewController_Cellular.m
//  WAT
//
//  Created by Mark I. Walsh on 2015-02-28.
//  Copyright (c) 2015 Mark I. Walsh. All rights reserved.
//

#import "DashboardViewController.h"
#import "DashboardViewController_Cellular.h"


@implementation DashboardViewController ( Cellular )


//  prompt warning about excessive data usage...
- ( void ) cellularDisplayDataWarning

    {

    __LOG_METHOD_START;

    NSUserDefaults * const userDefaults = [ NSUserDefaults standardUserDefaults ];
    BOOL const storedCellular = [ userDefaults boolForKey: CELLULAR_WARNNING_SWITH ];

    //  if we do not have an "ignore" instruction saved previously...
    if ( !storedCellular )

        {

        //  if we're running on iOS 8 or later...
        if ( [ CommonValues iOSVersion ] >= IOS8 )

            {

            alertController = [ UIAlertController alertControllerWithTitle: CELLULAR_TITLE
                                                                   message: CELLULAR_MSG
                                                            preferredStyle: UIAlertControllerStyleAlert
                              ];
            UIAlertAction * const cancelAction = [ UIAlertAction actionWithTitle: @"Do Not Show"
                                                                           style: UIAlertActionStyleCancel
                                                                         handler: ^( UIAlertAction * action )
                                                      {
                                                      [ self cellularNotShowAgain ];
                                                      }
                                                 ];
            UIAlertAction * const okAction = [ UIAlertAction actionWithTitle: @"OK"
                                                                       style: UIAlertActionStyleDefault
                                                                     handler: ^( UIAlertAction * action )
                                                      {
                                                      [ self cellularOK ];
                                                      }
                                             ];
            [ alertController addAction: cancelAction ];
            [ alertController addAction: okAction ];
            [ self performSelector: @selector( showAlertController )
                        withObject: nil
                        afterDelay: 0.1f
            ];
            }  // end process for iOS8 and above

        //  otherwise, we're running on an iOS version prior to 8...
        else

            {

            UIAlertView * const alert = [ [ UIAlertView alloc ] initWithTitle: CELLULAR_TITLE
                                                                      message: CELLULAR_MSG
                                                                     delegate: self
                                                            cancelButtonTitle: nil
                                                            otherButtonTitles: @"Do Not Show", @"OK", nil
                                        ];
            alert.tag = cellularTag;
            [ alert show ];

            }  // end process for iOS prior to version 8

        }  // end "ignore" instruction not saved

    __LOG_METHOD_END;

    return;

    }


//  handle "ignore" response...
- ( void ) cellularNotShowAgain

    {

    __LOG_METHOD_START;

    NSUserDefaults * const userDefalt = [ NSUserDefaults standardUserDefaults ];
    [ userDefalt setBool: YES
                  forKey: CELLULAR_WARNNING_SWITH
    ];
    [ userDefalt synchronize ];

    __LOG_METHOD_END;

    return;

    }


//  handle "OK" response...
- ( void ) cellularOK

    {

    __LOG_METHOD_START;

    //  do nothing, just keep processing normally...

    __LOG_METHOD_END;

    return;

    }


@end  //  end @implementation DashboardViewController ( Cellular )

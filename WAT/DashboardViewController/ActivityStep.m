//
//  ActivityStep.m
//  WAT
//
//  Created by NeuroSky on 11/11/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import "ActivityStep.h"

#import "TGBleEmulator.h"


@interface ActivityStep ()

@end

@implementation ActivityStep
@synthesize hv;
@synthesize calories_value,distance_value,activity_time_value,sleep_value;
@synthesize title_value,title_value2;
@synthesize step,calories;
@synthesize img;


- ( void ) dataReceived: ( NSDictionary * ) data { return; }


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    __LOG_METHOD_START;
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    __LOG_METHOD_END;
    return self;
}
-(NSUInteger)supportedInterfaceOrientations{
    __LOG_METHOD_START;
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    __LOG_METHOD_START;
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    __LOG_METHOD_START;
    return UIInterfaceOrientationPortrait;
}
- (void)viewDidLoad
{
    __LOG_METHOD_START;

    myWholeView.frame = CGRectMake(0, [CommonValues offsetHeight4DiffiOS], 320, 568-20);
    
    [super viewDidLoad];
    
    db = [CommonValues getMyDB];
    
    [[TGBleManager sharedTGBleManager] setDelegate:self];
    //    [self addClickEventsFor4Img];
    df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    dbTimeArray = [[NSMutableArray alloc]init];
    sleepIntervalTimeArr = [[NSMutableArray alloc]init];
    sleepPhaseArray = [[NSMutableArray alloc]init];
    
    sleepTimeArray = [[NSMutableArray alloc]init];
    
    oneDayStepArr = [[NSMutableArray alloc]init];
    
    stepColor = [UIColor colorWithRed:2.0/255.0 green:190.0/255.0 blue:253.0/255.0 alpha:1.0];
    
    
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    goalStep = [userDef integerForKey:USER_GOAL_STEP];
    storedUnits = [userDef boolForKey:DISPLAY_IMPERIAL_UNITS];
    // NSLog(@"goalStep = %d",goalStep);
    
    StepArray = [[NSArray alloc]init];
    step4GraphArray =  [[NSMutableArray alloc]init];
    mulStepArray =   [[NSMutableArray alloc]init];
    tempStepsArrList =[[NSMutableArray alloc]init];
    stepIndex = [CommonValues getActivityIndex];
    NSLog(@"CommonValues getActivityIndex = %ld",(long)[CommonValues getActivityIndex]);
    
    
    //add
    // NSLog(@"2");
    
    NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
    demoModeStr = [defaults1 stringForKey:@"DemoMode"];
    if ([demoModeStr isEqualToString:@"1"]) {
        NSLog(@" cardioPlot DemoMode ---- ");
        dbTimeArray = [[NSMutableArray alloc]initWithObjects:@"2014-02-13",@"2014-02-12",@"2014-02-11", nil];
        
        totalEndTime = 7.5*60;
        
        NSMutableArray *arr1 = [[NSMutableArray alloc]initWithObjects:@"50",@"216", @"195",@"163",@"85",@"208",@"300",@"63",@"75",@"106",@"70",@"217",@"293",@"86",@"114",@"76",@"69",nil];
        
        NSMutableArray *arr2 = [[NSMutableArray alloc]initWithObjects:@"0",@"166", @"145",@"13",@"35",@"158",@"250",@"13",@"25",@"36",@"20",@"167",@"243",@"36",@"34",@"26",@"19",nil];
        NSMutableArray *arr3 = [[NSMutableArray alloc]initWithObjects:@"30",@"196", @"175",@"43",@"65",@"188",@"280",@"43",@"55",@"86",@"50",@"197",@"273",@"66",@"64",@"56",@"49",nil];
        
        
        [tempStepsArrList addObject:arr1];
        [tempStepsArrList addObject:arr2];
        [tempStepsArrList addObject:arr3];
        
    }
    
    else{
        NSLog(@" cardioPlot RealMode ---- ");
        dbTimeArray = [CommonValues getPedTimeArr:db];
        
        [self getDataFromDB];
        
    }
    
    __LOG_METHOD_NOTE_STR( "loading fonts and values" );
    calories_value.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
    calories_value.backgroundColor = [UIColor clearColor];
    calories_value.textColor = [UIColor colorWithRed:255/255.0 green:44/255.0 blue:120/255.0 alpha:1.0];
    calories_value.text = [NSString stringWithFormat:@"%@kcal",[StringOperation intToString:Calories_count]];
    if (dbTimeArray.count > 0) {
        calories_value.text = [NSString stringWithFormat:@"%dkcal",Calories_count];
    }
    
    // NSLog(@"3.5");
    distance_value.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
    distance_value.backgroundColor = [UIColor clearColor];
    distance_value.textColor = [UIColor colorWithRed:40/255.0 green:190/255.0 blue:30/255.0 alpha:1];
    distance_value.text = [NSString stringWithFormat:@"%dm",Distance_count];
    
    if (dbTimeArray.count > 0) {
        distance_value.text = [NSString stringWithFormat:@"%dm",Distance_count];
    }
    // NSLog(@"3.6");
    
    activity_time_value.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
    activity_time_value.backgroundColor = [UIColor clearColor];
    activity_time_value.textColor = [UIColor colorWithRed:255/255.0 green:156/255.0 blue:37/255.0 alpha:(1)];
    activity_time_value.text = @"1h24m";
    
    sleep_value.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
    sleep_value.backgroundColor = [UIColor clearColor];
    sleep_value.textColor = [UIColor colorWithRed:200/255.0 green:36/255.0 blue:167/255.0 alpha:(1)];
    sleep_value.text = @"6h42m";
    
    if ([CommonValues startSleepAnalysis:stepIndex :db] == -1) {
        totalEndTime = 0;
        
    }
    
    NSInteger hour = totalEndTime/60;
    NSInteger min = totalEndTime%60;
    NSLog(@"hour = %ld, min = %ld", (long)hour, (long)min);
    sleep_value.text = [NSString stringWithFormat:@"%ldh%ldm", (long)hour, (long)min];
    
    
    title_value.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:23.0];
    title_value.backgroundColor = [UIColor clearColor];
    title_value.textColor = [UIColor grayColor];
    title_value.text=@"Activity";
    
    title_value2.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:20.0];
    title_value2.backgroundColor = [UIColor clearColor];
    title_value2.textColor = [UIColor grayColor];
    title_value2.text=timeString;
    UIFont *timeLabelFont = [UIFont fontWithName:DEFAULT_FONT_NAME size:18.0];
    
    leftLabel.font = timeLabelFont;
    leftLabel.backgroundColor = [UIColor clearColor];
    leftLabel.textColor = [UIColor blackColor];
    leftLabel.text=@"0:00 AM";
    
    leftMidLabel.font = timeLabelFont;
    leftMidLabel.backgroundColor = [UIColor clearColor];
    leftMidLabel.textColor = [UIColor blackColor];
    leftMidLabel.text = @"6:00AM";
    
    midLabel.font = timeLabelFont;
    midLabel.backgroundColor = [UIColor clearColor];
    midLabel.textColor = [UIColor blackColor];
    midLabel.text=@"12:00 PM";
    
    rightMidLabel.font = timeLabelFont;
    rightMidLabel.backgroundColor = [UIColor clearColor];
    rightMidLabel.textColor = [UIColor blackColor];
    rightMidLabel.text = @"6:00PM";
    
    rightLabel.font = timeLabelFont;
    rightLabel.backgroundColor = [UIColor clearColor];
    rightLabel.textColor = [UIColor blackColor];
    rightLabel.text=@"0:00 AM";
    
    circle = [[circleProgressSmall alloc] initWithFrame:CGRectMake(20, 74+[CommonValues offsetHeight4DiffiOS], 55, 55)];
    circle.foreImage = [UIImage imageNamed:@"progress_2"];
    circle.innerLabelTextColor = stepColor;
    [self.view addSubview:circle];
    
    label=[[UILabel alloc]initWithFrame:CGRectMake(80, 80+[CommonValues offsetHeight4DiffiOS], 150, 50)];
    label.text=[NSString stringWithFormat:@"%@ Steps",[StringOperation intToString:Step_count]];
    label.font =[UIFont fontWithName:DEFAULT_FONT_NAME size:35.0];
    label.textColor = stepColor;
    [self.view addSubview:label];
    // NSLog(@"3.7");
    step.text=@"STEPS:";
    step.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:18.0];
    step.textColor = [UIColor grayColor];
    
    calories.text=@"CALORIES:";
    calories.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:18.0];
    calories.textColor = [UIColor grayColor];
    // NSLog(@"3.8");
    
    
    uiView=[[UIView alloc]initWithFrame:CGRectMake(0, 128+[CommonValues offsetHeight4DiffiOS], 50, 141)];
    [self.view addSubview:uiView];
    img=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 50, 141)];
    img.image=[UIImage imageNamed:@"symbol_step@2x"];
    img.userInteractionEnabled=YES;
    [uiView addSubview:img];
    //  NSLog(@"4.0");
    
    noticeLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 22, 50, 30)];
    
    if (StepArray.count>0) {
        noticeLabel.text=[NSString stringWithFormat:@"%@",[StepArray objectAtIndex:0]];
        
    }
    noticeLabel.textColor=[UIColor whiteColor];
    noticeLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:12.0];
    noticeLabel.textAlignment = NSTextAlignmentCenter;
    noticeLabel.backgroundColor = [UIColor clearColor];
    [uiView addSubview:noticeLabel];
    UIPanGestureRecognizer *pan=[[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handPan:)];
    pan.minimumNumberOfTouches=1;
    pan.maximumNumberOfTouches=1;
    [uiView addGestureRecognizer:pan];
    
    
    StepArray = oneDayStepArr;
    
    
    circle.progress = 1.0*Step_count/goalStep;
    int aa=(int)([[NSString stringWithFormat:@"%.2f", 1.0*Step_count/goalStep]doubleValue]*100);
    circle.pString=[NSString stringWithFormat:@"%d%%",aa];
    
    [self drawGrafe];
    [self addValue];
    
    NSLog(@"StepArray count = %lu",(unsigned long)StepArray.count);
    
    NSLog(@"setDelegate to Dashboard,prepare for auto sync");
    [[TGBleManager sharedTGBleManager] setDelegate:[CommonValues getDashboardID]];
    
    __LOG_METHOD_END;
}

- (void)sleepResults:(TGsleepResult) result
           startTime:(NSDate*) startTime
             endTime:(NSDate*) endTime
            duration:(int) duration  // in seconds
            preSleep:(int) preSleep // in seconds
            notSleep:(int) notSleep // in seconds active time
           deepSleep:(int) deepSleep // in seconds
          lightSleep:(int) lightSleep // in seconds
         wakeUpCount:(int) wakeUpCount // number of wake ups after 1st sleep
          totalSleep:(int) totalSleep
             preWake:(int) preWake // number of seconds active before the end time
          efficiency:(int) efficiency {
    __LOG_METHOD_START;
    NSString * message;
    NSLog(@"-241 sleep call back ------------");
    int sleepSeconds = 0;
    switch (result) {
            
        case TGsleepResultValid:
        {
            NSLog(@"sleepResults: TGsleepResultValid");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\nTotal Seconds: %d\nSeconds before Sleep: %d\nSeconds AWAKE: %d\nSeconds Deep Sleep: %d\nSeconds Light Sleep: %d\nWake Up Count: %d\nTotal Sleep Seconds: %d\nEfficiency: %d %%",
                       startTime,
                       endTime,
                       duration,
                       preSleep,
                       notSleep,
                       deepSleep,
                       lightSleep,
                       wakeUpCount,
                       totalSleep,
                       efficiency
                       ];
            sleepSeconds = lightSleep + deepSleep;
            
            
        }
            break;
            
        case TGsleepResultNegativeTime:
            NSLog(@"sleepResults: TGsleepResultNegativeTime");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\n\nStart Time is After the End Time",
                       startTime,
                       endTime
                       ];
            break;
            
        case TGsleepResultMoreThan48hours:
            NSLog(@"sleepResults: TGsleepResultMoreThan24hours");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\n\nToo Much Time, must be less than 24 hours",
                       startTime,
                       endTime
                       ];
            break;
        case TGsleepResultNoData:
            NSLog(@"sleepResults: TGsleepResultNoData");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\n\nNo Sleep Data Supplied",
                       startTime,
                       endTime
                       ];
            break;
            
        case TGsleepResultNotValid:
            NSLog(@"sleepResults: TGsleepResultNotValid");
        default:
            NSLog(@"sleepResults: default");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\n\nUnexpected Sleep Results\nResult Code: %lu",
                       startTime,
                       endTime,
                       (unsigned long)result
                       ];
            
            break;
    }
    
    
    
    //    dispatch_async(dispatch_get_main_queue(), ^{ [alert show]; }); // do all alerts on the main thread
    NSLog(@"message  = %@",message);
    totalEndTime = sleepSeconds;
    
    sleep_value.text = [NSString stringWithFormat:@"%dh%dm",sleepSeconds/60,sleepSeconds%60];
    //    customerView3.progressImageView.progress = 1.0f*sleepSeconds/(goalSleep*60);
    
    __LOG_METHOD_END;
}

-(void)handPan:(UIPanGestureRecognizer *)paramSender{
    __LOG_METHOD_START;
    CGPoint point=[paramSender locationInView:self.view];
    if (point.x<297 && 16<point.x) {
        uiView.frame=CGRectMake(point.x - 25, 128+[CommonValues offsetHeight4DiffiOS], 50, 141);
    }
    if (point.x>297) {
        uiView.frame=CGRectMake(270, 128+[CommonValues offsetHeight4DiffiOS], 50, 141);
    }
    if (point.x<16) {
        uiView.frame=CGRectMake(0, 128+[CommonValues offsetHeight4DiffiOS], 50, 141);
    }
    
    float xielv = 11.347;
    
    for (int i = 0; i < 24; i++) {
        if ( point.x > (xielv*i+26)  &&  point.x < (xielv*i+26+7) ) {
            
            noticeLabel.text=[NSString stringWithFormat:@"%@",[StepArray objectAtIndex:i]];
        }
        if ( point.x > (xielv*i+26+8) && point.x < (xielv*i+26+8+2.347)) {
            noticeLabel.text = @"";
        }
    }
    
    
    noticeLabel.backgroundColor = [UIColor clearColor];
    __LOG_METHOD_END;
}

-(void)addValue{
    __LOG_METHOD_START;
    noticeLabel.text = @"";
    circle.progress = 1.0*Step_count/goalStep;
    
    NSLog(@"addValue  oneDayTotalStep sum every minute = %d",oneDayTotalStep);
    if (dbTimeArray.count >= stepIndex+1) {
        float b =  1.0*oneDayTotalStep/goalStep;
        NSLog(@"goal step percent = %f",b);
        
        circle.progress = b;
        
        // float c = 0.50;
        
        int aa=(int)([[NSString stringWithFormat:@"%.2f", 1.0*oneDayTotalStep/goalStep] doubleValue]*100);
        NSLog(@" goal step percent format like 35%% ==%d",aa);
        
        
        if (aa > 100) {
            aa = 100;
        }
        else if(aa<  0){
            aa = 0;
        }
        circle.pString=[NSString stringWithFormat:@"%d%%",aa];
        
    }
    
    
    
    label.text=[NSString stringWithFormat:@"%@ Steps",[StringOperation intToString:Step_count]];
    calories_value.text = [NSString stringWithFormat:@"%@kcal",[StringOperation intToString:Calories_count]];
    distance_value.text = [NSString stringWithFormat:@"%dm",Distance_count];
    title_value2.text=timeString;
    if ([CommonValues startSleepAnalysis:stepIndex :db] == -1) {
        totalEndTime = 0;
        
    }
    
    NSInteger hour = totalEndTime/60;
    NSInteger min = totalEndTime%60;
    NSLog(@"hour = %ld, min = %ld", (long)hour, (long)min);
    sleep_value.text = [NSString stringWithFormat:@"%ldh%ldm", (long)hour, (long)min];
    
    NSLog(@"1111");
    if (dbTimeArray.count > 0) {
        NSLog(@"[dbTimeArray objectAtIndex:timeDateIndex] = %@",[dbTimeArray objectAtIndex:stepIndex]);
        
        NSArray *time = [[dbTimeArray objectAtIndex:stepIndex] componentsSeparatedByString:@"-"];
        timeString = [StringOperation formatDateFromTimeArr:time];
        title_value2.text=timeString;
    }
    NSLog(@"222");
    
    // 假如是最近的同步的那天，则使用currentCount 来代替 sum step/calories/distance
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *storedLatestStepStr =  [defaults objectForKey:STEP_LATESTDATE];
    NSString *storedLatestCalorieStr =  [defaults objectForKey:CALORIE_LATESTDATE];
    NSString *storedLatestDistanceStr =  [defaults objectForKey:DISTANCE_LATESTDATE];
    
    if (dbTimeArray.count > 0) {
        if (stepIndex == 0) {
            if (storedLatestStepStr == NULL) {
                storedLatestStepStr = @"0";
            }
            label.text = [NSString stringWithFormat:@"%@ steps",storedLatestStepStr];
        }
        else{
            label.text = [NSString stringWithFormat:@"%d steps",Step_count];
        }
    }
    NSLog(@"333");
    
    if (dbTimeArray.count > 0) {
        if (stepIndex == 0) {
            if (storedLatestCalorieStr == NULL) {
                storedLatestCalorieStr = @"0";
            }
            calories_value.text = [NSString stringWithFormat:@"%@kcal",storedLatestCalorieStr];
        }
        else{
            calories_value.text = [NSString stringWithFormat:@"%dkcal",Calories_count];
        }
    }
    
    if (dbTimeArray.count > 0) {
        if (stepIndex == 0) {
            if (storedLatestDistanceStr == NULL) {
                storedLatestDistanceStr = @"0";
            }
            
            int storedDistance = [storedLatestDistanceStr intValue];
            distance_value.text = [CommonValues distanceStrFromDistanceCount:storedDistance];
            
        }
        else{
            
            distance_value.text =[CommonValues distanceStrFromDistanceCount:Distance_count];
            
        }
    }
    
    //active time get
    NSString *activeTimeStr = @"0h0m";
    NSInteger active_time_minutes = 0;
    if (dbTimeArray.count > stepIndex ) {
        [CommonValues activeModeArr:[dbTimeArray objectAtIndex:stepIndex] :db];
        active_time_minutes = [CommonValues getActiveTimeTotalMinutes];
        activeTimeStr = [NSString stringWithFormat:@"%ldh%ldm",(long)active_time_minutes/60,(long)active_time_minutes%60];
    }
    activity_time_value.text = activeTimeStr;
    __LOG_METHOD_END;
}
-(void)drawGrafe{
    __LOG_METHOD_START;
    barChart = [[CPTXYGraph alloc] initWithFrame:CGRectZero];
    CPTTheme *theme = [CPTTheme themeNamed:kCPTPlainWhiteTheme];
    [barChart applyTheme:theme];
    // CPTGraphHostingView *hostingView = (CPTGraphHostingView *)hv;
    CPTGraphHostingView *hostingView = (CPTGraphHostingView *)hv;
    hostingView.hostedGraph = barChart;
    
    // Border
    barChart.plotAreaFrame.borderLineStyle = nil;
    barChart.plotAreaFrame.cornerRadius = 0.0f;
    
    // Paddings
    barChart.paddingLeft = 0.0f;
    barChart.paddingRight = 0.0f;
    barChart.paddingTop = 0.0f;
    barChart.paddingBottom = 0.0f;
    
    barChart.plotAreaFrame.paddingLeft = 0.0;
    barChart.plotAreaFrame.paddingTop = 20.0;
    barChart.plotAreaFrame.paddingRight = 0.0;
    barChart.plotAreaFrame.paddingBottom = 0.0;
    
    int maxStep = [CommonValues findMaxNumInArr:oneDayStepArr];
    NSLog(@"maxStep = %d",maxStep);
    
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)barChart.defaultPlotSpace;
    plotSpace.allowsUserInteraction = NO;
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(0)length:CPTDecimalFromInt(maxStep)];
    plotSpace.globalYRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(0)length:CPTDecimalFromInt(maxStep)];
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(-1)length:CPTDecimalFromFloat(25)];
    
    
    //    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.0f) length:CPTDecimalFromFloat(150.0f)];
    //    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(-1.0f) length:CPTDecimalFromFloat(25.0f)];
    
    //	plotSpace.allowsUserInteraction = YES;
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)barChart.axisSet;
    CPTXYAxis *x = axisSet.xAxis;
    x.minorTickLineStyle = nil;
    
    x.labelingPolicy = CPTAxisLabelingPolicyNone;
    // x.axisConstraints = [CPTConstraints constraintWithRelativeOffset:0.2];
    
    CPTXYAxis *y = axisSet.yAxis;
    
    y.labelingPolicy = CPTAxisLabelingPolicyNone;
    y.axisLineStyle=CPTAxisLabelingPolicyNone;
    y.axisConstraints = [CPTConstraints constraintWithRelativeOffset:11/12.0];
    y.delegate = self;
    
    CPTBarPlot *barPlot = [CPTBarPlot tubularBarPlotWithColor:[CPTColor colorWithComponentRed:2.0/255.0 green:190.0/255.0 blue:253.0/255.0 alpha:1.0] horizontalBars:NO];
    barPlot.dataSource = self;
    barPlot.identifier = @"Bar Plot 2";
    barPlot.fill = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:2.0/255.0 green:190.0/255.0 blue:253.0/255.0 alpha:1.0]];
    barPlot.lineStyle = nil;
    [barChart addPlot:barPlot toPlotSpace:plotSpace];
    __LOG_METHOD_END;
}

-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot {
    __LOG_METHOD_START;
    return StepArray.count;
    }
-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{
    __LOG_METHOD_START;
    NSDecimalNumber *num = nil;
    if ( [plot isKindOfClass:[CPTBarPlot class]] ) {
        switch ( fieldEnum ) {
            case CPTBarPlotFieldBarLocation:
                
                num = (NSDecimalNumber *)[NSDecimalNumber numberWithUnsignedInteger:index];
                break;
            case CPTBarPlotFieldBarTip:
                
                
                num = (NSDecimalNumber *)[NSDecimalNumber numberWithUnsignedInt:[[StepArray objectAtIndex:index ] intValue] ];
                break;
        }
    }
    
    __LOG_METHOD_END;
    return num;
}

- (IBAction)back:(id)sender {
    __LOG_METHOD_START;
    [self dismissViewControllerAnimated:YES completion:nil];
    __LOG_METHOD_END;
}
- (IBAction)leftBt:(id)sender {
    __LOG_METHOD_START;
    if (stepIndex+1 >= dbTimeArray.count ) {
        __LOG_METHOD_END;
        return;
    }
    
    [[TGBleManager sharedTGBleManager] setDelegate:self];
    
    sleepIndex++;
    stepIndex++;
    totalEndTime = 0;
    [CommonValues setActivityIndex:stepIndex];
    
    caloriesIndex++;
    distanceIndex++;
    timeDateIndex++;
    
    NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
    demoModeStr = [defaults1 stringForKey:@"DemoMode"];
    if ([demoModeStr isEqualToString:@"1"]) {
        NSLog(@" cardioPlot DemoMode ---- ");
        totalEndTime = 7.5*60;
        
    }
    
    else{
        NSLog(@" cardioPlot RealMode ---- ");
        
        [self getDataFromDB];
        
    }
    
    mulStepArray = nil;
    mulStepArray = [[NSMutableArray alloc]init];
    
    
    NSLog(@"leftBt---");
    StepArray = oneDayStepArr;
    
    [self drawGrafe];
    [self addValue];
    NSLog(@"setDelegate to Dashboard,prepare for auto sync");
    [[TGBleManager sharedTGBleManager] setDelegate:[CommonValues getDashboardID]];
    
    __LOG_METHOD_END;
}

- (IBAction)rightBt:(id)sender {
    __LOG_METHOD_START;

    if (stepIndex < 1) {
        __LOG_METHOD_END;
        return;
    }
    
    NSLog(@"rightBt---");
    [[TGBleManager sharedTGBleManager] setDelegate:self];
    sleepIndex--;
    stepIndex--;
    caloriesIndex--;
    distanceIndex--;
    timeDateIndex--;
    totalEndTime = 0;
    [CommonValues setActivityIndex:stepIndex];
    NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
    demoModeStr = [defaults1 stringForKey:@"DemoMode"];
    if ([demoModeStr isEqualToString:@"1"]) {
        NSLog(@" cardioPlot DemoMode ---- ");
        totalEndTime = 7.5*60;
        
    }
    
    else{
        NSLog(@" cardioPlot RealMode ---- ");
        
        [self getDataFromDB];
    }
    mulStepArray = nil;
    mulStepArray = [[NSMutableArray alloc]init];
    StepArray = oneDayStepArr;
    
    NSLog(@"StepArray = %@",StepArray);
    
    [self drawGrafe];
    [self addValue];
    NSLog(@"setDelegate to Dashboard,prepare for auto sync");
    [[TGBleManager sharedTGBleManager] setDelegate:[CommonValues getDashboardID]];
    
    __LOG_METHOD_END;
}



-(void)getDataFromDB{
    __LOG_METHOD_START;

    oneDayStepArr = nil;
    oneDayStepArr = [[NSMutableArray alloc]init];
    
    oneDayTotalStep = 0;
    oneDayTotalCalories = 0;
    oneDayTotalDistance = 0;
    
    NSMutableArray *mutArray = [[NSMutableArray alloc]initWithCapacity:24];
    for (int i = 0; i < 24; i++) {
        [mutArray addObject:[NSNumber numberWithInt:0]];
    }
    
    sqlite3_stmt *stmt;
    if (dbTimeArray.count <= 0) {
        oneDayStepArr =  mutArray;
        __LOG_METHOD_END;
        return;
    }
    
    NSString * const thisDate =   [dbTimeArray objectAtIndex:stepIndex];
    NSString * const quary3  = [NSString stringWithFormat:@"select datetime(datetime(time,'%ld hour','+0 minute'))  as time1,step,calories,distance from ped where time1 like '%@%%' order by time1 desc",(long)[[df timeZone] secondsFromGMT]/3600,thisDate];
//    NSString *quary3  = [ NSString stringWithFormat: @"select datetime(datetime(time,'%d hour','+0 minute')) as time1,sum(step),sum(calories),sum(distance) from ped where time1 like '%@%%' group by time1 order by time1 desc"
//                                                   , [ [ df timeZone ] secondsFromGMT ] / 3600
//                                                   , thisDate
//                        ];

    NSLog(@"Total query this day - ped query3 = %@",quary3);
    
    
    if(sqlite3_prepare_v2(db, [quary3 UTF8String], -1, &stmt, NULL) == SQLITE_OK) {
        NSLog( @"query executed" );
        while(sqlite3_step(stmt) == SQLITE_ROW) {
            NSLog( @"row retrieved" );
            const  unsigned  char *dateStr = sqlite3_column_text(stmt, 0);
            NSString *timeStr = [NSString stringWithFormat:@"%s",dateStr];
            
            NSString *subTimeStr =  [timeStr substringToIndex:13];
            //                    NSLog(@"subTimeStr = %@",subTimeStr);
            
            int temStep = sqlite3_column_int(stmt, 1);
            int temCalories = sqlite3_column_int(stmt, 2);
            
            int temDistance = sqlite3_column_int(stmt, 3);
            
            
            oneDayTotalStep +=  temStep;
            oneDayTotalCalories +=  temCalories;
            oneDayTotalDistance +=  temDistance;
            
            for (int  l = 0 ; l < 24; l++) {
                NSString *dateStr111 = [NSString stringWithFormat:@"%@ %02d",[dbTimeArray objectAtIndex:stepIndex],l];
                if ([subTimeStr isEqualToString:dateStr111]) {
                    
                    NSInteger   tmpStepValue = [[mutArray objectAtIndex:l] integerValue];
                    tmpStepValue += temStep;
                    [mutArray replaceObjectAtIndex:l withObject:[NSNumber numberWithInteger:tmpStepValue ] ];
                }
            }
            
            
            
        }
        NSLog( @"query completed" );
        sqlite3_finalize(stmt);
    }
    
    NSLog(@"mutArray = %@",mutArray);
    
    
    NSLog(@"oneDayTotalStep = %d,oneDayTotalCalories = %d,oneDayTotalDistance = %d",oneDayTotalStep,oneDayTotalCalories,oneDayTotalDistance);
    
    
    Step_count = oneDayTotalStep;
    Calories_count = oneDayTotalCalories;
    Distance_count = oneDayTotalDistance;
    
    
    oneDayStepArr = mutArray;
    
    //    StepArray =  mutArray;
    NSLog(@"oneDayStepArr = %@",oneDayStepArr);
    __LOG_METHOD_END;
}


-(void)addClickEventsFor4Img{
    __LOG_METHOD_START;
    caloriesImg.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap1 =           [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(caloriesImgClick:)];
    [caloriesImg addGestureRecognizer:singleTap1];
    
    distanceImg.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap2 =           [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(distanceImgClick:)];
    [distanceImg addGestureRecognizer:singleTap2];
    
    sleepImg.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap3 =           [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sleepImgClick:)];
    [sleepImg addGestureRecognizer:singleTap3];
    
    activeImg.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap4 =           [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(activeImgClick:)];
    [activeImg addGestureRecognizer:singleTap4];
    __LOG_METHOD_END;
}
-(void)activeImgClick:(id)sender{
    __LOG_METHOD_START;
    ActiveTimeViewController *acvc = [[ActiveTimeViewController alloc]init];
    [self presentViewController:acvc animated:YES completion:nil];
    __LOG_METHOD_END;
}
-(void)caloriesImgClick:(id)sender{
    __LOG_METHOD_START;
    ActivityCalorieViewController *acvc = [[ActivityCalorieViewController alloc]init];
    [self presentViewController:acvc animated:YES completion:nil];
    __LOG_METHOD_END;
}

-(void)destorySelfView{
    __LOG_METHOD_START;
    [self dismissViewControllerAnimated:NO completion:nil];
    __LOG_METHOD_END;
}

-(void)distanceImgClick:(id)sender{
    __LOG_METHOD_START;
    ActivityDistanceViewController *acvc = [[ActivityDistanceViewController alloc]init];
    [self presentViewController:acvc animated:YES completion:nil];
    __LOG_METHOD_END;
}

- ( void ) sleepImgClick: ( id ) sender
{
    __LOG_METHOD_START;
    int screenHeight = [CommonValues getScreenHeight];
    if (screenHeight == 568) {
        SleepViewController *cpvc = [[SleepViewController alloc]init] ;
        [self presentViewController:cpvc animated:YES
                         completion:nil];
        
    }
    
    else{
        Sleep_ViewController *cpvc = [[Sleep_ViewController alloc]init] ;
        [self presentViewController:cpvc animated:YES
                         completion:nil];
        
    }
    __LOG_METHOD_END;
}

@end

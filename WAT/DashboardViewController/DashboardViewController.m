//
//  DashboardViewController.m
//  WAT
//
//  Created by  NeuroSky on 9/22/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import "DashboardViewController.h"
#import "DashboardViewController_Cellular.h"

#import "UIViewController_Extensions.h"

#import "TGBleEmulator.h"

#import "TGLibEmulator.h"

@interface DashboardViewController ()

@end
static float percent;
//use for synclog.
static NSString *syncTimeStr;
BOOL manualSyncFlag ;
BOOL syncPassFlag ;
int dataReceived;
int totalDataAvailable;


@implementation DashboardViewController
{
    NSTimer *autoSyncTaskTimer;
    float testIntValue;
    
}
@synthesize selectedIndexPath;
@synthesize manager;


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section { return -1; }
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath { return nil; }
- (void)sleepAnalysisResults:(TGEKGsleepResult)result
                   startTime:(NSDate *)startTime
                     endTime:(NSDate *)endTime
                    duration:(int)duration          // in minutes
                    preSleep:(int)preSleep          // in minutes
                    notSleep:(int)notSleep          // in minutes
                   deepSleep:(int)deepSleep         // in minutes
                  lightSleep:(int)lightSleep        // in minutes
                 wakeUpCount:(int)wakeUpCount       // number of wake ups after 1st sleep
                  totalSleep:(int)totalSleep
                     preWake:(int)preWake           // number of minutes active before the endTime
                  efficiency:(int)efficiency        // integer sleep efficiency
{ return; }

-(void)sleepAnalysisSmoothData:(int)samepleRate
                     sleepTime:(NSMutableArray*)sleepTimeArray
                    sleepPhase:(NSMutableArray*)sleepPhaseArray
{ return; }


- ( void ) touchtouchForDismiss { return; }


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    __LOG_METHOD_START;
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        CGRect rect_screen = [[UIScreen mainScreen]bounds];
        CGSize size_screen = rect_screen.size;
        screenHeight = size_screen.height;
        NSLog(@"screenHeight = %f",screenHeight);
        [CommonValues setScreenHeight:screenHeight];
        
    }
    __LOG_METHOD_END;
    return self;
}


//auto sync stuff
- (void) endBackgroundTask{
    __LOG_METHOD_START;
    //    [updateTimer invalidate];
    //    updateTimer = nil;
    [[UIApplication sharedApplication] endBackgroundTask:backgroundTask];
    backgroundTask = UIBackgroundTaskInvalid;
    //    [self startBackgroundTask];
    __LOG_METHOD_END;
}


- (void)doSyncStuff{
    __LOG_METHOD_START;
    //    return;
    backgroundTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        NSLog(@"beginBackgroundTaskWithExpirationHandler-----");
        
    }];
    
    NSData *sdkToken = [[TGBleManager sharedTGBleManager] bondToken];
    if (sdkToken == NULL) {
        NSLog(@"NO token,can't auto sync ,return");
        return;
        
    }
    
    if ([CommonValues getHasTaskFlag]) {
        NSLog(@"IS doing normal task, will do autoSync next time");
        return;
    }
    
    if ([CommonValues getAutoSyncingFlag]) {
        NSLog(@"already doing autoSYnc,return");
        return;
    }
    [TGBleManager sharedTGBleManager].delegate = [CommonValues getDashboardID];
    
    [CommonValues setAutoSyncingFlag:YES];
    wantSyncing = YES;
    syncTimeStr = [[CommonValues timeDateFormatter:DateFormatterTimeColon] stringFromDate:[NSDate date]];
    manualSyncFlag = NO;
    NSLog(@"auto sync  TimeStr = %@",syncTimeStr);
    if ([TGBleManager sharedTGBleManager].status == TGBleConnectedToBond) {
        NSLog(@"已连接，直接去call trySync");
        NSLog(@"call askCurrentCount");
        [[TGBleManager sharedTGBleManager] askCurrentCount ];
        syncMsgLabel.hidden = NO;
        syncMsgLabel.text = RECEIVE_CURRENTCOUNT_MSG;
        askCurrentCountNoResponseTimer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(askCurrrentNoResponseTodo) userInfo:nil repeats:NO];
        
        
    }
    else{
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *candId = [defaults objectForKey:@"DeviceID"];
        NSLog(@"call candidateConnect----");
        
        [[TGBleManager sharedTGBleManager] candidateConnect:candId];
        timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
    }
    __LOG_METHOD_END;
}
//auto sync stuff


- (void)viewDidLoad
{
    __LOG_METHOD_START;
    [super viewDidLoad];
    //offsetHeight4DiffiOS  for iOS 6 and >= 7
    myWholeView.frame = CGRectMake(0, [CommonValues offsetHeight4DiffiOS], 320, 568-20);
    db = [CommonValues getMyDB];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL storedAutoSyncSwitch = [userDefaults boolForKey:AUTOSYNC_SWITCH];
    NSString *storedAutoSyncTime =  [userDefaults objectForKey:AUTOSYNC_TIME];
    
    //    storedAutoSyncSwitch = NO;
    if (storedAutoSyncSwitch) {
        if (storedAutoSyncTime != NULL) {
            //开启自动同步任务计划s
            NSLog(@"start auto sync ----");
            NSInteger seconds = [CommonValues secondsFromString:storedAutoSyncTime];
            NSLog(@"AUTOSYNC_TIME stored seconds == %ld",(long)seconds);
            //            [CommonValues setAutoSyncTimer:[NSTimer scheduledTimerWithTimeInterval:seconds target:self selector:@selector(doSyncStuff) userInfo:nil repeats:YES]]  ;
            
            if (FALSE) {
                NSLog(@"background auto sync task 暂时不启用");
            }
            else{
                /*注释掉 为了测试T-T后台 */
                [[UIApplication sharedApplication] clearKeepAliveTimeout];
                NSLog(@"clearKeepAliveTimeout -------remove last background task ");
                [[UIApplication sharedApplication] setKeepAliveTimeout:seconds handler:^{
                    [[CommonValues getDashboardID] performSelector:@selector(doSyncStuff)];
                    
                }];
                
                NSLog(@" background auto sync task  开始");
                
            }
            
        }
    }
    
    [CommonValues setDashboardID:self];
    
    manager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    manager.delegate = self;
    tapGestureHR = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(eventTapGestureHR)];
    
    tapGestureHRV = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(eventTapGestureHRV)];
    
    tapGestureSleep=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(eventTapGestureSleep)];
    tapGestureDistance=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(eventTapGestureDistance)];
    dashboard = [[Dashboard  alloc] init];
    
    [ self cellularDisplayDataWarning ];
//    BOOL storedCellular =  [ userDefaults boolForKey:CELLULAR_WARNNING_SWITH];
//    
//    if (!storedCellular) {
//        
//        if ([CommonValues iOSVersion] >= IOS8) {
//            alertController = [UIAlertController alertControllerWithTitle:CELLULAR_TITLE message:CELLULAR_MSG preferredStyle:UIAlertControllerStyleAlert];
//            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Do Not Show" style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
//                [self cellularNotShowAgain];
//                
//            }];
//            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//                [self cellularOK];
//            }];
//            [alertController addAction:cancelAction];
//            [alertController addAction:okAction];
//            [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0.1];
//        }
//        else{
//            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:CELLULAR_TITLE message:CELLULAR_MSG      delegate:self cancelButtonTitle:nil otherButtonTitles:@"Do Not Show",@"OK", nil];
//            alert.tag = cellularTag;
//            [alert show];
//        }
//
//        
//    }
    ////////start here,from willappear
    
    __LOG_METHOD_END;
}

-(void)initVariables{
    __LOG_METHOD_START;
    [CommonValues  creatOrOpenDB :db];
    //reset ystLast
    ystLastSleepPhase = 1;
    ystLastSleepTime = @"";
    
    local_solid_df = [[NSDateFormatter alloc] init];
    // NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
    //reset ystLast
    ystLastSleepPhase = 1;
    ystLastSleepTime = @"";
    ped2Data = [[NSString alloc] init];
    sleep2Data = [[NSString alloc] init];
    // Do any additional setup after loading the view from its nib.
    
    // sleepGraphDataSouceArr = [[NSArray alloc]i];
    
    sleepGraphDataSouceArr = [[NSMutableArray alloc] initWithCapacity:600];
    
    dbSleepTimeArray = [[NSMutableArray alloc] init];
    
    hrvArr = [[NSMutableArray alloc] init];
    hrArr = [[NSMutableArray alloc] init];
    
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [local_solid_df setTimeZone:gmt];
    
    syncMsgLabel.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:10.0];
    syncProgressLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:15.0];
    
    successful_updated_label.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:25.0];
    successful_updated_label.textColor =  [UIColor colorWithRed:200/255.0f green:200/255.0f blue:200/255.0f alpha:1.0f];
    
    sleepIntervalTimeArr = [[NSMutableArray alloc]init];
    devicesArray  = [[NSMutableArray alloc]init];
    mfgDataArray = [[NSMutableArray alloc]init];
    rssiArray = [[NSMutableArray alloc]init];
    candIDArray = [[NSMutableArray alloc]init];
    
    devicesIDArray  = [[NSMutableArray alloc]init];
    
    sortedArr = [[NSMutableArray alloc]init];
    // [self getDeviceID]]
    connectedAlert = [[UIAlertView alloc]initWithTitle:@"Successfully connected" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@" OK ", nil];
    
    disconnectedAlert = [[UIAlertView alloc]initWithTitle:@"Disconnected" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@" OK ", nil];
    
    bondErrorAlert  = [[UIAlertView alloc]initWithTitle:@"Bond Failed" message: @"Please reset the band by holding the button for 30s the try again, or try another band" delegate:nil cancelButtonTitle:nil otherButtonTitles:@" OK ", nil];
    
    //    autoSyncingAlert = [[UIAlertView alloc]initWithTitle:@"" message:DoingAutoSync delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    
    [CommonValues setActivityIndex:0];
    wantErase = NO;
    isSyncing = NO;
    isRT = NO;
    wantRT = NO;
    wantSyncing = NO;
    
    successful_updated_label.hidden = YES;
    devicesArray  = [[NSMutableArray alloc]init];
    mfgDataArray  = [[NSMutableArray alloc]init];
    rssiArray  = [[NSMutableArray alloc]init];
    candIDArray  = [[NSMutableArray alloc]init];
    
    
    currentStepCount = 0;
    currentCalorieCount = 0;
    currentDistanceCount = 0;
    
    
    totalEndTime = 0 ;
    stepFromDb = 0 ;
    caloriesFromDb = 0;
    distanceFromDb = 0;
    
    hasPedoFlag = NO;
    hasEkgFlag = NO;
    hasSleepFlag = NO;
    fw_versionStr = @"";
    sw_versionStr = @"";
    
    [[TGBleManager sharedTGBleManager] setDelegate:self];
    NSLog(@"TGBleManager version = %d",[[TGBleManager sharedTGBleManager]getVersion]);
    bondToken = [[TGBleManager sharedTGBleManager] bondToken];
    NSLog(@"bondToken = %@",bondToken);
    //
    // enable the enhanced Ble manager logging
    // and redirect all console/NSLog data to a file in Documents/TG_log/Console
    
    NSLog(@" device id =  %@",[CommonValues getDeviceID] );
    //    bandOnRightWrist = NO;// should match the data file
    tglibEKG = [[TGLibEKG alloc] init];
    dbTimeArray = [[NSMutableArray alloc]init];
    sleepPhaseArray = [[NSMutableArray alloc]init];
    sleepTimeArray = [[NSMutableArray alloc]init];
    defaultDevicesArray = [CommonValues bandDeviceArray];
    
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    documentDir = [documentPaths objectAtIndex:0];
    //     NSLog(@" device id =  %@",[self getDeviceID] );
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    deviceName = [defaults stringForKey:@"DeviceName"];
    NSLog(@"NSUserDefaults deviceName = %@",deviceName);
    sync_in_progress = NO;
    eraseFlag = NO;
    //check if stanly demo mode
    if ([defaults stringForKey:@"StanlyDemoMode"] == nil  || [[defaults stringForKey:@"StanlyDemoMode"] isEqualToString:@"0"]) {
        
        //real mode
        [demoModeBtn setImage:[UIImage imageNamed:@"demo_mode"] forState:UIControlStateNormal];
    }
    else if ([[defaults stringForKey:@"StanlyDemoMode"] isEqualToString:@"1"]){
        //stanly demo mode
        
        [demoModeBtn setImage:[UIImage imageNamed:@"demo_mode2"] forState:UIControlStateNormal];
    }
    
    else{
        
        NSLog(@"StanlyDemoMode not regular");
    }
    if ([[defaults stringForKey:@"DemoMode"] isEqualToString:@"1"]){
        demoModeBtn.backgroundColor = [UIColor greenColor];
        NSLog(@"Dashboard DemoMode---1");
        goalStep = 5000;
        goalCalories = 2000;
        goalDistance = 8;
        //fake data
        stepFromDb = 2386;  //total step in one day
        caloriesFromDb = 1450;  //total calories in one day
        distanceFromDb = 5;
        totalEndTime = 7.5*60;
        currentDistanceCount = 5;
        currentCalorieCount = 1450;
        currentStepCount = 2386;
    }
    
    else {
        
        //get goals from userdefault;
        goalStep = [defaults integerForKey:USER_GOAL_STEP];
        goalCalories = [defaults integerForKey:USER_GOAL_CALORI];
        goalDistance = [defaults integerForKey:USER_GOAL_DISTANCE];
        goalSleep = [defaults floatForKey:USER_GOAL_SLEEP];
        goalActiveTime = [defaults floatForKey:USER_GOAL_ACTIVE_TIME];
        NSLog(@"Dashboard realMode---0");
        demoModeBtn.backgroundColor = [UIColor clearColor];
        [self getDataFromDB];
        //get sleep data;
        [self calculateSleepTime];
    }
    
    [self setStressConfig];
    __LOG_METHOD_END;
}

-(void)handleSwips:(UISwipeGestureRecognizer *)paramSender{
    __LOG_METHOD_START;
    if (paramSender.direction & UISwipeGestureRecognizerDirectionDown) {
        NSLog(@"Swiped Down");
    }else if (paramSender.direction & UISwipeGestureRecognizerDirectionLeft) {
        
        [UIView beginAnimations:@"AAA" context:nil];
        
        if (startSyncFlag) {
            // myScrollView.center=CGPointMake(160, 304);
            myScrollView.frame = CGRectMake(0, 43+41, 320, screenHeight-43-41-20);
            syncView.frame =  CGRectMake(0, 43, 320, 41);
            syncView.hidden = NO;
        }
        else
            //myScrollView.center=CGPointMake(160, 304);
            myScrollView.frame = CGRectMake(0, 43, 320, screenHeight-43-20);
        [UIView commitAnimations];
        [myScrollView setScrollEnabled:YES];
        
        
        NSLog(@"Swiped Left");
    }else if (paramSender.direction & UISwipeGestureRecognizerDirectionRight) {
        [UIView beginAnimations:@"AAA" context:nil];
        if (startSyncFlag) {
            //  myScrollView.center=CGPointMake(360, 304);
            myScrollView.frame = CGRectMake(200, 43+41, 320, screenHeight-43-41-20);
            syncView.frame =  CGRectMake(200, 43, 320, 41);
            syncView.hidden = NO;
        }
        else
            myScrollView.frame = CGRectMake(200, 43, 320, screenHeight-43-20);
        
        // myScrollView.center=CGPointMake(360, 304);
        // myScrollView.frame=CGRectMake(200, 45, self.view.bounds.size.width, self.view.bounds.size.height);
        [UIView commitAnimations];
        NSLog(@"Swiped Right");
    }else if (paramSender.direction & UISwipeGestureRecognizerDirectionUp) {
        NSLog(@"Swiped Up");
    }
    __LOG_METHOD_END;
}



- ( void ) eventTapGestureSteps
{
    __LOG_METHOD_START;

    ActivityStep * const cpvc = [ [ ActivityStep alloc ] init ];
    [ self presentViewController: cpvc
                        animated: YES
                      completion: nil
    ];

    //    [self.navigationController popToViewController:cpvc animated:YES];
    __LOG_METHOD_END;

}

-(void)eventTapGestureCalories{
    __LOG_METHOD_START;

    ActivityCalorieViewController *cpvc = [[ActivityCalorieViewController alloc]init] ;
    [self presentViewController:cpvc animated:YES
                     completion:nil];
    //    ActivityStep *cpvc = [[ActivityStep alloc]init] ;
    //    [self presentViewController:cpvc animated:YES
    //                     completion:nil];
    
    __LOG_METHOD_END;
}

-(void)eventTapGestureDistance{
    __LOG_METHOD_START;

    ActivityDistanceViewController *cpvc = [[ActivityDistanceViewController alloc]init] ;
    [self presentViewController:cpvc animated:YES
                     completion:nil];
    //    ActivityStep *cpvc = [[ActivityStep alloc]init] ;
    //    [self presentViewController:cpvc animated:YES
    //                     completion:nil];
    
    __LOG_METHOD_END;
}

-(void)eventTapGestureActiveTime{
    __LOG_METHOD_START;

    ActiveTimeViewController *cpvc = [[ActiveTimeViewController alloc]init] ;
    [self presentViewController:cpvc animated:YES
                     completion:nil];
    __LOG_METHOD_END;

}

-(void)eventTapGestureSleep{
    __LOG_METHOD_START;
    if (screenHeight == 568) {
        SleepViewController *cpvc = [[SleepViewController alloc]init] ;
        [self presentViewController:cpvc animated:YES
                         completion:nil];
        
    }
    
    else{
        Sleep_ViewController *cpvc = [[Sleep_ViewController alloc]init] ;
        [self presentViewController:cpvc animated:YES
                         completion:nil];
        
    }
    __LOG_METHOD_END;
}


-(void)eventTapGestureHR{
    __LOG_METHOD_START;

    HeartViewController *cpvc = [[HeartViewController alloc] initwithHrLabel:YES] ;
    [self presentViewController:cpvc animated:YES
                     completion:nil];
    __LOG_METHOD_END;
}

-(void)eventTapGestureHRV{
    __LOG_METHOD_START;
    HeartViewController *cpvc = [[HeartViewController alloc] initwithHrLabel:NO] ;

  //  HRVTrendDetailViewController *cpvc = [[HRVTrendDetailViewController alloc]init] ;
    [self presentViewController:cpvc animated:YES
                     completion:nil];
    __LOG_METHOD_END;
}

-(void)eventTapGestureStress{
    __LOG_METHOD_START;
    //    HeartViewController *cpvc = [[HeartViewController alloc]init] ;
    //    [self presentViewController:cpvc animated:YES
    //                     completion:nil];
    __LOG_METHOD_END;
}

-(void)eventTapGestureCardioplot{
    __LOG_METHOD_START;
    if ([CommonValues getAutoSyncingFlag]) {
        [[CommonValues autoSyncingAlert] show];
        return;
    }
    [CommonValues setPlayBackMode:NO];
    
    
    NSData *token =  [[TGBleManager sharedTGBleManager] bondToken];
    
    if (![CommonValues getBluetoothOnFlag]) {
        NSLog(@"BT OFF, can't go on candidateScaning");
        [self showBlueToothNotOnAlert];
        __LOG_METHOD_END;
        return;
    }
    
    [CommonValues setHasTaskFlag:YES];
    lostBeforeMainOperation = YES;
    
    if ([TGBleManager sharedTGBleManager].status == TGBleConnectedToBond) {
        //Existing connection and directly in the main operation
        NSLog(@"Existing connection and directly in the main operation");
        
        //        [SVProgressHUD showWithStatus:@" Start real time ECG"];
        NSThread *cardioPlotThread =  [[NSThread  alloc]initWithTarget:self selector:@selector(startCardioPlot) object:nil];
        [cardioPlotThread start];
    }
    else{
        NSLog(@"Not connected");
        
        if (token == NULL) {
            isRT = YES;
            wantRT = YES;
            wantSyncing = NO;
            [self but_select:nil];
            
        }
        else{
            //        [SVProgressHUD showWithStatus:@"Starting real time ECG..."];
            [self.view setUserInteractionEnabled:NO];
            isRT = YES;
            wantRT = YES;
            wantSyncing = NO;
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *candId = [defaults objectForKey:@"DeviceID"];
            NSLog(@"candId = %@",candId);
            NSLog(@"call candidateConnect ------");
            [[TGBleManager sharedTGBleManager] candidateConnect:candId];
            [SVProgressHUD showWithStatus:@" Connecting"];
            
            
        }
        
        timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
        
    }
    __LOG_METHOD_END;
}


- (NSUInteger)supportedInterfaceOrientations{
    __LOG_METHOD_START;
    return UIInterfaceOrientationMaskPortrait;
    
}


- (BOOL)shouldAutorotate{
    __LOG_METHOD_START;
    return NO;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    __LOG_METHOD_START;
    return UIInterfaceOrientationPortrait;
}



- (void)initUI{

    __LOG_METHOD_START;

    int ecgCount = 0;
    NSDate *date = [NSDate date];
    NSString* dateStr =  [[CommonValues timeDateFormatter:DateFormatterDate] stringFromDate:date];
    ecgCount = [CommonValues getDayECGCount:dateStr :db];
    
    NSLog(@"ecgCount  =  %d",ecgCount);
    
    if ([TGBleManager sharedTGBleManager].status == TGBleConnected ||
        [TGBleManager sharedTGBleManager].status == TGBleConnectedToBond  ) {
        [self delayTodo];
    }
    successful_updated_label.hidden = YES;
    
    jinCount = 0;
    yinCount = 0;
    tongCount = 0 ;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![[defaults stringForKey:@"DemoMode"] isEqualToString:@"1"]){
        
        NSString *currentLatestStep =  [defaults objectForKey:STEP_LATESTDATE];
        if (currentLatestStep != NULL) {
            currentStepCount = [currentLatestStep intValue];
            NSLog(@"currentLatestStep not null");
        }
        
        else {
            currentStepCount = 0;
            NSLog(@"currentLatestStep is null");
        }
        NSString *currentLatestCalorie =  [defaults objectForKey:CALORIE_LATESTDATE];
        if (currentLatestCalorie != NULL) {
            currentCalorieCount = [currentLatestCalorie intValue];
            NSLog(@"currentCalorieCount  = %d",currentCalorieCount);
        }
        else{
            currentCalorieCount = 0;
        }
        NSString *currentLatestDistance =  [defaults objectForKey:DISTANCE_LATESTDATE];
        NSLog(@"currentLatestDistance string = %@",currentLatestDistance);
        if (currentLatestDistance != NULL) {
            currentDistanceCount = [currentLatestDistance intValue];
            NSLog(@"currentDistanceCount  = %d",currentDistanceCount);
        }
        else{
            currentDistanceCount = 0;
        }
        
    }
    
    
    
    myScrollView.frame = CGRectMake(0, 43, 320, screenHeight-43-20);
    
    
    if ([CommonValues getIsECGAppMode]) {
        //ECGAppMode
        
        myScrollView.contentSize = CGSizeMake(320, 1265+130-380-365-95-205);
        // Heart section label
        UILabel  *heartLabel = [[UILabel alloc] initWithFrame:CGRectMake(210, 380-380, 100, 35)];
        heartLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:26.0];
        heartLabel.backgroundColor = [UIColor clearColor];
        heartLabel.textColor = [UIColor grayColor];
        heartLabel.text=@"     Heart";
        //  label2.text =  @"Last CardioPlot reading 1:28pm today";
        heartLabel.frame=CGRectMake(0, 380-380,80, 35);
        [myScrollView addSubview:heartLabel];
        
        
        UILabel  *heartLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(95, 380-380, 199, 25)];
        heartLabel2.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:17.0];
        heartLabel2.backgroundColor = [UIColor clearColor];
        heartLabel2.textColor = [UIColor lightGrayColor];
        heartLabel2.text=@"Last CardioPlot reading 1:28pm today";
        //  label2.text =  @"Last CardioPlot reading 1:28pm today";
        heartLabel2.frame=CGRectMake(95, 380-380, 199, 35);
        [myScrollView addSubview:heartLabel2];
        // heartLabel.frame=CGRectMake(0, 380,80, 35);
        
        
        //heart view /body view
        
        
        /*UIImageView **/hr_bpm_img = [[UIImageView alloc] initWithFrame:CGRectMake(11,350+50+20-380, 596/2, 240/2)];
        
        /*UILabel  **/hr_bpm_labelLeft = [[UILabel alloc] initWithFrame:CGRectMake(40,350+70+35-380,50, 50)];
        hr_bpm_labelLeft.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
        hr_bpm_labelLeft.backgroundColor = [UIColor clearColor];
        
        /*UILabel  **/hr_bpm_labeRight = [[UILabel alloc] initWithFrame:CGRectMake(272,350+70+65-380,50, 50)];
        hr_bpm_labeRight.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
        hr_bpm_labeRight.backgroundColor = [UIColor clearColor];
        
        [hr_bpm_img setImage:[UIImage imageNamed:@"hr_bpm@2x.png"]];
        [hr_bpm_labelLeft setText:@"78"];
        [hr_bpm_labeRight setText:@"72"];
        //ADD GestureRecognizer
        
        hr_bpm_img.userInteractionEnabled = YES;
        
        
        [hr_bpm_img addGestureRecognizer:tapGestureHR];
        
        [myScrollView addSubview:hr_bpm_img];
        [myScrollView addSubview:hr_bpm_labelLeft];
        [myScrollView addSubview:hr_bpm_labeRight];
        
        
        /*UIImageView **/ hrv_img = [[UIImageView alloc] initWithFrame:CGRectMake(11,350+0+130+50+15-380, 596/2, 240/2)];
        
        /*UILabel  **/hrv_labelLeft = [[UILabel alloc] initWithFrame:CGRectMake(40,350+35+130+50+15,50-380, 50)];
        
        hrv_labelLeft.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
        hrv_labelLeft.backgroundColor = [UIColor clearColor];
        
        /*UILabel  **/hrv_labelRight = [[UILabel alloc] initWithFrame:CGRectMake(272,350+65+130+50+15-380,50, 50)];
        hrv_labelRight.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
        hrv_labelRight.backgroundColor = [UIColor clearColor];
        
        [hrv_img setImage:[UIImage imageNamed:@"hrv@2x.png"]];
        [hrv_labelLeft setText:@"34"];
        [hrv_labelRight setTextColor:[UIColor blackColor]];
        
        [hrv_labelRight setText:@"24"];
        hrv_img.userInteractionEnabled = YES;
        
        [hrv_img addGestureRecognizer:tapGestureHRV];
        
        
        [myScrollView addSubview:hrv_img];
        [myScrollView addSubview:hrv_labelLeft];
        [myScrollView addSubview:hrv_labelRight];
        
        
        UIImageView *stressImg = [[UIImageView alloc] initWithFrame:CGRectMake(11,670-380, 596/2, 240/2)];
        [stressImg setImage:[UIImage imageNamed:@"stress@2x.png"]];
        
        [myScrollView addSubview:stressImg];
        
        
        
        ////
        UILabel  *stress_labelLeft = [[UILabel alloc] initWithFrame:CGRectMake(40,670+35-380,50, 50)];
        stress_labelLeft.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
        stress_labelLeft.backgroundColor = [UIColor clearColor];
        
        UILabel  *stress_labelRight = [[UILabel alloc] initWithFrame:CGRectMake(272,670+65-380,50, 50)];
        stress_labelRight.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
        stress_labelRight.backgroundColor = [UIColor clearColor];
        
        [stress_labelRight setText:@"22"];
        [stress_labelLeft setText:@"20"];
        //ADD GestureRecognizer
        
        stressImg.userInteractionEnabled = YES;
        
        UITapGestureRecognizer *tapGestureStress = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(eventTapGestureHR)];
        [stressImg addGestureRecognizer:tapGestureStress];
        
        [myScrollView addSubview:stress_labelLeft];
        [myScrollView addSubview:stress_labelRight];
        
        ///
        
        
        
        UIImageView *cardioplotImg = [[UIImageView alloc] initWithFrame:CGRectMake(11,920-125-380, 596/2, 240/2)];
        [cardioplotImg setImage:[UIImage imageNamed:@"cardioplot@2x.png"]];
        
        [myScrollView addSubview:cardioplotImg];
        
        cardioplotImg.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGestureCardioplot = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(eventTapGestureCardioplot)];
        [cardioplotImg addGestureRecognizer:tapGestureCardioplot];
        
        
        //play back image
        
        UIImageView *playbackImg = [[UIImageView alloc] initWithFrame:CGRectMake(11,920-125+125-380, 596/2, 240/2)];
        [playbackImg setImage:[UIImage imageNamed:@"playback@2x.png"]];
        
        [myScrollView addSubview:playbackImg];
        
        playbackImg.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapPlaybackImg = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(eventTapPlaybackImg)];
        [playbackImg addGestureRecognizer:tapPlaybackImg];
        
        
        //add separate line2
        UIImageView *sparateLine2 = [[UIImageView alloc]initWithFrame:CGRectMake(0,1055-125+125-380,320, 1)];
        sparateLine2.image = [UIImage imageNamed:@"line"];
        [myScrollView addSubview:sparateLine2];
        
    }
    
    else{
        
        //BaseAppMode
        
        myScrollView.contentSize = CGSizeMake(320, 1265+130-205);
        
        steps_circle = [[CircleProgress alloc] initWithFrame:CGRectMake(10, 40, 146, 146)];
        steps_circle.foreImage = [UIImage imageNamed:@"steps_progress_rate"];
        
        steps_circle.iconImage = [UIImage imageNamed:@"steps"];
        steps_circle.textColor = [UIColor colorWithRed:2.0/255.0 green:190.0/255.0 blue:253.0/255.0 alpha:1.0];
        steps_circle.targetString = @"/10,000";
        steps_circle.targetString = [NSString stringWithFormat:@"/%ld",(long)goalStep];
        //    circle.actualString = [self intToString:5100];
        steps_circle.actualString = @"10000";
        
        //    steps_circle.actualString = [NSString stringWithFormat:@"%d",stepFromDb];
        steps_circle.actualString = [NSString stringWithFormat:@"%d",currentStepCount];
        
        steps_circle.progress = 1.0;
        //    steps_circle.progress = 1.0*stepFromDb/goalStep;
        steps_circle.progress = 1.0*currentStepCount/goalStep;
        
        
        UITapGestureRecognizer *tapGestureSteps = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(eventTapGestureSteps)];
        [steps_circle addGestureRecognizer:tapGestureSteps];
        [myScrollView addSubview:steps_circle];
        
        //calories circle
        calories_circle = [[CircleProgress alloc] initWithFrame:CGRectMake(165, 40, 146, 146)];
        calories_circle.foreImage = [UIImage imageNamed:@"calories_progress_rate"];
        calories_circle.iconImage = [UIImage imageNamed:@"calories"];
        
        calories_circle.textColor =[UIColor colorWithRed:255/255.0 green:44/255.0 blue:120/255.0 alpha:1.0];
        calories_circle.targetString = @"/20,000";
        calories_circle.targetString = [NSString stringWithFormat:@"/%ld",(long)goalCalories];
        calories_circle.actualString = @"12500";
        
        //    calories_circle.actualString = [NSString stringWithFormat:@"%d",caloriesFromDb];
        
        
        NSLog(@"init UI currentCalorieCount = %d",currentCalorieCount);
        calories_circle.actualString = [NSString stringWithFormat:@"%d",currentCalorieCount];
        calories_circle.progress = 0.625;
        
        
        //    calories_circle.progress = 1.0*caloriesFromDb/goalCalories;
        
        calories_circle.progress = 1.0*currentCalorieCount/goalCalories;
        
        UITapGestureRecognizer *tapGestureCalories = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(eventTapGestureCalories)];
        [calories_circle addGestureRecognizer:tapGestureCalories];
        [myScrollView addSubview:calories_circle];
        
        NSString *distanceStr = [CommonValues distanceStrFromDistanceCount:currentDistanceCount];
        customerView1=[[CustomerView alloc]initWithFrame:CGRectMake(8,185+60, 98, 108) backgroundImage:[UIImage imageNamed:@"distance2@2x.png"] textValue:distanceStr withTextColor:[UIColor colorWithRed:40/255.0 green:190/255.0 blue:30/255.0 alpha:1] progress:(1.0*currentDistanceCount/goalDistance)progressForImgName:@"distance_progress"];
        [myScrollView addSubview:customerView1];
        
        [customerView1 addGestureRecognizer:tapGestureDistance];
        //
        NSString *activeTimeStr = @"0h0m";
        NSInteger activeTime = 0;
        if (dbTimeArray.count > 0 ) {
            [CommonValues activeModeArr:[dbTimeArray objectAtIndex:0] :db];
            activeTime = [CommonValues getActiveTimeTotalMinutes];
            activeTimeStr = [NSString stringWithFormat:@"%ldh%ldm",(long)activeTime/60,(long)activeTime%60];
        }
        
        
        customerView2=[[CustomerView alloc]initWithFrame:CGRectMake(112,185+60, 98, 108) backgroundImage:[UIImage imageNamed:@"active_time@2x.png"] textValue:activeTimeStr withTextColor:[UIColor colorWithRed:255/255.0 green:156/255.0 blue:37/255.0 alpha:(1)] progress:(1.0 *activeTime/ goalActiveTime/60) progressForImgName:@"active time_progress"] ;
        [myScrollView addSubview:customerView2];
        UITapGestureRecognizer *tapGestureActiveTime=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(eventTapGestureActiveTime)];
        [customerView2 addGestureRecognizer:tapGestureActiveTime];
        
      
        //Tommy Test Sleep ,use the longest one.
//        NSMutableArray *sleepTimesArr = [[NSMutableArray alloc] init];
        NSMutableArray * const sleepDateArr = /* [[NSMutableArray alloc] init];
        sleepDateArr = */ [CommonValues getSleepTimeArr :db];
        if (sleepDateArr.count > 0) {
            NSString *dateFromSleep = [sleepDateArr objectAtIndex:[CommonValues getActivityIndex]];
            [dashboard startAnalysisSleepWithDate:dateFromSleep withDB:db];
        }
        else{
            totalEndTime = 0;
        }
       
        sleep_hour = totalEndTime/60;
        sleep_min = totalEndTime%60;
        //      NSLog(@"hour = %d, min = %d",hour,min);
        
        
        customerView3=[[CustomerView alloc]initWithFrame:CGRectMake(216,185+60, 98, 108) backgroundImage:[UIImage imageNamed:@"sleep2@2x.png"] textValue:[NSString stringWithFormat:@"%ldh%ldm",(long)sleep_hour,(long)sleep_min] withTextColor:[UIColor colorWithRed:200/255.0 green:36/255.0 blue:167/255.0 alpha:(1)] progress:1.0*(sleep_hour*60+sleep_min)/goalSleep/60 progressForImgName:@"sleep_progress"] ;
        [myScrollView addSubview:customerView3];
        
        [customerView3 addGestureRecognizer:tapGestureSleep];
        
        
        //add separate line
        UIImageView *sparateLine1 = [[UIImageView alloc]initWithFrame:CGRectMake(0,370,320, 1)];
        sparateLine1.image = [UIImage imageNamed:@"line"];
        [myScrollView addSubview:sparateLine1];
        
        
        // activity label
        
        UILabel  *activityLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 7,80, 35)];;
        activityLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:26.0];
        activityLabel.backgroundColor = [UIColor clearColor];
        activityLabel.textColor = [UIColor grayColor];
        activityLabel.text = @"     Activity";
        [myScrollView addSubview:activityLabel];
        
        
        // step  calories label
        
        
        UILabel  *stepLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 145+40, 100, 35)];
        stepLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
        stepLabel.backgroundColor = [UIColor clearColor];
        stepLabel.textColor = [UIColor lightGrayColor];
        stepLabel.text = @"STEPS";
        [myScrollView addSubview:stepLabel];
        
        UILabel  *caloriesLabel = [[UILabel alloc] initWithFrame:CGRectMake(205, 145+40, 100, 35)];
        caloriesLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
        caloriesLabel.backgroundColor = [UIColor clearColor];
        caloriesLabel.textColor = [UIColor lightGrayColor];
        caloriesLabel.text = @"CALORIES";
        [myScrollView addSubview:caloriesLabel];
        
        // add gold_image
        //step medel position
        
        //    NSLog(@"steps percent = %f",steps_circle.progress);
        step_medal_image=[[UIImageView alloc]initWithFrame:CGRectMake(70, 15, 26, 35)];
        
        [myScrollView addSubview:step_medal_image];
        
        
        /*
         // ative time medal
         UIImageView *silver_image=[[UIImageView alloc]initWithFrame:CGRectMake(175, 200+20, 26, 35)];
         if (calories_circle.progress >= 1) {
         silver_image.image=[UIImage imageNamed:@"gold_medal@2x.png"];
         jinCount++;
         }
         else if (calories_circle.progress < 1 && calories_circle.progress >= 0.9){
         silver_image.image=[UIImage imageNamed:@"silver_medal@2x.png"];
         yinCount++;
         
         }
         
         else if (calories_circle.progress < 0.9 && calories_circle.progress >= 0.8){
         silver_image.image=[UIImage imageNamed:@"bronze_medal@2x.png"];
         tongCount++;
         
         }
         else
         {
         
         NSLog(@" active time no medal  ");
         }
         
         
         
         [myScrollView addSubview:silver_image];
         
         */
        
        // calorie medal position
        //  NSLog(@"calories percent = %f",calories_circle.progress);
        calorie_medal_image = [[UIImageView alloc]initWithFrame:CGRectMake(225, 15, 26, 35)];
        [myScrollView addSubview:calorie_medal_image];
        
        //distance medal position
        //  NSLog(@"distance percent = %f",1.0*currentDistanceCount/goalDistance);
        distance_medal_image=[[UIImageView alloc]initWithFrame:CGRectMake(70, 200+20, 26, 35)];
        [myScrollView addSubview:distance_medal_image];
        //active time medal position
        active_time_medal_image=[[UIImageView alloc]initWithFrame:CGRectMake(175, 200+20, 26, 35)];
        [myScrollView addSubview:active_time_medal_image];
        //sleep medal position
        sleep_medal_image=[[UIImageView alloc]initWithFrame:CGRectMake(280, 200+20, 26, 35)];
        [myScrollView addSubview:sleep_medal_image];
        
        
        label1 = [[UILabel alloc] initWithFrame:CGRectMake(134-20,15, 100, 25)];
        label1.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:17.0];
        label1.backgroundColor = [UIColor clearColor];
        label1.textColor = [UIColor lightGrayColor];
        
        NSString *lastSyncTime =  [defaults stringForKey:@"last_sync_time"];
        if (lastSyncTime) {
            label1.text = [NSString stringWithFormat:@"Last sync %@",lastSyncTime];
        }
        
        [myScrollView addSubview:label1];
        
        
        
        // Heart section label
        UILabel  *heartLabel = [[UILabel alloc] initWithFrame:CGRectMake(210, 380, 100, 35)];
        heartLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:26.0];
        heartLabel.backgroundColor = [UIColor clearColor];
        heartLabel.textColor = [UIColor grayColor];
        heartLabel.text=@"     Heart";
        //  label2.text =  @"Last CardioPlot reading 1:28pm today";
        heartLabel.frame=CGRectMake(0, 380,80, 35);
        [myScrollView addSubview:heartLabel];
        
        //add   ecg counts
        
        ecgCountIv1 = [[UIImageView alloc] initWithFrame:CGRectMake(100, 385, 23, 23)];
        ecgCountIv1.image =  [UIImage imageNamed:@"ecg_done"];
        [myScrollView addSubview:ecgCountIv1];
        ecgCountIv2 = [[UIImageView alloc] initWithFrame:CGRectMake(150, 385, 23, 23)];
        ecgCountIv2.image =  [UIImage imageNamed:@"ecg_done"];
        [myScrollView addSubview:ecgCountIv2];
        
        ecgCountIv3 = [[UIImageView alloc] initWithFrame:CGRectMake(200, 385, 23, 23)];
        ecgCountIv3.image =  [UIImage imageNamed:@"ecg_done"];
        [myScrollView addSubview:ecgCountIv3];
        
        [self changeECGCountImgWithCount:ecgCount];
        
        //heart view /body view
        
        
        hr_bpm_img = [[UIImageView alloc] initWithFrame:CGRectMake(11,350+50+20, 596/2, 240/2)];
        
        hr_bpm_labelLeft = [[UILabel alloc] initWithFrame:CGRectMake(23,350+70+35+5,48, 40)];
        hr_bpm_labelLeft.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
        hr_bpm_labelLeft.textAlignment = NSTextAlignmentCenter;
        hr_bpm_labelLeft.backgroundColor = [UIColor clearColor];
        
        hr_bpm_labeRight = [[UILabel alloc] initWithFrame:CGRectMake(250+10,350+70+65,40, 40)];
        hr_bpm_labeRight.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
        hr_bpm_labeRight.backgroundColor = [UIColor clearColor];
        hr_bpm_labeRight.textAlignment = NSTextAlignmentCenter;
        
        // test trend view instead of image
        [hr_bpm_img setImage:[UIImage imageNamed:@"hr_bpm@2x.png"]];
        
        
        //ADD GestureRecognizer
        
        hr_bpm_img.userInteractionEnabled = YES;
        
        
        [hr_bpm_img addGestureRecognizer:tapGestureHR];
        
        
        [myScrollView addSubview:hr_bpm_img];
        [myScrollView addSubview:hr_bpm_labelLeft];
        [myScrollView addSubview:hr_bpm_labeRight];
        
        hrv_img = [[UIImageView alloc] initWithFrame:CGRectMake(11,350+0+130+50+15, 596/2, 240/2)];
        
        hrv_labelLeft = [[UILabel alloc] initWithFrame:CGRectMake(23,350+70+35+5+120+5,48, 40)];
        
        hrv_labelLeft.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
        hrv_labelLeft.backgroundColor = [UIColor clearColor];
        hrv_labelLeft.textAlignment = NSTextAlignmentCenter;
        
        hrv_labelRight = [[UILabel alloc] initWithFrame:CGRectMake(250+10,350+70+65+120+5,40, 40)];
        hrv_labelRight.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
        hrv_labelRight.backgroundColor = [UIColor clearColor];
        hrv_labelRight.textAlignment = NSTextAlignmentCenter;
        
        //             hrv_labelLeft.backgroundColor = [UIColor yellowColor];
        //             hrv_labelRight.backgroundColor = [UIColor yellowColor];
        
        
        [hrv_img setImage:[UIImage imageNamed:@"hrv@2x.png"]];
        //    [hrv_labelLeft setText:@"34"];
        [hrv_labelRight setTextColor:[UIColor blackColor]];
        
        //    [hrv_labelRight setText:@"24"];
        hrv_img.userInteractionEnabled = YES;
        
        [hrv_img addGestureRecognizer:tapGestureHRV];
        
        
        [myScrollView addSubview:hrv_img];
        [myScrollView addSubview:hrv_labelLeft];
        [myScrollView addSubview:hrv_labelRight];
        
        
        //stanly demo mode check;
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if ([defaults stringForKey:@"StanlyDemoMode"] == nil  || [[defaults stringForKey:@"StanlyDemoMode"] isEqualToString:@"0"]) {
            // test trend view instead of image
            NSMutableArray * const mul_arr = /*[[NSMutableArray alloc]init];
            mul_arr =  */ [CommonValues  getHistoryHROrHRVArr2:db :YES];
            NSLog(@"------1111------");
            if (mul_arr.count > 0) {
                int sumHR = 0;
                NSInteger count = 0;
                count = mul_arr.count;
                for (int i = 0; i < mul_arr.count; i++) {
                    sumHR += [[mul_arr objectAtIndex:i]intValue];
                }
                int avgHR = sumHR/count;
                NSLog(@"sumHR = %d,count = %ld, hr_bpm_labeRight avgHR = %d",sumHR,(long)count,avgHR);
                //                NSMutableArray *temp2Arr = [[NSMutableArray alloc]init];
                //                temp2Arr  = mul_arr.lastObject;
                NSString *latestHR = [NSString stringWithFormat:@"%@", mul_arr.lastObject];
                NSString *weekAvgHR = [NSString stringWithFormat:@"%d", avgHR];
                
                //            [hr_bpm_labelLeft setText:[mul_arr objectAtIndex:0]];
                //            [hr_bpm_labeRight setText:[NSString stringWithFormat:@"%d",avgHR]];
                //以上会引起 [__NSCFNumber length]: unrecognized selector sent to instance  错误
                [hr_bpm_labelLeft setText:latestHR];
                [hr_bpm_labeRight setText:weekAvgHR];
                
                trendView1 = [[TrendView alloc]initWithFrame:CGRectMake(11+72,350+50+20+25+5+5, 596/2/2+30, 240/2/2) dataArr:mul_arr];
                [myScrollView addSubview:trendView1];
                //
                //                UITapGestureRecognizer *tapGestureHR = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(eventTapGestureHR)];
                //                [trendView1 addGestureRecognizer:tapGestureHR];
                
                //                TrendLineView *trendLineView1 = [[TrendLineView alloc]initWithFrame:CGRectMake(11+72,350+50+20+25+5+5, 596/2/2+30, 240/2/2) dataArr:mul_arr];
                
                trendLineView1 = [[TrendLineView alloc]initWithFrame:CGRectMake(11+72,350+50+20+25+5+5, 596/2/2+30, 240/2/2) :mul_arr];
                [myScrollView addSubview:trendLineView1];
                
                
                
                [trendLineView1 addGestureRecognizer:tapGestureHR];
                
            }
            
            //hrv graph
            NSLog(@"------2222------");
            
            NSMutableArray * const hrv_arr = /*[[NSMutableArray alloc]init];
            
            hrv_arr = */ [CommonValues getHistoryHROrHRVArr2:db:NO];
            
            if (hrv_arr.count > 0) {
                int avgHRV = [HeartData avgHrvWithHrvArr:hrv_arr];
                NSLog(@"hr_bpm_labeRight avgHRV = %d",avgHRV);
                
                NSString *latestHRV = [NSString stringWithFormat:@"%@",hrv_arr.lastObject];
                NSString *weekAvgHRV = [NSString stringWithFormat:@"%d", avgHRV];
                
                //            [hr_bpm_labelLeft setText:[mul_arr objectAtIndex:0]];
                //            [hr_bpm_labeRight setText:[NSString stringWithFormat:@"%d",avgHR]];
                //以上会引起 [__NSCFNumber length]: unrecognized selector sent to instance  错误
                [hrv_labelLeft setText:latestHRV];
                [hrv_labelRight setText:weekAvgHRV];
                
                trendView2 = [[TrendView alloc]initWithFrame:CGRectMake(11+72,455+120, 596/2/2+30, 240/2/2) dataArr:hrv_arr];
                
                [myScrollView addSubview:trendView2];
                
                
                trendLineView2 = [[TrendLineView alloc]initWithFrame:CGRectMake(11+72,455+120, 596/2/2+30, 240/2/2):hrv_arr];
                [myScrollView addSubview:trendLineView2];
                
                [trendLineView2 addGestureRecognizer:tapGestureHRV];
                
                
            }
            
        }
        
        else if ([[defaults stringForKey:@"StanlyDemoMode"] isEqualToString:@"1"]){
            UIImageView *trendView1IV = [[UIImageView alloc]initWithFrame:CGRectMake(11+72,350+50+20+25+5+5, 596/2/2+30, 240/2/2)];
            [ trendView1IV setImage:[UIImage imageNamed:@"hr_trend"]];
            
            [myScrollView addSubview:trendView1IV];
            /*UITapGestureRecognizer **/tapGestureHR = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(eventTapGestureHR)];
            [trendView1IV addGestureRecognizer:tapGestureHR];
            
            
            UIImageView *trendView2IV = [[UIImageView alloc]initWithFrame:CGRectMake(11+72,455+120, 596/2/2+30, 240/2/2) ];
            
            [ trendView2IV setImage:[UIImage imageNamed:@"hrv_trend"]];
            
            UITapGestureRecognizer *tapGestureHR1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(eventTapGestureHR1)];
            [trendView2IV addGestureRecognizer:tapGestureHR1];
            
            [myScrollView addSubview:trendView2IV];
            
            NSString *latestHR = @"72";
            NSString *weekAvgHR = @"78";
            NSString *latestHRV = @"105";
            NSString *weekAvgHRV = @"95";
            [hr_bpm_labelLeft setText:latestHR];
            [hr_bpm_labeRight setText:weekAvgHR];
            [hrv_labelLeft setText:latestHRV];
            [hrv_labelRight setText:weekAvgHRV];
            
        }
        
        else{
            NSLog(@"initUI get StanlyDemoMode vules unnormal,should be somewhere set error");
            
        }
        
        UIImageView *stressImg = [[UIImageView alloc] initWithFrame:CGRectMake(11,670, 596/2, 240/2)];
        [stressImg setImage:[UIImage imageNamed:@"stress@2x.png"]];
        [myScrollView addSubview:stressImg];
        
        ////
        UILabel  *stress_labelLeft = [[UILabel alloc] initWithFrame:CGRectMake(40,670+35,50, 50)];
        stress_labelLeft.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
        stress_labelLeft.backgroundColor = [UIColor clearColor];
        
        UILabel  *stress_labelRight = [[UILabel alloc] initWithFrame:CGRectMake(272,670+65,50, 50)];
        stress_labelRight.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
        stress_labelRight.backgroundColor = [UIColor clearColor];
        
        [stress_labelRight setText:@"22"];
        [stress_labelLeft setText:@"20"];
        //ADD GestureRecognizer
        
        stressImg.userInteractionEnabled = YES;
        
        UITapGestureRecognizer *tapGestureStress = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(eventTapGestureStress)];
        [stressImg addGestureRecognizer:tapGestureStress];
        
        [myScrollView addSubview:stress_labelLeft];
        [myScrollView addSubview:stress_labelRight];
        
        ///
        
        
        
        UIImageView *cardioplotImg = [[UIImageView alloc] initWithFrame:CGRectMake(11,920-125, 596/2, 240/2)];
        [cardioplotImg setImage:[UIImage imageNamed:@"cardioplot@2x.png"]];
        
        [myScrollView addSubview:cardioplotImg];
        
        cardioplotImg.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGestureCardioplot = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(eventTapGestureCardioplot)];
        [cardioplotImg addGestureRecognizer:tapGestureCardioplot];
        
        
        //play back image
        
        UIImageView *playbackImg = [[UIImageView alloc] initWithFrame:CGRectMake(11,920-125+125, 596/2, 240/2)];
        [playbackImg setImage:[UIImage imageNamed:@"playback@2x.png"]];
        
        [myScrollView addSubview:playbackImg];
        
        playbackImg.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapPlaybackImg = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(eventTapPlaybackImg)];
        [playbackImg addGestureRecognizer:tapPlaybackImg];
        
        
        
        //add separate line3
        UIImageView *sparateLine3 = [[UIImageView alloc]initWithFrame:CGRectMake(0,1310-65-150-35,320, 1)];
        sparateLine3.image = [UIImage imageNamed:@"line"];
        [myScrollView addSubview:sparateLine3];
        
        
        // Awards section label
        UILabel  *awardsLabel = [[UILabel alloc] initWithFrame:CGRectMake(210, 1310-240, 100, 35)];
        awardsLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:26.0];
        awardsLabel.backgroundColor = [UIColor clearColor];
        awardsLabel.textColor = [UIColor grayColor];
        awardsLabel.text=@"     Awards";
        //  label2.text =  @"Last CardioPlot reading 1:28pm today";
        awardsLabel.frame=CGRectMake(0, 1310-240,80, 35);
        
        [myScrollView addSubview:awardsLabel];
        
        UIImageView *jinImg = [[UIImageView alloc] initWithFrame:CGRectMake(20+5,1310+50-240, 26, 35)];
        [jinImg setImage:[UIImage imageNamed:@"gold_medal@2x.png"]];
        [myScrollView addSubview:jinImg];
        
        jinLabel = [[UILabel alloc] initWithFrame:CGRectMake(55, 1310+50-240, 50, 35)];
        jinLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:25.0];
        jinLabel.backgroundColor = [UIColor clearColor];
        jinLabel.textColor = [UIColor grayColor];
        
        [myScrollView addSubview:jinLabel];
        
        UIImageView *yinImg = [[UIImageView alloc] initWithFrame:CGRectMake(126+5,1310+50-240, 26, 35)];
        [yinImg setImage:[UIImage imageNamed:@"silver_medal@2x.png"]];
        [myScrollView addSubview:yinImg];
        
        yinLabel = [[UILabel alloc] initWithFrame:CGRectMake(55+106, 1310+50-240, 50, 35)];
        yinLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:25.0];
        yinLabel.backgroundColor = [UIColor clearColor];
        yinLabel.textColor = [UIColor grayColor];
        
        [myScrollView addSubview:yinLabel];
        
        
        UIImageView *tongImg = [[UIImageView alloc] initWithFrame:CGRectMake(232,1310+50-240, 26, 35)];
        [tongImg setImage:[UIImage imageNamed:@"bronze_medal@2x.png"]];
        [myScrollView addSubview:tongImg];
        tongLabel = [[UILabel alloc] initWithFrame:CGRectMake(55+106+106-5, 1310+50-240, 50, 35)];
        tongLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:25.0];
        tongLabel.backgroundColor = [UIColor clearColor];
        tongLabel.textColor = [UIColor grayColor];
        
        [myScrollView addSubview:tongLabel];
        
        NSLog(@"current step = %d ,calories = %d ,Distance = %d",currentStepCount,currentCalorieCount,currentDistanceCount);
        
        
        UISwipeGestureRecognizer *swip=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwips:)];
        swip.numberOfTouchesRequired=1;
        swip.direction=UISwipeGestureRecognizerDirectionLeft;
        [myScrollView addGestureRecognizer:swip];
        
        [self updateMedalImg];
        
    }
    __LOG_METHOD_END;
}

-(void)viewDidAppear:(BOOL)animated{
    __LOG_METHOD_START;
    [ super viewDidAppear: animated ];
    [[UIApplication sharedApplication]setStatusBarOrientation:UIInterfaceOrientationPortrait];
    __LOG_METHOD_END;
}

-(void)viewWillAppear:(BOOL)animated{
    __LOG_METHOD_START;
    [ super viewWillAppear: animated ];
    //    return;
    [self initVariables];
    
    //proThread  = [[NSThread alloc]initWithTarget:self selector:@selector(proThreadRun) object:nil];
    //[proThread start];
    
    [syncProgress setImage:[UIImage imageNamed:@"sync_progress"]];
    syncProgress.verticalProgress = NO;
    syncProgress.hasGrayscaleBackground = NO;
    
    syncProgress.progress = 0.0;
    //    progressLabel.text = [NSString stringWithFormat:@"%d",;
    syncProgressLabel.text = [NSString stringWithFormat:@"%d%%",(int)(syncProgress.progress*100)];
    sync_ped_data_size = 0;
    sync_bytes_read = 0;
    syncView.hidden = YES;
    
    // Do any additional setup after loading the view.
    
    struct utsname systemInfo;
    uname(&systemInfo);
    deviceString = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    NSLog(@"deviceString =  %@",deviceString);
    //test dddd
    [myScrollView removeFromSuperview];
    myScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 43, 320, screenHeight-43-20)];
    [myScrollView setBackgroundColor:[UIColor whiteColor]];
    myScrollView.delegate = self;
    [myWholeView addSubview:myScrollView];
    [self initUI];
    previousStepPercent =  1.0*currentStepCount/goalStep;
    previousCaloriePercent = 1.0*currentCalorieCount/goalCalories;
    NSLog(@"Dashboard viewWillappear end----");
    //test insert cloud data from txt.
    //           [CommonValues insertCloudSleepData:db];
    //         [CommonValues insertCloudPedData:db];
    
    //test new sleep calculate
    
    __LOG_METHOD_END;
}

- (void)didReceiveMemoryWarning
{
    __LOG_METHOD_START;
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    __LOG_METHOD_END;
}

#pragma marks
#pragma tableview datasource methods
#pragma mark -
- (NSInteger)popoverListView:(ZSYPopoverListView *)tableView numberOfRowsInSection:(NSInteger)section
{
    __LOG_METHOD_START;
    __LOG_METHOD_END;
    return devicesArray.count;
}

- (UITableViewCell *)popoverListView:(ZSYPopoverListView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    __LOG_METHOD_START;

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString * savedDeviceName = [defaults stringForKey:@"DeviceName"];
    NSLog(@"NSUserDefaults deviceName = %@",savedDeviceName);
    
    static NSString *identifier = @"identifier";
    UITableViewCell *cell = [tableView dequeueReusablePopoverCellWithIdentifier:identifier];
    if (nil == cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] ;
    }
    
    
    NSString *tmpStr = [NSString stringWithFormat:@"%@:%@",[ mfgDataArray objectAtIndex:indexPath.row],[ devicesArray objectAtIndex:indexPath.row]];
    //cell.textLabel.text =[ devicesArray objectAtIndex:indexPath.row];
    cell.textLabel.text = tmpStr;
    cell.textLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:25.0];
    
    if ([[ devicesArray objectAtIndex:indexPath.row] isEqualToString:savedDeviceName]) {
        // many devices have the same name
        //
        //cell.imageView.image = [UIImage imageNamed:@"fs_main_login_selected.png"];
    }
    
    __LOG_METHOD_END;
    return cell;
}

- (void)popoverListView:(ZSYPopoverListView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    __LOG_METHOD_START;
    NSLog(@"popoverListView deselect:%ld", (long)indexPath.row);
    __LOG_METHOD_END;
}

- (void)popoverListView:(ZSYPopoverListView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    __LOG_METHOD_START;
    self.selectedIndexPath = indexPath;
    NSLog(@"select:%ld", (long)indexPath.row);
    //  NSString *deviceName = [NSString stringWithFormat:@"Wat%d", indexPath.row];
    
    NSString *lDeviceName = [devicesArray objectAtIndex:indexPath.row];
    NSString *mfgData = [mfgDataArray objectAtIndex:indexPath.row];
    readyToSaveDeviceName = [devicesArray objectAtIndex:indexPath.row];
    readyToSaveDeviceID = [devicesIDArray objectAtIndex:indexPath.row];
    NSString *msg = [NSString stringWithFormat:@"Are you sure you would like to connect to %@ %@ ?",lDeviceName, mfgData];
    
    [listView dismiss];
    
    if ([CommonValues iOSVersion] >= IOS8) {
        alertController = [UIAlertController alertControllerWithTitle:msg message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
            [self selectDeviceAlertCancel];
            
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self selectDeviceAlertOK];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:NO completion:nil];
    }
    else{
        
        UIAlertView * const alert = [ [ UIAlertView alloc ] initWithTitle: msg
                                                                  message: nil
                                                                 delegate: self
                                                        cancelButtonTitle: @"Cancel"
                                                        otherButtonTitles: @"OK"
                                                                         , nil
                                    ];
        alert.tag   = selectDeviceAlertTag;
        [alert show];
    }
    
    __LOG_METHOD_END;
}


//alertview clicked do functions,  ios 7 use alertview & ios 8 use alertController ,  different.
#if( 0 )
- (void)checkBTStatusDone{
    __LOG_METHOD_START;
    if ([CommonValues getBluetoothOnFlag]) {
        //turn on the BT
        NSLog(@"already have  turn on the BT");
    }
    else{
        NSLog(@" BT  still not on state");
        [self showBlueToothNotOnAlert];
    }
    __LOG_METHOD_END;
}
#endif

- (void)hasTokenTimeOutAlertCancel{
    __LOG_METHOD_START;
    __LOG_METHOD_END;
}
- (void)hasTokenTimeOutAlertRetry{
    __LOG_METHOD_START;
    //Retry
    if (wantSyncing) {
        NSLog(@"  Retry isSyncing = ture");
        [self syncBtnClick:nil ];
    }
    else if (wantRT) {
        NSLog(@"Retry  isRT = ture");
        
        [self eventTapGestureCardioplot];
        
    }
    else{
        NSLog(@"isRT = no && isSyncing = no");
        
    }
    __LOG_METHOD_END;
}

- (void)noTokenTimeOutAlertCancel{
    __LOG_METHOD_START;
    __LOG_METHOD_END;
}
- (void)noTokenTimeOutAlertRetry{
    __LOG_METHOD_START;
    if (wantSyncing) {
        NSLog(@" Retry isSyncing = ture");
        [self syncBtnClick:nil ];
    }
    else if (wantRT) {
        NSLog(@"Retry  isRT = ture");
        
        [self eventTapGestureCardioplot];
        
    }
    else{
        NSLog(@"isRT = no && isSyncing = no");
        
    }
    __LOG_METHOD_END;
}

- (void)selectDeviceAlertCancel{
    __LOG_METHOD_START;
    if (wantSyncing) {
        isSyncing = NO;
        [self unLockSyncBtn];
    }
    else if (wantRT){
        isRT = NO;
        
    }
    [self.view setUserInteractionEnabled:YES];
    [self stopScan];
    [self resetDeviceListTemp];
    
    __LOG_METHOD_END;
}
- (void)selectDeviceAlertOK{
    __LOG_METHOD_START;
    [self stopScan];
    [devicesArray removeAllObjects];
    [mfgDataArray removeAllObjects];
    [rssiArray removeAllObjects];
    [candIDArray removeAllObjects];
    [devicesIDArray removeAllObjects];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSLog(@"need to save deviceName = %@",readyToSaveDeviceName);
    //if ([defaults stringForKey:@"DeviceName"] == nil) {
    [defaults setObject: readyToSaveDeviceName forKey:@"DeviceName"];
    [defaults setObject: readyToSaveDeviceID forKey:@"DeviceID"];
    [defaults synchronize];
    
    //candidate connect to the band;
    NSLog(@"call candidateConnect----");
    [[TGBleManager sharedTGBleManager] candidateConnect:readyToSaveDeviceID];
    
    timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
    [SVProgressHUD showWithStatus:@" Connecting"];
    
    if (wantSyncing) {
        //                    [SVProgressHUD showWithStatus:@"Syncing..."];
        [self.view setUserInteractionEnabled:NO];
    }
    else if (wantRT){
        //                    [SVProgressHUD showWithStatus:@"Starting real time ECG..."];
        [self.view setUserInteractionEnabled:NO];
    }
    [self resetDeviceListTemp];
    __LOG_METHOD_END;
}

- (void)resetDeviceListTemp{
    __LOG_METHOD_START;
    [devicesArray removeAllObjects];
    [mfgDataArray removeAllObjects];
    [rssiArray removeAllObjects];
    [candIDArray removeAllObjects];
    [devicesIDArray removeAllObjects];
    [listView dismiss];
    selectDeviceBtnClickFlag = NO;
    
    __LOG_METHOD_END;
}

- (void)bondAlertCancel{
    __LOG_METHOD_START;
    NSLog(@"call tryDisconnect------");
    [[TGBleManager sharedTGBleManager] tryDisconnect];
    __LOG_METHOD_END;
}
- (void)bondAlertOK{
    __LOG_METHOD_START;
    NSLog(@"call takeBond------");
    [[TGBleManager sharedTGBleManager] takeBond];
    [SVProgressHUD showWithStatus:@" Bonding"];
    __LOG_METHOD_END;
}

- (void)LostConnectAlertCancel{
    __LOG_METHOD_START;
    __LOG_METHOD_END;
}
- (void)LostConnectAlertOK{
    __LOG_METHOD_START;
    NSLog(@"call candidateConnect------");
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *candId = [defaults objectForKey:@"DeviceID"];
    [[TGBleManager sharedTGBleManager] candidateConnect:candId];
    timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
    [SVProgressHUD showWithStatus:@" Connecting"];
    __LOG_METHOD_END;
}

- (void)syncReportAlertOK{
    __LOG_METHOD_START;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *stored_name = [defaults objectForKey:USER_NAME];
    NSLog(@"stored_name = %@ ",stored_name);
    if ((stored_name!= NULL ) && (![stored_name isEqualToString:@""])) {
        // upload to S3
        [self performSelector:@selector(startUploadS3) withObject:(nil) afterDelay:3];
    }
    else{
        NSLog(@"user name is null ,don't upload to S3 ,delete local files");
        [dashboard deleteAllSyncData];
    }
    __LOG_METHOD_END;
}

- (void)noTokenBondFailedCancel{
    __LOG_METHOD_START;
    [SVProgressHUD dismiss];
    [self.view setUserInteractionEnabled:YES];
    __LOG_METHOD_END;
}
- (void)noTokenBondFailedRetry{
    __LOG_METHOD_START;
    [SVProgressHUD dismiss];
    //connect previous band
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"DeviceName"];
    NSString *candId = [defaults objectForKey:@"DeviceID"];
    NSLog(@"stored candId = %@",candId);
    lostBeforeMainOperation = YES;
    
    NSLog(@"call candidateConnect---");
    [[TGBleManager sharedTGBleManager]candidateConnect:candId];
    timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
    
    [self delay2ShowSVProgressHUD:@" Connecting"];
    
    if (wantSyncing) {
        //                    [SVProgressHUD showWithStatus:@"Syncing..."];
    }
    else if (wantRT){
        //                  [SVProgressHUD showWithStatus:@"Starting real time ECG..."];
    }
    else{
        NSLog(@"wantSyncing == NO && wantRT == NO");
    }
    __LOG_METHOD_END;
}
- (void)noTokenBondFailedRtyAnother{
    __LOG_METHOD_START;
    [SVProgressHUD dismiss];
    if (wantSyncing) {
        //scan the nearby bands.
        [self syncBtnClick:nil];
    }
    else if (wantRT){
        [self eventTapGestureCardioplot];
    }
    else{
        NSLog(@"wantSyncing == NO && wantRT == NO");
    }
    __LOG_METHOD_END;
}

- (void)hasTokenBondFailedCancel{
    __LOG_METHOD_START;
    [SVProgressHUD dismiss];
    [self.view setUserInteractionEnabled:YES];
    __LOG_METHOD_END;
}
- (void)hasTokenBondFailedRetry {
    __LOG_METHOD_START;
    [SVProgressHUD dismiss];
    //connect previous band
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"DeviceName"];
    NSString *candId = [defaults objectForKey:@"DeviceID"];
    NSLog(@"stored candId = %@",candId);
    
    lostBeforeMainOperation = YES;
    
    if ([TGBleManager sharedTGBleManager].status == TGBleConnectedToBond) {
        //已有连接,直接做主要操作
        NSLog(@"已有连接,直接做主要操作");
        if (wantSyncing) {
            [SVProgressHUD showWithStatus:@" Syncing"];
            NSLog(@"call trySyncData:NO ----");
            [[TGBleManager sharedTGBleManager] trySyncData:NO];
            if ([CommonValues getAutoSyncingFlag]) {
                NSLog(@"trySyncTimer start");
                trySyncTimer = [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(trySyncTimeOut) userInfo:nil repeats:NO];
            }
            
        }
        else if (wantRT){
            //                        [SVProgressHUD showWithStatus:@" Start real time ECG"];
            NSThread *cardioPlotThread =  [[NSThread  alloc]initWithTarget:self selector:@selector(startCardioPlot) object:nil];
            [cardioPlotThread start];
            
        }
        else{
            NSLog(@"wantSyncing == NO && wantRT == NO");
        }
        
    }
    
    else{
        NSLog(@"没有连接");
        
        NSLog(@"call candidateConnect---");
        [[TGBleManager sharedTGBleManager]candidateConnect:candId];
        timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
        NSThread *delay2ShowThread = [[NSThread alloc]initWithTarget:self selector:@selector(delay2ShowSVProgressHUD:) object:@" Connecting"];
        [delay2ShowThread start];
        
    }
    __LOG_METHOD_END;
}
//alertview clicked do functions  here end.


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    __LOG_METHOD_START;
    NSLog(@"buttonIndex:%ld", (long)buttonIndex);
    
    switch (alertView.tag) {
        case cellularTag:
            if (buttonIndex == 1) {
                [self cellularOK];
            }
            else if (buttonIndex == 0) {
                [self cellularNotShowAgain];
            }
            break;
            
            
        case checkBTStatusTag:
            [ self checkBlueToothStatusDone: YES ];
            break;
            
        case   hasTokenTimeOutAlertTag:
            
            if (buttonIndex == 0) {
                [self hasTokenTimeOutAlertRetry];
            }
            else if (buttonIndex == 2){
            }
            break;
            
            
        case   noTokenTimeOutAlertTag:
            
            if (buttonIndex == 0) {
                [self noTokenTimeOutAlertCancel];
            }
            
            else if (buttonIndex == 1)
            {
                [self noTokenTimeOutAlertRetry];
            }
            
            break;
            
        case selectDeviceAlertTag:{
            
            if (buttonIndex == 0) {
                [self selectDeviceAlertCancel];
            }
            
            else if(buttonIndex == 1){
                [self selectDeviceAlertOK];
            }
            
            [self resetDeviceListTemp];
        }
            
            break;
            
        case bondAlertTag:{
            
            if (buttonIndex == 0) {
                [self bondAlertCancel];
            }
            
            else if(buttonIndex == 1){
                [self bondAlertOK];
            }
            
        }
            
            break;
        case LostConnectAlertTag:
        {
            if (buttonIndex == 0) {
                [self LostConnectAlertCancel];
            }
            
            else if(buttonIndex == 1){
                [self LostConnectAlertOK];
            }
        }
            break;
            
            
        case syncReportAlertTag:{
            [self syncReportAlertOK];
        }
            break;
            
            
        case noTokenBondFailedTag:
        {
            if (buttonIndex == 0) {
                [self noTokenBondFailedCancel];
            }
            
            else if(buttonIndex == 1){
                [self noTokenBondFailedRetry];
            }
            else if(buttonIndex == 2){
                [self noTokenBondFailedRtyAnother];
            }
            
        }
            break;
            
        case hasTokenBondFailedTag:
        {
            if (buttonIndex == 1) {
                [self hasTokenBondFailedRetry];
            }
            
            else{
                [self hasTokenBondFailedCancel];
            }
        }
            break;
            
        default:
            break;
    }
    __LOG_METHOD_END;
}

- (IBAction)menuClick:(id)sender {
    __LOG_METHOD_START;
    [devicesArray removeAllObjects];
    [mfgDataArray removeAllObjects];
    [rssiArray removeAllObjects];
    [candIDArray removeAllObjects];
    [devicesIDArray removeAllObjects];
    //move menu when clicked
    if (myScrollView.frame.origin.x==0) {
        [UIView beginAnimations:@"AAA" context:nil];
        //  myScrollView.center=CGPointMake(360, 304);
        NSLog(@"myScrollView.frame.origin.x = 0");
        if (startSyncFlag) {
            NSLog(@"startSyncFlag = yes");
            syncView.hidden = NO;
            myScrollView.frame = CGRectMake(200, 43+41, 320, screenHeight-43-41-20);
            syncView.frame =  CGRectMake(200, 43, 320, 41);
        }
        else{
            NSLog(@"startSyncFlag = no");
            
            myScrollView.frame = CGRectMake(200, 43, 320, screenHeight-43-20);
            syncView.hidden = YES;
        }
        
        
        [UIView commitAnimations];
        //syncView.hidden=YES;
        [myScrollView setScrollEnabled:NO];
        
    }
    else{
        [UIView beginAnimations:@"AAA" context:nil];
        //  myScrollView.center=CGPointMake(160, 304);
        
        if (startSyncFlag) {
            myScrollView.frame = CGRectMake(0, 43+41, 320, screenHeight-43-41-20);
            syncView.frame =  CGRectMake(0, 43, 320, 41);
            syncView.hidden = NO;
        }
        else{
            myScrollView.frame = CGRectMake(0, 43, 320, screenHeight-43-20);
            syncView.hidden = YES;
        }
        
        [UIView commitAnimations];
        [myScrollView setScrollEnabled:YES];
    }
    __LOG_METHOD_END;
}

- (void) pushMenuItem:(id)sender
{
    __LOG_METHOD_START;
    NSLog(@"%@", sender);
    NSLog(@"Menu item  devicesArray count = %lu",(unsigned long)devicesArray.count);
    listView = [[ZSYPopoverListView alloc] initWithFrame:CGRectMake(0, 0+20, 200, 200)];
    listView.datasource = self;
    listView.delegate = self;
    [listView show];
    __LOG_METHOD_END;
}

- (IBAction)demoModeClick:(id)sender {
    __LOG_METHOD_START;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults stringForKey:@"StanlyDemoMode"] == nil  || [[defaults stringForKey:@"StanlyDemoMode"] isEqualToString:@"0"]) {
        [defaults setObject: @"1" forKey:@"StanlyDemoMode"];
        [defaults synchronize];
        NSLog(@"StanlyDemoMode set 0 ");
        [demoModeBtn setImage:[UIImage imageNamed:@"demo_mode2"] forState:UIControlStateNormal];
        [myScrollView removeFromSuperview];
        myScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 43, 320, screenHeight-43-20)];
        [myScrollView setBackgroundColor:[UIColor whiteColor]];
        myScrollView.delegate = self;
        [myWholeView addSubview:myScrollView];
        [self initUI];
        myScrollView.scrollEnabled = YES ;
        
        
        
    }
    else if ([[defaults stringForKey:@"StanlyDemoMode"] isEqualToString:@"1"]){
        [defaults setObject: @"0" forKey:@"StanlyDemoMode"];
        [defaults synchronize];
        NSLog(@"StanlyDemoMode set 0 ");
        [demoModeBtn setImage:[UIImage imageNamed:@"demo_mode"] forState:UIControlStateNormal];
        [myScrollView removeFromSuperview];
        myScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 43, 320, screenHeight-43-20)];
        [myScrollView setBackgroundColor:[UIColor whiteColor]];
        myScrollView.delegate = self;
        [myWholeView addSubview:myScrollView];
        [self initUI];
        myScrollView.scrollEnabled = YES ;
        
    }
    
    else{
        
        NSLog(@"StanlyDemoMode not regular");
    }
    __LOG_METHOD_END;
}

- (void)stopScan{
    __LOG_METHOD_START;

    // reset button state and flags.
    //    isSyncing = NO;
    [self unLockSyncBtn];
    [self.view setUserInteractionEnabled:YES];
    //    isRT = NO;
    NSLog(@"call candidateStopScan----");
    [[TGBleManager sharedTGBleManager]candidateStopScan];
    [devicesArray removeAllObjects];
    [mfgDataArray removeAllObjects];
    [rssiArray removeAllObjects];
    [candIDArray removeAllObjects];
    [devicesIDArray removeAllObjects];
    
    selectDeviceBtnClickFlag = NO;
    
    haveDeviceFlag = NO;
    // cancel delayed time out alert.
    if ([timeOutTimer isValid]) {
        [timeOutTimer invalidate];
        NSLog(@"timeOutTimer invalidate----");
        
    }
    
    firstEnterCandiDel = NO;
    __LOG_METHOD_END;
}

- (IBAction)but_sync:(id)sender {
    __LOG_METHOD_START;
    if ([TGBleManager sharedTGBleManager].status == TGBleNotConnected) {
        if ([CommonValues getAutoSyncingFlag]) {
            //is AutoSyncing, no need to update UI
            NSLog(@"is AutoSyncing,  ---Not connected.  Please connect first");
        }
        
        __LOG_METHOD_END;
        return;
        
    }
    if (startSyncFlag) {
        NSLog(@"is Syncing..");
        __LOG_METHOD_END;
        return;
        
    }
    startSyncFlag = YES;
    if (![CommonValues getAutoSyncingFlag]) {
        //is AutoSyncing, no need to update UI
        NSLog(@"but_sync----");
        
        [UIView beginAnimations:@"AAA" context:nil];
        // myScrollView.center=CGPointMake(160, 304);
        myScrollView.frame = CGRectMake(0, 43, 320, screenHeight-43-20);
        [UIView commitAnimations];
        [myScrollView setScrollEnabled:YES];
        startSyncFlag = YES;
        pushMenuItemSynclag =YES;
        if ([TGBleManager sharedTGBleManager].status == TGBleConnected ||
            [TGBleManager sharedTGBleManager].status == TGBleConnectedToBond) {
            AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
            int level = appDelegate.batteryLevel ;
            //            [self updateConnectStateImg:level];
            [CommonValues updateConnectStateImg:connectStateImg withLevel:level];
            
        }
        
        retryBtn.hidden = YES;
        syncProgress.progress = 0;
        syncProgressLabel.text = @"0%";
        syncView.hidden = NO;
        
        [UIView beginAnimations:@"MoveIn" context:nil];
        [UIView setAnimationDuration:0.5];
        // myScrollView.center = downPos;
        myScrollView.frame = CGRectMake(0, 43+41, 320, screenHeight-43-41-20);
        [UIView commitAnimations];
        [myScrollView setScrollEnabled:YES];
        syncProgress.hidden = NO;
        syncBgImg.hidden = NO ;
        syncProgressLabel.hidden = NO;
        
        NSLog(@"start SyncData (ask+sync)--");
    }
    
    NSLog(@"call askCurrentCount-----");
    [[TGBleManager sharedTGBleManager] askCurrentCount];
    syncMsgLabel.hidden = NO;
    syncMsgLabel.text = RECEIVE_CURRENTCOUNT_MSG;
    NSThread *fakeProgressMovingThread =  [[NSThread alloc]initWithTarget:self selector:@selector(realProThreadRun2) object:nil];
    [fakeProgressMovingThread start];
    
    askCurrentCountNoResponseTimer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(askCurrrentNoResponseTodo) userInfo:nil repeats:NO];
    
    [SVProgressHUD dismiss];
    __LOG_METHOD_END;
}


-(void)trySyncTimeOut{
    __LOG_METHOD_START;

    //    if (![CommonValues getHasTaskFlag]) {
    //        NSLog(@"sync failed  ,already finished");
    //        return;
    //    }
    //
    //    if ([CommonValues getAutoSyncingFlag]) {
    NSLog(@"auto sync trysync time out ,return");
    NSLog(@"setAutoSyncingFlag:NO");
    [CommonValues setAutoSyncingFlag:NO];
    NSLog(@"call tryDisconnect-----");
    [[TGBleManager sharedTGBleManager] tryDisconnect];
    #pragma mark -AutoFail_2
    syncPassFlag = NO;
     [dashboard write2SyncLogFileWithTimeStr:syncTimeStr withManualFlag:manualSyncFlag withPassFlag:syncPassFlag withDataReceived:dataReceived withTotalDataAvailable:totalDataAvailable];
    //    }
    //    else{
    //        [SVProgressHUD dismiss];
    //
    //        // delay to show alert ,prevent the confligs from SVProgressHUD dismiss
    //        [self performSelector:@selector(delay2ShowTimeOutAlert) withObject:nil afterDelay:0.5];
    //
    //    }
    __LOG_METHOD_END;
}

- (void)showTimeOutAlert{
    __LOG_METHOD_START;
#pragma mark - AutoFail_3
    syncPassFlag = NO;
     [dashboard write2SyncLogFileWithTimeStr:syncTimeStr withManualFlag:manualSyncFlag withPassFlag:syncPassFlag withDataReceived:dataReceived withTotalDataAvailable:totalDataAvailable];
    [self stopScan];
    if ([CommonValues getAutoSyncingFlag]) {
        NSLog(@"auto sync connect time out ,return");
        NSLog(@"setAutoSyncingFlag:NO");
        [CommonValues setAutoSyncingFlag:NO];
                NSLog(@"call tryDisconnect-----");
                [[TGBleManager sharedTGBleManager] tryDisconnect];
        [self endBackgroundTask];
    }
    else{
        [SVProgressHUD dismiss];
        
        // delay to show alert ,prevent the confligs from SVProgressHUD dismiss
        [self performSelector:@selector(delay2ShowTimeOutAlert) withObject:nil afterDelay:0.5];
        
    }
    __LOG_METHOD_END;
}


- (void)delay2ShowTimeOutAlert{
    __LOG_METHOD_START;
    UIAlertView *timeOutAlert;
    
    if ([[TGBleManager sharedTGBleManager]bondToken] != NULL ) {
        if ([CommonValues iOSVersion] >= IOS8) {
            alertController = [UIAlertController alertControllerWithTitle:ConnectTimeOutTitle message:ConnectTimeOutMsg preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                [self hasTokenTimeOutAlertCancel];
                
            }];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self hasTokenTimeOutAlertRetry];
            }];
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            //            [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0.1];
        }
        else{
            timeOutAlert =  [[UIAlertView alloc]initWithTitle:ConnectTimeOutTitle message:ConnectTimeOutMsg delegate:self cancelButtonTitle:nil otherButtonTitles:@"Retry", @"Cancel", nil];
            timeOutAlert.tag = hasTokenTimeOutAlertTag;
        }
        
    }
    else{
        if ([CommonValues iOSVersion] >= IOS8) {
            alertController = [UIAlertController alertControllerWithTitle:ScanTimeOutTitle message:ScanTimeOutMsg preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                [self noTokenTimeOutAlertCancel];
                
            }];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self noTokenTimeOutAlertRetry];
            }];
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            //            [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0.1];
        }
        else{
            timeOutAlert = [[UIAlertView alloc]initWithTitle:ScanTimeOutTitle message:ScanTimeOutMsg delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
            timeOutAlert.tag = noTokenTimeOutAlertTag;
        }
    }
    
    if ([CommonValues iOSVersion] >= IOS8) {
        [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0.1];
    }
    else{
        [timeOutAlert show];
    }
    NSLog(@"call tryDisconnect----");
    [[TGBleManager sharedTGBleManager] tryDisconnect];
    __LOG_METHOD_END;
}

- (IBAction)but_select:(id)sender {
    __LOG_METHOD_START;
    NSLog(@"%@  select device click", sender);
    
    if (![CommonValues getBluetoothOnFlag]) {
        NSLog(@"BT OFF, can't go on candidateScaning");
        [self showBlueToothNotOnAlert];
        return;
    }
    
    
    //  if ([[TGBleManager sharedTGBleManager]isConnected]) {
    selectDeviceBtnClickFlag = YES;
    
    if ([TGBleManager sharedTGBleManager].status == TGBleConnected ||
        [TGBleManager sharedTGBleManager].status == TGBleConnectedToBond) {
        //  NSLog(@"SDK thought  it's connected");
    }
    else{
        //  NSLog(@"SDK thought  it's NOT connected");
    }
    [SVProgressHUD showWithStatus:@" Search bands "];
    [self.view setUserInteractionEnabled:NO];
    
    NSLog(@" call  candidateScan:nameList");
    [[TGBleManager sharedTGBleManager] candidateScan:[CommonValues bandDeviceArray]];
    
    NSLog(@"Menu item  devicesArray count = %lu",(unsigned long)devicesArray.count);
    __LOG_METHOD_END;
}


/**
 step/calorie progress increasement control like animation,when sync success
 */
- (void)stepProgressIncreaseRun:(NSMutableArray *)objArr

    {
    __LOG_METHOD_START;

    float currentPercent = 0.0f;
    float previousStepPercentL = 0.0f;
    //    currentPercent = previousStepPercent;
    float want_step_percent = 0.0f;
    NSString *type ;
    
    if (objArr.count == 3)
        {
        //        previousStepPercentL  = [[objArr objectAtIndex:0] floatValue];
        currentPercent = previousStepPercentL;
        want_step_percent = [[objArr objectAtIndex:1] floatValue];
        type = [objArr objectAtIndex:2] ;
        }
    else
        {
        NSLog(@"objArr bad formatted");
        }
    
    //    want_step_percent = 1.0f;
    
    if (want_step_percent > 1)
        {
        want_step_percent = 1.0;
        }
    if (want_step_percent == 0)
        {
        currentPercent = 0;
        if ([type isEqual:AnimationTypeStep])
            {
            [ self performSelectorOnMainThread: @selector( updateStepPercent: )
                                    withObject: [ NSNumber numberWithFloat: currentPercent ]
                                 waitUntilDone: YES
            ];
            }
        else
            {
            [ self performSelectorOnMainThread: @selector( updateCaloriePercent: )
                                    withObject: [ NSNumber numberWithFloat: currentPercent ]
                                 waitUntilDone: YES
            ];
            }
        }

    if (want_step_percent > previousStepPercentL)
        {
        while (currentPercent < want_step_percent)
            {
            currentPercent += 0.01;
            if ([type isEqual:AnimationTypeStep])
                {
                [ self performSelectorOnMainThread: @selector( updateStepPercent: )
                                        withObject: [ NSNumber numberWithFloat: currentPercent ]
                                     waitUntilDone: YES
                ];
                }
            else
                {
                [ self performSelectorOnMainThread: @selector( updateCaloriePercent: )
                                        withObject: [ NSNumber numberWithFloat: currentPercent ]
                                     waitUntilDone: YES
                ];
                }
            
            sleep(1.0/30);
            }
      //  previousStepPercentL = want_step_percent;
        }
    NSLog(@"step/calorie ProgressIncreaseRun finished");
    __LOG_METHOD_END;
    }


- (void)updateStepPercent:(NSNumber *) percent{
    __LOG_METHOD_START;
    steps_circle.progress = [percent floatValue];
    __LOG_METHOD_END;
}
- (void)updateCaloriePercent:(NSNumber *) percent{
    __LOG_METHOD_START;
    calories_circle.progress = [percent floatValue];
    __LOG_METHOD_END;
}

- (void)showDevicesList{
    __LOG_METHOD_START;
    listView = [[ZSYPopoverListView alloc] initWithFrame:CGRectMake(0, 0+20, 300, 400)];
    //    dpf.tagert = self;
    //    dpf.stopEBLScan = @selector(stopScan);
    //
    listView.tagert =self;
    listView.stopEBLScan =@selector(stopScan);
    listView.datasource = self;
    listView.delegate = self;
    [listView show];
    __LOG_METHOD_END;
}

- (void)proThreadRun{
    __LOG_METHOD_START;
    sleep(1);
    while (percent<1) {
        // percent += 0.1;
        NSString *num1 = [NSString stringWithFormat:@"%f",percent];
        //  NSLog(@"proThreadRun percent = %f",percent);
        [self performSelectorOnMainThread:@selector(updateProDash:) withObject:num1 waitUntilDone:YES];
        sleep(1);
        
    }
    fakeAdd2Percent = NO;
    NSString *num1 = [NSString stringWithFormat:@"%f",1.0];
    [self performSelectorOnMainThread:@selector(updateProDash:) withObject:num1 waitUntilDone:YES];
    sleep(1);
    // dismiss sync view
    menuView.hidden = YES;
    [UIView beginAnimations:@"MoveIn" context:nil];
    [UIView setAnimationDuration:0.5];
    //myScrollView.center = oriPos;
    
    myScrollView.frame = CGRectMake(0, 43, 320, screenHeight-43-20);
    syncView.frame =  CGRectMake(0, 43, 320, 41);
    [UIView commitAnimations];
    
    NSLog(@"3");
    [self resetSrollView];
    __LOG_METHOD_END;
}

- (void)resetSrollView{
    __LOG_METHOD_START;
    NSLog(@"resetSrollView--- percent = %f",percent);
    
    isSyncing = NO;
    //  syncFinishFlag = NO;
    myScrollView.scrollEnabled = YES;
    fakeAdd2Percent = NO;
    //   startSyncFlag = NO;
    menuView.hidden = NO;
    eraseFlag = NO;
    syncView.hidden = YES;
    
    [syncProgress setImage:[UIImage imageNamed:@"sync_progress"]];
    syncProgress.verticalProgress = NO;
    syncProgress.hasGrayscaleBackground = NO;
    
    syncProgress.progress = 0.0;
    percent = 0;
    
    //    progressLabel.text = [NSString stringWithFormat:@"%d",;
    syncProgressLabel.text = [NSString stringWithFormat:@"%d%%",(int)(syncProgress.progress*100)];
    sync_ped_data_size = 0;
    sync_bytes_read = 0;
    
    syncProgress.hidden = YES;
    //    connectStateLabel.text = @"Connecting....";
    connectStateImg.image = [UIImage imageNamed:@"disconnect"];
    //    connectStateLabel.hidden = NO;
    progressLabel.hidden = YES;
    syncBgImg.hidden = YES;
    __LOG_METHOD_END;
}

- (void) initHistSync {
    __LOG_METHOD_START;
    sync_in_progress = YES;
    sync_await_header = YES;
    before_ped_size = 0;
    before_ecg_size = 0;
    before_diag_size = 0;
    before_sleep_size = 0;
    sync_ped_data_size = 0;
    sync_ped_bytes_read = 0;
    sync_ecg_data_size = 0;
    sync_ecg_bytes_read = 0;
    sync_diag_data_size = 0;
    sync_diag_bytes_read = 0;
    sync_sleep_data_size = 0;
    sync_sleep_bytes_read = 0;
    
    sync_total_bytes = 0;
    __LOG_METHOD_END;
}


- (void)startUploadS3{
    __LOG_METHOD_START;
    //tommy test Network
    if ([CommonValues connectedToNetwork]) {
        NSLog(@"have net ");
        [dashboard userProfileWriteToFile];
    }
    else
    {
        NSLog(@"NO net ,don't show the Internet disconnected alert ");
        //        UIAlertView  *noNetWork = [[UIAlertView alloc]initWithTitle:@"Internet disconnected" message:@"The data will be syncronized next time." delegate:nil cancelButtonTitle:nil otherButtonTitles:@" OK ", nil];
        //
        //        [noNetWork show];
        __LOG_METHOD_END;
        return;
    }
    NSLog(@"startUploadS3-----");
    NSLog(@"sampleRate = %d",sampleRate);
    pedThreadFinishFalg = NO;
    sleepThreadFinishFalg = NO;
    ekgThreadFinishFalg = NO;
    userproThreadFinishFalg = NO;
    fatThreadFinishFalg = NO;
    ekgRealtimeThreadFinishFalg = NO;
    
    NSThread *thread_ped_upload = [[NSThread alloc]initWithTarget:self selector:@selector(runPED) object:nil ];
    [thread_ped_upload start];
    NSThread *thread_ecg_upload = [[NSThread alloc]initWithTarget:self selector:@selector(runECG) object:nil ];
    [thread_ecg_upload start];
    
    NSThread *thread_userpro_upload = [[NSThread alloc]initWithTarget:self selector:@selector(runUserPro) object:nil ];
    [thread_userpro_upload start];
    
    NSThread *thread_sleep_upload = [[NSThread alloc]initWithTarget:self selector:@selector(runSleep) object:nil ];
    [thread_sleep_upload start];
    
    NSThread *thread_fat_upload = [[NSThread alloc]initWithTarget:self selector:@selector(runFat) object:nil ];
    [thread_fat_upload start];
    
    NSThread *thread_ecg_realtime_upload = [[NSThread alloc]initWithTarget:self selector:@selector(runECG_Realtime) object:nil ];
    [thread_ecg_realtime_upload start];
    
    NSThread *thread_synclog_upload = [[NSThread alloc]initWithTarget:self selector:@selector(runSyncLog) object:nil ];
    [thread_synclog_upload start];
    __LOG_METHOD_END;
}

- (BOOL)allUploadThreadFinishFlag{
    __LOG_METHOD_START;
    __LOG_METHOD_END;
    return ekgThreadFinishFalg
        && sleepThreadFinishFalg
        && ekgThreadFinishFalg
        && userproThreadFinishFalg
        && fatThreadFinishFalg
        && ekgRealtimeThreadFinishFalg
        && syncLogThreadFinishFalg;
}

- (void)runSyncLog{
    __LOG_METHOD_START;
    syncLogThreadFinishFalg = [dashboard runSyncLog];
    if ([self allUploadThreadFinishFlag]) {
        NSLog(@"all upload thread finished");
        
        if (![CommonValues connectedToNetwork]) {
            NSLog(@" no network ,don't delete local files");
            return;
        }
        
        [dashboard deleteAllSyncData];
    }
    __LOG_METHOD_END;
}

- (void)runECG{
    __LOG_METHOD_START;
    ekgThreadFinishFalg = [dashboard runECG];
    if ([self allUploadThreadFinishFlag]) {
        NSLog(@"all upload thread finished");
        
        if (![CommonValues connectedToNetwork]) {
            NSLog(@" no network ,don't delete local files");
            return;
        }
        
        [dashboard deleteAllSyncData];
    }
    __LOG_METHOD_END;
}
- (void)runECG_Realtime{
    __LOG_METHOD_START;
    ekgRealtimeThreadFinishFalg = [dashboard runECG_Realtime];
    
    if ([self allUploadThreadFinishFlag]) {
        NSLog(@"all upload thread finished");
        
        if (![CommonValues connectedToNetwork]) {
            NSLog(@" no network ,don't delete local files");
            __LOG_METHOD_END;
            return;
        }
        
        [dashboard deleteAllSyncData];
    }
    __LOG_METHOD_END;
}

- (void)runUserPro{
    __LOG_METHOD_START;
    userproThreadFinishFalg = [dashboard runUserPro];
    if ([self allUploadThreadFinishFlag]) {
        NSLog(@"all upload thread finished");
        if (![CommonValues connectedToNetwork]) {
            NSLog(@" no network ,don't delete local files");
            return;
        }
        
        [dashboard deleteAllSyncData];
    }
    __LOG_METHOD_END;
}

- (void)runSleep{
    __LOG_METHOD_START;
    sleepThreadFinishFalg = [dashboard runSleep];
    if ([self allUploadThreadFinishFlag]) {
        NSLog(@"all upload thread finished");
        if (![CommonValues connectedToNetwork]) {
            NSLog(@" no network ,don't delete local files");
            __LOG_METHOD_END;
            return;
        }
        
        [dashboard deleteAllSyncData];
    }
    __LOG_METHOD_END;
}

- (void)runFat{
    __LOG_METHOD_START;
    fatThreadFinishFalg = [dashboard runFat];
    if ([self allUploadThreadFinishFlag]) {
        NSLog(@"all upload thread finished");
        if (![CommonValues connectedToNetwork]) {
            NSLog(@" no network ,don't delete local files");
            __LOG_METHOD_END;
            return;
        }
        
        [dashboard deleteAllSyncData];
    }
    __LOG_METHOD_END;
}

- (void)runPED{
    __LOG_METHOD_START;
    pedThreadFinishFalg = [dashboard runPED];
    if ([self allUploadThreadFinishFlag]) {
        NSLog(@"all upload thread finished");
        if (![CommonValues connectedToNetwork]) {
            NSLog(@" no network ,don't delete local files");
            __LOG_METHOD_END;
            return;
        }
        
        [dashboard deleteAllSyncData];
    }
    __LOG_METHOD_END;
}


- (void)getDataFromDB{
    __LOG_METHOD_START;
    //    return;
    totalEndTime = 0 ;
    stepFromDb = 0 ;
    caloriesFromDb = 0;
    distanceFromDb = 0;
    //test get time arr;
    [dbTimeArray removeAllObjects];
    dbTimeArray =  [CommonValues getPedTimeArr :db];

    if (dbTimeArray.count > 0) {
        NSMutableArray * const pedArr = /* [[NSMutableArray alloc] init];
        pedArr = */ [dashboard pedTotalInOneDayWithDate:[dbTimeArray objectAtIndex:0] WithDB:db];
        stepFromDb = [[pedArr objectAtIndex:0] intValue];
         caloriesFromDb = [[pedArr objectAtIndex:1] intValue];
         distanceFromDb = [[pedArr objectAtIndex:2] intValue];
        NSMutableArray * const sleepArr = /* [[NSMutableArray alloc] init];
        sleepArr = */ [dashboard sleepArrWithDate:[dbTimeArray objectAtIndex:sleepIndex] withDB:db];
        sleepTimeArray = [sleepArr objectAtIndex:0];
        sleepPhaseArray = [sleepArr objectAtIndex:1];
    }
    
    //    NSLog(@"call sleepRequestAnalysis-----");
    //    [[TGBleManager sharedTGBleManager] sleepRequestAnalysis];
    for (int i = 0 ; i < sleepPhaseArray.count; i++) {
        if ([[sleepPhaseArray objectAtIndex:i] integerValue] == 4  || [[sleepPhaseArray objectAtIndex:i] integerValue] == 1) {
            NSString *tmpTime =  [sleepTimeArray objectAtIndex:i ];
            //time1 = tmpTime;
            [sleepIntervalTimeArr addObject:tmpTime];
            
            //  NSLog(@"tmpTime = %@",tmpTime);
            //dayTotalSleepTime +=[[ sleepPhaseArray objectAtIndex:i] integerValue];
        }
    }
    // [sleepIntervalTimeArr addObject:tmpTime];
//    NSLog(@"sleepIntervalTimeArr  = %@",sleepIntervalTimeArr);
    NSLog(@"dayTotalSleepTime = %d",dayTotalSleepTime);
    __LOG_METHOD_END;
}
- (IBAction)connectClick:(id)sender {
    __LOG_METHOD_START;
    if ([TGBleManager sharedTGBleManager].status == TGBleConnected ||
        [TGBleManager sharedTGBleManager].status == TGBleConnectedToBond) {
        NSLog(@"此处应该不会被执行！！！");
        NSString *msg = [NSString stringWithFormat:@"Already connected to %@",[[TGBleManager sharedTGBleManager]advName]];
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:msg message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@" OK ", nil];
        
        [alert show];
        __LOG_METHOD_END;
        return;
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"DeviceName"];
    NSString *candId = [defaults objectForKey:@"DeviceID"];
    NSLog(@"Connect click candId = %@",candId);
    isConnectingFlag = YES;
    NSLog(@"call candidateConnect:candId-----");
    [[TGBleManager sharedTGBleManager] candidateConnect:candId];
    timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
    
    [SVProgressHUD showWithStatus:@" Connecting"];
    __LOG_METHOD_END;
}


- (IBAction)bondClick:(id)sender {
    __LOG_METHOD_START;
    if ([TGBleManager sharedTGBleManager].status == TGBleConnected ||
        [TGBleManager sharedTGBleManager].status == TGBleConnectedToBond) {        NSLog(@"SDK say it is Connected");
        
        NSData *sdkToken =  [[TGBleManager sharedTGBleManager] bondToken];
        
        if (sdkToken == NULL) {
            NSLog(@"call tryBond-----");
            [[TGBleManager sharedTGBleManager] tryBond];
            
        }
        
        else{
            
            NSString *sn =  [[TGBleManager sharedTGBleManager] hwSerialNumber];
            NSLog(@"sdkToken = %@, sn = %@",sdkToken,sn);
            NSLog(@"call adoptBond: serialNumber ");
            [[TGBleManager sharedTGBleManager] adoptBond:sdkToken serialNumber:sn];
            
        }
        
        if ([CommonValues getAutoSyncingFlag]) {
            //is AutoSyncing, no need to update UI
        }
        else{
            dispatch_async( dispatch_get_main_queue(), ^ {
                [SVProgressHUD showWithStatus:@" Bonding"];
            });
            
            
        }
    }
    
    else{
        NSLog(@"此处应该不会被执行！！！");
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Not connected.  Please connect first." message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@" OK ", nil];
        
        dispatch_async( dispatch_get_main_queue(), ^ {
            [alert show];
        });
        NSLog(@"SDK say it is NOT Connected");
    }
    __LOG_METHOD_END;
}


- (IBAction)retryBtnClick:(id)sender {
    __LOG_METHOD_START;
    if ([CommonValues getAutoSyncingFlag]) {
        [autoSyncingAlert show];
    }
    else {
        [self syncBtnClick:nil];
    }
    __LOG_METHOD_END;
    return;
}


- (IBAction)syncBtnClick:(id)sender {
    __LOG_METHOD_START;
    if ([CommonValues getAutoSyncingFlag]) {
        [autoSyncingAlert show];
        __LOG_METHOD_END;
        return;
    }
    
    //连续同步失败3次 ，提示
    
    if ([CommonValues getConsecutiveSyncCount] >= retryMaxCount) {
        NSLog(@" sync failed more than 3 times // 连续同步失败3次以上");
        [CommonValues resetConsecutiveSyncCount];
        if ([CommonValues iOSVersion] >= IOS8) {
            alertController = [UIAlertController alertControllerWithTitle:ResetBluetoothTitle message:ResetBluetoothMsg preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:okAction];
            [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
        }
        else{
            UIAlertView *consecutiveSyncCountAlert = [[UIAlertView alloc]initWithTitle:ResetBluetoothTitle message:ResetBluetoothMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [consecutiveSyncCountAlert show];
        }
        
        __LOG_METHOD_END;
        return;
    }
    
    
    NSLog(@"syncBtnClick----");
    
    if (![CommonValues getBluetoothOnFlag]) {
        NSLog(@"BT OFF, can't go on syncing");
        [self showBlueToothNotOnAlert];
        __LOG_METHOD_END;
        return;
    }
    
    NSLog(@" syncBtnClick ,set HasTaskFlag YES ");
    [CommonValues setHasTaskFlag:YES];
    isSyncing = YES;
    wantSyncing = YES;
    wantRT = NO;
    lostBeforeMainOperation = YES;
    NSLog(@"lostBeforeMainOperation FLag set YES");
    [self lockSyncBtn];
    syncTimeStr = [[CommonValues timeDateFormatter:DateFormatterTimeColon] stringFromDate:[NSDate date]];
    manualSyncFlag = YES;
    //假如已有连接，则直接执行主要操作
    if ( [TGBleManager sharedTGBleManager].status == TGBleConnectedToBond) {
        NSLog(@"已有连接");
        [self.view setUserInteractionEnabled:NO];
        [self but_sync:nil];
    }
    
    else{
        NSLog(@"没有连接");
        NSData *token = [[TGBleManager sharedTGBleManager]bondToken];
        if (token != NULL) {
            NSLog(@"bond_state");
            [SVProgressHUD showWithStatus:@" Syncing"];
            [self.view setUserInteractionEnabled:NO];
            // connect
            [self connectClick:nil];
            
        }
        
        else {
            NSLog(@"not_bond_state");
            [self but_select:nil];
            timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
            
        }
        
        
        
    }
    __LOG_METHOD_END;
}

- (IBAction)settingBtnClick:(id)sender {
    __LOG_METHOD_START;
    SettingsViewController *svc = [[SettingsViewController alloc]init];
    
    [self presentViewController:svc animated:YES completion:nil];
    __LOG_METHOD_END;
}

- (IBAction)aboutBtnClick:(id)sender {
    __LOG_METHOD_START;
    AboutViewController *avc = [[AboutViewController alloc]init];
    [self presentViewController:avc animated:YES completion:nil];
    __LOG_METHOD_END;
}

- (IBAction)helpBtnClick:(id)sender {
    __LOG_METHOD_START;
    HelpViewController *hvc = [[HelpViewController alloc] init];
    [self presentViewController:hvc animated:YES completion:nil];
    __LOG_METHOD_END;
}

- (IBAction)logsBtnClick:(id)sender {
    __LOG_METHOD_START;

    LogsViewController *lvc = [[LogsViewController alloc] init];
    [self presentViewController:lvc animated:YES completion:nil];
    
    __LOG_METHOD_END;
}

#pragma mark - TestBtnClcik
- (IBAction)testBtnClick:(id)sender {
    __LOG_METHOD_START;
//    [AWSTransferManager createAWSStaticCredentialsProvider];
//    [AWSTransferManager upload:@"Tommy.zip"];
    __LOG_METHOD_END;
}


- (void)potentialBond:(NSString*) code sn: (NSString*) sn devName:(NSString*) devName
{
    __LOG_METHOD_START;
    NSString* displayMessage = [NSString stringWithFormat:
                                @"%@%@?",
                                DigitCodeConfirmMsg,code];
    dispatch_async( dispatch_get_main_queue(), ^ {
        [SVProgressHUD dismiss];
        
        if ([CommonValues iOSVersion] > IOS8) {
            alertController = [UIAlertController alertControllerWithTitle:@"" message:displayMessage preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                [self bondAlertCancel];
                
            }];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self bondAlertOK];
            }];
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
        }
        else{
            
            
            UIAlertView *bondDialog = [[UIAlertView alloc]initWithTitle:@"" message:displayMessage delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
            bondDialog.tag = bondAlertTag;
            [bondDialog show];
            
        }
    });
    __LOG_METHOD_END;
}

- (void) bleDidBond:(TGbondResult)result {
    __LOG_METHOD_START;
    [self delayTodo];
    // set bond flag,check it when do every action
    if (result == TGbondResultTokenAccepted) {
        successBondedFlag  = YES;
    }
    else{
        successBondedFlag  = NO;
        
        NSLog(@"call SDK tryDisconnect---");
        [[TGBleManager sharedTGBleManager]tryDisconnect];
    }
    
    
    NSString * message;
    NSString * title ;
    
    if (result == TGbondResultTokenAccepted) {
        message = @"Bond Succesful";
        
        NSLog(@"Bond Succesful,start to sync/RT");
        if (wantSyncing) {
            NSLog(@"isSyncing = ture");
            isSyncing = NO;
            if ([CommonValues getAutoSyncingFlag]) {
                //is AutoSyncing, no need to update UI
                [self but_sync:nil];
            }
            else{
                dispatch_async( dispatch_get_main_queue(), ^ {
                    [SVProgressHUD showWithStatus:@" Syncing"];
                    [self but_sync:nil];
                });
                
            }
        }
        
        else if (wantRT) {
            NSLog(@"isRT = ture");
            
            if (![CommonValues getBluetoothOnFlag]) {
                
                
                NSLog(@"BT OFF, can't go on realtime RT---------------XXXXX");
                if ([CommonValues getAutoSyncingFlag]) {
                    //is AutoSyncing, no need to update UI
                }
                else{
                    [self showBlueToothNotOnAlert];
                    
                }
                __LOG_METHOD_END;
                return;
            }
            
            
            //            dispatch_async( dispatch_get_main_queue(), ^ {
            //                [SVProgressHUD showWithStatus:@" Start real time ECG"];
            //            });
            NSLog(@"start cardioplot--");
            lostBeforeMainOperation = YES;
            batteryFlag = YES;
            NSThread *cardioPlotThread =  [[NSThread  alloc]initWithTarget:self selector:@selector(startCardioPlot) object:nil];
            [cardioPlotThread start];
            
            
        }
        else{
            NSLog(@"wantSyncing = no && wantRT = no");
        }
        
        
    }
    else if (result == TGbondResultTokenReleased) {
        title = TokenReleasedTitle;
        message = TokenReleasedMsg;
    }
    else if (result == TGbondResultErrorBondedNoMatch) {
        title = CommunicationErrorTitle;
        message = BondedNoMatchMsg;
    }
    else if (result == TGbondResultErrorBadTokenFormat) {
        title = BondRejectedTitle;
        message = BadTokenFormatMsg;
    }
    else if (result == TGbondResultErrorTimeOut) {
        title = TimeOutTitle;
        message = TimeOutMsg;
    }
    
    else if (result == TGbondResultErrorNoConnection) {
        title = NoConnectionTitle;
        message = NoConnectionMsg;
    }
    
    else if (result == TGbondResultErrorReadTimeOut) {
        title = ReadBackTimeOutTitle;
        message = ReadTimeOutMsg;
    }
    
    
    else if (result == TGbondResultErrorReadBackTimeOut) {
        title = ReadBackTimeOutTitle;
        message = ReadBackTimeOutMsg;
    }
    
    else if (result == TGbondResultErrorWriteFail) {
        title = WriteFailTitle;
        message = WriteFailMsg;
    }
    
    
    else if (result == TGbondResultErrorTargetIsAlreadyBonded) {
        title = BondRejectedTitle;
        message = TargetIsAlreadyBondedMsg;
    }
    
    
    else if (result == TGbondAppErrorNoPotentialBondDelegate) {
        title  = BondAppErrorTitle;
        message = NoPotentialBondDelegateMsg;
    }
    
    
    else if (result == TGbondResultErrorTargetHasWrongSN) {
        title  = CommunicationErrorTitle;
        message = TargetHasWrongSNMSg;
    }
    
    else if (result == TGbondResultErrorPairingRejected) {
        title  = PairingErrorTitle;
        message = PairingRejectedMsg;
    }
    
    
    else if (result == TGbondResultErrorSecurityMismatch) {
        title  = PairingErrorTitle;
        message = SecurityMismatchMsg;
    }
    
    
    
    else {
        title  = UnkonwnBondErrorTitle;
        message = [NSString stringWithFormat:@"Bonding Error, code: %d", (int) result];
    }
    NSLog(@"BONDIING RESULT Code  %lu",(unsigned long)result);
    NSLog(@"BONDIING RESULT msg  %@",message);
    //    NSLog(@"call askCurrentCount -----");
    //    [[TGBleManager sharedTGBleManager]askCurrentCount];
    if (result == TGbondResultTokenReleased) {
        NSLog(@"此处应该不执行！！！");
    }
    
    else if (result != TGbondResultTokenAccepted ) {
        if ([CommonValues getAutoSyncingFlag]) {
            //is AutoSyncing, no need to update UI
        }
        else{
            dispatch_async( dispatch_get_main_queue(), ^ {
                [SVProgressHUD dismiss];
                
            });
        }
        
        lostBeforeMainOperation = NO;
        
        NSData *token = [[TGBleManager sharedTGBleManager] bondToken];
        if (token == NULL) {
            
            if ([CommonValues iOSVersion] >= IOS8) {
                alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                    [self noTokenBondFailedCancel];
                    
                }];
                UIAlertAction *retryAction = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self noTokenBondFailedRetry];
                }];
                UIAlertAction *tryAnotherAction = [UIAlertAction actionWithTitle:@"Try another band" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self noTokenBondFailedRtyAnother];
                }];
                [alertController addAction:cancelAction];
                [alertController addAction:retryAction];
                [alertController addAction:tryAnotherAction];
                [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
            }
            else{
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry",@"Try another band", nil];
                alert.tag = noTokenBondFailedTag;
                dispatch_async( dispatch_get_main_queue(), ^ {
                    [alert show];
                });
            }
        }
        
        else{
            if ([CommonValues iOSVersion] >= IOS8) {
                alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                    [self hasTokenBondFailedCancel];
                    
                }];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self hasTokenBondFailedRetry];
                }];
                [alertController addAction:cancelAction];
                [alertController addAction:okAction];
                [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
            }
            else{
                UIAlertView *alert_hasToken = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
                
                alert_hasToken.tag = hasTokenBondFailedTag;
                if ([CommonValues getAutoSyncingFlag]) {
                    //is AutoSyncing, no need to update UI
                }
                else{
                    dispatch_async( dispatch_get_main_queue(), ^ {
                        [alert_hasToken show];
                    });
                }
            }
            
        }
        
        
    }
    else if(result == TGbondResultTokenAccepted){
        dispatch_async( dispatch_get_main_queue(), ^ {
            
            //            [disconnectedAlert dismissWithClickedButtonIndex:0 animated:NO];
            //            [connectedAlert show];
        });
    }
    
    else{
        
        
        
    }
    
    if ([CommonValues getAutoSyncingFlag]) {
        //is AutoSyncing, no need to update UI
    }
    else{
        //enabled sync btn;
        dispatch_async( dispatch_get_main_queue(), ^ {
            [self unLockSyncBtn];
        });
    }
    __LOG_METHOD_END;
}


- (void) transferInProgress:(bool)transfer  {
    __LOG_METHOD_START;
    if (transfer) {
        
        NSLog(@"History Sync started");
        syncMsgLabel.text = SYNC_DATA_MSG;
        char *errorMsg;
        sqlite3_exec(db, "BEGIN", NULL, NULL, &errorMsg);
        [self unLockSyncBtn];
        lostBeforeMainOperation = NO;
        latestPercent = 0;
        startSyncFlag = YES;
        pushMenuItemSynclag =YES;
        
        if ([CommonValues getAutoSyncingFlag]) {
            
        }
        
        else{
            //不是自动同步的情况下 show动画刷新UI
            //               dispatch_async( dispatch_get_main_queue(), ^ {
            //            [self.view setUserInteractionEnabled:NO];
            //            [SVProgressHUD dismiss];
            //            syncProgressLabel.text = @"0%";
            //            syncProgress.progress = 0.0f;
            //            syncView.hidden = NO;
            //            retryBtn.hidden = YES;
            //               });
            //            [self performSelectorOnMainThread:@selector(resetSyncProView) withObject:nil waitUntilDone:YES];
            
            dispatch_async( dispatch_get_main_queue(), ^ {
                
                [UIView beginAnimations:@"MoveIn" context:nil];
                [UIView setAnimationDuration:0.5];
                myScrollView.frame = CGRectMake(0, 43+41, 320, screenHeight-43-41-20);
                
                [UIView commitAnimations];
                
            });
            
        }
    }
    else {
        NSLog(@"History Sync STOPPED");
        //         sqlite3_exec( db, "COMMIT", 0, 0, 0);
        char *errorMsg;
        sqlite3_exec(db, "COMMIT", NULL, NULL, &errorMsg);
        //        [self finishPedWriteFile];
        [FileOperation write2FileAppendLastTimeContent:ped2Data filePath:[dashboard pedFilePath]];
        ped2Data = nil;
        NSLog(@"ped2Data = %@",ped2Data);
        //        [self finishSleepWriteFile];
        NSLog(@"sleep2Data = %@",sleep2Data);
        [FileOperation write2FileAppendLastTimeContent:sleep2Data filePath:[dashboard sleepFilePath]];
        sleep2Data = nil;
        //        [self finishFatWriter];
        startSyncFlag = NO;
        issyncing = NO;
        wantSyncing = NO;
        //get user profile
        if ([[TGBleManager sharedTGBleManager] isFemale]) {NSLog(@"it's a girl");}
        NSLog(@"year = %ld",(long)[[TGBleManager sharedTGBleManager]birthYear]);
        NSLog(@"step = %ld",(long)[[TGBleManager sharedTGBleManager]goalSteps]);
        NSLog(@"walk_lenth = %ld",(long)[[TGBleManager sharedTGBleManager]walkingStepLength]);
        NSLog(@"runningStepLength = %ld",(long)[[TGBleManager sharedTGBleManager]runningStepLength]);
        [self delayTodo];
        NSLog(@"sync finished---");
        
        hasPedoFlag = NO;
        hasEkgFlag = NO;
        hasSleepFlag = NO;
        //test
        syncFinishFlag = YES;
        
        latestPercent = 1;
        syncFakeThreadFlag = NO;
        
        
        
    }
    __LOG_METHOD_END;
}

- (void) transferReport:(TGsyncResult)result ped_avail:(int)ped_available ped_recv:(int)ped_received ekg_avail:(int)ekg_available ekg_recv:(int)ekg_received diag_avail:(int)diag_available diag_recv:(int)diag_received sleep_avail:(int)sleep_available sleep_recv:(int)sleep_received {
    __LOG_METHOD_START;
    NSLog(@">>>>>>>-----History Sync ENDED: result code: %lu", (unsigned long)result);
    NSLog(@">>>>>>>-----History Sync   ped avail: %d, recv: %d", ped_available, ped_received);
    NSLog(@">>>>>>>-----History Sync   ekg avail: %d, recv: %d", ekg_available, ekg_received);
    NSLog(@">>>>>>>-----History Sync  diag avail: %d, recv: %d", diag_available, diag_received);
    NSLog(@">>>>>>>-----History Sync sleep avail: %d, recv: %d", sleep_available, sleep_received);
    dataReceived = ped_received + ekg_received + diag_received + sleep_received;
    totalDataAvailable = ped_available + ekg_available + diag_available + sleep_available;
    
    lostBeforeMainOperation = NO;
    
//    NSString * message2;
//    
//    if (result == TGsyncResultErrorTimeOut) {
//        message2 = @"TIME OUT ERROR";
//    }
//    else if (result == TGsyncResultErrorFirmwareNotCompatible) {
//        message2 = @"OLD 4 CHARACTERISTIC FW, NOT SUPPORTED";
//    }
//    else if (result == TGsyncResultNormal) {
//        message2 = @"Successful Sync";
//        
//        
//    }
//    else if (result == TGsyncResultErrorBlankData) {
//        message2 = @"MANY ZEROs Received";
//    }
//    
//    else if (result == TGsyncReusltEroorDataLost) {
//        message2 = @"Data Lost)";
//    }
//    else if (result == TGsyncResultErrorFlashCorrupt) {
//        message2 = @"Flash Corrupt)";
//    }
//    else if (result == TGsyncResultError) {
//        message2 = @"No Flash Header Received";
//    }
//    else if (result == TGsyncResultDisconnect) {
//        message2 = @"Disconnect";
//    }
//    else {
//        message2 = [NSString stringWithFormat:@"Sync Error, code: %d", (int) result];
//    }

//    NSString * message;
//    
//    message = [NSString stringWithFormat:
//               @"%@\n\nPed Avail: %d         Recv: %d\nEKG Avail: %d         Recv: %d\nDiag Avail: %d         Recv: %d\nSleep Avail: %d         Recv: %d",
//               message2,
//               ped_available, ped_received,
//               ekg_available, ekg_received,
//               diag_available, diag_received,
//               sleep_available, sleep_received
//               ];

    
    //    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"SYNC Progress" message:message delegate:self cancelButtonTitle:nil otherButtonTitles:@" OK ", nil];
    // if sync successful ,upload to S3
    if (result == TGsyncResultNormal || result == TGsyncResultErrorFlashCorrupt) {
        syncPassFlag = YES;
        [CommonValues resetConsecutiveSyncCount];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *currentTime = [StringOperation nowTimeStringForLastSyncDisplay];
        NSLog(@"currentTime  ==== %@",currentTime);
        [FileOperation save2UserDefaultsWithObj:currentTime withKey:@"last_sync_time"];
        label1.text = [NSString stringWithFormat:@"Last sync %@",currentTime];
        totalEndTime = 0;
        [self getDataFromDB];
        //get sleep data;
        [self calculateSleepTime];
        //        alert.tag = syncReportAlertTag;
        NSString *stored_name = [defaults objectForKey:USER_NAME];
        NSLog(@"stored_name = %@ ",stored_name);
        if ((stored_name!= NULL ) && (![stored_name isEqualToString:@""])) {
            // upload to S3
            [self performSelector:@selector(startUploadS3) withObject:(nil) afterDelay:3];
        }
        else{
            NSLog(@"user name is null ,don't upload to S3 ,delete local files");
            [dashboard deleteAllSyncData];
        }
        
        
        //sync success, erase data
        
        if ([CommonValues getShouldEraseData]) {
            
            NSLog(@"sync finish normally ,call tryEraseData ----");
            
            [[TGBleManager sharedTGBleManager]tryEraseData];
            wantErase = YES;
            
            syncMsgLabel.text = ERASE_DATA_MSG;
            // add eraseNotResponse  Timer;
            eraseNotResponseTimer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(showBigRetryBtnCase) userInfo:nil repeats:NO];
            
            
        }
        else
        {
            NSLog(@"sync finish normally ,");
            NSLog(@"setAutoSyncingFlag:NO");
            [CommonValues setAutoSyncingFlag:NO];
            
            [CommonValues setHasTaskFlag:NO];
            //don't erase data after sync  for wenfeng
            if ([CommonValues isBackgroundFlag]) {
                NSLog(@"sync 完成，且在后台，应断开连接");
                NSLog(@"call tryDisconnect ------");
                [[TGBleManager sharedTGBleManager] tryDisconnect];
            }
            
            dispatch_async( dispatch_get_main_queue(), ^ {
                
                //                connectStateLabel.hidden = YES;
                syncProgressLabel.hidden = YES;
                syncProgress.hidden = YES;
                syncBgImg.hidden = YES;
                successful_updated_label.hidden = YES;
                //erase successfully ,start animation
                [UIView beginAnimations:@"MoveIn" context:nil];
                [UIView setAnimationDuration:1];
                myScrollView.frame = CGRectMake(0, 43, 320, screenHeight-43-20);
                syncView.frame =  CGRectMake(0, 43, 320, 41);
                [UIView commitAnimations];
            });
            [self performSelectorOnMainThread:@selector(delay2refreshUI) withObject:nil waitUntilDone:YES];
        }
        
        //not sure if should add these code
        wantSyncing  =  NO;
        isSyncing = NO;
        
        
    }
    else{
        syncPassFlag = NO;
        if (![CommonValues getAutoSyncingFlag]) {
            [CommonValues addConsecutiveSyncCount];
            NSLog(@"sync finish unnormally ,no need to erase,just disconnect and show retry button...");
            [self showBigRetryBtnCase];
        }
        else
        {
            NSLog(@"auto sync sync failed");
            NSLog(@"setAutoSyncingFlag:NO");
            [CommonValues setAutoSyncingFlag:NO];
        }
        [CommonValues setHasTaskFlag:NO];
    }
    
    //    if (![CommonValues getShouldEraseData]){
    //add this line code to fit  don't erase data after sync  for wenfeng
    //    }
#pragma mark -AutoFail_1
    //write to synclog file,no matter it is sync success or failed.
    if (trySyncTimer.valid) {
        [trySyncTimer invalidate];
    }
    [dashboard write2SyncLogFileWithTimeStr:syncTimeStr withManualFlag:manualSyncFlag withPassFlag:syncPassFlag withDataReceived:dataReceived withTotalDataAvailable:totalDataAvailable];
    [self resetSyncLogValues];
    __LOG_METHOD_END;
}

- (void)resetSyncLogValues{
    __LOG_METHOD_START;
    manualSyncFlag = NO;
    syncPassFlag = NO;
    dataReceived = 0;
    totalDataAvailable = 0;
    __LOG_METHOD_END;
}

- (void)updateSyncProgressBar :(NSString *) floatNumStr{
    __LOG_METHOD_START;
    float num = [floatNumStr floatValue];
    syncProgress.progress = num;
    int percent = 100* num;
    syncProgressLabel.text = [NSString stringWithFormat:@"%d%%",percent];
    __LOG_METHOD_END;
}

- (void)askCurrrentNoResponseTodo{
    __LOG_METHOD_START;
    syncPassFlag = NO;
     [dashboard write2SyncLogFileWithTimeStr:syncTimeStr withManualFlag:manualSyncFlag withPassFlag:syncPassFlag withDataReceived:dataReceived withTotalDataAvailable:totalDataAvailable];
    syncMsgLabel.text = SYNC_FAILED_MSG;
    NSLog(@"askCurrrentNoResponseTodo-----");
    startSyncFlag = NO;
    [self showBigRetryBtnCase];
    NSLog(@"call tryDisconnect----");
    [[TGBleManager sharedTGBleManager] tryDisconnect];
    if ([CommonValues getAutoSyncingFlag]) {
        [self endBackgroundTask];
    }
    __LOG_METHOD_END;
}


- (void)showBigRetryBtnCase{
    __LOG_METHOD_START;
    syncFakeThreadFlag = NO;
    latestPercent = 1;
    
    NSLog(@"showBigRetryBtnCase------");
    if ([CommonValues getAutoSyncingFlag]) {
        NSLog(@"auto sync erase NOT responce");
        NSLog(@"setAutoSyncingFlag:NO");
        [CommonValues setAutoSyncingFlag:NO];
        
    }
    else{
        NSLog(@"SYNC_FAILED_MSG---");
        dispatch_async(dispatch_get_main_queue(), ^{
            
            syncMsgLabel.text = SYNC_FAILED_MSG;
            syncProgress.progress = 0.0f;
            syncProgressLabel.text = @"0%";
        });
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //test  retry
            syncProgressLabel.hidden = YES;
            //            connectStateLabel.hidden = YES;
            syncProgress.hidden = YES;
            syncBgImg.hidden = YES;
            retryBtn.hidden = NO;
            [self.view setUserInteractionEnabled:YES];
        });
        
    }
    __LOG_METHOD_END;
}

- (void)delay2refreshUI{
    __LOG_METHOD_START;

    /*
     [myScrollView removeFromSuperview];
     myScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 43+20, 320, screenHeight-43-20)];
     [myScrollView setBackgroundColor:[UIColor whiteColor]];
     myScrollView.delegate = self;
     [self.view addSubview:myScrollView];
     
     [self initUI];
     */
    [self autoSyncRefreshUI];
    [self.view setUserInteractionEnabled:YES];
    [self unLockSyncBtn];
    syncProgress.hidden = NO;
    syncBgImg.hidden = NO ;
    syncProgressLabel.hidden = NO;
    syncProgressLabel.text = @"0%";
    syncProgress.progress = 0.0;
    __LOG_METHOD_END;
}



- (void)transferPercent:(int)percent {
    __LOG_METHOD_START;
    float const fpercent = (float) percent / 100.0;
    
    if ([CommonValues getAutoSyncingFlag]) {
        //AutoSync ,no need to refresh UI
        if (percent != rememberedPercent) {
            
            latestPercent = fpercent;
            rememberedPercent = percent;
            NSLog(@">>>>>>>-----History Sync: percent complete: %d -- %f", percent, fpercent);
            if (percent != 0) {
                if ([trySyncTimer isValid]) {
                    NSLog(@"trySyncTimer invalidate");
                    [trySyncTimer invalidate];
                }
            }
            
            
        }
    }
    
    else{
        if (percent != rememberedPercent) {
            
            latestPercent = fpercent;
            rememberedPercent = percent;
            NSLog(@">>>>>>>-----History Sync: percent complete: %d -- %f", percent, fpercent);
            
            if (percent != 0) {
                syncFakeThreadFlag = NO;
                //                if ([trySyncTimer isValid]) {
                //                    NSLog(@"trySyncTimer invalidate");
                //                    [trySyncTimer invalidate];
                //                }
                
            }
            
        }
        
    }
    __LOG_METHOD_END;
}


- (void)realProThreadRun2{
    __LOG_METHOD_START;
    latestPercent = 0;
    float currentProgressBarValue = 0;
    float secondCounter = 0;
    float fakePercent = 0;
    float currentPercentValue = 0;
    
    while (currentProgressBarValue<1) {
        usleep(1000000/30);
        
        if(secondCounter < 30) secondCounter++;
        else{
            if(latestPercent == 0) fakePercent = fakePercent + 0.01;
            secondCounter = 0;
        }
        
        if (latestPercent != 0) {
            currentPercentValue = fakePercent + (latestPercent * (1-fakePercent));
        }
        else{
            currentPercentValue = fakePercent;
        }
        if (currentPercentValue > currentProgressBarValue) {
            currentProgressBarValue = currentProgressBarValue + 0.01;
            //            NSLog(@"fakePercent = %f",fakePercent);
            //            NSLog(@"currentProgressBarValue = %f,",currentProgressBarValue);
            NSString *progressBarValueStr = [NSString stringWithFormat:@"%f",  currentProgressBarValue];
            [self performSelectorOnMainThread:@selector(updateSyncProgressBar:) withObject:progressBarValueStr waitUntilDone:YES];
        }
    }
    __LOG_METHOD_END;
}

- (void) batteryLevel:(int)level {
    __LOG_METHOD_START;
    NSLog(@">>>>>>>-----New Battery Level: percent complete: %d", level);
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    appDelegate.batteryLevel = level;
    [FileOperation save2UserDefaultsWithInteger:level withKey:LATEST_BATTERY];
    if ([CommonValues getAutoSyncingFlag]) {
        
    }
    else{
        //save battery level whenever level changed
        [CommonValues updateConnectStateImg:connectStateImg withLevel:level];
    }
    __LOG_METHOD_END;
}


- (void)ekgStarting:(NSDate *)time sampleRate:(int) sr realTime:(bool) rt comment:(NSString*) comment{
    __LOG_METHOD_START;
    [[TGBleManager sharedTGBleManager] stopLogging];
    
    ecgSyncDate = time;
    sampleRate = sr;
    hasEkgFlag = YES;
    ekgLongStr = [[NSMutableString alloc] init];
    
    NSLog(@">>>>>>>-----START EKG RECORDING: %d Hz ,comment = %@", sr,comment);
    // sampleCount = 0;
    NSString *currentDateStr = [[CommonValues timeDateFormatter:DateFormatterTimeAllUnderLineGMT] stringFromDate:time];
    NSLog(@"%@",currentDateStr);
    ecgFileName = [currentDateStr stringByAppendingString:@".txt"];
    path = [[CommonValues getDocumentPath] stringByAppendingPathComponent:@"EKGData"];
    NSLog(@"sample path = %@",path);
    BOOL bo = [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    NSAssert(bo,@"no directory 创建目录失败");
    path = [path stringByAppendingPathComponent:ecgFileName];
    NSLog(@"sample path 2 = %@",path);
    
    fileManager = [[NSFileManager alloc] init];
    [fileManager createFileAtPath:path contents:nil attributes:nil];
    NSLog(@"File %@ Initialized!", path);
    fwrite = [NSFileHandle fileHandleForWritingAtPath:path];
    ecgHistFileOpen = YES;
    
    //record time stamp
    NSString* tempTS = [NSString stringWithFormat:@"%@\n",ecgFileName];
    // [fwrite writeData:[tempTS dataUsingEncoding:NSUTF8StringEncoding]];
    [ekgLongStr appendString:tempTS] ;
    [ekgLongStr appendString:comment];
    __LOG_METHOD_END;
};


- (void)ekgStop:(TGekgResult) result
        finalHR:(int)finalHR{
    __LOG_METHOD_START;
    [[TGBleManager sharedTGBleManager] enableLogging];
    NSLog(@">>>>>>>-----STOP EKG RECORDING  finalHR = %d",finalHR);
    sampleCount = 0;
    int final_hrv =  [[TGBleManager sharedTGBleManager] computeHRVNow];
    NSLog(@"final_hrv = %d",final_hrv);
    
    //同步过来的HR ，HRV  保存到数据库
    
    if (finalHR == 0) {
        //realtime ECG case
    }
    else{
        //syncing ECG case
        //平均HRV
        int  avgHrv = [HeartData avgHrvWithHrvArr:hrvArr];
        //平均HR
        int  avgHr = [HeartData avgHrWithHrArr:hrArr];
        NSLog(@"ecgSyncDate = %@,final hr = %d , avg_hr = %d ,avg_hrv = %d",ecgSyncDate,finalHR,avgHr,avgHrv);
        //ekg stop ,add avg hr , final_hrv, avg_hr
        [ekgLongStr appendFormat:@"HR : %d\n",avgHr];
        [CommonValues insertHeartData2DB : ecgSyncDate
                                 finalHR : finalHR
                                   avgHR : avgHr
                                  avgHRV : avgHrv
                                finalHRV : final_hrv
                                  stress : stress
                                 database: db
        ];
        [hrArr removeAllObjects];
        [hrvArr removeAllObjects];
    }
    [ekgLongStr appendFormat:@"HRV : %d\n",final_hrv];
    [FileOperation saveEcg2DocumentWithFilePath:path withFileContent:ekgLongStr];
    NSLog(@"save hist ekg file finished");
    ekgLongStr = nil;
    // sampleCount = 0;
    __LOG_METHOD_END;
}
- (void)ekgSample:(int)sample{
    __LOG_METHOD_START;
    // [ekgLineView addValue:sample];
    sampleCount++;
    if (sampleCount%512 == 0) {
        NSLog(@">>>>>>>-----received EKG samples %d", sampleCount);
        
    }
    // [tglibEKG requestEkgAnalysis_s: sample];
    [ekgLongStr appendString:[NSString stringWithFormat:@"RAW : %d\n",sample]] ;
    __LOG_METHOD_END;
}


- (void)pedometryRecord:(NSDate*) time
                  steps:(int) steps
               calories:(int) calories
               distance:(int) distance
         activeCalories:(int) activeCalories
                stepBPM:(int) stepBPM
                 energy:(int) energy
                   mode:(TGpedometryActivityType) mode
             sleepPhase:(TGsleepPhaseType) sleepPhase {
    __LOG_METHOD_START;
    NSLog(@">>>>>>>-----pedometry record: Time:%@ Steps:%d Calories:%d Distance:%d ActCalories:%d BPM:%d Energy:%d Mode:%lu SleepPhase:%lu", time, steps, calories, distance, activeCalories, stepBPM, energy, (unsigned long)mode, (unsigned long)sleepPhase);
    
    hasPedoFlag = YES;
    NSString *currentDateStr = [[CommonValues timeDateFormatter:DateFormatterTimeAllUnderLineGMT] stringFromDate:time];
    NSString * currentDateStr4db = [[CommonValues timeDateFormatter:DateFormatterTimeColonGMT] stringFromDate:time];
    //build ped long string;
    
    ped2Data = [ped2Data stringByAppendingString:currentDateStr];
    ped2Data = [ped2Data stringByAppendingString:@":\t"];
    ped2Data = [ped2Data stringByAppendingString:[NSString stringWithFormat:@"%d\t%d\t%d\t%d\t%d\t%d\t%lu\t%lu\t",steps,calories, distance, activeCalories, stepBPM, energy, (unsigned long)mode, (unsigned long)sleepPhase]];//step
    
    ped2Data = [ped2Data stringByAppendingString:@"\n"];
    //NSLog(@"Recorded Ped data: %@", ped2Data);
    
    //insert to db
    NSString *sql1 = [NSString stringWithFormat:
                      @"INSERT INTO '%@' ('%@', '%@','%@', '%@', '%@') VALUES ('%@', '%d','%d', '%d','%lu')",
                      TABLENAME,  TIME ,STEP  , CALORIES, DISTANCE,MODE, currentDateStr4db, steps,calories,distance,(unsigned long)mode];
    //    NSLog(@"sql  insert ped   = %@",sql1);
    
    //    [self execSql:sql1];
    [CommonValues execSql:sql1 :db];
    __LOG_METHOD_END;
}


- (void)sleepRecord:(NSDate *)time phase:(TGsleepPhaseType) phase initCode:(TGsleepInitReason) code{
    __LOG_METHOD_START;
    //here also can't get recorded sleep data
    
    NSLog(@">>>>>>>-----sleep record:  %@ phase: %lu   initCode: %lu", time, (unsigned long)phase, (unsigned long)code);
    
    hasSleepFlag = YES;
    NSString *currentDateStr = [[CommonValues timeDateFormatter:DateFormatterTimeAllUnderLineGMT] stringFromDate:time];
    NSLog(@"%@",currentDateStr);
    
    NSString *currentDateStr4db = [[CommonValues timeDateFormatter:DateFormatterTimeColonGMT]  stringFromDate:time];
    NSLog(@"%@",currentDateStr4db);
    
    //build long sleep dataa
    
    sleep2Data = [sleep2Data stringByAppendingString:currentDateStr];
    sleep2Data = [sleep2Data stringByAppendingString:@":\t"];
    sleep2Data = [sleep2Data stringByAppendingString:[NSString stringWithFormat:@"%lu\t", (unsigned long)phase]];
    sleep2Data = [sleep2Data stringByAppendingString:[NSString stringWithFormat:@"%lu\n", (unsigned long)code]];
    //insert sleep data   to db
    [dashboard insertSleepData2SqilteWithTime:currentDateStr4db withPhase:phase withInitCode:code database:db];
    __LOG_METHOD_END;
};


- (void)fatBurnRecord:(NSDate*) time
            heartRate:(int) heartRate
              fatBurn:(int) fatBurn
         trainingZone:(int) trainingZone{
    
    __LOG_METHOD_START;
    NSLog(@">>>>>>>----- BURN record: %@ hr: %d  burn: %d, trainingZone: %d", time, heartRate, fatBurn, trainingZone);
    
//    NSString * message;
//    
//    message = [NSString stringWithFormat:@"HR: %d\nBurn: %d\ntrainingZone: %d", (int) heartRate, fatBurn, trainingZone];
    //fatburn write to file
    
    hasFatFlag = YES;
    NSString *currentDateStr = [[CommonValues timeDateFormatter:DateFormatterTimeAllUnderLineGMT] stringFromDate:time];
    NSLog(@"%@",currentDateStr);
    NSString *currentDateStr4db = [[CommonValues timeDateFormatter:DateFormatterTimeColonGMT] stringFromDate:time];
    NSLog(@"%@",currentDateStr4db);
    NSString* fData = [[NSString alloc] init];
    fData = [fData stringByAppendingString:currentDateStr];
    fData = [fData stringByAppendingString:@":\t"];
    fData = [fData stringByAppendingString:[NSString stringWithFormat:@"%d\t%d\t%d\t\n",heartRate,fatBurn, trainingZone]];
    [FileOperation write2FileAppendLastTimeContent:fData filePath:[dashboard fatFilePath]];
    
    __LOG_METHOD_END;
}

- (void)currentCount:(NSDate *)time steps:(int)steps calories:(int)calories distance:(int)distance energy:(int)energy mode:(int)mode hr:(int)hr {
    __LOG_METHOD_START;
    NSLog(@">>>>>>>-----current Counts: Time:%@ Steps:%d Calories:%d Distance:%d Energy:%d Mode:%d HR:%d", time, steps, calories, distance, energy, mode, hr);
    currentStepCount = steps;
    currentDistanceCount = distance;
    currentCalorieCount = calories;
    
    if (!firstConnectFlag){
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        
        if (askCurrentCountNoResponseTimer.isValid) {
            [askCurrentCountNoResponseTimer invalidate];
            NSLog(@"askCurrentCountNoResponseTimer invalidate---");
        }
        
        NSLog(@"call trySyncData:NO");
        [[TGBleManager sharedTGBleManager] trySyncData:NO];
        
        if ([CommonValues getAutoSyncingFlag]) {
            NSLog(@"trySyncTimer start");
            trySyncTimer = [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(trySyncTimeOut) userInfo:nil repeats:NO];
        }
        
    }
    
    if (firstConnectFlag) {
        firstConnectFlag = NO;
    }
    __LOG_METHOD_END;
}

- (void)diagEventRecord:(NSDate*) time
              eventCode:(int) eventCode{
    __LOG_METHOD_START;
    NSLog(@"time = %@,eventCode = %d ",time,eventCode);
    __LOG_METHOD_END;

}

- (void)sleepResults:(TGsleepResult) result
           startTime:(NSDate*) startTime
             endTime:(NSDate*) endTime
            duration:(int) duration  // in seconds
            preSleep:(int) preSleep // in seconds
            notSleep:(int) notSleep // in seconds active time
           deepSleep:(int) deepSleep // in seconds
          lightSleep:(int) lightSleep // in seconds
         wakeUpCount:(int) wakeUpCount // number of wake ups after 1st sleep
          totalSleep:(int) totalSleep
             preWake:(int) preWake // number of seconds active before the end time
          efficiency:(int) efficiency {
    __LOG_METHOD_START;
    NSString * message;
    int sleepSeconds = 0;
    switch (result) {
            
        case TGsleepResultValid:
        {
            NSLog(@"sleepResults: TGsleepResultValid");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\nTotal Seconds: %d\nSeconds before Sleep: %d\nSeconds AWAKE: %d\nSeconds Deep Sleep: %d\nSeconds Light Sleep: %d\nWake Up Count: %d\nTotal Sleep Seconds: %d\nEfficiency: %d %%",
                       startTime,
                       endTime,
                       duration,
                       preSleep,
                       notSleep,
                       deepSleep,
                       lightSleep,
                       wakeUpCount,
                       totalSleep,
                       efficiency
                       ];
            sleepSeconds = lightSleep + deepSleep;
            
            
        }
            break;
            
        case TGsleepResultNegativeTime:
            NSLog(@"sleepResults: TGsleepResultNegativeTime");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\n\nStart Time is After the End Time",
                       startTime,
                       endTime
                       ];
            break;
            
        case TGsleepResultMoreThan48hours:
            NSLog(@"sleepResults: TGsleepResultMoreThan24hours");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\n\nToo Much Time, must be less than 24 hours",
                       startTime,
                       endTime
                       ];
            break;
        case TGsleepResultNoData:
            NSLog(@"sleepResults: TGsleepResultNoData");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\n\nNo Sleep Data Supplied",
                       startTime,
                       endTime
                       ];
            break;
            
        case TGsleepResultNotValid:
            NSLog(@"sleepResults: TGsleepResultNotValid");
        default:
            NSLog(@"sleepResults: default");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\n\nUnexpected Sleep Results\nResult Code: %lu",
                       startTime,
                       endTime,
                       (unsigned long)result
                       ];
            
            break;
    }
    
    
    
    //    dispatch_async(dispatch_get_main_queue(), ^{ [alert show]; }); // do all alerts on the main thread
    NSLog(@"message  = %@",message);
    totalEndTime = sleepSeconds;
    customerView3.label.text = [NSString stringWithFormat:@"%dh%dm",sleepSeconds/60,sleepSeconds%60];
    customerView3.progressImageView.progress = 1.0f*sleepSeconds/(goalSleep*60);
    
    __LOG_METHOD_END;
}

- (void) bleDidConnect {
    __LOG_METHOD_START;
    if ([timeOutTimer isValid]) {
        [timeOutTimer invalidate];
        NSLog(@"timeOutTimer invalidate----");
    }
    firstConnectFlag = YES;
    lostBeforeMainOperation = NO;
    devName  = [[TGBleManager sharedTGBleManager]advName];
    NSLog(@"devName = %@",devName);
    //isConnectedFlag = YES;
    isConnectingFlag = NO;
    [self delayTodo];
    //   [self performSelector:@selector(delayTodo) withObject:nil afterDelay:3];
    //auto bond when connected.
    [self bondClick:nil];
    __LOG_METHOD_END;
}

- (void)waitInd2Sdis{
    __LOG_METHOD_START;
    [disconnectedAlert dismissWithClickedButtonIndex:0 animated:NO];
    [connectedAlert show];
    __LOG_METHOD_END;
}

- (void)dismissSyncView{
    // dismiss sync view
    __LOG_METHOD_START;
    menuView.hidden = YES;
    [UIView beginAnimations:@"MoveIn" context:nil];
    [UIView setAnimationDuration:0.5];
    myScrollView.frame = CGRectMake(0, 43, 320, screenHeight-43-20);
    syncView.frame =  CGRectMake(0, 43, 320, 41);
    [UIView commitAnimations];
    [self resetSrollView];
    __LOG_METHOD_END;
}
- (void)delayTodo{
    __LOG_METHOD_START;
    fw_versionStr = [NSString stringWithFormat:@"FW Version %@",[[TGBleManager sharedTGBleManager]fwVersion] ];
    sw_versionStr=  [NSString stringWithFormat:@"SW Version %@",[[TGBleManager sharedTGBleManager] swVersion]];
    fw_versionLabel.text = fw_versionStr;
    sw_versionLabel.text = sw_versionStr;
    NSLog(@"fw_versionStr = %@",fw_versionStr);
    NSLog(@"sw_versionStr = %@",sw_versionStr);
    __LOG_METHOD_END;
}

- (void) bleDidDisconnect {
    __LOG_METHOD_START;

    firstEnterCandiDel = NO;
    issyncing =NO;
    isRT = NO;
    
    dispatch_async( dispatch_get_main_queue(), ^ {
        [self.view setUserInteractionEnabled:YES];
        [SVProgressHUD dismiss];
        [self unLockSyncBtn];
    });
    
    
    if (!selectDeviceBtnClickFlag) {
        NSLog(@"此处不应该被执行！！！");
        [connectedAlert dismissWithClickedButtonIndex:0 animated:NO];
        NSString *device_name = [[TGBleManager sharedTGBleManager] advName];
        NSString *msg = [NSString stringWithFormat:@"Disconnect from %@",device_name];
        disconnectedAlert = [[UIAlertView alloc]initWithTitle:msg message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@" OK ", nil];
        //        dispatch_async( dispatch_get_main_queue(), ^ {
        //            [disconnectedAlert show];
        //        });
        
    }
    __LOG_METHOD_END;
}

- (void) bleLostConnect {
    __LOG_METHOD_START;
    NSLog(@"setAutoSyncingFlag:NO");
    [CommonValues setAutoSyncingFlag:NO];
    firstConnectFlag = NO;
    
    dispatch_async( dispatch_get_main_queue(), ^ {
        [self unLockSyncBtn];
        
    });
    
    if ([timeOutTimer isValid]) {
        [timeOutTimer invalidate];
        NSLog(@"timeOutTimer invalidate----");
        
    }
    
    
    //erase过程中 丢失连接  没有erase result的处理
    if ( wantErase ) {
        NSLog(@"lost connection during erasing data!!!!  ");
        
        if ([eraseNotResponseTimer isValid]) {
            [eraseNotResponseTimer invalidate];
            NSLog(@"eraseNotResponseTimer invalidate----");
            
        }
        isSyncing = YES;
        wantSyncing = YES;
        wantRT = NO;
        [self showBigRetryBtnCase];
    }
    
    
    //连接上 band 之前就丢失连接 处理
    if (lostBeforeMainOperation ) {
        lostBeforeMainOperation = NO;
        if (wantSyncing) {
            NSLog(@"want to SYNC , lost connection before Main operation");
            syncPassFlag = NO;
             [dashboard write2SyncLogFileWithTimeStr:syncTimeStr withManualFlag:manualSyncFlag withPassFlag:syncPassFlag withDataReceived:dataReceived withTotalDataAvailable:totalDataAvailable];
            
        }
        
        else if (wantRT) {
            NSLog(@"want to RT , lost connection before Main operation");
            
        }
        else{
            NSLog(@"wantSyncing = no && wantRT = no,lost connection before Main operation");
            __LOG_METHOD_END;
            return;
        }
        
        NSData *token = [[TGBleManager sharedTGBleManager] bondToken];
        if (token == NULL) {
            if ([CommonValues iOSVersion] >= IOS8) {
                alertController = [UIAlertController alertControllerWithTitle:LostConnectTitle message:LostConnectMsg preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                    [self noTokenBondFailedCancel];
                    
                }];
                UIAlertAction *retryAction = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self noTokenBondFailedRetry];
                }];
                UIAlertAction *tryAnoterAction = [UIAlertAction actionWithTitle:@"Try another band" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self noTokenBondFailedRtyAnother];
                }];
                [alertController addAction:cancelAction];
                [alertController addAction:retryAction];
                [alertController addAction:tryAnoterAction];
                [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
            }
            else{
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:LostConnectTitle message:LostConnectMsg
                                                              delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry",@"Try another band", nil];
                alert.tag = noTokenBondFailedTag;
                dispatch_async( dispatch_get_main_queue(), ^ {
                    [alert show];
                });
            }
        }
        
        else{
            if ([CommonValues iOSVersion] >= IOS8) {
                alertController = [UIAlertController alertControllerWithTitle:LostConnectTitle message:LostConnectMsg preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                    [self hasTokenBondFailedCancel];
                    
                }];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self hasTokenBondFailedRetry];
                }];
                [alertController addAction:cancelAction];
                [alertController addAction:okAction];
                [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
            }
            else{
                UIAlertView *alert_hasToken = [[UIAlertView alloc]initWithTitle:LostConnectTitle message:LostConnectMsg delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
                
                alert_hasToken.tag = hasTokenBondFailedTag;
                dispatch_async( dispatch_get_main_queue(), ^ {
                    //                [self.view setUserInteractionEnabled:YES];
                    //                [SVProgressHUD dismiss];
                    [alert_hasToken show];
                });
                
            }
            
        }
    }
    else{
        dispatch_async( dispatch_get_main_queue(), ^ {
            
            [SVProgressHUD dismiss];
            [self.view setUserInteractionEnabled: YES];
        });
    }
    __LOG_METHOD_END;
}

- (void) bleDidAbortConnect {
    //
    // TODO: add code to turn off the Connection animation and
    // allow user to press the Connect button again
    //
    __LOG_METHOD_START;

    if (startSyncFlag && !syncFinishFlag) {
        // dismiss sync view
        dispatch_async( dispatch_get_main_queue(), ^ {
            [self dismissSyncView];
        });
    }
    
    
    NSString *lDeviceName = [[TGBleManager sharedTGBleManager] advName];
    NSString *msg = [NSString stringWithFormat:@"Abort Disconnect from %@",lDeviceName];
    NSLog(@"AbortConnect msg = %@",msg);
    
    if ([CommonValues iOSVersion] >= IOS8) {
        alertController = [UIAlertController alertControllerWithTitle:msg message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:okAction];
        [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:msg message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@" OK ", nil];
        dispatch_async( dispatch_get_main_queue(), ^ {
            [alert show];
        });
    }
    __LOG_METHOD_END;
}

- (void) eraseComplete:(TGeraseResult)result {
    __LOG_METHOD_START;
    NSLog(@">>>>>>>-----Recorded Data Erase complete: %lu", (unsigned long)result);
    
    
    if ([eraseNotResponseTimer isValid]) {
        [eraseNotResponseTimer invalidate];
        NSLog(@"eraseNotResponseTimer invalidate----");
        
    }
    eraseFlag = NO;
    wantErase = NO;
    if (result == TGeraseResultNormal) {
        
        NSLog(@"Erased successfully" );
        //save the latest ped data.
        [FileOperation save2UserDefaultsWithObj:[StringOperation stringFromInt:currentStepCount] withKey:STEP_LATESTDATE];
        [FileOperation save2UserDefaultsWithObj:[StringOperation stringFromInt:currentCalorieCount] withKey:CALORIE_LATESTDATE];
        [FileOperation save2UserDefaultsWithObj:[StringOperation stringFromInt:currentDistanceCount] withKey:DISTANCE_LATESTDATE];
        
        if ([CommonValues isBackgroundFlag]) {
            //sync(sync  + erase) finished & in background ,shoud disconnect band
            NSLog(@"sync 完成，且在后台，应断开连接");
            NSLog(@"call tryDisconnect ------");
            [[TGBleManager sharedTGBleManager] tryDisconnect];
        }
        
        dispatch_async( dispatch_get_main_queue(), ^ {
            syncMsgLabel.text = @"";
            syncMsgLabel.hidden = YES;
        });
        
        if (![CommonValues getAutoSyncingFlag]) {
            //不在自动同步的情况下，  动画刷新UI
            dispatch_async( dispatch_get_main_queue(), ^ {
                //                connectStateLabel.hidden = YES;
                syncProgressLabel.hidden = YES;
                syncProgress.hidden = YES;
                syncBgImg.hidden = YES;
                successful_updated_label.hidden = NO;
                //erase successfully ,start animation
                [UIView beginAnimations:@"MoveIn" context:nil];
                [UIView setAnimationDuration:1];
                
                myScrollView.frame = CGRectMake(0, 43, 320, screenHeight-43-20);
                syncView.frame =  CGRectMake(0, 43, 320, 41);
                
                [UIView commitAnimations];
            });
            
            sleep(1);
            [self performSelectorOnMainThread:@selector(delay2refreshUI) withObject:nil waitUntilDone:YES];
        }
        else{
            
            NSLog(@"autoSync success,erase success");
            //            sleep(1);
            [self performSelectorOnMainThread:@selector(autoSyncRefreshUI) withObject:nil waitUntilDone:NO];
            //            [self autoSyncRefreshUI];
            [self endBackgroundTask];
        }
        
        
    }
    else{
        
        
        NSLog(@"Erased failed" );
        if ([CommonValues isBackgroundFlag]) {
            NSLog(@"sync 完成，且在后台，应断开连接");
            NSLog(@"call tryDisconnect ------");
            [[TGBleManager sharedTGBleManager] tryDisconnect];
        }
        if ([CommonValues getAutoSyncingFlag]) {
            NSLog(@"autoSync success,erase failed");
        }
        else{
            
            dispatch_async( dispatch_get_main_queue(), ^ {
                
                //                connectStateLabel.hidden = YES;
                syncProgressLabel.hidden = YES;
                syncProgress.hidden = YES;
                syncBgImg.hidden = YES;
                successful_updated_label.hidden = NO;
                //erase successfully ,start animation
                [UIView beginAnimations:@"MoveIn" context:nil];
                [UIView setAnimationDuration:1];
                
                myScrollView.frame = CGRectMake(0, 43, 320, screenHeight-43-20);
                syncView.frame =  CGRectMake(0, 43, 320, 41);
                
                [UIView commitAnimations];
            });
            
        }
        //            [self performSelector:@selector(delay2refreshUI) withObject:nil afterDelay:1];
        sleep(1);
        [self performSelectorOnMainThread:@selector(delay2refreshUI) withObject:nil waitUntilDone:YES];
        
        
    }
    //    });
    
    
    NSLog(@"erase finished, sync over,set the HasTaskFlag to NO");
    [CommonValues setHasTaskFlag:NO];
    NSLog(@"setAutoSyncingFlag:NO");
    [CommonValues setAutoSyncingFlag:NO];
    wantSyncing  =  NO;
    isSyncing = NO;
    __LOG_METHOD_END;
}



- ( void ) exceptionMessage: ( TGBleExceptionEvent ) eventType
{
    __LOG_METHOD_START;
    NSString * message;
    
    switch ( eventType )
        {
        case TGBleHistoryCorruptErased:
            message = @"Flash Memory Corrupted, Will Be erased, suspect battery has been exhausted";
            break;

        case TGBleConfigurationModeCanNotBeChanged:
            message = @"Exception Message - BLE Connection Mode CAN NOT be changed";
            break;

        case TGBleUserAgeHasBecomeNegative_BirthDateIsNOTcorrect:
            message = @"User Age has become negative, correct the birth date";
            break;

        case TGBleUserAgeIsTooLarge_BirthDateIsNOTcorrect:
            message = @"User Age is TOO OLD, check the birth date";
            break;

        case TGBleUserBirthDateRejected_AgeOutOfRange:
            message = @"User Age is out of RANGE, corret the birth date";
            break;

        case TGBleFailedOtherOperationInProgress:
            message = @"Exception Message - Another Operation is Already in Progress";
            break;

        case TGBleFailedSecurityNotInplace:
            message = @"Exception Message - Security is NOT In Place";
            break;

        case TGBleTestingEvent:
            message = @"Exception Message - This is only a Test";
            break;

        case TGBleReInitializedNeedAlarmAndGoalCheck:
            message = @"Exception Message - Band lost power or was MFG reset, check your alarm and goal settings";
            break;

        case TGBleStepGoalRejected_OutOfRange:
            message = @"Exception Message - Step Goal Settings rejected -- OUT OF RANGE";
            break;

        case TGBleCurrentCountRequestTimedOut:
            message = @"Exception Message - Can Not get Current Count values\nBand not responding";
            break;

        case TGBleConnectFailedSuspectKeyMismatch:
            message = @"Exception Message - Band appears to be paired, possible encryption key mismatch, hold side button for 10 seconds to reboot";
            break;

        case TGBleNonRepeatingAlarmMayBeStaleCheckAlarmConfiguration:
            message = @"Exception Message - A Non-repeating Alarm may be stale, check your alarm settings";
            break;

        default:
            message = [NSString stringWithFormat:@"Exception Detected, code: %lu", (unsigned long)eventType];
            break;
        }

    //    NSLog(@"message = %@",message);

    NSLog(@"exception Message =  %@",message);

    __LOG_METHOD_END;
}




- (void)dataReceived:(NSDictionary *)data {
    __LOG_METHOD_START;

    if([data valueForKey:dicEKGheartRate])
    {
        cardioZoneHeartRate = [[data valueForKey:dicEKGheartRate] intValue];
        
        //        [ekgLongStr appendFormat:@"HR : %d\n",cardioZoneHeartRate];
        
        //hrLabel.text = hrStr;
        NSLog(@"cardioZoneHeartRate  ======  %d",cardioZoneHeartRate);
        [hrArr addObject:[NSNumber numberWithInt:cardioZoneHeartRate]];
        
    }
    if([data valueForKey:dicEKGheartRate_TS])
    {
        cardioZoneHeartRate_TS = [data valueForKey:dicEKGheartRate_TS];
        NSLog(@"CardioZoneHeartRate_TS ====== %@", cardioZoneHeartRate_TS);
    }
    if([data valueForKey:dicEKGr2rInt]) {
        rrInt = [[data valueForKey:dicEKGr2rInt] intValue];
        // rrIntLabel.text = rrIntStr;
        NSLog(@"rrInt  ======  %d",rrInt);
    }
    if([data valueForKey:dicEKGr2rInt_TS]) {
        rrInt_TS = [data valueForKey:dicEKGr2rInt_TS];
        NSLog(@"rrInt_TS ====== %@", rrInt_TS);
    }
    
    if([data valueForKey:dicEKGhrv]) {
        cardioZoneHRV = [[data valueForKey:dicEKGhrv ] intValue];
        // hrvLabel.text = hrvStr;
        NSLog(@"cardioZoneHRV  ======  %d",cardioZoneHRV);
        
        [hrvArr addObject:[NSNumber numberWithInt:cardioZoneHRV]];
        
    }
    if([data valueForKey:dicEKGhrv_TS]) {
        cardioZoneHRV_TS = [data valueForKey:dicEKGhrv_TS];
        NSLog(@"CardioZoneHRV_TS ====== %@", cardioZoneHRV_TS);
    }
    
    if([data valueForKey:dicEKGstress]) {
        stress = [[data valueForKey:dicEKGstress] intValue];
        NSLog(@"synced stress ====== %d", stress);
    }
    
    if([data valueForKey:dicEKGheartAge]) {
        heartAge = [[data valueForKey:dicEKGheartAge] intValue];
        NSLog(@"synced heartAge ====== %d", heartAge);
    }
    __LOG_METHOD_END;
}

- (void)setStressConfig{
    __LOG_METHOD_START;
    //    [[TGBleManager sharedTGBleManager] setHeartAgeOutputPoint:30];
    //    [[TGBleManager sharedTGBleManager] setHeartAgeRecordNumber:5];
    //    [[TGBleManager sharedTGBleManager] setHeartAgeParameters:@"Mary" withAge:30 withPath:@""];
    //    [[TGBleManager sharedTGBleManager] setStressParameters:@"Mary" withGender:[[TGBleManager sharedTGBleManager] isFemale] withAge:[[TGBleManager sharedTGBleManager] age] withHeight:[[TGBleManager sharedTGBleManager] height] withWeight:[[TGBleManager sharedTGBleManager] weight]*10 withPath:@""];
    //    [[TGBleManager sharedTGBleManager] setStressOutputInterval:30];
    /*
     [[TGBleManager sharedTGBleManager] setHeartAgeOutputPoint:30];
     [[TGBleManager sharedTGBleManager] setHeartAgeRecordNumber:5];
     [[TGBleManager sharedTGBleManager] setHeartAgeParameters:@"Tommy" withAge:26 withPath:@""];
     [[TGBleManager sharedTGBleManager] setStressParameters:@"Tommy" withGender:true withAge:26 withHeight:181 withWeight:70 withPath:@""];
     [[TGBleManager sharedTGBleManager] setStressOutputInterval:30];
     */
    __LOG_METHOD_END;
}

- (void) candidateFound:(NSString *)devName_ rssi:(NSNumber *)rssi mfgID:(NSString *)mfgID candidateID:(NSString *)candidateID
{
    __LOG_METHOD_START;
    devName = devName_;
    NSString* message = [NSString stringWithFormat:
                         @"Device Name: %@\nMfg Data: %@\nID: %@\nRSSI value: %@",
                         devName,
                         mfgID,
                         candidateID,
                         rssi
                         ];
    NSLog(@"SELECT DEVICE candidateFound delegate: %@", message);
    if ([candIDArray containsObject:candidateID]) {
        
        
        NSLog(@"SDK appear the repeated candidateID in candidateFound delegate!!!");
        __LOG_METHOD_END;
        return;
    }
    else{
        if (!firstEnterCandiDel) {
            NSLog(@"first enter candidate Found");
            firstEnterCandiDel = YES;
            if ([timeOutTimer isValid]) {
                [timeOutTimer invalidate];
                NSLog(@"timeOutTimer invalidate----");
                
            };
            [SVProgressHUD dismiss];
            
            listView = [[ZSYPopoverListView alloc] initWithFrame:CGRectMake(0, 0, 300, 400)];
            listView.tagert =self;
            listView.stopEBLScan =@selector(stopScan);
            listView.datasource = self;
            listView.delegate = self;
            showListTimer =  [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(delay2ShowList) userInfo:nil repeats:NO];
            
            //        [listView show];
            
        }
        haveDeviceFlag = YES;
        [devicesArray addObject:devName];
        [mfgDataArray addObject:mfgID];
        [rssiArray addObject:rssi];
        [candIDArray addObject:candidateID];
        [devicesIDArray addObject:candidateID];
        
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [listView reload];
    });
    __LOG_METHOD_END;
}

- (void)delay2ShowList{
    __LOG_METHOD_START;
    [listView show];
    __LOG_METHOD_END;
}


- (void)startCardioPlot{
    __LOG_METHOD_START;
    isRT = NO;
    
    if ([TGBleManager sharedTGBleManager].status == TGBleConnected ||
        [TGBleManager sharedTGBleManager].status == TGBleConnectedToBond  )
        
    {
        NSLog(@"normal， 正常跳转到realtime");
        [SVProgressHUD performSelectorOnMainThread:@selector(dismiss) withObject:nil waitUntilDone:YES];
        //        sleep(1);
        [self.view setUserInteractionEnabled:YES];
        dispatch_async( dispatch_get_main_queue(), ^ {
            //for iphone 5/5s
            if (screenHeight == 568) {
                CardioPlotViewController *cpvc = [[CardioPlotViewController alloc]initWithNibName:@"CardioPlotViewController_iphone5" bundle:nil] ;
                [self presentViewController:cpvc animated:YES
                                 completion:nil];
                
                
            }
            //for iphone4s
            else{
                
                CardioPlotViewController *cpvc = [[CardioPlotViewController alloc]initWithNibName:@"CardioPlotViewController_iphone4s" bundle:nil] ;
                [self presentViewController:cpvc animated:YES
                                 completion:nil];
            }
        });
    }
    
    else{
        //跳转前，断掉了     disconnect unfortunally
        NSLog(@"很遗憾，realtime 跳转前disconnect");
    }
    __LOG_METHOD_END;
}


- (void)eventTapPlaybackImg{
    __LOG_METHOD_START;
    ECGHistoryViewController *hsv = [[ECGHistoryViewController alloc]init];
    [self presentViewController:hsv animated:YES completion:nil];
    __LOG_METHOD_END;
}

- (BOOL)prefersStatusBarHidden {
    __LOG_METHOD_START;
    return NO;
}


//test calculate sleep time
- (void)calculateSleepTime{
    __LOG_METHOD_START;
    NSMutableArray * const sleepTimeArrTommy = /* [[NSMutableArray alloc]init];
    sleepTimeArrTommy = */ [CommonValues getSleepTimeArr :db];
    
    if (sleepTimeArrTommy.count > 0) {
        
        [CommonValues calculateSleepTime:[sleepTimeArrTommy objectAtIndex:0] :db];
    }
    
    __LOG_METHOD_END;
}


#if( 0 )
- (void)showBTNotOnAlert {
    __LOG_METHOD_START;
    if ([CommonValues iOSVersion] >= IOS8) {
        alertController = [UIAlertController alertControllerWithTitle:BluetoothIsOffTitle message:BluetoothIsOffMsg preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self checkBTStatusDone];
        }];
        [alertController addAction:okAction];
        [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
    }else{
        UIAlertView *alert =[ [UIAlertView alloc]initWithTitle:BluetoothIsOffTitle message:BluetoothIsOffMsg delegate:self cancelButtonTitle:nil otherButtonTitles:@"Done", nil];
        alert.tag = checkBTStatusTag;
        [alert show];
    }
    __LOG_METHOD_END;
}
#endif


- (void)centralManagerDidUpdateState:(CBCentralManager *)central{
    __LOG_METHOD_START;
    NSLog(@"central.state = %ld",(long)central.state);
    NSString *message;
    switch (central.state) {
        case 0:
            message = @"初始化中，请稍后……";
            NSLog(@"message1 = %@",message );
            break;
        case 1:
            message = @"设备不支持状态，过会请重试……";
            NSLog(@"message = %@",message );
            break;
        case 2:
            message = @"设备未授权状态，过会请重试……";
            NSLog(@"message = %@",message );
            break;
        case 3:
            message = @"设备未授权状态，过会请重试……";
            NSLog(@"message = %@",message );
            break;
        case 4:{
            message = @"尚未打开蓝牙，请在设置中打开……";
            NSLog(@"message1111 = %@",message );
            [CommonValues setBluetoothOnFlag:NO];
            
            
        }
            break;
        case 5:
            message = @"蓝牙已经成功开启，稍后……";
            NSLog(@"message = %@",message );
            [CommonValues setBluetoothOnFlag:YES];
            break;
        default:
            break;
            
            
    }
    __LOG_METHOD_END;
}

- (void)unLockSyncBtn{
    __LOG_METHOD_START;
    [syncBtn setBackgroundImage:[UIImage imageNamed:@"menu_normal"] forState:UIControlStateNormal];
    [syncBtn setUserInteractionEnabled:YES];
    __LOG_METHOD_END;
}

- (void)lockSyncBtn{
    __LOG_METHOD_START;
    [syncBtn setBackgroundImage:[UIImage imageNamed:@"menu_press"] forState:UIControlStateNormal];
    [syncBtn setUserInteractionEnabled:NO];
    __LOG_METHOD_END;
}

- (void)autoSyncRefreshUI{
    __LOG_METHOD_START;

    int ecgCount = 0;
    NSDate *date = [NSDate date];
    NSString* dateStr =  [[CommonValues timeDateFormatter:DateFormatterDate] stringFromDate:date];
    ecgCount = [CommonValues getDayECGCount:dateStr :db];
    
    NSLog(@"ecgCount  =  %d",ecgCount);
    successful_updated_label.hidden = YES;
    jinCount = 0;
    yinCount = 0;
    tongCount = 0 ;
    totalEndTime = 0;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    
    //BaseAppMode
    
    //    steps_circle.progress = 1.0*currentStepCount/goalStep;
    NSMutableArray *arrayWithPreviousWantAndTpye = [[NSMutableArray alloc]init];
    float wantStepPercent =  1.0*currentStepCount/goalStep;
    [arrayWithPreviousWantAndTpye addObject:[NSNumber numberWithFloat:previousStepPercent]];
    [arrayWithPreviousWantAndTpye addObject:[NSNumber numberWithFloat:wantStepPercent]];
    [arrayWithPreviousWantAndTpye addObject:AnimationTypeStep];
    
    NSThread *stepProgressIncreaseThread = [[NSThread alloc]initWithTarget:self selector:@selector(stepProgressIncreaseRun:) object:arrayWithPreviousWantAndTpye];
    [stepProgressIncreaseThread start];
    previousStepPercent = wantStepPercent;
    
    steps_circle.actualString = [NSString stringWithFormat:@"%d",currentStepCount];
    
    
    //    calories_circle.progress = 1.0*currentCalorieCount/goalCalories;
    calories_circle.actualString = [NSString stringWithFormat:@"%d",currentCalorieCount];
    NSMutableArray *arrayWithPreviousWantAndTpyeCalo = [[NSMutableArray alloc]init];
    float wantCaloriePercent =  1.0*currentCalorieCount/goalCalories;
    [arrayWithPreviousWantAndTpyeCalo addObject:[NSNumber numberWithFloat:previousCaloriePercent]];
    [arrayWithPreviousWantAndTpyeCalo addObject:[NSNumber numberWithFloat:wantCaloriePercent]];
    [arrayWithPreviousWantAndTpyeCalo addObject:AnimationTypeCalorie];
    
    NSThread *calorieProgressIncreaseThread = [[NSThread alloc]initWithTarget:self selector:@selector(stepProgressIncreaseRun:) object:arrayWithPreviousWantAndTpyeCalo];
    [calorieProgressIncreaseThread start];
    previousCaloriePercent = wantCaloriePercent;
    
    
    
    customerView1.label.text = [CommonValues distanceStrFromDistanceCount:currentDistanceCount] ;
    customerView1.progressImageView.progress = 1.0*currentDistanceCount/goalDistance;
    
    
    NSString *activeTimeStr = @"0h0m";
    NSInteger active_time_minutes = 0;
    if (dbTimeArray.count > 0 ) {
        [CommonValues activeModeArr:[dbTimeArray objectAtIndex:0] :db];
        active_time_minutes = [CommonValues getActiveTimeTotalMinutes];
        activeTimeStr = [NSString stringWithFormat:@"%ldh%ldm",(long)active_time_minutes/60,(long)active_time_minutes%60];
    }
    
    customerView2.progressImageView.progress = 1.0 *active_time_minutes/ goalActiveTime/60;
    customerView2.label.text = activeTimeStr;
    
    
    //    totalEndTime = [CommonValues getSleepTotalMinutes];
    NSMutableArray * const sleepDateArr = /* [[NSMutableArray alloc] init];
    sleepDateArr =  */ [CommonValues getSleepTimeArr :db];
    if (sleepDateArr.count > 0) {
        NSString *dateFromSleep = [sleepDateArr objectAtIndex:[CommonValues getActivityIndex]];
        [dashboard startAnalysisSleepWithDate:dateFromSleep withDB:db];
    }
    else{
        totalEndTime = 0;
    }
    
    sleep_hour = totalEndTime/60;
    sleep_min = totalEndTime%60;
    //      NSLog(@"hour = %d, min = %d",hour,min);
    customerView3.progressImageView.progress = 1.0*(sleep_hour*60+sleep_min)/goalSleep/60;
    customerView3.label.text = [NSString stringWithFormat:@"%ldh%ldm",(long)sleep_hour,(long)sleep_min];
    //     [myScrollView addSubview:customerView3];
    //    [customerView3 addGestureRecognizer:tapGestureSleep];
    
    //add separate line
    
    NSString *lastSyncTime =  [defaults stringForKey:@"last_sync_time"];
    if (lastSyncTime) {
        label1.text = [NSString stringWithFormat:@"Last sync %@",lastSyncTime];
    }
    
    
    //stanly demo mode check;
    
    
    if ([defaults stringForKey:@"StanlyDemoMode"] == nil  || [[defaults stringForKey:@"StanlyDemoMode"] isEqualToString:@"0"]) {
        // test trend view instead of image
        NSMutableArray * const mul_arr = /* [[NSMutableArray alloc]init];
        mul_arr =  */ [CommonValues  getHistoryHROrHRVArr2:db :YES];
        NSLog(@"------1111------");
        if (mul_arr.count > 0) {
            int sumHR = 0;
            NSInteger count = mul_arr.count;
            for (int i = 0; i < count; i++) {
                sumHR += [[mul_arr objectAtIndex:i]intValue];
            }
            int avgHR = sumHR/count;
            NSLog(@"sumHR = %d,count = %ld, hr_bpm_labeRight avgHR = %d",sumHR,(long)count,avgHR);
            //                NSMutableArray *temp2Arr = [[NSMutableArray alloc]init];
            //                temp2Arr  = mul_arr.lastObject;
            NSString *latestHR = [NSString stringWithFormat:@"%@", mul_arr.lastObject];
            NSString *weekAvgHR = [NSString stringWithFormat:@"%d", avgHR];
            
            //            [hr_bpm_labelLeft setText:[mul_arr objectAtIndex:0]];
            //            [hr_bpm_labeRight setText:[NSString stringWithFormat:@"%d",avgHR]];
            //以上会引起 [__NSCFNumber length]: unrecognized selector sent to instance  错误
            [hr_bpm_labelLeft setText:latestHR];
            [hr_bpm_labeRight setText:weekAvgHR];
            
            [trendView1 removeFromSuperview];
            trendView1 = [[TrendView alloc]initWithFrame:CGRectMake(11+72,350+50+20+25+5+5, 596/2/2+30, 240/2/2) dataArr:mul_arr];
            [myScrollView addSubview:trendView1];
            
            [trendLineView1 removeFromSuperview];
            trendLineView1 = [[TrendLineView alloc]initWithFrame:CGRectMake(11+72,350+50+20+25+5+5, 596/2/2+30, 240/2/2) :mul_arr];
            [myScrollView addSubview:trendLineView1];
            trendLineView1.userInteractionEnabled = YES;
            [trendLineView1 addGestureRecognizer:tapGestureHR];
        }
        
        //hrv graph
        NSLog(@"------2222------");
        
        NSMutableArray * const hrv_arr = /* [[NSMutableArray alloc]init];
        
        hrv_arr = */ [CommonValues getHistoryHROrHRVArr2:db:NO];
        
        if (hrv_arr.count > 0) {
            NSInteger sumHRV = 0;
            NSInteger  count = 0;
            count = hrv_arr.count;
            for (NSInteger i = 0; i < hrv_arr.count; i++) {
                sumHRV += [[hrv_arr objectAtIndex:i]intValue];
                
            }
            NSInteger avgHRV = sumHRV/count;
            NSLog(@"hr_bpm_labeRight avgHRV = %ld",(long)avgHRV);
            
            
            NSString *latestHRV = [NSString stringWithFormat:@"%@",hrv_arr.lastObject];
            NSString *weekAvgHRV = [NSString stringWithFormat:@"%ld", (long)avgHRV];
            
            //            [hr_bpm_labelLeft setText:[mul_arr objectAtIndex:0]];
            //            [hr_bpm_labeRight setText:[NSString stringWithFormat:@"%d",avgHR]];
            //以上会引起 [__NSCFNumber length]: unrecognized selector sent to instance  错误
            [hrv_labelLeft setText:latestHRV];
            [hrv_labelRight setText:weekAvgHRV];
            
            [trendView2 removeFromSuperview];
            trendView2 = [[TrendView alloc]initWithFrame:CGRectMake(11+72,455+120, 596/2/2+30, 240/2/2) dataArr:hrv_arr];
            [myScrollView addSubview:trendView2];
            [trendLineView2 removeFromSuperview];
            trendLineView2 = [[TrendLineView alloc]initWithFrame:CGRectMake(11+72,455+120, 596/2/2+30, 240/2/2):hrv_arr];
            [myScrollView addSubview:trendLineView2];
            trendLineView2.userInteractionEnabled = YES;
            [trendLineView2 addGestureRecognizer:tapGestureHRV];
            
        }
        
    }
    else{
        NSLog(@"initUI get StanlyDemoMode vules unnormal,should be somewhere set error");
        
    }
    
    
    NSLog(@"current step = %d ,calories = %d ,Distance = %d",currentStepCount,currentCalorieCount,currentDistanceCount);
    
    [self changeECGCountImgWithCount:ecgCount];
    [self updateMedalImg];
    __LOG_METHOD_END;
}

-(void)changeECGCountImgWithCount : (int)ecgCount{
    __LOG_METHOD_START;
    if (ecgCount <= 0) {
        ecgCountIv1.image =  [UIImage imageNamed:@"ecg_undone"];
        ecgCountIv2.image =  [UIImage imageNamed:@"ecg_undone"];
        ecgCountIv3.image =  [UIImage imageNamed:@"ecg_undone"];
    }
    else if (ecgCount == 1){
        ecgCountIv1.image =  [UIImage imageNamed:@"ecg_done"];
        ecgCountIv2.image =  [UIImage imageNamed:@"ecg_undone"];
        ecgCountIv3.image =  [UIImage imageNamed:@"ecg_undone"];
    }
    else if (ecgCount == 2){
        
        ecgCountIv1.image =  [UIImage imageNamed:@"ecg_done"];
        ecgCountIv2.image =  [UIImage imageNamed:@"ecg_done"];
        ecgCountIv3.image =  [UIImage imageNamed:@"ecg_undone"];
    }
    else{
        ecgCountIv1.image =  [UIImage imageNamed:@"ecg_done"];
        ecgCountIv2.image =  [UIImage imageNamed:@"ecg_done"];
        ecgCountIv3.image =  [UIImage imageNamed:@"ecg_done"];
    }
    __LOG_METHOD_END;
}

- (void) updateMedalImg{
    __LOG_METHOD_START;
    float wantPercentStep = 1.0*currentStepCount/goalStep;
    float wantPercentCalorie = 1.0*currentCalorieCount/goalCalories;
    //    float wantPercentDistance = 1.0*currentDistanceCount/goalStep;
    if (wantPercentStep >= 1) {
        step_medal_image.image=[UIImage imageNamed:@"gold_medal@2x.png"];
        jinCount++;
    }
    else if (wantPercentStep < 1 && wantPercentStep >= 0.9){
        step_medal_image.image=[UIImage imageNamed:@"silver_medal@2x.png"];
        yinCount++;
        
    }
    
    else if (wantPercentStep < 0.9 && wantPercentStep >= 0.8){
        step_medal_image.image=[UIImage imageNamed:@"bronze_medal@2x.png"];
        tongCount++;
        
    }
    else
    {
        //        NSLog(@"steps no demal !");
        step_medal_image.image = NULL;
    }
    
    
    if (wantPercentCalorie >= 1) {
        calorie_medal_image.image=[UIImage imageNamed:@"gold_medal@2x.png"];
        jinCount++;
    }
    else if (wantPercentCalorie < 1 && wantPercentCalorie >= 0.9){
        calorie_medal_image.image=[UIImage imageNamed:@"silver_medal@2x.png"];
        yinCount++;
        
    }
    
    else if (wantPercentCalorie < 0.9 && wantPercentCalorie >= 0.8){
        calorie_medal_image.image=[UIImage imageNamed:@"bronze_medal@2x.png"];
        tongCount++;
        
    }
    else
    {
        // NSLog(@"calories no demal !");
        calorie_medal_image.image = NULL;
    }
    
    //distance demal
    if (1.0*currentDistanceCount/goalDistance >= 1) {
        distance_medal_image.image=[UIImage imageNamed:@"gold_medal@2x.png"];
        jinCount++;
    }
    else if (1.0*currentDistanceCount/goalDistance < 1 && 1.0*currentDistanceCount/goalDistance >= 0.9){
        distance_medal_image.image=[UIImage imageNamed:@"silver_medal@2x.png"];
        yinCount++;
        
    }
    
    else if (1.0*currentDistanceCount/goalDistance < 0.9 && 1.0*currentDistanceCount/goalDistance >= 0.8){
        distance_medal_image.image=[UIImage imageNamed:@"bronze_medal@2x.png"];
        tongCount++;
        
    }
    else
    {
        distance_medal_image.image= NULL;
        //  NSLog(@"distance no demal !");
    }
    
    
    //active time medal
    if (1.0*[CommonValues getActiveTimeTotalMinutes]/goalActiveTime/60 >= 1) {
        active_time_medal_image.image=[UIImage imageNamed:@"gold_medal@2x.png"];
        jinCount++;
    }
    else if (1.0*[CommonValues getActiveTimeTotalMinutes]/goalActiveTime/60 < 1 && 1.0*[CommonValues getActiveTimeTotalMinutes]/goalActiveTime/60 >= 0.9){
        active_time_medal_image.image=[UIImage imageNamed:@"silver_medal@2x.png"];
        yinCount++;
        
    }
    
    else if (1.0*[CommonValues getActiveTimeTotalMinutes]/goalActiveTime/60 < 0.9 &&1.0*[CommonValues getActiveTimeTotalMinutes]/goalActiveTime/60 >= 0.8){
        active_time_medal_image.image=[UIImage imageNamed:@"bronze_medal@2x.png"];
        tongCount++;
        
    }
    else
    {
        
        //   NSLog(@"sleep no demal !");
        active_time_medal_image.image = NULL;
    }
    
    //sleep medal
    if (1.0*(sleep_hour*60+sleep_min)/goalSleep/60 >= 1) {
        sleep_medal_image.image=[UIImage imageNamed:@"gold_medal@2x.png"];
        jinCount++;
    }
    else if (1.0*(sleep_hour*60+sleep_min)/goalSleep/60 < 1 && 1.0*(sleep_hour*60+sleep_min)/goalSleep/60 >= 0.9){
        sleep_medal_image.image=[UIImage imageNamed:@"silver_medal@2x.png"];
        yinCount++;
        
    }
    
    else if (1.0*(sleep_hour*60+sleep_min)/goalSleep/60 < 0.9 &&1.0*(sleep_hour*60+sleep_min)/goalSleep/60 >= 0.8){
        sleep_medal_image.image=[UIImage imageNamed:@"bronze_medal@2x.png"];
        tongCount++;
        
    }
    else
    {
        
        //   NSLog(@"sleep no demal !");
        sleep_medal_image.image = NULL;
    }
    
    jinLabel.text=[NSString stringWithFormat:@"× %d",jinCount];
    yinLabel.text=[NSString stringWithFormat:@"× %d",yinCount];
    tongLabel.text=[NSString stringWithFormat:@"× %d",tongCount];
    __LOG_METHOD_END;
}



- (void)delay2ShowSVProgressHUD : (NSString *) statusStr{
    __LOG_METHOD_START;
    static int const usleepTime = 300000;
    usleep(usleepTime);
    [SVProgressHUD showWithStatus:statusStr];
    __LOG_METHOD_END;

}

- (void)resetSyncProView{
    __LOG_METHOD_START;
    [self.view setUserInteractionEnabled:NO];
    [SVProgressHUD dismiss];
    syncProgressLabel.text = @"0%";
    syncProgress.progress = 0.0f;
    syncView.hidden = NO;
    retryBtn.hidden = YES;
    __LOG_METHOD_END;
}

- (void)cancelActionEvent{
    __LOG_METHOD_START;
    return;
}

- (void)showAlertController{
    __LOG_METHOD_START;
    [self presentViewController:alertController animated:YES completion:nil];
    __LOG_METHOD_END;
}
@end

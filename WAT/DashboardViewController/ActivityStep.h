//
//  ActivityStep.h
//  WAT
//
//  Created by NeuroSky on 11/11/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"
#import "circleProgressSmall.h"
#import <sqlite3.h>
#import "circleProgressSmall.h"
#import "Constant.h"
#import "CommonValues.h"
#import "ActivityCalorieViewController.h"
#import "ActivityDistanceViewController.h"
#import "Sleep_ViewController.h"
#import "SleepViewController.h"
#import "ActiveTimeViewController.h"
#import "StringOperation.h"
#import "TGBleManagerDelegate.h"

@interface ActivityStep : UIViewController < CPTPlotDataSource, TGBleManagerDelegate >
{
    BOOL storedUnits;
    NSArray *StepArray;
    sqlite3 *db;
    NSMutableArray *mulStepArray;
    NSMutableArray *tempMulStepArray;
    NSArray *CaloriesArray;
    NSArray *DistanceArray;
    int Step_count;
    int Calories_count;
    int Distance_count;
    CPTXYGraph *barChart;
    CPTXYGraph *barChart1;
    CPTGraphHostingView *hv;
    circleProgressSmall *circle;
    UILabel *label;
    NSString *timeString;
    CGFloat beginy;
    UILabel *noticeLabel;
    UIView *uiView;
    NSMutableArray *step4GraphArray;
    NSMutableArray  *dbTimeArray;
    NSMutableArray *tempStepsArrList;
    NSMutableArray *oneDayStepArr; //6 am ~ 10 pm
    NSInteger subStepInOneHour;
    NSInteger stepIndex;
    NSInteger caloriesIndex;
    NSInteger distanceIndex;
    NSInteger timeDateIndex;
    NSInteger  tempStepIndex;
    NSInteger recordCount;
    NSInteger recordIndex;
    NSInteger goalStep;
    NSMutableArray * sleepPhaseArray;
    NSMutableArray *sleepTimeArray;
    NSInteger  sleepIndex;
    NSInteger dayTotalSleepTime;
    NSString *time1;
    NSString *time2;
    NSMutableArray *sleepIntervalTimeArr;
    NSInteger totalEndTime;
    int oneDayTotalStep;
    int oneDayTotalCalories;
    int oneDayTotalDistance;
    NSString *demoModeStr;
    NSDateFormatter *df;
    UIColor *stepColor;
    
    __weak IBOutlet UIView *myWholeView;
    __weak IBOutlet UILabel *leftLabel;
    __weak IBOutlet UILabel *rightLabel;
    __weak IBOutlet UILabel *midLabel;
    __weak IBOutlet UILabel *rightMidLabel;
    __weak IBOutlet UILabel *leftMidLabel;
    __weak IBOutlet UIImageView *sleepImg;
    __weak IBOutlet UIImageView *activeImg;
    __weak IBOutlet UIImageView *distanceImg;
    __weak IBOutlet UIImageView *caloriesImg;
    
}
@property (strong, nonatomic) IBOutlet UILabel *title_value;
@property (strong, nonatomic) IBOutlet UILabel *title_value2;

@property (strong, nonatomic) IBOutlet CPTGraphHostingView *hv;
@property (strong, nonatomic) IBOutlet UILabel *calories_value;
@property (strong, nonatomic) IBOutlet UILabel *distance_value;
@property (strong, nonatomic) IBOutlet UILabel *activity_time_value;
@property (strong, nonatomic) IBOutlet UILabel *sleep_value;
@property (strong, nonatomic) IBOutlet UILabel *step;

@property (strong, nonatomic) IBOutlet UILabel *calories;
- (IBAction)back:(id)sender;

- (IBAction)leftBt:(id)sender;
- (IBAction)rightBt:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *img;

@end

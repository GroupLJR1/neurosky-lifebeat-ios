//
//  DashboardViewController_Cellular.h
//  WAT
//
//  Created by Mark I. Walsh on 2015-02-28.
//  Copyright (c) 2015 Mark I. Walsh. All rights reserved.
//


#import <UIKit/UIKit.h>


#if !defined( DashboardViewController_Cellular_included )
#define DashboardViewController_Cellular_included


@interface DashboardViewController ( Cellular )

//  prompt warning about excessive data usage...
- ( void ) cellularDisplayDataWarning;

//  handle "ignore" response...
- ( void ) cellularNotShowAgain;

//  handle "OK" response...
- ( void ) cellularOK;

@end  //  end @interface DashboardViewController ( Cellular )


#endif  // !defined( DashboardViewController_Cellular_included )

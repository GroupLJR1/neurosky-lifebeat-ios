//
//  ActiveTimeViewController.m
//  WAT
//
//  Created by neurosky on 12/4/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//


#import "ActiveTimeViewController.h"
#import "ActivityDistanceViewController.h"
#import "ActivityStep.h"
#import "ActivityCalorieViewController.h"
#import "Sleep_ViewController.h"
#import "SleepViewController.h"
#import "Constant.h"
#import "StringOperation.h"

#import "TGBleEmulator.h"

@interface ActiveTimeViewController ()

@end

@implementation ActiveTimeViewController

- (void)dataReceived:(NSDictionary *)data { return; }



-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"active time viewdidload");
    // Do any additional setup after loading the view from its nib.
    myWholeView.frame = CGRectMake(0, [CommonValues offsetHeight4DiffiOS], 320, 568-20);
    db = [CommonValues getMyDB];
    
    titleLabel.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:25.0];
    
    titleLabel.textColor = [UIColor grayColor];
    
    //dynamic scroll view content
    CGRect rect_screen = [[UIScreen mainScreen]bounds];
    CGSize size_screen = rect_screen.size;
    screenHeight = size_screen.height;
    NSLog(@"screenHeight = %f",screenHeight);
    
    //reset ystLast
    ystLastSleepPhase = 1;
    ystLastSleepTime = @"";
    if ([CommonValues getScreenHeight] == 568) {
        NSLog(@"Active time ScreenHeight = %f",[CommonValues getScreenHeight]);
        my_scrollview4S = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 43+[CommonValues offsetHeight4DiffiOS], 320, 568-43-32)];
    }
    else{
        my_scrollview4S = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 43+[CommonValues offsetHeight4DiffiOS], 320, 525-88-22)];
        my_scrollview4S.scrollEnabled = NO;
        
    }
    my_scrollview4S.contentSize = CGSizeMake(320, 568-43-32);
    //    UIImageView *iv =[ [UIImageView alloc] initWithFrame:CGRectMake(0, 40, 320, 504)];
    //                      iv.image =      [UIImage imageNamed:@"activity_bg"];
    UIImageView *iv =[ [UIImageView alloc] initWithImage: [UIImage imageNamed:@"activity_bg"]];
    [my_scrollview4S addSubview:iv];
    //    headView.frame = CGRectMake(0, 43, 320, 32);
    [my_scrollview4S addSubview:headView];
    
    hv_4S = [[CPTGraphHostingView alloc]initWithFrame:CGRectMake(10, 39, 300, 183)];
    [my_scrollview4S addSubview:hv_4S];
    stepLabel = [[UILabel alloc]initWithFrame:CGRectMake(59, 265, 100, 34)];
    stepLabel.textAlignment = NSTextAlignmentCenter;
    //    stepLabel.backgroundColor = [UIColor yellowColor];
    [my_scrollview4S addSubview:stepLabel];
    calorieLabel = [[UILabel alloc]initWithFrame:CGRectMake(220, 265, 90, 34)];
    calorieLabel.textAlignment = NSTextAlignmentCenter;
    //     calorieLabel.backgroundColor = [UIColor yellowColor];
    [my_scrollview4S addSubview:calorieLabel];
    distanceLabel = [[UILabel alloc]initWithFrame:CGRectMake(59, 359-15+10, 100, 34)];
    distanceLabel.textAlignment = NSTextAlignmentCenter;
    [my_scrollview4S addSubview:distanceLabel];
    sleepLabel = [[UILabel alloc]initWithFrame:CGRectMake(220, 359-15+10, 90, 34)];
    sleepLabel.textAlignment = NSTextAlignmentCenter;
    [my_scrollview4S addSubview:sleepLabel];
    
    
    stepImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 39+183+28, 160, 90)];
    stepImg.image = [UIImage imageNamed:@"detail_steps"];
    
    //    stepImg.backgroundColor = [UIColor blueColor];
    calorieImg = [[UIImageView alloc] initWithFrame:CGRectMake(160, 39+183+28, 160, 90)];
    calorieImg.image = [UIImage imageNamed:@"detail_calories"];
    //     calorieImg.backgroundColor = [UIColor yellowColor];
    
    distanceImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 39+183+90+28, 160, 90)];
    distanceImg.image = [UIImage imageNamed:@"detail_distance"];
    //     distanceImg.backgroundColor = [UIColor redColor];
    
    sleepImg = [[UIImageView alloc] initWithFrame:CGRectMake(160, 39+183+90+28, 160, 90)];
    sleepImg.image = [UIImage imageNamed:@"detail_sleep"];
    //     sleepImg.backgroundColor = [UIColor grayColor];
    
    [my_scrollview4S addSubview:stepImg];
    [my_scrollview4S addSubview:calorieImg];
    [my_scrollview4S addSubview:distanceImg];
    [my_scrollview4S addSubview:sleepImg];
    
//    [self addClickEventsFor4Img];
    
    //    wake_count_label = [[UILabel alloc]initWithFrame:CGRectMake(59, 444-15, 80, 34)];
    //    wake_count_label.textAlignment = NSTextAlignmentCenter;
    //    [my_scrollview4S addSubview:wake_count_label];
    //    efficiencyLabel = [[UILabel alloc]initWithFrame:CGRectMake(220, 444-15, 80, 34)];
    //    efficiencyLabel.textAlignment = NSTextAlignmentCenter;
    //    [my_scrollview4S addSubview:efficiencyLabel];
    
    
    firstLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 223, 50, 28)];
    firstLabel.text = @"0:00AM";
    firstLabel.font = [CommonColors activeTimeAxisLabelFont];
    firstLabel.backgroundColor = [UIColor clearColor];
    firstLabel.textAlignment = NSTextAlignmentLeft;
    [my_scrollview4S addSubview:firstLabel];
    
    leftMidLabel = [[UILabel alloc]initWithFrame:CGRectMake(5+70, 223, 50, 28)];
    leftMidLabel.text = @"6:00AM";
    leftMidLabel.font = [CommonColors activeTimeAxisLabelFont];
    leftMidLabel.backgroundColor = [UIColor clearColor];
    leftMidLabel.textAlignment = NSTextAlignmentLeft;
    [my_scrollview4S addSubview:leftMidLabel];
    
    
    secondLabel = [[UILabel alloc]initWithFrame:CGRectMake(140, 223, 50, 28)];
    secondLabel.font = [CommonColors activeTimeAxisLabelFont];
    secondLabel.text = @"12:00PM";
    secondLabel.textAlignment = NSTextAlignmentCenter;
    secondLabel.backgroundColor = [UIColor clearColor];
    [my_scrollview4S addSubview:secondLabel];
    
    rightMidLabel = [[UILabel alloc]initWithFrame:CGRectMake(140+70, 223, 50, 28)];
    rightMidLabel.text = @"6:00PM";
    rightMidLabel.font = [CommonColors activeTimeAxisLabelFont];
    rightMidLabel.backgroundColor = [UIColor clearColor];
    rightMidLabel.textAlignment = NSTextAlignmentLeft;
    [my_scrollview4S addSubview:rightMidLabel];
    
    
    thirdLabel = [[UILabel alloc]initWithFrame:CGRectMake(265, 223, 50, 28)];
    thirdLabel.font = [CommonColors activeTimeAxisLabelFont];
    thirdLabel.text = @"0:00AM";
    thirdLabel.textAlignment = NSTextAlignmentRight;
    thirdLabel.backgroundColor = [UIColor clearColor];
    
    [my_scrollview4S addSubview:thirdLabel];
    
    [self.view addSubview:my_scrollview4S];
    
    //
    database_path = [[CommonValues getDocumentPath] stringByAppendingPathComponent:DBNAME];
    NSLog(@"database_path = %@",database_path);
    //    tglibEKG = [[TGLibEKG alloc] init];
    //    tglibEKG.delegate = self;
    [[TGBleManager sharedTGBleManager] setDelegate:self];
    font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
    titleLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:23.0];
    titleLabel.textColor = [UIColor grayColor];
    dateLabel.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:20.0];
    dateLabel.backgroundColor = [UIColor clearColor];
    dateLabel.textColor = [UIColor grayColor];
    calorieLabel.font = font;
    distanceLabel.font = font;
    stepLabel.font = font;
    sleepLabel.font = font;
    sleepLabel.textColor = [CommonColors getSleepColor];
    stepLabel.textColor = [CommonColors getStepColor];
    calorieLabel.textColor = [CommonColors getCaloriesColor];
    distanceLabel.textColor = [CommonColors getDistanceColor];
    
    calorieLabel.text = @"0";
    distanceLabel.text = @"0";
    stepLabel.text = @"0";
    sleepLabel.text = @"0";
    
    sleepIndex = 0;
    // Do any additional setup after loading the view from its nib.
    
    sleepGraphDataSouceArr = [[NSMutableArray alloc] initWithCapacity:600];
    df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    local_solid_df = [[NSDateFormatter alloc] init];
    // NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
    
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [local_solid_df setTimeZone:gmt];
    //  local_solid_df
    
    
    
    dbTimeArray = [[NSMutableArray alloc]init];
    SleepArray = [[NSArray alloc]init];
    sleepPhaseArray = [[NSMutableArray alloc]init];
    
    sleepTimeArray = [[NSMutableArray alloc]init];
    SleepArray = [[NSMutableArray alloc]initWithObjects:@"50",@"216", @"195",@"163",@"85",@"208",@"300",@"63",@"75",@"106",@"70",@"217",@"293",@"86",@"114",@"76",@"69",nil];
    
    // different device different layout
    
    //    }
    
    dbTimeArray = [CommonValues getPedTimeArr:db];
    
    //    [self getTimeArr];
    sleepIndex = [CommonValues getActivityIndex];
    
    if (sleepIndex < dbTimeArray.count) {
        NSString *dateStr = [dbTimeArray objectAtIndex:sleepIndex];
        NSLog(@"dateStr = %@",dateStr);
        sleepGraphDataSouceArr =  [CommonValues activeModeArr:dateStr :db];
        //        NSLog(@"sleepGraphDataSouceArr = %@",sleepGraphDataSouceArr);
        [self getDataFromDB];
        [self updateLabelValue:[dbTimeArray objectAtIndex:sleepIndex]];
    }
    
    [self drawGraph];
    
    
    //test testSleepAnalysis
    if ([CommonValues startSleepAnalysis:sleepIndex:db] == -1) {
        [sleepGraphDataSouceArr removeAllObjects];
        
        //        [self drawGraph];
    }
    NSLog(@"setDelegate to Dashboard,prepare to autoSync");
    [[TGBleManager sharedTGBleManager]setDelegate:[CommonValues getDashboardID]];
    
    
    float heafview_height =  headView.frame.size.height;
    float heafview_width =  headView.frame.size.width;
    
    NSLog(@" heafview_height = %f,heafview_width = %f,%f,%f",heafview_height,heafview_width,headView.frame.origin.x,headView.frame.origin.y
          );
    
    //    [CommonValues activeModeDateArr:db];
    NSLog(@"active time viewdidload finished");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)drawGraph{
    barChart = [[CPTXYGraph alloc] initWithFrame:CGRectZero];
    CPTTheme *theme = [CPTTheme themeNamed:kCPTPlainWhiteTheme];
    [barChart applyTheme:theme];
    // CPTGraphHostingView *hostingView = (CPTGraphHostingView *)hv;
    CPTGraphHostingView *hostingView;
    
    //    if (screenHeight == 568) {
    //        hostingView = (CPTGraphHostingView *)hv;
    //    }
    //    else{
    hostingView = (CPTGraphHostingView *)hv_4S;
    //    }
    
    hostingView.hostedGraph = barChart;
    
    // Border
    barChart.plotAreaFrame.borderLineStyle = nil;
    barChart.plotAreaFrame.cornerRadius = 0.0f;
    
    // Paddings
    barChart.paddingLeft = 0.0f;
    barChart.paddingRight = 0.0f;
    barChart.paddingTop = 0.0f;
    barChart.paddingBottom = 0.0f;
    
    barChart.plotAreaFrame.paddingLeft = 0.0;
    barChart.plotAreaFrame.paddingTop = 20.0;
    barChart.plotAreaFrame.paddingRight = 0.0;
    barChart.plotAreaFrame.paddingBottom = 0.0;
    
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)barChart.defaultPlotSpace;
    plotSpace.allowsUserInteraction = NO;
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(0)length:CPTDecimalFromFloat(2.3)];
    plotSpace.globalYRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(0)length:CPTDecimalFromFloat(2.3)];
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(-1)length:CPTDecimalFromFloat(sleepGraphDataSouceArr.count)];
    
    
    //    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.0f) length:CPTDecimalFromFloat(150.0f)];
    //    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(-1.0f) length:CPTDecimalFromFloat(25.0f)];
    
    //	plotSpace.allowsUserInteraction = YES;
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)barChart.axisSet;
    CPTXYAxis *x = axisSet.xAxis;
    x.minorTickLineStyle = nil;
    
    x.labelingPolicy = CPTAxisLabelingPolicyNone;
    // x.axisConstraints = [CPTConstraints constraintWithRelativeOffset:0.2];
    
    CPTXYAxis *y = axisSet.yAxis;
    
    y.labelingPolicy = CPTAxisLabelingPolicyNone;
    y.axisLineStyle=CPTAxisLabelingPolicyNone;
    //add y label test start
    //y.labelingPolicy = CPTAxisLabelingPolicyNone;
    NSArray *customTickLocations2 = [NSArray arrayWithObjects:[NSDecimalNumber numberWithFloat:1],[NSDecimalNumber numberWithFloat:2],    nil];
    NSArray *xAxisLabels2         = [NSArray arrayWithObjects:@"WALK",@"RUN   ",nil];
    NSUInteger labelLocation1     = 0;
    NSMutableArray *customLabels2 = [NSMutableArray arrayWithCapacity:[xAxisLabels2 count]];
    
    
    
    NSLog(@"customTickLocations2.count = %lu",(unsigned long)customTickLocations2.count);
    // Axes
    CPTMutableTextStyle *mm = [[CPTMutableTextStyle alloc]init];
    // font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
    mm.fontName=DEFAULT_FONT_NAME;
    mm.color = [CPTColor grayColor];
    mm.fontSize = 15;
    for ( NSNumber *tickLocation2 in customTickLocations2 ) {
        CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithText:[xAxisLabels2 objectAtIndex:labelLocation1++] textStyle:mm];
        newLabel.tickLocation = [tickLocation2 decimalValue];
        //    newLabel.offset       = y.labelOffset +0.4 ;
        // newLabel.rotation     = M_PI / 4;
        [customLabels2 addObject:newLabel];
        
    }
    
    y.axisLabels = [NSSet setWithArray:customLabels2];
    y.axisConstraints = [CPTConstraints constraintWithRelativeOffset:0.15];
    y.delegate = self;
    
    CPTBarPlot *barPlot = [CPTBarPlot tubularBarPlotWithColor:[CPTColor colorWithComponentRed:2.0/255.0 green:190.0/255.0 blue:253.0/255.0 alpha:0.0] horizontalBars:NO];
    
    
    
    barPlot.dataSource = self;
    //  barPlot.baseValue = CPTDecimalFromString(@"0");
    //   barPlot.barOffset = CPTDecimalFromFloat(0.25f);
    //  barPlot.barCornerRadius = 2.0f;
    barPlot.identifier = @"Bar Plot 2";
    barPlot.fill = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:2.0/255.0 green:190.0/255.0 blue:253.0/255.0 alpha:1.0]];
    //    barPlot.barBasesVary = YES;
    barPlot.lineStyle = nil;
    //   CPTLineStyle *myLineStye = [CPTLineStyle lineStyle] ;
    //    myLineStye.lineColor = [];
    
    
    
    //   barPlot.lineStyle = myLineStye;
    
    //   myLineStye.lineColor = [CPTColor colorWithComponentRed:1.0 green:1.0 blue:1.0 alpha:1.0];
    
    //    barPlot.  barWidthsAreInViewCoordinates = NO;
    barPlot.barWidth = CPTDecimalFromDouble(1.1);
    //    barPlot.barWidthScale = 1.0;
    [barChart addPlot:barPlot toPlotSpace:plotSpace];
    
    
    // barPlot.  barWidthsAreInViewCoordinates = NO;
    
}


-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot {
    NSLog(@"numberOfRecordsForPlot sleepGraphDataSouceArr.count = %lu",(unsigned long)sleepGraphDataSouceArr.count);
    return sleepGraphDataSouceArr.count;
    
}
-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{
    NSDecimalNumber *num = nil;
    if ( [plot isKindOfClass:[CPTBarPlot class]] ) {
        switch ( fieldEnum ) {
            case CPTBarPlotFieldBarLocation:
                
                num = (NSDecimalNumber *)[NSDecimalNumber numberWithUnsignedInteger:index];
                //     NSLog(@"num = %@",num);
                break;
            case CPTBarPlotFieldBarTip:
            {
                int sleep_phase = [[sleepGraphDataSouceArr objectAtIndex:index ] intValue];
                
                num = (NSDecimalNumber *)[NSDecimalNumber numberWithUnsignedInt:sleep_phase];
                //  NSLog(@"SleepArray object %@",[sleepGraphDataSouceArr objectAtIndex:index ]);
                //                if (index> 500) {
                //                    CPTBarPlot *ss = (CPTBarPlot*)plot;
                //
                //                    ss.fill = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:2.0/255.0 green:190.0/255.0 blue:253.0/255.0 alpha:0.0]];
                //                }
            }
                break;
        }
    }
    
    return num;
}


- (NSArray *) barFillsForBarPlot:		(CPTBarPlot *) 	barPlot
                recordIndexRange:		(NSRange) 	indexRange{
    
    NSLog(@"barFillsForBarPlot---");
    //sleep_phase  4 kinds state
    CPTFill *walk_fill = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:255.0/255.0 green:192.0/255.0 blue:43.0/255.0 alpha:1.0]];
    
    CPTFill *run_fill = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:236.0/255.0 green:133.0/255.0 blue:6.0/255.0 alpha:1.0]];
    
    //    CPTFill *wake_fill = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:236.0/255.0 green:102.0/255.0 blue:72.0/255.0 alpha:1.0]];
    
    NSMutableArray *fill_arr = [[NSMutableArray alloc]initWithCapacity:sleepGraphDataSouceArr.count];
    if (sleepGraphDataSouceArr.count >0) {
        for (int i = 0; i< sleepGraphDataSouceArr.count; i++) {
            NSString *tmpSleepPhase = [NSString stringWithFormat:@"%@",[sleepGraphDataSouceArr objectAtIndex:i]];
            
            if ([tmpSleepPhase isEqualToString:@"1"] ) {
                [fill_arr insertObject:walk_fill atIndex:i];
            }
            else if ([tmpSleepPhase isEqualToString:@"2"] ) {
                [fill_arr insertObject:run_fill atIndex:i];
            }
            
            
            //            else if ([sleepGraphDataSouceArr[i] isEqualToString:@"4"] ) {
            //                [fill_arr insertObject:wake_fill atIndex:i];
            //            }
            else{
                [fill_arr insertObject:@"0" atIndex:i];
            }
            
        }
        
    }
    
    
    //    NSLog(@"fill_arr = %@",fill_arr);
    return fill_arr;
    
}

- (IBAction)back_click:(id)sender {
    NSLog(@"back_click--");
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)right_click:(id)sender {
    NSLog(@"right_click--");
    if (sleepIndex < 1) {
        return;
    }
    
    [[TGBleManager sharedTGBleManager]setDelegate:self];
    //reset ystLast
    ystLastSleepPhase = 1;
    ystLastSleepTime = @"";
    
    sleepIndex--;
    [CommonValues setActivityIndex:sleepIndex];
    [sleepTimeArray removeAllObjects];
    [sleepPhaseArray removeAllObjects];
    
    
    if (sleepIndex < dbTimeArray.count) {
        NSString *dateStr = [dbTimeArray objectAtIndex:sleepIndex];
        NSLog(@"dateStr = %@",dateStr);
        sleepGraphDataSouceArr =  [CommonValues activeModeArr:dateStr :db];
    }
    
    [self drawGraph];
    [self getDataFromDB];
    
    [self updateLabelValue:[dbTimeArray objectAtIndex:sleepIndex]];
    if ([CommonValues startSleepAnalysis:sleepIndex :db] == -1) {
        [sleepGraphDataSouceArr removeAllObjects];
        
    }
    
    //    [self refreshSleepTimeAxis];
    NSLog(@"setDelegate to Dashboard,prepare to autoSync");
    [[TGBleManager sharedTGBleManager]setDelegate:[CommonValues getDashboardID]];
}

- (IBAction)left_click:(id)sender {
    NSLog(@"left_click--");
    if (sleepIndex+1 >= dbTimeArray.count ) {
        return;
    }
    
    [[TGBleManager sharedTGBleManager]setDelegate:self];
    
    //reset ystLast
    ystLastSleepPhase = 1;
    ystLastSleepTime = @"";
    
    sleepIndex++;
    [CommonValues setActivityIndex:sleepIndex];
    
    [sleepTimeArray removeAllObjects];
    [sleepPhaseArray removeAllObjects];
    
    
    if (sleepIndex < dbTimeArray.count) {
        NSString *dateStr = [dbTimeArray objectAtIndex:sleepIndex];
        NSLog(@"dateStr = %@",dateStr);
        sleepGraphDataSouceArr =  [CommonValues activeModeArr:dateStr :db];
    }
    
    [self drawGraph];
    [self getDataFromDB];
    [self updateLabelValue:[dbTimeArray objectAtIndex:sleepIndex]];
    
    if ([CommonValues startSleepAnalysis:sleepIndex :db] == -1) {
        [sleepGraphDataSouceArr removeAllObjects];
        
    }
    //    [self refreshSleepTimeAxis];
    NSLog(@"setDelegate to Dashboard,prepare to autoSync");
    [[TGBleManager sharedTGBleManager]setDelegate:[CommonValues getDashboardID]];
}

- (void)sleepResults:(TGsleepResult) result
           startTime:(NSDate*) startTime
             endTime:(NSDate*) endTime
            duration:(int) duration  // in seconds
            preSleep:(int) preSleep // in seconds
            notSleep:(int) notSleep // in seconds active time
           deepSleep:(int) deepSleep // in seconds
          lightSleep:(int) lightSleep // in seconds
         wakeUpCount:(int) wakeUpCount // number of wake ups after 1st sleep
          totalSleep:(int) totalSleep
             preWake:(int) preWake // number of seconds active before the end time
          efficiency:(int) efficiency {
    NSString * message;
    NSLog(@"-241 sleep call back ------------");
    int sleepSeconds = 0;
    switch (result) {
            
        case TGsleepResultValid:
        {
            NSLog(@"sleepResults: TGsleepResultValid");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\nTotal Seconds: %d\nSeconds before Sleep: %d\nSeconds AWAKE: %d\nSeconds Deep Sleep: %d\nSeconds Light Sleep: %d\nWake Up Count: %d\nTotal Sleep Seconds: %d\nEfficiency: %d %%",
                       startTime,
                       endTime,
                       duration,
                       preSleep,
                       notSleep,
                       deepSleep,
                       lightSleep,
                       wakeUpCount,
                       totalSleep,
                       efficiency
                       ];
            sleepSeconds = lightSleep + deepSleep;
            
            
        }
            break;
            
        case TGsleepResultNegativeTime:
            NSLog(@"sleepResults: TGsleepResultNegativeTime");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\n\nStart Time is After the End Time",
                       startTime,
                       endTime
                       ];
            break;
            
        case TGsleepResultMoreThan48hours:
            NSLog(@"sleepResults: TGsleepResultMoreThan24hours");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\n\nToo Much Time, must be less than 24 hours",
                       startTime,
                       endTime
                       ];
            break;
        case TGsleepResultNoData:
            NSLog(@"sleepResults: TGsleepResultNoData");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\n\nNo Sleep Data Supplied",
                       startTime,
                       endTime
                       ];
            break;
            
        case TGsleepResultNotValid:
            NSLog(@"sleepResults: TGsleepResultNotValid");
        default:
            NSLog(@"sleepResults: default");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\n\nUnexpected Sleep Results\nResult Code: %lu",
                       startTime,
                       endTime,
                       (unsigned long)result
                       ];
            
            break;
    }
    
    
    
    //    dispatch_async(dispatch_get_main_queue(), ^{ [alert show]; }); // do all alerts on the main thread
    NSLog(@"message  = %@",message);
    totalEndTime = sleepSeconds;
    
    sleepLabel.text = [NSString stringWithFormat:@"%dh%dm",sleepSeconds/60,sleepSeconds%60];
    //    customerView3.progressImageView.progress = 1.0f*sleepSeconds/(goalSleep*60);
    
}


-(void)getDataFromDB{
    oneDayTotalStep = 0;
    oneDayTotalCalories = 0;
    oneDayTotalDistance = 0;
    
    NSMutableArray *mutArray = [[NSMutableArray alloc]initWithCapacity:24];
    for (int i = 0; i < 24; i++) {
        [mutArray addObject:[NSNumber numberWithInt:0]];
    }
    
    sqlite3_stmt *stmt;
    if (dbTimeArray.count <= 0) {
        return;
    }
    
    NSString *thisDate =   [dbTimeArray objectAtIndex:sleepIndex];
    NSString *quary3  = [NSString stringWithFormat:@"select datetime(datetime(time,'%ld hour','+0 minute'))  as time1,step,calories,distance from ped where time1 like '%@%%' order by time1 desc",(long)[[df timeZone] secondsFromGMT]/3600,thisDate];
    
    NSLog(@"查询这一天总的ped quary3 = %@",quary3);
    
    
    if(sqlite3_prepare_v2(db, [quary3 UTF8String], -1, &stmt, NULL) == SQLITE_OK) {
        while(sqlite3_step(stmt) == SQLITE_ROW) {
            const  unsigned  char *dateStr = sqlite3_column_text(stmt, 0);
            NSString *timeStr = [NSString stringWithFormat:@"%s",dateStr];
            
            NSString *subTimeStr =  [timeStr substringToIndex:13];
            //                    NSLog(@"subTimeStr = %@",subTimeStr);
            
            int temStep = sqlite3_column_int(stmt, 1);
            int temCalories = sqlite3_column_int(stmt, 2);
            
            int temDistance = sqlite3_column_int(stmt, 3);
            
            
            oneDayTotalStep +=  temStep;
            oneDayTotalCalories +=  temCalories;
            oneDayTotalDistance +=  temDistance;
            
            for (int  l = 0 ; l < 24; l++) {
                NSString *dateStr111 = [NSString stringWithFormat:@"%@ %02d",[dbTimeArray objectAtIndex:sleepIndex],l];
                if ([subTimeStr isEqualToString:dateStr111]) {
                    
                    NSInteger tmpStepValue = [[mutArray objectAtIndex:l] integerValue];
                    tmpStepValue += temStep;
                    [mutArray replaceObjectAtIndex:l withObject:[NSNumber numberWithInteger:tmpStepValue ] ];
                }
            }
            
            
            
        }
        sqlite3_finalize(stmt);
    }
    
    //    NSLog(@"mutArray = %@",mutArray);
    
    
    NSLog(@"oneDayTotalStep = %d,oneDayTotalCalories = %d,oneDayTotalDistance = %d",oneDayTotalStep,oneDayTotalCalories,oneDayTotalDistance);
    
    
    Step_count = oneDayTotalStep;
    Calories_count = oneDayTotalCalories;
    Distance_count = oneDayTotalDistance;
    
    
    
    
}
-(void)updateLabelValue :(NSString *)dateStr{
    
    stepLabel.text=[NSString stringWithFormat:@"%@steps",[StringOperation intToString:Step_count]];
    calorieLabel.text = [NSString stringWithFormat:@"%@kcal",[StringOperation intToString:Calories_count]];
    distanceLabel.text = [NSString stringWithFormat:@"%dm",Distance_count];
    //    title_value2.text=timeString;
    if ([CommonValues startSleepAnalysis:sleepIndex :db] == -1) {
        totalEndTime = 0;
        
    }
    
    int hour = totalEndTime/60;
    int min = totalEndTime%60;
    NSLog(@"hour = %d, min = %d",hour,min);
    sleepLabel.text = [NSString stringWithFormat:@"%dh%dm",hour,min];
    
    if (dbTimeArray.count > 0) {
        NSLog(@"[dbTimeArray objectAtIndex:timeDateIndex] = %@",[dbTimeArray objectAtIndex:sleepIndex]);
        NSString *timeString = @"";
        NSArray *time = [[dbTimeArray objectAtIndex:sleepIndex] componentsSeparatedByString:@"-"];
        if ([@"01" isEqualToString:[time objectAtIndex:1]]) {
            timeString=[NSString stringWithFormat:@"Jan %@",[time objectAtIndex:2]];
        }else if ([@"02" isEqualToString:[time objectAtIndex:1]]){
            timeString=[NSString stringWithFormat:@"Feb %@",[time objectAtIndex:2]];
        }else if ([@"03" isEqualToString:[time objectAtIndex:1]]){
            timeString=[NSString stringWithFormat:@"Mar %@",[time objectAtIndex:2]];
        }else if ([@"04" isEqualToString:[time objectAtIndex:1]]){
            timeString=[NSString stringWithFormat:@"Apr %@",[time objectAtIndex:2]];
        }else if ([@"05" isEqualToString:[time objectAtIndex:1]]){
            timeString=[NSString stringWithFormat:@"May %@",[time objectAtIndex:2]];
        }else if ([@"06" isEqualToString:[time objectAtIndex:1]]){
            timeString=[NSString stringWithFormat:@"Jun %@",[time objectAtIndex:2]];
        }else if ([@"07" isEqualToString:[time objectAtIndex:1]]){
            timeString=[NSString stringWithFormat:@"Jul %@",[time objectAtIndex:2]];
        }else if ([@"08" isEqualToString:[time objectAtIndex:1]]){
            timeString=[NSString stringWithFormat:@"Aug %@",[time objectAtIndex:2]];
        }else if ([@"09" isEqualToString:[time objectAtIndex:1]]){
            timeString=[NSString stringWithFormat:@"Sep %@",[time objectAtIndex:2]];
        }else if ([@"10" isEqualToString:[time objectAtIndex:1]]){
            timeString=[NSString stringWithFormat:@"Oct %@",[time objectAtIndex:2]];
        }else if ([@"11" isEqualToString:[time objectAtIndex:1]]){
            timeString=[NSString stringWithFormat:@"Nov %@",[time objectAtIndex:2]];
        }else if ([@"12" isEqualToString:[time objectAtIndex:1]]){
            timeString=[NSString stringWithFormat:@"Dec %@",[time objectAtIndex:2]];
        }
        
        dateLabel.text=timeString;
    }
    
    // 假如是最近的同步的那天，则使用currentCount 来代替 sum step/calories/distance
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *storedLatestStepStr =  [defaults objectForKey:STEP_LATESTDATE];
    NSString *storedLatestCalorieStr =  [defaults objectForKey:CALORIE_LATESTDATE];
    NSString *storedLatestDistanceStr =  [defaults objectForKey:DISTANCE_LATESTDATE];
    
    if (dbTimeArray.count > 0) {
        if (sleepIndex == 0) {
            if (storedLatestStepStr == NULL) {
                storedLatestStepStr = @"0";
            }
            stepLabel.text = [NSString stringWithFormat:@"%@steps",storedLatestStepStr];
        }
        else{
            stepLabel.text = [NSString stringWithFormat:@"%dsteps",Step_count];
        }
    }
    NSLog(@"333");
    
    if (dbTimeArray.count > 0) {
        if (sleepIndex == 0) {
            if (storedLatestCalorieStr == NULL) {
                storedLatestCalorieStr = @"0";
            }
            calorieLabel.text = [NSString stringWithFormat:@"%@kcal",storedLatestCalorieStr];
        }
        else{
            calorieLabel.text = [NSString stringWithFormat:@"%dkcal",Calories_count];
        }
    }
    
    if (dbTimeArray.count > 0) {
        if (sleepIndex == 0) {
            if (storedLatestDistanceStr == NULL) {
                storedLatestDistanceStr = @"0";
            }
            
            int storedDistance = [storedLatestDistanceStr intValue];
            distanceLabel.text = [CommonValues distanceStrFromDistanceCount:storedDistance];
            
        }
        else{
            
            distanceLabel.text =[CommonValues distanceStrFromDistanceCount:Distance_count];
            
        }
    }
    
    
    
}


-(void)addClickEventsFor4Img{
    NSLog(@"addClickEventsFor4Img--");
    calorieImg.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap1 =           [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(caloriesImgClick:)];
    [calorieImg addGestureRecognizer:singleTap1];
    
    distanceImg.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap2 =           [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(distanceImgClick:)];
    [distanceImg addGestureRecognizer:singleTap2];
    
    sleepImg.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap3 =           [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sleepImgClick:)];
    [sleepImg addGestureRecognizer:singleTap3];
    
    stepImg.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap4 =           [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(stepImgClick:)];
    [stepImg addGestureRecognizer:singleTap4];
    
    
    
}
-(void)stepImgClick:(id)sender{
    NSLog(@"step Img Click---");
    ActivityStep *acvc = [[ActivityStep alloc]init];
    [self presentViewController:acvc animated:YES completion:nil];
    
    
}

-(void)caloriesImgClick:(id)sender{
    NSLog(@"calories Img Click---");
    ActivityCalorieViewController *acvc = [[ActivityCalorieViewController alloc]init];
    [self presentViewController:acvc animated:YES completion:nil];
    
    
}


-(void)distanceImgClick:(id)sender{
    NSLog(@"distance Img Click---");
    ActivityDistanceViewController *acvc = [[ActivityDistanceViewController alloc]init];
    [self presentViewController:acvc animated:YES completion:nil];
}
-(void)sleepImgClick:(id)sender{
    NSLog(@"sleep Img Click---");
    if ([CommonValues getScreenHeight] == 568) {
        SleepViewController *cpvc = [[SleepViewController alloc]init] ;
        [self presentViewController:cpvc animated:YES
                         completion:nil];
        
    }
    
    else{
        Sleep_ViewController *cpvc = [[Sleep_ViewController alloc]init] ;
        [self presentViewController:cpvc animated:YES
                         completion:nil];
        
    }
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

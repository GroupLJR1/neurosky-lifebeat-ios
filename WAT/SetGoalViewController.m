//
//  SetGoalViewController.m
//  WAT
//
//  Created by neurosky on 1/16/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import "SetGoalViewController.h"
#import "Constant.h"
#import "CommonValues.h"
#import "StringOperation.h"

#import "TGBleEmulator.h"

@interface SetGoalViewController ()

@end

@implementation SetGoalViewController


@synthesize step_tv,calo_tv,dis_tv,sleep_tv,active_tv;


- ( void ) dataReceived: ( NSDictionary * ) data { return; }

- ( void ) touchtouchForDismiss { return; }


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    __LOG_METHOD_START;
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    __LOG_METHOD_END;
    return self;
}

- (void)viewDidLoad
{
    __LOG_METHOD_START;
    [super viewDidLoad];
    //fit for ios 6
    double version = [[UIDevice currentDevice].systemVersion doubleValue];
    NSLog(@"IOS version = %f",version);
    if (version < IOS7) {
        NSLog(@"iOS6-------");
        //        myWholeView.frame = CGRectMake(0, 0, 320, 568);
        offsetHeight = 0;
    }
    else{
        NSLog(@">=iOS7-------");
        offsetHeight = 20;
        myWholeGoalView.frame = CGRectMake(0, offsetHeight, 320, 568-20);
        //        [self.view addSubview:myWholeView];
        
    }
    
    
    devicesArray  = [[NSMutableArray alloc]init];
    mfgDataArray = [[NSMutableArray alloc]init];
    rssiArray = [[NSMutableArray alloc]init];
    candIDArray = [[NSMutableArray alloc]init];
    
    devicesIDArray  = [[NSMutableArray alloc]init];
    
    if ([CommonValues getAutoSyncingFlag]) {
        [ [TGBleManager sharedTGBleManager] setDelegate:[CommonValues getDashboardID]];
    }
    else{
        
        [ [TGBleManager sharedTGBleManager] setDelegate:self];
        
    }
    
    
    lostBeforeMainOperation = YES;
    
    if ([CommonValues getScreenHeight] == 568) {
        titleLabel.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
    }
    else
        titleLabel.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:25.0];
    
    // Do any additional setup after loading the view from its nib.
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    step =  [defaults integerForKey:USER_GOAL_STEP];
    distance =  [defaults integerForKey:USER_GOAL_DISTANCE];
    
    calorie =  [defaults integerForKey:USER_GOAL_CALORI];
    
    sleep_ = [defaults floatForKey:USER_GOAL_SLEEP];
    
    active_time =  [defaults floatForKey:USER_GOAL_ACTIVE_TIME];
    
    
    if (step) {
        step_tv.text = [NSString stringWithFormat:@"%ld",(long)step];
    }
    
    if (distance) {
        dis_tv.text = [NSString stringWithFormat:@"%ld",(long)distance];
    }
    
    if (calorie) {
        calo_tv.text =  [NSString stringWithFormat:@"%ld",(long)calorie];
    }
    
    if (sleep_) {
        sleep_tv.text =  [NSString stringWithFormat:@"%.1f",sleep_];
    }
    if (active_time) {
        active_tv.text = [NSString stringWithFormat:@"%.1f",active_time];
    }
    __LOG_METHOD_END;
}


- (void)checkBTStatusDone{
    __LOG_METHOD_START;
    if ([CommonValues getBluetoothOnFlag]) {
        //turn on the BT
        NSLog(@"already have  turn on the BT");
    }
    else{
        NSLog(@" BT  still not on state");
        [self showBTNotOnAlert];
    }
    __LOG_METHOD_END;
}

- (void)hasTokenTimeOutCancel{
    __LOG_METHOD_START;
    __LOG_METHOD_END;
}

- (void)hasTokenTimeOutRetry{
    __LOG_METHOD_START;
    [self saveBtnClick:nil];
    __LOG_METHOD_END;
}

- (void)noTokenTimeOutCancel{
    __LOG_METHOD_START;
    __LOG_METHOD_END;
}

- (void)noTokenTimeOutRetry{
    __LOG_METHOD_START;
    //Retry
    [self saveBtnClick:nil];
    __LOG_METHOD_END;
}

- (void)selectDeviceCancel{
    __LOG_METHOD_START;
    [self stopScan];
    [self resetDeviceListTemp];
    __LOG_METHOD_END;
}
- (void)selectDeviceOK{
    __LOG_METHOD_START;
    [self.view setUserInteractionEnabled:NO];
    [self stopScan];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSLog(@"need to save deviceName = %@",readyToSaveDeviceName);
    //if ([defaults stringForKey:@"DeviceName"] == nil) {
    [defaults setObject: readyToSaveDeviceName forKey:@"DeviceName"];
    [defaults setObject: readyToSaveDeviceID forKey:@"DeviceID"];
    [defaults synchronize];
    
    //candidate connect to the band;
    NSLog(@"call candidateConnect-----");
    [[TGBleManager sharedTGBleManager] candidateConnect:readyToSaveDeviceID];
    
    timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
    [SVProgressHUD showWithStatus:@" Connecting"];
    [self resetDeviceListTemp];
    __LOG_METHOD_END;
}

- (void)resetDeviceListTemp{
    __LOG_METHOD_START;
    [devicesArray removeAllObjects];
    [mfgDataArray removeAllObjects];
    [rssiArray removeAllObjects];
    [candIDArray removeAllObjects];
    [devicesIDArray removeAllObjects];
    [listView dismiss];
    //            selectDeviceBtnClickFlag = NO;
    [self.view setUserInteractionEnabled:YES];
    __LOG_METHOD_END;
}

- (void)bondAlertCancel{
    __LOG_METHOD_START;
    [[TGBleManager sharedTGBleManager] tryDisconnect];
    __LOG_METHOD_END;
}

- (void)bondAlertOK{
    __LOG_METHOD_START;
    [[TGBleManager sharedTGBleManager] takeBond];
    [SVProgressHUD showWithStatus:@" Setting Goals"];
    [self.view setUserInteractionEnabled:NO];
    __LOG_METHOD_END;
}

- (void)noTokenBondFailedCancel{
    __LOG_METHOD_START;
    [SVProgressHUD dismiss];
    [self.view setUserInteractionEnabled:YES];
    __LOG_METHOD_END;
}
- (void)noTokenBondFailedRetry{
    __LOG_METHOD_START;
    [SVProgressHUD dismiss];
    //connect previous band
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"DeviceName"];
    NSString *candId = [defaults objectForKey:@"DeviceID"];
    NSLog(@"stored candId = %@",candId);
    NSLog(@"call candidateConnect---");
    [[TGBleManager sharedTGBleManager]candidateConnect:candId];
    timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
    
    [SVProgressHUD showWithStatus:@" Connecting"];
    [self.view setUserInteractionEnabled:NO];
    __LOG_METHOD_END;
}

- (void)noTokenBondFailedTryAnother{
    __LOG_METHOD_START;
    [SVProgressHUD dismiss];
    //scan the nearby bands.
    [self saveBtnClick:nil];
    __LOG_METHOD_END;
}

- (void)hasTokenBondFailedCancel{
    __LOG_METHOD_START;
    [SVProgressHUD dismiss];
    [self.view setUserInteractionEnabled:YES];
    __LOG_METHOD_END;
}
- (void)hasTokenBondFailedRetry{
    __LOG_METHOD_START;
    [SVProgressHUD dismiss];
    //connect previous band
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults objectForKey:@"DeviceName"];
    NSString *candId = [defaults objectForKey:@"DeviceID"];
    NSLog(@"stored candId = %@",candId);
    if ([TGBleManager sharedTGBleManager].status == TGBleConnectedToBond) {
        //已有连接,直接做主要操作
        NSLog(@"已有连接,直接做主要操作");
        step = [step_tv.text  integerValue];
        [[TGBleManager  sharedTGBleManager] setGoalSteps:(int)step];
        [SVProgressHUD showWithStatus:@" Setting Goals"];
    }
    else{
        NSLog(@"没有连接");
        NSLog(@"call candidateConnect---");
        [[TGBleManager sharedTGBleManager]candidateConnect:candId];
        timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
        
        [SVProgressHUD showWithStatus:@" Connecting"];
    }
    [self.view setUserInteractionEnabled:NO];
    __LOG_METHOD_END;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    __LOG_METHOD_START;
    //NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSLog(@"buttonIndex:%ld", (long)buttonIndex);
    switch (alertView.tag) {
        case checkBTStatusTag:
            [self checkBTStatusDone];
            break;
            
            
        case   hasTokenTimeOutAlertTag:
            if (buttonIndex == 0) {
                [self hasTokenTimeOutRetry];
                
            }
            else{
                [self hasTokenTimeOutCancel];
            }
            break;
            
            
        case   noTokenTimeOutAlertTag:
            
            if (buttonIndex == 0) {
                [self noTokenTimeOutCancel];
            }
            
            else if (buttonIndex == 1)
            {
                [self noTokenTimeOutRetry];
            }
            
            break;
            
        case selectDeviceAlertTag:
        {
            
            if (buttonIndex == 0) {
                [self selectDeviceCancel];
            }
            
            else if(buttonIndex == 1){
                [self selectDeviceOK];
            }
            
        }
            break;
        case bondAlertTag:{
            
            if (buttonIndex == 0) {
                [self bondAlertCancel];
            }
            
            else if(buttonIndex == 1){
                [self bondAlertOK];
            }
        }
            
            break;
        case noTokenBondFailedTag:
        {
            if (buttonIndex == 0) {
                [self noTokenBondFailedCancel];
            }
            
            else if(buttonIndex == 1){
                [self noTokenBondFailedRetry];
            }
            else if(buttonIndex == 2){
                [self noTokenBondFailedTryAnother];
            }
            
        }
            break;
            
        case hasTokenBondFailedTag:
        {
            if (buttonIndex == 1) {
                [self hasTokenBondFailedRetry];
            }
            
            else{
                [self hasTokenBondFailedCancel];
                
            }
            
            
        }
            break;
            
        default:
            break;
    }
    __LOG_METHOD_END;
}

- (void) bleDidConnect {
    __LOG_METHOD_START;
    [timeOutTimer setFireDate:[NSDate distantFuture]];
    [timeOutTimer invalidate];
    NSData *sdkToken =  [[TGBleManager sharedTGBleManager] bondToken];
    
    if (sdkToken == NULL) {
        NSLog(@"call tryBond-----");
        [[TGBleManager sharedTGBleManager] tryBond];
        
    }
    
    else{
        NSString *sn =  [[TGBleManager sharedTGBleManager] hwSerialNumber];
        NSLog(@"sdkToken = %@, sn = %@",sdkToken,sn);
        NSLog(@"call adoptBond: serialNumber ");
        [[TGBleManager sharedTGBleManager] adoptBond:sdkToken serialNumber:sn];
        
    }
    
    lostBeforeMainOperation = NO;
    dispatch_async( dispatch_get_main_queue(), ^ {
        [SVProgressHUD showWithStatus:@" Bonding"];
    });
    __LOG_METHOD_END;
}

- (void) bleDidDisconnect {
    __LOG_METHOD_START;
    firstEnterCandiDel = NO;
    
    dispatch_async( dispatch_get_main_queue(), ^ {
        [SVProgressHUD dismiss];
        [self.view setUserInteractionEnabled:YES];
    });
    __LOG_METHOD_END;
}

- (void) bleLostConnect {
    __LOG_METHOD_START;
    /*
     firstEnterCandiDel = NO;
     
     dispatch_async( dispatch_get_main_queue(), ^ {
     [SVProgressHUD dismiss];
     [self.view setUserInteractionEnabled:YES];
     });
     */
    if ([timeOutTimer isValid]) {
        [timeOutTimer invalidate];
        NSLog(@"timeOutTimer invalidate----");
        
    }
    if (lostBeforeMainOperation ) {
        NSLog(@"want to set goal , lost connection before Main operation");
        NSData *token = [[TGBleManager sharedTGBleManager] bondToken];
        if (token == NULL) {
            if ([CommonValues iOSVersion] >= IOS8) {
                alertController = [UIAlertController alertControllerWithTitle:LostConnectTitle message:LostConnectMsg preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                    [self noTokenBondFailedCancel];
                    
                }];
                UIAlertAction *retryAction = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self noTokenBondFailedRetry];
                }];
                UIAlertAction *tryAnotherAction = [UIAlertAction actionWithTitle:@"Try another band" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self noTokenBondFailedTryAnother];
                }];
                [alertController addAction:cancelAction];
                [alertController addAction:retryAction];
                [alertController addAction:tryAnotherAction];
                [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
            }
            else{
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:LostConnectTitle message:LostConnectMsg
                                                              delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry",@"Try another band", nil];
                alert.tag = noTokenBondFailedTag;
                dispatch_async( dispatch_get_main_queue(), ^ {
                    [alert show];
                });
            }
        }
        
        else{
            if ([CommonValues iOSVersion] >= IOS8) {
                alertController = [ UIAlertController alertControllerWithTitle: LostConnectTitle
                                                                       message: LostConnectMsg
                                                                preferredStyle: UIAlertControllerStyleAlert
                                  ];
                UIAlertAction *cancelAction = [ UIAlertAction actionWithTitle: @"Cancel"
                                                                        style: UIAlertActionStyleCancel
                                                                      handler: ^(UIAlertAction * action) {
                    [self hasTokenBondFailedCancel];
                    
                }];
                UIAlertAction *okAction = [ UIAlertAction actionWithTitle: @"Retry"
                                                                    style: UIAlertActionStyleDefault
                                                                  handler: ^(UIAlertAction *action) {
                    [self hasTokenBondFailedRetry];
                }];
                [alertController addAction:cancelAction];
                [alertController addAction:okAction];
                [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
            }
            else{
                UIAlertView *alert_hasToken = [ [ UIAlertView alloc ] initWithTitle: LostConnectTitle
                                                                            message: LostConnectMsg
                                                                           delegate: self
                                                                  cancelButtonTitle: @"Cancel"
                                                                  otherButtonTitles: @"Retry", nil
                                              ];
                
                alert_hasToken.tag = hasTokenBondFailedTag;
                dispatch_async( dispatch_get_main_queue(), ^ {
                    //                [self.view setUserInteractionEnabled:YES];
                    //                [SVProgressHUD dismiss];
                    [alert_hasToken show];
                });
            }
            
        }
        
        
        
    }
    
    else{
        dispatch_async( dispatch_get_main_queue(), ^ {
            
            [SVProgressHUD dismiss];
            [self.view setUserInteractionEnabled: YES];
        });
    }
    __LOG_METHOD_END;
}

- (void) bleDidAbortConnect {
    __LOG_METHOD_START;
    __LOG_METHOD_END;
}
- (void) candidateFound:(NSString *)devName rssi:(NSNumber *)rssi mfgID:(NSString *)mfgID candidateID:(NSString *)candidateID
{
    __LOG_METHOD_START;
    NSString* message = [NSString stringWithFormat:
                         @"Device Name: %@\nMfg Data: %@\nID: %@\nRSSI value: %@",
                         devName,
                         mfgID,
                         candidateID,
                         rssi
                         ];
    NSLog(@"SELECT DEVICE candidateFound delegate: %@", message);
    
    
    if ([candIDArray containsObject:candidateID]) {
        __LOG_METHOD_END;
        return;
    }
    if (!firstEnterCandiDel) {
        NSLog(@"first enter candidate Found");
        firstEnterCandiDel = YES;
        [timeOutTimer setFireDate:[NSDate distantFuture]];
        [timeOutTimer invalidate];
        [SVProgressHUD dismiss];
        
        listView = [[ZSYPopoverListView alloc] initWithFrame:CGRectMake(0, 0, 300, 400)];
        
        listView.tagert =self;
        listView.stopEBLScan =@selector(stopScan);
        
        
        listView.datasource = self;
        listView.delegate = self;
        showListTimer =  [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(delay2ShowList) userInfo:nil repeats:NO];
        
        //        [listView show];
        
    }
    
    
    [devicesArray addObject:devName];
    [mfgDataArray addObject:mfgID];
    [rssiArray addObject:rssi];
    [candIDArray addObject:candidateID];
    [devicesIDArray addObject:candidateID];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [listView reload];
        
        
    });
    __LOG_METHOD_END;
}

- (void) bleDidSendAlarmGoals:(TGsendResult)result {
    __LOG_METHOD_START;
    NSString * message;
    
    lostBeforeMainOperation  = NO;
    
    [CommonValues setHasTaskFlag:NO];
    
    [ [TGBleManager sharedTGBleManager] setDelegate:[CommonValues getDashboardID]];
    
    
    if (result == TGSendSuccess) {
        message = @"ALARM CFG Succesful";
        
        if ([CommonValues isBackgroundFlag]) {
            NSLog(@"set goal 完成，且在后台，应断开连接");
            NSLog(@"call tryDisconnect ------");
            [[TGBleManager sharedTGBleManager] tryDisconnect];
        }
        
        if ([CommonValues getSaveBtnClcikFlag]) {
            [CommonValues setSaveBtnClcikFlag:NO];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            //save goal
            step = [step_tv.text  integerValue];
            calorie = [calo_tv.text integerValue];
            distance = [dis_tv.text integerValue];
            sleep_ = [sleep_tv.text floatValue];
            active_time = [active_tv.text floatValue];
            NSLog(@"set step to UserDefault---- = %ld",(long)step);
            [defaults setInteger:step forKey:USER_GOAL_STEP];
            [defaults setInteger:calorie forKey:USER_GOAL_CALORI];
            [defaults setInteger:distance forKey:USER_GOAL_DISTANCE];
            [defaults setFloat:sleep_  forKey:USER_GOAL_SLEEP];
            [defaults setFloat:active_time  forKey:USER_GOAL_ACTIVE_TIME];
            [defaults synchronize];
            
            
            dispatch_async( dispatch_get_main_queue(), ^ {
                [SVProgressHUD showSuccessWithStatus:@"Set Goal completed."];
                
            });
            
        }
        
    }
    else if (result == TGSendFailedNoAcknowledgement) {
        message = @"ALARM CFG time out, no ack";
        
    }
    else if (result == TGSendFailedDisconnected) {
        message = @"ALARM CFG disconnected";
    }
    else {
        message = [NSString stringWithFormat:@"ALARM CFG Unexpected Error, code: %d", (int) result];
    }
    
    NSLog(@"ALARM CFG Result Code  %lu",(unsigned long)result);
    NSLog(@"ALARM CFG Result msg  %@",message);
    //    NSThread *delay2DisThread = [[NSThread alloc]initWithTarget:self selector:@selector(delayToDisconnect) object:nil];
    //    [delay2DisThread start];
    
    if (result != TGSendSuccess){
        if ([CommonValues iOSVersion] >= IOS8) {
            alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Set Goal failed." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                [self hasTokenBondFailedCancel];
                
            }];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self hasTokenBondFailedRetry];
            }];
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
        }
        else{
            UIAlertView *alert_hasToken = [[UIAlertView alloc]initWithTitle:@"Error " message:@"Set Goal failed." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
            
            alert_hasToken.tag = hasTokenBondFailedTag;
            
            dispatch_async(dispatch_get_main_queue(), ^{ [alert_hasToken show];
            }); // do all alerts on the main thread
        }
    }
    
    dispatch_async( dispatch_get_main_queue(), ^ {
        [self.view setUserInteractionEnabled:YES];
    });
    __LOG_METHOD_END;
}

-(void)delayToDisconnect{
    __LOG_METHOD_START;
    usleep(1000000);
    [[TGBleManager sharedTGBleManager]tryDisconnect];
    __LOG_METHOD_END;
}

- (void) bleDidSendUserConfig:(TGsendResult)result {
    __LOG_METHOD_START;
    NSString * message;
    if (result == TGSendSuccess) {
        message = @"USER CFG Succesful";
        
    }
    else if (result == TGSendFailedNoAcknowledgement) {
        message = @"USER CFG time out, no ack";
    }
    else if (result == TGSendFailedDisconnected) {
        message = @"USER CFG disconnected";
    }
    else {
        message = [NSString stringWithFormat:@"USER CFG Unexpected Error, code: %d", (int) result];
    }
    NSLog(@"USER CFG Result Code  %lu",(unsigned long)result);
    NSLog(@"USER CFG Result msg  %@",message);
    __LOG_METHOD_END;
}

- (void)potentialBond:(NSString*) code sn: (NSString*) sn devName:(NSString*) devName
{
    __LOG_METHOD_START;
    NSString* displayMessage = [NSString stringWithFormat:
                                @"%@%@?",DigitCodeConfirmMsg,
                                code];
    
    dispatch_async( dispatch_get_main_queue(), ^ {
        [SVProgressHUD dismiss];
    });
    dispatch_async( dispatch_get_main_queue(), ^ {
        
    if ([CommonValues iOSVersion] >= IOS8) {
        alertController = [UIAlertController alertControllerWithTitle:@"" message:displayMessage preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
            [self bondAlertCancel];
            
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self bondAlertOK];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
    }
    else{
        UIAlertView *bondDialog = [[UIAlertView alloc]initWithTitle:@"" message:displayMessage delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
            bondDialog.tag = bondAlertTag;
            [bondDialog show];
           }
        
    });
    __LOG_METHOD_END;
}


- (void) bleDidBond:(TGbondResult)result {
    __LOG_METHOD_START;
    NSString * message = @"";
    NSString * title = @"";

    switch ( result )
        {
        case TGbondResultTokenAccepted:
            message = @"Bond Succesful";
            break;

        case TGbondResultTokenReleased:
            title = TokenReleasedTitle;
            message = TokenReleasedMsg;
            break;

        case TGbondResultErrorBondedNoMatch:
            title = CommunicationErrorTitle;
            message = BondedNoMatchMsg;
            break;

        case TGbondResultErrorBadTokenFormat:
            title = BondRejectedTitle;
            message = BadTokenFormatMsg;
            break;

        case TGbondResultErrorTimeOut:
            title = TimeOutTitle;
            message = TimeOutMsg;
            break;

        case TGbondResultErrorNoConnection:
            title = NoConnectionTitle;
            message = NoConnectionMsg;
            break;
    
        case TGbondResultErrorReadTimeOut:
            title = ReadBackTimeOutTitle;
            message = ReadTimeOutMsg;
            break;
    
    
        case TGbondResultErrorReadBackTimeOut:
            title = ReadBackTimeOutTitle;
            message = ReadBackTimeOutMsg;
            break;
    
        case TGbondResultErrorWriteFail:
            title = WriteFailTitle;
            message = WriteFailMsg;
            break;

        case TGbondResultErrorTargetIsAlreadyBonded:
            title = BondRejectedTitle;
            message = TargetIsAlreadyBondedMsg;
            break;

        case TGbondAppErrorNoPotentialBondDelegate:
            title  = BondAppErrorTitle;
            message = NoPotentialBondDelegateMsg;
            break;
    
        case TGbondResultErrorTargetHasWrongSN:
            title  = CommunicationErrorTitle;
            message = TargetHasWrongSNMSg;
            break;

        case TGbondResultErrorPairingRejected:
            title  = PairingErrorTitle;
            message = PairingRejectedMsg;
            break;

        case TGbondResultErrorSecurityMismatch:
            title  = PairingErrorTitle;
            message = SecurityMismatchMsg;
            break;

        default:
            title  = UnkonwnBondErrorTitle;
            message = [ NSString stringWithFormat: @"Bonding Error, code: %d", (int)result ];
            break;
        }

    NSLog( @"BONDIING RESULT Code %lu", (unsigned long)result );
    NSLog( @"BONDIING RESULT msg %@", message );
    
    // set bond flag,check it when do every action
    if ( result == TGbondResultTokenAccepted ) {
        
        NSLog( @"Bond Succesful,start to erase/setTimer/setAlarm" );
        step = [ step_tv.text  integerValue ];
        [ [ TGBleManager  sharedTGBleManager ] setGoalSteps: (int)step ];
        
        dispatch_async( dispatch_get_main_queue(), ^ {
            [ SVProgressHUD showWithStatus: @" Setting Goals" ];
        } );
        
    }
    else if (result == TGbondResultTokenReleased) {
        NSLog(@"此处不应该被执行到！！！");
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:@" OK ", nil];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view setUserInteractionEnabled:YES];
            [SVProgressHUD dismiss];
            [alert show];
            
        });
        
    }
    else{
        
        lostBeforeMainOperation =  NO;
        
        NSLog(@"call tryDisconnect----");
        [[TGBleManager sharedTGBleManager]tryDisconnect];
        NSData *token = [[TGBleManager sharedTGBleManager] bondToken];
        if (token == NULL) {
            if ([CommonValues iOSVersion] >= IOS8) {
                alertController = [ UIAlertController alertControllerWithTitle: title
                                                                       message: message
                                                                preferredStyle: UIAlertControllerStyleAlert
                                  ];
                UIAlertAction * const cancelAction = [ UIAlertAction actionWithTitle: CANCEL
                                                                               style: UIAlertActionStyleCancel
                                                                             handler: ^(UIAlertAction * action) {
                                                                                 [self noTokenBondFailedCancel];
                                                                                 }
                                                     ];
                UIAlertAction *const retryAction = [ UIAlertAction actionWithTitle: RETRY
                                                                             style: UIAlertActionStyleDefault
                                                                           handler: ^(UIAlertAction *action) {
                                                                               [self noTokenBondFailedRetry];
                                                                               }
                                                   ];
                UIAlertAction * const tryAnotherAction = [ UIAlertAction actionWithTitle: TRYANOTHERBAND
                                                                                   style: UIAlertActionStyleDefault
                                                                                 handler: ^(UIAlertAction *action) {
                                                                                 [self noTokenBondFailedTryAnother];
                                                                                 }
                                                         ];
                [ alertController addAction: cancelAction ];
                [ alertController addAction: retryAction ];
                [ alertController addAction: tryAnotherAction ];
                [ self performSelector: @selector( showAlertController )
                            withObject: nil
                            afterDelay: 0
                ];
            }
            else{
                UIAlertView * const alert = [ [ UIAlertView alloc ] initWithTitle: title
                                                                          message: message
                                                                         delegate: self
                                                                cancelButtonTitle: CANCEL
                                                                otherButtonTitles: RETRY, TRYANOTHERBAND, nil
                                            ];
                alert.tag = noTokenBondFailedTag;
                dispatch_async( dispatch_get_main_queue(), ^ {
                    [alert show];
                });
            }
        }
        
        else{
            if ([CommonValues iOSVersion] >= IOS8) {
                alertController = [ UIAlertController alertControllerWithTitle: title
                                                                       message: message
                                                                preferredStyle: UIAlertControllerStyleAlert
                                  ];
                UIAlertAction *cancelAction = [ UIAlertAction actionWithTitle: CANCEL
                                                                        style: UIAlertActionStyleCancel
                                                                      handler: ^(UIAlertAction * action) {
                                                                          [self hasTokenBondFailedCancel];
                                                                      }];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle: RETRY
                                                                   style: UIAlertActionStyleDefault
                                                                 handler: ^(UIAlertAction *action) {
                                                                     [self hasTokenBondFailedRetry];
                                                                 }];
                [alertController addAction:cancelAction];
                [alertController addAction:okAction];
                [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
            }
            else{
                UIAlertView *alert_hasToken = [ [ UIAlertView alloc ] initWithTitle: title
                                                                            message: message
                                                                           delegate: self
                                                                  cancelButtonTitle: CANCEL
                                                                  otherButtonTitles: RETRY, nil
                                              ];
                
                alert_hasToken.tag = hasTokenBondFailedTag;
                dispatch_async( dispatch_get_main_queue(), ^ {
                    [alert_hasToken show];
                });
                
            }
        }
    }
    __LOG_METHOD_END;
}

-(void) batteryLevel:(int)level {
    __LOG_METHOD_START;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:level  forKey:LATEST_BATTERY];
    [defaults synchronize];
    __LOG_METHOD_END;
}

- (void) exceptionMessage:(TGBleExceptionEvent)eventType {
    __LOG_METHOD_START;
    NSString * message;
    
    if (eventType == TGBleHistoryCorruptErased) {
        message = @"Flash Memory Corrupted, Will Be erased, suspect battery has been exhausted";
    }
    else if (eventType == TGBleConfigurationModeCanNotBeChanged) {
        message = @"Exception Message - Ble Connection Mode CAN NOT be changed";
    }
    else if (eventType == TGBleUserAgeHasBecomeNegative_BirthDateIsNOTcorrect) {
        message = @"User Age has become negative, correct the birth date";
    }
    else if (eventType == TGBleUserAgeIsTooLarge_BirthDateIsNOTcorrect) {
        message = @"User Age is TOO OLD, check the birth date";
    }
    else if (eventType == TGBleUserBirthDateRejected_AgeOutOfRange) {
        message = @"User Age is out of RANGE, corret the birth date";
    }
    else if (eventType == TGBleFailedOtherOperationInProgress) {
        message = @"Exception Message - Another Operation is Already in Progress";
    }
    else if (eventType == TGBleFailedSecurityNotInplace) {
        message = @"Exception Message - Security is NOT In Place";
    }
    else if (eventType == TGBleTestingEvent) {
        message = @"Exception Message - This is only a Test";
    }
    else if (eventType == TGBleReInitializedNeedAlarmAndGoalCheck) {
        message = @"Exception Message - Band lost power or was MFG reset, check your alarm and goal settings";
    }
    else if (eventType == TGBleStepGoalRejected_OutOfRange) {
        message = @"Exception Message - Step Goal Settings rejected -- OUT OF RANGE";
    }
    else if (eventType == TGBleCurrentCountRequestTimedOut) {
        message = @"Exception Message - Can Not get Current Count values\nBand not responding";
    }
    else if (eventType == TGBleConnectFailedSuspectKeyMismatch) {
        message = @"Exception Message - Band appears to be paired, possible encryption key mismatch, hold side button for 10 seconds to reboot";
    }
    else if (eventType == TGBleNonRepeatingAlarmMayBeStaleCheckAlarmConfiguration) {
        message = @"Exception Message - A Non-repeating Alarm may be stale, check your alarm settings";
    }
    else {
        message = [NSString stringWithFormat:@"Exception Detected, code: %lu", (unsigned long)eventType];
    }
    
    //    NSLog(@"message = %@",message);
    
    NSLog(@"exceptionMessage =  %@",message);
    __LOG_METHOD_END;
}



#pragma marks
#pragma tableview datasource methods
#pragma mark -
- (NSInteger)popoverListView:(ZSYPopoverListView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return devicesArray.count;
}

- (UITableViewCell *)popoverListView:(ZSYPopoverListView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    __LOG_METHOD_START;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString * savedDeviceName = [defaults stringForKey:@"DeviceName"];
    NSLog(@"NSUserDefaults deviceName = %@",savedDeviceName);
    
    static NSString *identifier = @"identifier";
    UITableViewCell *cell = [tableView dequeueReusablePopoverCellWithIdentifier:identifier];
    if (nil == cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] ;
    }
    
    
    NSString *tmpStr = [NSString stringWithFormat:@"%@:%@",[ mfgDataArray objectAtIndex:indexPath.row],[ devicesArray objectAtIndex:indexPath.row]];
    //cell.textLabel.text =[ devicesArray objectAtIndex:indexPath.row];
    cell.textLabel.text = tmpStr;
    cell.textLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:25.0];
    
    if ([[ devicesArray objectAtIndex:indexPath.row] isEqualToString:savedDeviceName]) {
        // many devices have the same name
        //
        //cell.imageView.image = [UIImage imageNamed:@"fs_main_login_selected.png"];
    }
    
    __LOG_METHOD_END;
    return cell;
}

- (void)popoverListView:(ZSYPopoverListView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    __LOG_METHOD_START;
    /*UITableViewCell *cell =*/ [tableView popoverCellForRowAtIndexPath:indexPath];
    //cell.imageView.image = [UIImage imageNamed:@"fs_main_login_normal.png"];
    NSLog(@"deselect:%ld", (long)indexPath.row);
    __LOG_METHOD_END;
}

- (void)popoverListView:(ZSYPopoverListView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    __LOG_METHOD_START;
    self.selectedIndexPath = indexPath;
    /*UITableViewCell *cell =*/ [tableView popoverCellForRowAtIndexPath:indexPath];
    //cell.imageView.image = [UIImage imageNamed:@"fs_main_login_selected.png"];
    NSLog(@"select:%ld", (long)indexPath.row);
    //  NSString *deviceName = [NSString stringWithFormat:@"Wat%d", indexPath.row];
    
    NSString *deviceName = [devicesArray objectAtIndex:indexPath.row];
    NSString *mfgData = [mfgDataArray objectAtIndex:indexPath.row];
    //NSString *rssi = [rssiArray objectAtIndex:indexPath.row];
    readyToSaveDeviceName = [devicesArray objectAtIndex:indexPath.row];
    readyToSaveDeviceID = [devicesIDArray objectAtIndex:indexPath.row];
    NSString *msg = [NSString stringWithFormat:@"Are you sure you would like to connect to %@ %@  ?",deviceName, mfgData];
    [listView dismiss];
    if ([CommonValues iOSVersion] >= IOS8) {
        alertController = [UIAlertController alertControllerWithTitle:msg message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:CANCEL style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
            [self selectDeviceCancel];
            
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:OK style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self selectDeviceOK];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
    }
    
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:msg message:nil delegate:self cancelButtonTitle:CANCEL otherButtonTitles:OK, nil];
        alert.tag   = selectDeviceAlertTag;
        [alert show];
    }
    __LOG_METHOD_END;
}

- (void)didReceiveMemoryWarning
{
    __LOG_METHOD_START;
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    __LOG_METHOD_END;
}

- (IBAction)backClick:(id)sender {
    __LOG_METHOD_START;
    [CommonValues setSaveBtnClcikFlag:NO];
    [self dismissViewControllerAnimated:YES completion:nil];
    __LOG_METHOD_END;
}
- (NSUInteger)supportedInterfaceOrientations

{
    return UIInterfaceOrientationMaskPortrait;
    
}

-(BOOL)shouldAutorotate{
    
    return NO;
}

- (IBAction)bgTouch:(id)sender {
    __LOG_METHOD_START;
    [step_tv resignFirstResponder];
    [dis_tv resignFirstResponder];
    [calo_tv resignFirstResponder];
    [sleep_tv resignFirstResponder];
    [active_tv resignFirstResponder];
    __LOG_METHOD_END;
}

- (IBAction)saveBtnClick:(id)sender {
    __LOG_METHOD_START;

    if ([CommonValues getAutoSyncingFlag]) {
        [[CommonValues autoSyncingAlert] show];
        __LOG_METHOD_END;
        return;
    }
    
    if (![CommonValues getBluetoothOnFlag]) {
        NSLog(@"BT OFF, can't go on candidateConnect:candId");
        [self showBTNotOnAlert];
        __LOG_METHOD_END;
        return;
    }
    
    
    
    // test  check input valid
    if (![self inputValidCheck]) {
        __LOG_METHOD_END;
        return;
    }
    
    [CommonValues setSaveBtnClcikFlag:YES];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if([self goalValueChangedCheck]){
        
        [CommonValues setHasTaskFlag:YES];
        
        [ [TGBleManager sharedTGBleManager] setDelegate:self];
        [self.view setUserInteractionEnabled:NO];
        //goal changed
        
        if ([TGBleManager sharedTGBleManager].status == TGBleConnectedToBond) {
            //已有连接,直接做主要操作
            NSLog(@"已有连接,直接做主要操作");
            step = [step_tv.text  integerValue];
            [[TGBleManager  sharedTGBleManager] setGoalSteps:(int)step];
            [SVProgressHUD showWithStatus:@" Setting Goals"];
            
            
        }
        else{
            NSLog(@"没有连接");
            NSData *token = [[TGBleManager sharedTGBleManager]bondToken];
            if (token != NULL) {
                NSLog(@"bond_state");
                // connect
                NSString *candId = [defaults objectForKey:@"DeviceID"];
                NSLog(@"call candidateConnect:candId-----");
                [[TGBleManager sharedTGBleManager] candidateConnect:candId];
                [SVProgressHUD showWithStatus:@" Connecting"];
                [self.view setUserInteractionEnabled:NO];
            }
            
            else {
                NSLog(@"not_bond_state");
                // show devices list
                [self showDevicesList];
            }
            
            timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
            
        }
    }
    
    else{
        //        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Nothing Changed,There's No Need To Set." message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@" OK ", nil];
        //        [alert show];
        //        return;
        
    }
    __LOG_METHOD_END;
}

- (BOOL)inputValidCheck{
    __LOG_METHOD_START;
    NSString *sleepStr = [sleep_tv text];
    NSString *stepStr = [step_tv text];
    NSString *caloStr = [calo_tv text];
    NSString *disStr = [dis_tv text];
    NSString *activeStr = [active_tv text];
    /*float*/ sleep_ = [sleepStr floatValue];
    /*float*/ active_time = [activeStr floatValue];
    /*int*/ step = [stepStr intValue];
    int calo = [caloStr intValue];
    int dis = [disStr intValue];
    
    NSString *message = @"";
    
    if ( [StringOperation inputValidation3:sleepStr] || [StringOperation inputValidation2:sleepStr] ) {
        NSLog(@"sleepStr 是非负整数 或 一位小数");
        if (sleep_ > 24.0 || sleep_ <= 0.0) {
            NSLog(@"sleep should be between 0.1  and 24.0");
            message = @"sleep should be between 0.1   and 24.0 ";
            [CommonValues showAlert:message];
            return  NO;
        }
    }
    else{
        message = @"sleep should be between 0.1  and 24.0 ";
        [CommonValues showAlert:message];
        return NO;
    }
    
    if ([StringOperation inputValidation3:stepStr]) {
        if (step > 65535 || step <= 0) {
            NSLog(@"step should be between 1  and 65535");
            message = @"step should be between 1  and 65535 ";
            [CommonValues showAlert:message];
            return  NO;
        }
        
    }
    else{
        message = @"step should be between 1  and 65535 ";
        [CommonValues showAlert:message];
        return NO;
        
    }
    
    if ([StringOperation inputValidation3:caloStr]) {
        if (calo > 65535 || calo <= 0) {
            NSLog(@"calories should be between 1  and 65535");
            message = @"calories should be between 1  and 65535 ";
            [CommonValues showAlert:message];
            return  NO;
        }
        
    }
    else{
        message = @"calories should be between 1  and 65535 ";
        [CommonValues showAlert:message];
        return NO;
        
    }
    
    
    if ([StringOperation inputValidation3:disStr]) {
        if (dis > 39000 || dis <= 0) {
            NSLog(@"distance should be between 1  and 39000");
            message = @"distance should be between 1  and 39000 ";
            [CommonValues showAlert:message];
            return  NO;
        }
        
    }
    else{
        message = @"distance should be between 1  and 39000 ";
        [CommonValues showAlert:message];
        return NO;
        
    }
    
    if ( [StringOperation inputValidation3:activeStr] || [StringOperation inputValidation2:activeStr] ) {
        NSLog(@"activeStr 是非负整数 或 一位小数");
        if (active_time > 24.0 || active_time <= 0.0) {
            NSLog(@"activeStr should be between 0.1  and 24.0");
            message = @"active time should be between 0.1   and 24.0 ";
            [CommonValues showAlert:message];
            return  NO;
        }
    }
    else{
        message = @"active time should be between 0.1  and 24.0 ";
        [CommonValues showAlert:message];
        return NO;
    }
    NSLog(@"sleepStr = %@ ,stepStr = %@ ,caloStr = %@ ,disStr = %@,activeStr = %@, sleep = %f ,step = %ld ,calo = %d ,dis = %d ,active = %f",sleepStr,stepStr,caloStr,disStr,activeStr, sleep_,(long)step,calo,dis,active_time);
    __LOG_METHOD_END;
    return  YES;
}
- (BOOL) goalValueChangedCheck{
    __LOG_METHOD_START;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSLog(@"stored sleep = %f",[defaults floatForKey:USER_GOAL_SLEEP]);
    NSLog(@"stored step = %ld",(long)[defaults integerForKey:USER_GOAL_STEP]);
    NSLog(@"stored distance = %ld",(long)[defaults integerForKey:USER_GOAL_DISTANCE]);
    NSLog(@"stored calo = %ld",(long)[defaults integerForKey:USER_GOAL_CALORI]);
    NSLog(@"stored active time = %f",[defaults floatForKey:USER_GOAL_ACTIVE_TIME]);
    
    NSInteger sdkStep = [[TGBleManager sharedTGBleManager] goalSteps];
    NSLog(@"sdkStep = %ld",(long)sdkStep);
    
    
    if ([sleep_tv.text floatValue] == [defaults floatForKey:USER_GOAL_SLEEP]
        && [step_tv.text integerValue ] == [defaults integerForKey:USER_GOAL_STEP]
        && [dis_tv.text integerValue ] == [defaults integerForKey:USER_GOAL_DISTANCE]
        && [calo_tv.text integerValue ] == [defaults integerForKey:USER_GOAL_CALORI]
        &&[active_tv.text floatValue] == [defaults floatForKey:USER_GOAL_ACTIVE_TIME])
    {
        
        if ([CommonValues iOSVersion] >= IOS8) {
            alertController = [UIAlertController alertControllerWithTitle:NOTHINGCHANGED message:nil preferredStyle:UIAlertControllerStyleAlert];
            [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NOTHINGCHANGED message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@" OK ", nil];
            [alert show];
        }
        __LOG_METHOD_END;
        return NO;
        
    }
    __LOG_METHOD_END;
    return  YES;
}


-(void)showDevicesList{
    __LOG_METHOD_START;
    if (![CommonValues getBluetoothOnFlag]) {
        NSLog(@"BT OFF, can't go on candidateScaning");
        [self showBTNotOnAlert];
        __LOG_METHOD_END;
        return;
    }
    [SVProgressHUD showWithStatus:@" Search bands "];
    [self.view setUserInteractionEnabled:NO];
    NSLog(@" call  candidateScan:nameList");
    [[TGBleManager sharedTGBleManager] candidateScan:[CommonValues bandDeviceArray]];
    NSLog(@"Menu item  devicesArray count = %lu",(unsigned long)devicesArray.count);
    __LOG_METHOD_END;
}

-(void)stopScan{
    __LOG_METHOD_START;
    NSLog(@"touch listview other place to stopScan ");
    [self.view setUserInteractionEnabled:YES];
    NSLog(@"call candidateStopScan----");
    [[TGBleManager sharedTGBleManager]candidateStopScan];
    [devicesArray removeAllObjects];
    [mfgDataArray removeAllObjects];
    [rssiArray removeAllObjects];
    [candIDArray removeAllObjects];
    [devicesIDArray removeAllObjects];
    // cancel delayed time out alert.
    [timeOutTimer setFireDate:[NSDate distantFuture]];
    [timeOutTimer invalidate];
    firstEnterCandiDel = NO;
    __LOG_METHOD_END;
}

-(void)showTimeOutAlert{
    __LOG_METHOD_START;
    [SVProgressHUD dismiss];
    [self stopScan];
    // delay to show alert ,prevent the confligs from SVProgressHUD dismiss
    [self performSelector:@selector(delay2ShowTimeOutAlert) withObject:nil afterDelay:0.5];
    __LOG_METHOD_END;
}

- (void)delay2ShowTimeOutAlert{
    __LOG_METHOD_START;
    if ([CommonValues iOSVersion] >= IOS8) {
        UIAlertAction *cancelAction;
        UIAlertAction *okAction;
        if ([[TGBleManager sharedTGBleManager]bondToken] != NULL ) {
            alertController = [UIAlertController alertControllerWithTitle:ConnectTimeOutTitle message:ConnectTimeOutMsg preferredStyle:UIAlertControllerStyleAlert];
            cancelAction = [UIAlertAction actionWithTitle:CANCEL style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                [self hasTokenTimeOutCancel];
                
            }];
            okAction = [UIAlertAction actionWithTitle:RETRY style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self hasTokenTimeOutRetry];
            }];
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
        }
        else{
            alertController = [UIAlertController alertControllerWithTitle:ScanTimeOutTitle message:ScanTimeOutMsg preferredStyle:UIAlertControllerStyleAlert];
            cancelAction = [UIAlertAction actionWithTitle:CANCEL style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                [self noTokenTimeOutCancel];
                
            }];
            okAction = [UIAlertAction actionWithTitle:RETRY style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self noTokenTimeOutRetry];
            }];
            
        }
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
        
        
    }
    else{
        UIAlertView *timeOutAlert;
        if ([[TGBleManager sharedTGBleManager]bondToken] != NULL ) {
            timeOutAlert =  [[UIAlertView alloc]initWithTitle:ConnectTimeOutTitle message:ConnectTimeOutMsg delegate:self cancelButtonTitle:nil otherButtonTitles:RETRY, CANCEL, nil];
            timeOutAlert.tag = hasTokenTimeOutAlertTag;
            
            
        }
        else{
            timeOutAlert = [[UIAlertView alloc]initWithTitle:ScanTimeOutTitle message:ScanTimeOutMsg delegate:self cancelButtonTitle:CANCEL otherButtonTitles:RETRY, nil];
            timeOutAlert.tag = noTokenTimeOutAlertTag;
        }
        
        [timeOutAlert show];
    }
    NSLog(@"call tryDisconnect----");
    [[TGBleManager sharedTGBleManager] tryDisconnect];
    __LOG_METHOD_END;
}

-(void)delay2ShowList{
    __LOG_METHOD_START;
    [listView show];
    __LOG_METHOD_END;
}

-(void)showBTNotOnAlert {
    __LOG_METHOD_START;
    if ([CommonValues iOSVersion] >= IOS8) {
        alertController = [UIAlertController alertControllerWithTitle:BluetoothIsOffTitle message:BluetoothIsOffMsg preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self checkBTStatusDone];
        }];
        [alertController addAction:okAction];
        [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
    }
    else{
        UIAlertView *alert =[ [UIAlertView alloc]initWithTitle:BluetoothIsOffTitle message:BluetoothIsOffMsg delegate:self cancelButtonTitle:nil otherButtonTitles:@"Done", nil];
        alert.tag = checkBTStatusTag;
        [alert show];
    }
    __LOG_METHOD_END;
}

- (void)showAlertController{
    __LOG_METHOD_START;
    [self presentViewController:alertController animated:YES completion:nil];
    __LOG_METHOD_END;
}

@end

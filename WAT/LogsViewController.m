//
//  LogsViewController.m
//  WAT
//
//  Created by neurosky on 5/28/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import "LogsViewController.h"
#import "SVProgressHUD.h"
#import "CommonValues.h"
#import "Constant.h"

#import "TGBleEmulator.h"


@interface LogsViewController ()

@end

@implementation LogsViewController

- ( void ) dataReceived: ( NSDictionary * ) data { return; }

- ( void ) touchtouchForDismiss { return; }


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    myWholeView.frame = CGRectMake(0, [CommonValues offsetHeight4DiffiOS], 320, 568-20);
    
    devicesArray  = [[NSMutableArray alloc]init];
    mfgDataArray = [[NSMutableArray alloc]init];
    rssiArray = [[NSMutableArray alloc]init];
    candIDArray = [[NSMutableArray alloc]init];
    
    devicesIDArray  = [[NSMutableArray alloc]init];
    //    longStr = [[NSMutableString alloc]init];
    [ [TGBleManager sharedTGBleManager] setDelegate:self];
    logs_title_label.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
    logs_tv.text = @"";
    
    [self.view setUserInteractionEnabled:NO];
    [SVProgressHUD showWithStatus:@"Loading..."];
    NSThread *loadThread = [[NSThread alloc] initWithTarget:self selector:@selector(loadFile) object:nil];
    [loadThread start];
    
}

-(void)loadFile{
//    NSString *documentDir2 = [[CommonValues getDocumentPath] stringByAppendingPathComponent:@"TG_log/Console"];
    
    NSArray *arr =  [self getFileArr:@"TG_log/Console"];
    NSLog(@"arr = %@",arr);
    
    latestLogName = @"";
#if( 0 )
    for (int i = 0; i<arr.count; i++) {
        if ([arr[i] compare:latestLogName  options:NSLiteralSearch] > 0) {
            latestLogName = arr[i];
        }
    }
    NSLog(@"latestLogName = %@",latestLogName);
    if (latestLogName.length > 0) {
        content=[NSString stringWithContentsOfFile:[documentDir2 stringByAppendingPathComponent:latestLogName] encoding:NSUTF8StringEncoding error:nil];
        //    NSLog(@"文件读取成功: %@",content);
        latestLogPath = [documentDir2 stringByAppendingPathComponent:latestLogName];
        NSLog(@"latestLogPath = %@",latestLogPath);
    }
#endif
    [self performSelectorOnMainThread:@selector(dismissWindow) withObject:nil waitUntilDone:YES];
    
    
}

-(void)dismissWindow{
    [SVProgressHUD dismiss];
    logs_tv.text = content;
    [logs_tv scrollRangeToVisible:NSMakeRange(logs_tv.text.length, 1)];
    [self.view setUserInteractionEnabled:YES];
}

-(NSArray *)getFileArr:(NSString *)type{
    NSFileManager *fileManager1 = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documents = [paths objectAtIndex:0];
    
    NSString *documentDir3 = [documents stringByAppendingPathComponent:type];
    
    NSError *error = nil;
    NSArray * const fileList = /* [[NSArray alloc] init];
    
    fileList = */ [fileManager1 contentsOfDirectoryAtPath:documentDir3 error:&error];
    
    NSLog(@"Every Thing in the dir:%@",fileList);
    return fileList;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnClick:(id)sender {
    NSLog(@"Logs backBtnClick---");
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)eraseBtnClick:(id)sender {
    NSLog(@"eraseClick---");
    if ([CommonValues iOSVersion] >= IOS8) {
        alertController = [UIAlertController alertControllerWithTitle:@"Are you sure ?" message:@"Erase data will delete ..." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
            [self eraseAlertCancel];
            
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self eraseAlertOK];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Are you sure ?" message:@"Erase data will delete ..." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        alert.tag = ERASEDATATAG;
        [alert show];
        
    }
}

- (IBAction)releaseBtnClick:(id)sender {
    NSLog(@"releaseClcik---");
    if ([CommonValues iOSVersion] >= IOS8) {
        alertController = [UIAlertController alertControllerWithTitle:@"Are you sure ?" message:@"Release bond ...." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
            [self releaseBondCancel];
            
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self releaseBondOK];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
        
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Are you sure ?" message:@"Release bond ...." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        alert.tag = RELEASEBONDTAG;
        [alert show];
    }
}

- (IBAction)resetBandClick:(id)sender {
    
    NSLog(@"resetBandClick---");
    
    if ([CommonValues iOSVersion] >= IOS8) {
        alertController = [UIAlertController alertControllerWithTitle:@"Are you sure ?" message:@"eset band to shipping mode..." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
            [self shippingModeCancel];
            
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self shippingModeOK];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
        
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Are you sure ?" message:@"Reset band to shipping mode..." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        alert.tag = SHIPPINGMODE;
        [alert show];
        
    }
    
}

- (IBAction)vibrationOffClick:(id)sender {
    NSLog(@"vibrationOffClick ----");
    wantSetVibrationOff = YES;
    wantSetVibrationOn = NO;
    if ([TGBleManager sharedTGBleManager].status == TGBleConnectedToBond) {
        //已有连接,直接做主要操作
        NSLog(@"已有连接,直接做主要操作");
        
        NSLog(@"call disableVBM ---");
        [[TGBleManager  sharedTGBleManager] disableVBM ];
        
        [SVProgressHUD showSuccessWithStatus:@"Vibration Off ..."];
        
        [CommonValues setHasTaskFlag:NO];
        
    }
    else{
        NSLog(@"没有连接");
        
        //                [TGBleManager sharedTGBleManager];
        NSData *token = [[TGBleManager sharedTGBleManager]bondToken];
        if (token != NULL) {
            NSLog(@"bond_state");
            [self.view setUserInteractionEnabled:NO];
            
            // connect
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *candId = [defaults objectForKey:@"DeviceID"];
            NSLog(@"call candidateConnect:candId-----");
            [[TGBleManager sharedTGBleManager] candidateConnect:candId];
            [SVProgressHUD showWithStatus:@" Connecting"];
        }
        
        else {
            NSLog(@"not_bond_state");
            // show devices list
            [self showDevicesList];
        }
        timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
        
    }
    
}

- (IBAction)vibrationOnClick:(id)sender {
    NSLog(@"vibrationOnClick ----");
    [CommonValues setHasTaskFlag:YES];
    wantSetVibrationOn = YES;
    wantSetVibrationOff = NO;
    
    if ([TGBleManager sharedTGBleManager].status == TGBleConnectedToBond) {
        //已有连接,直接做主要操作
        NSLog(@"已有连接,直接做主要操作");
        
        NSLog(@"call enableVBM ---");
        [[TGBleManager  sharedTGBleManager] enableVBM ];
        [SVProgressHUD showSuccessWithStatus:@" Vibration On ..."];
        [CommonValues setHasTaskFlag:NO];
        
    }
    else{
        NSLog(@"没有连接");
        
        //                [TGBleManager sharedTGBleManager];
        NSData *token = [[TGBleManager sharedTGBleManager]bondToken];
        if (token != NULL) {
            NSLog(@"bond_state");
            [self.view setUserInteractionEnabled:NO];
            
            // connect
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *candId = [defaults objectForKey:@"DeviceID"];
            NSLog(@"call candidateConnect:candId-----");
            [[TGBleManager sharedTGBleManager] candidateConnect:candId];
            
            
            [SVProgressHUD showWithStatus:@" Connecting"];
            
        }
        
        else {
            NSLog(@"not_bond_state");
            // show devices list
            [self showDevicesList];
        }
        timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
        
    }
}

- (IBAction)touchClick:(id)sender {
    NSLog(@"touchClick-----");
    
    
    if (touchDownFlag) {
        if ([TGBleManager sharedTGBleManager].status == TGBleConnectedToBond) {
            touchDownFlag = NO;
            [[TGBleManager sharedTGBleManager] disableVBM];
        }
    }
    
}

- (IBAction)dragExitClick:(id)sender {
    NSLog(@"dragExitClick----");
}

- (IBAction)dragEnterClick:(id)sender {
    NSLog(@"dragEnterClick----");
}

- (IBAction)touchDragInside:(id)sender {
    //     NSLog(@"touchDragInside----");
}

- (IBAction)touchDragOutside:(id)sender {
    //    NSLog(@"touchDragOutside----");
    
}

- (IBAction)touchDown:(id)sender {
    NSLog(@"touchDown----");
    
    if ([TGBleManager sharedTGBleManager].status == TGBleConnectedToBond) {
        touchDownFlag = YES;
        [[TGBleManager sharedTGBleManager] enableVBM];
    }
    else{
        if ([CommonValues iOSVersion] >= IOS8) {
            alertController = [UIAlertController alertControllerWithTitle:@"Disconnected" message:CONNECT_BAND_FIRST preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@" OK " style:UIAlertActionStyleCancel handler:nil];
            [alertController addAction:okAction];
            [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
        }
        else{
            UIAlertView  *notConnected = [[UIAlertView alloc]initWithTitle:@"Disconnected" message:CONNECT_BAND_FIRST delegate:nil cancelButtonTitle:nil otherButtonTitles:@" OK ", nil];
            
            [notConnected show];
        }
        return;
    }
}

- (IBAction)touchCancel:(id)sender {
    NSLog(@"touchCancel----");
}

- (IBAction)editingBegin:(id)sender {
    NSLog(@"editingBegin----");
    
}

- (IBAction)editingEnd:(id)sender {
    NSLog(@"editingEnd----");
    
}

- (IBAction)sendEmailClick:(id)sender {
    NSLog(@"sendEmailClick---");
    
    if ([CommonValues connectedToNetwork]) {
        NSLog(@"have net ");
        [self showPicker];
    }
    else
    {
        NSLog(@"NO net ");
        if ([CommonValues iOSVersion] >= IOS8) {
            alertController = [UIAlertController alertControllerWithTitle:INTERNET_DISCONNECTED message:CONNECT_BAND_FIRST preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@" OK " style:UIAlertActionStyleCancel handler:nil];
            [alertController addAction:okAction];
            [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
        }
        else{
            
            UIAlertView  *noNetWork = [[UIAlertView alloc]initWithTitle:INTERNET_DISCONNECTED message:CONNECT_BAND_FIRST delegate:nil cancelButtonTitle:nil otherButtonTitles:@" OK ", nil];
            
            [noNetWork show];
        }
        return;
    }
    
    
    
}

- (void) bleDidConnect {
    NSLog(@"delegate bleDidConnect---");
    if (timeOutTimer .isValid) {
        [timeOutTimer invalidate];
    }
    
    NSData *sdkToken =  [[TGBleManager sharedTGBleManager] bondToken];
    
    if (sdkToken == NULL) {
        NSLog(@"call tryBond-----");
        [[TGBleManager sharedTGBleManager] tryBond];
        
    }
    
    else{
        
        NSString *sn =  [[TGBleManager sharedTGBleManager] hwSerialNumber];
        NSLog(@"sdkToken = %@, sn = %@",sdkToken,sn);
        
        NSLog(@"call adoptBond: serialNumber ");
        [[TGBleManager sharedTGBleManager] adoptBond:sdkToken serialNumber:sn];
        
    }
    
    
    
    dispatch_async( dispatch_get_main_queue(), ^ {
        [SVProgressHUD showWithStatus:@" Bonding"];
    });
}

- (void) bleDidDisconnect {
    NSLog(@"delegate bleDidDisconnect---");
    firstEnterCandiDel = NO;
    isResetingBand = NO;
    isErasing = NO;
    isSettingTimer = NO;
    isSettingGoal = NO;
    isSettingAlarm = NO;
    //    [SVProgressHUD dismiss];
    
    [self.view setUserInteractionEnabled:YES];
}
- (NSUInteger)supportedInterfaceOrientations

{
    return UIInterfaceOrientationMaskPortrait;
    
}

-(BOOL)shouldAutorotate{
    
    return NO;
}
- (void) bleLostConnect {
    
    NSLog(@"delegate bleLostConnect---------");
    if ([timeOutTimer isValid]) {
        [timeOutTimer invalidate];
        NSLog(@"timeOutTimer invalidate----");
        
    }
    
    dispatch_async( dispatch_get_main_queue(), ^ {
        [SVProgressHUD dismiss];
    });
    
    if (isResetingBand) {
        isResetingBand = NO;
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:RESET_BAND_SUCCESS message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@" OK ", nil];
        alertController = [UIAlertController alertControllerWithTitle:RESET_BAND_SUCCESS message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@" OK " style:UIAlertActionStyleCancel handler:nil];
        [alertController addAction:okAction];
        
        dispatch_async( dispatch_get_main_queue(), ^ {
            if ([CommonValues iOSVersion] >= IOS8) {
                [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
            }
            else{
                [alert show];
            }
        });
        
        
    }
    
    isResetingBand = NO;
    
    isErasing = NO;
    isSettingTimer = NO;
    isSettingGoal = NO;
    isSettingAlarm = NO;
    //    [SVProgressHUD dismiss];
    
    [self.view setUserInteractionEnabled:YES];
    
}

- (void) bleDidAbortConnect {
    NSLog(@"delegate bleDidAbortConnect----");
    
}
-(void) eraseComplete:(TGeraseResult)result {
    NSLog(@">>>>>>>-----Recorded Data Erase complete: %lu", (unsigned long)result);
    isErasing = NO;
    
    [CommonValues setHasTaskFlag:NO];
    
    if (result == 0) {
        NSLog(@"Erased successfully");
        if ([CommonValues isBackgroundFlag]) {
            NSLog(@"sync 完成，且在后台，应断开连接");
            NSLog(@"call tryDisconnect ------");
            [[TGBleManager sharedTGBleManager] tryDisconnect];
        }
        
        dispatch_async( dispatch_get_main_queue(), ^ {
            //            [alert show];
            
            [SVProgressHUD showSuccessWithStatus:@"Erase completed"];
            
        });
    }
    else{
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:ERASE_FAILED message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@" OK ", nil];
        alertController = [UIAlertController alertControllerWithTitle:ERASE_FAILED message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@" OK " style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
            [self selectDeviceAlertCancel];
            
        }];
        [alertController addAction:okAction];
        
        dispatch_async( dispatch_get_main_queue(), ^ {
            if ([CommonValues iOSVersion] >= IOS8) {
                [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
            }
            else{
                [alert show];
            }
        });
    }
    //    NSLog(@"call SDK tryDisconnect ---");
    dispatch_async( dispatch_get_main_queue(), ^ {
        [self.view setUserInteractionEnabled:YES];
    });
}
- (void) candidateFound:(NSString *)devName rssi:(NSNumber *)rssi mfgID:(NSString *)mfgID candidateID:(NSString *)candidateID
{
    NSString* message = [NSString stringWithFormat:
                         @"Device Name: %@\nMfg Data: %@\nID: %@\nRSSI value: %@",
                         devName,
                         mfgID,
                         candidateID,
                         rssi
                         ];
    NSLog(@"SELECT DEVICE candidateFound delegate: %@", message);
    if ([candIDArray containsObject:candidateID]) {
        NSLog(@"SDK appear the repeated candidateID in candidateFound delegate!!!");
        return;
    }
    else{
        if (!firstEnterCandiDel) {
            NSLog(@"first enter candidate Found");
            firstEnterCandiDel = YES;
            if ([timeOutTimer isValid]) {
                [timeOutTimer invalidate];
                NSLog(@"timeOutTimer invalidate----");
                
            };
            [SVProgressHUD dismiss];
            
            listView = [[ZSYPopoverListView alloc] initWithFrame:CGRectMake(0, 0, 300, 400)];
            
            listView.tagert =self;
            listView.stopEBLScan =@selector(stopScan);
            
            
            listView.datasource = self;
            listView.delegate = self;
            showListTimer =  [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(delay2ShowList) userInfo:nil repeats:NO];
            
            //        [listView show];
            
        }
        
        
        haveDeviceFlag = YES;
        
        [devicesArray addObject:devName];
        [mfgDataArray addObject:mfgID];
        [rssiArray addObject:rssi];
        [candIDArray addObject:candidateID];
        [devicesIDArray addObject:candidateID];
        
    }
    // sort  devicesIDArray,mfgDataArray,devicesArray   according to rssi
    //  数值从大到小   信号从强到弱
    for (int i = 0; i <rssiArray.count ; i++) {
        NSInteger const rssi =   [[rssiArray objectAtIndex:i]integerValue];
        NSLog(@"rssi = %ld",(long)rssi);
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [listView reload];
    });
    
}

-(void)delay2ShowList{
    NSLog(@"showTimeOutAlerttest---");
    [listView show];
    
}
-(void)showTimeOutAlert{
    
    [self stopScan];
    if ([CommonValues getAutoSyncingFlag]) {
        NSLog(@"auto sync connect time out ,failed ");
    }
    else{
        [SVProgressHUD dismiss];
        
        // delay to show alert ,prevent the confligs from SVProgressHUD dismiss
        [self performSelector:@selector(delay2ShowTimeOutAlert) withObject:nil afterDelay:0.5];
    }
    
}
-(void)delay2ShowTimeOutAlert{
    
    NSLog(@"SHOW  TIME OUT  ALERT !!!!");
    UIAlertView *timeOutAlert;
    
    if ([[TGBleManager sharedTGBleManager]bondToken] != NULL ) {
        timeOutAlert =  [[UIAlertView alloc]initWithTitle:ConnectTimeOutTitle message:ConnectTimeOutMsg delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        timeOutAlert.tag = hasTokenTimeOutAlertTag;
        
    }
    else{
        timeOutAlert = [[UIAlertView alloc]initWithTitle:ScanTimeOutTitle message:ScanTimeOutMsg delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        timeOutAlert.tag = noTokenTimeOutAlertTag;
    }
    
    [timeOutAlert show];
    NSLog(@"call tryDisconnect----");
    [[TGBleManager sharedTGBleManager] tryDisconnect];
    
}

- (void)checkBTStatusDone{
    NSLog(@"set bt done click ");
    if ([CommonValues getBluetoothOnFlag]) {
        //turn on the BT
        NSLog(@"already have  turn on the BT");
    }
    else{
        NSLog(@" BT  still not on state");
        [self showBTNotOnAlert];
    }
}

- (void)releaseBondCancel{
    NSLog(@"RELEASEBOND cancel Button");
}
- (void)releaseBondOK{
    NSLog(@"RELEASEBOND ok Button");
    [[TGBleManager sharedTGBleManager] releaseBond];
}

- (void)eraseAlertCancel{
    NSLog(@"ERASEDATA cancel Button");
}
- (void)eraseAlertOK{
    NSLog(@"ERASEDATA ok Button");
    isErasing = YES;
    
    [CommonValues setHasTaskFlag:YES];
    
    if ([TGBleManager sharedTGBleManager].status == TGBleConnectedToBond) {
        //已有连接,直接做主要操作
        NSLog(@"已有连接,直接做主要操作");
        
        NSLog(@"call tryEraseData ---");
        [[TGBleManager  sharedTGBleManager] tryEraseData];
        [SVProgressHUD showWithStatus:@" Erasing"];
        
    }
    else{
        NSLog(@"没有连接");
        
        //                [TGBleManager sharedTGBleManager];
        NSData *token = [[TGBleManager sharedTGBleManager]bondToken];
        if (token != NULL) {
            NSLog(@"bond_state");
            [self.view setUserInteractionEnabled:NO];
            
            // connect
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *candId = [defaults objectForKey:@"DeviceID"];
            NSLog(@"call candidateConnect:candId-----");
            [[TGBleManager sharedTGBleManager] candidateConnect:candId];
            
            
            [SVProgressHUD showWithStatus:@" Connecting"];
        }
        
        else {
            NSLog(@"not_bond_state");
            // show devices list
            [self showDevicesList];
        }
        timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
        
    }
    
}

- (void)shippingModeCancel{
    NSLog(@"SHIPPINGMODE cancel Button");
    
}
- (void)shippingModeOK{
    NSLog(@"SHIPPINGMODE ok Button");
    isResetingBand  = YES;
    [CommonValues setHasTaskFlag:YES];
    
    if ([TGBleManager sharedTGBleManager].status == TGBleConnectedToBond) {
        //已有连接,直接做主要操作
        NSLog(@"已有连接,直接做主要操作");
        
        NSLog(@"call mfg_support MFG_ShipModeConfig ---");
        //        [[TGBleManager  sharedTGBleManager] resetBand];
        [[TGBleManager sharedTGBleManager] mfg_support:YES arg2:MFG_ShipModeConfig];
        //                    [SVProgressHUD showSuccessWithStatus:@"call resetBand..."];
        
    }
    else{
        NSLog(@"没有连接");
        
        //                [TGBleManager sharedTGBleManager];
        NSData *token = [[TGBleManager sharedTGBleManager]bondToken];
        if (token != NULL) {
            NSLog(@"bond_state");
            [self.view setUserInteractionEnabled:NO];
            
            // connect
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *candId = [defaults objectForKey:@"DeviceID"];
            NSLog(@"call candidateConnect:candId-----");
            [[TGBleManager sharedTGBleManager] candidateConnect:candId];
            
            [SVProgressHUD showWithStatus:@" Connecting"];
        }
        
        else {
            NSLog(@"not_bond_state");
            // show devices list
            [self showDevicesList];
        }
        //                     timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
        timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
        
    }
}

- (void)selectDeviceAlertCancel{
    NSLog(@"selectDeviceAlertTag cancelButton");
    [self stopScan];
}
- (void)selectDeviceAlertOK{
    NSLog(@"selectDeviceAlertTag OK Button");
    if (isSettingGoal) {
        //                    [SVProgressHUD showWithStatus:@"Setting Goal..."];
        [self.view setUserInteractionEnabled:NO];
        
    }
    else if (isSettingAlarm) {
        //                    [SVProgressHUD showWithStatus:@"Setting Alarm..."];
        [self.view setUserInteractionEnabled:NO];
        
    }
    else if (isSettingTimer) {
        //                    [SVProgressHUD showWithStatus:@"Setting Timer..."];
        [self.view setUserInteractionEnabled:NO];
        
    }
    else if (isErasing) {
        //                    [SVProgressHUD showWithStatus:@"Erasing..."];
        
    }
    else{
        NSLog(@"SVProgressHUD unknown status");
    }
    
    NSLog(@"call  candidateStopScan------");
    [[TGBleManager sharedTGBleManager] candidateStopScan];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSLog(@"need to save deviceName = %@",readyToSaveDeviceName);
    //if ([defaults stringForKey:@"DeviceName"] == nil) {
    [defaults setObject: readyToSaveDeviceName forKey:@"DeviceName"];
    [defaults setObject: readyToSaveDeviceID forKey:@"DeviceID"];
    [defaults synchronize];
    
    //candidate connect to the band;
    [[TGBleManager sharedTGBleManager] candidateConnect:readyToSaveDeviceID];
    [SVProgressHUD showWithStatus:@" Connecting"];
    timeOutTimer =  [NSTimer scheduledTimerWithTimeInterval:[CommonValues getTimeOutTime] target:self selector:@selector(showTimeOutAlert) userInfo:nil repeats:NO];
}
- (void)resetDeviceTmpList{
    [devicesArray removeAllObjects];
    [mfgDataArray removeAllObjects];
    [rssiArray removeAllObjects];
    [candIDArray removeAllObjects];
    [devicesIDArray removeAllObjects];
    [listView dismiss];
}

- (void)bondAlertCancel{
    NSLog(@"bondAlert cancelButton");
    NSLog(@"call tryDisconnect------");
    [[TGBleManager sharedTGBleManager] tryDisconnect];
    [SVProgressHUD dismiss];
    [self.view setUserInteractionEnabled:YES];
}
- (void)bondAlertOK{
    NSLog(@"bondAlert OK Button");
    NSLog(@"call takeBond------");
    [[TGBleManager sharedTGBleManager] takeBond];
    [SVProgressHUD showWithStatus:@" Bonding"];
    [self.view setUserInteractionEnabled:NO];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    /*NSUserDefaults *defaults =*/ [NSUserDefaults standardUserDefaults];
    NSLog(@"buttonIndex:%ld", (long)buttonIndex);
    switch (alertView.tag) {
        case checkBTStatusTag:
            [self checkBTStatusDone];
            break;
            
        case RELEASEBONDTAG:
        {
            if (buttonIndex == 0) {
                [self releaseBondCancel];
            }
            else if (buttonIndex == 1) {
                [self releaseBondOK];
                
            }
        }
            
            break;
        case ERASEDATATAG:{
            if (buttonIndex == 0) {
                [self eraseAlertCancel];
                
            }
            else if (buttonIndex == 1) {
                [self eraseAlertOK];
                
                
            }
            
        }
            
            break;
        case SHIPPINGMODE:{
            if (buttonIndex == 0) {
                [self shippingModeCancel];
                
            }
            else if (buttonIndex == 1) {
                [self shippingModeOK];
            }
        }
            
            break;
            
            
        case selectDeviceAlertTag:
        {
            
            if (buttonIndex == 0) {
                [self selectDeviceAlertCancel];
            }
            
            else if(buttonIndex == 1){
                [self selectDeviceAlertOK];
                
            }
            [self resetDeviceTmpList];
            //            selectDeviceBtnClickFlag = NO;
            
        }
            break;
        case bondAlertTag:{
            
            if (buttonIndex == 0) {
                [self bondAlertCancel];
            }
            
            else if(buttonIndex == 1){
                [self bondAlertOK];
                
            }
            
            
        }
            
            break;
        default:
            break;
    }
    
    
    
}

- (void)potentialBond:(NSString*) code sn: (NSString*) sn devName:(NSString*) devName
{
    
    
    NSString* displayMessage = [NSString stringWithFormat:
                                @"%@%@?",DigitCodeConfirmMsg,
                                code];
    
    dispatch_async( dispatch_get_main_queue(), ^ {
        [SVProgressHUD dismiss];
    });
    dispatch_async( dispatch_get_main_queue(), ^ {
        
        if ([CommonValues iOSVersion] >= IOS8) {
            alertController = [UIAlertController alertControllerWithTitle:@"" message:displayMessage preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
                [self bondAlertCancel];
                
            }];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self bondAlertOK];
            }];
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
        }
        else{
            UIAlertView *bondDialog = [[UIAlertView alloc]initWithTitle:@"" message:displayMessage delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
            bondDialog.tag = bondAlertTag;
            [bondDialog show];
        }
        
    });
    
}


- (void) bleDidBond:(TGbondResult)result {
    
    NSString * message;
    NSString * title;
    if (result == TGbondResultTokenAccepted) {
        message = @"Bond Succesful";
        
    }
    else if (result == TGbondResultTokenReleased) {
        title = TokenReleasedTitle;
        message = TokenReleasedMsg;
    }
    else if (result == TGbondResultErrorBondedNoMatch) {
        title = CommunicationErrorTitle;
        message = BondedNoMatchMsg;
    }
    else if (result == TGbondResultErrorBadTokenFormat) {
        title = BondRejectedTitle;
        message = BadTokenFormatMsg;
    }
    else if (result == TGbondResultErrorTimeOut) {
        title = TimeOutTitle;
        message = TimeOutMsg;
    }
    
    else if (result == TGbondResultErrorNoConnection) {
        title = NoConnectionTitle;
        message = NoConnectionMsg;
    }
    
    else if (result == TGbondResultErrorReadTimeOut) {
        title = ReadBackTimeOutTitle;
        message = ReadTimeOutMsg;
    }
    
    
    else if (result == TGbondResultErrorReadBackTimeOut) {
        title = ReadBackTimeOutTitle;
        message = ReadBackTimeOutMsg;
    }
    
    else if (result == TGbondResultErrorWriteFail) {
        title = WriteFailTitle;
        message = WriteFailMsg;
    }
    
    else if (result == TGbondResultErrorTargetIsAlreadyBonded) {
        title = BondRejectedTitle;
        message = TargetIsAlreadyBondedMsg;
    }
    
    
    else if (result == TGbondAppErrorNoPotentialBondDelegate) {
        title  = BondAppErrorTitle;
        message = NoPotentialBondDelegateMsg;
    }
    
    
    else if (result == TGbondResultErrorTargetHasWrongSN) {
        title  = CommunicationErrorTitle;
        message = TargetHasWrongSNMSg;
    }
    
    else if (result == TGbondResultErrorPairingRejected) {
        title  = PairingErrorTitle;
        message = PairingRejectedMsg;
    }
    
    
    else if (result == TGbondResultErrorSecurityMismatch) {
        title  = PairingErrorTitle;
        message = SecurityMismatchMsg;
    }
    
    
    
    else {
        title  = UnkonwnBondErrorTitle;
        message = [NSString stringWithFormat:@"Bonding Error, code: %d", (int) result];
    }
    NSLog(@"BONDIING RESULT Code  %lu",(unsigned long)result);
    NSLog(@"BONDIING RESULT msg  %@",message);
    
    // set bond flag,check it when do every action
    if (result == TGbondResultTokenAccepted) {
        
        
        
        NSLog(@"Bond Succesful,start to erase/setTimer/setAlarm");
        if (isErasing) {
            NSLog(@"call tryEraseData ---");
            [[TGBleManager  sharedTGBleManager] tryEraseData];
            dispatch_async( dispatch_get_main_queue(), ^ {
                [SVProgressHUD showWithStatus:@" Erase"];
            });
            
        }
        else if (isResetingBand)
        {
            NSLog(@"call mfg_support MFG_ShipModeConfig ---");
            //        [[TGBleManager  sharedTGBleManager] resetBand];
            [[TGBleManager sharedTGBleManager] mfg_support:YES arg2:MFG_ShipModeConfig];
            //            dispatch_async( dispatch_get_main_queue(), ^ {
            //                [SVProgressHUD showSuccessWithStatus:@"call resetBand..."];
            //            });
            
        }
        
        else if (wantSetVibrationOn){
            dispatch_async( dispatch_get_main_queue(), ^ {
                [SVProgressHUD showSuccessWithStatus:@"Vibration On ..."];
                [self.view setUserInteractionEnabled:YES];
            });
            NSLog(@"call vibration on ------");
            [[TGBleManager sharedTGBleManager]enableVBM];
            [CommonValues setHasTaskFlag:NO];
            
        }
        
        else if (wantSetVibrationOff){
            dispatch_async( dispatch_get_main_queue(), ^ {
                [SVProgressHUD showSuccessWithStatus:@"Vibration Off ..."];
                [self.view setUserInteractionEnabled:YES];
            });
            NSLog(@"call vibration off------");
            [[TGBleManager sharedTGBleManager]disableVBM];
            [CommonValues setHasTaskFlag:NO];
            
        }
        
        
        
        
    }
    else if (result == TGbondResultTokenReleased){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Finished!" message:@"Release Bond completed" delegate:nil cancelButtonTitle:nil otherButtonTitles:@" OK ", nil];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view setUserInteractionEnabled:YES];
            [SVProgressHUD dismiss];
            if ([CommonValues iOSVersion] >= IOS8) {
                alertController = [UIAlertController alertControllerWithTitle:@"Finished!" message:@"Release Bond completed" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@" OK " style:UIAlertActionStyleCancel handler:nil];
                [alertController addAction:okAction];
                [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
                
            }
            else{
                [alert show];
            }
            
        });
        
    }
    else{
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:@" OK ", nil];
        NSLog(@"call SDK tryDisconnect---");
        [[TGBleManager sharedTGBleManager]tryDisconnect];
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            if ([CommonValues iOSVersion] >= IOS8) {
                alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@" OK " style:UIAlertActionStyleCancel handler:nil];
                [alertController addAction:okAction];
                [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
            }
            else{
                [alert show];
            }
        });
        
    }
    
}

-(void)showDevicesList{
    NSLog(@" select device click");
    
    if (![CommonValues getBluetoothOnFlag]) {
        NSLog(@"BT OFF, can't go on candidateScaning");
        [self showBTNotOnAlert];
        return;
    }
    [SVProgressHUD showWithStatus:@" Search bands "];
    [self.view setUserInteractionEnabled:NO];
    
    NSLog(@" call  candidateScan:nameList");
    [[TGBleManager sharedTGBleManager] candidateScan:[CommonValues bandDeviceArray]];
    
    
    NSLog(@"Menu item  devicesArray count = %lu",(unsigned long)devicesArray.count);
    
}
- (void)showBTNotOnAlert {
    if ([CommonValues iOSVersion] >= IOS8) {
        alertController = [ UIAlertController alertControllerWithTitle: BluetoothIsOffTitle
                                                               message: BluetoothIsOffMsg
                                                        preferredStyle: UIAlertControllerStyleAlert
                          ];
        UIAlertAction * const okAction = [ UIAlertAction actionWithTitle: @"Done"
                                                                   style: UIAlertActionStyleDefault
                                                                 handler: ^( UIAlertAction * action )
                                           {
                                           [ self checkBTStatusDone ];
                                           }
                                         ];
        [ alertController addAction: okAction ];
        [ self performSelector: @selector( showAlertController )
                    withObject: nil
                    afterDelay: 0
        ];
        }
    else
        {
        UIAlertView *alert =[ [UIAlertView alloc]initWithTitle:BluetoothIsOffTitle message:BluetoothIsOffMsg delegate:self cancelButtonTitle:nil otherButtonTitles:@"Done", nil];
        alert.tag = checkBTStatusTag;
        [alert show];
        }
    
}

- (void) exceptionMessage:(TGBleExceptionEvent)eventType {
    NSString * message;
    
    if (eventType == TGBleHistoryCorruptErased) {
        message = @"Flash Memory Corrupted, Will Be erased, suspect battery has been exhausted";
    }
    else if (eventType == TGBleConfigurationModeCanNotBeChanged) {
        message = @"Exception Message - Ble Connection Mode CAN NOT be changed";
    }
    else if (eventType == TGBleUserAgeHasBecomeNegative_BirthDateIsNOTcorrect) {
        message = @"User Age has become negative, correct the birth date";
    }
    else if (eventType == TGBleUserAgeIsTooLarge_BirthDateIsNOTcorrect) {
        message = @"User Age is TOO OLD, check the birth date";
    }
    else if (eventType == TGBleUserBirthDateRejected_AgeOutOfRange) {
        message = @"User Age is out of RANGE, corret the birth date";
    }
    else if (eventType == TGBleFailedOtherOperationInProgress) {
        message = @"Exception Message - Another Operation is Already in Progress";
    }
    else if (eventType == TGBleFailedSecurityNotInplace) {
        message = @"Exception Message - Security is NOT In Place";
    }
    else if (eventType == TGBleTestingEvent) {
        message = @"Exception Message - This is only a Test";
    }
    else if (eventType == TGBleReInitializedNeedAlarmAndGoalCheck) {
        message = @"Exception Message - Band lost power or was MFG reset, check your alarm and goal settings";
    }
    else if (eventType == TGBleStepGoalRejected_OutOfRange) {
        message = @"Exception Message - Step Goal Settings rejected -- OUT OF RANGE";
    }
    else if (eventType == TGBleCurrentCountRequestTimedOut) {
        message = @"Exception Message - Can Not get Current Count values\nBand not responding";
    }
    else if (eventType == TGBleConnectFailedSuspectKeyMismatch) {
        message = @"Exception Message - Band appears to be paired, possible encryption key mismatch, hold side button for 10 seconds to reboot";
    }
    else if (eventType == TGBleNonRepeatingAlarmMayBeStaleCheckAlarmConfiguration) {
        message = @"Exception Message - A Non-repeating Alarm may be stale, check your alarm settings";
    }
    else {
        message = [NSString stringWithFormat:@"Exception Detected, code: %lu", (unsigned long)eventType];
    }
    
    NSLog(@"exceptionMessage =  %@",message);
    
}


#pragma marks
#pragma tableview datasource methods
#pragma mark -
- (NSInteger)popoverListView:(ZSYPopoverListView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return devicesArray.count;
}

- (UITableViewCell *)popoverListView:(ZSYPopoverListView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString * savedDeviceName = [defaults stringForKey:@"DeviceName"];
    NSLog(@"NSUserDefaults deviceName = %@",savedDeviceName);
    
    static NSString *identifier = @"identifier";
    UITableViewCell *cell = [tableView dequeueReusablePopoverCellWithIdentifier:identifier];
    if (nil == cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] ;
    }
    
    
    NSString *tmpStr = [NSString stringWithFormat:@"%@:%@",[ mfgDataArray objectAtIndex:indexPath.row],[ devicesArray objectAtIndex:indexPath.row]];
    //cell.textLabel.text =[ devicesArray objectAtIndex:indexPath.row];
    cell.textLabel.text = tmpStr;
    cell.textLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:25.0];
    
    if ([[ devicesArray objectAtIndex:indexPath.row] isEqualToString:savedDeviceName]) {
        // many devices have the same name
        //
        //cell.imageView.image = [UIImage imageNamed:@"fs_main_login_selected.png"];
    }
    
    return cell;
}

- (void)popoverListView:(ZSYPopoverListView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"deselect:%ld", (long)indexPath.row);
}

- (void)popoverListView:(ZSYPopoverListView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedIndexPath = indexPath;
    NSLog(@"select:%ld", (long)indexPath.row);
    NSString *deviceName = [devicesArray objectAtIndex:indexPath.row];
    NSString *mfgData = [mfgDataArray objectAtIndex:indexPath.row];
    readyToSaveDeviceName = [devicesArray objectAtIndex:indexPath.row];
    readyToSaveDeviceID = [devicesIDArray objectAtIndex:indexPath.row];
    NSString *msg = [NSString stringWithFormat:@"Are you sure you would like to connect to %@ %@  ?",deviceName, mfgData];
    //     [devicesArray removeAllObjects];
    //     [listView dismiss];
    [listView dismiss];
    if ([CommonValues iOSVersion] >= IOS8) {
        alertController = [UIAlertController alertControllerWithTitle:msg message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:CANCEL style:UIAlertActionStyleCancel handler:^ (UIAlertAction * action) {
            [self selectDeviceAlertCancel];
            
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:OK style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self selectDeviceAlertOK];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self performSelector:@selector(showAlertController) withObject:nil afterDelay:0];
    }
    
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:msg message:nil delegate:self cancelButtonTitle:CANCEL otherButtonTitles:OK, nil];
        alert.tag   = selectDeviceAlertTag;
        [alert show];
    }
}



//send email related
-(void)showPicker{
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil)
    {
        // We must always check whether the current device is configured for sending emails
        if ([mailClass canSendMail])
        {
            
            NSString *fileName = latestLogName;
            
            [self displayComposerSheet :fileName :@"txt"];
        }
        else
        {
            [self launchMailAppOnDevice];
        }
    }
    else
    {
        [self launchMailAppOnDevice];
    }
    
}

- (void)displayComposerSheet:(NSString *) fileName :(NSString *) fileType{
    
    NSLog(@"displayComposerSheet---");
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    //    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    //    NSString * userName = @"Tommy";
    
    //	[picker setSubject:[NSString stringWithFormat:@"Measurement Data from %@", userName]];
    [picker setSubject:@"Log From Seagull Base App" ];
    //	 NSString * sendToAddress = @"329136446@qq.com";
    //    NSString * sendToAddress = @"aluo@neurosky.com";
    //      NSString * sendToAddress = @"tommysima@neurosky.com";
    //    NSString *stroedEmailToAddress = [userDefaults objectForKey:@"email_receiver_address"];
    //    NSString *stroedEmailToAddress = [userDefaults objectForKey:@"email_receiver_address"];
    
    //aluo@neurosky.com
    
    // Set up recipients
    
    //@"cwang@neurosky.com",@"kelvin@neurosky.com",@"jliu@neurosky.com",
    NSArray *toRecipients = [NSArray arrayWithObjects:@"tommysima@neurosky.com",nil];
    [picker setToRecipients:toRecipients];
    NSData *myData = [NSData dataWithContentsOfFile:latestLogPath];
    
    [picker addAttachmentData:myData mimeType:@"text/plain" fileName:fileName];
    [self presentViewController:picker animated:YES completion:nil];
    
}
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    // Notifies users about errors associated with the interface
    NSLog(@"mailComposeController  didFinishWithResult");
    
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            //			sentMailStateLabel.text = @"Result: canceled";
            NSLog(@"Result: canceled-----");
            
            break;
        case MFMailComposeResultSaved:
            //			sentMailStateLabel.text = @"Result: saved";
            NSLog(@"Result: saved-----");
            
            break;
        case MFMailComposeResultSent:
            NSLog(@"Result: sent-----");
            
            //			sentMailStateLabel.text = @"Result: sent";
            break;
        case MFMailComposeResultFailed:
            //			sentMailStateLabel.text = @"Result: failed";
            NSLog(@"Result: failed-----");
            
            break;
        default:
            //			sentMailStateLabel.text = @"Result: not sent";
            NSLog(@"Result: not sent---");
            
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)launchMailAppOnDevice{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *stroedEmailToAddress = [userDefaults objectForKey:@"email_receiver_address"];
    
    NSString *recipients =[NSString stringWithFormat: @"mailto:%@?cc= , &subject=Log From Starter Kit Base App",stroedEmailToAddress];
    NSString *body = @"&body=Attachment is the latest log from me!";
    NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
    email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
}

- (void)stopScan{
    NSLog(@"touch listview other place to stopScan ");
    
    // reset button state and flags.
    //    isSyncing = NO;
    
    [self.view setUserInteractionEnabled:YES];
    //    isRT = NO;
    NSLog(@"call candidateStopScan----");
    [[TGBleManager sharedTGBleManager]candidateStopScan];
    [devicesArray removeAllObjects];
    [mfgDataArray removeAllObjects];
    [rssiArray removeAllObjects];
    [candIDArray removeAllObjects];
    [devicesIDArray removeAllObjects];
    
    
    
    haveDeviceFlag = NO;
    // cancel delayed time out alert.
    if ([timeOutTimer isValid]) {
        [timeOutTimer invalidate];
        NSLog(@"timeOutTimer invalidate----");
        
    }
    
    firstEnterCandiDel = NO;
}

- (void)showAlertController{
    [self presentViewController:alertController animated:YES completion:nil];
    
}
@end

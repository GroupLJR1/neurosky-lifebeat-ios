//
//  CardioZone2ViewController.h
//  WAT
//
//  Created by  NeuroSky on 5/21/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EX_Graph_View_Controller.h"

@interface CardioZone2ViewController : EX_Graph_View_Controller
{

  IBOutlet UIButton *back;

}

-(IBAction)back:(id)sender;
@end

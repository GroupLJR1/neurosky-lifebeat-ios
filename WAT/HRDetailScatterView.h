//
//  HRDetailScatterView.h
//  WAT
//
//  Created by neurosky on 10/10/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"

@interface HRDetailScatterView : UIView<CPTPlotDataSource,CPTAxisDelegate>{

}
- (id)initWithFrame:(CGRect)frame :(NSMutableArray *)arr :(NSString *)dateStr :(NSInteger)dayCounts :(NSInteger)chartXaxisPeriod :(NSInteger)chartLableDisplayCount;

@end

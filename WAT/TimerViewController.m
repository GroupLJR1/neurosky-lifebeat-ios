//
//  TimerViewController.m
//  WAT
//
//  Created by neurosky on 5/26/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import "TimerViewController.h"
#import "TGBleManager.h"
#import "Constant.h"
#import "CommonValues.h"

@interface TimerViewController ()

@end

@implementation TimerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    __LOG_METHOD_START;
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    __LOG_METHOD_END;
    return self;
}
- (NSUInteger)supportedInterfaceOrientations

{
    return UIInterfaceOrientationMaskPortrait;
    
}

-(BOOL)shouldAutorotate{
    
    return NO;
}

- (void)viewDidLoad
{
    __LOG_METHOD_START;
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    timerPicker = [ [ UIDatePicker alloc] initWithFrame:CGRectMake(0.0,100.0,0.0,0.0)];
    timerPicker.minuteInterval = 1;
    [timerPicker addTarget:self action:@selector(timerTimeChanged_:) forControlEvents:UIControlEventValueChanged ];
    
    
    [timerPicker setDatePickerMode:UIDatePickerModeCountDownTimer] ;
    [timerPicker setCountDownDuration:1];
    
    NSDate *today =  [NSDate date];
    NSDateFormatter *df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSString *dateStr =  [df stringFromDate:today];
    NSString  *dateTimeStr =  [NSString stringWithFormat:@"%@ 00:01",dateStr];
    NSLog(@"dateTimeStr = %@",dateTimeStr);
    [df setDateFormat:@"yyyy-MM-dd HH:mm"];
    //    NSDate *newDate = [df dateFromString:dateTimeStr];
    //    [ timerPicker setDate:newDate animated:YES];
    
    //
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *storedHM = [defaults objectForKey:USER_TIMER_ALARM];
    NSDate *newDate;
    if (storedHM == NULL) {
        NSString  *dateTimeStr =  [NSString stringWithFormat:@"%@ 00:01",dateStr];
        newDate = [df dateFromString:dateTimeStr];
    }
    
    else{
        
        NSString *storedTimerDateStr = [NSString stringWithFormat:@"%@ %@", dateStr, [defaults objectForKey:USER_TIMER_ALARM]];
        NSLog(@"storedTimerDateStr = %@",storedTimerDateStr);
        
        newDate = [df dateFromString:storedTimerDateStr];
        
    }
    
    
    [ timerPicker setDate:newDate animated:YES];
    
    
    [self.view addSubview:timerPicker];
    
    
    //    timerPicker.datePickerMode = UIDatePickerModeCountDownTimer;
    
    __LOG_METHOD_END;
}

-(void)timerTimeChanged_:(id)sender{
    __LOG_METHOD_START;
    UIDatePicker* control = (UIDatePicker*)sender;
    timerSelectedTime = control.date;
    
    NSLog(@"timerSelectedTime  = %@",timerSelectedTime);
    /*添加你自己响应代码*/
    
    //selectDateBtn.
    __LOG_METHOD_END;
}



- (void)didReceiveMemoryWarning
{
    __LOG_METHOD_START;
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    __LOG_METHOD_END;
}

- (IBAction)cancelBtnClick:(id)sender {
    __LOG_METHOD_START;
    [CommonValues setFlag:4];
    [self dismissViewControllerAnimated:YES completion:nil];
    __LOG_METHOD_END;
}

- (IBAction)saveBtnClick:(id)sender {
    __LOG_METHOD_START;
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSString *dateString;
    NSDate *nowDate = [NSDate date];
    NSDate *date1;
    NSString *dateString2;
    if (timerSelectedTime == NULL) {
        // timerSelectedTime = [NSDate date];
        NSString *storedTimer = [defaults objectForKey:USER_TIMER_ALARM];
        NSLog(@"storedTimer = %@",storedTimer);
        
        if (storedTimer == NULL) {
            NSTimeInterval interval = 60;
            date1 = [nowDate initWithTimeIntervalSinceNow:+interval];
            dateString2 = [dateFormatter stringFromDate:date1];
            NSLog(@"dateString2 = %@",dateString2);
            dateString = @"00:01";
        }
        else{
            dateString = storedTimer;
            
        }
    }
    
    else{
        dateString = [dateFormatter stringFromDate:timerSelectedTime];
        
        NSLog(@"timerSelectedTime = %@",dateString);
        NSArray *arr = [dateString componentsSeparatedByString:@":"];
        
        // NSLog(@"now date = %@",[dateFormatter stringFromDate :[NSDate date]]);
        
        
        if (arr.count == 3) {
            NSTimeInterval interval = [arr[0] integerValue]*60*60+ [arr [1] integerValue]*60 + [arr[2] integerValue];
            date1 = [nowDate initWithTimeIntervalSinceNow:+interval];
            dateString2 = [dateFormatter stringFromDate:date1];
            NSLog(@"dateString2 = %@",dateString2);
        }
        
        NSDateFormatter *dateFormatterWithHm =[[NSDateFormatter alloc] init];
        
        [dateFormatterWithHm setDateFormat:@"HH:mm"];
        dateString = [dateFormatterWithHm stringFromDate:timerSelectedTime];
        
    }
    
    
    
    //    timerText.text = dateString;
    
    //set GoalDuration
    
    NSArray *timer_alarm_array = [dateString2 componentsSeparatedByString:@":"];
    int alarm_h = 0;
    int alarm_m = 0;
    int alarm_s = 0;
    if (timer_alarm_array.count == 3) {
        alarm_h = [ timer_alarm_array[0] intValue];
        alarm_m =[ timer_alarm_array[1] intValue];
        alarm_s = [ timer_alarm_array[2] intValue];
        
        
    }
    
    //    timerSwitch.on = YES;
    
    [defaults setBool:YES forKey:TIMER_SWITCH];
    NSLog(@"alarm_h = %d ,alarm_m = %d ,alarm_s = %d",alarm_h,alarm_m,alarm_s);
    //    NSLog(@"call setGoalDuration-----");
    //    [[TGBleManager sharedTGBleManager] setGoalDurationHour:alarm_h];
    //    [[TGBleManager sharedTGBleManager] setGoalDurationMinute:alarm_m];
    //    [[TGBleManager sharedTGBleManager] setGoalDurationSecond:alarm_s];
    // GoalDuration save to userDefault
    //    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    //    NSString * timerAlarmStr =  [defaults objectForKey:USER_TIMER_ALARM];
    
    NSLog(@"TIMER_DERICTORY_TIME date1 = %@",date1);
    [defaults setObject:date1 forKey:TIMER_DERICTORY_TIME];
    
    
    [defaults setObject:dateString forKey:USER_TIMER_ALARM];
    [defaults synchronize];
    
    [CommonValues setFlag:3];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    __LOG_METHOD_END;
}

@end

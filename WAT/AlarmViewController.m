//
//  AlarmViewController.m
//  WAT
//
//  Created by neurosky on 5/26/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import "AlarmViewController.h"
#import "Constant.h"
#import "TGBleManager.h"
#import "CommonValues.h"

@interface AlarmViewController ()

@end

@implementation AlarmViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    __LOG_METHOD_START;
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    __LOG_METHOD_END;
    return self;
}

- (void)viewDidLoad
{
    __LOG_METHOD_START;
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    repeatDayArray = [[NSMutableArray alloc]init];
    
    repeatArray = [[NSArray alloc]initWithObjects:@"Once",@"Everyday",@"Monday",@"Tuesday", @"Wednesday",@"Thursday", @"Friday",@"Saturday", @"Sunday", nil];
    
    // repeat mode select
    
    repeatTableView = [[UITableView alloc]initWithFrame:CGRectMake(20.0,300,280,180)];
    [repeatTableView setDelegate:self];
    [repeatTableView setDataSource:self];
    [self.view addSubview:repeatTableView];
    
    alarmPicker.datePickerMode = UIDatePickerModeTime;
    [alarmPicker addTarget:self action:@selector(timeChanged:) forControlEvents:UIControlEventValueChanged ];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    alarmStr = [defaults objectForKey:USER_ALARM];
    if (alarmStr != NULL) {
        NSDate *today =  [NSDate date];
        NSDateFormatter *df = [[NSDateFormatter alloc]init];
        [df setDateFormat:@"yyyy-MM-dd"];
        NSString *dateStr =  [df stringFromDate:today];
        NSString  *dateTimeStr =  [NSString stringWithFormat:@"%@ %@",dateStr,alarmStr];
        NSLog(@"dateTimeStr = %@",dateTimeStr);
        [df setDateFormat:@"yyyy-MM-dd HH:mm"];
        NSDate *tmpDate = [df dateFromString:dateTimeStr];
        alarmPicker.date = tmpDate;
        
    }
    
    
    
    
    alarmMode = [defaults objectForKey:USER_ALARM_MODE];
    NSLog(@"alarmMode = %@",alarmMode);
    for (int i = 0; i< alarmMode.count; i++) {
        [repeatDayArray addObject:[alarmMode objectAtIndex:i]];
    }
    __LOG_METHOD_END;
}

-(void)timeChanged:(id)sender{
    __LOG_METHOD_START;
    UIDatePicker* control = (UIDatePicker*)sender;
    selectedTime = control.date;
    
    NSLog(@"time  = %@",selectedTime);
    /*添加你自己响应代码*/
    __LOG_METHOD_END;
}

- (void)didReceiveMemoryWarning
{
    __LOG_METHOD_START;
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    __LOG_METHOD_END;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    __LOG_METHOD_START;
    NSLog(@"numberOfRowsInSection repeatArray.count = %lu",(unsigned long)repeatArray.count);
    __LOG_METHOD_END;
    return repeatArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    __LOG_METHOD_START;
    static NSString * const CellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:CellIdentifier] ;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    NSUInteger row = [indexPath row];
    //  NSLog(@"cellForRowAtIndexPath = %d",row);
    
    
    cell.textLabel.text = [repeatArray objectAtIndex:row];
    
    if([repeatDayArray containsObject: [repeatArray objectAtIndex:row] ]){
        //   cell.imageView.image =  [UIImage imageNamed:@"Connection2"];
        cell.imageView.image = [UIImage imageNamed:@"duihao"];
        //     NSLog(@"3");
    }
    
    else {
        
        cell.imageView.image = [UIImage imageNamed:@"nothing"];
    }
    __LOG_METHOD_END;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    __LOG_METHOD_START;
    NSUInteger row =[indexPath row];
    NSString *rowString =  [repeatArray objectAtIndex:row];
    NSLog(@"rowString = %@",rowString);
    
    
    //save时保存 到userDefaults ;cancel时 不需要保存
    //    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    alarmMode  = [repeatArray objectAtIndex:row];
    //    [userDefault setObject:[repeatArray objectAtIndex:row] forKey:USER_ALARM_MODE];
    //    [userDefault synchronize];
    
    
    [self add2RepeatArray:rowString];
    
    [repeatTableView reloadData];
    __LOG_METHOD_END;
}

- (NSUInteger)supportedInterfaceOrientations

{
    __LOG_METHOD_START;
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate{
    __LOG_METHOD_START;
    return NO;
}
-(NSMutableArray *) add2RepeatArray :(NSString *) day{
    __LOG_METHOD_START;
    NSArray *sevenDays = [[NSArray alloc]initWithObjects:@"Monday",@"Tuesday", @"Wednesday",@"Thursday", @"Friday",@"Saturday", @"Sunday",nil];
    NSArray *twoDays =   [[NSArray alloc]initWithObjects:@"Once",@"Everyday" ,nil];
    //数组中包含这一天
    if ([repeatDayArray containsObject:day]) {
        NSLog(@"11111");
        [repeatDayArray removeObject:day];
    }
    
    //数组中不包含这一天
    else {
        //  day是7天中的某一天，则删除每天&&仅一次
        NSLog(@"22222");
        if ([sevenDays containsObject:day]) {
            NSLog(@"3333");
            
            if ([repeatDayArray containsObject:twoDays[0]]) {
                [repeatDayArray removeObject:twoDays[0]];
            }
            if ([repeatDayArray containsObject:twoDays[1]]) {
                [repeatDayArray removeObject:twoDays[1]];
            }
            
            
        }
        
        // day是每天||仅一次，则属于7天中的每一天
        else{
            NSLog(@"4444");
            [repeatDayArray removeAllObjects];
            //  [repeatDayArray addObject:@"Test"];
        }
        
        //加上day
        [repeatDayArray addObject:day];
    }
    
    if (![repeatDayArray containsObject:@"Test"]) {
        [repeatDayArray addObject:@"Test"];
    }
    
    if (repeatDayArray.count == 1 ) {
        [repeatDayArray addObject:@"Once"];
    }
    
    
    NSLog(@"add2RepeatArray  ----repeatDayArray = %@",repeatDayArray);
    
    
    if (repeatDayArray.count < 2) {
        NSLog(@"Error!!!!!!");
    }
    __LOG_METHOD_END;
    return repeatDayArray;
}


- (IBAction)cancelBtnClick:(id)sender {
    __LOG_METHOD_START;
    [CommonValues setFlag:2];
    [self dismissViewControllerAnimated:YES completion:nil];
    __LOG_METHOD_END;
}

- (IBAction)saveBtnClick:(id)sender {
    __LOG_METHOD_START;
    NSDateFormatter * const dateFormatter = [ [ NSDateFormatter alloc ] init ];
    NSUserDefaults * const defaults = [ NSUserDefaults standardUserDefaults ];
    [ dateFormatter setDateFormat: @"HH:mm" ];
    NSString *dateString;
    if (selectedTime == NULL) {
        NSString *storedAlarm =  [defaults objectForKey: USER_ALARM];
        NSLog(@"storedAlarm = %@",storedAlarm);
        if (storedAlarm == NULL) {
            selectedTime = [NSDate date];
            dateString = [dateFormatter stringFromDate:selectedTime];
        }
        else{
            dateString = storedAlarm;
        }
        
    }
    else{
        dateString = [dateFormatter stringFromDate:selectedTime];
        
    }
    NSLog(@"timeString = %@",dateString);
    
    
    
    
    //    alarmText.text = dateString;
    
    
    // Set Alarm
    
    
    
    //   NSString * alarmStr_2 =  [defaults objectForKey:USER_ALARM];
    NSArray *alarm_array = [dateString componentsSeparatedByString:@":"];
    int alarm_h = 0;
    int alarm_m = 0;
    if (alarm_array.count == 2) {
        alarm_h = [ alarm_array[0] intValue];
        alarm_m =[ alarm_array[1] intValue];
        
    }
    
    
    if (repeatDayArray == NULL ) {
        [repeatDayArray addObject:@"Test"];
        [repeatDayArray addObject:@"Once"];
        [defaults setObject:repeatDayArray forKey:USER_ALARM_MODE];
    }
    
    else if (repeatDayArray != NULL && [repeatDayArray isKindOfClass:[NSArray class]]) {
        //        alarmSwitch.on = YES;
        
        
        //        NSLog(@"call setAlarm --------");
        //        [[TGBleManager sharedTGBleManager] setAlarmHour:alarm_h];// 10:09 AM
        //        [[TGBleManager sharedTGBleManager] setAlarmMinute:alarm_m];
        NSLog(@"alarm_h = %d,alarm_m = %d",alarm_h,alarm_m);
        if (repeatDayArray.count == 0) {
            [repeatDayArray addObject:@"Test"];
            [repeatDayArray addObject:@"Once"];
            //            [defaults setObject:repeatDayArray forKey:USER_ALARM_MODE];
        }
        [defaults setObject:repeatDayArray forKey:USER_ALARM_MODE];
        
        //        NSLog(@"call setAlarmRepeat ----");
        //        [[TGBleManager sharedTGBleManager] setAlarmRepeat:[self repeatCode:repeatDayArray]];
        
        
    }
    
    NSDate *willStroreTime;
    if ([selectedTime timeIntervalSince1970] < [[NSDate date] timeIntervalSince1970]) {
        //选择小于此刻的时间 = 明天的这个时间
        willStroreTime = [selectedTime dateByAddingTimeInterval:(60*60*24)];
    }
    else {
        willStroreTime = selectedTime;
    }
    
    //    NSDate *willStroreTime = [selectedTime dateByAddingTimeInterval:(60*60*24)];
    if (selectedTime == NULL) {
        [defaults setObject:willStroreTime forKey:ALARM_DERICTORY_TIME];
        NSLog(@"willStroreTime = %@",willStroreTime);
    }
    
    
    [defaults setObject:dateString forKey:USER_ALARM];
    [defaults synchronize];
    
    NSLog(@"done click repeatDayArray = %@",repeatDayArray);
    NSString *stored_mode =   [defaults objectForKey:USER_ALARM_MODE];
    NSLog(@"stored_mode = %@",stored_mode);
    
    [CommonValues setFlag:1];
    [self dismissViewControllerAnimated:YES completion:nil];
    __LOG_METHOD_END;
}



@end

//
//  NewCell.m
//  WAT
//
//  Created by  NeuroSky on 9/23/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import "NewCell.h"
#import "CustomerView.h"
#import "CircleProgress.h"
#import "Constant.h"


@implementation NewCell
@synthesize image1;
@synthesize labelLeft;
@synthesize labelRight;
@synthesize section;
@synthesize uiView;

@synthesize delegate;
@synthesize ns_method;
@synthesize ns_target;
@synthesize circle;
@synthesize circle1;
@synthesize version;
@synthesize separ;
@synthesize customerView1,customerView2,customerView3;

@synthesize caloriesProgress;
@synthesize stepProgress;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
       
        //progress bar view
        self.backgroundColor = [UIColor clearColor];
        uiView = [[UIView alloc] initWithFrame:CGRectMake(0,0, 596/2, 300)];
        
        

        circle = [[CircleProgress alloc] initWithFrame:CGRectMake(10, 0, 146, 146)];
        circle.foreImage = [UIImage imageNamed:@"steps_progress_rate"];

        circle.iconImage = [UIImage imageNamed:@"steps"];
        circle.textColor = [UIColor colorWithRed:2.0/255.0 green:190.0/255.0 blue:253.0/255.0 alpha:1.0];
        circle.targetString = @"/10,000";
        circle.actualString = [self intToString:5100];
        circle.progress = 1.0;
//        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showHelpViewAction)];
//        [circle addGestureRecognizer:tapGesture];
        [uiView addSubview:circle];
        
        //calories circle
       circle1 = [[CircleProgress alloc] initWithFrame:CGRectMake(165, 0, 146, 146)];
        circle1.foreImage = [UIImage imageNamed:@"calories_progress_rate"];
        circle1.iconImage = [UIImage imageNamed:@"calories"];
        
        circle1.textColor =[UIColor colorWithRed:255/255.0 green:44/255.0 blue:120/255.0 alpha:1.0];
        circle1.targetString = @"/20,000";
        circle1.actualString = [self intToString:1450];
        circle1.progress = 0.5;
        [uiView addSubview:circle1];
        
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(134,0, 100, 25)];
        label.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:17.0];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor lightGrayColor];
        label.text = @"1:32pm today";
       // [uiView addSubview:label];
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(60, 145, 100, 35)];
        label.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor lightGrayColor];
        label.text = @"STEPS";
        [uiView addSubview:label];
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(210, 145, 100, 35)];
        label.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor lightGrayColor];
        label.text = @"CALORIES";
        [uiView addSubview:label];
        
        
        customerView1=[[CustomerView alloc]initWithFrame:CGRectMake(8,185, 98, 108) backgroundImage:[UIImage imageNamed:@"distance2@2x.png"] textValue:@"5.5mi" withTextColor:[UIColor colorWithRed:47/255.0 green:105/255.0 blue:55/255.0 alpha:(1)] progress:0.5 progressForImgName:@"distance_progress"];
        [uiView addSubview:customerView1];
        
        customerView2=[[CustomerView alloc]initWithFrame:CGRectMake(112,185, 98, 108) backgroundImage:[UIImage imageNamed:@"active_time@2x.png"] textValue:@"1h24m" withTextColor:[UIColor colorWithRed:255/255.0 green:156/255.0 blue:37/255.0 alpha:(1)] progress:0.8 progressForImgName:@"active time_progress"] ;
        [uiView addSubview:customerView2];
        
        customerView3=[[CustomerView alloc]initWithFrame:CGRectMake(216,185, 98, 108) backgroundImage:[UIImage imageNamed:@"sleep2@2x.png"] textValue:@"6h42m" withTextColor:[UIColor colorWithRed:200/255.0 green:36/255.0 blue:167/255.0 alpha:(1)] progress:0.5 progressForImgName:@"sleep_progress"] ;
        [uiView addSubview:customerView3];
        
        
            [self addSubview:uiView];
            
        
       
       //heart view /body view
         image1 = [[UIImageView alloc] initWithFrame:CGRectMake(11,0, 596/2, 240/2)];
        
        labelLeft = [[UILabel alloc] initWithFrame:CGRectMake(40,35,50, 50)];
        
       labelLeft.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
        labelLeft.backgroundColor = [UIColor clearColor];
        
         labelRight = [[UILabel alloc] initWithFrame:CGRectMake(272,65,50, 50)];
        labelRight.font =  [UIFont fontWithName:DEFAULT_FONT_NAME size:30.0];
        labelRight.backgroundColor = [UIColor clearColor];
        
        // [image1 setImage:[UIImage imageNamed:@"hr_bpm@2x"]];
      //  [image1  addSubview:image1];
        
        [self addSubview:image1];
        [self addSubview:labelLeft];
        [self addSubview:labelRight];
        
        version = [[UILabel alloc] initWithFrame:CGRectMake(134,120, 100, 25)];
        version.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:17.0];
        version.backgroundColor = [UIColor clearColor];
        version.textColor = [UIColor lightGrayColor];
        NSString *s=[NSString stringWithFormat:@"Version %@",[[UIDevice currentDevice]systemVersion]];
        version.text =s;
        [self addSubview:version];
        //[self addSubview:progressLabel];
        
        separ=[[UIImageView alloc]initWithFrame:CGRectMake(0, 130, 320, 1)];
        separ.image=[UIImage imageNamed:@"line@2x.png"];
        [self addSubview:separ];
        
    }
    return self;
}
-(void)showHelpViewAction{
    NSLog(@"ddddddd");
    [delegate showHelpView:@"STEPS"];
    //[ns_target ns_method];
}
-(NSString *)intToString:(int)value{
    if (value<0) {
        return nil;
    }
    NSString *s = [NSString stringWithFormat:@"%d",value];
    //    NSLog(@"%d",s.length);
    NSMutableArray *a = [[NSMutableArray alloc] init];
    NSString *s1;
    NSInteger length = s.length;
    while (length>3) {
        s1 =[s substringFromIndex:s.length-3];
        s = [s substringWithRange:NSMakeRange(0,length-3)];
        //        NSLog(@"s----%@",s);
        length = s.length;
        [a addObject:s1];
    }
    [a addObject:s];
    NSArray *reversedArray = [[a reverseObjectEnumerator] allObjects];
    NSString *string = [reversedArray componentsJoinedByString:@","];
    //    NSLog(@"string----%@",string);
    return string;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setStepProgress:(double)sProgress{
    stepProgress = sProgress;
    [self setNeedsDisplay];
    
}
-(void)setCaloriesProgress:(double)cProgress{
    caloriesProgress =cProgress;
    [self setNeedsDisplay];
}

@end

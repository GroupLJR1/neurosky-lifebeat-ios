//
//  CardioPlot.m
//  WAT
//
//  Created by NeuroSky on 2/10/15.
//  Copyright (c) 2015 Julia. All rights reserved.
//

#import "CardioPlot.h"

@implementation CardioPlot


- (void)controlMoodChangeForIphone5S: (int) moodValue btn:(UIButton *)btn{
    if (moodValue<7 && 0<=moodValue) {
        /// img.center=CGPointMake(point.x, 185);
        btn.backgroundColor=[UIColor colorWithRed:255/255.0 green:0/255.0 blue:255/255.0 alpha:1];
    }
    if (moodValue<15 && 7<=moodValue) {
        /// img.center=CGPointMake(point.x, 185);
        float red=255*(1-(moodValue-7.0)/8);
        btn.backgroundColor=[UIColor colorWithRed:red/255.0 green:0/255.0 blue:255/255.0 alpha:1];
    }
    if (moodValue<30 && 13<=moodValue) {
        // img.center=CGPointMake(point.x, 185);
        float green=255*((moodValue-13.0)/17);
        btn.backgroundColor=[UIColor colorWithRed:0/255.0 green:green/255.0 blue:255/255.0 alpha:1];
    }
    if (moodValue<45 && 30<=moodValue) {
        // img.center=CGPointMake(point.x, 185);
        float blue=255*(1-(moodValue-30.0)/15);
        btn.backgroundColor=[UIColor colorWithRed:0/255.0 green:255/255.0 blue:blue/255.0 alpha:1];
    }
    if (moodValue<65 && 45<=moodValue) {
        // img.center=CGPointMake(point.x, 185);
        float red=255*((moodValue-45.0)/20);
        btn.backgroundColor=[UIColor colorWithRed:red/255.0 green:255/255.0 blue:0/255.0 alpha:1];
    }
    if (moodValue<101 && 65<=moodValue) {
        // img.center=CGPointMake(point.x, 185);
        float green=255*(1-(moodValue-65.0)/36);
        btn.backgroundColor=[UIColor colorWithRed:255/255.0 green:green/255.0 blue:0/255.0 alpha:1];
    }

}

- (void)controlMoodChangeForIphone4S: (int) moodValue btn:(UIButton *)btn{
    if (moodValue<11 && 0<=moodValue) {
        /// img.center=CGPointMake(point.x, 185);
        btn.backgroundColor=[UIColor colorWithRed:255/255.0 green:0/255.0 blue:255/255.0 alpha:1];
    }
    if (moodValue<17 && 11<=moodValue) {
        /// img.center=CGPointMake(point.x, 185);
        float red=255*(1-(moodValue-11.0)/6);
        btn.backgroundColor=[UIColor colorWithRed:red/255.0 green:0/255.0 blue:255/255.0 alpha:1];
    }
    if (moodValue<36 && 17<=moodValue) {
        // img.center=CGPointMake(point.x, 185);
        float green=255*((moodValue-17.0)/19);
        btn.backgroundColor=[UIColor colorWithRed:0/255.0 green:green/255.0 blue:255/255.0 alpha:1];
    }
    if (moodValue<50 && 36<=moodValue) {
        // img.center=CGPointMake(point.x, 185);
        float blue=255*(1-(moodValue-36.0)/14);
        btn.backgroundColor=[UIColor colorWithRed:0/255.0 green:255/255.0 blue:blue/255.0 alpha:1];
    }
    if (moodValue<64 && 50<=moodValue) {
        // img.center=CGPointMake(point.x, 185);
        float red=255*((moodValue-50.0)/15);
        btn.backgroundColor=[UIColor colorWithRed:red/255.0 green:255/255.0 blue:0/255.0 alpha:1];
    }
    if (moodValue<101 && 64<=moodValue) {
        // img.center=CGPointMake(point.x, 185);
        float green=255.0*(1-(moodValue-64.0)/36);
        btn.backgroundColor=[UIColor colorWithRed:255/255.0 green:green/255.0 blue:0/255.0 alpha:1];
    }

}

- (NSString *)ekg2Path {
    //real time ECG raw data save path
    NSString *ekg2Path;
    NSString *timeStr = [[CommonValues timeDateFormatter:DateFormatterTimeAllUnderLineGMT] stringFromDate:[NSDate date ]];
    NSString *ekg2FileName = [NSString stringWithFormat:@"%@.txt",timeStr];
    ekg2Path = [[CommonValues getDocumentPath] stringByAppendingPathComponent:@"EKGDataInteralTest"];
    ekg2Path = [ekg2Path stringByAppendingPathComponent:ekg2FileName];
    NSLog(@"cardio plot ekg2Path = %@",ekg2Path);
    return ekg2Path;

}

- (void)gestureAccording2Point:(CGPoint )point uiView:(UIView *)uiview btn:(UIButton *)but{
    if ([CommonValues getScreenHeight] == 568) {
        if (point.x<500 && 108<point.x) {
            uiview.center=CGPointMake(point.x, 289-20);
        }
        if (point.x<135 && 108<point.x) {
            /// img.center=CGPointMake(point.x, 185);
            but.backgroundColor=[UIColor colorWithRed:255/255.0 green:0/255.0 blue:255/255.0 alpha:1];
        }
        if (point.x<160 && 135<point.x) {
            /// img.center=CGPointMake(point.x, 185);
            int red=255*(1-(point.x-135)/25);
            but.backgroundColor=[UIColor colorWithRed:red/255.0 green:0/255.0 blue:255/255.0 alpha:1];
        }
        if (point.x<235 && 160<point.x) {
            // img.center=CGPointMake(point.x, 185);
            int green=255*((point.x-160)/75);
            but.backgroundColor=[UIColor colorWithRed:0/255.0 green:green/255.0 blue:255/255.0 alpha:1];
        }
        if (point.x<310 && 235<point.x) {
            // img.center=CGPointMake(point.x, 185);
            int blue=255*(1-(point.x-235)/75);
            but.backgroundColor=[UIColor colorWithRed:0/255.0 green:255/255.0 blue:blue/255.0 alpha:1];
        }
        if (point.x<385 && 310<point.x) {
            // img.center=CGPointMake(point.x, 185);
            int red=255*((point.x-310)/75);
            but.backgroundColor=[UIColor colorWithRed:red/255.0 green:255/255.0 blue:0/255.0 alpha:1];
        }
        if (point.x<500 && 385<point.x) {
            // img.center=CGPointMake(point.x, 185);
            int green=255*(1-(point.x-385)/130);
            but.backgroundColor=[UIColor colorWithRed:255/255.0 green:green/255.0 blue:0/255.0 alpha:1];
        }
    }else{
        if (point.x<456 && 60<point.x) {
            uiview.center=CGPointMake(point.x, 289-20);
        }
        if (point.x<100 && 60<point.x) {
            /// img.center=CGPointMake(point.x, 185);
            but.backgroundColor=[UIColor colorWithRed:255/255.0 green:0/255.0 blue:255/255.0 alpha:1];
        }
        if (point.x<125 && 100<point.x) {
            /// img.center=CGPointMake(point.x, 185);
            int red=255*(1-(point.x-100)/25);
            but.backgroundColor=[UIColor colorWithRed:red/255.0 green:0/255.0 blue:255/255.0 alpha:1];
        }
        if (point.x<200 && 125<point.x) {
            // img.center=CGPointMake(point.x, 185);
            int green=255*((point.x-125)/75);
            but.backgroundColor=[UIColor colorWithRed:0/255.0 green:green/255.0 blue:255/255.0 alpha:1];
        }
        if (point.x<275 && 200<point.x) {
            // img.center=CGPointMake(point.x, 185);
            int blue=255*(1-(point.x-200)/75);
            but.backgroundColor=[UIColor colorWithRed:0/255.0 green:255/255.0 blue:blue/255.0 alpha:1];
        }
        if (point.x<350 && 275<point.x) {
            // img.center=CGPointMake(point.x, 185);
            int red=255*((point.x-275)/75);
            but.backgroundColor=[UIColor colorWithRed:red/255.0 green:255/255.0 blue:0/255.0 alpha:1];
        }
        if (point.x<456 && 350<point.x) {
            // img.center=CGPointMake(point.x, 185);
            int green=255*(1-(point.x-350)/130);
            but.backgroundColor=[UIColor colorWithRed:255/255.0 green:green/255.0 blue:0/255.0 alpha:1];
        }
    }


}
@end

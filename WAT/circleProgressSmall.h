//
//  CircleProgress.h
//  WAT
//
//  Created by Julia on 9/23/13.
//  Copyright (c) 2013 NeuroSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface circleProgressSmall : UIView{
    UIImage *foreImage;
    UIImage *iconImage;
    
    UIColor *textColor;
    
    NSString *targetString;
    NSString *actualString;
    double progress;
    UIImageView *progressImageView;
    
    
}
@property (retain,nonatomic) UIImage *foreImage;
@property (retain,nonatomic) UIImage *iconImage;
@property (retain,nonatomic) UIColor *innerLabelTextColor;
@property (retain,nonatomic) UIColor *textColor;

@property (retain,nonatomic) NSString *targetString;
@property (retain,nonatomic) NSString *actualString;
@property (retain,nonatomic) NSString *pString;
@property (assign,nonatomic) double progress;

-(void)setTextColor:(UIColor *)color;
@end

//
//  TGLibEmulator.h
//  WAT
//
//  Created by Mark I. Walsh on 2/22/15.
//  Copyright 2015 Julia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"

#if !defined( TGLIBEMULATOR_H_INCLUDED )
#define TGLIBEMULATOR_H_INCLUDED


#if !defined( USE_THINKGEAR_LIBRARY )
#define TGLibEKG TGLibEmulator
#endif  // !defined( USE_THINKGEAR_LIBRARY )


@interface TGLibEmulator : NSObject

{

    id delegate;

}

@property (retain) id delegate;

- (id)init:(int) rawDataPowerGridNoiseFrequency;

- (bool)initEkgAnalysis:(NSDate *) oneSecondTS; // deprecated, use the following replacement (sampleRate: 512, settling: 2)
- (bool)initEkgAnalysis:(NSDate *) oneSecondTS sampleRate:(int) sampleRate; // // deprecated, use the following replacement
- (bool)initEkgAnalysis:(NSDate *) oneSecondTS sampleRate:(int) sampleRate settlingTime: (int) seconds;
// sampleRate must be either 256 or 512
// settlingTime must be 0 to 5 seconds


- (bool)requestEkgAnalysis_s:(short) rawSample;
- (int)getVersion;
- (NSString *)getProductId;
- (NSString *)getAlgoVersion;

- (void)setUserProfile:(int) age
            withGender:(bool) female // true = female, false = male
             withWrist:(bool) left; // true = band is on left wrist


- (void)setHRVOutputInterval:(int)outputInterval;

- (int)computeHRVNow;


- (void)sleepInitAnalysis:(NSDate*) startTime endTime:(NSDate *) endTime DEPRECATED_ATTRIBUTE;
- (void)sleepInitAnalysis;
- (void)sleepAddData:(NSDate*) sampleTime sleepPhase:(int) sleepPhase;
- (void)sleepRequestAnalysis DEPRECATED_ATTRIBUTE;
- (void)sleepRequestAnalysis:(NSDate *) startTime endTime:(NSDate *) endTime;
- (void)sleepSetInterval:(int)minutes completeData:(bool) fullDataSet;

@end

#endif  // !defined( TGLIBEMULATOR_H_INCLUDED )

//
//  CommonColors.h
//  WAT
//
//  Created by neurosky on 9/22/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommonColors : NSObject



+(UIColor *) getStepColor;
+(UIColor *) getCaloriesColor;
+(UIColor *) getDistanceColor;
+(UIColor *) getActiveTimeColor;
+(UIColor *) getSleepColor;
+(UIColor *) getProfileBlueColor;
+(UIFont *) activeTimeAxisLabelFont;

@end

//
//  CommonColors.m
//  WAT
//
//  Created by neurosky on 9/22/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import "CommonColors.h"
#import "Constant.h"


@implementation UIColor ( Extension )

/*
 Primary Color Palette:
    Pantone / PMS 298 / #57b5e0  (87, 181, 224)
    White
    C = 0, M = 0, Y = 0, K = 80
 Secondary Color Palette:
    C = 0, M = 0, Y = 0, K = 60
    C = 0, M = 0, Y = 0, K = 20
    C = 4, M = 3, Y = 3, K = 0
    Pantone / PMS 152 / #d97300 (217, 115, 0)
 */


+ ( UIColor * ) getColorPMS298
{
    static CGFloat const colorR =  87.0f / 255.0f;
    static CGFloat const colorG = 181.0f / 255.0f;
    static CGFloat const colorB = 224.0f / 255.0f;
    return [ UIColor colorWithRed: colorR
                            green: colorG
                             blue: colorB
                            alpha: 1.0f
           ];
}

+ ( UIColor * ) getColorPMS152
{
    static CGFloat const colorR = 217.0f / 255.0f;
    static CGFloat const colorG = 115.0f / 255.0f;
    static CGFloat const colorB =   0.0f / 255.0f;
    return [ UIColor colorWithRed: colorR
                            green: colorG
                             blue: colorB
                            alpha: 1.0
           ];
}

@end  //  end @implementation UIColor ( Extension )



@implementation CommonColors


+(UIColor *) getStepColor{
    return [ UIColor getColorPMS298 ];
}
+(UIColor *) getCaloriesColor{
    return [ UIColor getColorPMS152 ];
}
+(UIColor *) getDistanceColor{
    return [ UIColor getColorPMS298 ];
}

+(UIColor *) getActiveTimeColor{
    return [ UIColor getColorPMS152 ];
}
+(UIColor *) getSleepColor{
    return [ UIColor getColorPMS298 ];
}
+(UIColor *) getProfileBlueColor{
    return [ UIColor getColorPMS152 ];
}

+(UIFont *) activeTimeAxisLabelFont{
   return  [UIFont fontWithName:DEFAULT_FONT_NAME size:17.0];
}


@end

//
//  AboutViewController.h
//  WAT
//
//  Created by  NeuroSky on 8/5/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TGBleManager.h"
#import "CHYPorgressImageView.h"
#import "ZSYPopoverListView.h"
#import "SVProgressHUD.h"
#import "CommonValues.h"

@interface AboutViewController : UIViewController<TGBleManagerDelegate,ZSYPopoverListDatasource,ZSYPopoverListDelegate>{

    __weak IBOutlet UILabel *title_label;
    __weak IBOutlet UIProgressView *proView;
    UIProgressView *pv;
    BOOL updatingFW;
    __weak IBOutlet UILabel *algo_value_label;
    __weak IBOutlet CHYPorgressImageView *progressbar;
    __weak IBOutlet UIImageView *progress_bg_img;
    __weak IBOutlet UILabel *ble_value_label;
    __weak IBOutlet UIButton *updateBtn;
    __weak IBOutlet UILabel *appVersionLabel;
    __weak IBOutlet UILabel *sdkVersionLabel;
    __weak IBOutlet UILabel *hrVersionLabel;
    __weak IBOutlet UILabel *fwVersionLabel;
    __weak IBOutlet UILabel *algoVersionLabel;
    ZSYPopoverListView *listView ;
    NSArray *defaultDevicesArray;
    
    __weak IBOutlet UILabel *batteryLabel;
    NSString *readyToSaveDeviceName;
    NSString *readyToSaveDeviceID;
    NSMutableArray *devicesArray;
    NSMutableArray *mfgDataArray;
    NSMutableArray *rssiArray;
    NSMutableArray *candIDArray;
    NSMutableArray  *devicesIDArray;
    
    NSTimer *timeOutTimer;
    BOOL firstEnterCandiDel;
    
    __weak IBOutlet UILabel *progressPercentLabel;
    NSTimer *showListTimer;
    float screenHeight;

    BOOL lostBeforeMainOperation ;
    BOOL identifyingBand;
     BOOL identifyDisconnectFlag;
    BOOL reconnectingBand;
    __weak IBOutlet UIView *myWholeView;
    UIAlertView *cannotConfirmUpdateAlert;
    BOOL isReleaseBondDone;
    UIAlertController *alertController;
}
@property (nonatomic, retain) NSIndexPath *selectedIndexPath;
@property (strong, nonatomic) IBOutlet UILabel *ModelNumLabel;
@property (strong, nonatomic) IBOutlet UILabel *SerialNumLabel;
@property (strong, nonatomic) IBOutlet UILabel *FW_VersionLabel;
@property (strong, nonatomic) IBOutlet UILabel *HW_VersionLabel;
@property (strong, nonatomic) IBOutlet UILabel *SW_VersionLabel;
@property (strong, nonatomic) IBOutlet UILabel *ManufacturerLabel;
@property (strong, nonatomic) IBOutlet UILabel *AppVersion;
@property (strong, nonatomic) IBOutlet UILabel *CZVerison;
- (IBAction)backClick:(id)sender;
- (IBAction)fwUpdateClick:(id)sender;
- (IBAction)identifyBtnClick:(id)sender;
- (IBAction)releaseBondClick:(id)sender;

@end

//
//  TGLibEmulator.m
//  WAT
//
//  Created by Mark I. Walsh on 2/22/15.
//  Copyright 2015 Julia. All rights reserved.
//

#import "TGLibEmulator.h"


@implementation TGLibEmulator


@synthesize delegate;

- ( id ) init: ( int ) rawDataPowerGridNoiseFrequency
    {
    __LOG_METHOD_START;
    self = [ super init ];
    delegate = self;
    __LOG_METHOD_END;
    return self;
    }


- ( bool ) initEkgAnalysis: ( NSDate * ) oneSecondTS
    {
    __LOG_METHOD_START;
    bool const val = [ self initEkgAnalysis: oneSecondTS
                                 sampleRate: 0
                               settlingTime: 0
                     ];
    __LOG_METHOD_END;
    return val;
    }


- ( bool ) initEkgAnalysis: ( NSDate * ) oneSecondTS
                sampleRate: ( int ) sampleRate
    {
    __LOG_METHOD_START;
    bool const val = [ self initEkgAnalysis: oneSecondTS
                                 sampleRate: sampleRate
                               settlingTime: 0
                     ];
    __LOG_METHOD_END;
    return val;
    }


- ( bool ) initEkgAnalysis: ( NSDate * ) oneSecondTS
                sampleRate: ( int ) sampleRate
              settlingTime: ( int ) seconds
    {
    __LOG_METHOD_START;
    bool const val = YES;
    __LOG_METHOD_END;
    return val;
    }


- ( bool ) requestEkgAnalysis_s: ( short ) rawSample
    {
    __LOG_METHOD_START;
    bool const val = YES;
    __LOG_METHOD_END;
    return val;
    }


- ( int ) getVersion
    {
    __LOG_METHOD_START;
    int const val = 99;
    __LOG_METHOD_END;
    return val;
    }


- ( NSString * ) getProductId
    {
    __LOG_METHOD_START;
    NSString * const val = @"Emulator";
    __LOG_METHOD_END;
    return val;
    }


- ( NSString * ) getAlgoVersion
    {
    __LOG_METHOD_START;
    NSString * const val = @"Emulator";
    __LOG_METHOD_END;
    return val;
    }


- ( void ) setUserProfile: ( int )  age
               withGender: ( bool ) female // true = female, false = male
                withWrist: ( bool ) left   // true = band is on left wrist
    {
    __LOG_METHOD_START;
    __LOG_METHOD_END;
    return;
    }


- ( void ) setHRVOutputInterval: ( int ) outputInterval
    {
    __LOG_METHOD_START;
    __LOG_METHOD_END;
    return;
    }


- ( int ) computeHRVNow
    {
    __LOG_METHOD_START;
    int const val = /*72*/ 68 + ( rand( ) % 20 );
    __LOG_METHOD_END;
    return val;
    }



//   ***** DEPRECATED *****
- ( void ) sleepInitAnalysis: ( NSDate * ) startTime
                     endTime: ( NSDate * ) endTime
    {
    __LOG_METHOD_START;
    [ self sleepInitAnalysis ];
    __LOG_METHOD_END;
    return;
    }


- ( void ) sleepInitAnalysis
    {
    __LOG_METHOD_START;
//    [ self sleepInitAnalysis: nil
//                     endTime: nil
//    ];
    __LOG_METHOD_END;
    return;
    }


- ( void ) sleepAddData: ( NSDate * ) sampleTime
             sleepPhase: ( int ) sleepPhase
    {
    __LOG_METHOD_START;
    __LOG_METHOD_END;
    return;
    }


//   ***** DEPRECATED *****
- ( void ) sleepRequestAnalysis
    {
    __LOG_METHOD_START;
    [ self sleepRequestAnalysis: nil
                        endTime: nil
    ];
    __LOG_METHOD_END;
    return;
    }


- ( void ) sleepRequestAnalysis: ( NSDate * ) startTime
                        endTime: ( NSDate * ) endTime
    {
    __LOG_METHOD_START;
    __LOG_METHOD_END;
    return;
    }


- ( void ) sleepSetInterval: ( int ) minutes
               completeData: ( bool ) fullDataSet
    {
    __LOG_METHOD_START;
    __LOG_METHOD_END;
    return;
    }


@end  //  end @implementation TGLibEmulator

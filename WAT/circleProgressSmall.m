//
//  CircleProgress.m
//  WAT
//
//  Created by Julia on 9/23/13.
//  Copyright (c) 2013 NeuroSky. All rights reserved.
//

#import "circleProgressSmall.h"
#import "Constant.h"

#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)

@implementation circleProgressSmall
@synthesize foreImage = _foreImage;
@synthesize iconImage = _iconImage;
@synthesize textColor = _textColor;
@synthesize targetString = _targetString;
@synthesize actualString = _actualString;
@synthesize progress = _progress;
@synthesize pString;
@synthesize innerLabelTextColor;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    
    UIImage *img = [UIImage imageNamed:@"progress_1"];
    [img drawInRect:rect];
    
    [self drawProgress:rect];
    
    //[iconImage drawInRect:CGRectMake(22, 22, 102, 102)];
    
    //actual
    UILabel *label= [[UILabel alloc] initWithFrame:CGRectMake(44, 4, 80, 50)];
    label.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:19.0];
    label.backgroundColor = [UIColor clearColor];
    label.text = pString;
    //[UIColor colorWithRed:2.0/255.0 green:190.0/255.0 blue:253.0/255.0 alpha:1.0];
    label.textColor = innerLabelTextColor;
    [label drawTextInRect:CGRectMake(18, 3, 80, 50)];
    
}
-(void)drawProgress:(CGRect)rect{
    CGFloat progressAngle = _progress * 360.0 - 90;
    CGPoint center = CGPointMake(rect.size.width / 2.0f+rect.origin.x, rect.size.height / 2.0f+rect.origin.y);
    CGFloat radius = MIN(rect.size.width, rect.size.height) / 2.0f;
    
//    CGRect square;
//    if (rect.size.width > rect.size.height)
//    {
//        square = CGRectMake((rect.size.width - rect.size.height) / 2.0, 0.0, rect.size.height, rect.size.height);
//    }
//    else
//    {
//        square = CGRectMake(0.0, (rect.size.height - rect.size.width) / 2.0, rect.size.width, rect.size.width);
//    }

    
    CGFloat circleWidth = radius * 0.43;
    //    CGFloat circleWidth = radius;
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    
    
    
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    
    [path appendPath:[UIBezierPath bezierPathWithArcCenter:center
                                                    radius:radius
                                                startAngle:DEGREES_TO_RADIANS(-90)
                                                  endAngle:DEGREES_TO_RADIANS(progressAngle)
                                                 clockwise:YES]];
    
    CGPoint point;
    point.x = (cos(DEGREES_TO_RADIANS(progressAngle)) * (radius - circleWidth/2)) + center.x;
    point.y = (sin(DEGREES_TO_RADIANS(progressAngle)) * (radius - circleWidth/2)) + center.y;
    
    [path addArcWithCenter:point
                    radius:circleWidth/2
                startAngle:DEGREES_TO_RADIANS(progressAngle)
                  endAngle:DEGREES_TO_RADIANS(270.0 + progressAngle - 90.0)
                 clockwise:YES];
    
    [path addArcWithCenter:center
                    radius:radius-circleWidth
                startAngle:DEGREES_TO_RADIANS(progressAngle)
                  endAngle:DEGREES_TO_RADIANS(-90)
                 clockwise:NO];
    
    [path closePath];
    
    [path addClip];
    
    
    UIImage *image = foreImage;
    [image drawInRect:rect];
    
    
    [[UIColor grayColor] setFill];
    
    CGContextRestoreGState(context);
}
-(void)setForeImage:(UIImage *)fImage{
    foreImage = fImage;
    [self setNeedsDisplay];
}

-(void)setIconImage:(UIImage *)iImage{
    iconImage = iImage;
    [self setNeedsDisplay];
}
-(void)setTextColor:(UIColor *)color{
    textColor = color;
    [self setNeedsDisplay];
}
-(void)setTargetString:(NSString *)tString{
    targetString = tString;
    [self setNeedsDisplay];
    
}
-(void)setActualString:(NSString *)aString{
    actualString = aString;
    [self setNeedsDisplay];
    
}

- (void)setProgress:(double)p{
    _progress = MIN(1.0, MAX(0.0, p));
    
    [self setNeedsDisplay];
}


@end

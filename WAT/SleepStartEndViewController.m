//
//  SleepStartEndViewController.m
//  WAT
//
//  Created by neurosky on 12/30/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import "SleepStartEndViewController.h"
#import "CommonValues.h"
#import "Constant.h"

@interface SleepStartEndViewController ()

@end

@implementation SleepStartEndViewController
-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (void)viewDidLoad {
    __LOG_METHOD_START;
    [super viewDidLoad];
    myPicker.date=_pickDateDisplay;
    __LOG_METHOD_END;
}



-(NSString*) stringFromDate:(NSDate *)date
{
    __LOG_METHOD_START;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    [dateFormatter setTimeZone:[[CommonValues timeDateFormatter:DateFormatterDate] timeZone] ];
    NSString *_pickDateString = [dateFormatter stringFromDate:date];
    __LOG_METHOD_END;
    return _pickDateString;
}

- (IBAction)cancelClick:(id)sender
{
    __LOG_METHOD_START;
    [CommonValues setSleepStartEndTimeBackFlag:0];
    [self dismissViewControllerAnimated:YES completion:nil];
    __LOG_METHOD_END;
}

- (IBAction)saveClick:(id)sender
{
    __LOG_METHOD_START;
    [CommonValues setSleepStartEndTimeBackFlag:1];
    
    
    if ([CommonValues getSleepStartEndTimeEnterFlag] == 0)
    {
        _pickDateStartString=[self stringFromDate:myPicker.date];
        
    }
    else
    {
        
        _pickDateEndString=[self stringFromDate:myPicker.date];
        
    }
    
    if (self.saveBlock)
    {
        self.saveBlock();
    }
    __LOG_METHOD_END;
}

- (void)didReceiveMemoryWarning
{
    __LOG_METHOD_START;
    [super didReceiveMemoryWarning];
    __LOG_METHOD_END;
}

@end

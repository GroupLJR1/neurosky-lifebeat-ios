//
//  LineGraphView.h
//  MindView
//
//  Created by NeuroSky on 10/13/11.
//  Copyright 2012. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Accelerate/Accelerate.h>
#import "math.h"

#define AVERAGE_COUNT 50
#define DECIMATE 2
#define BANDPASS_ENABLED NO
#define CARDIO_SCALER 0.16
#define EEG_SCALER 1.0
#define LINE_WIDTH 1.5
#define ROTATION YES

#define MIN_POINT_COUNT_IN1DIP 4     //4个点的宽度 > 1个像素

#define REFRESH_RECT_WIDTH 64           //64points width

@interface LineGraphView : UIView {
    UIBezierPath *path;// = [UIBezierPath bezierPath];
    UIBezierPath *cursor;// = [UIBezierPath bezierPath];
    NSNumber *tempValue;// = (NSNumber *)[data objectAtIndex:i];
    CGPoint tempPixel;// =
    
    NSMutableArray *data;
    
    double xAxisMin;
    double xAxisMax;
    double yAxisMin;
    double yAxisMax;
    int dataRate;
    
    NSLock *dataLock;
    
    int startIndex;
    
    int index;
    double scaler;
    
   
    NSThread *redraw;
    
    UIColor * backgroundColor;
    UIColor * lineColor;
    UIColor * cursorColor;
    BOOL cursorEnabled;
    BOOL gridEnabled;
    BOOL touchEnabled;
    BOOL rotated;
    
@private
    SEL addTo;
    id target;
    
    UIPinchGestureRecognizer * pinch;
    UITapGestureRecognizer * taptap;
    BOOL newData;
    int decimateCount;
    //int testIndex;
    
    /* DC offset removal */
    int averageCount;
    double average;
    BOOL offsetRemovalEnabled;
    
    // for bandpass filter
    float * ECGbuffer;
    float * filteredPoint;
    
    int ECGBufferCounter;
    int ECGBufferLength;
    int hpcoeffLength;
//    BOOL freshEKGFlag;
    
    float full_height;
    float full_width;
    
    float dipPerPoint;
    
    int rawCoutIndex;
    //int tmpRawIndex;
}

- (CGPoint)point2PixelsWithXValue:(double) xValue yValue:(double) yValue;
- (void)addPoint;
- (void)addValue:(double) value;
- (void)twoFingerPinch:(UIPinchGestureRecognizer *)recognizer;
- (void)doubleTap;
- (void)cleardata;

@property UIColor * backgroundColor;
@property UIColor * lineColor;
@property UIColor * cursorColor;
@property BOOL cursorEnabled;

@property BOOL freshEKGFlag;

@property id target;
@property SEL addTo;

@property double scaler;
@property double xAxisMax;

@end

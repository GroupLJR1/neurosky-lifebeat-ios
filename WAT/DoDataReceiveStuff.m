//
//  DoDataReceiveStuff.m
//  WAT
//
//  Created by neurosky on 14-2-15.
//  Copyright (c) 2014年 Julia. All rights reserved.
//

#import "DoDataReceiveStuff.h"
#import "TGLibEKGdictionary.h"

@implementation DoDataReceiveStuff

- (void)sleepAnalysisResults:(TGEKGsleepResult)result
                   startTime:(NSDate *)startTime
                     endTime:(NSDate *)endTime
                    duration:(int)duration          // in minutes
                    preSleep:(int)preSleep          // in minutes
                    notSleep:(int)notSleep          // in minutes
                   deepSleep:(int)deepSleep         // in minutes
                  lightSleep:(int)lightSleep        // in minutes
                 wakeUpCount:(int)wakeUpCount       // number of wake ups after 1st sleep
                  totalSleep:(int)totalSleep
                     preWake:(int)preWake           // number of minutes active before the endTime
                  efficiency:(int)efficiency        // integer sleep efficiency
    {
    return;
    }

-(void)sleepAnalysisSmoothData:(int)samepleRate
                     sleepTime:(NSMutableArray*)sleepTimeArray
                    sleepPhase:(NSMutableArray*)sleepPhaseArray
    {
    return;
    }


-(id)initWithLabel:(UILabel *)hrvLabel{
    self = [super init];
    
    if (self) {
        _hrvLabel = hrvLabel;
    }
    
    return self;

}

-(void)dataReceived:(NSDictionary *)data{
    
    
    if([data valueForKey:dicEKGhrv]) {
              int  cardioZoneHRV = [[data valueForKey:@"CardioZoneHRV"] intValue];
              NSLog(@" stuff cardioZoneHRV  ======  %d",cardioZoneHRV);
        
             dispatch_async( dispatch_get_main_queue(), ^ {
       
                _hrvLabel.text = [NSString stringWithFormat:@"%d",cardioZoneHRV];
        
              });
    }


}
@end

//
//  SubCateViewController.h
//  top100
//
//  Created by Dai Cloud on 12-7-13.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CateViewController.h"
#import "LineGraphView.h"

@interface SubCateViewController : UIViewController{
    
    __block NSArray *array;
    __block NSArray *rawArray;
    NSThread *addDataThread;
    int rawIndex;
    int  cardioZoneHeartRate;
    int  cardioZoneHRV;
    int  cardioZoneBreathingIndex;
    
    NSArray* cardioZone3D;
    
    
}

//@property (strong, nonatomic) NSArray *subCates;
@property (strong, nonatomic) NSString *fileURL;
@property (strong, nonatomic) IBOutlet LineGraphView *ekgView;
@property (strong, nonatomic) CateViewController *cateVC;

@end

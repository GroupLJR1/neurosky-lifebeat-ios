//
//  ProfileModel.h
//  WAT
//
//  Created by NeuroSky on 1/27/15.
//  Copyright (c) 2015 Julia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Profile : NSObject

- (void)setBackPreviousInfo2SDK;
- (void)writeToTemp;
@end

//
//  SleepModifyViewController.h
//  WAT
//
//  Created by neurosky on 12/29/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>
#import "CommonValues.h"
#import "Constant.h"
#import "SleepStartEndViewController.h"


@interface SleepModifyViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    
    __weak IBOutlet UIImageView *select_img;
    
    __weak IBOutlet UIImageView *open_img;
    __weak IBOutlet UIView *myWholeView;
    __weak IBOutlet UILabel *dateLabel;
    __weak IBOutlet UILabel *bandDisplayLabel;
    __weak IBOutlet UILabel *startLabel;
    __weak IBOutlet UILabel *stopLabel;
    __weak IBOutlet UILabel *titleLabel;
    sqlite3 *db;
    NSMutableArray *sleepTimesArr;
    BOOL selectedFlag;
    __weak IBOutlet UIImageView *selected_img;
    __weak IBOutlet UITableView *myTableView;
    __weak IBOutlet UIImageView *backgroundImg;
    __weak IBOutlet UILabel *startTimeLabel;
    UIDatePicker *myDatePicker;
    NSDate *timerSelectedTime;
    NSMutableArray *dbTimeArray;
}

@property (strong,nonatomic) NSString* dateFromSleep;
- (IBAction)backClick:(id)sender;

@end

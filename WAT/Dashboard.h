//
//  DashboardModel.h
//  WAT
//
//  Created by NeuroSky on 1/27/15.
//  Copyright (c) 2015 Julia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "CommonValues.h"
#import "FileOperation.h"
#import "Constant.h"
#import "TGBleManager.h"
#import "StringOperation.h"

@interface Dashboard : NSObject

- (void)deleteAllSyncData;
- (void)insertSleepData2SqilteWithTime: (NSString *)time withPhase :(TGsleepPhaseType) phase withInitCode:(TGsleepInitReason) code database :(sqlite3 *)mydb;
- (void)userProfileWriteToFile;
- (NSString *)getFomattedTimeStr;
- (BOOL)returnFlagWithURLRequest:(NSMutableURLRequest *)request;
- (NSMutableURLRequest *) upload2PHPServer:(NSString*)txtfile type:(NSString*)dataType;
- (BOOL)runECG;
- (BOOL)runECG_Realtime;
- (BOOL)runUserPro;
- (BOOL)runSleep;
- (BOOL)runFat;
- (BOOL)runPED;
- (BOOL)runSyncLog;
- (void)deleteSyncLogBeforeThisDate:(NSString *)dateStr;
- (NSString *)sleepFilePath;
- (NSString *)pedFilePath;
- (NSString *)fatFilePath;
- (void)write2SyncLogFileWithTimeStr:(NSString *)timeStr withManualFlag:(BOOL)manualFlag withPassFlag:(BOOL)passFlag withDataReceived:(int)received withTotalDataAvailable:(int)available;
- (NSMutableArray *)pedTotalInOneDayWithDate :(NSString*)dateStr WithDB : (sqlite3 *)db;
- (NSMutableArray *)sleepArrWithDate :(NSString *)sleepDateStr withDB:(sqlite3 *)db;
- (NSArray *)findLongestSleepTimeFromArr :(NSMutableArray *)sleepTimesArr;
- (void)startAnalysisSleepWithDate:(NSString *)dateFromSleep withDB:(sqlite3 *)db;
@end

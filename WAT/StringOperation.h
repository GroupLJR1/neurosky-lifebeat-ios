//
//  StringOperatiom.h
//  WAT
//
//  Created by NeuroSky on 1/27/15.
//  Copyright (c) 2015 Julia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TGBleManager.h"
#import "TGLibEKG.h"
#import "CommonValues.h"

@interface StringOperation : NSObject

+ (NSString *)path2TimeStr: (NSString *)txtfile;
+ (NSString *)getLast14SN;
+ (NSString *) getVersionNumbers;
+ (NSString *)intToString:(NSInteger)value;
+ (BOOL) inputValidation: (NSString *)textNum;
+ (BOOL) inputValidation2: (NSString *)textNum;
+ (BOOL) inputValidation3: (NSString *)textNum;
+ (BOOL) emailValidation: (NSString *)textNum;
+ (NSString *)trim:(NSString*)String;
+ (NSString *)stringFromInt :(int)number;
+ (NSString *)nowTimeStringForLastSyncDisplay;
+ (NSString *)formatDateFromTimeArr :(NSArray *)time;
+ (NSString *) calculateStartTime:(NSString *) startPartTime stopTime:(NSString *)endPartTime date:(NSString *)dateString;

@end

//
//  SendEKGDataOperation.m
//  Hearty
//
//  Created by NeuroSky on 8/29/12.
//  Copyright (c) 2012 NeuroSky. All rights reserved.
//

#import "SendEKGDataOperation.h"


@implementation SendEKGDataOperation
@synthesize isPlayClicked;
@synthesize getRawCount;
@synthesize  isFinishThread;
@synthesize changeView;


-(id)initWithAction:(id)theTarget action:(SEL)theAction fileURL:(NSString *)theURL resumePoint:(int)theResumePoint{

    self = [super init];
    if(self){
        target = theTarget;
        action = theAction;
        fileURL = theURL;
        i = theResumePoint;
        
        
        isPlayClicked = YES;
        isFinishThread = NO;
    }
    return self;
}

-(void)main{
    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsPath = [paths objectAtIndex:0];
//    //@"aa@aa.nn/2012-08-30_02:52:27.plist"
//    NSString *filePath = [documentsPath stringByAppendingPathComponent:fileURL];
    
    data = [[NSMutableArray alloc] initWithContentsOfFile:fileURL] ;
    item = [[NSMutableArray alloc] initWithCapacity:2] ;
    
    item = [data lastObject];
    NSNumber *rawCount = [item objectAtIndex:1];
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    [target performSelector:getRawCount withObject:rawCount];
#pragma clang diagnostic pop

    
    while(i<data.count && isPlayClicked){
        
        item = [data objectAtIndex:i];
        
         str = [item objectAtIndex:0];
        
        i++;
        
        if(i == data.count) {
            i = 0;
        }
        
        if([str isEqualToString:@"raw"])
        {
            NSNumber *vv = [item objectAtIndex:1];
            [target performSelectorOnMainThread:action withObject:vv  waitUntilDone:NO];
        }else
        {
           continue;
        }
        
        
    //NSNumber *randomV = [NSNumber numberWithInt:(arc4random() % 2000)-1000];
    //[target performSelectorOnMainThread:action withObject:randomV waitUntilDone:NO];
        
        usleep(1953);
        
    }
    
    //thread run finish;
    if(i == data.count){
    
    self.isFinishThread = YES;
    [target performSelectorOnMainThread:changeView withObject:nil  waitUntilDone:NO];
        
    }

}

-(void)start{
    [super start];
}

- (void)cancel {
    isPlayClicked = NO;
}


-(BOOL)isConcurrent{
    return [super isConcurrent];
}

-(BOOL)isExecuting{
    return [super isExecuting];
}
-(BOOL)isFinished{
    return [super isFinished];
}
-(BOOL)isCancelled{
    return [super isCancelled];
}


@end

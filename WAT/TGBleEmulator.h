//
//  TGBleEmulator.h
//  WAT
//
//  Created by Mark I. Walsh on 2/22/15.
//  Copyright 2015 Julia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"
#import "TGBleManager.h"


#if !defined( TGBLEEMULATOR_H_INCLUDED )
#define TGBLEEMULATOR_H_INCLUDED

#if !defined( USE_THINKGEAR_LIBRARY )
#define TGBleManager TGBleEmulator
#endif  // !defined( USE_THINKGEAR_LIBRARY )


@interface TGBleEmulator : NSObject

{

}

@property (nonatomic,readonly) TGBleConnectStatus status;
@property (nonatomic,readonly,getter=isConnected) bool connected;

@property (nonatomic,readonly) NSInteger age;
@property (nonatomic) NSInteger birthYear;
@property (nonatomic) NSInteger birthDay;
@property (nonatomic) int birthMonth;
@property (nonatomic,getter=isFemale) bool female;
@property (nonatomic) NSInteger height;
@property (nonatomic) NSInteger weight;
@property (nonatomic) NSInteger walkingStepLength;
@property (nonatomic) NSInteger runningStepLength;
@property (nonatomic,getter=isDisplayImperialUnits) bool displayImperialUnits;
@property (nonatomic,getter=isDisplayTime24Hour) bool displayTime24Hour;
@property (nonatomic,getter=isBandOnRight) bool bandOnRight;
@property (nonatomic,getter=isAutoSyncEnabled) bool autoSyncEnabled;// NO LONGER USED - to be deleted


@property (nonatomic) int goalDurationHour;
@property (nonatomic) int goalDurationMinute;
@property (nonatomic) int goalDurationSecond;
@property (nonatomic) int goalSteps; // 0 to 65535, 0 = set to FW default (10,000)
@property (nonatomic) int alarmHour; // any value outside 0 to 23 disables the Alarm
@property (nonatomic) int alarmMinute; // any value outside 0 to 59 disables the Alarm
@property (nonatomic) int alarmRepeat;
// bit mask:  (only low order 8 bits have meaning, all others are ignored and should be set as zero)
// bit 0 = 0 - alarm functions once
//
// bit 0 = 1 - alarm repeats each day
// bit 1 = 1 - alarm repeat is suppressed on Monday
// bit 2 = 1 - alarm repeat is suppressed on Tuesday
// bit 3 = 1 - alarm repeat is suppressed on Wednesday
// bit 4 = 1 - alarm repeat is suppressed on Thursday
// bit 5 = 1 - alarm repeat is suppressed on Friday
// bit 6 = 1 - alarm repeat is suppressed on Saturday
// bit 7 = 1 - alarm repeat is suppressed on Sunday
//
// examples:
// 0x00 = disables repeat, if Alarm Hour and Minute are in range the Alarm functions once
// 0x11 = enables repeat Alarm every day except, Thursday

@property (nonatomic,readonly) NSString *advName;
@property (nonatomic,readonly) NSString *hwModel;
@property (nonatomic,readonly) NSString *hwManufacturer;
@property (nonatomic,readonly) NSString *hwSerialNumber;
@property (nonatomic,readonly) NSString *hwVersion;
@property (nonatomic,readonly) NSString *fwVersion;
@property (nonatomic,readonly) NSString *swVersion;
@property (nonatomic,readonly) NSString *sdkVersion;
@property (nonatomic,readonly) NSData *bondToken;
@property (nonatomic,readonly) NSString *mfgID;
@property (nonatomic,readonly) NSString *connectedID;
@property (nonatomic,readonly) int batteryLevel;

@property (nonatomic, assign) id delegate;


//  create/access a "singleton" object for this class...
+ ( TGBleEmulator * ) sharedTGBleManager;

- ( int ) getVersion;
- (void)teardownManager;

- (void)tryBond;
- (void)takeBond;
- (void)adoptBond:(NSData*) bondToken serialNumber:(NSString*) sn;
- (void)releaseBond;
- (void)tryDisconnect;

- (void)candidateScan:(NSArray*) nameList;
- (void)candidateConnect:(NSString*) candidateID;
- (void)candidateStopScan;

- (void)sleepInitAnalysis:(NSDate*) startTime endTime:(NSDate *) endTime DEPRECATED_ATTRIBUTE;
- (void)sleepInitAnalysis;
- (void)sleepAddData:(NSDate*) sampleTime sleepPhase:(int) sleepPhase;
- (void)sleepRequestAnalysis DEPRECATED_ATTRIBUTE;
- (void)sleepRequestAnalysis: (NSDate*) startTime endTime:(NSDate *) endTime;
- (void)sleepSetInterval:(int)minutes completeData:(bool) fullDataSet;

- (void)trySetSecurity; // to be deleted, no longer functional
- (void)tryStartRT;
- (void)tryStopRT;
- (void)trySyncData;
- (void)trySyncData:(bool) all;
// if all == true then all of the SYNC data received is transfered to the application
// if all == false then only new SYNC data is transfered to the application
- (void)tryEraseData;

- (int)computeHRVNow;

- (void)setupManager; // auto connect mode
- (void)setupManager:(bool) manualMode; // true = manual mode, false = auto connect

- (void)enableLogging; // enable additional NSLog messages
- (void)stopLogging; // disable the additional messages
- (void)redirectLoggingToFile; // redirect ALL NSLog message to a file in TG_log/Console
- (void)restoreLoggingToConsole; // return NSLog message to the original file (console/stderr)
- (void)diagnosticFilesEnabled; // NSK only, DO NOT USE, enable writing to files in the TG_log directory
- (void)diagnosticFilesDisabled:(bool) option; // NSK only, DO NOT USE disable writing to files in the TG_log directory

- (void)enableVBM;//test only
- (void)disableVBM;//test only

- (BOOL)mfg_support:(int) arg1 arg2:(int)arg2; // NSK only

- (void)tryFwDown;
- (void)askCurrentCount;

@end

#endif  // !defined( TGBLEEMULATOR_H_INCLUDED )

//
//  ProfileViewController.h
//  WAT
//
//  Created by neurosky on 5/23/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TGBleManager.h"
#import "TGBleManagerDelegate.h"
#import "ZSYPopoverListView.h"
#import "Profile.h"

@interface ProfileViewController : UIViewController<UITextFieldDelegate,TGBleManagerDelegate,ZSYPopoverListDatasource,ZSYPopoverListDelegate>
{

   
  //  NSString  *height;
  //  NSString  *weight;
    NSString  *walkLength;
    NSString  *runLength;
    NSString *name;
    BOOL is_band_right;
    BOOL is_female;
    BOOL successBondedFlag;
    BOOL isBonded;
      ZSYPopoverListView *listView ;
    NSMutableArray *devicesArray;
    NSMutableArray *mfgDataArray;
    NSMutableArray *rssiArray;
    NSMutableArray *candIDArray;
    NSMutableArray *devicesIDArray;
    
    NSString *readyToSaveDeviceName;
    NSString *readyToSaveDeviceID;
    
    NSTimer *timeOutTimer;
    BOOL firstEnterCandiDel;
    
    NSTimer *showListTimer;
    float screenHeight;
    NSString *birthdayFromSDK;
    BOOL lostBeforeMainOperation;
    __weak IBOutlet UIView *myWholeView;
    __weak IBOutlet UILabel *profile_title_label;
    
    __weak IBOutlet UITextField *birthDateText;
    __weak IBOutlet UITextField *runningLengthText;
    __weak IBOutlet UITextField *heightText;
    
    __weak IBOutlet UITextField *nameText;
    __weak IBOutlet UITextField *walkingLengthText;
    __weak IBOutlet UITextField *weightText;
    __weak IBOutlet UIButton *femaleBtn;
    __weak IBOutlet UIButton *maleBtn;
    __weak IBOutlet UIButton *rightBtn;
    __weak IBOutlet UIButton *leftBtn;
    __weak IBOutlet UIButton *hourBtn;
    BOOL is24hour;
    BOOL isImperialUnits;
    __weak IBOutlet UIButton *unitsBtn;
    __weak IBOutlet UIButton *walkingLengthAutoBtn;
    __weak IBOutlet UIButton *walkingLengthManualBtn;
    __weak IBOutlet UIButton *runningLengthAutoBtn;
    __weak IBOutlet UIButton *runningLengthManualBtn;
    BOOL walkingAutoFlag;
    BOOL runningAutoFlag;
    Profile *profile;
    UIAlertController *alertController;
    
}
@property (weak, nonatomic) IBOutlet UIButton *femaleBtn;
@property (weak, nonatomic) IBOutlet UIButton *maleBtn;
@property (nonatomic, retain) NSIndexPath *selectedIndexPath;
- (IBAction)backClick:(id)sender;
- (IBAction)bgTouch:(id)sender;
- (IBAction)femaleClick:(id)sender;
- (IBAction)maleClick:(id)sender;
- (IBAction)rightClick:(id)sender;
- (IBAction)leftClick:(id)sender;
- (IBAction)hour24Click:(id)sender;
- (IBAction)unitsClick:(id)sender;
- (IBAction)birthdayBtnClick:(id)sender;
- (IBAction)walkingLengthAutoClick:(id)sender;
- (IBAction)walkingLengthManualClick:(id)sender;
- (IBAction)runningLengthAutoClick:(id)sender;
- (IBAction)runningLengthManualClick:(id)sender;

- (IBAction)saveBtnClick:(id)sender;

@end

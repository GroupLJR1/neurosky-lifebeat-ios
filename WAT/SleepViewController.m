//
//  SleepViewController.m
//  WAT
//
//  Created by neurosky on 14-3-7.
//  Copyright (c) 2014年 Julia. All rights reserved.
//

#import "SleepViewController.h"
#import "Constant.h"

#import "TGBleEmulator.h"

@interface SleepViewController ()


@end



@implementation SleepViewController
@synthesize my_scrollview,hv,my_scrollview4S;


- ( void ) dataReceived: ( NSDictionary * ) data { return; }


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    __LOG_METHOD_START;
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    __LOG_METHOD_END;
    return self;
}
-(NSUInteger)supportedInterfaceOrientations
{
    __LOG_METHOD_START;
    __LOG_METHOD_END;
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    __LOG_METHOD_START;
    __LOG_METHOD_END;
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    __LOG_METHOD_START;
    __LOG_METHOD_END;
    return UIInterfaceOrientationPortrait;
}

- (void)viewDidLoad
{
    __LOG_METHOD_START;
    [super viewDidLoad];
    NSLog(@"SleepViewController--------5S");
    myWholeView.frame = CGRectMake(0, [CommonValues offsetHeight4DiffiOS], 320, 568-20);
    db = [CommonValues getMyDB];
    sleep_ = [[Sleep alloc] init];
//    if ([CommonValues getAutoSyncingFlag]) {
//        NSLog(@"AutoSyncing, set delegate to Dashboard---");
//        [[TGBleManager sharedTGBleManager]setDelegate:[CommonValues getDashboardID]];
//    }
//    else{
//        [[TGBleManager sharedTGBleManager]setDelegate:self];
//    }
    font =  [UIFont fontWithName: DEFAULT_FONT_NAME size:30.0];
    titleLabel.font = [UIFont fontWithName: DEFAULT_FONT_NAME size:23.0];
    titleLabel.textColor = [UIColor grayColor];
    DateLabel.font =  [UIFont fontWithName: DEFAULT_FONT_NAME size:20.0];
    DateLabel.backgroundColor = [UIColor clearColor];
    DateLabel.textColor = [UIColor grayColor];
    wake_count_label.font =
    awakeLabel.font =
    deepLabel.font =
    beforeLabel.font =
    efficiencyLabel.font =
    light_sleep_label.font = font;
    wake_count_label.text =
    awakeLabel.text =
    deepLabel.text =
    beforeLabel.text =
    efficiencyLabel.text =
    light_sleep_label.text = @"0";
    sleepIndex = 0;
    
    //reset ystLast
    ystLastSleepPhase = 1;
    ystLastSleepTime = @"";
    // Do any additional setup after loading the view from its nib.
    
    // sleepGraphDataSouceArr = [[NSArray alloc]i];
    
    sleepGraphDataSouceArr = [[NSMutableArray alloc] init];
    //    [MutableArray insertObject:@"3" atIndex:0];
    //     [MutableArray insertObject:@"4" atIndex:1];
    //    NSLog(@"MutableArray [1] = %@",MutableArray[1]);
    
    
    df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    local_solid_df = [[NSDateFormatter alloc] init];
    // NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
    
    NSTimeZone * const gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [ local_solid_df setTimeZone: gmt ];
    //  local_solid_df

    dbTimeArray = [[NSMutableArray alloc]init];
    SleepArray = [[NSArray alloc]init];
    sleepPhaseArray = [[NSMutableArray alloc]init];
    
    sleepTimeArray = [[NSMutableArray alloc]init];
    SleepArray = [ [ NSMutableArray alloc ] initWithObjects: @"50"
                                                           , @"216"
                                                           , @"195"
                                                           , @"163"
                                                           , @"85"
                                                           , @"208"
                                                           , @"300"
                                                           , @"63"
                                                           , @"75"
                                                           , @"106"
                                                           , @"70"
                                                           , @"217"
                                                           , @"293"
                                                           , @"86"
                                                           , @"114"
                                                           , @"76"
                                                           , @"69"
                                                           , nil
                 ];
    
    // different device different layout
    CGRect rect_screen = [[UIScreen mainScreen]bounds];
    CGSize size_screen = rect_screen.size;
    screenHeight = size_screen.height;
    NSLog(@"screenHeight = %f",screenHeight);
    my_scrollview.center = CGPointMake(160, (315-20+[CommonValues offsetHeight4DiffiOS]));
    [self.view addSubview:my_scrollview];
    UIFont *timeFont = [UIFont fontWithName: DEFAULT_FONT_NAME size:20.0];
    
    firstLabel.font = timeFont;
    secondLabel.font = timeFont;
    thirdLabel.font = timeFont;
    
    dbTimeArray =  [CommonValues getSleepTimeArr :db];
   sleepIndex = [CommonValues getActivityIndex];
   
    
    /*先注释掉  为了modify sleep 返回处理
    if ([CommonValues startSleepAnalysis:sleepIndex :db] == -1) {
        [sleepGraphDataSouceArr removeAllObjects];
        [self drawGraph];
    }
    
    [self refreshSleepTimeAxis];
     */
    
    __LOG_METHOD_END;
    return;

}

-(void)startSleepAnalysis
{
    __LOG_METHOD_START;
    [[TGBleManager sharedTGBleManager]setDelegate:self];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    BOOL setSleepInApp = [userDefault boolForKey:SET_SLEEP_IN_APP];
    if (setSleepInApp) {
        //To set start and end time, whichever app
         NSLog(@"To set start and end time, whichever app");
        NSString *startTime = [userDefault objectForKey:APP_SLEEP_START_TIME];
        NSString *endTime = [userDefault objectForKey:APP_SLEEP_END_TIME];
        [CommonValues sleepAnalysisWithStartTime:startTime withEndTime:endTime withFlag:1 withDataBase:db];
        gStartTime = startTime;
        gEndTime = endTime;
    }
    else{
        //Button to start and stop times, whichever band corresponding to the time period selected band
        NSLog(@"Button to start and stop times, whichever band -");
        NSString * const startTime = [ userDefault objectForKey: BAND_SLEEP_START_TIME ];
        NSString * const endTime   = [ userDefault objectForKey: BAND_SLEEP_END_TIME ];
        [ CommonValues sleepAnalysisWithStartTime: startTime
                                      withEndTime: endTime
                                         withFlag: 0
                                     withDataBase: db
        ];
        gStartTime = startTime;
        gEndTime = endTime;
    }
    NSLog(@"gStartTime = %@, gEndTime = %@",gStartTime,gEndTime);
    /*
     if ([CommonValues startSleepAnalysis:sleepIndex :db] == -1) {
     [sleepGraphDataSouceArr removeAllObjects];
     [self drawGraph];
     }
     
     [self refreshSleepTimeAxis];
     */
    [ self refreshSleepTimeAxis ];
    NSLog(@"setDelegate to Dashboard,prepare to autoSync");
    [ [ TGBleManager sharedTGBleManager ] setDelegate: [ CommonValues getDashboardID ] ];
    __LOG_METHOD_END;
    return;

}

-(void)viewWillAppear:(BOOL)animated
{
    __LOG_METHOD_START;
    NSLog(@"iphoen5 sleep viewWillAppear");

    [ super viewWillAppear: animated ];

    if ([CommonValues modifyBackFlag]) {
        [[TGBleManager sharedTGBleManager]setDelegate:self];
        [self startSleepAnalysis];
        
         [CommonValues setModifyBackFlag:NO];
    }
    else{
        [[TGBleManager sharedTGBleManager]setDelegate:self];
        [self refreshSleepView];
    }
    __LOG_METHOD_END;
    return;

}

-(void)refreshSleepTimeAxis
{
    __LOG_METHOD_START;

//    if (sleepPhaseArray.count > 0 ) {
    
    NSString *startTime = gStartTime;
    NSString *endTime = gEndTime;
//        NSString *startTime = [CommonValues getSleepStartTime];
//        NSString *endTime = [CommonValues getSleepEndTime];
        NSDateFormatter *ddd = [[NSDateFormatter alloc]init];
        [ddd setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *startDate =  [ddd dateFromString:startTime];
        NSDate *endDate =  [ddd dateFromString:endTime];
        NSLog(@"startDate = %@",startDate);
        NSLog(@"endDate = %@",endDate);
        NSTimeInterval delta =   [endDate timeIntervalSinceDate:startDate];
        NSDate *midDate = [startDate dateByAddingTimeInterval:delta/2];
        
        
        [ddd setDateFormat:@"HH:mm"];
        NSString *startTimeX = [ddd stringFromDate:startDate];
        NSString *endDateX = [ddd stringFromDate:endDate];
        NSString *midDateX = [ddd stringFromDate:midDate];
        
        NSLog(@"startTimeX = %@",startTimeX);
        NSLog(@"endDateX = %@",endDateX);
        leftTimeLabel.text = startTimeX;
        rightTimeLabel.text =  endDateX;
        midTimeLabel.text = midDateX;
        
//    }
//    
//    else{
//        leftTimeLabel.text = @"";
//        rightTimeLabel.text =  @"";
//        midTimeLabel.text = @"";
//        
//    }
    __LOG_METHOD_END;
    return;

}

- (void)didReceiveMemoryWarning
{
    __LOG_METHOD_START;
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    __LOG_METHOD_END;
    return;
}

- (IBAction)right_click:(id)sender
{
    __LOG_METHOD_START;
    NSLog(@"right_click--");
    if (sleepIndex < 1) {
        return;
    }
    [[TGBleManager sharedTGBleManager]setDelegate:self];
    //reset ystLast
    ystLastSleepPhase = 1;
    ystLastSleepTime = @"";
    
    sleepIndex--;
    [CommonValues setActivityIndex:sleepIndex];
    [self updateLabelValue:[dbTimeArray objectAtIndex:sleepIndex]];
    [sleepTimeArray removeAllObjects];
    [sleepPhaseArray removeAllObjects];
    //    [self getDaySleepFromDb];
    /*
    if ([CommonValues startSleepAnalysis:sleepIndex :db] == -1) {
        [sleepGraphDataSouceArr removeAllObjects];
        [self drawGraph];
    }
    [self refreshSleepTimeAxis];
     */
        [self refreshSleepView];
//    NSLog(@"setDelegate to Dashboard,prepare to autoSync");
//    [[TGBleManager sharedTGBleManager]setDelegate:[CommonValues getDashboardID]];
    __LOG_METHOD_END;
    return;

}

- (IBAction)left_click:(id)sender
{
    __LOG_METHOD_START;
    NSLog(@"left_click--");
    
    
    if (sleepIndex+1 >= dbTimeArray.count ) {
        __LOG_METHOD_END;
        return;
    }
    
    [[TGBleManager sharedTGBleManager]setDelegate:self];
    //reset ystLast
    ystLastSleepPhase = 1;
    ystLastSleepTime = @"";
    
    
    sleepIndex++;
    [CommonValues setActivityIndex:sleepIndex];
    [self refreshSleepView];
    __LOG_METHOD_END;
    return;

}

//left,right 点击时 刷新view
-(void)refreshSleepView{
    __LOG_METHOD_START;
    NSLog(@"refreshSleepView");
    if (sleepIndex+1 > dbTimeArray.count) {
        __LOG_METHOD_END;
        return;
    }
    NSString *dateStr = [dbTimeArray objectAtIndex:sleepIndex];
    [self updateLabelValue:dateStr];
    
    [sleepTimeArray removeAllObjects];
    [sleepPhaseArray removeAllObjects];
    /*
     if ([CommonValues startSleepAnalysis:sleepIndex :db] == -1) {
     [sleepGraphDataSouceArr removeAllObjects];
     [self drawGraph];
     }
     */
    NSMutableArray * const oneDaySleepArr = /* [[NSMutableArray alloc] init];
    oneDaySleepArr = */ [CommonValues calculateSleepStartEndTimeArr:dateStr :db];
    if (oneDaySleepArr.count > 0) {
        //默认显示最后一次睡眠
        NSArray *oneceSleep = [oneDaySleepArr objectAtIndex:(oneDaySleepArr.count - 1)];
        NSString *startTime = [oneceSleep objectAtIndex:0];
        NSString *endTime = [oneceSleep objectAtIndex:1];
        gStartTime = startTime;
        gEndTime = endTime;
        NSLog(@"startTime = %@, endTime = %@",startTime,endTime);
        [CommonValues sleepAnalysisWithStartTime:startTime withEndTime:endTime withFlag:0 withDataBase:db];
    }
    else{
        //没有band按下sleep的记录，则用默认的时间段 10:00pm-7:00pm
        NSLog(@"默认的时间段 10:00pm-7:00pm");
        NSString *endTime = [NSString stringWithFormat:@"%@ 07:00:00",dateStr];
        NSDate *today = [[CommonValues timeDateFormatter:DateFormatterTimeColon] dateFromString:endTime];
        NSDate *yesterday = [[NSDate alloc] initWithTimeInterval:-9*60*60 sinceDate:today];
        NSLog(@"XXXXXX");
        NSLog(@"today ====== %@",today);
         NSLog(@"yesterday ====== %@",yesterday);
        NSString *startTime = [[CommonValues timeDateFormatter:DateFormatterTimeColon] stringFromDate:yesterday ];
        
        gStartTime = startTime;
        gEndTime = endTime;
        NSLog(@"startTime = %@, endTime = %@",startTime,endTime);
        [CommonValues sleepAnalysisWithStartTime:startTime withEndTime:endTime withFlag:1 withDataBase:db];
    }
    
    
    [self refreshSleepTimeAxis];
    NSLog(@"setDelegate to Dashboard,prepare to autoSync");
    [[TGBleManager sharedTGBleManager]setDelegate:[CommonValues getDashboardID]];
    __LOG_METHOD_END;
    return;
}
- (IBAction)back_click:(id)sender {
    __LOG_METHOD_START;
    [self dismissViewControllerAnimated:YES completion:nil];
    __LOG_METHOD_END;
    return;
}

- (IBAction)modifyClick:(id)sender {
    __LOG_METHOD_START;
    NSLog(@"iphone5 modifyClick---");
    if (sleepIndex+1 > dbTimeArray.count) {
        __LOG_METHOD_END;
        return;
    }
    SleepModifyViewController *smvc = [[SleepModifyViewController alloc]init];
    [self presentViewController:smvc animated:YES completion:nil];
    __LOG_METHOD_END;
    return;
}

-(void)getDataFromDB{
    
    __LOG_METHOD_START;
    return;

}

-(void)drawGraph{
    __LOG_METHOD_START;
    barChart = [[CPTXYGraph alloc] initWithFrame:CGRectZero];
    CPTTheme *theme = [CPTTheme themeNamed:kCPTPlainWhiteTheme];
    [barChart applyTheme:theme];
    // CPTGraphHostingView *hostingView = (CPTGraphHostingView *)hv;
    CPTGraphHostingView *hostingView;
    
    if (screenHeight == 568) {
        hostingView = (CPTGraphHostingView *)hv;
    }
    else{
        hostingView = (CPTGraphHostingView *)hv_4S;
    }
    
    hostingView.hostedGraph = barChart;
    
    // Border
    barChart.plotAreaFrame.borderLineStyle = nil;
    barChart.plotAreaFrame.cornerRadius = 0.0f;
    
    // Paddings
    barChart.paddingLeft = 0.0f;
    barChart.paddingRight = 0.0f;
    barChart.paddingTop = 0.0f;
    barChart.paddingBottom = 0.0f;
    
    barChart.plotAreaFrame.paddingLeft = 0.0;
    barChart.plotAreaFrame.paddingTop = 20.0;
    barChart.plotAreaFrame.paddingRight = 0.0;
    barChart.plotAreaFrame.paddingBottom = 0.0;
    
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)barChart.defaultPlotSpace;
    plotSpace.allowsUserInteraction = NO;
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(0)length:CPTDecimalFromFloat(5)];
    plotSpace.globalYRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(0)length:CPTDecimalFromFloat(5)];
    plotSpace.xRange = [ CPTPlotRange plotRangeWithLocation: CPTDecimalFromInt( -1 )
                                                     length: CPTDecimalFromInt( (int)sleepGraphDataSouceArr.count )
                       ];
    
    
    //    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.0f) length:CPTDecimalFromFloat(150.0f)];
    //    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(-1.0f) length:CPTDecimalFromFloat(25.0f)];
    
    //	plotSpace.allowsUserInteraction = YES;
    CPTXYAxisSet * const axisSet = (CPTXYAxisSet *)barChart.axisSet;
    CPTXYAxis * const x = axisSet.xAxis;
    x.minorTickLineStyle = nil;
    
    x.labelingPolicy = CPTAxisLabelingPolicyNone;
    // x.axisConstraints = [CPTConstraints constraintWithRelativeOffset:0.2];
    
    CPTXYAxis * const y = axisSet.yAxis;
    
    y.labelingPolicy = CPTAxisLabelingPolicyNone;
    y.axisLineStyle=CPTAxisLabelingPolicyNone;
    //add y label test start
    //y.labelingPolicy = CPTAxisLabelingPolicyNone;
    NSArray * const customTickLocations2 = [NSArray arrayWithObjects:[NSDecimalNumber numberWithFloat:1],[NSDecimalNumber numberWithFloat:2], [NSDecimalNumber numberWithFloat:3],    nil];
    NSArray * const xAxisLabels2         = [NSArray arrayWithObjects:@"AWAKE",@"LIGHT   ", @"DEEP    ",nil];
    NSUInteger labelLocation1     = 0;
    NSMutableArray * const customLabels2 = [NSMutableArray arrayWithCapacity:[xAxisLabels2 count]];
    
    
    
    NSLog(@"customTickLocations2.count = %lu",(unsigned long)customTickLocations2.count);
    // Axes
    CPTMutableTextStyle *mm = [[CPTMutableTextStyle alloc]init];
    // font =  [UIFont fontWithName: DEFAULT_FONT_NAME size:30.0];
    mm.fontName= DEFAULT_FONT_NAME;
    mm.color = [CPTColor grayColor];
    mm.fontSize = 12;
    for ( NSNumber *tickLocation2 in customTickLocations2 ) {
        CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithText:[xAxisLabels2 objectAtIndex:labelLocation1++] textStyle:mm];
        newLabel.tickLocation = [tickLocation2 decimalValue];
        [customLabels2 addObject:newLabel];
        
    }
    
    y.axisLabels = [NSSet setWithArray:customLabels2];
    
    
    
    //test end
    
    //  y.axisConstraints = [CPTConstraints constraintWithRelativeOffset:11/12.0];
    y.axisConstraints = [CPTConstraints constraintWithRelativeOffset:0.18];
    y.delegate = self;
    
    CPTBarPlot *barPlot = [CPTBarPlot tubularBarPlotWithColor:[CPTColor colorWithComponentRed:2.0/255.0 green:190.0/255.0 blue:253.0/255.0 alpha:0.0] horizontalBars:NO];
    
    barPlot.dataSource = self;
    //  barPlot.baseValue = CPTDecimalFromString(@"0");
    //   barPlot.barOffset = CPTDecimalFromFloat(0.25f);
    //  barPlot.barCornerRadius = 2.0f;
    barPlot.identifier = @"Bar Plot 2";
    barPlot.fill = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:2.0/255.0 green:190.0/255.0 blue:253.0/255.0 alpha:1.0]];
    //    barPlot.barBasesVary = YES;
    barPlot.lineStyle = nil;
    barPlot.barWidth = CPTDecimalFromDouble(1.1);
    //    barPlot.barWidthScale = 1.0;
    [barChart addPlot:barPlot toPlotSpace:plotSpace];
    
    
    // barPlot.  barWidthsAreInViewCoordinates = NO;

    __LOG_METHOD_END;
    return;

}


-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot {
    __LOG_METHOD_START;
    NSLog(@"numberOfRecordsForPlot sleepGraphDataSouceArr.count = %lu",(unsigned long)sleepGraphDataSouceArr.count);
    __LOG_METHOD_END;
    return sleepGraphDataSouceArr.count;
    
}
-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{
    __LOG_METHOD_START;
    NSDecimalNumber *num = nil;
    if ( [plot isKindOfClass:[CPTBarPlot class]] ) {
		switch ( fieldEnum ) {
			case CPTBarPlotFieldBarLocation:
                
				num = (NSDecimalNumber *)[NSDecimalNumber numberWithUnsignedInteger:index];
                //     NSLog(@"num = %@",num);
				break;
			case CPTBarPlotFieldBarTip:
            {
                int sleep_phase = [[sleepGraphDataSouceArr objectAtIndex:index ] intValue];
                
                num = (NSDecimalNumber *)[NSDecimalNumber numberWithUnsignedInt:sleep_phase];
                //  NSLog(@"SleepArray object %@",[sleepGraphDataSouceArr objectAtIndex:index ]);
                //                if (index> 500) {
                //                    CPTBarPlot *ss = (CPTBarPlot*)plot;
                //
                //                    ss.fill = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:2.0/255.0 green:190.0/255.0 blue:253.0/255.0 alpha:0.0]];
                //                }
            }
 				break;
		}
    }
    
    __LOG_METHOD_END;
    return num;
}


- (NSArray *) barFillsForBarPlot:		(CPTBarPlot *) 	barPlot
                recordIndexRange:		(NSRange) 	indexRange{
    __LOG_METHOD_START;
    //sleep_phase  4 kinds state
    NSMutableArray * const fill_arr = /*[[NSMutableArray alloc]initWithCapacity:sleepGraphDataSouceArr.count];
    fill_arr = */ [sleep_ barFillsFromSleepDataSourceArr:sleepGraphDataSouceArr];
    //    NSLog(@"sleepGraphDataSouceArr = %@",sleepGraphDataSouceArr);
    __LOG_METHOD_END;
    return fill_arr;
    
}
- (void)sleepDownsampleAnalysisResults:(int)minutes
                         withStartTime:(NSDate *)startTime
                           withEndTime:(NSDate *)endTime
                            withLength:(int)len
                         withSleepTime:(uint32_t*)sleeptime
                        withSleepphase:(int8_t*)sleepphase{

    __LOG_METHOD_START;
    NSLog(@"Sleep Downsample results: %d, %@, %@",minutes,startTime,endTime);
    NSMutableArray *tempSleepPhaseArr = [[NSMutableArray alloc]init];
    NSMutableArray *tempSleepTimeArr = [[NSMutableArray alloc]init];

    
//     [[TGBleManager sharedTGBleManager] sleepInitAnalysis:startTime endTime:endTime];
    NSDateFormatter *mdf = [[NSDateFormatter alloc]init];
    [mdf setDateFormat:@"yyyy-MM-dd HH:mm:ss"];

    for (int i = 0; i < len; i++) {
//        NSLog(@"Sleep Downsample data - ts: %d, phase: %d",sleeptime[i],sleepphase[i]);
        NSDate *tmpDate = [NSDate dateWithTimeIntervalSince1970:sleeptime[i]];
        int tmpPhase = sleepphase[i];
        NSString *tmpDateStr =  [mdf stringFromDate:tmpDate];
        [tempSleepTimeArr addObject:tmpDateStr];
        [tempSleepPhaseArr addObject:[NSString stringWithFormat:@"%d",tmpPhase]];
//        [[TGBleManager sharedTGBleManager] sleepAddData:tmpDate sleepPhase:tmpPhase];
    }
//    [[TGBleManager sharedTGBleManager] sleepRequestAnalysis];
    
//    NSLog(@"tempSleepTimeArr = %@",tempSleepTimeArr);
//    NSLog(@"tempSleepPhaseArr = %@",tempSleepPhaseArr);
    
//    [self getDaySleepGraphDataSource3:tempSleepTimeArr withSleepPhaseArr:tempSleepPhaseArr withStartTime:startTime withEndTime:endTime];
//    [self drawGraph];

    __LOG_METHOD_END;
    return;

}

-(void)sleepSmoothData:(int)samepleRate sleepTime:(NSMutableArray *)sleepTimeArray sleepPhase:(NSMutableArray *)sleepPhaseArray_{
    __LOG_METHOD_START;
    NSLog(@"sleepSmoothData samepleRate == %d",samepleRate);
//      NSLog(@"sleepPhaseArray count = %d",sleepPhaseArray.count);
//    NSLog(@"%@, %@", sleepTimeArray, sleepPhaseArray);
    sleepGraphDataSouceArr = sleepPhaseArray_;
    [self drawGraph];
    __LOG_METHOD_END;
    return;

}

- (void) sleepResults:(TGsleepResult)result
            startTime:(NSDate *)startTime
              endTime:(NSDate *)endTime
             duration:(int)duration
             preSleep:(int)preSleep
             notSleep:(int)notSleep
            deepSleep:(int)deepSleep
           lightSleep:(int)lightSleep
          wakeUpCount:(int)wakeUpCount
           totalSleep:(int)totalSleep
              preWake:(int)preWake
           efficiency:(int)efficiency {
    __LOG_METHOD_START;
    NSString * message;
    NSLog(@"-241 sleep call back ------------");
    
    //没有有效的sleep Phase 数组时 画空的图
    if (result != TGsleepResultValid) {
        sleepGraphDataSouceArr = NULL;
        [self drawGraph];

    }
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter setTimeZone:gmt];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *current = [dateFormatter dateFromString:[dbTimeArray objectAtIndex:sleepIndex]];
    NSLog(@"current = %@",current);
    [dateFormatter setDateFormat:@"MMM dd"];
    NSString *monthDay = [dateFormatter stringFromDate:current];
    NSLog(@"monthDay = %@",monthDay);
    NSDate *yesdate = [current dateByAddingTimeInterval:-24*60*60];
    NSLog(@"current = %@,yesdate = %@",current,yesdate);
    NSString *yesDat = [dateFormatter stringFromDate:yesdate];
    NSString *date = [NSString stringWithFormat:@"%@~%@",yesDat,monthDay];
    NSLog(@"date label text = %@",date);
    
    DateLabel.text = date;
    switch (result) {
            
        case TGsleepResultValid:
        {
            NSLog(@"sleepResults: TGsleepResultValid");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\nTotal Seconds: %d\nSeconds before Sleep: %d\nSeconds AWAKE: %d\nSeconds Deep Sleep: %d\nSeconds Light Sleep: %d\nWake Up Count: %d\nTotal Sleep Seconds: %d\nSeconds Awake before End: %d\nEfficiency: %d %%",
                       startTime,
                       endTime,
                       duration,
                       preSleep,
                       notSleep,
                       deepSleep,
                       lightSleep,
                       wakeUpCount,
                       totalSleep,
                       preWake,
                       efficiency
                       ];
            
            wake_count_label.text = [NSString stringWithFormat:@"%d",wakeUpCount];
            awakeLabel.text = [NSString stringWithFormat:@"%dh%dm",notSleep/60,notSleep%60];
            beforeLabel.text = [NSString stringWithFormat:@"%dh%dm",preSleep/60,preSleep%60];
            light_sleep_label.text = [NSString stringWithFormat:@"%dh%dm",lightSleep/60,lightSleep%60];
            deepLabel.text = [NSString stringWithFormat:@"%dh%dm",deepSleep/60,deepSleep%60];
            efficiencyLabel.text = [NSString stringWithFormat:@"%d%%",efficiency];
        }
            break;
            
        case TGsleepResultNegativeTime:
            NSLog(@"sleepResults: TGsleepResultNegativeTime");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\n\nStart Time is After the End Time",
                       startTime,
                       endTime
                       ];
            break;
            
        case TGsleepResultMoreThan48hours:
            NSLog(@"sleepResults: TGsleepResultMoreThan48hours");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\n\nToo Much Time, must be less than 48 hours",
                       startTime,
                       endTime
                       ];
            break;
        case TGsleepResultNoData:
            NSLog(@"sleepResults: TGsleepResultNoData");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\n\nNo Sleep Data Supplied",
                       startTime,
                       endTime
                       ];
            break;
            
        case TGsleepResultNotValid:
            NSLog(@"sleepResults: TGsleepResultNotValid");
        default:
            NSLog(@"sleepResults: default");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\n\nUnexpected Sleep Results\nResult Code: %lu",
                       startTime,
                       endTime,
                       (unsigned long)result
                       ];
            
            break;
    }
    
    NSLog(@"Sleep Effeciency Analysis Demo message: %@",message );
    /*
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sleep Effeciency Analysis Demo\nSee Code for details" message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:@" OK ", nil];
    
    dispatch_async(dispatch_get_main_queue(), ^{ [alert show]; }); // do all alerts on the main thread
     */
    __LOG_METHOD_END;
    return;
}

- (void)sleepAnalysisResults:(NSDate *)startTime withEndTime:(NSDate *)endTime withDuration:(int)duration withAwakePresleep:(int)awakePresleep withNotsleep:(int)notsleep withDeepsleep:(int)deepsleep withLightsleep:(int)lightsleep withWakecount:(int)wakecount withTotalsleep:(int)totalsleep withAwakePostsleep:(int)awakePostsleep withEfficiency:(int)efficiency withLength:(int)len withSleepTime:(uint32_t *)sleeptime withSleepPhase:(int8_t *)sleepphase{
    __LOG_METHOD_START;
    NSLog(@"ekg_Sleep Analysis results-  sleepStartTS:%@, sleepEndTS:%@, len:%d duration:%d, awakePresleep:%d, notsleep:%d, deepsleep:%d, lightsleep:%d, wakecount:%d, totalsleep:%d, awakePostsleep:%d, efficiency:%d",startTime,endTime,len,duration,awakePresleep,notsleep,deepsleep,lightsleep,wakecount,totalsleep,awakePostsleep,efficiency);
    
    
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter setTimeZone:gmt];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *current = [dateFormatter dateFromString:[dbTimeArray objectAtIndex:sleepIndex]];
    NSLog(@"current = %@",current);
    [dateFormatter setDateFormat:@"MMM dd"];
    NSString *monthDay = [dateFormatter stringFromDate:current];
    NSLog(@"monthDay = %@",monthDay);
    NSDate *yesdate = [current dateByAddingTimeInterval:-24*60*60];
    NSLog(@"current = %@,yesdate = %@",current,yesdate);
    NSString *yesDat = [dateFormatter stringFromDate:yesdate];
    NSString *date = [NSString stringWithFormat:@"%@~%@",yesDat,monthDay];
    NSLog(@"date label text = %@",date);
    
    DateLabel.text = date;
    wake_count_label.text = [NSString stringWithFormat:@"%d",wakecount];
    awakeLabel.text = [NSString stringWithFormat:@"%dh%dm",notsleep/60,notsleep%60];
    beforeLabel.text = [NSString stringWithFormat:@"%dh%dm",awakePresleep/60,awakePresleep%60];
    light_sleep_label.text = [NSString stringWithFormat:@"%dh%dm",lightsleep/60,lightsleep%60];
    deepLabel.text = [NSString stringWithFormat:@"%dh%dm",deepsleep/60,deepsleep%60];
    efficiencyLabel.text = [NSString stringWithFormat:@"%d%%",efficiency];
    
    NSMutableArray *tempSleepPhaseArr = [[NSMutableArray alloc]init];
    NSMutableArray *tempSleepTimeArr = [[NSMutableArray alloc]init];
    
    
    //     [[TGBleManager sharedTGBleManager] sleepInitAnalysis:startTime endTime:endTime];
    NSDateFormatter *mdf = [[NSDateFormatter alloc]init];
    [mdf setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    for (int i = 0; i < len; i++) {
        //        NSLog(@"Sleep Downsample data - ts: %d, phase: %d",sleeptime[i],sleepphase[i]);
        NSDate *tmpDate = [NSDate dateWithTimeIntervalSince1970:sleeptime[i]];
        int tmpPhase = sleepphase[i];
//        NSLog(@"tmpPhase = %d",tmpPhase);
        
        NSString *tmpDateStr =  [mdf stringFromDate:tmpDate];
        [tempSleepTimeArr addObject:tmpDateStr];
        [tempSleepPhaseArr addObject:[NSString stringWithFormat:@"%d",tmpPhase]];
        //        [[TGBleManager sharedTGBleManager] sleepAddData:tmpDate sleepPhase:tmpPhase];
    }
    //    [[TGBleManager sharedTGBleManager] sleepRequestAnalysis];
    
//    NSLog(@"tempSleepTimeArr = %@",tempSleepTimeArr);
//        NSLog(@"tempSleepPhaseArr = %@",tempSleepPhaseArr);
    
    [sleepGraphDataSouceArr removeAllObjects];
    sleepGraphDataSouceArr = [sleep_ getDaySleepGraphDataSource3:tempSleepTimeArr withSleepPhaseArr:tempSleepPhaseArr withStartTime:startTime withEndTime:endTime];
//    [self getDaySleepGraphDataSource3:tempSleepTimeArr withSleepPhaseArr:tempSleepPhaseArr withStartTime:startTime withEndTime:endTime];
    [self drawGraph];
    __LOG_METHOD_END;
    return;

}

- (void)sleepResults:(TGsleepResult) result
           startTime:(NSDate*) startTime
             endTime:(NSDate*) endTime
            duration:(int) duration  // in seconds
            preSleep:(int) preSleep // in seconds
            notSleep:(int) notSleep // in seconds active time
           deepSleep:(int) deepSleep // in seconds
          lightSleep:(int) lightSleep // in seconds
         wakeUpCount:(int) wakeUpCount // number of wake ups after 1st sleep
          totalSleep:(int) totalSleep
          efficiency:(int) efficiency{
    
    __LOG_METHOD_START;

    NSLog(@"startTime %@ endTime %@ duration %d preSleep %d notSleep %d deepSleep %d lightSleep %d wakeUpCount %d totalSleep %d efficiency %d",startTime,endTime,duration,preSleep,notSleep,deepSleep,lightSleep,wakeUpCount,totalSleep,efficiency);
    
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
    
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter setTimeZone:gmt];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *current = [dateFormatter dateFromString:[dbTimeArray objectAtIndex:sleepIndex]];
    //
    //NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
    NSLog(@"current = %@",current);
    
    [dateFormatter setDateFormat:@"MMM dd"];
    NSString *monthDay = [dateFormatter stringFromDate:current];
    NSLog(@"monthDay = %@",monthDay);
    NSDate *yesdate = [current dateByAddingTimeInterval:-24*60*60];
    
    NSLog(@"current = %@,yesdate = %@",current,yesdate);
    
    NSString *yesDat = [dateFormatter stringFromDate:yesdate];
    
    NSString *date = [NSString stringWithFormat:@"%@~%@",yesDat,monthDay];
    NSLog(@"date label text = %@",date);
    
    DateLabel.text = date;
    wake_count_label.text = [NSString stringWithFormat:@"%d",wakeUpCount];
    awakeLabel.text = [NSString stringWithFormat:@"%dh%dm",notSleep/60/60,notSleep/60%60];
    beforeLabel.text = [NSString stringWithFormat:@"%dh%dm",preSleep/60/60,preSleep/60%60];
    light_sleep_label.text = [NSString stringWithFormat:@"%dh%dm",lightSleep/60/60,lightSleep/60%60];
    deepLabel.text = [NSString stringWithFormat:@"%dh%dm",deepSleep/60/60,deepSleep/60%60];
    efficiencyLabel.text = [NSString stringWithFormat:@"%d%%",efficiency];
    
    __LOG_METHOD_END;
    return;

}


-(void)updateLabelValue :(NSString *) dateStr{
    __LOG_METHOD_START;
    wake_count_label.text = @"0";
    awakeLabel.text = @"0";
    deepLabel.text = @"0";
    beforeLabel.text = @"0";
    efficiencyLabel.text = @"0";
    light_sleep_label.text = @"0";
    
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
    
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter setTimeZone:gmt];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *current = [dateFormatter dateFromString:dateStr];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:current forKey:CURRENT_CARE_DATE];
    [userDefault synchronize];
    
    NSDate *yesdate = [current dateByAddingTimeInterval:-24*60*60];
    
    NSLog(@"current = %@,yesdate = %@",current,yesdate);
    
    [dateFormatter setDateFormat:@"MMM dd"];
    NSString *monthDay = [dateFormatter stringFromDate:current];
    
    NSString *yesDat = [dateFormatter stringFromDate:yesdate];
    
    NSString *date = [NSString stringWithFormat:@"%@~%@",yesDat,monthDay];
    NSLog(@"date label text = %@",date);
    DateLabel.text = date;
    __LOG_METHOD_END;
    return;

}



@end

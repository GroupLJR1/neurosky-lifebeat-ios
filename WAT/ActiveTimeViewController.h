//
//  ActiveTimeViewController.h
//  WAT
//
//  Created by neurosky on 12/4/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"
#import <sqlite3.h>
#import "TGBleManager.h"
#import "TGLibEKG.h"
#import "CommonValues.h"
#import "TGLibEKGdelegate.h"
#import "TGBleManagerDelegate.h"
#import "UserProfile.h"
#import "CommonColors.h"

@interface ActiveTimeViewController : UIViewController{
    __weak IBOutlet UILabel *dateLabel;
    
    UILabel *firstLabel;
    UILabel *secondLabel;
    UILabel *thirdLabel;
    UILabel *leftMidLabel;
    UILabel *rightMidLabel;
    __weak IBOutlet UILabel *titleLabel;
    
    __weak IBOutlet UIView *myWholeView;
    UILabel *calorieLabel;
    UILabel *distanceLabel;
    UILabel *stepLabel;
  
    UILabel *sleepLabel;
    int  sleepIndex;
    CPTXYGraph *barChart;
    NSArray *SleepArray;
    NSMutableArray  *dbTimeArray;
    sqlite3  *db;
    NSMutableArray *sleepPhaseArray;
    NSMutableArray *sleepTimeArray;
    UIFont *font;
    NSDateFormatter *df;
    NSDateFormatter *local_solid_df;
    
    NSString *database_path;
    //   TGLibEKG * tglibEKG;
    
    NSMutableArray *sleepGraphDataSouceArr;
    int  lastIndex;
    float  screenHeight;
    CPTGraphHostingView  *hv_4S;
    UIScrollView *my_scrollview4S;
    
    UIButton *leftBtn;
    UIButton *rightBtn;
    
    IBOutlet UIView *headView;
    int ystLastSleepPhase;
    NSString *ystLastSleepTime;
    UIImageView *stepImg;
    UIImageView *calorieImg;
    UIImageView *distanceImg;
    UIImageView *sleepImg;
    int oneDayTotalStep;
    int oneDayTotalCalories;
    int oneDayTotalDistance;
    int Step_count;
    int Calories_count;
    int Distance_count;

}


- (IBAction)right_click:(id)sender;
- (IBAction)left_click:(id)sender;
- (IBAction)back_click:(id)sender;



@end

//
//  FileOperation.h
//  WAT
//
//  Created by NeuroSky on 1/27/15.
//  Copyright (c) 2015 Julia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileOperation : NSObject
+ (void)deleteFile :(NSString *)fileName;
+(NSArray *)getFileArr:(NSString *)folderName;
+(void)deleteFolder :(NSString *)folderName;
+(void)saveEcg2DocumentWithFilePath:(NSString *)filePath withFileContent:(NSString *)fileContent;
+ (void)save2UserDefaultsWithObj:(id)value withKey:(NSString *)key;
+ (void)save2UserDefaultsWithInteger:(NSInteger)value withKey:(NSString *)key;
+ (void)write2FileAppendLastTimeContent:(NSString *)content filePath:(NSString *)path;
@end

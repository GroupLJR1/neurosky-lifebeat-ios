//
//  Constant.h
//  WAT
//
//  Created by NeuroSky on 1/20/15.
//  Copyright (c) 2015 Julia. All rights reserved.
//

#ifndef WAT_Constant_h
#define WAT_Constant_h


#define DEFAULT_FONT_NAME @"SteelfishRg-Regular"



//  redefine the "CHECK_GL_ERROR( )" macro to use the debug window for output instead of "stdout"...
#undef  CHECK_GL_ERROR
#define CHECK_GL_ERROR( ) ( { GLenum __error = glGetError( ); \
                            if ( __error ) NSLog( @"OpenGL error 0x%04X in %s %d\n", __error, __FUNCTION__, __LINE__ ); \
                            } \
                          )

//////////////////////////////////////////////////////////////////////////////////////
//
//  macros to log different points in a procedure/function/method, which will be
//  automatically removed by the compiler for release builds (on "start" and "end"
//  traces, call "sleep( 0 )" to allow other processes to perform any necessary
//  updates, and these calls will not be optimized out for release builds)...
//

#define __LOG_METHOD_START                      sleep( 0 ); \
                                                NSString * ___objectDetails = [ self description ]; \
                                                if ( ___objectDetails ) { }  /* prevent "unused variable" warnings */ \
                                                if ( _cmd )             { }  /* prevent "unused variable" warnings */ \
                                                NSLog( @"%@ - method \"%@\" start (%s)" \
                                                     , self \
                                                     , NSStringFromSelector( _cmd ) \
                                                     , __FILE__ \
                                                     )

#define __LOG_METHOD_NOTE_STR_INT( s, v )       NSLog( @"%@ - method \"%@\" %s=%d" \
                                                     , self \
                                                     , NSStringFromSelector( _cmd ) \
                                                     , s \
                                                     , v \
                                                     )
#define __LOG_METHOD_NOTE_STR( s )              NSLog( @"%@ - method \"%@\" %s" \
                                                     , self \
                                                     , NSStringFromSelector( _cmd ) \
                                                     , s \
                                                     )

#define __LOG_METHOD_END                        sleep( 0 ); \
                                                /*CHECK_GL_ERROR( );*/ \
                                                NSLog( @"%@ - method \"%@\" end (%s::%d)" \
                                                     , self \
                                                     , NSStringFromSelector( _cmd ) \
                                                     , __FILE__ \
                                                     , __LINE__ \
                                                     ); \
                                                ___objectDetails = nil

//  log messages for a method after it's already been destroyed ("self" no longer exists)...
#define __LOG_METHOD_END_DESTROYED              sleep( 0 ); \
                                                /*CHECK_GL_ERROR( );*/ \
                                                NSLog( @"%@ - method \"%@\" end (%s::%d)" \
                                                     , ___objectDetails \
                                                     , NSStringFromSelector( _cmd ) \
                                                     , __FILE__ \
                                                     , __LINE__ \
                                                     ); \
                                                ___objectDetails = nil




#undef USE_THINKGEAR_LIBRARY    //  MIW 2015-02-16
//#define USE_THINKGEAR_LIBRARY


#define LostConnectTitle @"Lost Connection"
#define LostConnectMsg @"Connection lost from band."

#define TokenReleasedMsg @"The token has been released."
#define TokenReleasedTitle @"Token Released"
#define CommunicationErrorTitle @"Communication Error"
#define BondedNoMatchMsg @"Band did not receive any token."

#define BadTokenFormatMsg @"App token rejected by band."

#define TimeOutTitle @"Bonding Error #4"
#define TimeOutMsg @"Timed out. Please try again."
#define NoConnectionTitle @"Bonding Error #5"
#define NoConnectionMsg @"No connection. Please try again.";
#define ReadTimeOutTitle @"Bonding Error #6"
#define ReadTimeOutMsg @"Read timeout. Please try again.";
#define ReadBackTimeOutTitle @"Bonding Error #7"
#define ReadBackTimeOutMsg @"ReadBack timeout. Please try again."
#define WriteFailTitle @"Bonding Error #8"
#define WriteFailMsg @"Write failed.  Please try again."

#define TargetIsAlreadyBondedMsg @"The target is already bonded.It must be cleared to connect to it."
#define BondRejectedTitle @"Bond Rejected"
#define BondAppErrorTitle @"Bond App Error"
#define NoPotentialBondDelegateMsg @"No potential bond delegate."
#define UnSupportedHardwareMsg @"The target is not a supported hardware."
#define TargetHasWrongSNMSg @"The target has the wrong S/N."
#define PairingErrorTitle @"Pairing Error"

#define PairingRejectedMsg @"User rejected iOS pairing dialog."

#define SecurityMismatchMsg @"Please open the Bluetooth settings on your phone, and delete/forget the target band."
#define UnkonwnBondErrorTitle @"Unkonw Bond Error"

#define DigitCodeConfirmMsg @"Look on your band display.  Do you see the numbers "

#define UnbondedTimeOutMesg @"Make sure your band has power, and is unbonded.  If unsure, MANUALLY clear your band (plug in, hold button 30s)."
#define ClearBondConfirmation @"After you do this, you will need to MANUALLY clear your band (plug in, hold button 30s), and then set up your connection from the beginning.  Are you sure?"

#define BluetoothIsOffTitle @"Bluetooth is powered off"
#define BluetoothIsOffMsg @"Turn it on in the settings"

#define ResetBluetoothTitle @"Sync failed many times"
#define ResetBluetoothMsg @"Turn off bluetooth and turn back on,or reboot your phone."

#define ConnectTimeOutTitle @"Connect time out"
#define ConnectTimeOutMsg @"Timed out while trying to connect to band."
#define ScanTimeOutTitle @"Scan time out"
#define ScanTimeOutMsg @"Timed out while trying to scan for band."
#define ConnectionLostTitle @"Connection lost"
#define ConnectionLostMsg @"Connection lost from band"
#define BondReleasedTitle @"App Bond released"
#define BondReleasedMsg @"The app can now connect to any \"UNBONDED BAND\"."

#define BinFileMissingTitle @"FW bin file missing"
#define BinFileMissingMsg @"FW update file missing from app!"

#define BatteryLowTitle @"Band battery low!"
#define BatteryLowMsg @"Battery level on band too low for FW update!  Recharge it first."

#define FWUpdateErrorTitle @"FW Update Error";
#define FWUpdateErrorMsg @"FW update error!";

#define FWDownDisconnectMsg @"FW update disconnect.";

#define FWDownDisconnectWriteFailedMsg @"FW update disconnect, write failed.";
#define FWDownWriteFailedMsg @"FW update write failed.";
#define FWDownWriteTimeOutMsg @"FW update write timed out.";
#define FWDownErrorImproperImageFileMsg @"FW update failed, bad FW update file.";

#define TerminatedNoDataTitle @"Realtime Error"
#define TerminatedNoDataMsg @"No Realtime ECG data received from band for 10s.  Canceling."

#define TerminatedDataStoppedTitle @"No More Data"
#define TerminatedDataStoppedMsg @"No Realtime ECG data received from band for 3s.  Stopping."

#define TerminatedLostConnectionTitle @"Lost Connection"
#define TerminatedLostConnectionMsg @"Make sure the device is nearby and retry."

#define NotFilledTitle @"Form Error"
#define NotFilledMsg @"Please fill in all the fields."


#define FilledInvalidInfoTitle @"Input Error "
#define FilledInvalidInfoMsg @"Invalid entry for XXX, please check and try again."

#define HR_BPM @"Heart Rate measured as number of Heart Beats per Minute."
#define HRV @"Hear Rate Variability is a measure of beat-to-beat variation over a period of time (minimum 30 seconds), and is measured in Milliseconds."
#define  STRESS @"Value between 0(not stressed)  to 100  (highly stressed)indicating the current stress level."
#define  CARDIOPLOT @"Live ECG display on smartphone. ECG and other related data displayed on the smartphone while the ECG measurement is being done on the LifeBeat wrist-band."

#define PLAYBACK @"Selection and display of previously recorded ECG data on the smartphone. Selection can be done from the list of date/time of recording."
#define  R2R_INTERVAL @"Time interval measured in Milliseconds between two successive heart-beats."

#define MAXIMUM_HEART_RATE @"Maximum value of Heart Rate for a given person based on age and gender. Heart Rate above this value is considered harmful."

#define TRAINNIGZONE @"Indicated by “fire” symbol when measured heart rate is over 65% of the maximum heart rate."
#define HEART_LEVEL @"Current Heart Rate as a percentage of Maximum heart Rate."

#define MOOD @"Current Mood indication between 0 (Calm/Relaxed) to 100 (Tense/Excited)."

#define CARDIOPLOT_BATTERY_WARNING_TITLE @"Are you sure you want to proceed?"

#define CARDIOPLOT_BATTERY_WARNING_MSG @"Notice:  Realtime CardioPlot uses up the battery power on the band quickly."

#define ERROR_TITLE @"Error"
#define NoAssociatedBand_MSG @"It does not have any bands associated with this app."
#define Associated_MSG @"Your associated band should display the numbers "

#define DoingAutoSync @"Auto Syncing... please wait"
#define TOUCH_SENSOR_MSG @"Please touch the sensor."
#define FILTER_DATA_MSG @"Acquiring data ..."

#define CELLULAR_MSG @"Limit the application to send data only with wifi, go to Settings -> Cellular and turn it off."
#define CELLULAR_TITLE @"Warning: Cellular Data Usage"
#define RECEIVE_CURRENTCOUNT_MSG @"Received Current Count"
#define SYNC_DATA_MSG @"Syncing Data from Band"
#define ERASE_DATA_MSG @"Erasing Data from Band"
#define SYNC_FAILED_MSG @"Sync failed"

#define UPDATE_FW_WARNING_TITLE @"Are you sure ?"
#define UPDATE_FW_WARNING_MSG @"Warning. Firmware update will clear the data in the band. Please sync before."
#define CONNECT_NEW_BAND_CONFIRM_MSG @"Are you sure you want to release the current band?"

#define RECONNECT_AFTER_FW_UPDATE_TITLE @"Notice"
#define RECONNECT_AFTER_FW_UPDATE_MSG @"Waiting for the band to reboot to connect."


#define CANNOT_CONFIRM_UPDATE_TITLE  @"No Update  Confirmation"
#define CANNOT_CONFIRM_UPDATE_MSG @"Could not confirm the update is installed or not."


#define DBNAME    @"seagull.sqlite"
#define TIME      @"time"
#define STEP       @"step"
#define CALORIES   @"calories"
#define DISTANCE      @"distance"
#define SPEED       @"speed"
#define ENERGY   @"energy"
#define MODE       @"mode"
#define SLEEP   @"sleep"
#define BPM   @"bpm"
#define RealtimeECGPath @"EKGDataInteralTest"
#define TABLENAME @"PED"
#define USER_NAME @"user_name"
#define USER_BIRTH_DATE @"user_birth_date"
#define USER_AGE @"user_age"
#define USER_IS_FEMALE @"user_isFemale"
#define USER_WEIGHT @"user_weight"
#define USER_HEIGHT @"user_height"
#define USER_IS_BAND_RIGHT @"user_isBandLeft"
#define USER_WALKING_STEP_LENGH @"user_walkingStepLenght"
#define USER_RUNNING_STEP_LENGH @"user_runningStepLenght"
#define USER_WEIGHT_UNIT @"user_weightUnit"
#define USER_GOAL_STEP @"goal_steps"
#define USER_GOAL_DISTANCE @"goal_distance"
#define USER_GOAL_CALORI @"goal_calori"
#define USER_GOAL_TIMER @"goal_timer"
#define USER_GOAL_NO_REPEAT @"goal_no_repeat"
#define USER_GOAL_SLEEP @"goal_sleep"
#define USER_GOAL_ACTIVE_TIME @"goal_active_time"
#define USER_ALARM @"user_alarm"
#define USER_ALARM_MODE @"user_alarm_mode"
#define USER_TIMER_ALARM @"user_timer_alarm"
#define DISPLAY_24HR_Time @"display_24hr"
#define DISPLAY_IMPERIAL_UNITS @"display_imperial"
#define IS_AUTO_WALKING @"is_auto_walking"
#define IS_AUTO_RUNNING @"is_auto_running"
#define ALARM_SWITCH @"alarm_switch"
#define TIMER_SWITCH @"timer_switch"
#define TIMER_DERICTORY_TIME @"timer_derictory_time"
#define ALARM_DERICTORY_TIME @"alarm_derictory_time"

#define AUTOSYNC_SWITCH @"autosync_switch"
#define AUTOSYNC_TIME @"autosync_time"

#define BEEP_SOUND @"beep_sound_on"

#define STEP_LATESTDATE @"CurrentLatestStep"
#define CALORIE_LATESTDATE @"CurrentLatestCalorie"
#define DISTANCE_LATESTDATE @"CurrentLatestDistance"

#define LATEST_BATTERY @"latest_battery"
#define CELLULAR_WARNNING_SWITH @"cellular_warning_switch"
#define SET_SLEEP_IN_APP @"set_sleep_in_app"
#define APP_SLEEP_START_TIME @"app_sleep_start_time"
#define APP_SLEEP_END_TIME @"app_sleep_end_time"
#define BAND_SLEEP_START_TIME @"band_sleep_start_time"
#define BAND_SLEEP_END_TIME @"band_sleep_end_time"
#define CURRENT_CARE_DATE @"currect_care_date"

#define SLEEP_DOWN_SAMPLE 5

//#define MONTH_COUNT 4*7
//#define SIX_MONTH_COUNT 24*7
//#define YEAR_COUNT 48*7

//#define MONTH_INTERVAL 5
//#define SIX_MONTH_INTERVAL 5
//#define YEAR_INTERVAL 5

#define DAYS_OF_WEEK 7

#define IPHONE_4S @"iPhone4,1"


#define UPLOAD_TYPE_SYNC_LOG  @"SyncLog"
#define UPLOAD_TYPE_ECG  @"ECG"
#define UPLOAD_TYPE_PED  @"PED"
#define UPLOAD_TYPE_PRO  @"PRO"
#define UPLOAD_TYPE_SLEEP  @"SLEEP"
#define UPLOAD_TYPE_FAT  @"FAT"

#define DBNAME    @"seagull.sqlite"
#define TIME      @"time"
#define STEP       @"step"
#define CALORIES   @"calories"
#define DISTANCE      @"distance"
#define SPEED       @"speed"
#define ENERGY   @"energy"
#define MODE       @"mode"
#define SLEEP   @"sleep"
#define BPM   @"bpm"


#define TABLENAME @"PED"
#define RealtimeECGPath @"EKGDataInteralTest"
#define AnimationTypeStep @"step_type_animation"
#define AnimationTypeCalorie @"calorie_type_animation"
#define SYNC_LOG_PATH @"SyncEvent"

#define CANCEL @"Cancel"
#define RETRY @"Retry"
#define TRYANOTHERBAND @"Retry another band"
#define OK @"OK"
#define NOTHINGCHANGED @"Nothing Changed,There's No Need To Set."
#define PROFILENOTHINGCHANGED @"Profile Nothing Changed,There's No Need To Set."
#define CONNECT_BAND_FIRST @"Please connect to band first."
#define INTERNET_DISCONNECTED @"Internet disconnected"
#define RESET_BAND_SUCCESS @"Reset Band Successfully"
#define ERASE_FAILED @"Erase failed"

#define AUTO_SLEEP_START_TIME @"auto_sleep_start_time"
#define AUTO_SLEEP_END_TIME @"auto_sleep_end_time"

#define selectDeviceAlertTag  100
#define bondAlertTag  101
#define LostConnectAlertTag  102
#define userNameAlertTag  103
#define syncReportAlertTag  104
#define timeOutAlertTag  105
#define bondFailedTag  106
#define  noTokenBondFailedTag  107
#define  hasTokenBondFailedTag  108

#define hasTokenTimeOutAlertTag  109
#define noTokenTimeOutAlertTag  110
#define releaseBondConfrimAlertTag  111
#define checkBTStatusTag  112
#define cellularTag  200
#define RELEASEBONDTAG  199
#define fwDownAlertTag  198
#define lostConnectAlertTag  197
#define batteryAlertTag 196
#define stopRTAlertTag 195
#define noConnectionAlertTag 194
#define EKGFileRawStartLine 193
#define ERASEDATATAG 192
#define SHIPPINGMODE 191
#define RECONNECTTAG 114

#define retryMaxCount  3

#define IOS8 8.0
#define IOS7 7.0

#endif

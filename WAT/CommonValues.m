//
//  CommonValues.m
//  WAT
//
//  Created by neurosky on 5/29/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import "CommonValues.h"
#import <netdb.h>
#import <AdSupport/AdSupport.h>
#import <SystemConfiguration/SCNetworkReachability.h>
#import "TGBleManager.h"
#import "TGLibEKG.h"
#import "Constant.h"
#import "sys/utsname.h"
#import "SVProgressHUD.h"
#import "FileOperation.h"
#import "sys/utsname.h"

#import "TGBleEmulator.h"

#define SLEEP_CREATE_SQL @"CREATE TABLE SLEEP (time DATE PRIMARY KEY,  sleep_phase INTEGER , init_code INTEGER)"
#define HEART_CREATE_SQL @"CREATE TABLE HEART (time DATE PRIMARY KEY , final_hr INTEGER,avg_hr INTEGER,  avg_hrv INTEGER)"
#define HEART_CREATE_SQL_2 @"CREATE TABLE HEART (time DATE PRIMARY KEY , final_hr INTEGER,avg_hr INTEGER,  avg_hrv INTEGER,final_hrv INTEGER, stress INTEGER)"


@implementation CommonValues

//+(NSTimer *) initAutoSyncTimerWithTime :(int)myTimeInterval : (id)myTarget{
//
//    autoSyncTimer = [NSTimer scheduledTimerWithTimeInterval:myTimeInterval target:myTarget selector:<#(SEL)#> userInfo:<#(id)#> repeats:YES];
//}

+(BOOL) getShouldEraseData{
    return  shouldEraseData;
}

+(NSInteger)secondsFromString:(NSString *)myStr{
    __LOG_METHOD_START;
    NSInteger seconds = -1;
    NSArray *timeArr = [myStr componentsSeparatedByString:@":"];
    if (timeArr.count == 2) {
        NSInteger const hour =  [[timeArr objectAtIndex:0]integerValue];
        NSInteger const min =  [[timeArr objectAtIndex:1]integerValue];
        
        seconds = (hour*60+min)*60;
    }
    __LOG_METHOD_END;
    return seconds;
}

+(void)setMyDB:(sqlite3 *)db{
    __LOG_METHOD_START;
    myDB = db;
    __LOG_METHOD_END;
}
+(sqlite3 *)getMyDB{
    return myDB;
}

+(UIAlertView *)autoSyncingAlert{
    __LOG_METHOD_START;
    if (autoSyncingAlert  == NULL) {
        autoSyncingAlert = [[UIAlertView alloc]initWithTitle:@"" message:DoingAutoSync delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    }
    __LOG_METHOD_END;
    return autoSyncingAlert;
}

+(int)getAutoSyncRepeatTime{
    return  autoSyncRepeatTime;
}

+(BOOL)getAutoSyncingFlag{
    return autoSyncingFlag;
}
+(void)setAutoSyncingFlag:(BOOL) myFlag{
    __LOG_METHOD_START;
    autoSyncingFlag = myFlag;
    __LOG_METHOD_END;
}
+ (id) getDashboardID{
    return dashboadId;
}
+ (void)setDashboardID : (id) myID{
    __LOG_METHOD_START;
    dashboadId = myID;
    __LOG_METHOD_END;
}


+ (NSTimer *) getAutoSyncTimer{
    
    return autoSyncTimer;
}

+ (void) setAutoSyncTimer: (NSTimer *) myTimer{
    __LOG_METHOD_START;
    autoSyncTimer = myTimer;
    __LOG_METHOD_END;
}


+(void)setTestIndex:(int) index{
    __LOG_METHOD_START;
    testIndex = index;
    __LOG_METHOD_END;
}
+(int)getTestIndex{
    return testIndex;
}


+(void)setActivityIndex: (NSInteger)index{
    __LOG_METHOD_START;
    activityIndex = index;
    __LOG_METHOD_END;
}
+(NSInteger) getActivityIndex{
    return activityIndex;
}
+(BOOL)getSaveBtnClcikFlag{
    return saveBtnClcikFlag;
}
+(void)setSaveBtnClcikFlag: (BOOL)flag{
    __LOG_METHOD_START;
    saveBtnClcikFlag = flag;
    __LOG_METHOD_END;
}

+(NSString*)getSleepTimeBeoforeFirst{
    return beoforeFirstSleepTime;
}
+(int)getSleepPhaseBeoforeFirst{
    return beoforeFirstSleepPhase;
    
}

+(BOOL)isBackgroundFlag{
    return isInBackground;
}
+(void)setBackgroundFlag: (BOOL)flag{
    __LOG_METHOD_START;
    isInBackground = flag;
    __LOG_METHOD_END;
}

/**
 唯一标示设备
 */
+(NSString *) getDeviceID{
    __LOG_METHOD_START;
    //    UIDevice *device = [UIDevice currentDevice];//创建设备对象
    //    NSUUID *iphone_id = [device identifierForVendor];
    //    NSString*  deviceId = [iphone_id UUIDString];
    //    NSLog(@"%@",deviceId);
    NSUUID   * const tesetID = [ [ ASIdentifierManager sharedManager ] advertisingIdentifier ];
    NSString * const deviceID = [ tesetID UUIDString ];
    NSLog( @"device Id = %@", deviceID );
    __LOG_METHOD_END;
    return deviceID;
}



/**
 是否有网络
 */
+ (BOOL) connectedToNetwork
{
    __LOG_METHOD_START;
    BOOL isConnected = NO;

    // Create zero addy
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    
    // Recover reachability flags
    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
    SCNetworkReachabilityFlags flags;
    
    BOOL const didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    CFRelease(defaultRouteReachability);
    
    if (didRetrieveFlags)
    {
        BOOL const isReachable = flags & kSCNetworkFlagsReachable;
        BOOL const needsConnection = flags & kSCNetworkFlagsConnectionRequired;
        isConnected = (isReachable && !needsConnection) ? YES : NO;
    }
    __LOG_METHOD_END;
    return isConnected;
}

+(int)getConsecutiveSyncCount{
    return  consecutiveSyncCount;
    
}
+(void)addConsecutiveSyncCount{
    __LOG_METHOD_START;
    consecutiveSyncCount++;
    __LOG_METHOD_END;
}
+(void)resetConsecutiveSyncCount{
    __LOG_METHOD_START;
    consecutiveSyncCount = 0;
    __LOG_METHOD_END;
}

+(BOOL)getBluetoothOnFlag{
    
    return BluetoothOnFlag;
}
+(void)setBluetoothOnFlag: (BOOL)flag{
    __LOG_METHOD_START;
    BluetoothOnFlag = flag;
    __LOG_METHOD_END;
}

+(BOOL)getHasTaskFlag{
    
    return hasTaskFlag;
}
+(void)setHasTaskFlag: (BOOL)flag{
    __LOG_METHOD_START;
    hasTaskFlag = flag;
    __LOG_METHOD_END;
}


+(BOOL)getStopRTNormalFlag{
    return stopRTNormalFlag;
}

+(void)setStopRTNormalFlag: (BOOL)flag{
    __LOG_METHOD_START;
    stopRTNormalFlag = flag;
    __LOG_METHOD_END;
}

+(void)showAlert :(NSString* )msg{
    __LOG_METHOD_START;
    UIAlertView * const alert = [ [ UIAlertView alloc ] initWithTitle: @"Invalid Input"
                                                              message: msg
                                                             delegate: nil
                                                    cancelButtonTitle: nil
                                                    otherButtonTitles: @"OK", nil
                                ];
    [ alert show ];
    __LOG_METHOD_END;
}


+(void)setAboutUpdateProgress:(float)percent{
    __LOG_METHOD_START;
    about_update_progress = percent;
    __LOG_METHOD_END;
}


+(float)getAboutUpdateProgress{
    
    
    return about_update_progress;
}

+(void)setFlag:(int) falg_p{
    __LOG_METHOD_START;
    flag = falg_p;
    __LOG_METHOD_END;
}

+(int)getFlag{
    
    return flag;
}

+(float)getTimeOutTime{
    return  timeout_time;
}


+(void)setScreenHeight: (float) height{
    __LOG_METHOD_START;
    screen_height = height;
    __LOG_METHOD_END;
}

+(float)getScreenHeight{
    return screen_height;
    
}

+(BOOL)getIsECGAppMode{
    return isECGAppMode;
    
}

+(BOOL)isPlayBackMode{
    
    return is_playback_mode;
}

+(void)setPlayBackMode:(BOOL)isplayback{
    __LOG_METHOD_START;
    is_playback_mode = isplayback;
    __LOG_METHOD_END;
}

+(BOOL)isHistoryECGPlayBackMode{
    
    return is_historyECG_playback_mode;
}

+(void)setHistoryECGPlayBackMode:(BOOL)isHistoryECGPlayBackMode{
    __LOG_METHOD_START;
    is_historyECG_playback_mode = isHistoryECGPlayBackMode;
    __LOG_METHOD_END;
}

+(void)setSleepStartTime:(NSString *)startTime{
    __LOG_METHOD_START;
    sleepStartTime = startTime;
    __LOG_METHOD_END;
}

+(void)setSleepEndTime:(NSString *)endTime{
    __LOG_METHOD_START;
    sleepEndTime = endTime;
    __LOG_METHOD_END;
}

+(NSString *)getSleepStartTime{
    return sleepStartTime;
}

+(NSString *)getSleepEndTime{
    return sleepEndTime;
}

+(int)getSleepTotalMinutes{
    __LOG_METHOD_START;
    if (totalEndTime < 0) {
        NSLog(@"SleepTotalMinutes calculation error ");
        totalEndTime = 0;
    }
    __LOG_METHOD_END;
    return totalEndTime;
}



+(void)setSelectedEKG :( NSString *)name{
    __LOG_METHOD_START;
    selectedEKG = name;
    __LOG_METHOD_END;
}
+(NSString *) getSelectedEKG{
    return selectedEKG;
    
}

+(void)setUserName :(NSString*) name{
    __LOG_METHOD_START;
    userName = name;
    __LOG_METHOD_END;
}

+(NSString *) getUserName{
    return userName;
    
}



/**
 插入Heart数据
 */
+(void)insertHeartData2DB :(NSDate *)date
                          finalHR:(int)finalHR
                          avgHR:(int)avgHR
                          avgHRV:(int)avgHRV
                          finalHRV:(int)finalHRV
                          stress:(int)stress
                          database:(sqlite3 *)database
{
    __LOG_METHOD_START;
    NSDateFormatter *df =  [[NSDateFormatter alloc]init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [df setTimeZone:gmt];
    NSString *dateStr =   [df stringFromDate:date];
    NSLog(@"dateStr = %@",dateStr);
    
    NSString *sql = [NSString stringWithFormat:
                     @"INSERT INTO Heart ('time', 'final_hr','avg_hr', 'avg_hrv','final_hrv','stress') VALUES ('%@', '%d','%d', '%d' ,'%d','%d')",
                     dateStr, finalHR, avgHR,avgHRV,finalHRV,stress];
    
    [ self execSql: sql: database ];
    __LOG_METHOD_END;
}


/**
 查询数据
 */
+(void)execSql:(NSString *)sql :(sqlite3 *)database
{
    __LOG_METHOD_START;
    char *err;
    if (sqlite3_exec(database, [sql UTF8String], NULL, NULL, &err) != SQLITE_OK) {
        NSLog(@"sql execution error = %s",err);
    }
    __LOG_METHOD_END;
}


/**
 返回 recorded ECG 的时间 数组  if isHistoryECG = YES
 返回 realtime ECG 的时间 数组  if isHistoryECG = NO
 */
+(NSMutableArray *)getECGTimeArr :(sqlite3 *)database :(BOOL)isHistoryECG {
    __LOG_METHOD_START;
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    NSDateFormatter *dateFor = [[NSDateFormatter alloc]init];
    NSString *sql ;
    if (isHistoryECG) {
        sql =  @"select time from heart where final_hr != 0";
    }
    else{
        //是realtime ECG
        sql =  @"select time from heart where final_hr = 0";
    }
    sqlite3_stmt *stmt;
    NSLog(@"getECGTimeArr sql = %@",sql);
    
    if(sqlite3_prepare_v2(database, [sql UTF8String], -1, &stmt, NULL) == SQLITE_OK) {
        while(sqlite3_step(stmt) == SQLITE_ROW) {
            const  unsigned  char *dateStr = sqlite3_column_text(stmt, 0);
            NSString *timeStr = [NSString stringWithFormat:@"%s",dateStr];
            //             NSLog(@"timeStr = %@",timeStr);
            [dateFor setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate *date = [dateFor dateFromString:timeStr];
            [dateFor setDateFormat:@"yyyy_MM_dd_HH_mm_ss"];
            NSString *timeStr2 = [dateFor stringFromDate:date];
            
            //            NSLog(@"timeStr = %@",timeStr2);
            [arr addObject:timeStr2];
            
        }
        sqlite3_finalize(stmt);
    }
    NSLog(@"getECGTimeArr = %@",arr);
    
    __LOG_METHOD_END;
    return arr;
}


/**
 打开DB
 */
+ (void)openDataBase :(sqlite3 *)database
{
    __LOG_METHOD_START;
    [self getDbPath];
    if (sqlite3_open([database_path UTF8String], &database)==SQLITE_OK)
    {
        NSLog(@"open sqlite db ok. 打开数据库成功");
    }
    else
    {
        NSLog( @"can not open sqlite db  打开数据库失败" );
        
        //close database
        if(sqlite3_close(database) == SQLITE_OK){
            NSLog(@"关闭数据库成功");
        }
        else{
            NSLog(@"关闭数据库失败!!!!????!!!!!!");
        }
        
    }
    __LOG_METHOD_END;
}


/**
 创建或打开DB
 */
+(void)creatOrOpenDB:(sqlite3 *)database{
    __LOG_METHOD_START;
    //创建表
    NSString *sqlCreateTable = @"CREATE TABLE IF NOT EXISTS PED (time DATE PRIMARY KEY, step INTEGER, calories INTEGER, distance INTEGER, speed INTEGER, bmp INTEGER,energy INTEGER, mode INTEGER, sleep INTEGER)";
    [self execSql:sqlCreateTable :database];
    
    NSString *sqlCreateSleepTable = @"CREATE TABLE IF NOT EXISTS SLEEP (time DATE PRIMARY KEY,  sleep_phase INTEGER , init_code INTEGER)";
    [self execSql:sqlCreateSleepTable :database];
    
    
    NSString *sqlCreateHeartTable = @"CREATE TABLE IF NOT EXISTS HEART (time DATE PRIMARY KEY , final_hr INTEGER,avg_hr INTEGER,  avg_hrv INTEGER,final_hrv INTEGER)";
    [self execSql:sqlCreateHeartTable :database];
    
    
    //假如存在HEART但是是老表，不包含final_hrv 字段，则修改表
    NSString *sleepSql =  @"select sql from sqlite_master where tbl_name='HEART' and type='table'";
    sqlite3_stmt *stmtSleepSql;
    NSString  *creatTableSql;
    NSLog(@"sleepSql = %@",sleepSql);
    if(sqlite3_prepare_v2(database, [sleepSql UTF8String], -1, &stmtSleepSql, NULL) == SQLITE_OK) {
        while(sqlite3_step(stmtSleepSql) == SQLITE_ROW) {
            const  unsigned  char *creatTableSqlChar = sqlite3_column_text(stmtSleepSql, 0);
            creatTableSql = [NSString stringWithFormat:@"%s",creatTableSqlChar];
            
        }
        sqlite3_finalize(stmtSleepSql);
    }
    NSLog(@"creatTableSql = %@",creatTableSql);
    
    if (![creatTableSql isEqualToString:HEART_CREATE_SQL_2]) {
        //是老表的建表语句,则需要修改表,添加stress字段
        NSString *alterTableSql = @"ALTER TABLE HEART ADD COLUMN stress INTEGER;";
        [self execSql:alterTableSql :database];
        NSLog(@"ALTER TABLE finished");
    }
    else{
        NSLog(@"No need to ALTER TABLE,the HEART table structure is already newest ");
        
    }
    __LOG_METHOD_END;
}


/**
 return 某一天（date） 的ECG record的次数
 */
+ (int)getDayECGCount:(NSString *)date :(sqlite3 *)database{
    __LOG_METHOD_START;
    [self getDbPath];
    NSLog(@"input  date = %@",date);
    int count = 0;
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *sql = [NSString stringWithFormat:@"select count(*),  datetime(datetime(time,'%ld hour','+0 minute'))  as time1 from heart where time1 like '%@%%' and final_hr != 0 ",(long)[[df timeZone] secondsFromGMT]/3600,date];
    sqlite3_stmt *stmt;
    NSLog(@"getDayECGCount sql = %@",sql);
    
    if(sqlite3_prepare_v2(database, [sql UTF8String], -1, &stmt, NULL) == SQLITE_OK) {
        while(sqlite3_step(stmt) == SQLITE_ROW) {
            count = sqlite3_column_int(stmt, 0);
            
        }
        sqlite3_finalize(stmt);
    }
    NSLog(@"count = %d",count);
    __LOG_METHOD_END;
    return count;
}


/**
 get sleep time Date arr
 */
+ (NSMutableArray *)getSleepTimeArr :(sqlite3 *)database{
    __LOG_METHOD_START;
    [self getDbPath];
    NSMutableArray *dbTimeArray = [[NSMutableArray alloc]init];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    sqlite3_stmt *stmt = nil;
    // local  date time  from sleep
    NSString *dbTimeStr =[NSString stringWithFormat:@"select  distinct date(datetime(time,'%ld hour','+0 minute'))  as time1 from sleep order by time1 desc;",(long)[[df timeZone] secondsFromGMT]/3600];
    
    
    NSLog(@"select_dbTimeStr = %@",dbTimeStr);
    
    if(sqlite3_prepare_v2(database, [dbTimeStr UTF8String], -1, &stmt, NULL) == SQLITE_OK) {
        while(sqlite3_step(stmt) == SQLITE_ROW) {
            const  unsigned  char *dateStr = sqlite3_column_text(stmt, 0);
            NSString *timeStr = [NSString stringWithFormat:@"%s",dateStr];
            //  NSLog(@"timeStr = %@",timeStr);
            [dbTimeArray addObject:timeStr];
        }
        sqlite3_finalize(stmt);
        
    }
    
    NSLog(@"dbTimeArray = %@",dbTimeArray);
    __LOG_METHOD_END;
    return dbTimeArray;
}


/**
 return DB路径
 */
+ (NSString *)getDbPath{
    __LOG_METHOD_START;
    if (database_path == NULL) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documents = [paths objectAtIndex:0];
        database_path = [documents stringByAppendingPathComponent:@"seagull.sqlite"];
    }
    NSLog(@"database_path = %@",database_path);
    __LOG_METHOD_END;
    return database_path;
    
    
}


+ (NSMutableArray *)getPedTimeArr :(sqlite3 *)database{
    __LOG_METHOD_START;
    [self getDbPath];
    NSMutableArray *dbTimeArray = [[NSMutableArray alloc]init];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    sqlite3_stmt *stmt = nil;
    
    // local  date time  from sleep
    NSString *dbTimeStr =[NSString stringWithFormat:@"select  distinct date(datetime(time,'%ld hour','+0 minute'))  as time1 from ped order by time1 desc;",(long)[[df timeZone] secondsFromGMT]/3600];
    
    
    NSLog(@"select_dbTimeStr = %@",dbTimeStr);
    
    if(sqlite3_prepare_v2(database, [dbTimeStr UTF8String], -1, &stmt, NULL) == SQLITE_OK) {
        while(sqlite3_step(stmt) == SQLITE_ROW) {
            const  unsigned  char *dateStr = sqlite3_column_text(stmt, 0);
            NSString *timeStr = [NSString stringWithFormat:@"%s",dateStr];
            //  NSLog(@"timeStr = %@",timeStr);
            [dbTimeArray addObject:timeStr];
        }
        sqlite3_finalize(stmt);
        
    }
    
    //     sqlite3_close(database);
    NSLog(@"dbTimeArray = %@",dbTimeArray);
    //    sqlite3_close(database);
    __LOG_METHOD_END;
    return dbTimeArray;
}


/**
 关闭数据库
 */
+ (void)closeDB:(sqlite3 *)database{
    __LOG_METHOD_START;
    if(sqlite3_close(database) == SQLITE_OK){
        NSLog(@"关闭数据库成功");
    }
    else{
        NSLog(@"关闭数据库失败!!!!????!!!!!!");
    }
    __LOG_METHOD_END;
}


/**
 获得/Documents  path
 */
+ (NSString *)getDocumentPath{
    __LOG_METHOD_START;
    if (documentPath == NULL) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentPath = [paths objectAtIndex:0];//去处需要的路径
    }
    __LOG_METHOD_END;
    return documentPath;
}



/**
 确定sleepStartTime ，sleepEndTime，beoforeFirstSleepPhase
 */
+ (void)calculateSleepTime : (NSString*)sleep_date :(sqlite3 *)database{
    __LOG_METHOD_START;
    [self getDbPath];
    
    totalEndTime = 0;
    jumpOutLoopFlag = NO;
    sleepStartTime = @"2020-01-01 01:01:33";
    sleepEndTime = @"1970-01-01 01:01:33";
    beoforeFirstSleepPhase = -1;
    NSString *latestDateStr;
    NSString  *yestDateStr;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSInteger const timeZone = [[dateFormatter timeZone] secondsFromGMT]/3600;
    NSLog(@"timeZone = %ld", (long)timeZone);
    if (sleep_date.length > 0) {
        latestDateStr = sleep_date;
        NSLog(@"latestDateStr = %@",latestDateStr);
        
        NSDate *latestDate = [dateFormatter dateFromString:latestDateStr ];
        NSDate  *yestDate =  [latestDate dateByAddingTimeInterval:-24*60*60];
        
        yestDateStr = [dateFormatter stringFromDate:yestDate ];
        
        
        NSLog(@"yestDateStr = %@",yestDateStr);
        
        
    }
    
    else{
        
        __LOG_METHOD_END;
        return;
    }
    
    //查询今天最早的一个 init_code == 0 的 timestamp  , 认为user睡觉结束的时间
    NSString *sql  =[NSString stringWithFormat: @"  select  distinct datetime(time,'%ld hour','+0 minute') as time1,sleep_phase   from sleep where init_code = 0 and time1 like '%@%%' order by time1  asc limit 1 ;",(long)timeZone,latestDateStr];
    
    //查询今天最早的一个 timestamp  ,init_code
    NSString *sql1  = [NSString stringWithFormat: @"  select  distinct datetime(time,'%ld hour','+0 minute') as time1 ,init_code  from sleep  where time1 like '%@%%' order by time1 asc limit 1 ;",(long)timeZone,latestDateStr];
    
    //查询昨天的 最迟的（接近今天0点的）一段连续的数据 ;
    NSString *sql2  = [NSString stringWithFormat:@"  select  distinct datetime(time,'%ld hour','+0 minute') as time1 ,init_code  from sleep  where time1 like '%@%%' order by time1 desc  ;",(long)timeZone,yestDateStr];
    
    //查询 今天的第一个 init_code == 1的timestamp
    NSString *sql3  = [NSString stringWithFormat:@"  select  distinct datetime(time,'%ld hour','+0 minute') as time1 ,init_code  from sleep  where time1 like '%@%%' and init_code = 1 order by time1 asc limit 1  ;",(long)timeZone,latestDateStr];
    
    sqlite3_stmt *stmt0;
    //查询今天最早的一个 init_code == 0 的 timestamp  , 认为user睡觉结束的时间
    NSLog(@"sql = %@",sql);
    if(sqlite3_prepare_v2(database, [sql UTF8String], -1, &stmt0, NULL) == SQLITE_OK) {
        while(sqlite3_step(stmt0) == SQLITE_ROW) {
            
            const  unsigned  char *time11 = sqlite3_column_text(stmt0, 0);
            NSString *timeStr =[NSString stringWithFormat:@"%s",time11];
            
            NSLog(@"查询今天最早的一个数据 init_code == 0 的数据 timeStr == %@",timeStr);
            
            sleepEndTime = timeStr;
        }
        sqlite3_finalize(stmt0);
    }
    
    
    
    sqlite3_stmt *stmt;
    //查询今天最早的一个 timestamp  ,init_code
    //    NSString *sql1  = @"  select  distinct datetime(time,'8 hour','+0 minute') as time1 ,init_code  from sleep  where time1 like '2014-07-10%' order by time1 asc limit 1 ;";
    NSLog(@"sql1 = %@",sql1);
    if(sqlite3_prepare_v2(database, [sql1 UTF8String], -1, &stmt, NULL) == SQLITE_OK) {
        while(sqlite3_step(stmt) == SQLITE_ROW && (!jumpOutLoopFlag)) {
            const  unsigned  char *time11 = sqlite3_column_text(stmt, 0);
            NSString *timeStr =[NSString stringWithFormat:@"%s",time11];
            int firstSleepInitCode = sqlite3_column_int(stmt, 1);
            
            NSLog(@"查询今天最早的一个  firstSleepInitCode == %d , time = %@",firstSleepInitCode,timeStr);
            
            
            if (firstSleepInitCode == 1) {
                NSLog(@"sleep before 0:00");
                //                NSString *sql2  = @"  select  distinct datetime(time,'8 hour','+0 minute') as time1 ,init_code  from sleep  where time1 like '2014-07-09%' order by time1 desc  ;";
                NSLog(@"sql2 = %@",sql2);
                sqlite3_stmt *stmt2;
                
                if(sqlite3_prepare_v2(database, [sql2 UTF8String], -1, &stmt2, NULL) == SQLITE_OK) {
                    //                    NSLog(@"----------SQLITE_OK-----");
                    while(sqlite3_step(stmt2) == SQLITE_ROW) {
                        //                          NSLog(@"---------- WHILE SQLITE_ROW-----");
                        
                        const  unsigned  char *time11 = sqlite3_column_text(stmt2, 0);
                        NSString *timeStr =[NSString stringWithFormat:@"%s",time11];
                        int firstSleepInitCode_2 = sqlite3_column_int(stmt2, 1);
                        //查询昨天的 最迟的（接近今天0点的）一段连续的数据
                        NSLog(@"查询昨天的 最迟的（接近今天0点的）一段连续的数据firstSleepInitCode_2 == %d , time = %@",firstSleepInitCode_2,timeStr);
                        
                        //假如昨天的睡觉和今天的是连续的
                        if (firstSleepInitCode_2 == 1) {
                            //                            NSLog(@"add to sleep arr===");
                            sleepStartTime = timeStr;
                            
                        }
                        
                        //不连续
                        else{
                            if ([sleepStartTime isEqualToString: @"2020-01-01 01:01:33"]) {
                                //特殊情况，  昨天的最后一条 不为1，但今天的第一条是1；
                                NSLog(@"special situation-----");
                                //查询 今天的第一个 init_code == 1的timestamp
                                //                                NSString *sql3  = @"  select  distinct datetime(time,'8 hour','+0 minute') as time1 ,init_code  from sleep  where time1 like '2014-07-10%' and init_code = 1 order by time1 asc limit 1  ;";
                                NSLog(@"sql3 = %@",sql3);
                                
                                sqlite3_stmt *stmt_sql3;
                                
                                if(sqlite3_prepare_v2(database, [sql3 UTF8String], -1, &stmt_sql3, NULL) == SQLITE_OK) {
                                    while(sqlite3_step(stmt_sql3) == SQLITE_ROW) {
                                        const  unsigned  char *time11 = sqlite3_column_text(stmt_sql3, 0);
                                        NSString *timeStr =[NSString stringWithFormat:@"%s",time11];
                                        int firstSleepInitCode = sqlite3_column_int(stmt_sql3, 1);
                                        
                                        NSLog(@" 特殊情况 firstSleepInitCode == %d , time = %@",firstSleepInitCode,timeStr);
                                        sleepStartTime = timeStr;
                                        
                                    }
                                    
                                    sqlite3_finalize(stmt_sql3);
                                }
                                
                                
                                
                            }
                            else{
                                
                            }
                            
                            NSLog(@" break =====");
                            //此时还没有sleepStartTime
                            jumpOutLoopFlag = YES;
                            break;
                            
                            
                        }
                    }
                    
                    sqlite3_finalize(stmt2);
                    
                    /*
                     //TEST 昨天没有sleep数据的情况
                     if ([sleepStartTime isEqualToString: @"2020-01-01 01:01:33"]) {
                     //特殊情况，  昨天的最后一条 不为1，但今天的第一条是1；
                     NSLog(@"special situation-----");
                     //查询 今天的第一个 init_code == 1的timestamp
                     //                                NSString *sql3  = @"  select  distinct datetime(time,'8 hour','+0 minute') as time1 ,init_code  from sleep  where time1 like '2014-07-10%' and init_code = 1 order by time1 asc limit 1  ;";
                     NSLog(@"sql3 = %@",sql3);
                     
                     
                     sqlite3_stmt *stmt3a;
                     if(sqlite3_prepare_v2(database, [sql3 UTF8String], -1, &stmt3a, NULL) == SQLITE_OK) {
                     while(sqlite3_step(stmt3a) == SQLITE_ROW) {
                     const  unsigned  char *time11 = sqlite3_column_text(stmt3a, 0);
                     NSString *timeStr =[NSString stringWithFormat:@"%s",time11];
                     int firstSleepInitCode = sqlite3_column_int(stmt3a, 1);
                     
                     NSLog(@" 特殊情况 firstSleepInitCode == %d , time = %@",firstSleepInitCode,timeStr);
                     sleepStartTime = timeStr;
                     
                     }
                     
                     sqlite3_finalize(stmt3a);
                     }
                     
                     
                     
                     }
                     
                     
                     //TEST end
                     
                     */
                    //break  跳出本次循环到此
                }
                
            }
            
            else{
                NSLog(@"sleep after 0:00");
                //查询 今天的第一个 init_code == 1的timestamp
                //                NSString *sql3  = @"  select  distinct datetime(time,'8 hour','+0 minute') as time1 ,init_code  from sleep  where time1 like '2014-07-10%' and init_code = 1 order by time1 asc limit 1  ;";
                sqlite3_stmt *stmt3_sql3;
                
                if(sqlite3_prepare_v2(database, [sql3 UTF8String], -1, &stmt3_sql3, NULL) == SQLITE_OK) {
                    while(sqlite3_step(stmt3_sql3) == SQLITE_ROW) {
                        const  unsigned  char *time11 = sqlite3_column_text(stmt3_sql3, 0);
                        NSString *timeStr =[NSString stringWithFormat:@"%s",time11];
                        int firstSleepInitCode = sqlite3_column_int(stmt3_sql3, 1);
                        
                        NSLog(@"firstSleepInitCode == %d , time = %@",firstSleepInitCode,timeStr);
                        sleepStartTime = timeStr;
                        
                    }
                    sqlite3_finalize(stmt3_sql3);
                }
            }
            
        }
        
        sqlite3_finalize(stmt);
    }
    
    NSLog(@"sleepStartTime = %@,sleepEndTime = %@",sleepStartTime,sleepEndTime);
    NSDateFormatter *dateFor = [[NSDateFormatter alloc]init];
    [dateFor setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *endDate = [dateFor dateFromString:sleepEndTime];
    
    NSDate *startDate = [dateFor dateFromString:sleepStartTime];
    
    NSTimeInterval sleepInterval =  [endDate timeIntervalSinceDate:startDate];
    
    NSLog(@"sleepInterval = %f",sleepInterval);
    
    if (sleepInterval > 0 ) {
        //用户把band按到sleep mode 并按回wake mode的情况
        int minTotal = sleepInterval/60;
        totalEndTime = minTotal;
        int hour = minTotal/60;
        int min = minTotal%60;
        NSLog(@"%d h %d min ",hour,min);
        
    }
    else{
        //用户什么都没操作的情况，默认用上一天的10：00pm 到这一天的5：00 am 作为sleep time
        
        sleepStartTime  = [NSString stringWithFormat:@"%@ 22:00:00",yestDateStr];
        sleepEndTime  = [NSString stringWithFormat:@"%@ 05:00:00",latestDateStr];
        
        NSDate *endDate = [dateFor dateFromString:sleepEndTime];
        
        NSDate *startDate = [dateFor dateFromString:sleepStartTime];
        
        NSTimeInterval sleepInterval2 =  [endDate timeIntervalSinceDate:startDate];
        int minTotal = sleepInterval2/60;
        
        totalEndTime = minTotal;
        
        
        int hour = minTotal/60;
        
        int min = minTotal%60;
        
        
        
        NSLog(@"sleepInterval2 %d h %d min ",hour,min);
        
        //查询上一天 <10：00 pm 且最靠近10：00PM 的一条sleep time
        NSString *sql_nearest = [NSString stringWithFormat:@"  select  distinct datetime(time,'%ld hour','+0 minute') as time1 ,sleep_phase,init_code  from sleep  where  time1<'%@' order by time1 desc limit 1  ;",(long)timeZone,sleepStartTime];
        NSLog(@"查询上一天 <10：00 pm 且最靠近10：00PM 的一条sleep time  sql_nearest = %@",sql_nearest);
        
        sqlite3_stmt *stmt3_sql_nearest;
        
        if(sqlite3_prepare_v2(database, [sql_nearest UTF8String], -1, &stmt3_sql_nearest, NULL) == SQLITE_OK) {
            int count = 0;
            while(sqlite3_step(stmt3_sql_nearest) == SQLITE_ROW) {
                beoforeFirstSleepPhase = sqlite3_column_int(stmt3_sql_nearest, 1);//beoforeFirstSleepPhase = -1 时，代表这里没查到数据
                
                const  unsigned  char *time11 = sqlite3_column_text(stmt3_sql_nearest, 0);
                beoforeFirstSleepTime =[NSString stringWithFormat:@"%s",time11];
                count++;
                
            }
            sqlite3_finalize(stmt3_sql_nearest);
            NSLog(@"sql result has %d records",count);
            if (count == 0) {
                //没查到数据，则认为是第一次用band
                NSLog(@" first time use band,no need to calculate sleep time");
                totalEndTime = 0;
            }
        }
        
        
        
    }
    __LOG_METHOD_END;
}


/**
 确定2天(8/2~8/3)内睡觉的次数和每次睡觉的起始时间 ,if 在按band的情况下sleepMode/wakeMode
 */
+(NSMutableArray *) calculateSleepStartEndTimeArr :(NSString *) endDateStr :(sqlite3 *)database{
    __LOG_METHOD_START;
    NSMutableArray *sleepTimesArr = [[NSMutableArray alloc]init];
    NSDate *today = [[CommonValues timeDateFormatter:DateFormatterDate ] dateFromString:endDateStr];
    NSDate  *yestDate =  [today dateByAddingTimeInterval:-24*60*60];
    NSString *yestDateStr = [[CommonValues timeDateFormatter:DateFormatterDate ] stringFromDate:yestDate];
    
    NSString *startEndSleepArrSql = [NSString stringWithFormat:@"select datetime(datetime(time,'%ld hour','+0 minute'))  as time1,init_code,sleep_phase from sleep where time1 > '%@ 00:00:00' and time1 < '%@ 24:00:00' order by time1 asc ;"
                                     , (long)[[[CommonValues timeDateFormatter:DateFormatterDate] timeZone] secondsFromGMT]/3600,yestDateStr,endDateStr];
    NSLog(@"startEndSleepArrSql = %@",startEndSleepArrSql);
    
    sqlite3_stmt *stmt;
    if(sqlite3_prepare_v2(database, [startEndSleepArrSql UTF8String], -1, &stmt, NULL) == SQLITE_OK) {
        NSString *tmpStartTime = @"";
        NSString *tmpEndTime = @"";
        BOOL startSleepFlag = NO;
        while(sqlite3_step(stmt) == SQLITE_ROW) {
            const  unsigned  char *dateStr = sqlite3_column_text(stmt, 0);
            NSString *dateString = [NSString stringWithFormat:@"%s",dateStr];
            int initCode = sqlite3_column_int(stmt, 1);
            //            NSLog(@"dateString = %@",dateString);
            //             NSLog(@"initCode = %d",initCode);
            if (initCode == 1) {
                if (!startSleepFlag) {
                    tmpStartTime = dateString;
                    startSleepFlag = YES;
                    
                }
                else{
                    
                }
            }
            else{
                if (startSleepFlag) {
                    NSMutableArray *onceSleepArr = [[NSMutableArray alloc] init];
                    tmpEndTime = dateString;
                    startSleepFlag = NO;
                    [onceSleepArr addObject:tmpStartTime];
                    [onceSleepArr addObject:tmpEndTime];
                    [sleepTimesArr addObject:onceSleepArr];
                }
                
            }
            
            
        }
        sqlite3_finalize(stmt);
    }
    NSLog(@"sleepTimesArr = %@",sleepTimesArr);
    __LOG_METHOD_END;
    return sleepTimesArr;
}


+(void)calculateConstantSleepTime : (NSString*)sleep_date :(sqlite3 *)database{
    __LOG_METHOD_START;
    //start Time 10:00pm,end time 5:00 am
    NSString *latestDateStr;
    NSString  *yestDateStr;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSInteger const timeZone = [[dateFormatter timeZone] secondsFromGMT]/3600;
    
    NSLog(@"timeZone = %ld", (long)timeZone);
    
    if (sleep_date.length > 0) {
        latestDateStr = sleep_date;
        NSLog(@"latestDateStr = %@",latestDateStr);
        
        NSDate *latestDate = [dateFormatter dateFromString:latestDateStr ];
        NSDate  *yestDate =  [latestDate dateByAddingTimeInterval:-24*60*60];
        yestDateStr = [dateFormatter stringFromDate:yestDate ];
        NSLog(@"yestDateStr = %@",yestDateStr);
        
        
    }
    
    __LOG_METHOD_END;
    return;
}


/**
 获得最近7天的HR的数组  if isHr = TRUE
 获得最近7天的HRV的数组  if isHr = FALSE
 */
+(NSMutableArray *)getLatestWeekHROrHRVArr :(sqlite3 *)database :(BOOL)isHr{
    __LOG_METHOD_START;
    //NSMutableArray *arr = [[NSMutableArray alloc]init];
    NSMutableArray *weekAvgHRArr = [[NSMutableArray alloc]init];
    NSMutableArray *weekAvgHRVArr = [[NSMutableArray alloc]init];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    sqlite3_stmt *stmt;
    
    // local  date time  from sleep
    //查询最近7天的
    NSString *heartDBTimeStr =[NSString stringWithFormat:@"select  distinct date(datetime(time,'%ld hour','+0 minute'))  as time1 from heart order by time1 desc limit 7;", (long)[[df timeZone] secondsFromGMT]/3600];
    
    
    if(sqlite3_prepare_v2(database, [heartDBTimeStr UTF8String], -1, &stmt, NULL) == SQLITE_OK) {
        while(sqlite3_step(stmt) == SQLITE_ROW) {
            const  unsigned  char *dateStr = sqlite3_column_text(stmt, 0);
            NSString *timeStr = [NSString stringWithFormat:@"%s",dateStr];
            NSLog(@"timeStr = %@",timeStr);
            NSString *sql = [NSString stringWithFormat:@"select final_hr,avg_hrv,datetime(datetime(time,'%ld hour','+0 minute'))  as time1 from heart where time1 like '%@%%';", (long)[[df timeZone] secondsFromGMT]/3600,timeStr];
            NSLog(@"sql = %@",sql);
            sqlite3_stmt *stmt2;
            if(sqlite3_prepare_v2(database, [sql UTF8String], -1, &stmt2, NULL) == SQLITE_OK) {
                
                int sumHR = 0 ;
                int countHR = 0 ;
                int sumHRV = 0;
                while(sqlite3_step(stmt2) == SQLITE_ROW) {
                    //                    const  unsigned  char *dateStr = sqlite3_column_text(stmt2, 0);
                    //                    NSString *timeStr = [NSString stringWithFormat:@"%s",dateStr];
                    //  NSLog(@"timeStr = %@",timeStr);
                    int hr = sqlite3_column_int(stmt2, 0);
                    //                    NSLog(@"final_hr = %d",hr);
                    int hrv = sqlite3_column_int(stmt2, 1);
                    sumHR += hr;
                    countHR ++;
                    sumHRV += hrv;
                }
                if ( countHR ) {
                    [weekAvgHRArr addObject:[NSNumber numberWithInt:sumHR/countHR]];
                    [weekAvgHRVArr addObject:[NSNumber numberWithInt:sumHRV/countHR]];
                    }
                sqlite3_finalize(stmt2);
                
            }
            
            
            
        }
        sqlite3_finalize(stmt);
        
    }
    
    //    NSLog(@"最近7天的heart Date arr = %@",arr);
    
    NSLog(@"weekAvgHRArr = %@",weekAvgHRArr);
    NSLog(@"weekAvgHRVArr = %@",weekAvgHRVArr);
    NSMutableArray * const arr = (isHr) ? weekAvgHRArr : weekAvgHRVArr;

    __LOG_METHOD_END;
    return arr;
}


/**
 获得所有HR的数组  if isHr = TRUE
 获得所有HRV的数组  if isHr = FALSE
 */
+(NSMutableArray *)getHistoryHROrHRVArr :(sqlite3 *)database :(BOOL)isHr{
    __LOG_METHOD_START;
    //NSMutableArray *arr = [[NSMutableArray alloc]init];
    NSMutableArray *historyHRArr = [[NSMutableArray alloc]init];
    NSMutableArray *historyHRVArr = [[NSMutableArray alloc]init];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    sqlite3_stmt *stmt;
    
    // local  date time  from sleep
    //查询历史日期有哪些
    NSString *heartDBTimeStr =[NSString stringWithFormat:@"select  distinct date(datetime(time,'%ld hour','+0 minute'))  as time1 from heart where final_hr != 0  order by time1 asc;", (long)[[df timeZone] secondsFromGMT]/3600];
    
    
    if(sqlite3_prepare_v2(database, [heartDBTimeStr UTF8String], -1, &stmt, NULL) == SQLITE_OK) {
        while(sqlite3_step(stmt) == SQLITE_ROW) {
            const  unsigned  char *dateStr = sqlite3_column_text(stmt, 0);
            NSString *timeStr = [NSString stringWithFormat:@"%s",dateStr];
            NSLog(@"timeStr = %@",timeStr);
            
            NSString *sql = [NSString stringWithFormat:@"select final_hr,avg_hrv,datetime(datetime(time,'%ld hour','+0 minute'))  as time1 from heart where time1 like '%@%%' and final_hr != 0 order by time1 asc;", (long)[[df timeZone] secondsFromGMT]/3600,timeStr];
            
            NSLog(@"sql = %@",sql);
            sqlite3_stmt *stmt2;
            NSMutableArray *tempHRArr = [[NSMutableArray alloc]init];
            NSMutableArray *tempHRVArr = [[NSMutableArray alloc]init];
            if(sqlite3_prepare_v2(database, [sql UTF8String], -1, &stmt2, NULL) == SQLITE_OK) {
                while(sqlite3_step(stmt2) == SQLITE_ROW) {
                    //                    const  unsigned  char *dateStr = sqlite3_column_text(stmt2, 0);
                    //                    NSString *timeStr = [NSString stringWithFormat:@"%s",dateStr];
                    //  NSLog(@"timeStr = %@",timeStr);
                    int hr = sqlite3_column_int(stmt2, 0);
                    int hrv = sqlite3_column_int(stmt2, 1);
                    [ tempHRArr addObject:[NSNumber numberWithInt:hr]];
                    [tempHRVArr addObject:[NSNumber numberWithInt:hrv]];
                    
                }
                sqlite3_finalize(stmt2);
            }
            [historyHRArr addObject:tempHRArr];
            [historyHRVArr addObject:tempHRVArr];
            
        }
        sqlite3_finalize(stmt);
        
    }
    
    //    NSLog(@"最近7天的heart Date arr = %@",arr);
    
    NSLog(@"historyHRArr = %@",historyHRArr);
    NSLog(@"historyHRVArr = %@",historyHRVArr);
    NSMutableArray * const arr = (isHr) ? historyHRArr : historyHRVArr;

    __LOG_METHOD_END;
    return arr;
}

+(NSMutableArray *)getHistoryHROrHRVArr2 :(sqlite3 *)database :(BOOL)isHr{
    __LOG_METHOD_START;
    //NSMutableArray *arr = [[NSMutableArray alloc]init];
    NSMutableArray *historyHRArr = [[NSMutableArray alloc]init];
    NSMutableArray *historyHRVArr = [[NSMutableArray alloc]init];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    //    sqlite3_stmt *stmt;
    
    // local  date time  from sleep
    //查询历史日期有哪些
    //    NSString *heartDBTimeStr =[NSString stringWithFormat:@"select  distinct date(datetime(time,'%d hour','+0 minute'))  as time1 from heart order by time1 asc;",[[df timeZone] secondsFromGMT]/3600];
    
    
    //    if(sqlite3_prepare_v2(database, [heartDBTimeStr UTF8String], -1, &stmt, NULL) == SQLITE_OK) {
    //        while(sqlite3_step(stmt) == SQLITE_ROW) {
    //            const  unsigned  char *dateStr = sqlite3_column_text(stmt, 0);
    //            NSString *timeStr = [NSString stringWithFormat:@"%s",dateStr];
    //            NSLog(@"timeStr = %@",timeStr);
    
    NSString *sql = [NSString stringWithFormat:@"select final_hr,avg_hrv,datetime(datetime(time,'%ld hour','+0 minute'))  as time1 from heart where final_hr != 0  order by time1 asc;", (long)[[df timeZone] secondsFromGMT]/3600];
    
    NSLog(@"sql = %@",sql);
    
    sqlite3_stmt *stmt2;
    
    
    //            NSMutableArray *tempHRArr = [[NSMutableArray alloc]init];
    //            NSMutableArray *tempHRVArr = [[NSMutableArray alloc]init];
    
    
    if(sqlite3_prepare_v2(database, [sql UTF8String], -1, &stmt2, NULL) == SQLITE_OK) {
        while(sqlite3_step(stmt2) == SQLITE_ROW) {
            //                    const  unsigned  char *dateStr = sqlite3_column_text(stmt2, 0);
            //                    NSString *timeStr = [NSString stringWithFormat:@"%s",dateStr];
            //  NSLog(@"timeStr = %@",timeStr);
            int hr = sqlite3_column_int(stmt2, 0);
            //                    NSLog(@"final_hr = %d",hr);
            int hrv = sqlite3_column_int(stmt2, 1);
            //                    [ tempHRArr addObject:[NSNumber numberWithInt:hr]];
            //                    [tempHRVArr addObject:[NSNumber numberWithInt:hrv]];
            [ historyHRArr addObject:[NSNumber numberWithInt:hr]];
            [historyHRVArr addObject:[NSNumber numberWithInt:hrv]];
            
        }
        
        
        
        sqlite3_finalize(stmt2);
        
    }
    NSLog(@"historyHRArr = %@",historyHRArr);
    NSLog(@"historyHRVArr = %@",historyHRVArr);
    NSMutableArray * const arr = (isHr) ? historyHRArr : historyHRVArr;

    __LOG_METHOD_END;
    return arr;
}



+(void)insertCloudSleepData :(sqlite3 *)db{
    __LOG_METHOD_START;
    //从S3取sleep文件 放到Document下，sleep数据插到DB，看sleep 图标来debug
    
    NSError *error;
    
    NSString *documents = [self getDocumentPath];
    
    //    NSString *path =  [documents stringByAppendingPathComponent:@"FFFFFFFFFFFFFF-2014-07-30T07-03-46Z-2.0.14-221-117-1.0.68-DVT1_sleep.txt"];
    //    NSLog(@"path = %@",path);
    documents = [documents stringByAppendingPathComponent:@"CloudSleepData"];
    NSArray *arr  =    [FileOperation getFileArr:@"/CloudSleepData"];
    NSLog(@"CloudSleepData file arr = %@",arr);
    
    
    for (int i = 0; i<arr.count; i++) {
        NSString *path =  [documents stringByAppendingPathComponent:arr[i]];
        NSLog(@"path = %@",path);
        NSData *sleepData = [NSData dataWithContentsOfFile:path];
        
        NSDictionary *sleepDic = [NSJSONSerialization JSONObjectWithData:sleepData options:NSJSONReadingMutableLeaves error:&error ];
        
        NSDictionary *sleepInfo = [sleepDic objectForKey:@"sleepRecords"];
        // NSEnumerator示例
        NSEnumerator *enumerator;
        enumerator = [sleepInfo objectEnumerator];
        id thing;
        while (thing = [enumerator nextObject]) {
            //       NSLog(@"I found %@",thing);
            NSDictionary *tmp = thing;
            
            NSString *initCode =  [tmp objectForKey:@"initCode"];
            NSString *phase =  [tmp objectForKey:@"phase"];
            NSString *time =  [tmp objectForKey:@"time"];
            
            NSString *tt =  [time substringToIndex:(time.length-1)];
            
            NSLog(@"time = %@,phase = %@,initCode =%@",tt,phase,initCode);
            NSDateFormatter *dfff = [[NSDateFormatter alloc]init];
            [dfff setDateFormat:@"yyyy_MM_dd_HH_mm_ss"];
            NSDate *tmpDate = [dfff dateFromString:tt];
            
            
            [dfff setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
            NSString* lastTime = [dfff stringFromDate:tmpDate];
            //insert sleep data   to db
            NSString *sql1 = [NSString stringWithFormat:
                              @"INSERT INTO SLEEP ('time', 'sleep_phase','init_code') VALUES ('%@', '%@','%@')",
                              lastTime,phase,initCode];
            //    NSLog(@"insert to Sleep ,sql1 = %@ ",sql1);
            
            [CommonValues execSql:sql1 :db];
            
        }
    }
    __LOG_METHOD_END;
}

+(void)insertCloudPedData :(sqlite3 *)db{
    __LOG_METHOD_START;
    //从S3取sleep文件 放到Document下，sleep数据插到DB，看sleep 图标来debug
    
    NSError *error;
    NSString *documents = [self getDocumentPath];
    documents = [documents stringByAppendingPathComponent:@"CloudPedData"];
    NSArray *arr  =    [FileOperation getFileArr:@"/CloudPedData"];
    NSLog(@"ped file arr = %@",arr);
    for (int i = 0; i<arr.count; i++) {
        NSString *path =  [documents stringByAppendingPathComponent:arr[i]];
        NSLog(@"path = %@",path);
        NSData *sleepData = [NSData dataWithContentsOfFile:path];
        
        NSDictionary *sleepDic = [NSJSONSerialization JSONObjectWithData:sleepData options:NSJSONReadingMutableLeaves error:&error ];
        
        NSDictionary *sleepInfo = [sleepDic objectForKey:@"PED"];
        //    NSLog(@"ped dic = %@",sleepInfo);
        // NSEnumerator示例
        NSEnumerator *enumerator;
        enumerator = [sleepInfo objectEnumerator];
        id thing;
        while (thing = [enumerator nextObject]) {
            //       NSLog(@"I found %@",thing);
            NSDictionary *tmp = thing;
            
            NSString *calories =  [tmp objectForKey:@"Calories"];
            NSString *step =  [tmp objectForKey:@"Step"];
            NSString *time =  [tmp objectForKey:@"Time"];
            NSString *distance =  [tmp objectForKey:@"Distance"];
            
            NSString *tt =  [time substringToIndex:(time.length-1)];
            
            NSLog(@"time = %@,step = %@,calories =%@ ,distance =%@",tt,step,calories,distance);
            NSDateFormatter *dfff = [[NSDateFormatter alloc]init];
            [dfff setDateFormat:@"yyyy_MM_dd_HH_mm_ss"];
            NSDate *tmpDate = [dfff dateFromString:tt];
            
            
            [dfff setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
            NSString* lastTime = [dfff stringFromDate:tmpDate];
            //insert sleep data   to db
            //        NSString *sql1 = [NSString stringWithFormat:
            //                          @"INSERT INTO SLEEP ('time', 'sleep_phase','init_code') VALUES ('%@', '%@','%@')",
            //                          lastTime,phase,initCode];
            //        //    NSLog(@"insert to Sleep ,sql1 = %@ ",sql1);
            //
            //        [CommonValues execSql:sql1 :db];
            
            
            NSString *sql2 = [NSString stringWithFormat:
                              @"INSERT INTO PED ('time', 'step','calories', 'distance') VALUES ('%@', '%@','%@', '%@')",
                              lastTime, step ,calories ,distance ];
            
            [CommonValues execSql:sql2 :db];
        }
    }
    __LOG_METHOD_END;
}

/**
 自定义字体并用其粗体
 
 */
+ (UILabel *)labelWithFrame:(CGRect)frame WithBoldFont:(UIFont*)font strokeWidth:(float)width withAttributeText:(NSString*)string
{
    __LOG_METHOD_START;
    NSAttributedString *atbString = [[NSAttributedString alloc]
                                     initWithString:string
                                     attributes:[NSDictionary dictionaryWithObjectsAndKeys:font,NSFontAttributeName,
                                                 [NSNumber numberWithFloat:width],NSStrokeWidthAttributeName, nil]];
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.attributedText = atbString;
    __LOG_METHOD_END;
    return label;
}



/**
 *
 *找出数组中最大的一个数
 */
+ (int) findMaxNumInArr:(NSMutableArray *) mulArr {
    __LOG_METHOD_START;
    int maxNum = 0;
    for (int i = 0; i < mulArr.count ; i++) {
        int temp = [[mulArr objectAtIndex:i] intValue];
        if (temp > maxNum) {
            maxNum = temp;
        }
    }
    NSLog(@"maxNum = %d",maxNum);
    __LOG_METHOD_END;
    return maxNum;
}



/**
 *
 *获得7天的dateArr，和对应的HRArr  if(isHR ==YES)  else 获得 HRVArr
 */
+ (NSMutableArray *) getOneWeekHRDetail : (sqlite3 *) database : (NSString *) lastDateInOneWeek :(BOOL) isHR {
    
    __LOG_METHOD_START;
    //    NSString *lastDateInOneWeek1 = @"2014-10-09";
    NSDateFormatter *df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSDate *lastDate =  [df dateFromString:lastDateInOneWeek];
    NSDate *firstDate = [lastDate dateByAddingTimeInterval:-6*24*60*60];
    NSString *firstDateInOneWeek = [df stringFromDate:firstDate];
    NSLog(@"firstDateInOneWeek = %@",firstDateInOneWeek);
    
    
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    
    NSString *sqlStr =   [NSString stringWithFormat:@"select date(datetime(time,'%ld hour','+0 minute'))  as time1,final_hr,avg_hrv from heart where final_hr != 0 and time1 >= '%@' and time1 < '%@ 24:00:00' order by time1 asc", (long)[[df timeZone] secondsFromGMT]/3600,firstDateInOneWeek,lastDateInOneWeek];
    //     NSString *sqlStr  = @"select time,final_hr,avg_hrv from heart";
    
    sqlite3_stmt *stmtSql;
    NSLog(@"sqlStr = %@",sqlStr);
    NSMutableArray *hr_arr = [[NSMutableArray alloc]init];
    NSMutableArray *hrv_arr = [[NSMutableArray alloc]init];
    NSMutableArray *date_arr = [[NSMutableArray alloc]init];
    
    if(sqlite3_prepare_v2(database, [sqlStr UTF8String], -1, &stmtSql, NULL) == SQLITE_OK) {
        while(sqlite3_step(stmtSql) == SQLITE_ROW) {
            const  unsigned  char *timeCharStr = sqlite3_column_text(stmtSql, 0);
            int final_hr = sqlite3_column_int(stmtSql, 1);
            int avg_hrv = sqlite3_column_int(stmtSql, 2);
            NSString* timeStr = [NSString stringWithFormat:@"%s",timeCharStr];
            NSString* timeDateStr = [timeStr substringToIndex:10];
            //            NSLog(@"timeDateStr = %@",timeDateStr);
            //             NSLog(@"final_hr = %d",final_hr);
            
            [hr_arr addObject:[NSNumber numberWithInt:final_hr]];
            [hrv_arr addObject:[NSNumber numberWithInt:avg_hrv]];
            [date_arr addObject:timeDateStr];
        }
        sqlite3_finalize(stmtSql);
    }
    //    NSLog(@"timeDateStr = %@",timeDateStr);
    if (isHR) {
        [arr addObject:date_arr];
        [arr addObject:hr_arr];
    }
    else{
        [arr addObject:date_arr];
        [arr addObject:hrv_arr];
    }
    
    
    NSLog(@"arr = %@",arr);
    __LOG_METHOD_END;
    return arr;
}



/**
 根据某个日期dateStr 获得最近指定天数的的日期
 */

+(NSMutableArray *) getDateArr : (NSString *) dateStr : (NSInteger)dayCount{
    __LOG_METHOD_START;
    NSMutableArray *oneWeekDateArr = [[NSMutableArray alloc]init];
    NSDateFormatter *df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSDate *inputDate =  [df dateFromString:dateStr];
    NSLog(@"input dayCount ========= %ld",(long)dayCount);
    
    [df setDateFormat:@"MM/dd"];
    for (NSInteger i = dayCount-1 ; i > -1; i--) {
        NSDate *tempDate  = [inputDate dateByAddingTimeInterval:-24*60*60*i];
        NSString *tempStr = [df stringFromDate:tempDate];
        [oneWeekDateArr addObject:tempStr];
        
    }
    
    NSLog(@"getDateArr = %@",oneWeekDateArr);
    
    __LOG_METHOD_END;
    return oneWeekDateArr;
}


/**
 根据某个日期dateStr 获得最近一周的日期
 */

+(NSMutableArray *) getOneWeekDateArr : (NSString *) dateStr{
    __LOG_METHOD_START;
    NSMutableArray *oneWeekDateArr = [[NSMutableArray alloc]init];
    NSDateFormatter *df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSDate *inputDate =  [df dateFromString:dateStr];
    [df setDateFormat:@"MM/dd"];
    for (int i = 6 ; i > -1; i--) {
        NSDate *tempDate  = [inputDate dateByAddingTimeInterval:-24*60*60*i];
        NSString *tempStr = [df stringFromDate:tempDate];
        [oneWeekDateArr addObject:tempStr];
    }
    
    NSLog(@"oneWeekDateArr = %@",oneWeekDateArr);
    __LOG_METHOD_END;
    return oneWeekDateArr;
}


+(NSMutableArray *) getOneWeekDateIndexNumArr :(NSMutableArray *) inputArr  : (NSString *) dateStr{
    
    __LOG_METHOD_START;
    NSLog(@"inputArr = %@",inputArr);
    NSMutableArray *oneWeekDateIndexNumArr = [[NSMutableArray alloc]init];
    NSDateFormatter *df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSDate *inputDate =  [df dateFromString:dateStr];
    
    for (int i = 0 ; i < inputArr.count; i++) {
        
        NSString *tempStr = [inputArr objectAtIndex:i];
        NSDate *tempDate  = [df dateFromString:tempStr];
        NSTimeInterval interval =   [inputDate timeIntervalSinceDate:tempDate];
        int invervalDays =  interval/(24*60*60) ;
        
        [oneWeekDateIndexNumArr addObject:[NSNumber numberWithInt:(6-invervalDays)]];
        
    }
    
    NSLog(@"oneWeekDateIndexNumArr = %@",oneWeekDateIndexNumArr);
    
    __LOG_METHOD_END;
    return oneWeekDateIndexNumArr;
}


/**
 获得Heart表中 日期最早的那个日期
 */
+(NSString *) getHeartMinDateStr  : (sqlite3 *) database {
    
    __LOG_METHOD_START;
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *minDateStr;
    NSString *sqlStr =   [NSString stringWithFormat:@"select date(datetime(time,'%ld hour','+0 minute'))  as time1 from heart order by time1 asc limit 1", (long)[[df timeZone] secondsFromGMT]/3600];
    sqlite3_stmt *stmtSql;
    
    if(sqlite3_prepare_v2(database, [sqlStr UTF8String], -1, &stmtSql, NULL) == SQLITE_OK) {
        while(sqlite3_step(stmtSql) == SQLITE_ROW) {
            const  unsigned  char *timeCharStr = sqlite3_column_text(stmtSql, 0);
            minDateStr = [NSString stringWithFormat:@"%s",timeCharStr];
            minDateStr = [minDateStr substringToIndex:10];
        }
        sqlite3_finalize(stmtSql);
        
    }
    
    __LOG_METHOD_END;
    return minDateStr;
}




/**
 根据电量值改变UIImageView
 */
+ (void)updateConnectStateImg :(UIImageView * )stateImg withLevel:(int)level{
    __LOG_METHOD_START;
    if (level < 20) {
        stateImg.image = [UIImage imageNamed:@"Connection5"];
    }
    else if (level < 40) {
        stateImg.image = [UIImage imageNamed:@"Connection4"];
    }
    
    else if(level < 60){
        
        stateImg.image = [UIImage imageNamed:@"Connection3"];
    }
    
    else if(level < 80){
        
        stateImg.image = [UIImage imageNamed:@"Connection2"];
    }
    else{
        stateImg.image = [UIImage imageNamed:@"Connection1"];
        
    }
    __LOG_METHOD_END;
}



/**
 根据imperal units or not ,显示mi(或者yd) / km(或者m)
 当 >= 1 km时  显示km，<1 km时 显示 m;
 当 >= 1 mile 时，显示 mi ,  < 1mile 时，显示 yd
 */
+ (NSString *) distanceStrFromDistanceCount: (NSInteger) currentDistanceCount{
    __LOG_METHOD_START;
    NSString *distanceStr;
    float float_distance = 0;
    // 1 mi = 1609.344 m; 1yd = 0.9144 m
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults boolForKey:DISPLAY_IMPERIAL_UNITS]) {
        if (currentDistanceCount > 1609) {
            //四舍五入 保留一位小数
            float_distance = currentDistanceCount/1609.344f;
            //            distanceStr = [NSString stringWithFormat:@"%.1fmi",float_distance];
            
            //保留一位小数 ，去掉第一位小数后面的数值
            int int_distance = float_distance * 10;
            float_distance = int_distance/10.0;
            distanceStr = [NSString stringWithFormat:@"%.1fmi",float_distance];
            
        }
        else
        {
            
            distanceStr = [NSString stringWithFormat:@"%dyd",(int)(currentDistanceCount/0.9144f)];
        }
        
        
    }
    
    else{
        if (currentDistanceCount >= 1000) {
            //四舍五入 保留一位小数
            float_distance = currentDistanceCount/1000.0f;
            //            distanceStr = [NSString stringWithFormat:@"%.1fkm",float_distance];
            
            //保留一位小数 ，去掉第一位小数后面的数值
            int int_distance = float_distance * 10;
            float_distance = int_distance/10.0;
            distanceStr = [NSString stringWithFormat:@"%.1fkm",float_distance];
            
        }
        else{
            distanceStr = [NSString stringWithFormat:@"%ldm",(long)currentDistanceCount];
        }
        
    }
    
    NSLog(@"distanceStr = %@",distanceStr);
    
    __LOG_METHOD_END;
    return distanceStr;
}


/**
 根据系统不同  不同的offset （为解决status bar 在ios 6,和7 上适配问题）
 */
+ (int) offsetHeight4DiffiOS{
    __LOG_METHOD_START;
    //fit for ios 6
    double version = [[UIDevice currentDevice].systemVersion doubleValue];
    //    NSLog(@"IOS version = %f",version);
    int offsetHeight;
    if (version < 7.0) {
        //        NSLog(@"iOS6-------");
        offsetHeight = 0;
    }
    else{
        //        NSLog(@">=iOS7-------");
        offsetHeight = 20;
        
    }
    __LOG_METHOD_END;
    return offsetHeight;
}



/**
 Filter band, only the following devices to scan the list of names
 */
+ (NSArray *) bandDeviceArray {
    
    return  [[NSArray alloc]initWithObjects:
             @"Life Beat",@"Pelican",@"BeatBand",
             @"SG DEV", @"WAT Phase 2",@"Seagull",@"SEAGULL",@"ORT_000",
             @"ORT_001",@"ORT_002",@"ORT_003",@"ORT_004",@"ORT_005",
             @"ORT_006",@"ORT_007",@"ORT_008",@"ORT_009",@"ORT_010",
             @"ORT_011",@"ORT_012",@"ORT_013",@"ORT_014",@"ORT_015",
             @"ORT_016",@"ORT_017",@"ORT_018",@"ORT_019",@"ORT_020",
             nil];
    
}

/**
 用到的若干种日期格式
 */
+ (NSDateFormatter *) timeDateFormatter :(DateFormatter) mydf{
    __LOG_METHOD_START;
    NSDateFormatter *myDateFormatter  = [[NSDateFormatter alloc]init];
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    
    if (mydf == DateFormatterTimeAllUnderLine) {
        [myDateFormatter setDateFormat:@"yyyy_MM_dd_HH_mm_ss"];
    }
    else if (mydf == DateFormatterTimeColon){
        [myDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
    }
    else if (mydf == DateFormatterDate){
        [myDateFormatter setDateFormat:@"yyyy-MM-dd"];
        
    }
    else if (mydf == DateFormatterDateSlash){
        [myDateFormatter setDateFormat:@"MM/dd/yyyy"];
        
    }
    else if (mydf == DateFormatterTimeColonAMPM){
        [myDateFormatter setDateFormat:@"h:mm a"];
        
    }
    else if (mydf == DateFormatterDateNoneSeparater){
        [myDateFormatter setDateFormat:@"yyyyMMdd"];
        
    }
    
    else if (mydf == DateFormatterTimeAllUnderLineGMT){
        [myDateFormatter setDateFormat:@"yyyy_MM_dd_HH_mm_ss"];
        [myDateFormatter setTimeZone:gmt];
        
    }
    else if (mydf == DateFormatterTimeColonGMT){
        [myDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [myDateFormatter setTimeZone:gmt];
        
    }
    else if (mydf == DateFormatterDateGMT){
        [myDateFormatter setDateFormat:@"yyyy-MM-dd"];
        [myDateFormatter setTimeZone:gmt];
        
    }
    else if (mydf == DateFormatterDateSlashGMT){
        [myDateFormatter setDateFormat:@"MM/dd/yyyy"];
        [myDateFormatter setTimeZone:gmt];
        
    }
    
    __LOG_METHOD_END;
    return myDateFormatter;
}




/**
 获取设备版本号
 */
+ (NSString *)getiPhoneDeviceName {
    __LOG_METHOD_START;
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *deviceString = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    __LOG_METHOD_END;
    return deviceString;
}

+ (void)setViewControllerCount : (int)count{
    __LOG_METHOD_START;
    viewControllerCount = count;
    __LOG_METHOD_END;
}

+ (int) getViewControllerCount {
    return viewControllerCount;
    
}

+ (int)startSleepAnalysis :(NSInteger)sleepIndex  :(sqlite3 *)db{
    __LOG_METHOD_START;
    NSLog(@"start startSleepAnalysis method,sleepIndex = %ld",(long)sleepIndex);
    NSMutableArray * const sleepTimeArrTommy = /* [[NSMutableArray alloc]init];
    sleepTimeArrTommy = */ [CommonValues getSleepTimeArr :db];
    if (sleepTimeArrTommy.count > sleepIndex) {
        NSLog(@"[sleepTimeArrTommy objectAtIndex:sleepIndex] = %@",[sleepTimeArrTommy objectAtIndex:sleepIndex]);
        [CommonValues setSleepStartTime:@"2020-01-01 01:01:33"];
        [CommonValues setSleepEndTime:@"1970-01-01 01:01:33"];
        [CommonValues calculateSleepTime:[sleepTimeArrTommy objectAtIndex:sleepIndex] :db];
        
    }
    
    else{
        NSLog(@"sleepTimeArrTommy null, reuturn");
        return -1;
    }
    
    NSDateFormatter *dateFo = [[NSDateFormatter alloc]init];
    [dateFo setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *startTime = [CommonValues getSleepStartTime];
    NSString *endTimte = [CommonValues getSleepEndTime];
    NSDate *sTime = [dateFo dateFromString:startTime];
    NSDate *eTime = [dateFo dateFromString:endTimte];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    //    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    //    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSInteger timeZone = [[dateFo timeZone] secondsFromGMT]/3600;
    if (sleepTimeArrTommy.count > sleepIndex) {
//        NSString *latestDateStr;
//        latestDateStr = [sleepTimeArrTommy objectAtIndex:sleepIndex];
        NSLog(@"latestDateStr not null");
        
    }
    else{
        
        NSLog(@"latestDateStr is null,return");
        return -1;
    }
    
    
    
    NSLog(@"call sleepInitAnalysis----");
    NSLog(@"input sTime = %@,eTime = %@",sTime,eTime);
    //true取碎片化模式
    [[TGBleManager sharedTGBleManager] sleepInitAnalysis];
    
    if ([CommonValues getSleepPhaseBeoforeFirst] != -1) {
        NSLog(@"没有按到sleep mode的情况，要加10点前的一条数据 去分析----");
        NSDate *date = [dateFo dateFromString:[CommonValues getSleepTimeBeoforeFirst]];
        //        if (FALSE) {
        [[TGBleManager sharedTGBleManager] sleepAddData:date sleepPhase:[CommonValues getSleepPhaseBeoforeFirst]];
        
        //         NSLog(@"date = %@,phase = %d",date,[CommonValues getSleepPhaseBeoforeFirst]);
        //        }
        //        else{
        
        if ([CommonValues getSleepTotalMinutes] == 0) {
            NSLog(@"sleep time = 0 ,no need to analysis");
            
            return -1;
        }
        else{
            
            NSLog(@"按到了sleep mode的情况----");
            //        NSLog(@"SleepPhaseBeoforeFirst = %d",[CommonValues getSleepPhaseBeoforeFirst]);
            
        }
        
        //        }
    }
    
    NSString *sql  = [NSString stringWithFormat:@"select datetime(time,'%ld hour','+0 minute') as time1 ,sleep_phase  from sleep  where time1 >= '%@' and time1 <= '%@' order by time1 asc  ;", (long)timeZone,startTime,endTimte];
    NSLog(@"sql = %@",sql);
    
    sqlite3_stmt *stmt2;
    
    if(sqlite3_prepare_v2(db, [sql UTF8String], -1, &stmt2, NULL) == SQLITE_OK) {
        int count = 0;
        while(sqlite3_step(stmt2) == SQLITE_ROW) {
            //             NSLog(@"sql result  has records");
            const  unsigned  char *time11 = sqlite3_column_text(stmt2, 0);
            NSString *timeStr =[NSString stringWithFormat:@"%s",time11];
            
            //            NSLog(@"timeStr == %@",timeStr);
            NSDate *date = [dateFo dateFromString:timeStr];
            int phase =  sqlite3_column_int(stmt2, 1);
            [[TGBleManager sharedTGBleManager] sleepAddData:date sleepPhase:phase];
            //              NSLog(@"sql date = %@,phase = %d",date,phase);
            count++;
            
        }
        
        NSLog(@"sql result  has %d records",count);
        sqlite3_finalize(stmt2);
    }
    else{
        NSLog(@"sql result  no records!!!");
    }
    NSLog(@"call sleepRequestAnalysis----");
    [[TGBleManager sharedTGBleManager] sleepRequestAnalysis:sTime endTime:eTime];
    [[TGBleManager sharedTGBleManager] sleepSetInterval:0 completeData:false];
    NSLog(@"end startSleepAnalysis method");
    
    __LOG_METHOD_END;
    return 0;
}



/**
 get repeatCode from repeatArry
 repeatArry (Monday,Tuesday)
 */
+ (int) repeatCode : (NSMutableArray*) repeatArry{
    int repeatCode = 0x00;
    int mon = 0x02;int tue = 0x04;int wed = 0x08;
    int thu = 0x10;int fri = 0x20;int sat = 0x40;int sun = 0x80;
    
    
    //    if ([repeatArry containsObject:@"Once"]) {
    //        repeatCode = 0x00;
    //    }
    //    else if ([repeatArry containsObject:@"Everyday"]){
    //        repeatCode = 0x01;
    //    }
    //   else{
    
    if (repeatArry.count <= 2) {
        if ([repeatArry[0] isEqualToString:@"Monday"]) {
            repeatCode = mon;
        }
        else if ([repeatArry[0] isEqualToString:@"Tuesday"]) {
            repeatCode = tue;
        }
        else if ([repeatArry[0] isEqualToString:@"Wednesday"]) {
            repeatCode = wed;
        }
        else if ([repeatArry[0] isEqualToString:@"Thursday"]) {
            repeatCode = thu;
        }
        else if ([repeatArry[0] isEqualToString:@"Friday"]) {
            repeatCode = fri;
        }
        else if ([repeatArry[0] isEqualToString:@"Saturday"]) {
            repeatCode = sat;
        }
        else if ([repeatArry[0] isEqualToString:@"Sunday"]) {
            repeatCode = sun;
        }
        
        else if ([repeatArry[0] isEqualToString:@"Once"]) {
            repeatCode = 0x00;
        }
        else if ([repeatArry[0] isEqualToString:@"Everyday"]) {
            repeatCode = 0x01;
        }
        
    }
    
    else{
        for (int i = 0; i< repeatArry.count; i++) {
            if ([[repeatArry objectAtIndex:i] isEqualToString:@"Monday"]) {
                repeatCode = repeatCode|mon;
            }
            if ([[repeatArry objectAtIndex:i] isEqualToString:@"Tuesday"]) {
                repeatCode = repeatCode|tue;
            }
            if ([[repeatArry objectAtIndex:i] isEqualToString:@"Wednesday"]) {
                repeatCode = repeatCode|wed;
            }
            if ([[repeatArry objectAtIndex:i] isEqualToString:@"Thursday"]) {
                repeatCode = repeatCode|thu;
            }
            if ([[repeatArry objectAtIndex:i] isEqualToString:@"Friday"]) {
                repeatCode = repeatCode|fri;
            }
            if ([[repeatArry objectAtIndex:i] isEqualToString:@"Saturday"]) {
                repeatCode = repeatCode|sat;
            }
            if ([[repeatArry objectAtIndex:i] isEqualToString:@"Sunday"]) {
                repeatCode = repeatCode|sun;
            }
            
        }
        
        repeatCode = ~repeatCode;
        
    }
    
    // }
    
    NSLog(@"repeatCode = %X",repeatCode);
    
    return  repeatCode;
}



/**
 获得 active time mode值不为空的 那些日期数组
 */
+ (NSMutableArray *)activeModeDateArr :(sqlite3 *)database{
    NSMutableArray *modeDateArr = [[NSMutableArray alloc]init];
    
    NSString *sql = [NSString stringWithFormat:@"select distinct date(datetime(time,'%ld hour','+0 minute'))  as time1 from ped where  mode != 0 order by time1 desc", (long)[[[self timeDateFormatter:DateFormatterTimeAllUnderLine] timeZone] secondsFromGMT]/3600];
    sqlite3_stmt *stmt;
    NSLog(@"select mode date sql = %@",sql);
    
    if(sqlite3_prepare_v2(database, [sql UTF8String], -1, &stmt, NULL) == SQLITE_OK) {
        while(sqlite3_step(stmt) == SQLITE_ROW) {
            const unsigned char *modeTime = sqlite3_column_text(stmt, 0);
            NSString *timeStr = [NSString stringWithFormat:@"%s",modeTime];
            NSString* timeDateStr = [timeStr substringToIndex:10];
            [modeDateArr addObject:timeDateStr];
            
        }
        sqlite3_finalize(stmt);
    }
    NSLog(@"modeDateArr = %@",modeDateArr);
    
    return modeDateArr;
}




/**
 获得60*24长度的数组，mode ，用于active time 画柱状图
 {1,2,0,0}
 regard 7 as 0, use for graph
 */
+ (NSMutableArray *)activeModeArr: (NSString *)date :(sqlite3 *)database{
    NSMutableArray *modeStateArr = [[NSMutableArray alloc]init];
    NSMutableArray *modeTimeArr = [[NSMutableArray alloc]init];
    NSMutableArray *modeArr = [[NSMutableArray alloc]init];
    NSString *sql = [NSString stringWithFormat:@"select datetime(datetime(time,'%ld hour','+0 minute'))  as time1,mode from ped where time1 like '%@%%' order by time1 asc", (long)[[[self timeDateFormatter:DateFormatterTimeAllUnderLine] timeZone] secondsFromGMT]/3600,date];
    sqlite3_stmt *stmt;
    NSLog(@"select mode sql = %@",sql);
    
    if(sqlite3_prepare_v2(database, [sql UTF8String], -1, &stmt, NULL) == SQLITE_OK) {
        while(sqlite3_step(stmt) == SQLITE_ROW) {
            const unsigned char *modeTime = sqlite3_column_text(stmt, 0);
            int modeState = sqlite3_column_int(stmt, 1);
            NSString *timeStr = [NSString stringWithFormat:@"%s",modeTime];
            [modeTimeArr addObject:timeStr];
            [modeStateArr addObject:[NSNumber numberWithInt:modeState]];
            
        }
        sqlite3_finalize(stmt);
    }
    NSLog(@"mode sql result -----1");
    //    NSLog(@"modeTimeArr = %@",modeTimeArr);
    //    NSLog(@"modeStateArr = %@",modeStateArr);
    
    NSDate *zeroTime = [[CommonValues timeDateFormatter:DateFormatterTimeColon] dateFromString:[NSString stringWithFormat:@"%@ 00:00:00",date]];
    int index = 0;
    int activeMimutes = 0;
    NSString *modeState;
    int totalCount = 0;
    for (int i = 0 ; i < modeTimeArr.count; i++) {
        NSDate *tmpDate =  [[CommonValues timeDateFormatter:DateFormatterTimeColon] dateFromString:[modeTimeArr objectAtIndex:i]];
        NSTimeInterval myInterval = [tmpDate timeIntervalSinceDate:zeroTime];
        //        NSLog(@"myInterval = %f",myInterval);
        int interval = myInterval/60;
        if (i > 0) {
            modeState = [NSString stringWithFormat:@"%@",[modeStateArr objectAtIndex:i-1]];
        }
        
        
        
        for (int j = index ; j < interval; j++) {
            totalCount++;
            if (i == 0) {
                [modeArr addObject:@"0"];
            }
            else{
                if ([modeState isEqualToString:@"7"]) {
                    [modeArr addObject:@"0"];
                }
                else{
                    [modeArr addObject:modeState];
                    
                }
            }
            index++;
        }
        //        NSLog(@"index = %d",index);
        //        zeroTime =  [[CommonValues timeDateFormatter:DateFormatterTimeColon] dateFromString:[modeTimeArr objectAtIndex:i]];
    }
    
    NSLog(@"totalCount = %d",totalCount);
    
    NSLog(@"mode array -----2");
    
    NSInteger const arrSize = modeArr.count ;
    //    NSLog(@"arrSize = %d",arrSize);
    if (arrSize < 24*60) {
        //        NSString *modeStateStr =  [NSString stringWithFormat:@"%@",[modeArr objectAtIndex:arrSize-1]];
        for (NSInteger i = arrSize; i < 24*60; i++) {
            [modeArr addObject:@"0"];
        }
    }
    
    //    NSLog(@"modeArr = %@",modeArr);
    NSLog(@"mode new  array -----3");
    
    for (int i = 0 ; i< modeArr.count; i++) {
        NSString *modeStateStr =  [NSString stringWithFormat:@"%@",[modeArr objectAtIndex:i]];
        if ([modeStateStr isEqual:@"1"] ||[modeStateStr isEqual:@"2"] ) {
            activeMimutes++;
        }
        
    }
    NSLog(@"mode new define array -----4");
    
    activeTimeMinutes = activeMimutes;
    
    
    NSLog(@"activeTimeMinutes = %ld",(long)activeTimeMinutes);
    //    NSLog(@"modeArr count = %d",modeArr.count);
    return modeArr;
}



/**
 此方法需要先调用 +(NSMutableArray *)activeModeArr: (NSString *)date :(sqlite3 *)database时，
 给activeTimeMinutes 赋值
 */
+ (NSInteger ) getActiveTimeTotalMinutes{
    return activeTimeMinutes;
    
}






/**
 *auto sync 后台任务 开始结束
 */
+ (void) startBackgroundTask : (int)timeInterval{
    /*
     NSLog(@"startBackgroundTask--");
     autoSyncTimer = [NSTimer scheduledTimerWithTimeInterval:timeInterval
     target:[CommonValues getDashboardID]
     selector:@selector(doSyncStuff)
     userInfo:nil
     repeats:YES];
     //    backgroundTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
     //        [self endBackgroundTask];
     //    }];
     backgroundTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
     //        [self endBackgroundTask];
     NSLog(@"expirationHandler---");
     [self endBackgroundTask];
     [self startBackgroundTask:timeInterval];
     }];
     int remainingTime =  [[UIApplication sharedApplication] backgroundTimeRemaining];
     NSLog(@"remainingTime = %d",remainingTime);
     */
}

+ (void) endBackgroundTask{
    __LOG_METHOD_START;
    if (autoSyncTimer.valid) {
        [autoSyncTimer invalidate];
        autoSyncTimer = nil;
        
    }
    [[UIApplication sharedApplication] endBackgroundTask:backgroundTask];
    backgroundTask = UIBackgroundTaskInvalid;
    __LOG_METHOD_END;
}


+ (void)setSleepStartEndTimeBackFlag:(int) flag{
    sleepStartEndTimeBackFlag = flag;
}

+ (int)getSleepStartEndTimeBackFlag{
    return sleepStartEndTimeBackFlag;
}

+ (void)setSleepStartEndTimeEnterFlag:(int) flag{
    sleepStartEndTimeEnterFlag = flag;
}

+ (int)getSleepStartEndTimeEnterFlag{
    return sleepStartEndTimeEnterFlag;
}

+ (void)setSleepStartTimeStr:(NSString *)myStartTime{
    
    sleepStartTimeStr = myStartTime;
}
+ (void)setSleepEndTimeStr:(NSString *)myEndTime{
    
    sleepEndTimeStr = myEndTime;
}
+ (NSString *)getSleepEndTimeStr{
    
    return sleepEndTimeStr ;
}
+ (NSString *)getSleepStartTimeStr{
    
    return sleepStartTimeStr ;
}


/**
 需要分析的睡眠起止时间
 flag == 0 ,band 模式分析；flag == 1,set in app 模式分析
 */
+ (void)sleepAnalysisWithStartTime :(NSString*)startTime withEndTime:(NSString *)endTime withFlag:(int)flag withDataBase:(sqlite3 *)db {
    __LOG_METHOD_START;
    [[TGBleManager sharedTGBleManager] sleepInitAnalysis];

    NSString *realStartTime = @"";
    realStartTime = startTime;
    if (flag == 0) {
        NSLog(@"band Pattern Analysis");
    }
    else{
        //获得startTime之前的一个sleepPhase 加入分析
        NSLog(@"set in app Pattern Analysis");
        NSString *sql_before  = [NSString stringWithFormat:@"select datetime(time,'%ld hour','+0 minute') as time1 ,sleep_phase  from sleep  where time1 < '%@'  order by time1 desc limit 1  ;", (long)[[[CommonValues timeDateFormatter:DateFormatterDate] timeZone] secondsFromGMT]/3600,startTime];
        sqlite3_stmt *stmt;
        int count = 0;
        if(sqlite3_prepare_v2(db, [sql_before UTF8String], -1, &stmt, NULL) == SQLITE_OK) {
            while(sqlite3_step(stmt) == SQLITE_ROW) {
                //             NSLog(@"sql result  has records");
                const  unsigned  char *timeChar = sqlite3_column_text(stmt, 0);
                NSString *timeStr =[NSString stringWithFormat:@"%s",timeChar];
                NSDate *date = [[CommonValues timeDateFormatter:DateFormatterTimeColon] dateFromString:timeStr];
                int phase =  sqlite3_column_int(stmt, 1);
                [[TGBleManager sharedTGBleManager] sleepAddData:date sleepPhase:phase];
                NSLog(@"sql_before  date = %@,phase = %d",date,phase);
                //                realStartTime = timeStr;
                count++;
            }
            
            sqlite3_finalize(stmt);
        }
        //        else{
        if (count == 0) {
            
            NSLog(@"sql_before result  no records!!!");
            //处理第一次的情况，app 设定的睡眠开始时间之前没有sleep phase的记录，暂且
            realStartTime = startTime;
            //        }
        }
        
        
    }
    
    NSString *sql  = [NSString stringWithFormat:@"select datetime(time,'%ld hour','+0 minute') as time1, sleep_phase from sleep where time1 >= '%@' and time1 <= '%@' order by time1 asc;", (long)[[[CommonValues timeDateFormatter:DateFormatterDate] timeZone] secondsFromGMT]/3600,startTime,endTime];
    NSLog(@"sql = %@",sql);
    
    sqlite3_stmt *stmt2;
    
    if(sqlite3_prepare_v2(db, [sql UTF8String], -1, &stmt2, NULL) == SQLITE_OK) {
        int count = 0;
        while(sqlite3_step(stmt2) == SQLITE_ROW) {
            //             NSLog(@"sql result  has records");
            const  unsigned  char *timeChar = sqlite3_column_text(stmt2, 0);
            NSString *timeStr =[NSString stringWithFormat:@"%s",timeChar];
            NSDate *date = [[CommonValues timeDateFormatter:DateFormatterTimeColon] dateFromString:timeStr];
            int phase =  sqlite3_column_int(stmt2, 1);
            [[TGBleManager sharedTGBleManager] sleepAddData:date sleepPhase:phase];
            NSLog(@"sql date = %@,phase = %d",date,phase);
            count++;
            
        }
        
        NSLog(@"sql result  has %d records",count);
        sqlite3_finalize(stmt2);
    }
    else{
        NSLog(@"sql result  no records!!!");
    }
    NSLog(@"call sleepRequestAnalysis----");
    NSDate *sTimeDate = [[CommonValues timeDateFormatter:DateFormatterTimeColon] dateFromString:realStartTime];
    NSDate *sEndDate = [[CommonValues timeDateFormatter:DateFormatterTimeColon] dateFromString:endTime];
    NSLog(@"input to SDK sTimeDate = %@,sEndDate = %@",sTimeDate,sEndDate);
    [[TGBleManager sharedTGBleManager] sleepRequestAnalysis:sTimeDate endTime:sEndDate];
    [[TGBleManager sharedTGBleManager] sleepSetInterval:0 completeData:false];
    __LOG_METHOD_END;
}


+ ( void ) setModifyBackFlag: ( BOOL ) flag
{
    __LOG_METHOD_START;
    __LOG_METHOD_NOTE_STR_INT( "current value", modifyBackFlag );
    __LOG_METHOD_NOTE_STR_INT( "new value", flag );
    modifyBackFlag = flag;
    __LOG_METHOD_END;
    return;
}

+ ( BOOL ) modifyBackFlag
{
    __LOG_METHOD_START;
    __LOG_METHOD_NOTE_STR_INT( "current value", modifyBackFlag );
    __LOG_METHOD_END;
    return modifyBackFlag;
}


/**
 delay to show SVProgressHUD ,avoid focus problem
 */
+ (void)delay2ShowSVProgressHUD : (NSString *) statusStr{
    int usleepTime = 300000;
    usleep(usleepTime);
    [SVProgressHUD showWithStatus:statusStr];
}


/**
 get iOS version of device
 */
+ (double)iOSVersion{
    if ( iOSVersion < 1 ) {
        iOSVersion = [[UIDevice currentDevice].systemVersion doubleValue];
        return iOSVersion;
    }
    else{
        return iOSVersion;
    }
    
}



@end

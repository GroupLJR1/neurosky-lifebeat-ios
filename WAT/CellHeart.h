//
//  CellHeart.h
//  WAT
//
//  Created by NeuroSky on 11/11/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"
@interface CellHeart : UITableViewCell<CPTPlotDataSource,CPTAxisDelegate>{
    CPTXYGraph *graph;
    
    NSMutableArray *dataForPlot;
    
   // NSMutableArray  *dataArray;
    CPTGraphHostingView *hv;
    
    UIImage *screenshot;
    NSString *screenshotPath;
    NSString *ccc;
    
    CPTScatterPlot *dataSourceLinePlot3;
}
@property (strong, nonatomic)NSArray *HRArray;
@property (strong, nonatomic)UIImageView *bgImage;
@property (strong, nonatomic)UILabel *label1;
@property (strong, nonatomic)UILabel *label2;
@property (readwrite, strong, nonatomic) NSMutableArray *dataForPlot;
@property float fff;
@property (retain, nonatomic) NSString *ccc;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier fit:(float)fit1;
@end

//
//  SubCateViewController.m
//  top100
//
//  Created by Dai Cloud on 12-7-13.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "SubCateViewController.h"
#import "AppDelegate.h"
//#define COLUMN 4

@interface SubCateViewController ()

@end

@implementation SubCateViewController
@synthesize ekgView,fileURL,cateVC;


- (void)viewDidLoad
{
    [super viewDidLoad];
    rawIndex = 0;
    
    /* we are not using this for now.
     * comment it out for now, if you want to use it you need to
     * get the access to the user_profiule
     *

    [[TGAccessoryManager sharedTGAccessoryManager] setDelegate:self];
    
    [[TGAccessoryManager sharedTGAccessoryManager] initEkgAnalysis: [NSDate date]];

    [[TGAccessoryManager sharedTGAccessoryManager] setAge: user_profile.age];
    [[TGAccessoryManager sharedTGAccessoryManager] setGender: user_profile.isFemale]; // true = female
    [[TGAccessoryManager sharedTGAccessoryManager] setWristLocation: !user_profile.isBandOnRight]; // true = left
     */
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        array = [[NSArray alloc] initWithContentsOfFile:fileURL];
        rawArray = [[NSArray alloc] init];
        NSError* error;
        NSString* contents = [NSString stringWithContentsOfFile:fileURL encoding:NSUTF8StringEncoding error: & error];
        if (contents == nil) {
            NSLog(@"WAT Raw Data File is NULL!!!!!!!");
        }else{
            rawArray = [contents componentsSeparatedByString:@"\n"];
        }
//      rawArray = [[NSMutableArray alloc] init];
//      ParseAndSendWATData *p = [[ParseAndSendWATData alloc] init];
//      rawArray = [p rawArrayFromFile:fileURL];
    });
    if (addDataThread == nil) {
        addDataThread = [[NSThread alloc] initWithTarget:self selector:@selector(addDataToEKG) object:nil];
        [addDataThread start];
    }
}

-(void)addDataToEKG{
    while (1) {
//     NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
        [self getData];
     //   [pool drain];
        [NSThread sleepForTimeInterval:0.003];
    }
    
    
}
-(void)getData{
    if (rawIndex>=[rawArray count]) {
        
        return;
    }
    NSNumber *value = (NSNumber *)[rawArray objectAtIndex:rawIndex++];
    [ekgView addValue:[value intValue]];
    
    //[[TGAccessoryManager sharedTGAccessoryManager] requestEkgAnalysis_s:(short)[value intValue]];
}

-(void)dataReceived:(NSDictionary *)data{

    if([data valueForKey:@"CardioZoneHeartRate"])
    {
         cardioZoneHeartRate = [[data valueForKey:@"CardioZoneHeartRate"] intValue];
        
        NSLog(@"cardioZoneHeartRate  ======  %d",cardioZoneHeartRate);
        
    }
    
    if([data valueForKey:@"CardioZoneHeartRate_TS"])
    {
        cardioZoneHeartRate = [[data valueForKey:@"CardioZoneHeartRate_TS"] intValue];
        
    }

    if([data valueForKey:@"CardioZoneBreathingIndex"]) {
         cardioZoneBreathingIndex = [[data valueForKey:@"CardioZoneBreathingIndex"] intValue];
       // NSLog(@"cardioZoneBreathingIndex  ======  %f",cardioZoneBreathingIndex);
        
    }
    
    if([data valueForKey:@"CardioZoneBreathingIndex_TS"]) {
        cardioZoneBreathingIndex = [[data valueForKey:@"CardioZoneBreathingIndex_TS"] intValue];
        
    }
    
    if([data valueForKey:@"CardioZoneHRV"]) {
         cardioZoneHRV = [[data valueForKey:@"CardioZoneHRV"] intValue];
       // NSLog(@"cardioZoneBreathingIndex  ======  %d",cardioZoneHRV);
        
    }
    
    if([data valueForKey:@"CardioZoneHRV_TS"]) {
        cardioZoneHRV = [[data valueForKey:@"CardioZoneHRV_TS"] intValue];
        
    }
    
    if([data valueForKey:@"CardioZone3D"]) {
        cardioZone3D = [data valueForKey:@"CardioZone3D"];
        NSLog(@"CardioZone3D");
        
        //write cardioZone to csv file
        
        NSArray *documents = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *docementDir = [documents objectAtIndex:0];

        NSString *filePath1 = [docementDir stringByAppendingPathComponent:@"Sub10_Masa.csv"];
        [self writeContent2CSV:filePath1 withDate:@"Date" withHR:70 withHRV:70 withBI:20];
    }

}




-(void)viewWillAppear:(BOOL)animated{
    [ super viewWillAppear: animated ];
//    operationQueue = [[NSOperationQueue alloc] init];
//    [operationQueue setMaxConcurrentOperationCount:1];
//    // Do any additional setup after loading the view from its nib.
//
//    [ekgView setOpaque:NO];
//    //    timeCount=rawCount*2000;
//    
//    dataCount = 0;
//    
//    sendOperation = [[[SendEKGDataOperation alloc] initWithAction:self action:@selector(addPointToLineView:) fileURL:fileURL resumePoint:dataCount] autorelease];
//    sendOperation.getRawCount = @selector(setTimeCount:);
//    sendOperation.changeView=@selector(changeView:);
//    
//    
//    [operationQueue addOperation:sendOperation];
    
    
    
    
}
- (void)viewDidUnload {
    [self setEkgView:nil];
    [self setEkgView:nil];
    [super viewDidUnload];
}


- (void)writeContent2CSV:(NSString *)fileName  withDate : (NSString *)date   withHR: (int)hr
                withHRV : (float)hrv  withBI:(float) bi   {
    
    NSLog(@"writeContent2CSV-----");
    NSOutputStream *output = [[NSOutputStream alloc] initToFileAtPath:fileName append:YES];
    [output open];
    
    
    if (![output hasSpaceAvailable]) {
        NSLog(@"no enough space");
    } else
    {
        
        NSString *contents ;
        contents = [NSString stringWithFormat:@",0,%d,%f,%f,0,0,%%after jumping (noon),,%@\n",hr,hrv,bi,date];
        
        //        NSString *header = @",39,80.6,65.3,20.63,22,21.4,%after jumping (noon),,9/18 05:05pm\n";
        const uint8_t *headerString = (const uint8_t *)[contents cStringUsingEncoding:NSUTF8StringEncoding];
        NSInteger headerLength = [contents lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
        NSInteger result = [output write:headerString maxLength:headerLength];
        
        NSLog(@"result ===  %ld",(long)result);
        if (result <= 0) {
            NSLog(@"write in error");
        }
        
        [output close];
    }
}


- (void)writeContent2CSV:(NSString *)fileName {
    
    NSLog(@"writeContent2CSV-----");
    NSOutputStream *output = [[NSOutputStream alloc] initToFileAtPath:fileName append:YES];
    [output open];
    
    
    if (![output hasSpaceAvailable]) {
        NSLog(@"没有足够可用空间");
    } else
    {
        NSDate *date = [NSDate date];
        NSDateFormatter *dateForm = [[NSDateFormatter alloc]init];
        [dateForm setDateFormat:@"MM/dd hh:mma"];
        
        NSString *dateStr =  [dateForm stringFromDate:date];
        
        
        NSString *contents ;
        
        
        contents = [NSString stringWithFormat:@",0,%d,%d,%d,0,0,%%after jumping (noon),,%@\n",cardioZoneHeartRate,cardioZoneHRV,cardioZoneBreathingIndex,dateStr];
        
        //        NSString *header = @",39,80.6,65.3,20.63,22,21.4,%after jumping (noon),,9/18 05:05pm\n";
        const uint8_t *headerString = (const uint8_t *)[contents cStringUsingEncoding:NSUTF8StringEncoding];
        NSInteger headerLength = [contents lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
        NSInteger result = [output write:headerString maxLength:headerLength];
        
        NSLog(@"result ===  %ld",(long)result);
        if (result <= 0) {
            NSLog(@"写入错误");
        }
        
        
        [output close];
    }
}


@end

//
//  CommonValues.h
//  WAT
//
//  Created by neurosky on 5/29/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>


static float about_update_progress  = 0.0;

static float flag  = 0;  //  1  means alarm  set back; 2 means alarm cancel back; 3 means timer save back;4 means timer cancel back; 0 means dashboard to setting; 5 means autosync save back; 6 means autosync cancel back
static  float timeout_time = 20.0;

static  float screen_height;

static BOOL isECGAppMode = NO;    // if YES,ECG App Mode ;if NO ,Base App Mode

static BOOL shouldEraseData = YES;  //if yes,erase data when sync success;if no(for wenfeng) ,don't erase data when sync success

static NSString *selectedEKG;

static BOOL is_playback_mode  = NO;

static BOOL is_historyECG_playback_mode = NO;

// static sqlite3 *database ;
static NSString *userName ;

static NSString *documentPath ;

static NSString *sleepStartTime ;
static NSString *sleepEndTime ;
static BOOL jumpOutLoopFlag;
static int totalEndTime = 0;
static NSString *database_path;
static BOOL  stopRTNormalFlag ;

static BOOL  BluetoothOnFlag ;
static int consecutiveSyncCount;
static int beoforeFirstSleepPhase;
static NSString *beoforeFirstSleepTime;
static BOOL  hasTaskFlag ;
static BOOL  isInBackground ;
static BOOL  saveBtnClcikFlag;
static NSInteger activityIndex = 0;
static NSInteger activeTimeMinutes = 0;

static int viewControllerCount = 0;

static int testIndex = 0;
static NSTimer *autoSyncTimer;

static id dashboadId;
static BOOL autoSyncingFlag;
static int autoSyncRepeatTime = 60*1;  //second
static UIAlertView *autoSyncingAlert;
static sqlite3 *myDB;
static NSTimer *autoSyncTimer;
static UIBackgroundTaskIdentifier backgroundTask;
static int sleepStartEndTimeBackFlag = 0;   //0 cancel back; 1 save back
static int sleepStartEndTimeEnterFlag = 0;  // 0 startTime enter; 1 end time enter

static NSString *sleepStartTimeStr ;
static NSString *sleepEndTimeStr ;
static BOOL modifyBackFlag = NO;
static double iOSVersion = 0;


typedef enum : NSUInteger {
    DateFormatterTimeAllUnderLine = 0,
    DateFormatterTimeColon = 1,
    DateFormatterDate = 2,
    DateFormatterTimeAllUnderLineGMT = 3,
    DateFormatterTimeColonGMT = 4,
    DateFormatterDateGMT = 5,
    DateFormatterDateSlash = 6 ,
    DateFormatterDateSlashGMT = 7,
    DateFormatterTimeColonAMPM = 8,
    DateFormatterDateNoneSeparater = 9
} DateFormatter;

#pragma mark - peter for Hr median data
typedef  enum: NSUInteger
{
    MovingIntervalDay=1,
    MovingIntervalOneWeek=7,
    MovingIntervalTwoWeek=14
    
}MovingIntervalEnum;

typedef  enum: NSUInteger
{
    TotalPointMonth=28,
    TotalPointSixMonth=24,
    TotalPointOneYear=24
    
}TotalPointEnum;

typedef  enum: NSUInteger
{
    ChartLabelMonth=0,
    ChartLabelSixMonth=1,
    ChartLabelOneYear=2
    
}ChartLabelEnum;

typedef  enum: NSUInteger
{
    ChartLabelDisplayCountMonth=8,
    ChartLabelDisplayCountSixMonth=7,
    ChartLabelDisplayCountOneYear=7
    
}ChartLabelDisplayCountEnum;

typedef  enum: NSUInteger
{
    MedianIntervalMonth=5,
    MedianIntervalSixMonth=5,
    MedianIntervalOneYear=5
    
}MedianIntervalEnum;


typedef  enum: NSUInteger
{
    PeriodIntervalMonth=28,
    PeriodIntervalSixMonth=24*7,
    PeriodIntervalOneYear=48*7
    
}PeriodIntervalEnum;

//==============================

//enum {
//    TimeAllUnderLine = 0,
//    TimeColon = 1,
//    Date = 2,
//
//};
//typedef NSUInteger DateFormatter;

@interface CommonValues : NSObject


+ (int) repeatCode : (NSMutableArray*) repeatArry;
+ (BOOL) getShouldEraseData;
+ (NSArray *) bandDeviceArray;
+ (NSInteger)secondsFromString:(NSString *)myStr;
+ (void)setMyDB:(sqlite3 *)db;
+ (sqlite3 *)getMyDB;
+ (UIAlertView *)autoSyncingAlert;
+ (int)getAutoSyncRepeatTime;
+ (BOOL)getAutoSyncingFlag;
+ (void)setAutoSyncingFlag:(BOOL) myFlag;

+ (void)setTestIndex:(int) index;
+ (int)getTestIndex;

+ (id) getDashboardID;
+ (void)setDashboardID : (id) myID;


+ (NSTimer *) getAutoSyncTimer;
+ (void) setAutoSyncTimer: (NSTimer *) myTimer;

+ (void)setActivityIndex: (NSInteger)index;
+ (NSInteger) getActivityIndex;
+ (BOOL)getSaveBtnClcikFlag;
+ (void)setSaveBtnClcikFlag: (BOOL)flag;

+ (UILabel *)labelWithFrame:(CGRect)frame WithBoldFont:(UIFont*)font strokeWidth:(float)width withAttributeText:(NSString*)string;
+ (int)getSleepPhaseBeoforeFirst;
+ (NSString*)getSleepTimeBeoforeFirst;
+ (BOOL)isBackgroundFlag;
+ (void)setBackgroundFlag: (BOOL)flag;

+ (BOOL)getHasTaskFlag;
+ (void)setHasTaskFlag: (BOOL)flag;
+ (void)insertCloudPedData :(sqlite3 *)db;
+ (NSString *) getDeviceID;

+ (BOOL) connectedToNetwork;
+ (int)getConsecutiveSyncCount;
+ (void)addConsecutiveSyncCount;
+ (void)resetConsecutiveSyncCount;

+ (BOOL)getStopRTNormalFlag;
+ (void)setStopRTNormalFlag: (BOOL)flag;

+ (BOOL)getBluetoothOnFlag;
+ (void)setBluetoothOnFlag: (BOOL)flag;

+ (int)getSleepTotalMinutes;
+ (void)setAboutUpdateProgress:(float)percent;
+ (float)getAboutUpdateProgress;

+ (void)setFlag:(int) falg;

+ (int)getFlag;

+ (float)getTimeOutTime;
+ (BOOL)isHistoryECGPlayBackMode;
+ (void)setHistoryECGPlayBackMode:(BOOL)isHistoryECGPlayBackMode;

+ (void)setScreenHeight: (float) height;
+ (float)getScreenHeight;

+ (BOOL)getIsECGAppMode;
+ (BOOL)isPlayBackMode;
+ (void)setPlayBackMode:(BOOL)isplayback;
+ (void)setSelectedEKG:( NSString *)name;
+ (NSString *) getSelectedEKG;

+ (void)setSleepStartTime:(NSString *)startTime;
+ (void)setSleepEndTime:(NSString *)endTime;
+ (NSString *)getSleepStartTime;
+ (NSString *)getSleepEndTime;

+ (void)insertHeartData2DB : (NSDate *)date
                   finalHR : (int) finalHR
                     avgHR : (int)avgHR
                    avgHRV : (int) avgHRV
                  finalHRV : (int) finalHRV
                    stress : (int) stress
                  database : (sqlite3 *) database
;

+ (void)openDataBase:(sqlite3 *)database;
+ (void)creatOrOpenDB:(sqlite3 *)database;
+ (void)closeDB:(sqlite3 *)database;

+ (int)getDayECGCount:(NSString *)date :(sqlite3 *)database;
+ (int) findMaxNumInArr:(NSMutableArray *) mulArr;
+ (NSMutableArray *)getSleepTimeArr:(sqlite3 *)database;
+ (NSMutableArray *)getPedTimeArr :(sqlite3 *)database;



+ (void)setUserName :(NSString*) name;
+ (NSString *) getUserName;
+ (NSString *)getDocumentPath;
+ (void)calculateSleepTime : (NSString*)sleep_date :(sqlite3 *)database;
+ (void)execSql:(NSString *)sql :(sqlite3 *)database;

+ (NSMutableArray *)getECGTimeArr :(sqlite3 *)database :(BOOL)isHistoryECG;
//+(sqlite3 *)getDB;
+ (NSMutableArray *)getLatestWeekHROrHRVArr :(sqlite3 *)database :(BOOL)isHr;

+ (NSString *)getDbPath;

+ (void)showAlert :(NSString* )msg;
+ (NSMutableArray *)getHistoryHROrHRVArr :(sqlite3 *)database :(BOOL)isHr;
+ (void)insertCloudSleepData :(sqlite3 *)db;
+ (NSMutableArray *)getHistoryHROrHRVArr2 :(sqlite3 *)database :(BOOL)isHr;

+ (NSMutableArray *) getOneWeekDateArr : (NSString *) dateStr;

+ (NSMutableArray *) getOneWeekDateIndexNumArr :(NSMutableArray *) inputArr  : (NSString *) dateStr;
+ (NSString *) getHeartMinDateStr  : (sqlite3 *) database ;
+ (NSMutableArray *) getOneWeekHRDetail : (sqlite3 *) database : (NSString *) lastDateInOneWeek :(BOOL) isHR;
+ (void)updateConnectStateImg :(UIImageView * )stateImg withLevel:(int)level;
+ (NSString *) distanceStrFromDistanceCount: (NSInteger) currentDistanceCount;
+ (int) offsetHeight4DiffiOS;
+ (NSDateFormatter *) timeDateFormatter :(DateFormatter) mydf;

+ (void)setViewControllerCount : (int)count;
+ (int) getViewControllerCount ;

+ (NSMutableArray *) getDateArr : (NSString *) dateStr : (NSInteger)dayCount;

+ (NSString *)getiPhoneDeviceName;
+ (int)startSleepAnalysis :(NSInteger)sleepIndex  :(sqlite3 *)db;
+ (NSMutableArray *)activeModeArr: (NSString *)date :(sqlite3 *)database;
+ (NSMutableArray *)activeModeDateArr :(sqlite3 *)database;
+ (NSInteger ) getActiveTimeTotalMinutes;
//+ (void) startBackgroundTask : (int)timeInterval;
//+ (void) endBackgroundTask;
+ (NSMutableArray *) calculateSleepStartEndTimeArr :(NSString *) endDateStr :(sqlite3 *)database;
+ (void)setSleepStartEndTimeBackFlag:(int) flag;
+ (int)getSleepStartEndTimeBackFlag;
+ (void)setSleepStartEndTimeEnterFlag:(int) flag;
+ (int)getSleepStartEndTimeEnterFlag;
+ (void)sleepAnalysisWithStartTime :(NSString*)startTime withEndTime:(NSString *)endTime withFlag:(int)flag withDataBase:(sqlite3 *)db;
+ (void)setModifyBackFlag:(BOOL)flag;
+ (BOOL)modifyBackFlag;
+ (void)delay2ShowSVProgressHUD : (NSString *) statusStr;
+ (double)iOSVersion;

@end

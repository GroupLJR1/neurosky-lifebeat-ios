//
//  CellHeart.m
//  WAT
//
//  Created by NeuroSky on 11/11/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import "CellHeart.h"
#import "Constant.h"

@implementation CellHeart
@synthesize HRArray,bgImage;
@synthesize label1,label2;
@synthesize dataForPlot;
@synthesize fff;
@synthesize ccc;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier fit:(float)fit1
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // HRArray = [[NSArray alloc]init];
        
        // Initialization code
        NSLog(@"cccc===%@",ccc);
        bgImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 214)];
        [self addSubview:bgImage];
        label1=[[UILabel alloc]initWithFrame:CGRectMake(60, 0, 100, 30)];
        label1.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:20.0];
        [self addSubview:label1];
        label2=[[UILabel alloc]initWithFrame:CGRectMake(300, 0, 100, 30)];
        label2.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:20.0];
        [self addSubview:label2];
        
        dataSourceLinePlot3 = [[CPTScatterPlot alloc] init] ;
        
        //  HRArray = [NSArray arrayWithObjects:@"2",@"4",@"5",@"8",@"6", @"8",@"9", nil];
        NSLog(@"hrarray=====%@",HRArray);
        //    NSString *backString = NSLocalizedString(@"back", @"");
        
        // Create graph from theme
        graph = [[CPTXYGraph alloc] initWithFrame:CGRectZero];
        CPTTheme *theme = [CPTTheme themeNamed:kCPTPlainWhiteTheme];
        [graph applyTheme:theme];
        CPTGraphHostingView *hostingView = [[CPTGraphHostingView alloc]initWithFrame:CGRectMake(0, 30, 320, 160)];
        [self addSubview:hostingView];
        graph.plotAreaFrame.borderLineStyle = nil;
        graph.plotAreaFrame.cornerRadius = 0.0f;
        //   hostingView.collapsesLayers = NO; // Setting to YES reduces GPU memory usage, but can slow drawing/scrolling
        hostingView.hostedGraph     = graph;
        
        graph.paddingLeft   = 0.0;
        graph.paddingTop    = 0.0;
        graph.paddingRight  = 0.0;
        graph.paddingBottom = 0.0;
        
        // Create a plot that uses the data source method
        
        NSLog(@"xxxxx");
        // Setup plot space
        CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
        plotSpace.allowsUserInteraction = YES;
        plotSpace.xRange                = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(-0.2) length:CPTDecimalFromFloat(6.5)];
        plotSpace.globalXRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(-2)length:CPTDecimalFromFloat(100)];
        plotSpace.yRange                = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(10) length:CPTDecimalFromFloat(40)];
        plotSpace.globalYRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(10)length:CPTDecimalFromFloat(40)];
        
        // Axes
        CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph.axisSet;
        CPTXYAxis *x          = axisSet.xAxis;
        
        x.minorTickLineStyle = nil;
        x.axisConstraints = [CPTConstraints constraintWithRelativeOffset:0.5];
        x.labelingPolicy = CPTAxisLabelingPolicyNone;
        // x.axisConstraints = [CPTConstraints constraintWithRelativeOffset:0.2];
        
        CPTXYAxis *y = axisSet.yAxis;
        y.labelingPolicy = CPTAxisLabelingPolicyNone;
        y.axisLineStyle=CPTAxisLabelingPolicyNone;
        y.axisConstraints = [CPTConstraints constraintWithRelativeOffset:0.5];
        
        NSLog(@"33333");
        ////绿色五角星
        dataSourceLinePlot3 = [[CPTScatterPlot alloc] init] ;
        CPTMutableLineStyle*lineStyle3 = [CPTMutableLineStyle lineStyle];
        //    lineStyle.miterLimit        = 1.0f;
        NSLog(@"sssss%f",fff);
        lineStyle3.lineWidth         = fit1;
        lineStyle3.lineColor         = [CPTColor blueColor];
        dataSourceLinePlot3.dataLineStyle = lineStyle3;
        
        dataSourceLinePlot3.identifier    = @"Green Plot";
        dataSourceLinePlot3.dataSource    = self;
        
        // dataSourceLinePlot3.delegate  =self;
        
        [graph addPlot:dataSourceLinePlot3];
        
        // Put an area gradient under the plot above
        
        CPTMutableLineStyle *symbolLineStyle3 = [CPTMutableLineStyle lineStyle];
        
        symbolLineStyle3.lineColor =[CPTColor blueColor];
        
        
        CPTPlotSymbol *plotSymbol3 = [CPTPlotSymbol ellipsePlotSymbol];
        plotSymbol3.fill          = [CPTFill fillWithColor:[CPTColor blueColor]];
        plotSymbol3.lineStyle = symbolLineStyle3;
        
        // plotSymbol3.lineStyle     = symbolLineStyle3;
        plotSymbol3.size          = CGSizeMake(5.0, 5.0);
        dataSourceLinePlot3.plotSymbol = plotSymbol3;
    }
    return self;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
       
        }
    return self;
}
-(void)changePlotRange
{
    
    NSLog(@"== changePlotRange ==");
    // Setup plot space
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.0) length:CPTDecimalFromFloat(3.0 + 2.0 * rand() / RAND_MAX)];
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.0) length:CPTDecimalFromFloat(3.0 + 2.0 * rand() / RAND_MAX)];
}

#pragma mark -
#pragma mark Plot Data Source Methods
-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot
{
    return [dataForPlot count];
}




-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index

{
    
    
    NSString *key = (fieldEnum == CPTScatterPlotFieldX ? @"x" : @"y");
    NSNumber *num = [[dataForPlot objectAtIndex:index] valueForKey:key];
    
    // Green plot gets shifted above the blue
    if ( [(NSString *)plot.identifier isEqualToString:@"Green Plot"] ) {
        if ( fieldEnum == CPTScatterPlotFieldY ) {
            //            num = [NSNumber numberWithDouble:[num doubleValue] + 1.0];
            // num = [NSNumber numberWithFloat:55+arc4random() % (10+1)];
            
            
        }
    }
    
    
    else if([(NSString *)plot.identifier isEqualToString:@"Red Plot"]){
        if ( fieldEnum == CPTScatterPlotFieldY ) {
            //   num = [NSNumber numberWithFloat:55+arc4random() % (10+1)];
        }
        
        
    }
    return num;
}

#pragma mark -
#pragma mark Axis Delegate Methods

-(BOOL)axis:(CPTAxis *)axis shouldUpdateAxisLabelsAtLocations:(NSSet *)locations
{
    
    
    static CPTTextStyle *positiveStyle = nil;
    static CPTTextStyle *negativeStyle = nil;
    
    //NSNumberFormatter *formatter = axis.labelFormatter;
    CGFloat labelOffset          = axis.labelOffset;
    NSDecimalNumber *zero        = [NSDecimalNumber zero];
    
    NSMutableSet *newLabels = [NSMutableSet set];
    
    for ( NSDecimalNumber *tickLocation in locations ) {
        CPTTextStyle *theLabelTextStyle;
        
        if ( [tickLocation isGreaterThanOrEqualTo:zero] ) {
            if ( !positiveStyle ) {
                CPTMutableTextStyle *newStyle = [axis.labelTextStyle mutableCopy];
                newStyle.color = [CPTColor blackColor];
                positiveStyle  = newStyle;
            }
            theLabelTextStyle = positiveStyle;
        }
        else {
            if ( !negativeStyle ) {
                CPTMutableTextStyle *newStyle = [axis.labelTextStyle mutableCopy];
                newStyle.color = [CPTColor whiteColor];
                negativeStyle  = newStyle;
            }
            theLabelTextStyle = negativeStyle;
        }
        
        NSString *labelString       = [NSString stringWithFormat:@"%ld", (long)[tickLocation integerValue]];
      //  NSLog(@"labelString = %@",labelString);
        CPTTextLayer *newLabelLayer = [[CPTTextLayer alloc] initWithText:labelString style:theLabelTextStyle];
        
        CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithContentLayer:newLabelLayer];
        newLabel.tickLocation = tickLocation.decimalValue;
        newLabel.offset       = labelOffset;
        
        [newLabels addObject:newLabel];
    }
    axis.axisLabels = newLabels;
    return NO;
}

@end

//
//  SendEKGDataOperation.h
//  Hearty
//
//  Created by NeuroSky on 8/29/12.
//  Copyright (c) 2012 NeuroSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SendEKGDataOperation : NSOperation{

    id target;
    SEL action;
    NSString *fileURL;
    
    SEL getRawCount;
    BOOL isFinishThread;
    
    NSMutableArray *data;
    NSMutableArray *item;
    int i;
    NSString *str;
    
    SEL changeView;
}

@property BOOL isPlayClicked;
@property SEL getRawCount;
@property BOOL isFinishThread;
@property SEL changeView;
-(id)initWithAction:(id)theTarget action:(SEL)theAction fileURL:(NSString *)theURL resumePoint:(int)theResumePoint;

//-(id)initWithAction:(id)theTarget action:(SEL)theAction fileURL:(NSString *)theURL resumePoint:(int)theResumePoint;



@end

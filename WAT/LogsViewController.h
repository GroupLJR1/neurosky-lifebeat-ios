//
//  LogsViewController.h
//  WAT
//
//  Created by neurosky on 5/28/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSYPopoverListView.h"
#import "TGBleManager.h"
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MessageUI.h>
#import "TGBleManager_mfg.h"

@interface LogsViewController : UIViewController<TGBleManagerDelegate,ZSYPopoverListDatasource,ZSYPopoverListDelegate,MFMailComposeViewControllerDelegate>{

    __weak IBOutlet UILabel *logs_title_label;

    __weak IBOutlet UITextView *logs_tv;
//    NSMutableString *longStr;
    ZSYPopoverListView *listView ;
    NSMutableArray *devicesArray;
    NSMutableArray *mfgDataArray;
    NSMutableArray *rssiArray;
    NSMutableArray *candIDArray;
    NSMutableArray *devicesIDArray;
    BOOL  isErasing;
    BOOL  isSettingGoal;
    BOOL isSettingAlarm;
    BOOL isSettingTimer;
    NSString *readyToSaveDeviceName;
    NSString *readyToSaveDeviceID;
    NSString *latestLogPath;
    NSString *latestLogName;
    NSString *content;
     NSTimer *timeOutTimer;
   BOOL isResetingBand;
    BOOL firstEnterCandiDel;
    NSTimer *showListTimer;
    BOOL haveDeviceFlag;
    BOOL wantSetVibrationOn;
     BOOL wantSetVibrationOff;
    BOOL touchDownFlag;
    __weak IBOutlet UIView *myWholeView;
    UIAlertController *alertController;
}

@property (nonatomic, retain) NSIndexPath *selectedIndexPath;
- (IBAction)backBtnClick:(id)sender;
- (IBAction)eraseBtnClick:(id)sender;
- (IBAction)releaseBtnClick:(id)sender;

- (IBAction)resetBandClick:(id)sender;
- (IBAction)vibrationOffClick:(id)sender;
- (IBAction)vibrationOnClick:(id)sender;
- (IBAction)touchClick:(id)sender;
- (IBAction)dragExitClick:(id)sender;
- (IBAction)dragEnterClick:(id)sender;
- (IBAction)touchDragInside:(id)sender;
- (IBAction)touchDragOutside:(id)sender;
- (IBAction)touchDown:(id)sender;
- (IBAction)touchCancel:(id)sender;
- (IBAction)editingBegin:(id)sender;
- (IBAction)editingEnd:(id)sender;

- (IBAction)sendEmailClick:(id)sender;
@end

//
//  AppDelegate.h
//  WAT
//
//  Created by Julia on 4/16/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>{

    NSMutableArray *arr1;
    NSMutableArray *arr2;
    NSMutableArray *arr3;
    NSString *animationOverFlag;
    NSString *safeDataFlag;
    NSString  *synFinishFlag;
    NSString  *enterFlag ;
    NSArray *DemoUsers;
}

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) NSString *animationOverFlag;
@property (nonatomic, strong) NSString *safeDataFlag;

@property (nonatomic, strong) NSString *synFinishFlag;
@property  int batteryLevel;
@property  BOOL batterWarningFlag;


@end

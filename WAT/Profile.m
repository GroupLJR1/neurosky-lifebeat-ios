//
//  ProfileModel.m
//  WAT
//
//  Created by NeuroSky on 1/27/15.
//  Copyright (c) 2015 Julia. All rights reserved.
//

#import "Profile.h"
#import "TGBleManager.h"
#import "Constant.h"

#import "TGBleEmulator.h"

@implementation Profile


- (void)setBackPreviousInfo2SDK{
    NSLog(@"setBackPreviousInfo2SDK---");
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL storedAutoWalkingFlag =  [defaults boolForKey:IS_AUTO_WALKING];
    BOOL storedAutoRunningFlag =  [defaults boolForKey:IS_AUTO_RUNNING];
    NSString *storedWalkingLength = [defaults objectForKey:USER_WALKING_STEP_LENGH];
    NSString *storedRunningLength = [defaults objectForKey:USER_RUNNING_STEP_LENGH];
    int wl = [storedWalkingLength intValue];
    int rl = [storedRunningLength intValue];
    if (storedAutoWalkingFlag || storedAutoRunningFlag) {
        [[TGBleManager sharedTGBleManager] setWalkingStepLength:0];
        [[TGBleManager sharedTGBleManager] setRunningStepLength:0];
        
    }
    else{
        [[TGBleManager sharedTGBleManager] setWalkingStepLength:wl];
        [[TGBleManager sharedTGBleManager] setRunningStepLength:rl];
        
    }
    
    BOOL tmp_is_band_right = [defaults boolForKey:USER_IS_BAND_RIGHT];
    BOOL tmp_is24hour = [defaults boolForKey:DISPLAY_24HR_Time];
    BOOL tmp_isImperialUnits = [defaults boolForKey:DISPLAY_IMPERIAL_UNITS];
    BOOL tmp_is_female = [defaults boolForKey:USER_IS_FEMALE];
    NSString *tmp_Birth = [defaults objectForKey:USER_BIRTH_DATE];
    NSString *tmp_Height = [defaults objectForKey:USER_HEIGHT];
    NSString *tmp_Weight = [defaults objectForKey:USER_WEIGHT];
    
    int year = 0;
    int  month = 0 ;
    int day = 0;
    NSArray *array_birth = [tmp_Birth componentsSeparatedByString:@"/"];
    if (array_birth.count == 3) {
        year  =  [ array_birth[0] intValue];
        month =  [ array_birth[1] intValue];
        day =  [ array_birth[2] intValue];
        //  NSLog(@"year = %d,month = %d, day =%d",year,month,day);
    }
    
    [[TGBleManager sharedTGBleManager] setBirthYear:year];
    [[TGBleManager sharedTGBleManager] setBirthMonth:month];
    [[TGBleManager sharedTGBleManager] setBirthDay:day];
    [[TGBleManager sharedTGBleManager] setBandOnRight:tmp_is_band_right];
    [[TGBleManager sharedTGBleManager] setDisplayTime24Hour:tmp_is24hour];
    [[TGBleManager sharedTGBleManager] setDisplayImperialUnits:tmp_isImperialUnits];
    [[TGBleManager sharedTGBleManager] setFemale:tmp_is_female];
    [[TGBleManager sharedTGBleManager] setHeight:[tmp_Height integerValue]];
    [[TGBleManager sharedTGBleManager] setWeight:(int)([tmp_Weight doubleValue]*10)];
    
    
    
}

- (void)writeToTemp{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString*   birth  = [defaults objectForKey:USER_AGE];
    int year = 0;
    int  month = 0 ;
    int day = 0;
    NSArray *array_birth = [birth componentsSeparatedByString:@"/"];
    if (array_birth.count == 3) {
        year  =  [ array_birth[0] intValue];
        month =  [ array_birth[1] intValue];
        day =  [ array_birth[2] intValue];
        //  NSLog(@"year = %d,month = %d, day =%d",year,month,day);
    }
    
    [[TGBleManager sharedTGBleManager] setBirthYear:year];
    [[TGBleManager sharedTGBleManager] setBirthMonth:month];
    [[TGBleManager sharedTGBleManager] setBirthDay:day];
    NSLog(@"age from SDK = %ld",(long)[[TGBleManager sharedTGBleManager]age]);
    NSLog(@"USER_IS_FEMALE = %@,USER_AGE = %@,USER_WEIGHT = %@, USER_HEIGHT = %@,USER_WALKING_STEP_LENGH = %@, USER_RUNNING_STEP_LENGH = %@", [defaults objectForKey:USER_IS_FEMALE],[defaults objectForKey:USER_AGE], [defaults objectForKey:USER_WEIGHT],[defaults objectForKey:USER_HEIGHT],[defaults objectForKey:USER_WALKING_STEP_LENGH],[defaults objectForKey:USER_RUNNING_STEP_LENGH]);
    
    NSString *temp = [NSString stringWithFormat:@"gender  %@\nage  %ld\nheight  %@\nweight  %@\nwalk_step_size  %@\nrun_step_size  %@",[defaults objectForKey:USER_IS_FEMALE],(long)[[TGBleManager sharedTGBleManager]age], [defaults objectForKey:USER_HEIGHT],[NSString stringWithFormat:@"%d",(int)[[defaults objectForKey:USER_WEIGHT] doubleValue]*10],[defaults objectForKey:USER_WALKING_STEP_LENGH],[defaults objectForKey:USER_RUNNING_STEP_LENGH]];
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];//去处需要的路径
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"userPro.txt"];
    [fileManager removeItemAtPath:path error:nil];
    NSMutableData *writer = [[NSMutableData alloc] init];
    [writer appendData:[temp dataUsingEncoding:NSUTF8StringEncoding]];
    [writer writeToFile:path atomically:YES];
}



@end

//
//  WATProtocols.h
//  WAT
//
//  Created by Julia on 4/23/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FacebookSDK/FacebookSDK.h>

@protocol WATProtocols <NSObject>

@end
// BOGGraphTruthValue protocol (graph accessors)
//
// Summary:
// Used to create and consume TruthValue open graph objects
@protocol BOGGraphTruthValue<FBGraphObject>

@property (retain, nonatomic) NSString                  *id;
@property (retain, nonatomic) NSString                  *url;
@property (retain, nonatomic) NSString                  *title;

@end

// BOGBooleanGraphAction protocol (graph accessors)
//
// Summary:
// Used to create and consume Or and And open graph actions
@protocol BOGGraphBooleanAction<FBOpenGraphAction>

@property (retain, nonatomic) NSString                  *result;
@property (retain, nonatomic) id<BOGGraphTruthValue>    truthvalue;
@property (retain, nonatomic) id<BOGGraphTruthValue>    anothertruthvalue;

@end

// BOGGraphPublishedBooleanAction protocol (graph accessors)
//
// Summary:
// Used to consume published Or and And open graph actions
@protocol BOGGraphPublishedBooleanAction<FBOpenGraphAction>

@property (retain, nonatomic) id<BOGGraphBooleanAction> data;
@property (retain, nonatomic) NSNumber                  *publish_time;
@property (retain, nonatomic) NSDate                    *publish_date;
@property (retain, nonatomic) NSString                  *verb;

@end

// BOGGraphBooleanActionList protocol (graph accessors)
//
// Summary:
// Used to consume published collections of Or and And open graph actions
@protocol BOGGraphBooleanActionList<FBOpenGraphAction>

@property (retain, nonatomic) NSArray                   *data;

@end

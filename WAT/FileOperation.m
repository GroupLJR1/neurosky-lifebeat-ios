//
//  FileOperation.m
//  WAT
//
//  Created by NeuroSky on 1/27/15.
//  Copyright (c) 2015 Julia. All rights reserved.
//

#import "FileOperation.h"
#import "CommonValues.h"

@implementation FileOperation


+ (void)deleteFile :(NSString *)fileName{
    //delete File under Documents  with  file name
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentDir = [[CommonValues getDocumentPath] stringByAppendingPathComponent:fileName];
    [fileManager removeItemAtPath:documentDir error:nil];
   
}

/**
 获得Documents/下某个文件夹的所有文件列表
 */
+ (NSArray *)getFileArr:(NSString *)folderName{
    // get the files list under Documnets Folder
    NSFileManager *fileManager1 = [NSFileManager defaultManager];
    NSString *documentDir3 = [[CommonValues getDocumentPath] stringByAppendingPathComponent:folderName];
    NSLog(@"path = %@",documentDir3);
    NSError *error = nil;
    NSArray * const fileList = /* [[NSArray alloc] init];
    fileList = */ [fileManager1 contentsOfDirectoryAtPath:documentDir3 error:&error];
    NSLog(@"Every Thing in the dir:%@",fileList);
    return fileList;
    
}



/**
 删除Documents/下的文件夹
 */
+ (void)deleteFolder :(NSString *)folderName{
    //delete the files in the folder which is under Documents/
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentDir = [[CommonValues getDocumentPath] stringByAppendingPathComponent:folderName];
    
    NSArray *arr =  [self getFileArr:folderName];
    NSLog(@"arr = %@",arr);
    for (int i = 0; i<arr.count; i++) {
        NSLog(@"PEDData  documentDir2 path 111= %@",[documentDir stringByAppendingPathComponent:arr[i]]);
        [fileManager removeItemAtPath:[documentDir stringByAppendingPathComponent:arr[i]] error:nil];
        
    }
}



+ (void)saveEcg2DocumentWithFilePath:(NSString *)filePath withFileContent:(NSString *)fileContent{
    //save  string file content to file path
    NSLog(@"ekg2Path = %@",filePath);
    NSString *tmpPath = [filePath stringByDeletingLastPathComponent];
    //if folder is not exsit,creat folder first.
    BOOL bo = [[NSFileManager defaultManager] createDirectoryAtPath:tmpPath withIntermediateDirectories:YES attributes:nil error:nil];
    NSAssert(bo,@"创建目录失败");
    //创建数据缓冲  create data buffer
    NSMutableData *writer = [[NSMutableData alloc] init];
    [writer appendData:[fileContent dataUsingEncoding:NSUTF8StringEncoding]];
    [writer writeToFile:filePath atomically:YES];
     NSLog(@"save file finished");
    
}

+ (void)save2UserDefaultsWithObj:(id)value withKey:(NSString *)key{
    //save object to userDefault
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:value forKey:key];
    [defaults synchronize];
}


+ (void)save2UserDefaultsWithInteger:(NSInteger)value withKey:(NSString *)key{
     //save NSInteger to userDefault
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:value forKey:key];
    [defaults synchronize];
}


+ (void)write2FileAppendLastTimeContent:(NSString *)content filePath:(NSString *)path{
    //write the data to file,if file exists,append the data continue to write it.
    NSFileHandle* fileHandle;
    NSFileManager *myFM = [[NSFileManager alloc] init];
    NSString *tmpPath = path;
    if (![myFM fileExistsAtPath:path]) {
        tmpPath = [path stringByDeletingLastPathComponent];
        //if folder is not exsit,creat folder first.
        BOOL bo = [[NSFileManager defaultManager] createDirectoryAtPath:tmpPath withIntermediateDirectories:YES attributes:nil error:nil];
        NSAssert(bo,@"创建目录失败");
        [myFM createFileAtPath:path contents:nil attributes:nil];
        NSLog(@"File %@ created!", path);
    }
    fileHandle = [NSFileHandle fileHandleForWritingAtPath:path];
    [fileHandle seekToEndOfFile];
    [fileHandle writeData:[content dataUsingEncoding:NSUTF8StringEncoding]];
    [fileHandle closeFile];
}

@end

//
//  StringOperatiom.m
//  WAT
//
//  Created by NeuroSky on 1/27/15.
//  Copyright (c) 2015 Julia. All rights reserved.
//

#import "StringOperation.h"

#import "TGBleEmulator.h"
#import "TGLibEmulator.h"

@implementation StringOperation

+(NSString *)path2TimeStr: (NSString *)txtfile{
    //sub string the path, and get the time string.
    
    
    //  eg:      NSString *txtfile = @"/var/mobile/Applications/7DEA77C8-2267-4F7B-8199-4694D2F89103/Documents/TG_log/Console/2014_04_24_02_40_11.txt";
    
    NSString *tempTimeWith = [txtfile substringFromIndex:(txtfile.length-23)];
    
    //    NSLog(@"tempTimeWith = %@",tempTimeWith);
    NSString *tempTime =  [tempTimeWith substringToIndex:(tempTimeWith.length - 4)];
    //    NSLog(@"tempTime = %@",tempTime);
    
    NSDateFormatter *dfx = [[NSDateFormatter alloc]init];
    [dfx setDateFormat:@"yyyy_MM_dd_HH_mm_ss"];
    NSDate *date =  [dfx dateFromString:tempTime];
    [dfx setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"];
    
    NSString *formattedTime = [NSString stringWithFormat:@"%@-%@",[self getLast14SN], [dfx stringFromDate:date]];
    NSLog(@"formattedTime = %@",formattedTime);
    return formattedTime;
    
}



+(NSString *)getLast14SN{
    //get the last letters of hwSerialNumber
    NSString *sn = [[TGBleManager sharedTGBleManager] hwSerialNumber];
    NSLog(@" SN = %@",sn);
    NSString * sub_sn;
    if ([sn length] >= 14) {
        sub_sn = [sn substringFromIndex:[sn length]-14];
        NSLog(@"last 14 SN = %@",sub_sn);
    }
    
    return sub_sn;
    
}


/**
 返回 各种version number拼接后的String
 return long string combined with multi- version numbers.
 */
+(NSString *) getVersionNumbers{
    // appVersion-bleVersion-algoVersion-FWVersion-HWVersion
    
    NSString *versions;
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString  *FW_Version = [defaults stringForKey:@"FW_Version"];
    NSString  *HW_Version = [defaults stringForKey:@"HW_Version"];
    NSString *App_version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    
    TGLibEKG *myTGlib = [[TGLibEKG alloc]init];
    int algoVersion = [myTGlib getVersion];
    int bleVersion = [[TGBleManager sharedTGBleManager]getVersion];
    versions = [NSString stringWithFormat:@"%@-%d-%d-%@-%@",App_version,bleVersion,algoVersion,FW_Version,HW_Version];
    
    NSLog(@"versions = %@",versions);
    return  versions;
    
    
}


/**
 int转String  每K加，
 eg: 3，000
 */
+(NSString *)intToString:(NSInteger)value{
    if (value<0) {
        return nil;
    }
    NSString *s = [NSString stringWithFormat:@"%ld",(long)value];
    //    NSLog(@"%d",s.length);
    NSMutableArray *a = [[NSMutableArray alloc] init];
    NSString *s1;
    NSInteger length = s.length;
    while (length>3) {
        s1 =[s substringFromIndex:s.length-3];
        s = [s substringWithRange:NSMakeRange(0,length-3)];
        //        NSLog(@"s----%@",s);
        length = s.length;
        [a addObject:s1];
    }
    [a addObject:s];
    NSArray *reversedArray = [[a reverseObjectEnumerator] allObjects];
    NSString *string = [reversedArray componentsJoinedByString:@","];
    //    NSLog(@"string----%@",string);
    return string;
}


+ (BOOL) inputValidation: (NSString *)textNum
{
    //    NSString *regex = @"^(\\d{14}|\\d{17})(\\d|[xX])$";
    NSString *regex = @"^(([0-9]+\\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\\.[0-9]+)|([0-9]*[1-9][0-9]*))$";
    NSPredicate *identityCardPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    return [identityCardPredicate evaluateWithObject:textNum];
}


+ (BOOL) inputValidation2: (NSString *)textNum
{
    //real Numbers inclue A decimal
    textNum = [textNum stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    //验证一位小数的正实数
    NSString *regex = @"^[0-9]+(.[0-9]{1})?$";
    NSPredicate *identityCardPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    return [identityCardPredicate evaluateWithObject:textNum];
}

+ (BOOL) inputValidation3: (NSString *)textNum
{
    //validate Non-negative integer
    textNum = [textNum stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    //验证非负整数
    NSString *regex = @"^\\d+$";
    NSPredicate *identityCardPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    return [identityCardPredicate evaluateWithObject:textNum];
}



+(BOOL) emailValidation: (NSString *)textNum{
    // validate email address
    textNum = [textNum stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    //验证Email地址：
    NSString *regex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *identityCardPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    return [identityCardPredicate evaluateWithObject:textNum];
    
}


/**
 * trim String
 */
+(NSString *)trim:(NSString*)String{
    NSString *tempStr = [String stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return tempStr;
}

/**
 *change int to String
 */
+ (NSString *)stringFromInt :(int)number{
    NSString *str = [NSString stringWithFormat:@"%d",number] ;
    return str;
}


+ (NSString *)nowTimeStringForLastSyncDisplay{
    //return now time, like "10:12 02/01"
    NSDate  *now = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSTimeZone *zone = [NSTimeZone localTimeZone];
    [formatter setTimeZone:zone];
    [formatter setDateFormat:@"HH:mm MM/dd"];
    NSString *currentTime = [formatter stringFromDate:now];
    return currentTime;
}

+ (NSString *)formatDateFromTimeArr :(NSArray *)time{
    NSString *timeString;
    if ([@"01" isEqualToString:[time objectAtIndex:1]]) {
        timeString = [NSString stringWithFormat:@"Jan %@",[time objectAtIndex:2]];
    }else if ([@"02" isEqualToString:[time objectAtIndex:1]]){
        timeString = [NSString stringWithFormat:@"Feb %@",[time objectAtIndex:2]];
    }else if ([@"03" isEqualToString:[time objectAtIndex:1]]){
        timeString = [NSString stringWithFormat:@"Mar %@",[time objectAtIndex:2]];
    }else if ([@"04" isEqualToString:[time objectAtIndex:1]]){
        timeString = [NSString stringWithFormat:@"Apr %@",[time objectAtIndex:2]];
    }else if ([@"05" isEqualToString:[time objectAtIndex:1]]){
        timeString = [NSString stringWithFormat:@"May %@",[time objectAtIndex:2]];
    }else if ([@"06" isEqualToString:[time objectAtIndex:1]]){
        timeString = [NSString stringWithFormat:@"Jun %@",[time objectAtIndex:2]];
    }else if ([@"07" isEqualToString:[time objectAtIndex:1]]){
        timeString = [NSString stringWithFormat:@"Jul %@",[time objectAtIndex:2]];
    }else if ([@"08" isEqualToString:[time objectAtIndex:1]]){
        timeString = [NSString stringWithFormat:@"Aug %@",[time objectAtIndex:2]];
    }else if ([@"09" isEqualToString:[time objectAtIndex:1]]){
        timeString = [NSString stringWithFormat:@"Sep %@",[time objectAtIndex:2]];
    }else if ([@"10" isEqualToString:[time objectAtIndex:1]]){
        timeString = [NSString stringWithFormat:@"Oct %@",[time objectAtIndex:2]];
    }else if ([@"11" isEqualToString:[time objectAtIndex:1]]){
        timeString = [NSString stringWithFormat:@"Nov %@",[time objectAtIndex:2]];
    }else if ([@"12" isEqualToString:[time objectAtIndex:1]]){
        timeString = [NSString stringWithFormat:@"Dec %@",[time objectAtIndex:2]];
    }else{
        timeString = @"unexpected";
    }

    return timeString;

}

+ (NSString *) calculateStartTime:(NSString *) startPartTime stopTime:(NSString *)endPartTime date:(NSString *)dateString
{
    NSString *endTime = [NSString stringWithFormat:@"%@ %@:00",dateString,endPartTime];
    NSString *startTime;
    if ([startPartTime compare:endPartTime] < 0) {
        startTime = [NSString stringWithFormat:@"%@ %@:00",dateString,startPartTime];
    }
    else{
        startTime = [NSString stringWithFormat:@"%@ %@:00",dateString,startPartTime];
        NSDate *startDateTime = [[CommonValues timeDateFormatter:DateFormatterTimeColonGMT] dateFromString:startTime];
        startDateTime = [startDateTime dateByAddingTimeInterval:-24*60*60];
        startTime = [[CommonValues timeDateFormatter:DateFormatterTimeColonGMT] stringFromDate:startDateTime];
        
    }
    NSLog(@"auto default time sleep startTime = %@,endTime = %@",startTime,endTime);
    return startTime;
}

@end

//
//  TabBarController.h
//  WAT
//
//  Created by Julia on 4/16/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "lib/TGAccessoryDelegate.h"

@interface TabBarController : UITabBarController<TGAccessoryDelegate>

@end

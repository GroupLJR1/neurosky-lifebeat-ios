//
//  ActivityCalorieViewController.m
//  WAT
//
//  Created by neurosky on 9/17/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import "ActivityCalorieViewController.h"
#import "CommonValues.h"
#import "circleProgressSmall.h"
#import "Constant.h"
#import "ActivityDistanceViewController.h"
#import "ActivityStep.h"
#import "Sleep_ViewController.h"
#import "SleepViewController.h"
#import "ActiveTimeViewController.h"

@interface ActivityCalorieViewController ()

@end

@implementation ActivityCalorieViewController
@synthesize hv;
@synthesize step_value,distance_value,activity_time_value,sleep_value;
@synthesize title_calorie,title_date,title_end_time,title_start_time,title_mid_time;
@synthesize img;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    myWholeView.frame = CGRectMake(0, [CommonValues offsetHeight4DiffiOS], 320, 568-20);
    db = [CommonValues getMyDB];
    // Do any additional setup after loading the view from its nib.
//    [self addClickEventsFor4Img];
    stepIndex = [CommonValues getActivityIndex];
    database_path = [CommonValues getDbPath];
    NSLog(@"database_path = %@",database_path);
    df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    dbTimeArray = [[NSMutableArray alloc]init];
    sleepIntervalTimeArr = [[NSMutableArray alloc]init];
    sleepPhaseArray = [[NSMutableArray alloc]init];
    sleepTimeArray = [[NSMutableArray alloc]init];
    oneDayCalorieArr = [[NSMutableArray alloc]init];
    calorieColor =  [UIColor colorWithRed:255/255.0 green:60/255.0 blue:140/255.0 alpha:1.0];
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    goalCalorie = [userDef integerForKey:USER_GOAL_CALORI];
    NSLog(@"goalCalorie = %d",goalCalorie);
    
    StepArray = [[NSArray alloc]init];
    step4GraphArray =  [[NSMutableArray alloc]init];
    mulStepArray =   [[NSMutableArray alloc]init];
    tempCaloriesArrList =[[NSMutableArray alloc]init];
    
    sss =  [[NSMutableArray alloc]init];
    
    [[TGBleManager sharedTGBleManager] setDelegate:self];
    //add
    // NSLog(@"2");
    
    NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
    demoModeStr = [defaults1 stringForKey:@"DemoMode"];
    if ([demoModeStr isEqualToString:@"1"]) {
        NSLog(@" cardioPlot DemoMode ---- ");
        dbTimeArray = [[NSMutableArray alloc]initWithObjects:@"2014-02-13",@"2014-02-12",@"2014-02-11", nil];
        
        totalEndTime = 7.5*60;
        
        NSMutableArray *arr1 = [[NSMutableArray alloc]initWithObjects:@"50",@"216", @"195",@"163",@"85",@"208",@"300",@"63",@"75",@"106",@"70",@"217",@"293",@"86",@"114",@"76",@"69",nil];
        
        NSMutableArray *arr2 = [[NSMutableArray alloc]initWithObjects:@"0",@"166", @"145",@"13",@"35",@"158",@"250",@"13",@"25",@"36",@"20",@"167",@"243",@"36",@"34",@"26",@"19",nil];
        NSMutableArray *arr3 = [[NSMutableArray alloc]initWithObjects:@"30",@"196", @"175",@"43",@"65",@"188",@"280",@"43",@"55",@"86",@"50",@"197",@"273",@"66",@"64",@"56",@"49",nil];
        
        
        [tempCaloriesArrList addObject:arr1];
        [tempCaloriesArrList addObject:arr2];
        [tempCaloriesArrList addObject:arr3];
        
    }
    
    else{
        NSLog(@" cardioPlot RealMode ---- ");
        dbTimeArray = [CommonValues getPedTimeArr:db];
        
        [self getDataFromDB];
        [self calculateSleepTime];
        
    }
    
    
    //test select like sleep  ,consider timeZone
    
    
    step_value.font = [UIFont fontWithName:@"steelfish" size:30.0];
    step_value.backgroundColor = [UIColor clearColor];
    step_value.textColor = [UIColor colorWithRed:2.0/255.0 green:190.0/255.0 blue:253.0/255.0 alpha:1.0];
    step_value.text = [NSString stringWithFormat:@"%@steps",[StringOperation intToString:Step_count]];
    if (dbTimeArray.count > 0) {
        step_value.text = [NSString stringWithFormat:@"%dsteps",Step_count];
    }
    
    // NSLog(@"3.5");
    distance_value.font = [UIFont fontWithName:@"steelfish" size:30.0];
    distance_value.backgroundColor = [UIColor clearColor];
    distance_value.textColor = [UIColor colorWithRed:40/255.0 green:190/255.0 blue:30/255.0 alpha:1];
    distance_value.text = [NSString stringWithFormat:@"%dm",Distance_count];
    
    if (dbTimeArray.count > 0) {
        distance_value.text = [NSString stringWithFormat:@"%dm",Distance_count];
    }
    // NSLog(@"3.6");
    
    activity_time_value.font = [UIFont fontWithName:@"steelfish" size:30.0];
    activity_time_value.backgroundColor = [UIColor clearColor];
    activity_time_value.textColor = [UIColor colorWithRed:255/255.0 green:150/255.0 blue:40/255.0 alpha:1];
    activity_time_value.text = @"1h24m";
    
    sleep_value.font = [UIFont fontWithName:@"steelfish" size:30.0];
    sleep_value.backgroundColor = [UIColor clearColor];
    sleep_value.textColor = [UIColor colorWithRed:200/255.0 green:36/255.0 blue:167/255.0 alpha:(1)];
    sleep_value.text = @"6h42m";
    
    int hour = totalEndTime/60;
    int min = totalEndTime%60;
    NSLog(@"hour = %d, min = %d",hour,min);
    sleep_value.text = [NSString stringWithFormat:@"%dh%dm",hour,min];
    
    
    title_calorie.font = [UIFont fontWithName:@"steelfish" size:23.0];
    title_calorie.backgroundColor = [UIColor clearColor];
    title_calorie.textColor = [UIColor grayColor];
    title_calorie.text=@"Activity";
    
    title_date.font = [UIFont fontWithName:@"steelfish" size:20.0];
    title_date.backgroundColor = [UIColor clearColor];
    title_date.textColor = [UIColor grayColor];
    title_date.text=timeString;
    
    title_end_time.font = [UIFont fontWithName:@"steelfish" size:18.0];
    title_end_time.backgroundColor = [UIColor clearColor];
    title_end_time.textColor = [UIColor blackColor];
    title_end_time.text=@"0:00 AM";
    
    title_start_time.font = [UIFont fontWithName:@"steelfish" size:18.0];
    title_start_time.backgroundColor = [UIColor clearColor];
    title_start_time.textColor = [UIColor blackColor];
    title_start_time.text=@"0:00 AM";
    
    title_mid_time.font = [UIFont fontWithName:@"steelfish" size:18.0];
    title_mid_time.backgroundColor = [UIColor clearColor];
    title_mid_time.textColor = [UIColor blackColor];
    title_mid_time.text=@"12:00 PM";
    
    leftMidLabel.font = [UIFont fontWithName:@"steelfish" size:18.0];;
    leftMidLabel.backgroundColor = [UIColor clearColor];
    leftMidLabel.textColor = [UIColor blackColor];
    leftMidLabel.text = @"6:00AM";
    
    
    rightMidLabel.font = [UIFont fontWithName:@"steelfish" size:18.0];;
    rightMidLabel.backgroundColor = [UIColor clearColor];
    rightMidLabel.textColor = [UIColor blackColor];
    rightMidLabel.text = @"6:00PM";

    
    
    circle = [[circleProgressSmall alloc] initWithFrame:CGRectMake(20, 74+[CommonValues offsetHeight4DiffiOS], 55, 55)];
    circle.foreImage = [UIImage imageNamed:@"progress_calories"];
    circle.innerLabelTextColor = calorieColor;
    [self.view addSubview:circle];
    
    label=[[UILabel alloc]initWithFrame:CGRectMake(80, 80+[CommonValues offsetHeight4DiffiOS], 100, 50)];
    label.text=[NSString stringWithFormat:@"%@ kcal",[StringOperation intToString:Calories_count]];
    label.font =[UIFont fontWithName:@"steelfish" size:35.0];
    label.textColor = calorieColor;
    [self.view addSubview:label];
    // NSLog(@"3.7");
    
    
    
    uiView=[[UIView alloc]initWithFrame:CGRectMake(0, 128+[CommonValues offsetHeight4DiffiOS], 50, 141)];
    [self.view addSubview:uiView];
    img=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 50, 141)];
    img.image=[UIImage imageNamed:@"symbol_calories"];
    img.userInteractionEnabled=YES;
    [uiView addSubview:img];
    //  NSLog(@"4.0");
    
    noticeLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 22, 50, 30)];
    
    if (StepArray.count>0) {
        noticeLabel.text=[NSString stringWithFormat:@"%@",[StepArray objectAtIndex:0]];
        
    }
    noticeLabel.textColor=[UIColor whiteColor];
    noticeLabel.font = [UIFont fontWithName:@"steelfish" size:12.0];
    noticeLabel.textAlignment = NSTextAlignmentCenter;
    noticeLabel.backgroundColor = [UIColor clearColor];
    [uiView addSubview:noticeLabel];
    UIPanGestureRecognizer *pan=[[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handPan:)];
    pan.minimumNumberOfTouches=1;
    pan.maximumNumberOfTouches=1;
    [uiView addGestureRecognizer:pan];
    
    StepArray = oneDayCalorieArr;
    
    
    circle.progress = 1.0*Calories_count/goalCalorie;
    int aa=(int)([[NSString stringWithFormat:@"%.2f", 1.0*Calories_count/goalCalorie]doubleValue]*100);
    NSLog(@"aa ====== %d",aa);
    circle.pString=[NSString stringWithFormat:@"%d%%",aa];
    
    [self drawGrafe];
    [self addValue];
    
    //    NSLog(@"StepArray = %@",StepArray);
    NSLog(@"setDelegate to Dashboard,prepare for auto sync");
    [[TGBleManager sharedTGBleManager] setDelegate:[CommonValues getDashboardID]];
}

-(void)handPan:(UIPanGestureRecognizer *)paramSender{
    CGPoint point=[paramSender locationInView:self.view];
    // NSLog(@"x====%f",point.x);
    if (point.x<297 && 16<point.x) {
        uiView.frame=CGRectMake(point.x-25, 128+[CommonValues offsetHeight4DiffiOS], 50, 141);
    }
    if (point.x>297) {
        uiView.frame=CGRectMake(270, 128+[CommonValues offsetHeight4DiffiOS], 50, 141);
    }
    if (point.x<16) {
        uiView.frame=CGRectMake(0, 128+[CommonValues offsetHeight4DiffiOS], 50, 141);
    }
    float xielv = 11.347;
    
    for (int i = 0; i < 24; i++) {
        if ( point.x > (xielv*i+26)  &&  point.x < (xielv*i+26+7) ) {
            
            noticeLabel.text=[NSString stringWithFormat:@"%@",[StepArray objectAtIndex:i]];
        }
        if ( point.x > (xielv*i+26+8) && point.x < (xielv*i+26+8+2.347)) {
            noticeLabel.text = @"";
        }
    }
    noticeLabel.backgroundColor = [UIColor clearColor];
    
    
    
    
    
}

-(void)addValue{
    
    noticeLabel.text = @"";
    circle.progress = 1.0*Calories_count/goalCalorie;
    
    NSLog(@"addValue  oneDayTotalCalories sum every minute = %d",oneDayTotalCalories);
    if (dbTimeArray.count >= stepIndex+1) {
        float b =  1.0*oneDayTotalCalories/goalCalorie;
        NSLog(@"goal calories percent = %f",b);
        
        circle.progress = b;
        
        // float c = 0.50;
        
        int aa=(int)([[NSString stringWithFormat:@"%.2f", 1.0*oneDayTotalCalories/goalCalorie] doubleValue]*100);
        NSLog(@" goal calories percent format like 35%% ==%d",aa);
        
        
        if (aa > 100) {
            aa = 100;
        }
        else if(aa<  0){
            aa = 0;
        }
        circle.pString=[NSString stringWithFormat:@"%d%%",aa];
        
    }
    
    
    
    label.text=[NSString stringWithFormat:@"%@ kcal",[StringOperation intToString:Calories_count]];
    step_value.text = [NSString stringWithFormat:@"%@steps",[StringOperation intToString:Step_count]];
    distance_value.text = [NSString stringWithFormat:@"%dm",Distance_count];
    title_date.text=timeString;
    
    int hour = totalEndTime/60;
    int min = totalEndTime%60;
    NSLog(@"hour = %d, min = %d",hour,min);
    sleep_value.text = [NSString stringWithFormat:@"%dh%dm",hour,min];
    
    NSLog(@"1111");
    if (dbTimeArray.count > 0) {
        NSLog(@"[dbTimeArray objectAtIndex:timeDateIndex] = %@",[dbTimeArray objectAtIndex:stepIndex]);
        
        NSArray *time = [[dbTimeArray objectAtIndex:stepIndex] componentsSeparatedByString:@"-"];
        timeString = timeString = [StringOperation formatDateFromTimeArr:time];
        title_date.text = [StringOperation formatDateFromTimeArr:time];
    }
    NSLog(@"222");
    
    // 假如是最近的同步的那天，则使用currentCount 来代替 sum step/calories/distance
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *storedLatestStepStr =  [defaults objectForKey:STEP_LATESTDATE];
    NSString *storedLatestCalorieStr =  [defaults objectForKey:CALORIE_LATESTDATE];
    NSString *storedLatestDistanceStr =  [defaults objectForKey:DISTANCE_LATESTDATE];
    if (dbTimeArray.count > 0) {
        if (stepIndex == 0) {
            if (storedLatestCalorieStr == NULL) {
                storedLatestCalorieStr = @"0";
            }
            label.text = [NSString stringWithFormat:@"%@ kcal",storedLatestCalorieStr];
            
        }
        else{
            label.text = [NSString stringWithFormat:@"%d kcal",Calories_count];
        }
    }
    NSLog(@"333");
    
    if (dbTimeArray.count > 0) {
        if (stepIndex == 0) {
            if (storedLatestStepStr == NULL) {
                storedLatestStepStr = @"0";
            }
            step_value.text = [NSString stringWithFormat:@"%@steps",storedLatestStepStr];
        }
        else{
            step_value.text = [NSString stringWithFormat:@"%dsteps",Step_count];
        }
    }
    
    if (dbTimeArray.count > 0) {
        if (stepIndex == 0) {
            if (storedLatestDistanceStr == NULL) {
                storedLatestDistanceStr = @"0";
            }
            int storedDistance = [storedLatestDistanceStr intValue];
            
            distance_value.text = [CommonValues distanceStrFromDistanceCount:storedDistance];
        }
        else{
            
            distance_value.text = [CommonValues distanceStrFromDistanceCount:Distance_count];
        }
    }
    
    //active time get
    NSString *activeTimeStr = @"0h0m";
    int active_time_minutes = 0;
    if (dbTimeArray.count > stepIndex ) {
        [CommonValues activeModeArr:[dbTimeArray objectAtIndex:stepIndex] :db];
        active_time_minutes = [CommonValues getActiveTimeTotalMinutes];
        activeTimeStr = [NSString stringWithFormat:@"%dh%dm",active_time_minutes/60,active_time_minutes%60];
    }
    activity_time_value.text = activeTimeStr;
    NSLog(@"444");
}
-(void)drawGrafe{
    barChart = [[CPTXYGraph alloc] initWithFrame:CGRectZero];
    CPTTheme *theme = [CPTTheme themeNamed:kCPTPlainWhiteTheme];
    [barChart applyTheme:theme];
    // CPTGraphHostingView *hostingView = (CPTGraphHostingView *)hv;
    CPTGraphHostingView *hostingView = (CPTGraphHostingView *)hv;
    hostingView.hostedGraph = barChart;
    
    // Border
    barChart.plotAreaFrame.borderLineStyle = nil;
    barChart.plotAreaFrame.cornerRadius = 0.0f;
    
    // Paddings
    barChart.paddingLeft = 0.0f;
    barChart.paddingRight = 0.0f;
    barChart.paddingTop = 0.0f;
    barChart.paddingBottom = 0.0f;
    
    barChart.plotAreaFrame.paddingLeft = 0.0;
    barChart.plotAreaFrame.paddingTop = 20.0;
    barChart.plotAreaFrame.paddingRight = 0.0;
    barChart.plotAreaFrame.paddingBottom = 0.0;
    
    int maxCalorie = [CommonValues findMaxNumInArr:oneDayCalorieArr];
    NSLog(@"maxCalorie = %d",maxCalorie);
    
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)barChart.defaultPlotSpace;
    plotSpace.allowsUserInteraction = NO;
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(0)length:CPTDecimalFromInt(maxCalorie)];
    plotSpace.globalYRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(0)length:CPTDecimalFromInt(maxCalorie)];
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(-1)length:CPTDecimalFromFloat(25)];
    
    
    //    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.0f) length:CPTDecimalFromFloat(150.0f)];
    //    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(-1.0f) length:CPTDecimalFromFloat(25.0f)];
    
    //	plotSpace.allowsUserInteraction = YES;
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)barChart.axisSet;
    CPTXYAxis *x = axisSet.xAxis;
    x.minorTickLineStyle = nil;
    
    x.labelingPolicy = CPTAxisLabelingPolicyNone;
    // x.axisConstraints = [CPTConstraints constraintWithRelativeOffset:0.2];
    
    CPTXYAxis *y = axisSet.yAxis;
    
    y.labelingPolicy = CPTAxisLabelingPolicyNone;
    y.axisLineStyle=CPTAxisLabelingPolicyNone;
    y.axisConstraints = [CPTConstraints constraintWithRelativeOffset:11/12.0];
    y.delegate = self;
    
    //设置柱状图的颜色[UIColor colorWithRed:255/255.0 green:60/255.0 blue:140/255.0 alpha:1.0];
    CPTColor *caloreColor = [CPTColor colorWithComponentRed:255/255.0 green:60/255.0 blue:140/255.0 alpha:1.0];
    CPTBarPlot *barPlot = [CPTBarPlot tubularBarPlotWithColor:caloreColor horizontalBars:NO];
    barPlot.dataSource = self;
    barPlot.identifier = @"Bar Plot 2";
    barPlot.fill = [CPTFill fillWithColor:caloreColor];
    barPlot.lineStyle = nil;
    [barChart addPlot:barPlot toPlotSpace:plotSpace];
}

-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot {
    return StepArray.count;
    
}
-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{
    NSDecimalNumber *num = nil;
    if ( [plot isKindOfClass:[CPTBarPlot class]] ) {
        switch ( fieldEnum ) {
            case CPTBarPlotFieldBarLocation:
                
                num = (NSDecimalNumber *)[NSDecimalNumber numberWithUnsignedInteger:index];
                break;
            case CPTBarPlotFieldBarTip:
                
                
                num = (NSDecimalNumber *)[NSDecimalNumber numberWithUnsignedInt:[[StepArray objectAtIndex:index ] intValue] ];
                break;
        }
    }
    
    return num;
}

- (IBAction)leftBt:(id)sender {
    NSLog(@"left btton click");
    
    
    if (stepIndex+1 >= dbTimeArray.count ) {
        return;
    }
    [[TGBleManager sharedTGBleManager] setDelegate:self];
    sleepIndex++;
    stepIndex++;
    totalEndTime = 0;
    
    caloriesIndex++;
    distanceIndex++;
    timeDateIndex++;
    [CommonValues setActivityIndex:stepIndex];
    NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
    demoModeStr = [defaults1 stringForKey:@"DemoMode"];
    if ([demoModeStr isEqualToString:@"1"]) {
        NSLog(@" cardioPlot DemoMode ---- ");
        totalEndTime = 7.5*60;
        
    }
    
    else{
        NSLog(@" cardioPlot RealMode ---- ");
        
        [self getDataFromDB];
        //    [self getDaySleepFromDb];
        [ self calculateSleepTime];
        
    }
    
    mulStepArray = nil;
    mulStepArray = [[NSMutableArray alloc]init];
    
    
    NSLog(@"leftBt---");
    
    StepArray = oneDayCalorieArr;
    
    [self drawGrafe];
    [self addValue];
    NSLog(@"setDelegate to Dashboard,prepare for auto sync");
    [[TGBleManager sharedTGBleManager] setDelegate:[CommonValues getDashboardID]];

}

- (IBAction)rightBt:(id)sender {
    NSLog(@"right btton click");
    
    if (stepIndex < 1) {
        return;
    }
    
    NSLog(@"rightBt---");
    [[TGBleManager sharedTGBleManager] setDelegate:self];
    sleepIndex--;
    stepIndex--;
    caloriesIndex--;
    distanceIndex--;
    timeDateIndex--;
    totalEndTime = 0;
    [CommonValues setActivityIndex:stepIndex];
    NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
    demoModeStr = [defaults1 stringForKey:@"DemoMode"];
    if ([demoModeStr isEqualToString:@"1"]) {
        NSLog(@" cardioPlot DemoMode ---- ");
        totalEndTime = 7.5*60;
        
    }
    
    else{
        NSLog(@" cardioPlot RealMode ---- ");
        
        [self getDataFromDB];
        [ self calculateSleepTime];
    }
    mulStepArray = nil;
    mulStepArray = [[NSMutableArray alloc]init];
    StepArray = oneDayCalorieArr;
    
    //StepArray = mulStepArray;
    
    NSLog(@"StepArray = %@",StepArray);
    
    [self drawGrafe];
    [self addValue];
    NSLog(@"setDelegate to Dashboard,prepare for auto sync");
    [[TGBleManager sharedTGBleManager] setDelegate:[CommonValues getDashboardID]];

}



-(void)getDataFromDB{
    
    oneDayCalorieArr = nil;
    oneDayCalorieArr = [[NSMutableArray alloc]init];
    
    oneDayTotalStep = 0;
    oneDayTotalCalories = 0;
    oneDayTotalDistance = 0;
    
    
    sqlite3_stmt *stmt;
    NSMutableArray *mutArray = [[NSMutableArray alloc]initWithCapacity:24];
    for (int i = 0; i < 24; i++) {
        [mutArray addObject:[NSNumber numberWithInt:0]];
    }

    if (dbTimeArray.count <= 0) {
        oneDayCalorieArr = mutArray;
        return;
    }
    
    NSString *thisDate =   [dbTimeArray objectAtIndex:stepIndex];
    NSString *quary3  = [NSString stringWithFormat:@"select datetime(datetime(time,'%d hour','+0 minute'))  as time1,step,calories,distance from ped where time1 like '%@%%' order by time1 desc",[[df timeZone] secondsFromGMT]/3600,thisDate];
   
    
    NSLog(@"查询这一天总的ped quary3 = %@",quary3);
    if(sqlite3_prepare_v2(db, [quary3 UTF8String], -1, &stmt, NULL) == SQLITE_OK) {
        while(sqlite3_step(stmt) == SQLITE_ROW) {
            const  unsigned  char *dateStr = sqlite3_column_text(stmt, 0);
            NSString *timeStr = [NSString stringWithFormat:@"%s",dateStr];
            
            NSString *subTimeStr =  [timeStr substringToIndex:13];
            int temStep = sqlite3_column_int(stmt, 1);
            int temCalories = sqlite3_column_int(stmt, 2);
            int temDistance = sqlite3_column_int(stmt, 3);
            
            
            oneDayTotalStep +=  temStep;
            oneDayTotalCalories +=  temCalories;
            oneDayTotalDistance +=  temDistance;
            
            for (int  l = 0 ; l < 24; l++) {
                NSString *dateStr111 = [NSString stringWithFormat:@"%@ %02d",[dbTimeArray objectAtIndex:stepIndex],l];
                if ([subTimeStr isEqualToString:dateStr111]) {
                    
                    int   tmpStepValue = [[mutArray objectAtIndex:l] integerValue];
                    tmpStepValue += temCalories;
                    [mutArray replaceObjectAtIndex:l withObject:[NSNumber numberWithInt:tmpStepValue ] ];
                }
            }

            
        }
        sqlite3_finalize(stmt);
    }
    
    NSLog(@"mutArray = %@",mutArray);
    NSLog(@"oneDayTotalStep = %d,oneDayTotalCalories = %d,oneDayTotalDistance = %d",oneDayTotalStep,oneDayTotalCalories,oneDayTotalDistance);
    
    
    Step_count = oneDayTotalStep;
    Calories_count = oneDayTotalCalories;
    Distance_count = oneDayTotalDistance;
    
  
    oneDayCalorieArr = mutArray;
    NSLog(@"oneDayCalorieArr = %@",oneDayCalorieArr);
    
}

-(void)getTimeArr{
    
    sqlite3_stmt *stmt;
    
    // local  date time  from sleep
    NSString *dbTimeStr =[NSString stringWithFormat:@"select  distinct date(datetime(time,'%d hour','+0 minute'))  as time1 from ped order by time1 desc;",[[df timeZone] secondsFromGMT]/3600];
    
    
    NSLog(@"select_dbTimeStr = %@",dbTimeStr);
    
    if(sqlite3_prepare_v2(db, [dbTimeStr UTF8String], -1, &stmt, NULL) == SQLITE_OK) {
        while(sqlite3_step(stmt) == SQLITE_ROW) {
            const  unsigned  char *dateStr = sqlite3_column_text(stmt, 0);
            NSString *timeStr = [NSString stringWithFormat:@"%s",dateStr];
            //  NSLog(@"timeStr = %@",timeStr);
            [dbTimeArray addObject:timeStr];
            
        }
        sqlite3_finalize(stmt);
        
    }
    NSLog(@"dbTimeArray = %@",dbTimeArray);
    
    
    
}
- (void)sleepResults:(TGsleepResult) result
           startTime:(NSDate*) startTime
             endTime:(NSDate*) endTime
            duration:(int) duration  // in seconds
            preSleep:(int) preSleep // in seconds
            notSleep:(int) notSleep // in seconds active time
           deepSleep:(int) deepSleep // in seconds
          lightSleep:(int) lightSleep // in seconds
         wakeUpCount:(int) wakeUpCount // number of wake ups after 1st sleep
          totalSleep:(int) totalSleep
             preWake:(int) preWake // number of seconds active before the end time
          efficiency:(int) efficiency {
    NSString * message;
    NSLog(@"-241 sleep call back ------------");
    int sleepSeconds = 0;
    switch (result) {
            
        case TGsleepResultValid:
        {
            NSLog(@"sleepResults: TGsleepResultValid");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\nTotal Seconds: %d\nSeconds before Sleep: %d\nSeconds AWAKE: %d\nSeconds Deep Sleep: %d\nSeconds Light Sleep: %d\nWake Up Count: %d\nTotal Sleep Seconds: %d\nEfficiency: %d %%",
                       startTime,
                       endTime,
                       duration,
                       preSleep,
                       notSleep,
                       deepSleep,
                       lightSleep,
                       wakeUpCount,
                       totalSleep,
                       efficiency
                       ];
            sleepSeconds = lightSleep + deepSleep;
            
            
        }
            break;
            
        case TGsleepResultNegativeTime:
            NSLog(@"sleepResults: TGsleepResultNegativeTime");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\n\nStart Time is After the End Time",
                       startTime,
                       endTime
                       ];
            break;
            
        case TGsleepResultMoreThan48hours:
            NSLog(@"sleepResults: TGsleepResultMoreThan24hours");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\n\nToo Much Time, must be less than 24 hours",
                       startTime,
                       endTime
                       ];
            break;
        case TGsleepResultNoData:
            NSLog(@"sleepResults: TGsleepResultNoData");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\n\nNo Sleep Data Supplied",
                       startTime,
                       endTime
                       ];
            break;
            
        case TGsleepResultNotValid:
            NSLog(@"sleepResults: TGsleepResultNotValid");
        default:
            NSLog(@"sleepResults: default");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\n\nUnexpected Sleep Results\nResult Code: %d",
                       startTime,
                       endTime,
                       result
                       ];
            
            break;
    }
    
    
    
    //    dispatch_async(dispatch_get_main_queue(), ^{ [alert show]; }); // do all alerts on the main thread
    NSLog(@"message  = %@",message);
    totalEndTime = sleepSeconds;
    
    sleep_value.text = [NSString stringWithFormat:@"%dh%dm",sleepSeconds/60,sleepSeconds%60];
    //    customerView3.progressImageView.progress = 1.0f*sleepSeconds/(goalSleep*60);
    
}



//test calculate sleep time
-(void)calculateSleepTime{
    NSLog(@"testCalcuSleepTime====== start");
    if ([CommonValues startSleepAnalysis:stepIndex :db] == -1) {
        totalEndTime = 0;
        
    }
    
    NSLog(@"testCalcuSleepTime====== end");
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnClick:(id)sender {
    NSLog(@"backBtnClick----");
       
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)addClickEventsFor4Img{
    NSLog(@"addClickEventsFor4Img--");
    distanceImg.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap1 =           [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(distanceImgClick:)];
    [distanceImg addGestureRecognizer:singleTap1];
    
    stepImg.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap2 =           [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(stepImgClick:)];
    [stepImg addGestureRecognizer:singleTap2];
    
    sleepImg.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap3 =           [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sleepImgClick:)];
    [sleepImg addGestureRecognizer:singleTap3];
    
    activeImg.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap4 =           [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(activeImgClick:)];
    [activeImg addGestureRecognizer:singleTap4];
    
    
    
}
-(void)activeImgClick:(id)sender{
    NSLog(@"active time Img Click---");
    ActiveTimeViewController *acvc = [[ActiveTimeViewController alloc]init];
    [self presentViewController:acvc animated:YES completion:nil];
    
    
}
-(void)distanceImgClick:(id)sender{
    NSLog(@"distance Img Click---");
    ActivityDistanceViewController *acvc = [[ActivityDistanceViewController alloc]init];
    [self presentViewController:acvc animated:YES completion:nil];
}
-(void)stepImgClick:(id)sender{
    NSLog(@"step Img Click---");
    ActivityStep *acvc = [[ActivityStep alloc]init];
    [self presentViewController:acvc animated:YES completion:nil];
}
-(void)sleepImgClick:(id)sender{
    NSLog(@"sleep Img Click---");
    int screenHeight = [CommonValues getScreenHeight];
    if (screenHeight == 568) {
        SleepViewController *cpvc = [[SleepViewController alloc]init] ;
        [self presentViewController:cpvc animated:YES
                         completion:nil];
        
    }
    
    else{
        Sleep_ViewController *cpvc = [[Sleep_ViewController alloc]init] ;
        [self presentViewController:cpvc animated:YES
                         completion:nil];
        
    }
    
}


@end

//
//  SleepViewController.h
//  WAT
//
//  Created by neurosky on 14-3-7.
//  Copyright (c) 2014年 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"
#import <sqlite3.h>
#import "TGBleManager.h"
#import "TGLibEKG.h"
#import "CommonValues.h"
#import "Constant.h"
#import "SleepModifyViewController.h"
#import "Sleep.h"
#import "TGLibEKGdelegate.h"
#import "TGBleManagerDelegate.h"
@interface SleepViewController : UIViewController < CPTPlotDataSource, TGBleManagerDelegate > {

    __weak IBOutlet UIView *myWholeView;
    __weak IBOutlet UILabel *DateLabel;

    __weak IBOutlet UILabel *titleLabel;

    __weak IBOutlet UILabel *wake_count_label;
    __weak IBOutlet UILabel *awakeLabel;

    __weak IBOutlet UILabel *rightTimeLabel;
    __weak IBOutlet UILabel *midTimeLabel;
    __weak IBOutlet UILabel *leftTimeLabel;
    __weak IBOutlet UILabel *deepLabel;
    __weak IBOutlet UILabel *beforeLabel;
    __weak IBOutlet UILabel *efficiencyLabel;
    __weak IBOutlet UILabel *light_sleep_label;
    Sleep *sleep_;
     NSInteger  sleepIndex;
     CPTXYGraph *barChart;
    NSArray *SleepArray;
      NSMutableArray  *dbTimeArray;
    sqlite3  *db;
    NSMutableArray *sleepPhaseArray;
    NSMutableArray *sleepTimeArray;
    UIFont *font;
    NSDateFormatter *df;
        NSDateFormatter *local_solid_df;
    
    NSString *database_path;
 //   TGLibEKG * tglibEKG;
    
    NSMutableArray *sleepGraphDataSouceArr;
    int  lastIndex;
    float  screenHeight;
    CPTGraphHostingView  *hv_4S;
    
    int ystLastSleepPhase;
    NSString *ystLastSleepTime;
    NSString *yesterdayDateStr;
    
    __weak IBOutlet UILabel *firstLabel;
    __weak IBOutlet UILabel *secondLabel;
    __weak IBOutlet UILabel *thirdLabel;
    NSString *gStartTime;
    NSString *gEndTime;
   
}

@property (weak, nonatomic) IBOutlet CPTGraphHostingView *hv;
@property (strong, nonatomic) IBOutlet UIScrollView *my_scrollview;
@property (strong, nonatomic) IBOutlet UIScrollView *my_scrollview4S;
- (IBAction)right_click:(id)sender;
- (IBAction)left_click:(id)sender;
- (IBAction)back_click:(id)sender;
- (IBAction)modifyClick:(id)sender;




@end

//
//  HelpViewController.h
//  WAT
//
//  Created by neurosky on 5/28/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpViewController : UIViewController{

    float screenHeight;

    __weak IBOutlet UIView *myWholeView;
    __weak IBOutlet UIScrollView *helpScrollView;
    __weak IBOutlet UILabel *help_title_label;
}
- (IBAction)backClick:(id)sender;

@end

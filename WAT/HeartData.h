//
//  HeartData.h
//  WAT
//
//  Created by NeuroSky on 1/29/15.
//  Copyright (c) 2015 Julia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "CommonValues.h"

@interface HeartData : NSObject

+ (NSMutableArray *)  MedianArrayDayCount :(NSInteger)dayCount endDateStr :(NSString *) endDateStr isHR :(BOOL)is_hr database : (sqlite3 *) database MovingInterval:(NSUInteger) MovingInterval MedianInterval:(NSUInteger)medianInterval;

+ (NSInteger)getMedian :(NSMutableArray *)HRArr;
+ (NSMutableArray *) avgValueArr : (NSUInteger) intervalCount :(NSMutableArray *)filteredMedianArray ;

+(NSMutableArray *)  MultiHeartArrayDayCount :(int)dayCount  endDateStr :(NSString *) endDateStr isHR :(BOOL)is_hr database : (sqlite3 *) database;
+(NSMutableArray *)  MultiDaysOfHeartArray :(int)dayCount endDateStr :(NSString *) endDateStr  timeAndHrOrHrvArr :(NSMutableArray *)timeAndHrOrHrvArr calculateDaysInterval:(int)daysInterval;
+ (NSMutableArray *) movingAvgMedianArr :(int)dayCount MultiDaysOfHeartArray:(NSMutableArray *)dataArr;
+ (int)avgHrvWithHrvArr:(NSMutableArray *)hrvArr;
+ (int)avgHrWithHrArr:(NSMutableArray *)hrArr;

@end

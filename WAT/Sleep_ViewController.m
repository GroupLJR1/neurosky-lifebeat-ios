//
//  SleepViewController.m
//  WAT
//
//  Created by neurosky on 14-3-7.
//  Copyright (c) 2014年 Julia. All rights reserved.
//

#import "Sleep_ViewController.h"

@interface Sleep_ViewController ()


@end



@implementation Sleep_ViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (void)viewDidLoad
{
    
    //   NSMutableArray *fill_arr = [[NSMutableArray alloc]initWithCapacity:600];
    
    //    fill_arr = [[NSMutableArray alloc]init];
     NSLog(@"Sleep_ViewController--------4S");
    [super viewDidLoad];
    
    myWholeView.frame = CGRectMake(0, [CommonValues offsetHeight4DiffiOS], 320, 568-20);
    db = [CommonValues getMyDB];
    sleep = [[Sleep alloc] init];
    titleLabel.font =  [UIFont fontWithName:@"steelfish" size:25.0];
    
    titleLabel.textColor = [UIColor grayColor];
    
    //dynamic scroll view content
    CGRect rect_screen = [[UIScreen mainScreen]bounds];
    CGSize size_screen = rect_screen.size;
    screenHeight = size_screen.height;
    NSLog(@"screenHeight = %f",screenHeight);
    
    //reset ystLast
    ystLastSleepPhase = 1;
    ystLastSleepTime = @"";
    my_scrollview4S = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 40+[CommonValues offsetHeight4DiffiOS], 320, 525-88-20)];
    my_scrollview4S.contentSize = CGSizeMake(320, 525-20);
    UIImageView *iv =[ [UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_sleep"]];
    [my_scrollview4S addSubview:iv];
    [my_scrollview4S addSubview:headView];
    hv_4S = [[CPTGraphHostingView alloc]initWithFrame:CGRectMake(10, 39, 300, 183)];
    [my_scrollview4S addSubview:hv_4S];
    beforeLabel = [[UILabel alloc]initWithFrame:CGRectMake(59, 265, 80, 34)];
    beforeLabel.textAlignment = NSTextAlignmentCenter;
    [my_scrollview4S addSubview:beforeLabel];
    
    awakeLabel = [[UILabel alloc]initWithFrame:CGRectMake(220, 265, 80, 34)];
    awakeLabel.textAlignment = NSTextAlignmentCenter;
    [my_scrollview4S addSubview:awakeLabel];
    deepLabel = [[UILabel alloc]initWithFrame:CGRectMake(59, 359-15, 80, 34)];
    deepLabel.textAlignment = NSTextAlignmentCenter;
    [my_scrollview4S addSubview:deepLabel];
    light_sleep_label = [[UILabel alloc]initWithFrame:CGRectMake(220, 359-15, 80, 34)];
    light_sleep_label.textAlignment = NSTextAlignmentCenter;
    [my_scrollview4S addSubview:light_sleep_label];
    wake_count_label = [[UILabel alloc]initWithFrame:CGRectMake(59, 444-15, 80, 34)];
    wake_count_label.textAlignment = NSTextAlignmentCenter;
    [my_scrollview4S addSubview:wake_count_label];
    efficiencyLabel = [[UILabel alloc]initWithFrame:CGRectMake(220, 444-15, 80, 34)];
    efficiencyLabel.textAlignment = NSTextAlignmentCenter;
    [my_scrollview4S addSubview:efficiencyLabel];
    
    
    firstLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 223, 50, 28)];
    firstLabel.text = @"12:00PM";
    firstLabel.font = [UIFont fontWithName:@"steelfish" size:20.0];
    //    firstLabel.backgroundColor = [UIColor blueColor];
    secondLabel.textAlignment = NSTextAlignmentLeft;
    [my_scrollview4S addSubview:firstLabel];
    
    secondLabel = [[UILabel alloc]initWithFrame:CGRectMake(140, 223, 50, 28)];
    secondLabel.font = [UIFont fontWithName:@"steelfish" size:20.0];
    secondLabel.text = @"5:00AM";
    secondLabel.textAlignment = NSTextAlignmentCenter;
    //    secondLabel.backgroundColor = [UIColor blueColor];
    [my_scrollview4S addSubview:secondLabel];
    
    thirdLabel = [[UILabel alloc]initWithFrame:CGRectMake(265, 223, 50, 28)];
    thirdLabel.font = [UIFont fontWithName:@"steelfish" size:20.0];
    thirdLabel.text = @"10:00AM";
    thirdLabel.textAlignment = NSTextAlignmentRight;
    
    //    thirdLabel.backgroundColor = [UIColor blueColor];
    
    [my_scrollview4S addSubview:thirdLabel];
    
    [self.view addSubview:my_scrollview4S];
    
    //
     database_path = [[CommonValues getDocumentPath] stringByAppendingPathComponent:DBNAME];
    NSLog(@"database_path = %@",database_path);
    //    tglibEKG = [[TGLibEKG alloc] init];
    //    tglibEKG.delegate = self;
    [[TGBleManager sharedTGBleManager] setDelegate:self];
    font =  [UIFont fontWithName:@"steelfish" size:30.0];
    titleLabel.font = [UIFont fontWithName:@"steelfish" size:23.0];
    titleLabel.textColor = [UIColor grayColor];
    dateLabel.font =  [UIFont fontWithName:@"steelfish" size:20.0];
    dateLabel.backgroundColor = [UIColor clearColor];
    dateLabel.textColor = [UIColor grayColor];
    wake_count_label.font = font;
    awakeLabel.font = font;
    deepLabel.font = font;
    beforeLabel.font = font;
    efficiencyLabel.font = font;
    light_sleep_label.font = font;
    
    wake_count_label.text = @"0";
    awakeLabel.text = @"0";
    deepLabel.text = @"0";
    beforeLabel.text = @"0";
    efficiencyLabel.text = @"0";
    light_sleep_label.text = @"0";
    
    sleepIndex = 0;
    // Do any additional setup after loading the view from its nib.
    
    // sleepGraphDataSouceArr = [[NSArray alloc]i];
    
    sleepGraphDataSouceArr = [[NSMutableArray alloc] initWithCapacity:600];
    
    
    //    [MutableArray insertObject:@"3" atIndex:0];
    //     [MutableArray insertObject:@"4" atIndex:1];
    //    NSLog(@"MutableArray [1] = %@",MutableArray[1]);
    
    
    df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    local_solid_df = [[NSDateFormatter alloc] init];
    // NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
    
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [local_solid_df setTimeZone:gmt];
    //  local_solid_df
    
    
    
    dbTimeArray = [[NSMutableArray alloc]init];
    SleepArray = [[NSArray alloc]init];
    sleepPhaseArray = [[NSMutableArray alloc]init];
    
    sleepTimeArray = [[NSMutableArray alloc]init];
    SleepArray = [[NSMutableArray alloc]initWithObjects:@"50",@"216", @"195",@"163",@"85",@"208",@"300",@"63",@"75",@"106",@"70",@"217",@"293",@"86",@"114",@"76",@"69",nil];
    
    // different device different layout
    
    //    }
    dbTimeArray = [CommonValues getSleepTimeArr:db];
    sleepIndex = [CommonValues getActivityIndex];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)startSleepAnalysis{
    [[TGBleManager sharedTGBleManager]setDelegate:self];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    BOOL setSleepInApp = [userDefault boolForKey:SET_SLEEP_IN_APP];
    if (setSleepInApp) {
        //以app设置的起止时间为准
        NSLog(@"以app设置的起止时间为准");
        NSString *startTime = [userDefault objectForKey:APP_SLEEP_START_TIME];
        NSString *endTime = [userDefault objectForKey:APP_SLEEP_END_TIME];
        [CommonValues sleepAnalysisWithStartTime:startTime withEndTime:endTime withFlag:1 withDataBase:db];
        gStartTime = startTime;
        gEndTime = endTime;
    }
    else{
        //以band按键的起止时间为准，对应band选中的那个时间段
        NSLog(@"以band按键的起止时间为准--");
        NSString *startTime = [userDefault objectForKey:BAND_SLEEP_START_TIME];
        NSString *endTime = [userDefault objectForKey:BAND_SLEEP_END_TIME];
        [CommonValues sleepAnalysisWithStartTime:startTime withEndTime:endTime withFlag:0 withDataBase:db];
        gStartTime = startTime;
        gEndTime = endTime;
    }
    NSLog(@"gStartTime = %@, gEndTime = %@",gStartTime,gEndTime);
    /*
     if ([CommonValues startSleepAnalysis:sleepIndex :db] == -1) {
     [sleepGraphDataSouceArr removeAllObjects];
     [self drawGraph];
     }
     
     [self refreshSleepTimeAxis];
     */
    [self refreshSleepTimeAxis];
    NSLog(@"setDelegate to Dashboard,prepare to autoSync");
    [[TGBleManager sharedTGBleManager]setDelegate:[CommonValues getDashboardID]];
    
}
-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"iphoen4S sleep viewWillAppear");
    
    //从Modify page 回到sleep view
    if ([CommonValues modifyBackFlag]) {
        [[TGBleManager sharedTGBleManager]setDelegate:self];
        [self startSleepAnalysis];
        
        [CommonValues setModifyBackFlag:NO];
    }
    // 从Doahboard或者activity page进到sleep view

    else{
        [[TGBleManager sharedTGBleManager]setDelegate:self];
        [self refreshSleepView];
    }
    
}
-(void)refreshSleepTimeAxis{

        NSString *startTime = gStartTime;
        NSString *endTime = gEndTime;
        NSDateFormatter *ddd = [[NSDateFormatter alloc]init];
        [ddd setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *startDate =  [ddd dateFromString:startTime];
        NSDate *endDate =  [ddd dateFromString:endTime];
        NSLog(@"startDate = %@",startDate);
        NSLog(@"endDate = %@",endDate);
        NSTimeInterval delta =   [endDate timeIntervalSinceDate:startDate];
        NSDate *midDate = [startDate dateByAddingTimeInterval:delta/2];
        [ddd setDateFormat:@"HH:mm"];
        NSString *startTimeX = [ddd stringFromDate:startDate];
        NSString *endDateX = [ddd stringFromDate:endDate];
        NSString *midDateX = [ddd stringFromDate:midDate];
        NSLog(@"startTimeX = %@",startTimeX);
        NSLog(@"endDateX = %@",endDateX);
        firstLabel.text = startTimeX;
        secondLabel.text =  midDateX;
        thirdLabel.text = endDateX;

}
- (IBAction)right_click:(id)sender {
    NSLog(@"right_click--");
    if (sleepIndex < 1) {
        return;
    }
    
     [[TGBleManager sharedTGBleManager]setDelegate:self];
    //reset ystLast
    ystLastSleepPhase = 1;
    ystLastSleepTime = @"";
    
    sleepIndex--;
    
    
    [sleepTimeArray removeAllObjects];
    [sleepPhaseArray removeAllObjects];
    [CommonValues setActivityIndex:sleepIndex];
    [self updateLabelValue:[dbTimeArray objectAtIndex:sleepIndex]];
    /*
    if ([CommonValues startSleepAnalysis:sleepIndex :db] == -1) {
        [sleepGraphDataSouceArr removeAllObjects];
        [self drawGraph];
    }
     
    [self refreshSleepTimeAxis];*/
     [self refreshSleepView];
    NSLog(@"setDelegate to Dashboard,prepare to autoSync");
    [[TGBleManager sharedTGBleManager]setDelegate:[CommonValues getDashboardID]];
}

- (IBAction)left_click:(id)sender {
    NSLog(@"left_click--");
    if (sleepIndex+1 >= dbTimeArray.count ) {
        return;
    }
    
    [[TGBleManager sharedTGBleManager]setDelegate:self];
//reset ystLast
    ystLastSleepPhase = 1;
    ystLastSleepTime = @"";
    sleepIndex++;
    [CommonValues setActivityIndex:sleepIndex];
    [sleepTimeArray removeAllObjects];
    [sleepPhaseArray removeAllObjects];
    [self updateLabelValue:[dbTimeArray objectAtIndex:sleepIndex]];
    [self refreshSleepView];
}
//left,right 点击时 刷新view
-(void)refreshSleepView{
    NSLog(@"refreshSleepView");
    if (sleepIndex+1 > dbTimeArray.count) {
        return;
    }
    NSString *dateStr = [dbTimeArray objectAtIndex:sleepIndex];
    [self updateLabelValue:dateStr];
    
    [sleepTimeArray removeAllObjects];
    [sleepPhaseArray removeAllObjects];
     NSMutableArray *oneDaySleepArr = [[NSMutableArray alloc] init];
    oneDaySleepArr = [CommonValues calculateSleepStartEndTimeArr:dateStr :db];
    if (oneDaySleepArr.count > 0) {
        //默认显示最后一次睡眠
        NSArray *oneceSleep = [oneDaySleepArr objectAtIndex:(oneDaySleepArr.count - 1)];
        NSString *startTime = [oneceSleep objectAtIndex:0];
        NSString *endTime = [oneceSleep objectAtIndex:1];
        gStartTime = startTime;
        gEndTime = endTime;
        NSLog(@"startTime = %@, endTime = %@",startTime,endTime);
        [CommonValues sleepAnalysisWithStartTime:startTime withEndTime:endTime withFlag:0 withDataBase:db];
    }
    else{
        //没有band按下sleep的记录，则用默认的时间段 10:00pm-7:00pm
        NSLog(@"默认的时间段 10:00pm-7:00pm");
        NSString *endTime = [NSString stringWithFormat:@"%@ 07:00:00",dateStr];
        
        NSDate *today = [[CommonValues timeDateFormatter:DateFormatterTimeColon] dateFromString:endTime];
        NSLog(@"XXXXXX");
        NSLog(@"today ====== %@",today);
        NSDate *yesterday = [[NSDate alloc] initWithTimeInterval:-9*60*60 sinceDate:today];
        NSLog(@"yesterday ====== %@",yesterday);
        NSString *startTime = [[CommonValues timeDateFormatter:DateFormatterTimeColon] stringFromDate:yesterday ];
        gStartTime = startTime;
        gEndTime = endTime;
        NSLog(@"startTime = %@, endTime = %@",startTime,endTime);
        [CommonValues sleepAnalysisWithStartTime:startTime withEndTime:endTime withFlag:1 withDataBase:db];
    }
    
    
    [self refreshSleepTimeAxis];
    NSLog(@"setDelegate to Dashboard,prepare to autoSync");
    [[TGBleManager sharedTGBleManager]setDelegate:[CommonValues getDashboardID]];
}
- (IBAction)back_click:(id)sender {
    NSLog(@"back_click--");
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)modifyClick:(id)sender {
    NSLog(@"iphone4S modifyClick---");
    if (sleepIndex+1 > dbTimeArray.count) {
        return;
    }
    SleepModifyViewController *smvc = [[SleepModifyViewController alloc]init];
    [self presentViewController:smvc animated:YES completion:nil];
}



-(void)sleepSmoothData:(int)samepleRate sleepTime:(NSMutableArray *)sleepTimeArray sleepPhase:(NSMutableArray *)sleepPhaseArray{
    //    NSLog(@"sleepSmoothData samepleRate == %d",samepleRate);
    //      NSLog(@"sleepPhaseArray count = %d",sleepPhaseArray.count);
        NSLog(@"sleepSmoothData ---%@, %@", sleepTimeArray, sleepPhaseArray);
    sleepGraphDataSouceArr = sleepPhaseArray;
    [self drawGraph];
    
}

- (void) sleepResults:(TGsleepResult)result
            startTime:(NSDate *)startTime
              endTime:(NSDate *)endTime
             duration:(int)duration
             preSleep:(int)preSleep
             notSleep:(int)notSleep
            deepSleep:(int)deepSleep
           lightSleep:(int)lightSleep
          wakeUpCount:(int)wakeUpCount
           totalSleep:(int)totalSleep
              preWake:(int)preWake
           efficiency:(int)efficiency {
    NSString * message;
    NSLog(@"-241 sleep call back ------------");
    
    //没有有效的sleep Phase 数组时 画空的图
    if (result != TGsleepResultValid) {
        sleepGraphDataSouceArr = NULL;
        [self drawGraph];
        
    }
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter setTimeZone:gmt];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *current = [dateFormatter dateFromString:[dbTimeArray objectAtIndex:sleepIndex]];
    NSLog(@"current = %@",current);
    [dateFormatter setDateFormat:@"MMM dd"];
    NSString *monthDay = [dateFormatter stringFromDate:current];
    NSLog(@"monthDay = %@",monthDay);
    NSDate *yesdate = [current dateByAddingTimeInterval:-24*60*60];
    NSLog(@"current = %@,yesdate = %@",current,yesdate);
    NSString *yesDat = [dateFormatter stringFromDate:yesdate];
    NSString *date = [NSString stringWithFormat:@"%@~%@",yesDat,monthDay];
    NSLog(@"date label text = %@",date);
    
    dateLabel.text = date;
    switch (result) {
            
        case TGsleepResultValid:
        {
            NSLog(@"sleepResults: TGsleepResultValid");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\nTotal Seconds: %d\nSeconds before Sleep: %d\nSeconds AWAKE: %d\nSeconds Deep Sleep: %d\nSeconds Light Sleep: %d\nWake Up Count: %d\nTotal Sleep Seconds: %d\nSeconds Awake before End: %d\nEfficiency: %d %%",
                       startTime,
                       endTime,
                       duration,
                       preSleep,
                       notSleep,
                       deepSleep,
                       lightSleep,
                       wakeUpCount,
                       totalSleep,
                       preWake,
                       efficiency
                       ];
            
            wake_count_label.text = [NSString stringWithFormat:@"%d",wakeUpCount];
            awakeLabel.text = [NSString stringWithFormat:@"%dh%dm",notSleep/60,notSleep%60];
            beforeLabel.text = [NSString stringWithFormat:@"%dh%dm",preSleep/60,preSleep%60];
            light_sleep_label.text = [NSString stringWithFormat:@"%dh%dm",lightSleep/60,lightSleep%60];
            deepLabel.text = [NSString stringWithFormat:@"%dh%dm",deepSleep/60,deepSleep%60];
            efficiencyLabel.text = [NSString stringWithFormat:@"%d%%",efficiency];
        }
            break;
            
        case TGsleepResultNegativeTime:
            NSLog(@"sleepResults: TGsleepResultNegativeTime");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\n\nStart Time is After the End Time",
                       startTime,
                       endTime
                       ];
            break;
            
        case TGsleepResultMoreThan48hours:
            NSLog(@"sleepResults: TGsleepResultMoreThan48hours");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\n\nToo Much Time, must be less than 48 hours",
                       startTime,
                       endTime
                       ];
            break;
        case TGsleepResultNoData:
            NSLog(@"sleepResults: TGsleepResultNoData");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\n\nNo Sleep Data Supplied",
                       startTime,
                       endTime
                       ];
            break;
            
        case TGsleepResultNotValid:
            NSLog(@"sleepResults: TGsleepResultNotValid");
        default:
            NSLog(@"sleepResults: default");
            message = [NSString stringWithFormat:
                       @"Start: %@\nEnd: %@\n\nUnexpected Sleep Results\nResult Code: %lu",
                       startTime,
                       endTime,
                       (unsigned long)result
                       ];
            
            break;
    }
    
    NSLog(@"Sleep Effeciency Analysis Demo message: %@",message );
    /*
     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sleep Effeciency Analysis Demo\nSee Code for details" message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:@" OK ", nil];
     
     dispatch_async(dispatch_get_main_queue(), ^{ [alert show]; }); // do all alerts on the main thread
     */
}


-(void)drawGraph{
    barChart = [[CPTXYGraph alloc] initWithFrame:CGRectZero];
    CPTTheme *theme = [CPTTheme themeNamed:kCPTPlainWhiteTheme];
    [barChart applyTheme:theme];
    // CPTGraphHostingView *hostingView = (CPTGraphHostingView *)hv;
    CPTGraphHostingView *hostingView;
    
    //    if (screenHeight == 568) {
    //        hostingView = (CPTGraphHostingView *)hv;
    //    }
    //    else{
    hostingView = (CPTGraphHostingView *)hv_4S;
    //    }
    
    hostingView.hostedGraph = barChart;
    
    // Border
    barChart.plotAreaFrame.borderLineStyle = nil;
    barChart.plotAreaFrame.cornerRadius = 0.0f;
    
    // Paddings
    barChart.paddingLeft = 0.0f;
    barChart.paddingRight = 0.0f;
    barChart.paddingTop = 0.0f;
    barChart.paddingBottom = 0.0f;
    
    barChart.plotAreaFrame.paddingLeft = 0.0;
    barChart.plotAreaFrame.paddingTop = 20.0;
    barChart.plotAreaFrame.paddingRight = 0.0;
    barChart.plotAreaFrame.paddingBottom = 0.0;
    
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)barChart.defaultPlotSpace;
    plotSpace.allowsUserInteraction = NO;
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(0)length:CPTDecimalFromFloat(5)];
    plotSpace.globalYRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(0)length:CPTDecimalFromFloat(5)];
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(-1)length:CPTDecimalFromFloat(sleepGraphDataSouceArr.count)];
    
    
    //    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.0f) length:CPTDecimalFromFloat(150.0f)];
    //    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(-1.0f) length:CPTDecimalFromFloat(25.0f)];
    
    //	plotSpace.allowsUserInteraction = YES;
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)barChart.axisSet;
    CPTXYAxis *x = axisSet.xAxis;
    x.minorTickLineStyle = nil;
    
    x.labelingPolicy = CPTAxisLabelingPolicyNone;
    // x.axisConstraints = [CPTConstraints constraintWithRelativeOffset:0.2];
    
    CPTXYAxis *y = axisSet.yAxis;
    
    y.labelingPolicy = CPTAxisLabelingPolicyNone;
    y.axisLineStyle=CPTAxisLabelingPolicyNone;
    //add y label test start
    //y.labelingPolicy = CPTAxisLabelingPolicyNone;
    NSArray *customTickLocations2 = [NSArray arrayWithObjects:[NSDecimalNumber numberWithFloat:1],[NSDecimalNumber numberWithFloat:2], [NSDecimalNumber numberWithFloat:3],    nil];
    NSArray *xAxisLabels2         = [NSArray arrayWithObjects:@"AWAKE",@"LIGHT   ", @"DEEP    ",nil];
    NSUInteger labelLocation1     = 0;
    NSMutableArray *customLabels2 = [NSMutableArray arrayWithCapacity:[xAxisLabels2 count]];
    
    
    
    NSLog(@"customTickLocations2.count = %d",customTickLocations2.count);
    // Axes
    CPTMutableTextStyle *mm = [[CPTMutableTextStyle alloc]init];
    // font =  [UIFont fontWithName:@"steelfish" size:30.0];
    mm.fontName=@"steelfish";
    mm.color = [CPTColor grayColor];
    mm.fontSize = 12;
    for ( NSNumber *tickLocation2 in customTickLocations2 ) {
        CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithText:[xAxisLabels2 objectAtIndex:labelLocation1++] textStyle:mm];
        newLabel.tickLocation = [tickLocation2 decimalValue];
        //    newLabel.offset       = y.labelOffset +0.4 ;
        // newLabel.rotation     = M_PI / 4;
        [customLabels2 addObject:newLabel];
        
    }
    
    y.axisLabels = [NSSet setWithArray:customLabels2];
    y.axisConstraints = [CPTConstraints constraintWithRelativeOffset:0.18];
    y.delegate = self;
    
    CPTBarPlot *barPlot = [CPTBarPlot tubularBarPlotWithColor:[CPTColor colorWithComponentRed:2.0/255.0 green:190.0/255.0 blue:253.0/255.0 alpha:0.0] horizontalBars:NO];
    
    
    
    barPlot.dataSource = self;
    //  barPlot.baseValue = CPTDecimalFromString(@"0");
    //   barPlot.barOffset = CPTDecimalFromFloat(0.25f);
    //  barPlot.barCornerRadius = 2.0f;
    barPlot.identifier = @"Bar Plot 2";
    barPlot.fill = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:2.0/255.0 green:190.0/255.0 blue:253.0/255.0 alpha:1.0]];
    //    barPlot.barBasesVary = YES;
    barPlot.lineStyle = nil;
    //   CPTLineStyle *myLineStye = [CPTLineStyle lineStyle] ;
    //    myLineStye.lineColor = [];
    
    
    
    //   barPlot.lineStyle = myLineStye;
    
    //   myLineStye.lineColor = [CPTColor colorWithComponentRed:1.0 green:1.0 blue:1.0 alpha:1.0];
    
    //    barPlot.  barWidthsAreInViewCoordinates = NO;
    barPlot.barWidth = CPTDecimalFromDouble(1.1);
    //    barPlot.barWidthScale = 1.0;
    [barChart addPlot:barPlot toPlotSpace:plotSpace];
    
    
    // barPlot.  barWidthsAreInViewCoordinates = NO;
    
}


-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot {
    NSLog(@"numberOfRecordsForPlot sleepGraphDataSouceArr.count = %d",sleepGraphDataSouceArr.count);
    return sleepGraphDataSouceArr.count;
    
}
-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{
    NSDecimalNumber *num = nil;
    if ( [plot isKindOfClass:[CPTBarPlot class]] ) {
		switch ( fieldEnum ) {
			case CPTBarPlotFieldBarLocation:
                
				num = (NSDecimalNumber *)[NSDecimalNumber numberWithUnsignedInteger:index];
                //     NSLog(@"num = %@",num);
				break;
			case CPTBarPlotFieldBarTip:
            {
                int sleep_phase = [[sleepGraphDataSouceArr objectAtIndex:index ] intValue];
                
                num = (NSDecimalNumber *)[NSDecimalNumber numberWithUnsignedInt:sleep_phase];
                //  NSLog(@"SleepArray object %@",[sleepGraphDataSouceArr objectAtIndex:index ]);
                //                if (index> 500) {
                //                    CPTBarPlot *ss = (CPTBarPlot*)plot;
                //
                //                    ss.fill = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:2.0/255.0 green:190.0/255.0 blue:253.0/255.0 alpha:0.0]];
                //                }
            }
 				break;
		}
    }
    
    return num;
}


- (NSArray *) barFillsForBarPlot:		(CPTBarPlot *) 	barPlot
                recordIndexRange:		(NSRange) 	indexRange{
    
    NSLog(@"barFillsForBarPlot---");
    NSMutableArray *fill_arr = [[NSMutableArray alloc]initWithCapacity:sleepGraphDataSouceArr.count];
    fill_arr = [sleep barFillsFromSleepDataSourceArr:sleepGraphDataSouceArr];
    //    NSLog(@"fill_arr = %@",fill_arr);
    return fill_arr;
    
}

- (void)sleepDownsampleAnalysisResults:(int)minutes
                         withStartTime:(NSDate *)startTime
                           withEndTime:(NSDate *)endTime
                            withLength:(int)len
                         withSleepTime:(uint32_t*)sleeptime
                        withSleepphase:(int8_t*)sleepphase{
    NSLog(@"Sleep Downsample results: %d, %@, %@",minutes,startTime,endTime);
    NSMutableArray *tempSleepPhaseArr = [[NSMutableArray alloc]init];
    NSMutableArray *tempSleepTimeArr = [[NSMutableArray alloc]init];
    
    
    [[TGBleManager sharedTGBleManager] sleepInitAnalysis:startTime endTime:endTime];
    NSDateFormatter *mdf = [[NSDateFormatter alloc]init];
    [mdf setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    for (int i = 0; i < len; i++) {
        NSDate *tmpDate = [NSDate dateWithTimeIntervalSince1970:sleeptime[i]];
        int tmpPhase = sleepphase[i];
        NSString *tmpDateStr =  [mdf stringFromDate:tmpDate];
        [tempSleepTimeArr addObject:tmpDateStr];
        [tempSleepPhaseArr addObject:[NSString stringWithFormat:@"%d",tmpPhase]];
        [[TGBleManager sharedTGBleManager] sleepAddData:tmpDate sleepPhase:tmpPhase];
    }
    [[TGBleManager sharedTGBleManager] sleepRequestAnalysis];
    
    //    NSLog(@"tempSleepTimeArr = %@",tempSleepTimeArr);
    //    NSLog(@"tempSleepPhaseArr = %@",tempSleepPhaseArr);
    [sleepGraphDataSouceArr removeAllObjects];
    sleepGraphDataSouceArr = [sleep getDaySleepGraphDataSource3:tempSleepTimeArr withSleepPhaseArr:tempSleepPhaseArr withStartTime:startTime withEndTime:endTime];
    [self drawGraph];
    
    
}



- (void)sleepResults:(TGsleepResult) result
           startTime:(NSDate*) startTime
             endTime:(NSDate*) endTime
            duration:(int) duration  // in seconds
            preSleep:(int) preSleep // in seconds
            notSleep:(int) notSleep // in seconds active time
           deepSleep:(int) deepSleep // in seconds
          lightSleep:(int) lightSleep // in seconds
         wakeUpCount:(int) wakeUpCount // number of wake ups after 1st sleep
          totalSleep:(int) totalSleep
          efficiency:(int) efficiency{
    
    
    NSLog(@"startTime %@ endTime %@ duration %d preSleep %d notSleep %d deepSleep %d lightSleep %d wakeUpCount %d totalSleep %d efficiency %d",startTime,endTime,duration,preSleep,notSleep,deepSleep,lightSleep,wakeUpCount,totalSleep,efficiency);
    
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
    
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter setTimeZone:gmt];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *current = [dateFormatter dateFromString:[dbTimeArray objectAtIndex:sleepIndex]];
    //
    //NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
    NSLog(@"current = %@",current);
    
    [dateFormatter setDateFormat:@"MMM dd"];
    NSString *monthDay = [dateFormatter stringFromDate:current];
    NSLog(@"monthDay = %@",monthDay);
    NSDate *yesdate = [current dateByAddingTimeInterval:-24*60*60];
    
    NSLog(@"current = %@,yesdate = %@",current,yesdate);
    
    NSString *yesDat = [dateFormatter stringFromDate:yesdate];
    
    NSString *date = [NSString stringWithFormat:@"%@~%@",yesDat,monthDay];
    NSLog(@"date label text = %@",date);
    
    dateLabel.text = date;
    wake_count_label.text = [NSString stringWithFormat:@"%d",wakeUpCount];
    awakeLabel.text = [NSString stringWithFormat:@"%dh%dm",notSleep/60/60,notSleep/60%60];
    beforeLabel.text = [NSString stringWithFormat:@"%dh%dm",preSleep/60/60,preSleep/60%60];
    light_sleep_label.text = [NSString stringWithFormat:@"%dh%dm",lightSleep/60/60,lightSleep/60%60];
    deepLabel.text = [NSString stringWithFormat:@"%dh%dm",deepSleep/60/60,deepSleep/60%60];
    efficiencyLabel.text = [NSString stringWithFormat:@"%d%%",efficiency];
    
}


-(void)updateLabelValue :(NSString *) dateStr{
    wake_count_label.text = @"0";
    awakeLabel.text = @"0";
    deepLabel.text = @"0";
    beforeLabel.text = @"0";
    efficiencyLabel.text = @"0";
    light_sleep_label.text = @"0";
    
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
    
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter setTimeZone:gmt];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *current = [dateFormatter dateFromString:dateStr];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:current forKey:CURRENT_CARE_DATE];
    [userDefault synchronize];
    
    NSDate *yesdate = [current dateByAddingTimeInterval:-24*60*60];
    
    NSLog(@"current = %@,yesdate = %@",current,yesdate);
    
    [dateFormatter setDateFormat:@"MMM dd"];
    NSString *monthDay = [dateFormatter stringFromDate:current];
    
    NSString *yesDat = [dateFormatter stringFromDate:yesdate];
    
    NSString *date = [NSString stringWithFormat:@"%@~%@",yesDat,monthDay];
    NSLog(@"date label text = %@",date);
    dateLabel.text = date;
    
}



@end

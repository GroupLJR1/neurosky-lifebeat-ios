//
//  CardioZoneViewController.h
//  WAT
//
//  Created by Julia on 4/22/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"

@interface CardioZoneViewController : UIViewController<CPTPlotDataSource,CPTAxisDelegate>
{
    
    
    CPTXYGraph *graph;
    
    NSMutableArray *dataForPlot;
    
    NSMutableArray  *dataArray;
    CPTGraphHostingView *hv;
    IBOutlet UIButton *back;
    
    
}

@property (readwrite, retain, nonatomic) NSMutableArray *dataForPlot;
@property(retain,nonatomic) IBOutlet CPTGraphHostingView *hv;

@end

//
//  Sleep.m
//  WAT
//
//  Created by NeuroSky on 2/9/15.
//  Copyright (c) 2015 Julia. All rights reserved.
//

#import "Sleep.h"

@implementation Sleep


- (NSMutableArray *)barFillsFromSleepDataSourceArr:(NSMutableArray *)sleepGraphDataSouceArr{
    //sleep_phase  4 kinds state
    CPTFill *light_fill = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:0.0/255.0 green:240.0/255.0 blue:253.0/255.0 alpha:1.0]];
    
    CPTFill *deep_fill = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:76.0/255.0 green:136.0/255.0 blue:236.0/255.0 alpha:1.0]];
    
    //    CPTFill *wake_fill = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:236.0/255.0 green:102.0/255.0 blue:72.0/255.0 alpha:1.0]];
    
    CPTFill *restless_fill = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:236.0/255.0 green:208.0/255.0 blue:76.0/255.0 alpha:1.0]];
    NSMutableArray *fill_arr = [[NSMutableArray alloc]initWithCapacity:sleepGraphDataSouceArr.count];
    if (sleepGraphDataSouceArr.count >0) {
        for (int i = 0; i< sleepGraphDataSouceArr.count; i++) {
            NSString *tmpSleepPhase = [NSString stringWithFormat:@"%@",[sleepGraphDataSouceArr objectAtIndex:i]];
            
            if ([tmpSleepPhase isEqualToString:@"1"] ) {
                [fill_arr insertObject:restless_fill atIndex:i];
            }
            else if ([tmpSleepPhase isEqualToString:@"2"] ) {
                [fill_arr insertObject:light_fill atIndex:i];
            }
            
            else if ([tmpSleepPhase isEqualToString:@"3"] ) {
                [fill_arr insertObject:deep_fill atIndex:i];
            }
            
            //            else if ([sleepGraphDataSouceArr[i] isEqualToString:@"4"] ) {
            //                [fill_arr insertObject:wake_fill atIndex:i];
            //            }
            else{
                [fill_arr insertObject:restless_fill atIndex:i];
            }
            
        }
        
    }

    return fill_arr;

}

- (NSMutableArray *)getDaySleepGraphDataSource3 :(NSMutableArray *) sleepTimeArr withSleepPhaseArr:(NSMutableArray *)sleepPhaseArr withStartTime:(NSDate *)startTime withEndTime:(NSDate *) endTime   {
    NSLog(@"getDaySleepGraphDataSource3-------");
    NSMutableArray *sleepGraphDataSouceArr = [[NSMutableArray alloc] init];
    NSDateFormatter *dateF = [[NSDateFormatter alloc]init];
    [dateF setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    int lastDeltaMin = 0;
    
    //构建sleepGraphDataSouceArr，用于画sleep 柱状图
    
    
    if (sleepTimeArr.count > 0){
        NSLog(@" phase array > 0");
        
        //            NSString* time2 =  [sleepTimeArr objectAtIndex:0];
        //            NSDate *date1 = startTime;
        //            NSDate *date2 = [dateF dateFromString:time2];
        //            NSTimeInterval delta_ =  [date2 timeIntervalSinceDate:date1];
        //            int deltaMin_ =  delta_/60;
        //
        //            for (int j = 0; j< deltaMin_; j++) {
        //                [sleepGraphDataSouceArr insertObject:[sleepPhaseArr objectAtIndex:0]  atIndex:j];
        //            }
        //
        //
        //
        //            lastDeltaMin = deltaMin_;
        
        //count > 1
        for (int i = 1; i<sleepTimeArr.count; i++) {
            NSString* time1 =  [sleepTimeArr objectAtIndex:i-1];
            
            NSString* time2 =  [sleepTimeArr objectAtIndex:i];
            NSDate *date1 = [dateF dateFromString:time1];
            NSDate *date2 = [dateF dateFromString:time2];
            
            NSTimeInterval delta =  [date2 timeIntervalSinceDate:date1];
            int deltaMin =  delta/60;
            //                NSLog(@"delta mins = %d",deltaMin);
            
            for (int j = 0; j< deltaMin; j++) {
                [sleepGraphDataSouceArr insertObject:[sleepPhaseArr objectAtIndex:i-1] atIndex:(lastDeltaMin+j)];
            }
            
            lastDeltaMin += deltaMin;
        }
        
        //
        
        NSString* time22 =  [sleepTimeArr objectAtIndex:(sleepTimeArr.count-1)];
        NSDate *date11 = endTime;
        NSDate *date22 = [dateF dateFromString:time22];
        NSTimeInterval delta_1 =  [date11 timeIntervalSinceDate:date22];
        int deltaMin_1 =  delta_1/60;
        for (int j = 0; j< deltaMin_1; j++) {
            [sleepGraphDataSouceArr insertObject:[sleepPhaseArr objectAtIndex:(sleepTimeArr.count-1)] atIndex:j+lastDeltaMin];
        }
        
        
    }
    
    NSLog(@"sleepGraphDataSouceArr %@",sleepGraphDataSouceArr);
    return sleepGraphDataSouceArr;
    
}

@end

//
//  CustomerView.h
//  WAT
//
//  Created by  NeuroSky on 9/24/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CHYPorgressImageView.h"
@interface CustomerView : UIView


- (id)initWithFrame:(CGRect)frame backgroundImage:(UIImage *)backgroundImage textValue:(NSString*)stringValue  withTextColor:(UIColor *)textColor  progress:(float) progressValue  progressForImgName:(NSString *) imgName;

@property (retain,nonatomic) UIImageView * backgroundImageView;

@property (retain,nonatomic) UILabel * label;

@property (retain,nonatomic) UIImageView * progressForImg;
@property (retain,nonatomic) CHYPorgressImageView *progressImageView;

@end

//
//  DoDataReceiveStuff.h
//  WAT
//
//  Created by neurosky on 14-2-15.
//  Copyright (c) 2014年 Julia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TGLibEKG.h"
#import "TGLibEKGdelegate.h"

@interface DoDataReceiveStuff : NSObject<TGLibEKGdelegate>{

    UILabel *_hrvLabel;
}

-(id)initWithLabel : (UILabel *)hrvLabel;

@end

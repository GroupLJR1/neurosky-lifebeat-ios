//
//  Sleep.h
//  WAT
//
//  Created by NeuroSky on 2/9/15.
//  Copyright (c) 2015 Julia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CorePlot-CocoaTouch.h"

@interface Sleep : NSObject

- (NSMutableArray *)barFillsFromSleepDataSourceArr:(NSMutableArray *)sleepGraphDataSouceArr;
- (NSMutableArray *)getDaySleepGraphDataSource3 :(NSMutableArray *) sleepTimeArr withSleepPhaseArr:(NSMutableArray *)sleepPhaseArr withStartTime:(NSDate *)startTime withEndTime:(NSDate *) endTime ;

@end

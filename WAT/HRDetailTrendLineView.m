//
//  HRDetailTrendLineView.m
//  WAT
//
//  Created by neurosky on 10/10/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import "HRDetailTrendLineView.h"

@implementation HRDetailTrendLineView{
    
    CPTXYGraph *graph;
    
    NSMutableArray *dataForPlot;
     CPTGraphHostingView *hv;
    CPTScatterPlot *dataSourceLinePlot3;
    
    CPTScatterPlot *dataSourceLinePlot4;
    
    NSInteger dayCount ;
    float k_line;
    float b_line;
    
}
-(id)initWithFrame:(CGRect)frame   :(NSMutableArray *)arr{
    
    
    self = [super initWithFrame:frame];
    if (self) {
        
        if (arr == NULL) {
            return self;
        }
        
        NSMutableArray * const arrA = /* [[NSMutableArray alloc]init];
        
        arrA = */ [arr objectAtIndex:0];    //arrA为日期
        
        NSMutableArray * const arrB = /* [[NSMutableArray alloc]init];
        
        arrB = */ [arr objectAtIndex:1];   //arrA为HR
        
        [self GetTrendAB:arrA :arrB];
        
        
        //        dataForPlot = [NSMutableArray arrayWithCapacity:7];
        int maxHR = -1;
        int minHR = 999;
        dayCount = arrB.count;
        
        
        for (int j = 0; j< arrB.count; j++) {
          
            float hr = [[arrB objectAtIndex:j] floatValue];
            if (hr >  maxHR) {
                maxHR = hr;
            }
            
            if (hr < minHR) {
                minHR = hr;
            }
        
        }
        
        NSLog(@"minHR = %d,maxHR = %d",minHR,maxHR);
        //获得数据源数组
        
        //获得最小的dateIndexNum 和最大的dateIndexNum
        int maxIndexNum = -1;
         int minIndexNum = 9;
        for (int i = 0 ; i<arrA.count; i++) {
             float dateIndex = [[arrA objectAtIndex:i] floatValue];
            if (dateIndex > maxIndexNum) {
                maxIndexNum = dateIndex;
            }
            if (dateIndex < minIndexNum) {
                minIndexNum = dateIndex;
            }
        }
        
          NSLog(@"minIndexNum = %d,maxIndexNum = %d",minIndexNum,maxIndexNum);
        
        dataForPlot = [[NSMutableArray alloc]init];
        
        _X1 = minIndexNum;
        _X2 = maxIndexNum;
        
        _Y1 = k_line*minIndexNum + b_line;
        _Y2 =  k_line*maxIndexNum + b_line;
        
        
        id x1_point = [NSNumber numberWithFloat:_X1];
        id x2_point = [NSNumber numberWithFloat:_X2];
        id y1_point = [NSNumber numberWithFloat: _Y1];
        id y2_point = [NSNumber numberWithFloat: _Y2];
        [dataForPlot addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:x1_point, @"x", y1_point, @"y", nil]];
         [dataForPlot addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:x2_point, @"x", y2_point, @"y", nil]];
        
        
        NSLog(@"dataForPlot = %@",dataForPlot);
        
        // Create graph from theme
        // 设置CPTXYGraph的主题    self> CPTGraphHostingView (from CPTXYGraph) >
        
        graph = [[CPTXYGraph alloc] initWithFrame:CGRectZero];
        //        CPTTheme *theme = [CPTTheme themeNamed:kCPTPlainWhiteTheme];
        //        [graph applyTheme:theme];
        
        
        CPTGraphHostingView *hostingView = [[CPTGraphHostingView alloc]initWithFrame:CGRectMake(0, 0, frame.size.width-0, frame.size.height-0)];
        [self addSubview:hostingView];
        graph.plotAreaFrame.borderLineStyle = nil;
        graph.plotAreaFrame.cornerRadius = 0.0f;
        //   hostingView.collapsesLayers = NO; // Setting to YES reduces GPU memory usage, but can slow drawing/scrolling
        hostingView.hostedGraph     = graph;
        
        graph.paddingLeft   = 0.0;
        graph.paddingTop    = 0.0;
        graph.paddingRight  = 3.0;
        graph.paddingBottom = 0.0;
        
        // Create a plot that uses the data source method
        
        
        // Setup plot space  设置CPTXYGraph的位置，坐标轴，X,Y可见范围
        CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
        
        
        plotSpace.allowsUserInteraction = NO;
        //横向留出下面20%+1的(最大最小值只差)距离 以免画出的点 只有一半；总的留出40%+2
        
        if (minHR == maxHR) {
            plotSpace.xRange                = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(-1.2) length:CPTDecimalFromFloat(7.7)];

            
            plotSpace.yRange                = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(minHR - 2 - 4/0.7*0.2) length:CPTDecimalFromFloat(4/0.7)];
            minHR = minHR - 2;
            maxHR = maxHR + 2;
            
        }
        
        else{
            
            plotSpace.xRange                = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(-1.2) length:CPTDecimalFromFloat(7.7)];
            plotSpace.yRange                = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(minHR - (maxHR-minHR)/0.7*0.2) length:CPTDecimalFromFloat((maxHR-minHR)/0.7)];
            
        }
      
        // Axes
        CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph.axisSet;
        CPTXYAxis *x          = axisSet.xAxis;
        
        x.minorTickLineStyle = nil;
        //        x.axisConstraints = [CPTConstraints constraintWithRelativeOffset:0.5];
        x.labelingPolicy = CPTAxisLabelingPolicyNone;
        CPTMutableLineStyle *myLineStyle = [CPTLineStyle lineStyle];
        myLineStyle.lineColor = [CPTColor clearColor];
        
        x.axisLineStyle =  myLineStyle;
        x.orthogonalCoordinateDecimal = CPTDecimalFromFloat(minHR - (maxHR-minHR)/0.7*0.1);
        
        
        
        
        CPTXYAxis *y = axisSet.yAxis;
        y.labelingPolicy = CPTAxisLabelingPolicyNone;
        y.axisLineStyle=CPTAxisLabelingPolicyNone;
        y.axisLineStyle =  myLineStyle;
//        y.majorIntervalLength         = CPTDecimalFromString(@"5");
        //    y.minorTicksPerInterval       = 5;
        y.orthogonalCoordinateDecimal = CPTDecimalFromString(@"-0.5");
        
        //趋势图直线
        
        dataSourceLinePlot4 = [[CPTScatterPlot alloc] init] ;
        CPTMutableLineStyle *lineStyle4 = [CPTMutableLineStyle lineStyle];
        
        CPTColor *trendYellow = [CPTColor colorWithComponentRed:56.0f/255 green:120/255.0 blue:226 alpha:1];
        
        lineStyle4.lineColor         = trendYellow;
        dataSourceLinePlot4.dataLineStyle = lineStyle4;
        dataSourceLinePlot4.identifier    = @"trend_line";
        dataSourceLinePlot4.dataSource    = self;
        [graph addPlot:dataSourceLinePlot4];
        
        
    }
    return self;
}

#pragma mark -
#pragma mark Plot Data Source Methods
-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot
{
    
    return 2;
}




-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index

{
    //    NSLog(@"numberForPlot----");
    
    NSString *key = (fieldEnum == CPTScatterPlotFieldX ? @"x" : @"y");
    NSNumber *num = [[dataForPlot objectAtIndex:index] valueForKey:key];
    
    // Green plot gets shifted above the blue
    if ( [(NSString *)plot.identifier isEqualToString:@"Green Plot"] ) {
        if ( fieldEnum == CPTScatterPlotFieldY ) {
            //                       num = [NSNumber numberWithDouble:[num doubleValue] + 1.0];
            // num = [NSNumber numberWithFloat:55+arc4random() % (10+1)];
            
            
        }
    }
    
    
    else if([(NSString *)plot.identifier isEqualToString:@"Red Plot"]){
        if ( fieldEnum == CPTScatterPlotFieldY ) {
            //               num = [NSNumber numberWithFloat:55+arc4random() % (10+1)];
        }
        
        
    }
    
    //    NSLog(@"num = %@",num);
    return num;
}


#pragma mark -
#pragma mark Axis Delegate Methods

-(BOOL)axis:(CPTAxis *)axis shouldUpdateAxisLabelsAtLocations:(NSSet *)locations
{
    
    
    static CPTTextStyle *positiveStyle = nil;
    static CPTTextStyle *negativeStyle = nil;
    
    //NSNumberFormatter *formatter = axis.labelFormatter;
    CGFloat labelOffset          = axis.labelOffset;
    NSDecimalNumber *zero        = [NSDecimalNumber zero];
    
    NSMutableSet *newLabels = [NSMutableSet set];
    
    for ( NSDecimalNumber *tickLocation in locations ) {
        CPTTextStyle *theLabelTextStyle;
        
        if ( [tickLocation isGreaterThanOrEqualTo:zero] ) {
            if ( !positiveStyle ) {
                CPTMutableTextStyle *newStyle = [axis.labelTextStyle mutableCopy];
                newStyle.color = [CPTColor clearColor];
                positiveStyle  = newStyle;
            }
            theLabelTextStyle = positiveStyle;
        }
        else {
            if ( !negativeStyle ) {
                CPTMutableTextStyle *newStyle = [axis.labelTextStyle mutableCopy];
                newStyle.color = [CPTColor clearColor];
                negativeStyle  = newStyle;
            }
            theLabelTextStyle = negativeStyle;
        }
        
        NSString *labelString       = [NSString stringWithFormat:@"%ld",(long)[tickLocation integerValue]];
        //  NSLog(@"labelString = %@",labelString);
        CPTTextLayer *newLabelLayer = [[CPTTextLayer alloc] initWithText:labelString style:theLabelTextStyle];
        
        CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithContentLayer:newLabelLayer];
        newLabel.tickLocation = tickLocation.decimalValue;
        newLabel.offset       = labelOffset;
        
        [newLabels addObject:newLabel];
    }
    axis.axisLabels = newLabels;
    return NO;
}

-(void) GetTrendAB :(NSMutableArray *)arrX :(NSMutableArray *)arrY {
    float sumX = 0;
    float sumY = 0;
    float avgX = 0;
    float avgY = 0;
    
    for(int i = 0; i < arrX.count; i++){
        sumX += [[arrX objectAtIndex:i]floatValue];
        sumY += [[arrY objectAtIndex:i]floatValue];
        
    }
    avgX = sumX / arrX.count;
    avgY = sumY / arrY.count;
    
    float sumLowBlock = 0;
    float sumHighBlock = 0;
    
    for(int j = 0; j < arrX.count; j++){
        sumLowBlock += ([[arrX objectAtIndex:j]floatValue] - avgX) * ([[arrX objectAtIndex:j]floatValue] - avgX);
        sumHighBlock += ([[arrX objectAtIndex:j]floatValue] - avgX)*([[arrY objectAtIndex:j]floatValue] - avgY);
    }
    
    float a = sumHighBlock / sumLowBlock;
    float b = avgY - (a * avgX);
    k_line = a;
    b_line = b;
    
    NSLog(@"a = %f ,b = %f ",a,b);
    
    
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end

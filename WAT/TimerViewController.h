//
//  TimerViewController.h
//  WAT
//
//  Created by neurosky on 5/26/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimerViewController : UIViewController{


//    __weak IBOutlet UIDatePicker *timerPicker;
    NSDate *timerSelectedTime;
    NSString  *timerStr;
    UIDatePicker *timerPicker;
}
- (IBAction)cancelBtnClick:(id)sender;
- (IBAction)saveBtnClick:(id)sender;

@end

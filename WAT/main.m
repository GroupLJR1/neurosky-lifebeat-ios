
//  main.m
//  WAT
//
//  Created by Julia on 4/16/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

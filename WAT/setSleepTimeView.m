//
//  setSleepTimeView.m
//  WAT
//
//  Created by peterwang on 2/11/15.
//  Copyright (c) 2015 Julia. All rights reserved.
//

#import "setSleepTimeView.h"
#import "CommonValues.h"
#import "Constant.h"

@implementation setSleepTimeView

- (IBAction)save:(id)sender
{
    if (self.saveBlock)
    {
        self.saveBlock();
    }

    
}


- (IBAction)cancel:(id)sender
{
    if (self.cancelBlock)
    {
        self.cancelBlock();
    }
}


- (IBAction)setSleepStartTime:(id)sender
{
    NSLog(@"start time is setting");
    
    NSDate *pickerTime=_startTimePicker.date;
    
    [self stringFromDate:pickerTime startTimeSetFlag:YES];
    

}

- (IBAction)setSleepEndTime:(id)sender
{
    NSLog(@"End time is setting");
    
    NSDate *pickerTime=_endTimePicker.date;
    
    [self stringFromDate:pickerTime startTimeSetFlag:NO];

//    NSLog(@"End sleep time set up completely %@",_endSleepTime);
}


-(void) adaptIOSVersion
{
    
    self.frame = CGRectMake(0, [CommonValues offsetHeight4DiffiOS], 320, 568-20);
    
    NSString *tmpStartSleepTimeString = [self dateStringFromUserDefault:YES];
    NSDate *pickerTimeStart=[self dateFromString:tmpStartSleepTimeString];
    
    _startTimePicker.date=pickerTimeStart;
    [self stringFromDate:pickerTimeStart startTimeSetFlag:YES];
    
    NSString *tmpEndSleepTimeString = [self dateStringFromUserDefault:NO];
    NSDate *pickerTimeEnd=[self dateFromString:tmpEndSleepTimeString];

    _endTimePicker.date=pickerTimeEnd;
    [self stringFromDate:pickerTimeEnd startTimeSetFlag:NO];
    
}

-(void) stringFromDate:(NSDate *)date startTimeSetFlag:(BOOL) flag
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    [dateFormatter setTimeZone:[[CommonValues timeDateFormatter:DateFormatterDate] timeZone] ];
    if (flag)
    {
        _startSleepTimeString = [dateFormatter stringFromDate:date];
        NSLog(@"start sleep time set up completely %@",_startSleepTimeString);

    }
    else
    {
        _endSleepTimeString = [dateFormatter stringFromDate:date];
        NSLog(@"end sleep time set up completely %@",_endSleepTimeString);

    }
    
}


-(NSDate *) dateFromString:(NSString*) string
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    [dateFormatter setTimeZone:[[CommonValues timeDateFormatter:DateFormatterDate] timeZone] ];
    NSDate *date=[dateFormatter dateFromString:string];
    return date;
}

-(NSString*) dateStringFromUserDefault:(BOOL)startTimeStringFlag
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    if (startTimeStringFlag)
    {
        NSString *startSleepTimeString = [userDefault objectForKey:AUTO_SLEEP_START_TIME];
        NSLog(@"start sleep time string is %@",startSleepTimeString);
        return startSleepTimeString;
    }
    else
    {
        NSString *endSleepTimeString = [userDefault objectForKey:AUTO_SLEEP_END_TIME];
        NSLog(@"end sleep time string is %@",endSleepTimeString);
        return endSleepTimeString;
    }
    
    
}

@end

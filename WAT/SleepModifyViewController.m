//
//  SleepModifyViewController.m
//  WAT
//
//  Created by neurosky on 12/29/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import "SleepModifyViewController.h"
#import "StringOperation.h"

@interface SleepModifyViewController ()

@end

@implementation SleepModifyViewController
-(NSUInteger)supportedInterfaceOrientations{
    __LOG_METHOD_START;
    UIInterfaceOrientation const orientation = UIInterfaceOrientationPortrait;
    __LOG_METHOD_NOTE_STR_INT( "current value", (int)orientation );
    __LOG_METHOD_END;
    return orientation;
}

- (BOOL)shouldAutorotate
{
    __LOG_METHOD_START;
    __LOG_METHOD_END;
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    __LOG_METHOD_START;
    UIInterfaceOrientation const orientation = UIInterfaceOrientationPortrait;
    __LOG_METHOD_NOTE_STR_INT( "current value", (int)orientation );
    __LOG_METHOD_END;
    return orientation;
}
- (void)viewDidLoad {
    __LOG_METHOD_START;
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initView];
    [self initVariables];
    [self setValues];
    __LOG_METHOD_END;
}

-(void)viewWillAppear:(BOOL)animated
{
    __LOG_METHOD_START;
    [ super viewWillAppear: animated ];
    __LOG_METHOD_END;

}

- (void)didReceiveMemoryWarning {
    __LOG_METHOD_START;
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    __LOG_METHOD_END;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)initVariables{
    __LOG_METHOD_START;
    sleepTimesArr = [[NSMutableArray alloc] init];
    dbTimeArray = [[NSMutableArray alloc] init];
    db = [CommonValues getMyDB];
    [CommonValues setSleepStartEndTimeBackFlag:0];
    [CommonValues setSleepStartEndTimeEnterFlag:0];
    [CommonValues setModifyBackFlag:YES];
    __LOG_METHOD_END;

}

-(void) initView{
    __LOG_METHOD_START;
    myWholeView.frame = CGRectMake(0, [CommonValues offsetHeight4DiffiOS], 320, 568-20);
    bandDisplayLabel.font = [UIFont systemFontOfSize:17.0];
    startLabel.font = [UIFont systemFontOfSize:17.0];
    stopLabel.font = [UIFont systemFontOfSize:17.0];
    select_img.userInteractionEnabled = YES;
    UITapGestureRecognizer *select_img_single_tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectImageClick)];
 
    [select_img addGestureRecognizer:select_img_single_tap];
    myTableView.hidden = YES;
    open_img.userInteractionEnabled = YES;
    UITapGestureRecognizer *open_img_single_tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(openImageClick)];
    [open_img addGestureRecognizer:open_img_single_tap];
    backgroundImg.userInteractionEnabled = YES;
    UITapGestureRecognizer *background_img_single_tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(backgroundImageClick)];
    [backgroundImg addGestureRecognizer:background_img_single_tap];
    startTimeLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *start_sleep_single_tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(startLabelClick)];
    [startTimeLabel addGestureRecognizer:start_sleep_single_tap];
    stopLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *stop_sleep_single_tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(stopLabelClick)];
    [stopLabel addGestureRecognizer:stop_sleep_single_tap];
    __LOG_METHOD_END;

}

-(void)stopLabelClick{
    __LOG_METHOD_START;
    [CommonValues setSleepStartEndTimeEnterFlag:1];
    SleepStartEndViewController *ssevc = [[SleepStartEndViewController alloc]init];
    NSString *stringFromStopLabel=stopLabel.text;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    [dateFormatter setTimeZone:[[CommonValues timeDateFormatter:DateFormatterDate] timeZone] ];
    NSDate *date=[dateFormatter dateFromString:stringFromStopLabel];
    
    ssevc.pickDateDisplay=date;
    
    __weak SleepStartEndViewController *weakSleepStartEndViewController=ssevc;
    ssevc.saveBlock=^{
        
        stopLabel.text= weakSleepStartEndViewController.pickDateEndString;
        [weakSleepStartEndViewController dismissViewControllerAnimated:YES completion:nil];
    };
    
    
    [self presentViewController:ssevc animated:YES completion:nil];
    __LOG_METHOD_END;

}
-(void)startLabelClick{
    __LOG_METHOD_START;
    [CommonValues setSleepStartEndTimeEnterFlag:0];

    SleepStartEndViewController *ssevc = [[SleepStartEndViewController alloc]init];
    
    NSString *stringFromStartLabel=startLabel.text;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    [dateFormatter setTimeZone:[[CommonValues timeDateFormatter:DateFormatterDate] timeZone] ];
    NSDate *date=[dateFormatter dateFromString:stringFromStartLabel];
    
    ssevc.pickDateDisplay=date;
   
    __weak SleepStartEndViewController *weakSleepStartEndViewController=ssevc;
    ssevc.saveBlock=^{
        
        startLabel.text= weakSleepStartEndViewController.pickDateStartString;
        [weakSleepStartEndViewController dismissViewControllerAnimated:YES completion:nil];
    };
    
    
    [self presentViewController:ssevc animated:YES completion:nil];
    __LOG_METHOD_END;

}

-(void)backgroundImageClick{
    __LOG_METHOD_START;
    if (!myTableView.hidden) {
        myTableView.hidden = YES;
    }
    __LOG_METHOD_END;

}

-(void)openImageClick{
    __LOG_METHOD_START;
    if (myTableView.hidden) {
        myTableView.hidden = NO;
    }
    __LOG_METHOD_END;

}
-(void)selectImageClick {
    __LOG_METHOD_START;
    if (selectedFlag) {
        if (sleepTimesArr.count > 0) {
            select_img.image = [UIImage imageNamed:@"modify_select_1"];
            selectedFlag = NO;
        }
    }
    else{
        select_img.image = [UIImage imageNamed:@"modify_select_2"];
         selectedFlag = YES;
    }
    __LOG_METHOD_END;

}

- (void)setValues{
    __LOG_METHOD_START;
    //顶部日期显示
    dbTimeArray =  [CommonValues getSleepTimeArr :db];
    _dateFromSleep = [dbTimeArray objectAtIndex:[CommonValues getActivityIndex]];
    NSLog(@"setValues  dateFromSleep  = %@",_dateFromSleep);
    NSDate *todayDate = [[CommonValues timeDateFormatter:DateFormatterDate] dateFromString:_dateFromSleep];
   
    NSDate *yesterdayDate = [[NSDate alloc ] initWithTimeInterval:-24*60*60 sinceDate:todayDate];
    
    /*NSString *dateFromSleepYester =*/ [[CommonValues timeDateFormatter:DateFormatterDate] stringFromDate:yesterdayDate];
    
    NSString *formattedToday = [[CommonValues timeDateFormatter:DateFormatterDateSlash] stringFromDate:todayDate];
    NSString *formattedYesterday = [[CommonValues timeDateFormatter:DateFormatterDateSlash] stringFromDate:yesterdayDate];
    dateLabel.text = [NSString stringWithFormat:@"%@ - %@",formattedYesterday,formattedToday];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
   //Band 选中的 sleep时间段
    sleepTimesArr = [CommonValues calculateSleepStartEndTimeArr:_dateFromSleep :db];
    if (sleepTimesArr.count > 0) {
//        NSMutableArray *firstArr = [sleepTimesArr objectAtIndex:0];
        //默认显示最后一次睡眠
        NSArray *lastArr = [sleepTimesArr objectAtIndex:(sleepTimesArr.count - 1)];
        NSString *start = [lastArr objectAtIndex:0];
        NSString *end = [lastArr objectAtIndex:1];
        NSDate *startDate = [[CommonValues timeDateFormatter:DateFormatterTimeColon] dateFromString:start];
         NSDate *endDate = [[CommonValues timeDateFormatter:DateFormatterTimeColon] dateFromString:end];
        bandDisplayLabel.text = [NSString stringWithFormat:@"%@-%@",[[CommonValues timeDateFormatter:DateFormatterTimeColonAMPM] stringFromDate:startDate],[[CommonValues timeDateFormatter:DateFormatterTimeColonAMPM] stringFromDate:endDate]];
        NSString *testStr =  [[CommonValues timeDateFormatter:DateFormatterTimeColonAMPM] stringFromDate:endDate];
        NSLog(@"endDate ====== %@",endDate);
        NSLog(@"testStr ====== %@",testStr);
        myTableView.frame =  CGRectMake(8, 185, 304, 44*sleepTimesArr.count);
        myTableView.delegate = self;
        myTableView.dataSource = self;
        [userDefault setObject:start forKey:BAND_SLEEP_START_TIME];
        [userDefault setObject:end forKey:BAND_SLEEP_END_TIME];
        
    }
//    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//    BOOL isSetInApp = [userDefault boolForKey:SET_SLEEP_IN_APP];
//    if (isSetInApp) {
    else{
        //band 没有手动切换模式，只能以app设置时间为准 ；默认为10：00-7：00
        select_img.image = [UIImage imageNamed:@"modify_select_2"];
        selectedFlag = YES;
        bandDisplayLabel.text = @"NULL";
        open_img.hidden = YES;
    }
//    }
  
    NSString *startSleepTimeString = [userDefault objectForKey:AUTO_SLEEP_START_TIME];
    NSString *endSleepTimeString = [userDefault objectForKey:AUTO_SLEEP_END_TIME];
    NSLog(@"peter start sleep time %@",startSleepTimeString);
    NSLog(@"peter end sleep time %@",endSleepTimeString);

    startLabel.text = startSleepTimeString;
    stopLabel.text = endSleepTimeString;
    __LOG_METHOD_END;

}

- (IBAction)backClick:(id)sender
{
    __LOG_METHOD_START;
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];

    if (selectedFlag) {
        [userDefault setBool:YES forKey:SET_SLEEP_IN_APP];
    }
    else{
         [userDefault setBool:NO forKey:SET_SLEEP_IN_APP];
    }
    
    NSString *appSleepStartTime = startLabel.text;
    NSString *appSleepEndTime  = stopLabel.text;
    
    
    NSString *appSleepStartDate  = [StringOperation calculateStartTime:appSleepStartTime stopTime:appSleepEndTime date:_dateFromSleep];
    NSString *appSleepEndDate   = [NSString stringWithFormat:@"%@ %@:00",_dateFromSleep,appSleepEndTime];

    NSLog(@"peter back sleep start date %@",appSleepStartDate);
    NSLog(@"peter back sleep end date %@",appSleepEndDate);
   
    [userDefault setObject:appSleepStartDate forKey:APP_SLEEP_START_TIME];
    [userDefault setObject:appSleepEndDate forKey:APP_SLEEP_END_TIME];
    
    [userDefault synchronize];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    __LOG_METHOD_END;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    __LOG_METHOD_START;
    NSInteger const val = sleepTimesArr.count;
    __LOG_METHOD_NOTE_STR_INT( "elements in array", (int)val );
    __LOG_METHOD_END;
    return val;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    __LOG_METHOD_START;
    NSLog(@"cellForRowAtIndexPath indexPath row = %ld", (long)indexPath.row);
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
        
    {
       cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    NSArray *timeArr = [sleepTimesArr objectAtIndex:indexPath.row];
    NSString *formatterStr = [NSString stringWithFormat:@"%@-%@",timeArr[0],timeArr[1]];
    cell.textLabel.font = [UIFont systemFontOfSize:13.0];
    [[cell textLabel] setText:formatterStr];
    
    __LOG_METHOD_END;
    return cell;

}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    __LOG_METHOD_START;
       NSLog(@"didSelectRowAtIndexPath indexPath = %ld",(long)indexPath.row);
    NSArray *timeArr = [sleepTimesArr objectAtIndex:indexPath.row];
//    NSString *formatterStr = [NSString stringWithFormat:@"%@-%@",timeArr[0],timeArr[1]];
    NSDate *startDate = [[CommonValues timeDateFormatter:DateFormatterTimeColon] dateFromString:timeArr[0]];
    NSDate *endDate = [[CommonValues timeDateFormatter:DateFormatterTimeColon] dateFromString:timeArr[1]];
    bandDisplayLabel.text = [NSString stringWithFormat:@"%@-%@",[[CommonValues timeDateFormatter:DateFormatterTimeColonAMPM] stringFromDate:startDate],[[CommonValues timeDateFormatter:DateFormatterTimeColonAMPM] stringFromDate:endDate]];
//    bandDisplayLabel.text = formatterStr;
    
    myTableView.hidden = YES;
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:timeArr[0] forKey:BAND_SLEEP_START_TIME];
    [userDefault setObject:timeArr[1] forKey:BAND_SLEEP_END_TIME];
    [userDefault synchronize];
    __LOG_METHOD_END;
}
@end

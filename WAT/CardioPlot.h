//
//  CardioPlot.h
//  WAT
//
//  Created by NeuroSky on 2/10/15.
//  Copyright (c) 2015 Julia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommonValues.h"

@interface CardioPlot : NSObject

- (void)controlMoodChangeForIphone5S : (int) moodValue btn : (UIButton *) btn;
- (void)controlMoodChangeForIphone4S : (int) moodValue btn : (UIButton *) btn;
- (NSString *)ekg2Path;
- (void)gestureAccording2Point:(CGPoint )point uiView : (UIView *)uiview btn :(UIButton *)but;
@end

//
//  SetGoalViewController.h
//  WAT
//
//  Created by neurosky on 1/16/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TGBleManager.h"
#import "TGBleManagerDelegate.h"
#import "ZSYPopoverListView.h"
#import "SVProgressHUD.h"

@interface SetGoalViewController : UIViewController<UITextFieldDelegate,TGBleManagerDelegate,ZSYPopoverListDatasource,ZSYPopoverListDelegate>{

    NSInteger distance;
    NSInteger step;
    NSInteger calorie;
    float sleep_;
    float active_time;
    NSInteger offsetHeight;
   
    ZSYPopoverListView *listView ;
    NSMutableArray *devicesArray;
    NSMutableArray *mfgDataArray;
    NSMutableArray *rssiArray;
    NSMutableArray *candIDArray;
    NSMutableArray *devicesIDArray;
    
    NSString *readyToSaveDeviceName;
    NSString *readyToSaveDeviceID;
    
    NSTimer *timeOutTimer;
    BOOL firstEnterCandiDel;
    
    NSTimer *showListTimer;
    BOOL lostBeforeMainOperation ;
    __weak IBOutlet UILabel *titleLabel;
    
    __weak IBOutlet UIView *myWholeGoalView;
    UIAlertController *alertController;
}
- (IBAction)backClick:(id)sender;
@property (nonatomic, retain) NSIndexPath *selectedIndexPath;
@property (weak, nonatomic) IBOutlet UITextField *step_tv;
@property (weak, nonatomic) IBOutlet UITextField *calo_tv;
@property (weak, nonatomic) IBOutlet UITextField *dis_tv;
- (IBAction)bgTouch:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *sleep_tv;
- (IBAction)saveBtnClick:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *active_tv;

@end

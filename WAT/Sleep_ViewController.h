//
//  Sleep_ViewController.h
//  WAT
//
//  Created by neurosky on 3/19/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"
#import <sqlite3.h>
#import "TGBleManager.h"
#import "TGLibEKG.h"
#import "Sleep.h"
#import "TGLibEKGdelegate.h"
#import "TGBleManagerDelegate.h"
#import "CommonValues.h"
#import "Constant.h"
#import "SleepModifyViewController.h"
@interface Sleep_ViewController : UIViewController{
   
    __weak IBOutlet UILabel *dateLabel;
    
    UILabel *firstLabel;
    UILabel *secondLabel;
    UILabel *thirdLabel;
    __weak IBOutlet UILabel *titleLabel;
    
    __weak IBOutlet UIView *myWholeView;
     UILabel *wake_count_label;
     UILabel *awakeLabel;
    
    
     UILabel *deepLabel;
     UILabel *beforeLabel;
     UILabel *efficiencyLabel;
     UILabel *light_sleep_label;
    int  sleepIndex;
    CPTXYGraph *barChart;
    NSArray *SleepArray;
    NSMutableArray  *dbTimeArray;
    sqlite3  *db;
    NSMutableArray *sleepPhaseArray;
    NSMutableArray *sleepTimeArray;
    UIFont *font;
    NSDateFormatter *df;
    NSDateFormatter *local_solid_df;
    
    NSString *database_path;
    //   TGLibEKG * tglibEKG;
    
    NSMutableArray *sleepGraphDataSouceArr;
    int  lastIndex;
    float  screenHeight;
    CPTGraphHostingView  *hv_4S;
    UIScrollView *my_scrollview4S;
    
    UIButton *leftBtn;
    UIButton *rightBtn;

    IBOutlet UIView *headView;
    int ystLastSleepPhase;
    NSString *ystLastSleepTime;
    NSString *gStartTime;
    NSString *gEndTime;
    Sleep *sleep;
}


- (IBAction)right_click:(id)sender;
- (IBAction)left_click:(id)sender;
- (IBAction)back_click:(id)sender;
- (IBAction)modifyClick:(id)sender;


@end

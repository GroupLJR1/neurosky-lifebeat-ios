//
//  DashboardModel.m
//  WAT
//
//  Created by NeuroSky on 1/27/15.
//  Copyright (c) 2015 Julia. All rights reserved.
//

#import "Dashboard.h"

#import "TGBleEmulator.h"

@implementation Dashboard


- (void)deleteSyncLogBeforeThisDate:(NSString *)dateStr{
    //delete the sync logs except today's log.
    NSArray *arr =  [FileOperation getFileArr:@"/SyncEvent"];
    NSString *combinedStr = [NSString stringWithFormat:@"synclog_%@",dateStr];
    //synclog_20150201.txt
    for (int i = 0; i<arr.count; i++) {
        NSString *syncLogFileName = arr[i];
        //        NSLog(@"SyncEvent  documentDir path = %@",syncLogFileName);
        if([syncLogFileName  compare:combinedStr] < 0 ){
            NSString *filePath =  [@"/SyncEvent" stringByAppendingPathComponent:syncLogFileName];
            [FileOperation deleteFile:filePath];
            
        }
        
    }
    NSLog(@"deleted sync log files BeforeThisDate %@",dateStr);
    
}

- (void)deleteAllSyncData{
//    return;
    //delete all the data,inclue sync ecg,real time ecg,ped,sleep,fat data.
    [FileOperation deleteFolder:[NSString stringWithFormat:@"/%@",RealtimeECGPath]];
    NSLog(@"deleted  ecg realtime floder");
    
    [FileOperation deleteFolder:@"/EKGData"];
    NSLog(@"deleted sync ecg floder");
    
    [FileOperation deleteFile:@"/PEDData/SleepLog.txt"];
    NSLog(@"deleted sync sleep.txt");
    
    [FileOperation deleteFile:@"/PEDData/Pedometer2Data.txt"];
    NSLog(@"deleted sync Pedometer2Data.txt");
    
    [FileOperation deleteFile:@"/FATData"];
    NSLog(@"deleted sync fat floder");
    
    
    [self deleteSyncLogBeforeThisDate:[self dateOfToday]];
    
}

- (NSString *)dateOfToday{
//return the DateStr like "20150201"
    NSDate *now = [NSDate date];
    NSString *dateStr = [[CommonValues timeDateFormatter:DateFormatterDateNoneSeparater] stringFromDate:now];
    return dateStr;
}

- (void)insertSleepData2SqilteWithTime: (NSString *)time withPhase :(TGsleepPhaseType) phase withInitCode:(TGsleepInitReason) code database :(sqlite3 *)mydb{
    NSString *sql1 = [NSString stringWithFormat:
                      @"INSERT INTO SLEEP ('time', 'sleep_phase','init_code') VALUES ('%@', '%lu','%lu')",
                      time,(unsigned long)phase,(unsigned long)code];
    //    NSLog(@"insert to Sleep ,sql1 = %@ ",sql1);
    [CommonValues execSql:sql1 :mydb];
}

- (void)userProfileWriteToFile{
    NSTimeZone *fromzone = [NSTimeZone systemTimeZone];
    //    [defaults setObject:fromzone forKey:@"TimeZone"];
    
    NSInteger timeZone =   [fromzone secondsFromGMT]/3600;
    NSLog(@"timeZone = %ld", (long)timeZone);
    
    NSLog(@"USER_IS_FEMALE = %d,USER_AGE = %ld,USER_WEIGHT = %ld, USER_HEIGHT = %ld,USER_WALKING_STEP_LENGH = %ld, USER_RUNNING_STEP_LENGH = %ld, USER_TIME_ZONE = %ld", [[TGBleManager sharedTGBleManager]isFemale], (long)[[TGBleManager sharedTGBleManager] age], (long)[[TGBleManager sharedTGBleManager]weight],(long)[[TGBleManager sharedTGBleManager]height],(long)[[TGBleManager sharedTGBleManager]walkingStepLength],(long)[[TGBleManager sharedTGBleManager]runningStepLength],(long)timeZone);
    
    NSString *temp = [NSString stringWithFormat:@"gender  %d\nage  %ld\nheight  %ld\nweight  %ld\nwalk_step_size  %ld\nrun_step_size  %ld\ntime_zone  %ld",[[TGBleManager sharedTGBleManager]isFemale], (long)[[TGBleManager sharedTGBleManager] age], (long)[[TGBleManager sharedTGBleManager]height],(long)[[TGBleManager sharedTGBleManager]weight],(long)[[TGBleManager sharedTGBleManager]walkingStepLength],(long)[[TGBleManager sharedTGBleManager]runningStepLength],(long)timeZone];
    
    
    //    [NSString stringWithFormat:@"%d",[[defaults objectForKey:USER_WEIGHT]integerValue]*10];
    
    NSString *documentsDirectory = [CommonValues getDocumentPath];
    //创建文件管理器
    NSFileManager *myFileManager = [NSFileManager defaultManager];
    // 写入数据：
    //获取文件路径
    NSString *pathPro = [documentsDirectory stringByAppendingPathComponent:@"userPro.txt"];
    
    [myFileManager removeItemAtPath:pathPro error:nil];
    
    NSMutableData *writer = [[NSMutableData alloc] init];
    [writer appendData:[temp dataUsingEncoding:NSUTF8StringEncoding]];
    [writer writeToFile:pathPro atomically:YES];
}

- (NSString *)getFomattedTimeStr{
    
    NSString * last14SNStr =   [StringOperation getLast14SN];
    //    NSLog(@"NSTommy_test_getTimeStr");
    NSDate *date = [NSDate date];
    NSDateFormatter *f = [[NSDateFormatter alloc]init];
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [f setTimeZone:gmt];
    [f setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"];
    NSString*  strDate =  [f stringFromDate:date];
    //    NSLog(@"strDate = %@",strDate);
    NSString * snWithDate = [NSString stringWithFormat:@"%@-%@",last14SNStr,strDate];
    NSLog(@"snWithDate = %@",snWithDate);
    return snWithDate;
    
}

- (NSMutableURLRequest *) upload2PHPServer:(NSString*)txtfile type:(NSString*)dataType{
    //    NSLog(@"upload2PHPServer======");
    
    NSString * copyTimeFromECG = @"";
    
    NSString *url = @"";
    if ([dataType isEqualToString:UPLOAD_TYPE_ECG]) {
        url = @"http://54.218.40.3/httppost_ecg_single_hr_hrv.php";
        //         url = @"http://54.218.40.3/httppost_ecg_optimized_2.php";
        //                  url = @"http://192.168.1.19/BigBioData/httppost_ecg.php";
        //
        //        copyTimeFromECG = [self getTimeStr:uploadTypeECG];
        //        copyTimeFromECG = [self NSTommy_test_getTimeStr:nil];
        // use ecg recording time .
        //NS_Tommy_test
        //        NSString *txtfile = @"/var/mobile/Applications/7DEA77C8-2267-4F7B-8199-4694D2F89103/Documents/TG_log/Console/2014_04_24_02_40_11.txt";
        
        copyTimeFromECG = [StringOperation path2TimeStr:txtfile];
        
        
    }
    
    else if ([dataType isEqualToString:UPLOAD_TYPE_PED]) {
        url = @"http://54.218.40.3/httppost_ped.php";
        
        //       url = @"http://54.218.40.3/httppost_ped_optimized.php";
        
        //              url = @"http://54.218.40.3/httppost_ped_test.php";
        
        //         url = @"http://192.168.1.19/BigBioData/httppost_ped.php";
        //        copyTimeFromECG = [self getTimeStr:uploadTypePED];
        copyTimeFromECG = [self getFomattedTimeStr];
        
    }
    
    
    else if ([dataType isEqualToString:UPLOAD_TYPE_SLEEP]) {
        url = @"http://54.218.40.3/httppost_sleep.php";
        
        //         url = @"http://54.218.40.3/httppost_sleep_old.php";
        //
        //        url = @"http://54.218.40.3/httppost_sleep_optimized.php";
        //
        //        url = @"http://54.218.40.3/httppost_sleep_chear.php";
        
        //        url = @"http://192.168.1.19/BigBioData/httppost_sleep.php";
        //        copyTimeFromECG = [self getTimeStr:uploadTypeECG];
        copyTimeFromECG = [self getFomattedTimeStr];
        
    }
    
    else if ([dataType isEqualToString:UPLOAD_TYPE_FAT]) {
        url = @"http://54.218.40.3/httppost_fat.php";
        
        //        url = @"http://192.168.1.19/BigBioData/httppost_fat.php";
        //        copyTimeFromECG = [self getTimeStr:uploadTypeECG];
        copyTimeFromECG = [self getFomattedTimeStr];
        
    }
    else if ([dataType isEqualToString:UPLOAD_TYPE_SYNC_LOG]) {
        url = @"http://54.218.40.3/httppost_synclog.php";
        
        //        url = @"http://192.168.1.19/BigBioData/httppost_fat.php";
        //        copyTimeFromECG = [self getTimeStr:uploadTypeECG];
        copyTimeFromECG = [self getFomattedTimeStr];
        
    }
    
    else{
        
        url = @"http://54.218.40.3/httppost_userpro.php";
        
        //        url = @"http://192.168.1.19/BigBioData/httppost_userpro.php";
        //        copyTimeFromECG = [self getTimeStr:uploadTypeECG];
        copyTimeFromECG = [self getFomattedTimeStr];
        
        
    }
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"POST"];
    
    [request setTimeoutInterval:60*5];
    
    NSMutableData *body = [NSMutableData data];
    
    NSString *boundary = [NSString stringWithFormat:@"NeuroSky128"];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    // Another text parameter
    NSString *param2 = [NSString stringWithContentsOfFile:txtfile encoding:NSUTF8StringEncoding error:nil];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",dataType] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *stored_name = [defaults objectForKey:USER_NAME];
    [body appendData:[[NSString stringWithFormat:@"%@-%@\r\n",[CommonValues getDeviceID],stored_name] dataUsingEncoding:NSUTF8StringEncoding]];
    
    //copyTimeFromECG = [self getTimeStr:dataType];
    //     NSLog(@"copyTimeFromECG = %@ ,dataType = %@ ,getDeviceID = %@ ,stored_name = %@ ,getVersionNumbers = %@",copyTimeFromECG,dataType,[self getDeviceID],stored_name,[self getVersionNumbers]);
    if ([dataType isEqualToString:UPLOAD_TYPE_SYNC_LOG]) {
        
        [body appendData:[[NSString stringWithFormat:@"%@\r\n",[self dateStrFromlogFilePath:txtfile]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    else{
    [body appendData:[[NSString stringWithFormat:@"%@-%@\r\n",copyTimeFromECG,[StringOperation getVersionNumbers]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    
    //add version numbers
    //    [body appendData:[[NSString stringWithFormat:@"%@\r\n",copyTimeFromECG] dataUsingEncoding:NSUTF8StringEncoding]];
    
    if ([dataType isEqualToString:UPLOAD_TYPE_ECG]){
        
        int sampleRateFromFile = 0 ;
        NSArray *lineStr =  [param2 componentsSeparatedByString:@"\n"];
        if (lineStr.count >= 3) {
            NSString *SR =  lineStr[2];
            //            NSLog(@"SR = %@",SR);
            NSString *subStr =  [SR substringFromIndex:6];
            //              NSLog(@"subStr = %@",subStr);
            sampleRateFromFile = [subStr intValue];
        }
        
        NSLog(@"upload2PHPServer  sampleRateFromFile = %d",sampleRateFromFile);
        
        if  (sampleRateFromFile == 256) {
            
            
            [body appendData:[[NSString stringWithFormat:@"%@\r\n",@"1"] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        else if(sampleRateFromFile == 512){
            [body appendData:[[NSString stringWithFormat:@"%@\r\n",@"0"] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        else{
            [body appendData:[[NSString stringWithFormat:@"%@\r\n",@"-1"] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
    }
    //[body appendData:[[NSString stringWithString:param2] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[param2 dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // close form
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    //   NSData *testdd =   [request HTTPBody];
    //    if ([dataType isEqualToString:uploadTypeSLEEP]){
    //
//            NSLog(@"body = %@",[[NSString alloc] initWithData:body  encoding:NSUTF8StringEncoding]);
    //        NSLog(@"HTTPBody = %@",body);
    //    }
    
    
    return request;
    
    //     NSLog(@"upload2PHPServer======over=======");
    
}



- (BOOL)returnFlagWithURLRequest:(NSMutableURLRequest *)request{
    
    NSError *error = nil;
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error];
    
    if (returnData == nil) {
        NSLog(@"send request failed: %@", error);
        return NO;
    }
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    NSLog(@"returnString = %@",returnString);
    return YES;
    
}


- (BOOL)runECG{
    NSString *documentDir2 = [[CommonValues getDocumentPath] stringByAppendingPathComponent:@"/EKGData"];
    NSArray *arr =  [FileOperation getFileArr:@"/EKGData"];
    for (int i = 0; i<arr.count; i++) {
        NSLog(@"EKGData  documentDir2 path 111= %@",[documentDir2 stringByAppendingPathComponent:arr[i]]);
        NSString *filePath =  [documentDir2 stringByAppendingPathComponent:arr[i]];
        NSFileManager *fm = [[NSFileManager alloc]init];
        NSData *data = [fm contentsAtPath:filePath];
        if (data.length > 0) {
            //            [self upload2PHPServer:[documentDir2 stringByAppendingPathComponent:arr[i]] type:uploadTypeECG];
            BOOL tempFlag = [self returnFlagWithURLRequest:[self upload2PHPServer:[documentDir2 stringByAppendingPathComponent:arr[i]] type:UPLOAD_TYPE_ECG]];
            if (!tempFlag) {
                return NO;
            }
            
        }
        
    }
    return YES;
}

- (BOOL)runECG_Realtime{
    NSLog(@"runECG_Realtime thread");
    
    NSString *documentDir2 = [[CommonValues getDocumentPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",RealtimeECGPath]];
    NSArray *arr =  [FileOperation getFileArr:[NSString stringWithFormat:@"/%@",RealtimeECGPath]];
    for (int i = 0; i<arr.count; i++) {
        NSLog(@"Realtime EKGData  documentDir2 path 111= %@",[documentDir2 stringByAppendingPathComponent:arr[i]]);
        NSString *filePath =  [documentDir2 stringByAppendingPathComponent:arr[i]];
        NSFileManager *fm = [[NSFileManager alloc]init];
        NSData *data = [fm contentsAtPath:filePath];
        if (data.length > 0) {
            //            [self upload2PHPServer:[documentDir2 stringByAppendingPathComponent:arr[i]] type:uploadTypeECG];
            BOOL tempFlag = [self returnFlagWithURLRequest:[self upload2PHPServer:[documentDir2 stringByAppendingPathComponent:arr[i]] type:UPLOAD_TYPE_ECG]];
            
            if (!tempFlag) {
                return NO;
            }
            
            
        }
    }
    return YES;
}


- (BOOL)runUserPro{
    NSLog(@"runUserPro thread");
    NSString *userPorPath = [[CommonValues getDocumentPath] stringByAppendingPathComponent:@"/userPro.txt"];
    NSFileManager *fm = [[NSFileManager alloc]init];
    NSData *data = [fm contentsAtPath:userPorPath];
    if (data.length > 0) {
        
        BOOL tempFlag = [self returnFlagWithURLRequest:[self upload2PHPServer:userPorPath type:UPLOAD_TYPE_PRO]];
        if (!tempFlag) {
            return NO;
        }
    }
    return YES;
}

- (BOOL)runSleep{
    NSLog(@"runSleep Thread start");
    NSString *documentDir2 = [[CommonValues getDocumentPath] stringByAppendingPathComponent:@"/PEDData"];
    NSArray *arr =  [FileOperation getFileArr:@"/PEDData"];
    NSLog(@"arr = %@",arr);
    for (int i = 0; i<arr.count; i++) {
        NSLog(@"SleepData  documentDir path 111= %@",[documentDir2 stringByAppendingPathComponent:arr[i]]);
        if([arr[i]  isEqualToString:@"SleepLog.txt"]){
            NSString *filePath =  [documentDir2 stringByAppendingPathComponent:arr[i]];
            NSFileManager *fm = [[NSFileManager alloc]init];
            NSData *data = [fm contentsAtPath:filePath];
            if (data.length > 0) {
                BOOL tempFlag = [self returnFlagWithURLRequest:[self upload2PHPServer:filePath type:UPLOAD_TYPE_SLEEP]];
                if (!tempFlag) {
                    return NO;
                }
            }
            
        }
        
    }
    return YES;
}

- (BOOL)runFat{
    NSLog(@"runFat thread");
    NSString *documentDir = [[CommonValues getDocumentPath] stringByAppendingPathComponent:@"/FATData"];
    NSArray *arr =  [FileOperation getFileArr:@"/FATData"];
    NSLog(@"arr = %@",arr);
    for (int i = 0; i<arr.count; i++) {
        NSLog(@"FatData  documentDir2 path 111= %@",[documentDir stringByAppendingPathComponent:arr[i]]);
        NSString *filePath =  [documentDir stringByAppendingPathComponent:arr[i]];
        NSFileManager *fm = [[NSFileManager alloc]init];
        NSData *data = [fm contentsAtPath:filePath];
        if (data.length > 0) {
            BOOL tempFlag = [self returnFlagWithURLRequest:[self upload2PHPServer:filePath type:UPLOAD_TYPE_FAT]];
            if (!tempFlag) {
                return NO;
            }
            
        }
    }
    return YES;
}

- (BOOL)runPED{
    NSString *documentDir = [[CommonValues getDocumentPath] stringByAppendingPathComponent:@"/PEDData"];
    NSArray *arr =  [FileOperation getFileArr:@"/PEDData"];
    NSLog(@"arr = %@",arr);
    for (int i = 0; i<arr.count; i++) {
        NSLog(@"PEDData  documentDir2 path 111= %@",[documentDir stringByAppendingPathComponent:arr[i]]);
        if([arr[i]  isEqualToString:@"Pedometer2Data.txt"]){
            NSString *filePath =  [documentDir stringByAppendingPathComponent:arr[i]];
            NSFileManager *fm = [[NSFileManager alloc]init];
            NSData *data = [fm contentsAtPath:filePath];
            if (data.length > 0) {
                BOOL tempFlag = [self returnFlagWithURLRequest:[self upload2PHPServer:filePath type:UPLOAD_TYPE_PED]];
                if (!tempFlag) {
                    return NO;
                }
                
            }
            
        }
        
    }
    return  YES;
}

- (BOOL)runSyncLog{
    NSLog(@"runECG_Realtime thread");
    
    NSString *documentDir = [[CommonValues getDocumentPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",SYNC_LOG_PATH]];
    NSArray *arr =  [FileOperation getFileArr:[NSString stringWithFormat:@"/%@",SYNC_LOG_PATH]];
    for (int i = 0; i<arr.count; i++) {
        NSLog(@"SYNC LOG   path  = %@",[documentDir stringByAppendingPathComponent:arr[i]]);
        NSString *filePath =  [documentDir stringByAppendingPathComponent:arr[i]];
        NSFileManager *fm = [[NSFileManager alloc]init];
        NSData *data = [fm contentsAtPath:filePath];
        if (data.length > 0) {
            BOOL tempFlag = [self returnFlagWithURLRequest:[self upload2PHPServer:filePath type:UPLOAD_TYPE_SYNC_LOG]];
            
            if (!tempFlag) {
                return NO;
            }
        }
    }
    return YES;
}

- (NSString *)dateStrFromlogFilePath :(NSString *)filePath{
//input filePath like "/var/mobile/Applications/7DEA77C8-2267-4F7B-8199-4694D2F89103/Documents/SyncLog/synclog_20150201.txt"
//return the time string like "20150201"
    NSString *tempDateStr = [filePath substringFromIndex:(filePath.length-12)];
    
    //    NSLog(@"tempTimeWith = %@",tempTimeWith);
    NSString *tempDate =  [tempDateStr substringToIndex:(tempDateStr.length - 4)];
    return tempDate;
}

- (NSString *)sleepFilePath{
    NSString *sleep2Path = [[[CommonValues getDocumentPath] stringByAppendingPathComponent:@"PEDData"] stringByAppendingPathComponent:@"SleepLog.txt"];
    NSLog(@"sleepFilePath = %@",sleep2Path);
    return sleep2Path;
}

- (NSString *)pedFilePath{
    NSString *ped2Path = [[[CommonValues getDocumentPath] stringByAppendingPathComponent:@"PEDData"] stringByAppendingPathComponent:@"Pedometer2Data.txt"];
    NSLog(@"pedFilePath = %@",ped2Path);
    return ped2Path;
}

- (NSString *)fatFilePath{
    NSString *fat2Path = [[[CommonValues getDocumentPath] stringByAppendingPathComponent:@"FATData"] stringByAppendingPathComponent:@"FatLog.txt"];
    NSLog(@"fatFilePath = %@",fat2Path);
    return fat2Path;
}


- (void)write2SyncLogFileWithTimeStr:(NSString *)timeStr withManualFlag:(BOOL)manualFlag withPassFlag:(BOOL)passFlag withDataReceived:(int)received withTotalDataAvailable:(int)available {
    NSString *manualStr;
    NSString *passStr;
    if (manualFlag) {
        manualStr = @"Manual";
    }
    else{
        manualStr = @"Auto";
    }
    if (passFlag) {
        passStr = @"Pass";
    }
    else{
        passStr = @"Fail";
    }
    NSString *fwVersionStr = [[TGBleManager sharedTGBleManager] fwVersion];
     NSString *SDKVersionStr = [[TGBleManager sharedTGBleManager] sdkVersion];
    NSString *deviceModelStr = [[TGBleManager sharedTGBleManager] hwModel];
    
    NSString *longStr = [NSString stringWithFormat:@"- Time\t%@ \n- Manual/Auto\t%@ \n- Pass/Fail\t%@ \n- FW Version\t%@ \n- SDK Version\t%@ \n- Device Model\t%@ \n- Data Received\t%d \n- Total Data Available\t%d \n\n",timeStr,manualStr,passStr,fwVersionStr,SDKVersionStr,deviceModelStr,received,available];
    NSLog(@"sync log longStr = %@",longStr);
    NSString *syncLogPath = [[[CommonValues getDocumentPath] stringByAppendingPathComponent:SYNC_LOG_PATH] stringByAppendingPathComponent:[NSString stringWithFormat:@"synclog_%@.txt",[self dateOfToday]]];
    [FileOperation write2FileAppendLastTimeContent:longStr filePath:syncLogPath];

}


- (NSMutableArray *)pedTotalInOneDayWithDate :(NSString*)dateStr WithDB : (sqlite3 *)db{
    int stepFromDb = 0 ;
    int caloriesFromDb = 0;
    int distanceFromDb = 0;
    NSMutableArray *pedArr = [[NSMutableArray alloc]init];
    sqlite3_stmt *stmt;
     NSString *quary2 = [NSString stringWithFormat:@"select datetime(datetime(time,'%ld hour','+0 minute'))  as time1,step,calories,distance from ped where time1 like '%@%%' order by time1 desc",(long)[[[CommonValues timeDateFormatter:DateFormatterDate] timeZone] secondsFromGMT]/3600,dateStr];
    
    
    if(sqlite3_prepare_v2(db, [quary2 UTF8String], -1, &stmt, NULL) == SQLITE_OK) {
        while(sqlite3_step(stmt) == SQLITE_ROW) {
            stepFromDb += sqlite3_column_int(stmt, 1);  //total step in one day
            caloriesFromDb += sqlite3_column_int(stmt, 2);  //total calories in one day
            distanceFromDb += sqlite3_column_int(stmt, 3); //total distance in one day
            
        }
        
        NSLog(@"stepFromDb = %d ,caloriesFromDb = %d ,distanceFromDb = %d",stepFromDb,caloriesFromDb,distanceFromDb);
        sqlite3_finalize(stmt);
    }

    [pedArr addObject:[NSNumber numberWithInt:stepFromDb]];
    [pedArr addObject:[NSNumber numberWithInt:caloriesFromDb]];
    [pedArr addObject:[NSNumber numberWithInt:distanceFromDb]];
    NSLog(@"pedArr = %@",pedArr);
    return pedArr;

    
}

- (NSMutableArray *)sleepArrWithDate :(NSString *)sleepDateStr withDB:(sqlite3 *)db{
    NSLog(@"sleepArrWithDate--");
    NSMutableArray *sleepTimeArray = [[NSMutableArray alloc]init];
    NSMutableArray *sleepPhaseArray = [[NSMutableArray alloc]init];
     NSMutableArray *sleepArray = [[NSMutableArray alloc]init];
    NSString *quary3  = @"select time,sleep_phase from SLEEP where time like '2014_01_18%'";
    sqlite3_stmt *stmt;
    quary3 = [NSString stringWithFormat:@"select datetime(datetime(time,'%ld hour','+0 minute'))  as time1,sleep_phase from SLEEP where time1 like '%@%%' order by time1 desc",(long)[[[CommonValues timeDateFormatter:DateFormatterDate] timeZone] secondsFromGMT]/3600,sleepDateStr];
    
    
    if(sqlite3_prepare_v2(db, [quary3 UTF8String], -1, &stmt, NULL) == SQLITE_OK) {
        while(sqlite3_step(stmt) == SQLITE_ROW) {
            const  unsigned  char *time = sqlite3_column_text(stmt, 0);
            NSString *timeStr = [NSString stringWithFormat:@"%s",time];
            int sleepPhase =  sqlite3_column_int(stmt,1);
            [ sleepTimeArray addObject:timeStr];
            [ sleepPhaseArray addObject:[NSString stringWithFormat:@"%d",sleepPhase]];
            
        }
        
//        NSLog(@"sleepPhaseArray =  %@",sleepPhaseArray);
//        NSLog(@"sleepTimeArray =  %@",sleepTimeArray);
        
        sqlite3_finalize(stmt);
    }
    [sleepArray addObject:sleepTimeArray];
    [sleepArray addObject:sleepPhaseArray];
    return sleepArray;
}

- (int)totalSleepTime : (NSMutableArray *)sleepTimeArray{
    int totalEndTime = 0;
    if (sleepTimeArray.count > 0) {
        NSString *bigTime =  [sleepTimeArray objectAtIndex:0];
        NSString *sTime =  [sleepTimeArray objectAtIndex:(sleepTimeArray.count-1)];
        NSRange bigRange = NSMakeRange(11, 2);
        NSRange bigRange2 = NSMakeRange(14, 2);
        NSString *bigTimeHour = [bigTime substringWithRange:bigRange];
        NSString *bigTimeMin = [bigTime substringWithRange:bigRange2];
        NSString *sTimeHour = [sTime substringWithRange:bigRange];
        NSString *sTimeMin = [sTime substringWithRange:bigRange2];
        NSLog(@"bigTimeHour 2 = %@",bigTimeHour);
        NSLog(@"sTimeHour 2 = %@",sTimeHour);
        NSInteger tmpTotalEndTime  = [bigTimeHour intValue]*60 + [bigTimeMin intValue] -
        [sTimeHour intValue]*60 - [sTimeMin intValue];
        
        totalEndTime += tmpTotalEndTime;
        
        NSLog(@"totalEndTime 2 = %d",totalEndTime);
        
    }

    return totalEndTime;
}

- (NSArray *)findLongestSleepTimeFromArr :(NSMutableArray *)sleepTimesArr{
    //sleepTimesArr like sleepTimesArr
    NSTimeInterval maxInterval = 0 ;
    NSArray *longestSleepArr;
    for (int i = 0 ; i < sleepTimesArr.count; i++) {
        NSArray *lastArr = [sleepTimesArr objectAtIndex:i];
        NSString *start = [lastArr objectAtIndex:0];
        NSString *end = [lastArr objectAtIndex:1];
        NSDate *startDate = [[CommonValues timeDateFormatter:DateFormatterTimeColon] dateFromString:start];
        NSDate *endDate = [[CommonValues timeDateFormatter:DateFormatterTimeColon] dateFromString:end];
        
        NSTimeInterval intervalSeconds =  [endDate timeIntervalSinceDate:startDate];
        NSLog(@"intervalSeconds = %f",intervalSeconds);
        if (intervalSeconds > maxInterval) {
            maxInterval = intervalSeconds;
            longestSleepArr = [sleepTimesArr objectAtIndex:i];
        }
    }
    NSLog(@"longestSleepArr = %@",longestSleepArr);
    
    return longestSleepArr;
}


- (void)startAnalysisSleepWithDate:(NSString *)dateFromSleep withDB:(sqlite3 *)db{
    
    NSMutableArray * const sleepTimesArr = /* [[NSMutableArray alloc] init];
    sleepTimesArr = */ [CommonValues calculateSleepStartEndTimeArr:dateFromSleep :db];
    for (int i = 0; i < sleepTimesArr.count; i++) {
        NSArray *lastArr = [sleepTimesArr objectAtIndex:i];
        NSString *start = [lastArr objectAtIndex:0];
        NSString *end = [lastArr objectAtIndex:1];
        NSLog(@"start = %@,end = %@",start,end);
    }
    NSLog(@"----------------");
    if (sleepTimesArr.count > 0) {
        //manual sleep by press the band
        NSArray *longestSleepArr = [self findLongestSleepTimeFromArr:sleepTimesArr];
        [CommonValues sleepAnalysisWithStartTime:longestSleepArr[0] withEndTime:longestSleepArr[1] withFlag:0 withDataBase:db];
    }
    else{
        //no manual sleep, use default start & end time in app
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *startPartTime = [userDefault objectForKey:AUTO_SLEEP_START_TIME];
        NSString *endPartTime = [userDefault objectForKey:AUTO_SLEEP_END_TIME];
  
        NSString *endTime = [NSString stringWithFormat:@"%@ %@:00",dateFromSleep,endPartTime];
        NSString *startTime = [StringOperation calculateStartTime:startPartTime stopTime:endPartTime date:dateFromSleep];
        [CommonValues sleepAnalysisWithStartTime:startTime withEndTime:endTime withFlag:1 withDataBase:db];
    }
    
}





@end

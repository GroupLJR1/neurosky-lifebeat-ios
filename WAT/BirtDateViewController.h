//
//  BirtDateViewController.h
//  WAT
//
//  Created by neurosky on 5/26/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TGBleManager.h"
#import "TGBleManagerDelegate.h"
@interface BirtDateViewController : UIViewController{


    __weak IBOutlet UIDatePicker *datePicker;
    NSDate *selectedDate;
}
- (IBAction)saveBtnClick:(id)sender;
- (IBAction)cancelBtnClick:(id)sender;

@end

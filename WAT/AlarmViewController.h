//
//  AlarmViewController.h
//  WAT
//
//  Created by neurosky on 5/26/14.
//  Copyright (c) 2014 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlarmViewController : UIViewController< UITableViewDelegate, UITableViewDataSource >
{

    __weak IBOutlet UIDatePicker *alarmPicker;
    NSString  *alarmStr;
    NSString  *timerStr;
    NSMutableArray *alarmMode;
    
    UIDatePicker *timerPicker;
    
    NSDate *selectedDate;
    NSDate *selectedTime;
    
    UITableView *repeatTableView;
    NSArray *repeatArray;
    NSMutableArray *repeatDayArray;

}
- (IBAction)cancelBtnClick:(id)sender;

- (IBAction)saveBtnClick:(id)sender;
@end

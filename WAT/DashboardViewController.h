//
//  DashboardViewController.h
//  WAT
//
//  Created by  NeuroSky on 9/22/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TGBleManager.h"
#import "TGLibEKG.h"
#import "TGLibEKGdelegate.h"
#import "TGBleManagerDelegate.h"
#import "NewCell.h"
#import "ZSYPopoverListView.h"
#import "CHYPorgressImageView.h"
#import <sqlite3.h>
#import "CardioPlotViewController.h"
#import "AppDelegate.h"
#import "ActivityStep.h"
#import "HeartViewController.h"
#import "sys/utsname.h"
#import "SettingsViewController.h"
#import "AboutViewController.h"
#import <AdSupport/AdSupport.h>
#import "Sleep_ViewController.h"
#import "SleepViewController.h"
#import <sys/socket.h>
#import <netinet/in.h>
#import <arpa/inet.h>
#import <netdb.h>
#import <SystemConfiguration/SCNetworkReachability.h>
#import "SVProgressHUD.h"
#import "HelpViewController.h"
#import "LogsViewController.h"
#import "CommonValues.h"
#import "ECGHistoryViewController.h"
#import "TGLibEKGdictionary.h"
#import "ActivityCalorieViewController.h"
#import "ActivityDistanceViewController.h"
//#import "HRVTrendDetailViewController.h"
#import "ActiveTimeViewController.h"
#import "TrendView.h"
#import "TrendLineView.h"
#import "Dashboard.h"
#import "Constant.h"
#import "FileOperation.h"
#import "StringOperation.h"
#import "HeartData.h"


@interface DashboardViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,ZSYPopoverListDatasource,ZSYPopoverListDelegate,TGLibEKGdelegate,TGBleManagerDelegate,CBCentralManagerDelegate,
CBPeripheralDelegate>{
    
    TGLibEKG * tglibEKG;
    BOOL bandOnRightWrist;
    NSString *deviceString;
    IBOutlet UIScrollView *myScrollView;
    __weak IBOutlet UIButton *menuBtn;
    float screenHeight;
    __weak IBOutlet UIView *syncView;
    __weak IBOutlet UILabel *progressLabel;
    __weak IBOutlet UIButton *retryBtn;
    IBOutlet UIView *myWholeView;
    __weak IBOutlet UILabel *syncMsgLabel;
    __weak IBOutlet UIProgressView *myProgressView;
    __weak IBOutlet UIButton *demoModeBtn;
    __weak IBOutlet UIButton *syncBtn;
    __weak IBOutlet UIButton *settingBtn;
    __weak IBOutlet UILabel *successful_updated_label;
    __weak IBOutlet UIImageView *connectStateImg;
    __weak IBOutlet CHYPorgressImageView *syncProgress;
    __weak IBOutlet UILabel *syncProgressLabel;
    __weak IBOutlet UIImageView *syncBgImg;
    __weak IBOutlet UIView *menuView;
    NSMutableArray  *devicesIDArray;
    BOOL startSyncFlag;
    BOOL pushMenuItemSynclag;
    //record RT ecg data
    NSString* ecgRT_FileName;
    NSFileManager* ecgRT_fileManager;
    NSArray* ecgRT_paths;
    NSString* ecgRT_documentDirectory;
    NSString* ecgRT_path;
    NSFileHandle* ecgRT_write;
    NSString *deviceName;
    int wat2ecgReceived;
    int wat2ecgSessionFlag;
    int wat2recordSesssionFlag;
    int wat2currentECGRecordTS;
    int wat2previousECGRecordTS;
    int wat2ecgCurrentTS;
    int wat2ecgRecordedTS;
    NSDate *wat2ecgiOSTS;
    int wat2watCurrentTS;
    //record pedometer data
    NSString* pedFileName;
    NSString* pedPath;
    NSArray* pedPaths;
    NSString* pedDocumentDirectory;
    NSFileManager* pedFM;
    NSFileHandle* pedWrite;
    int pedBytes[18];
    NSString* pedData;
    NSString* ped2FileName;
    NSString* ped2Path;
    NSArray* ped2Paths;
    NSString* ped2DocumentDirectory;
    NSFileManager* ped2FM;
    NSFileHandle* ped2Write;
    int ped2Bytes[20];
    NSString* ped2Data;
    NSString* sleep2Data;
    NSString* sleep2FileName;
    NSString* sleep2Path;
    NSArray* sleep2Paths;
    NSFileManager* sleep2FM;
    NSFileHandle* sleep2Write;
    NSString* fat2Data;
    NSString* fat2FileName;
    NSString* fat2Path;
    NSArray* fat2Paths;
    NSFileManager* fat2FM;
    NSFileHandle* fat2Write;
    int sync_total_bytes;
    int sync_bytes_read;
    int sync_flash_format_version_ID;
    int sync_ped_data_size;
    int sync_ped_bytes_read;
    int sync_ecg_data_size;
    int sync_ecg_bytes_read;
    int sync_diag_data_size;
    int sync_diag_bytes_read;
    int sync_sleep_data_size;
    int sync_sleep_bytes_read;
    int pedometerReceived;
    int pedometer2Received;
    int pedRecordedTS;
    int ped2RecordedTS;
    NSDate* iOSCurrentTime;
    NSString *iOSCurrentTimeString;
    BOOL pushDownFlag;
    BOOL PEDOMETERY_else_flag;
    BOOL alreadyFinishSync;
    //float percent;
    NSThread  *proThread;
    CustomerView *customerView1;    //distance view
    CustomerView *customerView2;   //active view
    CustomerView *customerView3;   //sleep view
    CircleProgress *steps_circle;
    CircleProgress *calories_circle;
    UILabel *label3;
    UIImageView *image1;
    UIImageView *image2;
    UIImageView *image1B;
    UIImageView *image2B;
    ZSYPopoverListView *listView ;
    BOOL eraseFlag;
    BOOL isSyncing;
    double progressValue;
    int progressCount;
    NSString *demoModeStr;
    BOOL fakeAdd2Percent;
    //test end header
    BOOL sync_in_progress ;
    BOOL sync_await_header ;
    int  before_ped_size ;
    int before_ecg_size ;
    int  before_diag_size;
    int before_sleep_size ;
    int watSleepBaseTS ;
    NSArray *defaultDevicesArray;
    NSString *readyToSaveDeviceName;
    NSString *readyToSaveDeviceID;
    NSFileHandle* fwrite;
    int ecgHistCurrentTS;
    int ecgHistRecordedTS;
    NSDate *ecgHistiOSTS;
    int watHistCurrentTS;
    BOOL ecgHistLookingForHeader;
    BOOL ecgHistFileOpen;
    int ecgHistByteLimit;
    int ecgHistReadCount;
    BOOL ecgRT_fileIsOpen;
    NSString* ecgFileName;
    //record ecg data
    NSFileManager* fileManager;
    NSArray* paths;
    NSString* documentDirectory;
    NSString* path;
    BOOL syncFinishFlag;
    //BOOL isSyncing;
    UILabel *label1;
    int  totalRead;
    int  totalData;
    NSString *documentDir;
    sqlite3  *db;
    int stepFromDb;
    int caloriesFromDb;
    int distanceFromDb;
    int goalStep;
    int goalCalories;
    int goalDistance;
    float goalSleep;
    float goalActiveTime;
    NSMutableArray *dbTimeArray;
    NSMutableArray *dbSleepTimeArray;
    NSMutableArray *sleepPhaseArray;
    NSMutableArray *sleepTimeArray;
    int  sleepIndex;
    int dayTotalSleepTime;
    NSString *time1;
    NSString *time2;
    NSMutableArray *sleepIntervalTimeArr;
    NSInteger totalEndTime;
    int totalStartTime;
    BOOL is256Rate;
    UILabel  *jinLabel;
    UILabel  *yinLabel;
    UILabel  *tongLabel;
    int jinCount;
    int yinCount;
    int tongCount;
    int  cardioZoneHeartRate;
    NSDate * cardioZoneHeartRate_TS;
    int  cardioZoneHRV;
    NSDate * cardioZoneHRV_TS;
    int stress;
    int  rrInt;
    NSDate * rrInt_TS;
    int  cardioZoneBreathingIndex;
    NSDate * cardioZoneBreathingIndex_TS;
    NSArray* cardioZone3D;
    NSDate * cardioZone3D_TS;
    int  heartAge;
    NSDate * heartAge_TS;
    int  heartFitnessLevel;
    NSDate * heartFitnessLevel_TS;
    int  relaxation;
    NSDate * relaxation_TS;
    int  mood;
    NSDate * mood_TS;
    BOOL hasPedoFlag;
    BOOL hasEkgFlag;
    BOOL hasSleepFlag;
    NSString *devName;
    NSMutableString *pedoLongStr;
    NSMutableString *ekgLongStr;
    NSMutableString *sleepLongStr;
    NSMutableString *fatLongStr;
    //version labels
    UILabel * fw_versionLabel ;
    UILabel * app_versionLabel;
    UILabel * sw_versionLabel ;
    UILabel * algo_versionLabel;
    UILabel * ble_versionLabel ;
    NSString *fw_versionStr;
    NSString *sw_versionStr;
  
    BOOL isConnectedFlag;
    BOOL connectBtnClickFlag;
    BOOL isConnectingFlag;
    NSMutableArray *devicesArray;
    NSMutableArray *mfgDataArray;
    NSMutableArray *rssiArray;
    NSMutableArray *candIDArray;
    int currentStepCount;
    int currentCalorieCount;
    int currentDistanceCount;
    UIAlertView *connectedAlert;
    UIAlertView *disconnectedAlert;
    BOOL selectDeviceBtnClickFlag;
    NSData *bondToken;
    BOOL isReleasingBond;
    NSString *database_path;
    NSDateFormatter *df;
    BOOL pedThreadFinishFalg;
    BOOL sleepThreadFinishFalg;
    BOOL ekgThreadFinishFalg;
    BOOL ekgRealtimeThreadFinishFalg;
    BOOL userproThreadFinishFalg;
    BOOL successBondedFlag;
    BOOL fatThreadFinishFalg;
    BOOL syncLogThreadFinishFalg;
    UIAlertView *bondErrorAlert;
    int sampleRate;
    NSString *snStr;
    BOOL hasFatFlag;
    BOOL isBonded;
    BOOL isRT;
    BOOL issyncing;
    BOOL wantRT;
    BOOL wantSyncing;
    BOOL haveDeviceFlag;
    NSTimer *timeOutTimer;
    BOOL firstEnterCandiDel;
    NSTimer *showListTimer;
    NSMutableArray *sortedArr;
    int sampleCount;
    //like sleep view
    int ystLastSleepPhase;
    NSString *ystLastSleepTime;
    NSString *yesterdayDateStr;
    NSMutableArray *sleepGraphDataSouceArr;
    int  lastIndex;
    NSDateFormatter *local_solid_df;
    NSMutableArray *hrvArr;
    NSMutableArray *hrArr;
    NSDate *ecgSyncDate;
    NSString *sleepStartTime;
    NSString *sleepEndTime;
    CFAbsoluteTime   recordedLastTime;
    BOOL jumpOutLoopFlag;
    BOOL lostBeforeMainOperation ;
    BOOL batteryFlag;
    BOOL wantErase;
    NSTimer *eraseNotResponseTimer;
    UIImageView *step_medal_image;
    UIImageView *calorie_medal_image;
    UIImageView *distance_medal_image;
    UIImageView *active_time_medal_image;
    UIImageView *sleep_medal_image;
    UIImageView *ecgCountIv1;
    UIImageView *ecgCountIv2;
    UIImageView *ecgCountIv3;
    UIImageView *hr_bpm_img;
    UILabel  *hr_bpm_labelLeft;
    UILabel  *hr_bpm_labeRight;
    UIImageView *  hrv_img ;
    UILabel  *hrv_labelLeft;
    UILabel  *hrv_labelRight;
    TrendView *trendView1 ;
    TrendLineView *trendLineView1;
    TrendView *trendView2;
    TrendLineView *trendLineView2;
    int sleep_hour;
    int sleep_min;
    UITapGestureRecognizer *tapGestureHR;
    UITapGestureRecognizer *tapGestureHRV ;
    UITapGestureRecognizer *tapGestureSleep;
    UITapGestureRecognizer *tapGestureDistance;
    int rememberedPercent;
    NSThread *updateNormalProgressThread;
    float latestPercent;
    NSThread *addPed2DbThread;
    NSMutableArray *pedMutArr;
    BOOL firstConnectFlag;
    int offsetHeight;
    NSTimer *askCurrentCountNoResponseTimer;
    NSTimer *trySyncTimer;
    BOOL syncFakeThreadFlag;
    float previousStepPercent;
    float previousCaloriePercent;
    UIBackgroundTaskIdentifier backgroundTask;
    UIAlertController *alertController ;
    Dashboard *dashboard;
    
}

@property (nonatomic, retain) NSIndexPath *selectedIndexPath;
@property (nonatomic, strong) CBCentralManager *manager;

- (IBAction)menuClick:(id)sender;
- (IBAction)demoModeClick:(id)sender;
- (void)stopScan;
- (IBAction)but_sync:(id)sender;
- (IBAction)but_select:(id)sender;
- (IBAction)connectClick:(id)sender;
- (IBAction)bondClick:(id)sender;
- (IBAction)retryBtnClick:(id)sender;
- (IBAction)syncBtnClick:(id)sender;
- (IBAction)settingBtnClick:(id)sender;
- (IBAction)aboutBtnClick:(id)sender;
- (IBAction)helpBtnClick:(id)sender;
- (IBAction)logsBtnClick:(id)sender;

- (IBAction)testBtnClick:(id)sender;

@end

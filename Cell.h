//
//  Cell.h
//  WAT
//
//  Created by Julia on 6/26/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCProgressBarView.h"
@interface Cell : UITableViewCell{
    MCProgressBarView *progressView;
    UILabel *progressLabel;
    UIImageView *labelBg;
    float progress;
}
@property (assign,nonatomic)float progress;
@property (retain,nonatomic) MCProgressBarView *progressView;
-(void)hideView:(BOOL) b;
@end

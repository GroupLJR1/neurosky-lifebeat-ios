//
//  EKGData.h
//  WAT
//
//  Created by Julia on 6/26/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EKGData : NSObject
@property (retain,nonatomic) NSString *dataName;
@property (assign,nonatomic) int dataValue;
@property (assign,nonatomic) float progressValue;

-(id)initWithName:(NSString *)name value:(int)v progress:(float)p;

@end

//
//  EKGData.m
//  WAT
//
//  Created by Julia on 6/26/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import "EKGData.h"

@implementation EKGData
@synthesize dataName,dataValue,progressValue;


-(id)initWithName:(NSString *)name value:(int)v progress:(float)p{
    if (self == [super init]) {
        self.dataName = name;
        self.dataValue = v;
        self.progressValue = p;
       
//        if ([name isEqualToString:@"HRV"]|| [name isEqualToString:@"心拍変動"]) {
//             self.dataValue = 30+arc4random()%100;
//        }
//        else if ([name isEqualToString:@"Breathing Index"]|| [name isEqualToString:@"呼吸数"]) {
//            self.dataValue = 15+arc4random()%7;
//        }
//        
//        else if ([name isEqualToString:@"Relaxation"]|| [name isEqualToString:@"リラックス度"]) {
//            self.dataValue = 75 + arc4random()%20;
//        }
//        
//        else if ([name isEqualToString:@"Heart Age"]|| [name isEqualToString:@"心臓年齢"]) {
//            self.dataValue = 25+arc4random()%10;
//        }
//        
//        else if ([name isEqualToString:@"Avg HR"]|| [name isEqualToString:@"平均心拍数"]) {
//            self.dataValue = 65+arc4random()%10;
//        }
//        else if ([name isEqualToString:@"Heart Fitness"]|| [name isEqualToString:@"ハートフィットネス　レベル"]) {
//            self.dataValue = 80;
//        }
//        
//        else if ([name isEqualToString:@"Steps"]) {
//            self.dataValue = 1800+ arc4random()%600;
//        }
//        
//        else if ([name isEqualToString:@"Sleep"]) {
//            self.dataValue = 6+(arc4random()%3);
//        } 
       
    }else{
        return nil;
    }
    return self;
}


-(void)setDataValue:(int)v{
    dataValue = v;
}
-(void)setDataName:(NSString *)name{
    dataName = name;
}
-(void)setProgressValue:(float)p{
    progressValue = p;
}
@end

//
//  Cell.m
//  WAT
//
//  Created by Julia on 6/26/13.
//  Copyright (c) 2013 Julia. All rights reserved.
//

#import "Cell.h"
#import "MCProgressBarView.h"
@implementation Cell
@synthesize progressView,progress;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        UIImage * backgroundImage = [[UIImage imageNamed:@"slider_gray"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 10, 0, 10)];
        UIImage * foregroundImage = [[UIImage imageNamed:@"slider_green"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 10, 0, 10)];
        progressView = [[MCProgressBarView alloc] initWithFrame:CGRectMake(150, 12, 140, 18)
                                                   backgroundImage:backgroundImage
                                                   foregroundImage:foregroundImage];
        [self addSubview:progressView];
        
        
        labelBg = [[UIImageView alloc] initWithFrame:CGRectMake(85,8, 39, 28)];
        [labelBg setImage:[UIImage imageNamed:@"box"]];
        progressLabel = [[UILabel alloc] initWithFrame:labelBg.frame];
        progressLabel.text = @"100%";
        [progressLabel setFont:[UIFont systemFontOfSize:12]];
        progressLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:labelBg];
        [self addSubview:progressLabel];
        
    }
    return self;
}

-(void)hideView:(BOOL) b{
    [labelBg setHidden:b];
    [progressLabel setHidden:b];
    [progressView setHidden:b];
    [self.detailTextLabel setHidden:!b];
}

-(void)setProgress:(float)p{
    progress = p;
    progressView.progress = p;
    int x = 100*p;
    progressLabel.textAlignment = NSTextAlignmentCenter;
    progressLabel.text = [NSString stringWithFormat:@"%d%%",x];
    [progressLabel setFrame:CGRectMake(148+p*118, progressLabel.frame.origin.y, progressLabel.frame.size.width, progressLabel.frame.size.height)];
    [labelBg setFrame:CGRectMake(148+p*118, labelBg.frame.origin.y, progressLabel.frame.size.width, progressLabel.frame.size.height)];

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end